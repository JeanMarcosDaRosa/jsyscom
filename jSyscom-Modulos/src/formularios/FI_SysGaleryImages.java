/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import base.FrmPrincipal;
import beans.Imagem;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import dao.ImagemDao;
import funcoes.OperacoesDB;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import utilitarios.Extra;
import utilitarios.Utils;

/**
 *
 * @author Jean
 */
public final class FI_SysGaleryImages extends javax.swing.JInternalFrame {

    private static Integer numInstancias = 0;
    private Integer numMaxInstancias = 65535;
    private static final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private static Integer seqInstancias = 0;
    private Usuario usuario = null;
    public JLabel exportLabel = null;
    private DefaultTableModel modelo = null;
    public ArrayList<Imagem> listaImagensCurrence = new ArrayList<>();
    private static final int sizeDefaultImg = 160;
    private int column = 4;
    private int widthMaxExport = 0;
    private int widthImage = 0;
    private int pagAtual = 0;
    private int nMaxImgPag = 12;//40
    private int nPagTot = 1;
    public Imagem exportImagem = null;
    private String sqlDefault = "select cd_imagem_prod as cod, blob_imagem_prod as blob, md5_imagem_prod as md5, nome_imagem_prod as nome from imagem_prod where cd_empresa = " + Variaveis.getEmpresa().getCodigo();

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }

    public FI_SysGaleryImages(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title + (seqInstancias > 0 ? (" - [" + String.valueOf(seqInstancias) + "]") : ""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            widthImage = sizeDefaultImg;
            tabelaImagens.setModel(getModel());
        }
    }

    public DefaultTableModel getModel() {
        if (modelo == null) {
            modelo = new DefaultTableModel() {
                @Override
                public Class getColumnClass(int columnIndex) {
                    Object o = getValueAt(0, columnIndex);
                    if (o == null) {
                        return Object.class;
                    }
                    return o.getClass();
                }

                @Override
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
            for (int i = 0; i < column; i++) {
                modelo.addColumn("");
            }
        }
        return modelo;
    }

    public void limpaModelo(DefaultTableModel m) {
        while (m.getRowCount() > 0) {
            m.removeRow(0);
        }
        m.setColumnCount(0);
        for (int i = 0; i < column; i++) {
            m.addColumn("");
        }
    }

    public void atualizaImagens(Integer pagina) {
        if ((pagina < 0) || (pagina > (nPagTot - 1))) {
            return;
        }
        try {
            limpaModelo(getModel());
            listaImagensCurrence.clear();
            listaImagensCurrence = ImagemDao.getListaImagemBanco(getSqlDefault(), (pagina * nMaxImgPag), nMaxImgPag);
            int rq = OperacoesDB.countQueryResult(sqlDefault);
            int div = rq / nMaxImgPag;
            if ((rq % nMaxImgPag) > 0) {
                nPagTot = div + 1;
            } else {
                nPagTot = div;
            }
            if (nPagTot < 1) {
                pagAtual = 0;
            }
            if (listaImagensCurrence != null) {
                int e = 0;
                while (e < listaImagensCurrence.size()) {
                    Object[] o = new Object[column];
                    int c = e;
                    for (int g = 0; g < column; g++) {
                        if (c < listaImagensCurrence.size()) {
                            Imagem i = (Imagem) listaImagensCurrence.get(c).clone();
                            ImageIcon image = i.getBlobImagem();
                            if (i.getBlobImagem().getIconWidth() > widthImage) {
                                i.setBlobImagem(new ImageIcon(image.getImage().getScaledInstance(widthImage, -1, Image.SCALE_DEFAULT)));
                            }
                            o[g] = i.getBlobImagem();
                        } else {
                            o[g] = null;
                        }
                        c++;
                    }
                    getModel().addRow(o);
                    e += column;
                }
                pagAtual = pagina;
                packRows(tabelaImagens, 0);
            } else {
                pagAtual = 0;
            }

        } catch (CloneNotSupportedException | IOException ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }
// Returns the preferred height of a row.
// The result is equal to the tallest cell in the row.

    public int getPreferredRowHeight(JTable table, int rowIndex, int margin) {
        // Get the current default height for all rows
        int height = table.getRowHeight();

        // Determine highest cell in the row
        for (int c = 0; c < table.getColumnCount(); c++) {
            TableCellRenderer renderer = table.getCellRenderer(rowIndex, c);
            Component comp = table.prepareRenderer(renderer, rowIndex, c);
            int h = comp.getPreferredSize().height + 2 * margin;
            height = Math.max(height, h);
        }
        return height;
    }

// The height of each row is set to the preferred height of the
// tallest cell in that row.
    public void packRows(JTable table, int margin) {
        packRows(table, 0, table.getRowCount(), margin);
    }

// For each row >= start and < end, the height of a
// row is set to the preferred height of the tallest cell
// in that row.
    public void packRows(JTable table, int start, int end, int margin) {
        for (int r = 0; r < table.getRowCount(); r++) {
            // Get the preferred height
            int h = getPreferredRowHeight(table, r, margin);

            // Now set the row height using the preferred height
            if (table.getRowHeight(r) != h) {
                table.setRowHeight(r, h);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuPopup = new javax.swing.JPopupMenu();
        menuAtualizar = new javax.swing.JMenuItem();
        menuExportar = new javax.swing.JMenuItem();
        pnlPrincipal = new javax.swing.JPanel();
        btnSair = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaImagens = new javax.swing.JTable();
        btnPrimeiro = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnProximo = new javax.swing.JButton();
        btnUltimo = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        sControl = new javax.swing.JSlider();
        edtPercent = new javax.swing.JLabel();

        menuAtualizar.setText("Atualizar Lista");
        menuAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAtualizarActionPerformed(evt);
            }
        });
        menuPopup.add(menuAtualizar);

        menuExportar.setText("Exportar Imagem");
        menuExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExportarActionPerformed(evt);
            }
        });
        menuPopup.add(menuExportar);

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        pnlPrincipal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnlPrincipalKeyPressed(evt);
            }
        });

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        tabelaImagens.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tabelaImagens.setCellSelectionEnabled(true);
        tabelaImagens.setComponentPopupMenu(menuPopup);
        tabelaImagens.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tabelaImagens.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaImagensMouseClicked(evt);
            }
        });
        tabelaImagens.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaImagensKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaImagens);

        btnPrimeiro.setText("<<");
        btnPrimeiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrimeiroActionPerformed(evt);
            }
        });

        btnAnterior.setText("<");
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        btnProximo.setText(">");
        btnProximo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProximoActionPerformed(evt);
            }
        });

        btnUltimo.setText(">>");
        btnUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUltimoActionPerformed(evt);
            }
        });

        jLabel1.setText("Pressione 'F5' para atualizar");

        sControl.setMaximum(200);
        sControl.setMinimum(1);
        sControl.setValue(100);
        sControl.setExtent(25);
        sControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sControlMouseReleased(evt);
            }
        });
        sControl.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sControlStateChanged(evt);
            }
        });

        edtPercent.setText("100%");

        javax.swing.GroupLayout pnlPrincipalLayout = new javax.swing.GroupLayout(pnlPrincipal);
        pnlPrincipal.setLayout(pnlPrincipalLayout);
        pnlPrincipalLayout.setHorizontalGroup(
            pnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlPrincipalLayout.createSequentialGroup()
                        .addComponent(btnPrimeiro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProximo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUltimo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sControl, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtPercent)
                        .addGap(35, 35, 35)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pnlPrincipalLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAnterior, btnPrimeiro, btnProximo, btnUltimo});

        pnlPrincipalLayout.setVerticalGroup(
            pnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSair, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnProximo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnUltimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addComponent(edtPercent))
                    .addComponent(sControl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        try {
            setClosed(true);
        } catch (PropertyVetoException ex) {
            Variaveis.addNewLog(ex.getMessage(), false, true, this);
        }
    }//GEN-LAST:event_btnSairActionPerformed

    private void menuAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAtualizarActionPerformed
        atualizaImagens(pagAtual);
    }//GEN-LAST:event_menuAtualizarActionPerformed

    private void tabelaImagensKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaImagensKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            evt = null;
            menuExportarActionPerformed(null);
        } else {
            if (evt.getKeyCode() == KeyEvent.VK_F5) {
                evt = null;
                menuAtualizarActionPerformed(null);
            }
        }
    }//GEN-LAST:event_tabelaImagensKeyPressed

    private void menuExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuExportarActionPerformed
        if (tabelaImagens.getSelectedRowCount() > 0) {
            if (exportLabel != null) {
                int index = ((tabelaImagens.getSelectedRow() * column) + tabelaImagens.getSelectedColumn());
                Imagem o = listaImagensCurrence.get(index);
                ImageIcon iFim = o.getBlobImagem();
                if (widthMaxExport > 0) {
                    if (iFim.getIconWidth() > widthMaxExport) {
                        iFim = new ImageIcon(iFim.getImage().getScaledInstance(widthMaxExport, -1, Image.SCALE_DEFAULT));
                    }
                }
                exportLabel.setIcon(iFim);
            }
            if (exportImagem != null) {
                int index = ((tabelaImagens.getSelectedRow() * column) + tabelaImagens.getSelectedColumn());
                Imagem o = listaImagensCurrence.get(index);
                ImageIcon iFim = o.getBlobImagem();
                if (widthMaxExport > 0) {
                    if (iFim.getIconWidth() > widthMaxExport) {
                        iFim = new ImageIcon(iFim.getImage().getScaledInstance(widthMaxExport, -1, Image.SCALE_DEFAULT));
                    }
                }
                exportImagem.setBlobImagem(iFim);
                exportImagem.setCodImagem(o.getCodImagem());
                exportImagem.setMd5Imagem(o.getMd5Imagem());
                exportImagem.setNomeImagem(o.getNomeImagem());
                exportImagem.setFlagPronto(isIcon);
            }
            try {
                setClosed(true);
            } catch (PropertyVetoException e) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
            }
        } else {
            Extra.Informacao(this, "Selecione a imagem na tabela!");
        }
    }//GEN-LAST:event_menuExportarActionPerformed

    private void tabelaImagensMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaImagensMouseClicked
        if (evt.getClickCount() >= 2) {
            menuExportarActionPerformed(null);
        }
    }//GEN-LAST:event_tabelaImagensMouseClicked

    private void sControlStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sControlStateChanged
        edtPercent.setText(String.valueOf(sControl.getValue()) + "%");
        float percent = sControl.getValue();
        widthImage = (int) (sizeDefaultImg * (percent / 100));
        if (tabelaImagens.getWidth() >= widthImage) {
            column = tabelaImagens.getWidth() / widthImage;
        } else {
            column = 1;
        }
    }//GEN-LAST:event_sControlStateChanged

    private void sControlMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sControlMouseReleased
        atualizaImagens(pagAtual);
    }//GEN-LAST:event_sControlMouseReleased

    private void pnlPrincipalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnlPrincipalKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            evt = null;
            menuAtualizarActionPerformed(null);
        }
    }//GEN-LAST:event_pnlPrincipalKeyPressed

    private void btnPrimeiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrimeiroActionPerformed
        atualizaImagens(0);
    }//GEN-LAST:event_btnPrimeiroActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        atualizaImagens(pagAtual - 1);
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void btnProximoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProximoActionPerformed
        atualizaImagens(pagAtual + 1);
    }//GEN-LAST:event_btnProximoActionPerformed

    private void btnUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUltimoActionPerformed
        atualizaImagens(nPagTot - 1);
    }//GEN-LAST:event_btnUltimoActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnPrimeiro;
    private javax.swing.JButton btnProximo;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnUltimo;
    private javax.swing.JLabel edtPercent;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem menuAtualizar;
    private javax.swing.JMenuItem menuExportar;
    private javax.swing.JPopupMenu menuPopup;
    private javax.swing.JPanel pnlPrincipal;
    private javax.swing.JSlider sControl;
    private javax.swing.JTable tabelaImagens;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the widthMaxExport
     */
    public int getWidthMaxExport() {
        return widthMaxExport;
    }

    /**
     * @param widthMaxExport the widthMaxExport to set
     */
    public void setWidthMaxExport(int widthMaxExport) {
        this.widthMaxExport = widthMaxExport;
    }

    /**
     * @return the sqlDefault
     */
    public String getSqlDefault() {
        return sqlDefault;
    }

    /**
     * @param sqlDefault the sqlDefault to set
     */
    public void setSqlDefault(String sqlDefault) {
        this.sqlDefault = sqlDefault;
        atualizaImagens(0);
    }
}
