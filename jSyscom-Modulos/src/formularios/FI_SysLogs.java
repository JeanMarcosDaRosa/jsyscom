/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package formularios;

import base.FrmPrincipal;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Hashtable;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import utilitarios.DataBase;
import utilitarios.Format;
import utilitarios.Utils;

/**
 *
 * @author Jean
 */
class corTabela extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, java.lang.Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (!isSelected) {
            Color c = table.getBackground();
            if ((row % 2) == 0 && c.getRed() > 20 && c.getGreen() > 20 && c.getBlue() > 20) {
                //setBackground(new Color(c.getRed()-20, c.getGreen()-20, c.getBlue()-0));  
                setBackground(new Color(247, 250, 251));
                //setForeground(new Color(255,0,0));  
            } else {
                //setBackground(c);  
                setBackground(new Color(255, 255, 255));
                //setForeground(new Color(255,255,0));  
            }
        }
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}

class AlinharCentro extends DefaultTableCellRenderer {

    public AlinharCentro() {
        setHorizontalAlignment(CENTER); // ou LEFT, RIGHT, etc
    }
};

class AlinharLeft extends DefaultTableCellRenderer {

    public AlinharLeft() {
        setHorizontalAlignment(LEFT); // ou LEFT, RIGHT, etc
    }
};

class AlinharDireita extends DefaultTableCellRenderer {

    public AlinharDireita() {
        setHorizontalAlignment(RIGHT); // ou LEFT, RIGHT, etc
    }
};

class SparseTableModel extends AbstractTableModel {

    private Hashtable lookup;
    private int rows;
    private final int columns;
    private final String headers[];

    public SparseTableModel(int rows, String columnHeaders[]) {
        if ((rows < 0) || (columnHeaders == null)) {
            throw new IllegalArgumentException(
                    "numero de linhas/colunas invalidas ");
        }
        this.rows = rows;
        this.columns = columnHeaders.length;
        headers = columnHeaders;
        lookup = new Hashtable();
    }

    @Override
    public int getColumnCount() {
        return columns;
    }

    public void Clear() {
        rows = 0;
        lookup.clear();
    }

    public void newRow() {
        rows++;
    }

    @Override
    public int getRowCount() {
        return rows;
    }

    @Override
    public String getColumnName(int column) {
        return headers[column];
    }

    @Override
    public Object getValueAt(int row, int column) {
        return lookup.get(new Point(row, column));
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        if ((rows < 0) || (columns < 0)) {
            throw new IllegalArgumentException("erro na linha/coluna ");
        }
        if ((row < rows) && (column < columns)) {
            lookup.put(new Point(row, column), value);
        }
    }
};

public final class FI_SysLogs extends javax.swing.JInternalFrame {

    String headers[] = {"Id", "Data", "Hora", "Titulo", "Operação", "Descrição", "Usuário", "Empresa", "Versão Sistema"};
    SparseTableModel model = new SparseTableModel(0, headers);
    SparseTableModel model2 = new SparseTableModel(0, headers);
    corTabela mtr = new corTabela();
    private static Integer numInstancias = 0;
    private Integer numMaxInstancias = 65535;
    private static final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private static Integer seqInstancias = 0;
    private Usuario usuario = null;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if(i>-1){
            seqInstancias += 1;
        }
    }

    /** Creates new form FI_SysLogs */
    public FI_SysLogs(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title+(seqInstancias>0?(" - ["+String.valueOf(seqInstancias)+"]"):""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            registraItemGrid("SELECT * FROM LOGS", null);
        }
    }

    public void limpaItensGrid() {
        jTable1.setModel(model2);
        model.Clear();
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(20);
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(75);
        this.repaint();
    }

    public void registraItemGrid(String SQL, Object[] par) {
        limpaItensGrid();
        Variaveis.addNewLog("SQL execute:\n" + SQL, false, false, this);
        try {
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {

                Date data = rs.getDate("data_logs");
                int cont = model.getRowCount();

                model.newRow();
                model.setValueAt(cont, cont, 0);
                model.setValueAt(Format.DateToStr(data), cont, 1);
                model.setValueAt(rs.getTime("hora_logs"), cont, 2);
                model.setValueAt(rs.getString("titulo_logs"), cont, 3);
                model.setValueAt(rs.getString("operacao_logs"), cont, 4);
                model.setValueAt(rs.getString("descricao_logs"), cont, 5);
                model.setValueAt(rs.getInt("cd_usuario"), cont, 6);
                model.setValueAt(rs.getInt("cd_empresa"), cont, 7);
                model.setValueAt(rs.getString("versao_js_logs"), cont, 8);


                jTable1.setModel(model2);
                jTable1.setModel(model);

                jTable1.getColumnModel().getColumn(0).setWidth(4);
                jTable1.getColumnModel().getColumn(1).setWidth(12);
                jTable1.getColumnModel().getColumn(2).setWidth(12);
                jTable1.getColumnModel().getColumn(3).setWidth(20);
                jTable1.getColumnModel().getColumn(4).setWidth(20);
                jTable1.getColumnModel().getColumn(5).setWidth(40);
                jTable1.getColumnModel().getColumn(6).setWidth(12);
                jTable1.getColumnModel().getColumn(7).setWidth(12);
                jTable1.getColumnModel().getColumn(8).setWidth(12);

                TableCellRenderer tcr0 = new AlinharCentro();
                TableColumn column0 = jTable1.getColumnModel().getColumn(0);
                column0.setCellRenderer(tcr0);
                TableCellRenderer tcr1 = new AlinharLeft();
                TableColumn column1 = jTable1.getColumnModel().getColumn(1);
                column1.setCellRenderer(tcr1);
                TableCellRenderer tcr2 = new AlinharLeft();
                TableColumn column2 = jTable1.getColumnModel().getColumn(2);
                column2.setCellRenderer(tcr2);
                TableCellRenderer tcr3 = new AlinharLeft();
                TableColumn column3 = jTable1.getColumnModel().getColumn(3);
                column3.setCellRenderer(tcr3);
                TableCellRenderer tcr4 = new AlinharLeft();
                TableColumn column4 = jTable1.getColumnModel().getColumn(4);
                column4.setCellRenderer(tcr4);
                TableCellRenderer tcr5 = new AlinharLeft();
                TableColumn column5 = jTable1.getColumnModel().getColumn(5);
                column5.setCellRenderer(tcr5);
                TableCellRenderer tcr6 = new AlinharCentro();
                TableColumn column6 = jTable1.getColumnModel().getColumn(6);
                column6.setCellRenderer(tcr6);
                TableCellRenderer tcr7 = new AlinharCentro();
                TableColumn column7 = jTable1.getColumnModel().getColumn(7);
                column7.setCellRenderer(tcr7);
                TableCellRenderer tcr8 = new AlinharCentro();
                TableColumn column8 = jTable1.getColumnModel().getColumn(8);
                column8.setCellRenderer(tcr8);
                this.repaint();
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName()+":\n"+Utils.getStackTrace(e), false, true, this);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        edtDesc = new javax.swing.JTextField();
        cbTipo = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        edtCodUser = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        cbOrdenacao = new javax.swing.JComboBox();
        edtData = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jLabel1.setText("Filtro:");

        cbTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Titulo_logs", "Operacao_logs", "Descricao_logs" }));
        cbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTipoActionPerformed(evt);
            }
        });

        jLabel2.setText("Tipo:");

        jLabel3.setText("Data:");

        jLabel4.setText("Código do Usuario:");

        jButton1.setText("Filtrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setText("Ordenação:");

        cbOrdenacao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "DATA_LOGS", "HORA_LOGS", "TITULO_LOGS", "OPERACAO_LOGS", "DESCRICAO_LOGS", "CD_USUARIO_LOGS", "VERSAO_JS_LOGS" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(edtDesc, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
                        .addGap(8, 8, 8)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(edtData, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(edtCodUser, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbOrdenacao, 0, 146, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(edtDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(edtCodUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jLabel5)
                    .addComponent(cbOrdenacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 619, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
                .addGap(15, 15, 15))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void cbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbTipoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Object[] par = null;
        String sql = "SELECT * FROM logs WHERE "
                + "((" + (cbTipo.getSelectedItem().toString().trim().toUpperCase()) + " like '%" + edtDesc.getText() + "%') or "
                + "(" + (cbTipo.getSelectedItem().toString().trim().toUpperCase()) + " like '%" + edtDesc.getText().toUpperCase() + "%') or "
                + "(" + (cbTipo.getSelectedItem().toString().trim().toUpperCase()) + " like '%" + edtDesc.getText().toLowerCase() + "%')) and "
                + "cd_empresa = "+Variaveis.getEmpresa().getCodigo()+" ";
        if (!"".equals(edtCodUser.getText().trim())) {
            sql += " and cd_usuario = " + edtCodUser.getText().trim();
        }
        if (!"".equals(edtData.getText())) {
            sql += " and data_logs = ?";
            par = new Object[1];
            try {
                par[0] = Format.StrToDate(edtData.getText().trim());
            } catch (Exception e) {
                par[0] = "current_date";
                Variaveis.addNewLog(this.getClass().getName()+":\n"+Utils.getStackTrace(e), false, false, this);
            }
        }
        sql += " order by " + cbOrdenacao.getSelectedItem().toString().trim().toLowerCase();
        registraItemGrid(sql, par);
    }//GEN-LAST:event_jButton1ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cbOrdenacao;
    private javax.swing.JComboBox cbTipo;
    private javax.swing.JTextField edtCodUser;
    private javax.swing.JTextField edtData;
    private javax.swing.JTextField edtDesc;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
