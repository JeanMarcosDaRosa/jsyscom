/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import base.FrmPrincipal;
import beans.*;
import dao.NFEntradaDao;
import dao.PessoaDao;
import funcoes.Util;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import utilitarios.*;

/**
 *
 * @author Jean
 */
public final class FI_CadEntrada extends javax.swing.JInternalFrame implements Observer, ListSelectionListener {
    
    private static Integer numInstancias = 0;
    private Integer numMaxInstancias = 65535;
    private static final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private static Integer seqInstancias = 0;
    boolean alterado = false;
    private Usuario usuario = null;
    private JFileChooser fc;
    private String nomeArq = null;
    
    public Usuario getUsuario() {
        return usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public Integer getNumInstancias() {
        return numInstancias;
    }
    
    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }
    
    public FI_CadEntrada(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title + (seqInstancias > 0 ? (" - [" + String.valueOf(seqInstancias) + "]") : ""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            ativaEdits(false);
            ativaButtons(true, true, false, false, false, false);
            imagemProd.addObserver(this);
            produto.addObserver(this);
            nfEntrada.addObserver(this);
            fornecedor.addObserver(this);
            transportadora.addObserver(this);
            pnlTabSub.setEnabledAt(1, false);
            pnlTabSub.setEnabledAt(2, false);
            pnlTabSub.setEnabledAt(3, false);
            tabelaItensProd.getSelectionModel().addListSelectionListener(this);
            tabelaItensProd.getColumnModel().getSelectionModel().addListSelectionListener(this);
            limpaCampos();
        }
    }

// Returns the preferred height of a row.
// The result is equal to the tallest cell in the row.
    public int getPreferredRowHeight(JTable table, int rowIndex, int margin) {
        // Get the current default height for all rows
        int height = table.getRowHeight();

        // Determine highest cell in the row
        for (int c = 0; c < table.getColumnCount(); c++) {
            TableCellRenderer renderer = table.getCellRenderer(rowIndex, c);
            Component comp = table.prepareRenderer(renderer, rowIndex, c);
            int h = comp.getPreferredSize().height + 2 * margin;
            height = Math.max(height, h);
        }
        return height;
    }

// The height of each row is set to the preferred height of the
// tallest cell in that row.
    public void packRows(JTable table, int margin) {
        packRows(table, 0, table.getRowCount(), margin);
    }

// For each row >= start and < end, the height of a
// row is set to the preferred height of the tallest cell
// in that row.
    public void packRows(JTable table, int start, int end, int margin) {
        for (int r = 0; r < table.getRowCount(); r++) {
            // Get the preferred height
            int h = getPreferredRowHeight(table, r, margin);

            // Now set the row height using the preferred height
            if (table.getRowHeight(r) != h) {
                table.setRowHeight(r, h);
            }
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == tabelaItensProd.getSelectionModel()
                && tabelaItensProd.getRowSelectionAllowed() && pnlTabSub.getSelectedComponent() == pnlItensProd) {
            try {
                if (tabelaItensProd.getRowCount() > 0 && tabelaItensProd.getSelectedRow() >= 0) {
                    lblPrecoProd.setText(Format.FormataValor(Format.doubleToString(((NFItemProd) listaNFItems.get(tabelaItensProd.getSelectedRow())).getValorUnitario()), Variaveis.getDecimal()));
                    setImagemProd(((NFItemProd) listaNFItems.get(tabelaItensProd.getSelectedRow())).getProduto());
                }
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        }
    }
    
    public void setNFEntrada() {
        try {
            limpaCampos();
            for (Endereco o : nfEntrada.getFornecedor().getEnderecos()) {
                cboxEnderFornecedor.addItem(o);
            }
            for (Endereco o : nfEntrada.getTransportadora().getEnderecos()) {
                cboxEnderTransp.addItem(o);
            }
            fornecedor.setCdPessoa(nfEntrada.getFornecedor().getCdPessoa());
            fornecedor.setNomePessoa(nfEntrada.getFornecedor().getNomePessoa());
            fornecedor.setCpfCnpjPessoa(nfEntrada.getFornecedor().getCpfCnpjPessoa());
            fornecedor.setEnderecos(nfEntrada.getFornecedor().getEnderecos());
            setFornecedor();
            transportadora.setCdPessoa(nfEntrada.getTransportadora().getCdPessoa());
            transportadora.setNomePessoa(nfEntrada.getTransportadora().getNomePessoa());
            transportadora.setCpfCnpjPessoa(nfEntrada.getTransportadora().getCpfCnpjPessoa());
            transportadora.setEnderecos(nfEntrada.getTransportadora().getEnderecos());
            setTransportadora();
            if (nfEntrada.getEnderecoFornecedor() != null) {
                cboxEnderFornecedor.setSelectedItem(nfEntrada.getEnderecoFornecedor());
            }
            edtTransportadora.setText(nfEntrada.getTransportadora().getNomePessoa());
            if (nfEntrada.getEnderecoTransp() != null) {
                cboxEnderTransp.setSelectedItem(nfEntrada.getEnderecoTransp());
            }
            if (nfEntrada.getTipoFrete() != null) {
                cboxTipoFrete.setSelectedItem(nfEntrada.getTipoFrete());
            }
            if (nfEntrada.getUfPlacaVeiculo() != null) {
                cbUF.setSelectedItem(nfEntrada.getUfPlacaVeiculo());
            }
            
            if (nfEntrada.getDataEmissao() != null) {
                edtDataEmissao.setText(Format.DateToStr(nfEntrada.getDataEmissao()));
            }
            if (nfEntrada.getDataEntrada() != null) {
                edtDataEntrada.setText(Format.DateToStr(nfEntrada.getDataEntrada()));
            }
            edtNroNf.setText(String.valueOf(nfEntrada.getNumeroNf()));
            edtSerie.setText(String.valueOf(nfEntrada.getSerieNf()));
            edtNatureza.setText(nfEntrada.getNaturezaOperacao());
            edtDadosAdicionais.setText(nfEntrada.getDadosAdicionaisNf());
            edtPlacaVeiculo.setText(nfEntrada.getPlacaVeiculo());
            edtContaAntt.setText(nfEntrada.getContaAntt());
            edtChave.setText(nfEntrada.getChaveAcesso());
            edtProtocolo.setText(nfEntrada.getProtocoloAutorizacao());
            edtOutrasDesp.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getDemaisDespesas()), Variaveis.getDecimal()));
            edtDesconto.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getDesconto()), Variaveis.getDecimal()));
            edtValorBICMS.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getBaseIcms()), Variaveis.getDecimal()));
            edtValorFrete.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getValorFrete()), Variaveis.getDecimal()));
            edtValorICMS.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getValorIcms()), Variaveis.getDecimal()));
            edtValorSeguro.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getValorSeguroFrete()), Variaveis.getDecimal()));
            edtValorTotNF.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getValorTotNota()), Variaveis.getDecimal()));
            edtValorTotProd.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getValorTotProd()), Variaveis.getDecimal()));
            edtCargaEsp.setText(nfEntrada.getEspecieCarga());
            edtCargaMarca.setText(nfEntrada.getMarcaCarga());
            edtCargaNro.setText(nfEntrada.getNumeracaoCarga());
            edtCargaPesoB.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getPesoBrutoCarga()), Variaveis.getDecimal()));
            edtCargaPesoL.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getPesoLiqCarga()), Variaveis.getDecimal()));
            edtCargaQtd.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getQuantidadeCarga()), Variaveis.getDecimal()));
            edtImpIssqnInscMun.setText(nfEntrada.getIssqnInscMun());
            edtImpIssqnBase.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getIssqnBaseCalc()), Variaveis.getDecimal()));
            edtImpIssqnVTot.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getIssqnvlrTotServicos()), Variaveis.getDecimal()));
            edtImpIssqnValor.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getIssqnValor()), Variaveis.getDecimal()));
            edtImpVIcmsSubs.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getValorIcmsSubs()), Variaveis.getDecimal()));
            edtImpBIcmsSubs.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getBaseIcmsSubs()), Variaveis.getDecimal()));
            edtImpVIpi.setText(Format.FormataValor(Format.doubleToString(nfEntrada.getValorIpi()), Variaveis.getDecimal()));
            listaNFFatura.clear();
            listaNFFatura.addAll(nfEntrada.getFaturas());
            listaNFItems.clear();
            listaNFItems.addAll(nfEntrada.getItemsProd());
            imagemProd.clearImagem();
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }
    
    public void populaNFEntrada() {
        try {
            Integer cd = null;
            if (nfEntrada.getNumeroNf() != null) {
                cd = nfEntrada.getNumeroNf();
            }
            nfEntrada.clearNFEntrada();
            if (cd != null) {
                nfEntrada.setNumeroNf(cd);
            } else {
                nfEntrada.setNumeroNf(Integer.parseInt(Validador.soNumeros(edtNroNf.getText().trim())));
            }
            nfEntrada.setFornecedor(fornecedor);
            nfEntrada.setTransportadora(transportadora);
            nfEntrada.setEnderecoFornecedor((Endereco) cboxEnderFornecedor.getSelectedItem());
            nfEntrada.setEnderecoTransp((Endereco) cboxEnderTransp.getSelectedItem());
            nfEntrada.setTipoFrete((String) cboxTipoFrete.getSelectedItem());
            nfEntrada.setUfPlacaVeiculo((String) cbUF.getSelectedItem());
            if (!Validador.soNumeros(edtDataEmissao.getText()).equals("")) {
                nfEntrada.setDataEmissao(Format.StrToDate(edtDataEmissao.getText()));
            }
            if (!Validador.soNumeros(edtDataEntrada.getText()).equals("")) {
                nfEntrada.setDataEntrada(Format.StrToDate(edtDataEntrada.getText()));
            }
            nfEntrada.setSerieNf(Integer.parseInt(Validador.soNumeros(edtSerie.getText().trim())));
            nfEntrada.setNaturezaOperacao(edtNatureza.getText());
            nfEntrada.setDadosAdicionaisNf(edtDadosAdicionais.getText());
            nfEntrada.setPlacaVeiculo(edtPlacaVeiculo.getText());
            nfEntrada.setContaAntt(edtContaAntt.getText());
            nfEntrada.setChaveAcesso(edtChave.getText());
            nfEntrada.setProtocoloAutorizacao(edtProtocolo.getText());
            nfEntrada.setDemaisDespesas(Format.stringToDouble(edtOutrasDesp.getText()));
            nfEntrada.setDesconto(Format.stringToDouble(edtDesconto.getText()));
            nfEntrada.setBaseIcms(Format.stringToDouble(edtValorBICMS.getText()));
            nfEntrada.setValorFrete(Format.stringToDouble(edtValorFrete.getText()));
            nfEntrada.setValorIcms(Format.stringToDouble(edtValorICMS.getText()));
            nfEntrada.setValorSeguroFrete(Format.stringToDouble(edtValorFrete.getText()));
            nfEntrada.setValorTotNota(Format.stringToDouble(edtValorTotNF.getText()));
            nfEntrada.setValorTotProd(Format.stringToDouble(edtValorTotProd.getText()));
            nfEntrada.setEspecieCarga(edtCargaEsp.getText());
            nfEntrada.setMarcaCarga(edtCargaMarca.getText());
            nfEntrada.setNumeracaoCarga(edtCargaNro.getText());
            nfEntrada.setPesoBrutoCarga(Format.stringToDouble(edtCargaPesoB.getText()));
            nfEntrada.setPesoLiqCarga(Format.stringToDouble(edtCargaPesoL.getText()));
            nfEntrada.setQuantidadeCarga(Format.stringToDouble(edtCargaQtd.getText()));
            nfEntrada.setIssqnInscMun(edtImpIssqnInscMun.getText());
            nfEntrada.setIssqnBaseCalc(Format.stringToDouble(edtImpIssqnBase.getText()));
            nfEntrada.setIssqnvlrTotServicos(Format.stringToDouble(edtImpIssqnVTot.getText()));
            nfEntrada.setIssqnValor(Format.stringToDouble(edtImpIssqnValor.getText()));
            nfEntrada.setValorIcmsSubs(Format.stringToDouble(edtImpVIcmsSubs.getText()));
            nfEntrada.setBaseIcmsSubs(Format.stringToDouble(edtImpBIcmsSubs.getText()));
            nfEntrada.setValorIpi(Format.stringToDouble(edtImpVIpi.getText()));
            nfEntrada.getItemsProd().clear();
            nfEntrada.getItemsProd().addAll(listaNFItems);
            nfEntrada.getFaturas().clear();
            nfEntrada.getFaturas().addAll(listaNFFatura);
            
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }
    
    private void cancelar() {
        if (alterado) {
            if (Extra.Pergunta(this, "Descartar alterações não salvas?")) {
                limpaCampos();
                ativaEdits(false);
                ativaButtons(true, true, false, false, false, false);
                alterado = false;
                nfEntrada.clearNFEntrada();
                fornecedor.clearPessoa();
                transportadora.clearTransportadora();
                pnlTabSub.setSelectedComponent(pnlInfoBasic);
                pnlTabSub.setEnabledAt(1, false);
                pnlTabSub.setEnabledAt(2, false);
                pnlTabSub.setEnabledAt(3, false);
            }
        } else {
            limpaCampos();
            ativaEdits(false);
            ativaButtons(true, true, false, false, false, false);
            alterado = false;
            nfEntrada.clearNFEntrada();
            fornecedor.clearPessoa();
            transportadora.clearTransportadora();
            pnlTabSub.setSelectedComponent(pnlInfoBasic);
            pnlTabSub.setEnabledAt(1, false);
            pnlTabSub.setEnabledAt(2, false);
            pnlTabSub.setEnabledAt(3, false);
        }
    }
    
    private void ativaEdits(Boolean ativa) {
        btnBuscaFornecedor.setEnabled(ativa);
        btnNovaTrasp.setEnabled(ativa);
        edtCodFornecedor.setEnabled(ativa);
        edtNomeFornecedor.setEnabled(ativa);
        cboxEnderFornecedor.setEnabled(ativa);
        edtNroNf.setEnabled(ativa);
        edtSerie.setEnabled(ativa);
        edtNroFatura.setEnabled(ativa);
        edtDataEmissao.setEnabled(ativa);
        edtDataEntrada.setEnabled(ativa);
        edtNatureza.setEnabled(ativa);
        edtDadosAdicionais.setEnabled(ativa);
        edtPlacaVeiculo.setEnabled(ativa);
        edtContaAntt.setEnabled(ativa);
        edtOutrasDesp.setEnabled(ativa);
        edtDesconto.setEnabled(ativa);
        edtValorBICMS.setEnabled(ativa);
        edtValorFatura.setEnabled(ativa);
        edtValorFrete.setEnabled(ativa);
        edtValorICMS.setEnabled(ativa);
        edtValorSeguro.setEnabled(ativa);
        edtValorTotNF.setEnabled(ativa);
        edtValorUnProd.setEnabled(ativa);
        edtDtVencimentoFatura.setEnabled(ativa);
        edtTransportadora.setEnabled(ativa);
        edtValorAliqProd.setEnabled(ativa);
        edtQtdProd.setEnabled(ativa);
        edtValorIcmsProd.setEnabled(ativa);
        edtValorTotProd.setEnabled(ativa);
        cboxEnderTransp.setEnabled(ativa);
        cboxTipoFrete.setEnabled(ativa);
        cbUF.setEnabled(ativa);
        edtCargaEsp.setEnabled(ativa);
        edtCargaMarca.setEnabled(ativa);
        edtCargaNro.setEnabled(ativa);
        edtCargaPesoB.setEnabled(ativa);
        edtCargaPesoL.setEnabled(ativa);
        edtCargaQtd.setEnabled(ativa);
        edtChave.setEnabled(ativa);
        edtProtocolo.setEnabled(ativa);
        edtImpBIcmsSubs.setEnabled(ativa);
        edtImpIssqnBase.setEnabled(ativa);
        edtImpIssqnInscMun.setEnabled(ativa);
        edtImpIssqnVTot.setEnabled(ativa);
        edtImpIssqnValor.setEnabled(ativa);
        edtImpVIcmsSubs.setEnabled(ativa);
        edtImpVIpi.setEnabled(ativa);
    }
    
    private void ativaButtons(Boolean buscar, Boolean novo, Boolean alterar, Boolean excluir, Boolean cancelar, Boolean salvar) {
        btnBuscarNF.setEnabled(buscar);
        btnCancNF.setEnabled(cancelar);
        btnExcluirNF.setEnabled(excluir);
        btnEditNF.setEnabled(alterar);
        btnNovoNF.setEnabled(novo);
        btnSaveNF.setEnabled(salvar);
    }
    
    private void limpaCampos() {
        edtCodFornecedor.setText("");
        edtNomeFornecedor.setText("");
        cboxEnderFornecedor.removeAllItems();
        edtNroNf.setText("");
        edtSerie.setText("");
        edtNroFatura.setText("");
        edtDataEmissao.setText("");
        edtDataEntrada.setText("");
        edtNatureza.setText("");
        edtDadosAdicionais.setText("");
        edtPlacaVeiculo.setText("");
        edtContaAntt.setText("");
        edtOutrasDesp.setText("0,00");
        edtDesconto.setText("0,00");
        edtValorBICMS.setText("0,00");
        edtValorFatura.setText("0,00");
        edtValorFrete.setText("0,00");
        edtValorICMS.setText("0,00");
        edtValorSeguro.setText("0,00");
        edtValorTotNF.setText("0,00");
        edtDtVencimentoFatura.setText("");
        edtTransportadora.setText("");
        edtValorTotProd.setText("0,00");
        edtCargaEsp.setText("");
        edtCargaMarca.setText("");
        edtCargaNro.setText("");
        edtCargaPesoB.setText("0,00");
        edtCargaPesoL.setText("0,00");
        edtCargaQtd.setText("0,00");
        edtChave.setText("");
        edtProtocolo.setText("");
        edtImpBIcmsSubs.setText("0,00");
        edtImpIssqnBase.setText("0,00");
        edtImpIssqnInscMun.setText("");
        edtImpIssqnVTot.setText("0,00");
        edtImpIssqnValor.setText("0,00");
        edtImpVIcmsSubs.setText("0,00");
        edtImpVIpi.setText("0,00");
        limpaProd();
        cboxEnderTransp.removeAllItems();
        if (cboxTipoFrete.getItemCount() > 0) {
            cboxTipoFrete.setSelectedIndex(0);
        }
        if (cbUF.getItemCount() > 0) {
            cbUF.setSelectedIndex(0);
        }
        listaNFFatura.clear();
        listaNFItems.clear();
    }
    
    public void setFornecedor() {
        try {
            edtCodFornecedor.setText(String.valueOf(fornecedor.getCdPessoa()));
            edtNomeFornecedor.setText(fornecedor.getNomePessoa());
            cboxEnderFornecedor.removeAllItems();
            Endereco endPrinc = null;
            for (Endereco o : fornecedor.getEnderecos()) {
                cboxEnderFornecedor.addItem(o);
                if (o.getPrincipal()) {
                    endPrinc = o;
                }
            }
            if (endPrinc != null) {
                cboxEnderFornecedor.setSelectedItem(endPrinc);
            }
            if (fornecedor.getEnderecos().size() <= 0) {
                Extra.Informacao(this, "Aviso!\n\n"
                        + "Este fornecedor não tem nenhum endereço associado!\n"
                        + "Não será possivel salvar a NF sem associar um endereço ao fornecedor!");
                cboxEnderFornecedor.addItem("");
                cboxEnderFornecedor.setSelectedIndex(0);
            }
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }
    
    public void setTransportadora() {
        try {
            edtTransportadora.setText(transportadora.getNomePessoa());
            cboxEnderTransp.removeAllItems();
            Endereco endPrinc = null;
            for (Endereco o : transportadora.getEnderecos()) {
                cboxEnderTransp.addItem(o);
                if (o.getPrincipal()) {
                    endPrinc = o;
                }
            }
            if (endPrinc != null) {
                cboxEnderTransp.setSelectedItem(endPrinc);
            }
            if (transportadora.getEnderecos().size() <= 0) {
                Extra.Informacao(this, "Aviso!\n\n"
                        + "Esta transportadora não tem nenhum endereço associado!\n"
                        + "Não será possivel salvar a NF sem associar um endereço a transportadora!");
                cboxEnderTransp.addItem("");
                cboxEnderTransp.setSelectedIndex(0);
            }
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        buttonGroup1 = new javax.swing.ButtonGroup();
        imagemProd = new beans.Imagem();
        fornecedor = new beans.Fornecedor();
        transportadora = new beans.Transportadora();
        listaNFItems = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<NFItemProd>());
        listaNFFatura = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<NFFatura>());
        nfEntrada = new beans.NFEntrada();
        produto = new beans.Produto();
        menuItensProd = new javax.swing.JPopupMenu();
        mItemProdRemover = new javax.swing.JMenuItem();
        mItemProdAlterar = new javax.swing.JMenuItem();
        menuFaturas = new javax.swing.JPopupMenu();
        mRemoverFatura = new javax.swing.JMenuItem();
        jScrollPane4 = new javax.swing.JScrollPane();
        pnlTabulado = new javax.swing.JTabbedPane();
        pnlPrincipal = new javax.swing.JPanel();
        pnlInfoFornecedor = new javax.swing.JPanel();
        btnBuscaFornecedor = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        edtCodFornecedor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        edtNomeFornecedor = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        cboxEnderFornecedor = new javax.swing.JComboBox();
        pnlTabSub = new javax.swing.JTabbedPane();
        pnlInfoBasic = new javax.swing.JPanel();
        pnlValores = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        edtValorFrete = new javax.swing.JTextField();
        edtValorTotProd = new javax.swing.JTextField();
        edtValorSeguro = new javax.swing.JTextField();
        edtDesconto = new javax.swing.JTextField();
        edtValorTotNF = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        edtValorBICMS = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        edtValorICMS = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        edtOutrasDesp = new javax.swing.JTextField();
        btnNovaTrasp = new javax.swing.JButton();
        jLabel47 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        edtNroNf = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        edtSerie = new javax.swing.JTextField();
        cboxTipoFrete = new javax.swing.JComboBox();
        jLabel37 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        edtDadosAdicionais = new javax.swing.JTextArea();
        cboxEnderTransp = new javax.swing.JComboBox();
        jLabel21 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        edtDataEmissao = new javax.swing.JFormattedTextField();
        jLabel16 = new javax.swing.JLabel();
        edtDataEntrada = new javax.swing.JFormattedTextField();
        jLabel17 = new javax.swing.JLabel();
        edtPlacaVeiculo = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        cbUF = new javax.swing.JComboBox();
        jLabel19 = new javax.swing.JLabel();
        edtContaAntt = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        edtNatureza = new javax.swing.JTextField();
        edtTransportadora = new javax.swing.JTextField();
        pnlItensProd = new javax.swing.JPanel();
        iconProd = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lblPrecoProd = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        lblSubTotalProd = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelaItensProd = new javax.swing.JTable();
        btnAddProd = new javax.swing.JButton();
        edtNomeProd = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        edtValorUnProd = new javax.swing.JTextField();
        edtValorAliqProd = new javax.swing.JTextField();
        edtValorIcmsProd = new javax.swing.JTextField();
        edtQtdProd = new javax.swing.JTextField();
        btnAdicionarProduto = new javax.swing.JButton();
        btnCancProd = new javax.swing.JButton();
        pnlFaturas = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        edtNroFatura = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        edtDtVencimentoFatura = new javax.swing.JFormattedTextField();
        edtValorFatura = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        btnAddFatura = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelaFaturas = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        edtChave = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        edtProtocolo = new javax.swing.JTextField();
        pnlCarga = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        edtCargaPesoB = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        edtCargaPesoL = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        edtCargaQtd = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        edtCargaNro = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        edtCargaMarca = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        edtCargaEsp = new javax.swing.JTextField();
        pnlImpostos = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        edtImpBIcmsSubs = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        edtImpVIcmsSubs = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        edtImpVIpi = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        edtImpIssqnVTot = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        edtImpIssqnBase = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        edtImpIssqnValor = new javax.swing.JTextField();
        edtImpIssqnInscMun = new javax.swing.JTextField();
        pnlBotoes = new javax.swing.JPanel();
        btnSair = new javax.swing.JButton();
        btnBuscarNF = new javax.swing.JButton();
        btnNovoNF = new javax.swing.JButton();
        btnEditNF = new javax.swing.JButton();
        btnExcluirNF = new javax.swing.JButton();
        btnCancNF = new javax.swing.JButton();
        btnSaveNF = new javax.swing.JButton();

        mItemProdRemover.setText("Remover Selecionado");
        mItemProdRemover.setToolTipText("");
        mItemProdRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mItemProdRemoverActionPerformed(evt);
            }
        });
        menuItensProd.add(mItemProdRemover);

        mItemProdAlterar.setText("Alterar");
        mItemProdAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mItemProdAlterarActionPerformed(evt);
            }
        });
        menuItensProd.add(mItemProdAlterar);

        mRemoverFatura.setText("Remover Selecionado");
        mRemoverFatura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mRemoverFaturaActionPerformed(evt);
            }
        });
        menuFaturas.add(mRemoverFatura);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
        });

        pnlInfoFornecedor.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações Fornecedor"));

        btnBuscaFornecedor.setText("...");
        btnBuscaFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaFornecedorActionPerformed(evt);
            }
        });

        jLabel1.setText("Código Fornecedor:");

        edtCodFornecedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtCodFornecedorKeyPressed(evt);
            }
        });

        jLabel2.setText("Nome Fornecedor:");

        edtNomeFornecedor.setEditable(false);
        edtNomeFornecedor.setEnabled(false);

        jLabel22.setText("Endereço Fornecedor:");

        javax.swing.GroupLayout pnlInfoFornecedorLayout = new javax.swing.GroupLayout(pnlInfoFornecedor);
        pnlInfoFornecedor.setLayout(pnlInfoFornecedorLayout);
        pnlInfoFornecedorLayout.setHorizontalGroup(
            pnlInfoFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInfoFornecedorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInfoFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlInfoFornecedorLayout.createSequentialGroup()
                        .addComponent(edtCodFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscaFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtNomeFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 656, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cboxEnderFornecedor, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnlInfoFornecedorLayout.setVerticalGroup(
            pnlInfoFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoFornecedorLayout.createSequentialGroup()
                .addGroup(pnlInfoFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscaFornecedor)
                    .addComponent(jLabel1)
                    .addComponent(edtCodFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(edtNomeFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInfoFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboxEnderFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pnlValores.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel3.setText("Valor total dos produtos:");

        jLabel4.setText("Valor do Seguro:");

        jLabel5.setText("Valor do Frete:");

        jLabel6.setText("Desconto:");

        jLabel7.setText("Valor total da Nota:");

        edtValorFrete.setForeground(java.awt.Color.blue);
        edtValorFrete.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorFrete.setText("0,00");
        edtValorFrete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorFreteKeyReleased(evt);
            }
        });

        edtValorTotProd.setForeground(java.awt.Color.blue);
        edtValorTotProd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorTotProd.setText("0,00");
        edtValorTotProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorTotProdKeyReleased(evt);
            }
        });

        edtValorSeguro.setForeground(java.awt.Color.blue);
        edtValorSeguro.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorSeguro.setText("0,00");
        edtValorSeguro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorSeguroKeyReleased(evt);
            }
        });

        edtDesconto.setForeground(java.awt.Color.blue);
        edtDesconto.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtDesconto.setText("0,00");
        edtDesconto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtDescontoKeyReleased(evt);
            }
        });

        edtValorTotNF.setForeground(java.awt.Color.blue);
        edtValorTotNF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorTotNF.setText("0,00");
        edtValorTotNF.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorTotNFKeyReleased(evt);
            }
        });

        jLabel20.setText("Base C. ICMS:");

        edtValorBICMS.setForeground(java.awt.Color.blue);
        edtValorBICMS.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorBICMS.setText("0,00");
        edtValorBICMS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorBICMSKeyReleased(evt);
            }
        });

        jLabel23.setText("Valor do ICMS:");

        edtValorICMS.setForeground(java.awt.Color.blue);
        edtValorICMS.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorICMS.setText("0,00");
        edtValorICMS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorICMSKeyReleased(evt);
            }
        });

        jLabel24.setText("Outras Desp:");

        edtOutrasDesp.setForeground(java.awt.Color.blue);
        edtOutrasDesp.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtOutrasDesp.setText("0,00");
        edtOutrasDesp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtOutrasDespKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout pnlValoresLayout = new javax.swing.GroupLayout(pnlValores);
        pnlValores.setLayout(pnlValoresLayout);
        pnlValoresLayout.setHorizontalGroup(
            pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlValoresLayout.createSequentialGroup()
                .addGroup(pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlValoresLayout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addComponent(jLabel5))
                    .addGroup(pnlValoresLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtValorTotProd, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtValorFrete, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addGroup(pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlValoresLayout.createSequentialGroup()
                        .addComponent(edtValorTotNF, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20))
                    .addGroup(pnlValoresLayout.createSequentialGroup()
                        .addComponent(edtValorSeguro, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlValoresLayout.createSequentialGroup()
                        .addComponent(edtValorBICMS, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                        .addComponent(jLabel23))
                    .addGroup(pnlValoresLayout.createSequentialGroup()
                        .addComponent(edtDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                        .addComponent(jLabel24)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtValorICMS, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtOutrasDesp, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52))
        );
        pnlValoresLayout.setVerticalGroup(
            pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlValoresLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(edtValorTotProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(edtValorTotNF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(edtValorBICMS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)
                    .addComponent(edtValorICMS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlValoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(edtValorFrete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(edtValorSeguro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(edtDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(edtOutrasDesp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        btnNovaTrasp.setText("...");
        btnNovaTrasp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaTraspActionPerformed(evt);
            }
        });

        jLabel47.setText("Transportadora:");

        jLabel8.setText("Nro NF:");

        jLabel9.setText("Série NF:");

        cboxTipoFrete.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FOB", "CIF" }));
        cboxTipoFrete.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboxTipoFreteItemStateChanged(evt);
            }
        });

        jLabel37.setText("Tipo de Frete:");

        jLabel10.setText("Dados Adicionais da NF:");

        edtDadosAdicionais.setColumns(20);
        edtDadosAdicionais.setRows(5);
        jScrollPane1.setViewportView(edtDadosAdicionais);

        jLabel21.setText("Endereço:");

        jLabel15.setText("Data Emissão:");

        try {
            edtDataEmissao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel16.setText("Data Entrada:");

        try {
            edtDataEntrada.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel17.setText("Placa Veiculo:");

        jLabel18.setText("UF:");

        cbUF.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO" }));

        jLabel19.setText("Conta ANTT:");

        jLabel27.setText("Natureza de Operação:");

        edtTransportadora.setEditable(false);
        edtTransportadora.setEnabled(false);

        javax.swing.GroupLayout pnlInfoBasicLayout = new javax.swing.GroupLayout(pnlInfoBasic);
        pnlInfoBasic.setLayout(pnlInfoBasicLayout);
        pnlInfoBasicLayout.setHorizontalGroup(
            pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoBasicLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(pnlValores, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlInfoBasicLayout.createSequentialGroup()
                        .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addGroup(pnlInfoBasicLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtNroNf, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtDataEmissao, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlInfoBasicLayout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtNatureza, javax.swing.GroupLayout.PREFERRED_SIZE, 717, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlInfoBasicLayout.createSequentialGroup()
                                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel47)
                                    .addComponent(jLabel21))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cboxEnderTransp, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(pnlInfoBasicLayout.createSequentialGroup()
                                        .addComponent(edtTransportadora)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnNovaTrasp, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel37)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cboxTipoFrete, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel19))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(edtPlacaVeiculo)
                                    .addComponent(edtContaAntt, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbUF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(50, 50, 50)))
                .addContainerGap())
        );
        pnlInfoBasicLayout.setVerticalGroup(
            pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoBasicLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(edtNroNf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(edtSerie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(edtDataEmissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(edtDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(edtNatureza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(btnNovaTrasp)
                    .addComponent(cboxTipoFrete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37)
                    .addComponent(jLabel17)
                    .addComponent(edtPlacaVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbUF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(edtTransportadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInfoBasicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboxEnderTransp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(jLabel19)
                    .addComponent(edtContaAntt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(pnlValores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlTabSub.addTab("Informações Basicas", pnlInfoBasic);

        iconProd.setBackground(new java.awt.Color(153, 204, 255));
        iconProd.setForeground(new java.awt.Color(0, 102, 102));
        iconProd.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 51, 51)));

        jLabel14.setText("Preço Unitario:");

        lblPrecoProd.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblPrecoProd.setForeground(java.awt.Color.blue);
        lblPrecoProd.setText("0,00");

        jLabel29.setText("Subtotal:");

        lblSubTotalProd.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblSubTotalProd.setForeground(java.awt.Color.blue);
        lblSubTotalProd.setText("0,00");

        tabelaItensProd.setComponentPopupMenu(menuItensProd);
        tabelaItensProd.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaNFItems, tabelaItensProd);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${produto}"));
        columnBinding.setColumnName("Produto");
        columnBinding.setColumnClass(beans.Produto.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${quantidade}"));
        columnBinding.setColumnName("Quantidade");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${aliquota}"));
        columnBinding.setColumnName("Aliquota");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorICMS}"));
        columnBinding.setColumnName("Valor ICMS");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorUnitario}"));
        columnBinding.setColumnName("Valor Unitario");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorTotal}"));
        columnBinding.setColumnName("Valor Total");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        tabelaItensProd.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tabelaItensProdPropertyChange(evt);
            }
        });
        jScrollPane3.setViewportView(tabelaItensProd);

        btnAddProd.setText("...");
        btnAddProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddProdActionPerformed(evt);
            }
        });

        edtNomeProd.setEnabled(false);

        jLabel25.setText("Produto:");

        jLabel26.setText("Aliq:");

        jLabel28.setText("Valor UN:");

        jLabel30.setText("Valor ICMS:");

        jLabel31.setText("Qtd:");

        edtValorUnProd.setForeground(java.awt.Color.blue);
        edtValorUnProd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorUnProd.setText("0,00");
        edtValorUnProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorUnProdKeyReleased(evt);
            }
        });

        edtValorAliqProd.setForeground(java.awt.Color.blue);
        edtValorAliqProd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorAliqProd.setText("0,00");
        edtValorAliqProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorAliqProdKeyReleased(evt);
            }
        });

        edtValorIcmsProd.setForeground(java.awt.Color.blue);
        edtValorIcmsProd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorIcmsProd.setText("0,00");
        edtValorIcmsProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorIcmsProdKeyReleased(evt);
            }
        });

        edtQtdProd.setForeground(java.awt.Color.blue);
        edtQtdProd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtQtdProd.setText("0,00");
        edtQtdProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtQtdProdKeyReleased(evt);
            }
        });

        btnAdicionarProduto.setText("Adicionar");
        btnAdicionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarProdutoActionPerformed(evt);
            }
        });

        btnCancProd.setText("Cancelar");
        btnCancProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancProdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlItensProdLayout = new javax.swing.GroupLayout(pnlItensProd);
        pnlItensProd.setLayout(pnlItensProdLayout);
        pnlItensProdLayout.setHorizontalGroup(
            pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlItensProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 809, Short.MAX_VALUE)
                    .addGroup(pnlItensProdLayout.createSequentialGroup()
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel25)
                            .addComponent(jLabel31))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlItensProdLayout.createSequentialGroup()
                                .addComponent(edtNomeProd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAddProd, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlItensProdLayout.createSequentialGroup()
                                .addComponent(edtQtdProd, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtValorAliqProd, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel30)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtValorIcmsProd, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlItensProdLayout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtValorUnProd, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlItensProdLayout.createSequentialGroup()
                                .addComponent(btnAdicionarProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCancProd)))
                        .addGap(28, 28, 28)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlItensProdLayout.createSequentialGroup()
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSubTotalProd)
                            .addComponent(lblPrecoProd))))
                .addContainerGap())
        );
        pnlItensProdLayout.setVerticalGroup(
            pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlItensProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlItensProdLayout.createSequentialGroup()
                        .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(lblPrecoProd))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel29)
                            .addComponent(lblSubTotalProd))
                        .addGap(0, 125, Short.MAX_VALUE))
                    .addGroup(pnlItensProdLayout.createSequentialGroup()
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAddProd)
                            .addComponent(edtNomeProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25)
                            .addComponent(jLabel28)
                            .addComponent(edtValorUnProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(jLabel30)
                            .addComponent(edtValorIcmsProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edtQtdProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31)
                            .addComponent(edtValorAliqProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAdicionarProduto)
                            .addComponent(btnCancProd))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pnlTabSub.addTab("Itens de Produtos", pnlItensProd);

        jLabel11.setText("Numero:");

        jLabel12.setText("Vencimento:");

        try {
            edtDtVencimentoFatura.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        edtValorFatura.setForeground(java.awt.Color.blue);
        edtValorFatura.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorFatura.setText("0,00");
        edtValorFatura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorFaturaKeyReleased(evt);
            }
        });

        jLabel13.setText("Valor:");

        btnAddFatura.setText("Adicionar");
        btnAddFatura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddFaturaActionPerformed(evt);
            }
        });

        tabelaFaturas.setComponentPopupMenu(menuFaturas);
        tabelaFaturas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaNFFatura, tabelaFaturas);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${numero}"));
        columnBinding.setColumnName("Numero");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${vencimento}"));
        columnBinding.setColumnName("Vencimento");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valor}"));
        columnBinding.setColumnName("Valor");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane2.setViewportView(tabelaFaturas);

        javax.swing.GroupLayout pnlFaturasLayout = new javax.swing.GroupLayout(pnlFaturas);
        pnlFaturas.setLayout(pnlFaturasLayout);
        pnlFaturasLayout.setHorizontalGroup(
            pnlFaturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFaturasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFaturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(pnlFaturasLayout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtNroFatura, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtDtVencimentoFatura, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtValorFatura, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAddFatura, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 374, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlFaturasLayout.setVerticalGroup(
            pnlFaturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFaturasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFaturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(edtNroFatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(edtDtVencimentoFatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtValorFatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(btnAddFatura))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlTabSub.addTab("Faturas", pnlFaturas);

        jLabel32.setText("Chave NF:");

        jLabel33.setText("Protocolo Auth:");

        pnlCarga.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados da Carga"));

        jLabel34.setText("Peso Bruto:");

        edtCargaPesoB.setForeground(java.awt.Color.blue);
        edtCargaPesoB.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtCargaPesoB.setText("0,00");
        edtCargaPesoB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtCargaPesoBKeyReleased(evt);
            }
        });

        jLabel35.setText("Peso Liq:");

        edtCargaPesoL.setForeground(java.awt.Color.blue);
        edtCargaPesoL.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtCargaPesoL.setText("0,00");
        edtCargaPesoL.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtCargaPesoLKeyReleased(evt);
            }
        });

        jLabel36.setText("Quantidade:");

        edtCargaQtd.setForeground(java.awt.Color.blue);
        edtCargaQtd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtCargaQtd.setText("0,00");
        edtCargaQtd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtCargaQtdKeyReleased(evt);
            }
        });

        jLabel38.setText("Numero:");

        edtCargaNro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtCargaNroActionPerformed(evt);
            }
        });

        jLabel39.setText("Marca:");

        jLabel40.setText("Espécie:");

        javax.swing.GroupLayout pnlCargaLayout = new javax.swing.GroupLayout(pnlCarga);
        pnlCarga.setLayout(pnlCargaLayout);
        pnlCargaLayout.setHorizontalGroup(
            pnlCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCargaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel38, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel34, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(edtCargaNro)
                    .addComponent(edtCargaPesoB, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(pnlCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel35)
                    .addComponent(jLabel39))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlCargaLayout.createSequentialGroup()
                        .addComponent(edtCargaPesoL, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtCargaQtd, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlCargaLayout.createSequentialGroup()
                        .addComponent(edtCargaMarca)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel40)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtCargaEsp, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlCargaLayout.setVerticalGroup(
            pnlCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCargaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(edtCargaNro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39)
                    .addComponent(edtCargaMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40)
                    .addComponent(edtCargaEsp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(edtCargaPesoB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(edtCargaPesoL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36)
                    .addComponent(edtCargaQtd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        pnlImpostos.setBorder(javax.swing.BorderFactory.createTitledBorder("Valores Impostos"));

        jLabel41.setText("Base Icms Subs:");

        edtImpBIcmsSubs.setForeground(java.awt.Color.blue);
        edtImpBIcmsSubs.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtImpBIcmsSubs.setText("0,00");
        edtImpBIcmsSubs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtImpBIcmsSubsKeyReleased(evt);
            }
        });

        jLabel42.setText("Valor Icms Subs:");

        edtImpVIcmsSubs.setForeground(java.awt.Color.blue);
        edtImpVIcmsSubs.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtImpVIcmsSubs.setText("0,00");
        edtImpVIcmsSubs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtImpVIcmsSubsKeyReleased(evt);
            }
        });

        jLabel43.setText("Valor IPI:");

        edtImpVIpi.setForeground(java.awt.Color.blue);
        edtImpVIpi.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtImpVIpi.setText("0,00");
        edtImpVIpi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtImpVIpiKeyReleased(evt);
            }
        });

        jLabel44.setText("Issqn Insc Mun:");

        jLabel45.setText("Issqn Valor Total:");

        edtImpIssqnVTot.setForeground(java.awt.Color.blue);
        edtImpIssqnVTot.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtImpIssqnVTot.setText("0,00");
        edtImpIssqnVTot.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtImpIssqnVTotKeyReleased(evt);
            }
        });

        jLabel46.setText("Issqn Base:");

        edtImpIssqnBase.setForeground(java.awt.Color.blue);
        edtImpIssqnBase.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtImpIssqnBase.setText("0,00");
        edtImpIssqnBase.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtImpIssqnBaseKeyReleased(evt);
            }
        });

        jLabel48.setText("Issqn Valor:");

        edtImpIssqnValor.setForeground(java.awt.Color.blue);
        edtImpIssqnValor.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtImpIssqnValor.setText("0,00");
        edtImpIssqnValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtImpIssqnValorKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout pnlImpostosLayout = new javax.swing.GroupLayout(pnlImpostos);
        pnlImpostos.setLayout(pnlImpostosLayout);
        pnlImpostosLayout.setHorizontalGroup(
            pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlImpostosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel45, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel41, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtImpBIcmsSubs)
                    .addComponent(edtImpIssqnVTot))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel42, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel46, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtImpVIcmsSubs)
                    .addComponent(edtImpIssqnBase))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel43, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel48, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtImpVIpi)
                    .addComponent(edtImpIssqnValor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel44)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(edtImpIssqnInscMun, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlImpostosLayout.setVerticalGroup(
            pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlImpostosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(edtImpBIcmsSubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42)
                    .addComponent(edtImpVIcmsSubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43)
                    .addComponent(edtImpVIpi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlImpostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel45)
                    .addComponent(edtImpIssqnVTot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel46)
                    .addComponent(edtImpIssqnBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel48)
                    .addComponent(edtImpIssqnValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel44)
                    .addComponent(edtImpIssqnInscMun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlImpostos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlCarga, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel32)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtChave, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtProtocolo, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 119, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlCarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlImpostos, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 150, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(edtChave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(edtProtocolo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28))
        );

        pnlTabSub.addTab("Adicionais", jPanel1);

        javax.swing.GroupLayout pnlPrincipalLayout = new javax.swing.GroupLayout(pnlPrincipal);
        pnlPrincipal.setLayout(pnlPrincipalLayout);
        pnlPrincipalLayout.setHorizontalGroup(
            pnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTabSub)
                    .addComponent(pnlInfoFornecedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlPrincipalLayout.setVerticalGroup(
            pnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(pnlInfoFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlTabSub, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(54, Short.MAX_VALUE))
        );

        pnlTabulado.addTab("Dados de Entrada", pnlPrincipal);

        jScrollPane4.setViewportView(pnlTabulado);

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnBuscarNF.setText("Buscar");
        btnBuscarNF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarNFActionPerformed(evt);
            }
        });

        btnNovoNF.setText("Novo");
        btnNovoNF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoNFActionPerformed(evt);
            }
        });

        btnEditNF.setText("Alterar");
        btnEditNF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditNFActionPerformed(evt);
            }
        });

        btnExcluirNF.setText("Excluir");
        btnExcluirNF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirNFActionPerformed(evt);
            }
        });

        btnCancNF.setText("Cancelar");
        btnCancNF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancNFActionPerformed(evt);
            }
        });

        btnSaveNF.setText("Confirmar");
        btnSaveNF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveNFActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBotoesLayout = new javax.swing.GroupLayout(pnlBotoes);
        pnlBotoes.setLayout(pnlBotoesLayout);
        pnlBotoesLayout.setHorizontalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBuscarNF)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNovoNF)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditNF)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExcluirNF)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancNF)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSaveNF)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 553, Short.MAX_VALUE)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlBotoesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBuscarNF, btnCancNF, btnEditNF, btnExcluirNF, btnNovoNF, btnSair, btnSaveNF});

        pnlBotoesLayout.setVerticalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addGroup(pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarNF)
                    .addComponent(btnNovoNF)
                    .addComponent(btnEditNF)
                    .addComponent(btnExcluirNF)
                    .addComponent(btnCancNF)
                    .addComponent(btnSaveNF)
                    .addComponent(btnSair))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 69, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 708, Short.MAX_VALUE)
                    .addComponent(pnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlBotoes.getAccessibleContext().setAccessibleParent(this);

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        if (alterado) {
            if (!Extra.Pergunta(this, "Sair sem salvar?")) {
                return;
            }
        }
        dispose();
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing
    
    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        try {
            setClosed(true);
        } catch (PropertyVetoException e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
	}//GEN-LAST:event_btnSairActionPerformed
    
    private void btnCancNFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancNFActionPerformed
        cancelar();
    }//GEN-LAST:event_btnCancNFActionPerformed
    
    public void setAtivoButtonBuscar(boolean ativar) {
        this.btnBuscarNF.setEnabled(ativar);
        this.btnBuscarNF.setVisible(ativar);
    }
    
    public void executaButtonEditar() {
        btnEditNFActionPerformed(null);
    }
    
    private void btnNovoNFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoNFActionPerformed
        cancelar();
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true);
        alterado = true;
        nfEntrada.clearNFEntrada();
    }//GEN-LAST:event_btnNovoNFActionPerformed
    
    private void btnBuscarNFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarNFActionPerformed
        FI_CadFindNFEntrada o = (FI_CadFindNFEntrada) Util.execucaoDiretaClass("formularios.FI_CadFindNFEntrada", "Modulos-jSyscom.jar", "Localiza Nota Fiscal de Entrada - " + Util.getSource(seqInstancias, FI_CadFindNFEntrada.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.setAtivoButtonNovo(false);
            o.nfEntrada = nfEntrada;
        }
    }//GEN-LAST:event_btnBuscarNFActionPerformed
    
    private void btnEditNFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditNFActionPerformed
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true);
        alterado = true;
        edtNroNf.setEnabled(false); // Bloqueio de proteçao de duplicacao no banco
        pnlTabSub.setEnabledAt(1, true);
        pnlTabSub.setEnabledAt(2, true);
        pnlTabSub.setEnabledAt(3, true);
    }//GEN-LAST:event_btnEditNFActionPerformed
    
    private Boolean salvar() {
        if (edtNroNf.getText().trim().equals("")) {
            Extra.Informacao(this, "Campo Nro NF é obrigatório!");
            return false;
        }
        if (edtSerie.getText().trim().equals("")) {
            Extra.Informacao(this, "Campo Série é obrigatório!");
            return false;
        }
        try {
            populaNFEntrada();
            if (NFEntradaDao.salvarNFEntrada(nfEntrada, Variaveis.getEmpresa())) {
                setNFEntrada();
                return true;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
        return false;
    }
    
    private void btnSaveNFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveNFActionPerformed
        if (salvar()) {
            ativaEdits(false);
            ativaButtons(true, true, true, true, true, false);
            pnlTabSub.setSelectedComponent(pnlInfoBasic);
            pnlTabSub.setEnabledAt(1, false);
            pnlTabSub.setEnabledAt(2, false);
            pnlTabSub.setEnabledAt(3, false);
            alterado = false;
            Extra.Informacao(this, "Salvo com sucesso!");
        } else {
            Extra.Informacao(this, "Falha ao salvar nota de entrada!");
        }
    }//GEN-LAST:event_btnSaveNFActionPerformed
    
    private void btnExcluirNFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirNFActionPerformed
        if (nfEntrada.getNumeroNf() != null) {
            if (Extra.Pergunta(this, "Excluir nota atual?")) {
                if (NFEntradaDao.excluirNFEntrada(nfEntrada, Variaveis.getEmpresa())) {
                    alterado = false;
                    cancelar();
                    Extra.Informacao(this, "Registro apagado com sucesso!");
                } else {
                    Extra.Informacao(this, "Falha ao apagar registro!");
                }
            }
        } else {
            alterado = false;
            cancelar();
        }
    }//GEN-LAST:event_btnExcluirNFActionPerformed
    
    private void btnBuscaFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaFornecedorActionPerformed
        FI_CadFindPessoa fornecedorForm = (FI_CadFindPessoa) Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", "Localiza Fornecedor - " + Util.getSource(numInstancias, FI_CadFindPessoa.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (fornecedorForm != null) {
            fornecedorForm.pessoa = fornecedor;
            fornecedorForm.setTipoObrigatorioBusca(new Fornecedor());
        }
    }//GEN-LAST:event_btnBuscaFornecedorActionPerformed
    
    private void btnNovaTraspActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaTraspActionPerformed
        FI_CadFindPessoa c = (FI_CadFindPessoa) Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", "Localiza Transportadora- " + Util.getSource(seqInstancias, FI_CadFindPessoa.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (c != null) {
            c.pessoa = transportadora;
            c.setTipoObrigatorioBusca(new Transportadora());
        }
    }//GEN-LAST:event_btnNovaTraspActionPerformed
    
    private void cboxTipoFreteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboxTipoFreteItemStateChanged
        if (cboxTipoFrete.getSelectedItem().toString().toUpperCase().trim().equals("FOB")) {
            edtValorFrete.setEnabled(true);
        } else {
            edtValorFrete.setText("0,00");
            edtValorFrete.setEnabled(false);
        }
    }//GEN-LAST:event_cboxTipoFreteItemStateChanged
    
    private void edtValorFreteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorFreteKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorFreteKeyReleased
    
    private void edtValorTotProdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorTotProdKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorTotProdKeyReleased
    
    private void edtValorSeguroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorSeguroKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorSeguroKeyReleased
    
    private void edtDescontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtDescontoKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtDescontoKeyReleased
    
    private void edtValorTotNFKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorTotNFKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorTotNFKeyReleased
    
    private void edtValorFaturaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorFaturaKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorFaturaKeyReleased
    
    private void tabelaItensProdPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tabelaItensProdPropertyChange
    }//GEN-LAST:event_tabelaItensProdPropertyChange
    
    private void edtValorBICMSKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorBICMSKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorBICMSKeyReleased
    
    private void edtValorICMSKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorICMSKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorICMSKeyReleased
    
    private void edtOutrasDespKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtOutrasDespKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtOutrasDespKeyReleased
    
    private void btnAddFaturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddFaturaActionPerformed
        if (!edtNroFatura.getText().trim().equals("") && !Validador.soNumeros(edtDtVencimentoFatura.getText()).trim().equals("") && !edtValorFatura.getText().trim().equals("")) {
            try {
                NFFatura f = new NFFatura();
                f.setNfEntrada(nfEntrada);
                f.setNumero(edtNroFatura.getText().trim());
                f.setValor(Format.stringToDouble(edtValorFatura.getText().trim()));
                f.setVencimento(Format.StrToDate(edtDtVencimentoFatura.getText()));
                if (!listaNFFatura.contains(f)) {
                    listaNFFatura.add(f);
                    edtNroFatura.setText("");
                    edtDtVencimentoFatura.setText("");
                    edtValorFatura.setText("0,00");
                } else {
                    Extra.Informacao(this, "Fatura já existe com este numero!");
                }
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        }
        edtNroFatura.requestFocus();
    }//GEN-LAST:event_btnAddFaturaActionPerformed
    
    private void edtValorUnProdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorUnProdKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorUnProdKeyReleased
    
    private void edtValorAliqProdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorAliqProdKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorAliqProdKeyReleased
    
    private void edtValorIcmsProdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorIcmsProdKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorIcmsProdKeyReleased
    
    private void edtQtdProdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtQtdProdKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtQtdProdKeyReleased
    
    private void btnAddProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddProdActionPerformed
        edtValorUnProd.requestFocus();
        FI_CadFindProduto o = (FI_CadFindProduto) Util.execucaoDiretaClass("formularios.FI_CadFindProduto", "Modulos-jSyscom.jar", "Localiza Produtos - " + Util.getSource(seqInstancias, FI_CadFindProduto.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.produto = produto;
        }
    }//GEN-LAST:event_btnAddProdActionPerformed
    
    private void limpaProd() {
        produto = new Produto();
        produto.addObserver(this);
        edtNomeProd.setText("");        
        edtValorAliqProd.setText("0,00");
        edtQtdProd.setText("0,00");
        edtValorIcmsProd.setText("0,00");
        edtValorUnProd.setText("0,00");
        lblPrecoProd.setText("0,00");
        lblSubTotalProd.setText("0,00");
        btnAdicionarProduto.setEnabled(false);
        btnCancProd.setEnabled(false);
        edtValorAliqProd.setEnabled(false);
        edtQtdProd.setEnabled(false);
        edtValorIcmsProd.setEnabled(false);
        edtValorUnProd.setEnabled(false);
        tabelaItensProd.setEnabled(true);
        limpaImagemProd();
    }
    
    private void limpaImagemProd() {
        iconProd.setIcon(null);
    }
    
    public void setImagemProd(Produto prod) {
        if (prod != null) {
            if (prod.getImagemProduto().getBlobImagem() != null) {
                if (prod.getImagemProduto().getBlobImagem().getIconWidth() > 256) {
                    imagemProd.setBlobImagem(new ImageIcon(prod.getImagemProduto().getBlobImagem().getImage().getScaledInstance(256, -1, Image.SCALE_DEFAULT)));
                    iconProd.setIcon(imagemProd.getBlobImagem());
                } else {
                    iconProd.setIcon(prod.getImagemProduto().getBlobImagem());
                }
            }
        }
    }
    
    public void setProduto() {
        try {
            edtNomeProd.setText(produto.getDescricaoProduto());
            lblPrecoProd.setText(Format.FormataValor(Format.doubleToString(produto.getValorVendaProduto()), Variaveis.getDecimal()));
            setImagemProd(produto);
            btnAdicionarProduto.setEnabled(true);
            btnCancProd.setEnabled(true);
            edtValorAliqProd.setEnabled(true);
            edtQtdProd.setEnabled(true);
            edtValorIcmsProd.setEnabled(true);
            edtValorUnProd.setEnabled(true);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }
    
    public void limpaNFItem() {
        produto = new Produto();
        produto.addObserver(this);
        edtNomeProd.setText("");
        edtValorUnProd.setText("0,00");
        edtValorIcmsProd.setText("0,00");
        edtValorAliqProd.setText("0,00");
        edtQtdProd.setText("0,00");
        lblPrecoProd.setText("0,00");
        btnAdicionarProduto.setEnabled(false);
        btnCancProd.setEnabled(false);
        edtValorAliqProd.setEnabled(false);
        edtQtdProd.setEnabled(false);
        edtValorIcmsProd.setEnabled(false);
        edtValorTotProd.setEnabled(false);
        tabelaItensProd.setEnabled(true);
        limpaImagemProd();
    }
    
    public void setNFItem(NFItemProd nfItem) {
        try {
            produto = nfItem.getProduto();
            setProduto();
            edtValorUnProd.setText(Format.FormataValor(Format.doubleToString(nfItem.getValorUnitario()), Variaveis.getDecimal()));
            edtValorIcmsProd.setText(Format.FormataValor(Format.doubleToString(nfItem.getValorICMS()), Variaveis.getDecimal()));
            edtValorAliqProd.setText(Format.FormataValor(Format.doubleToString(nfItem.getAliquota()), Variaveis.getDecimal()));
            edtQtdProd.setText(Format.FormataValor(Format.doubleToString(nfItem.getQuantidade()), Variaveis.getDecimal()));
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }
    
    private void recalculaTotais() {
        try {
            Double valorItensProd = 0.00;
            for (NFItemProd o : listaNFItems) {
                valorItensProd += o.getValorTotal();
            }
            lblSubTotalProd.setText(Format.FormataValor(Format.doubleToString(valorItensProd), Variaveis.getDecimal()));
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }
    
    private void btnAdicionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarProdutoActionPerformed
        try {
            if (produto.getCodProduto() != null) {
                NFItemProd item = new NFItemProd();
                if (!tabelaItensProd.isEnabled()) {
                    item = listaNFItems.get(tabelaItensProd.getSelectedRow());
                    listaNFItems.remove(item);
                }
                item.setProduto(produto);
                item.setNfEntrada(nfEntrada);
                item.setAliquota(Format.stringToDouble(edtValorAliqProd.getText().trim()));
                item.setQuantidade(Format.stringToDouble(edtQtdProd.getText().trim()));
                item.setValorUnitario(Format.stringToDouble(edtValorUnProd.getText().trim()));
                item.setValorTotal(item.getValorUnitario() * item.getQuantidade());
                item.setValorICMS(Format.stringToDouble(edtValorIcmsProd.getText().trim()));
                for (NFItemProd i : listaNFItems) {
                    if (item.equals(i)) {
                        item.setQuantidade(item.getQuantidade() + i.getQuantidade());
                        item.setValorTotal(item.getValorTotal() + i.getValorTotal());
                        listaNFItems.remove(i);
                        break;
                    }
                }
                listaNFItems.add(item);
                limpaProd();
                recalculaTotais();
            } else {
                Extra.Informacao(this, "Nenhum produto selecionado!");
            }
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_btnAdicionarProdutoActionPerformed
    
    private void edtCargaPesoBKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtCargaPesoBKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtCargaPesoBKeyReleased
    
    private void edtCargaPesoLKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtCargaPesoLKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtCargaPesoLKeyReleased
    
    private void edtCargaQtdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtCargaQtdKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtCargaQtdKeyReleased
    
    private void edtImpBIcmsSubsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtImpBIcmsSubsKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtImpBIcmsSubsKeyReleased
    
    private void edtImpVIcmsSubsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtImpVIcmsSubsKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtImpVIcmsSubsKeyReleased
    
    private void edtImpVIpiKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtImpVIpiKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtImpVIpiKeyReleased
    
    private void edtImpIssqnVTotKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtImpIssqnVTotKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtImpIssqnVTotKeyReleased
    
    private void edtImpIssqnBaseKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtImpIssqnBaseKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtImpIssqnBaseKeyReleased
    
    private void edtImpIssqnValorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtImpIssqnValorKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtImpIssqnValorKeyReleased
    
    private void edtCargaNroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtCargaNroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtCargaNroActionPerformed
    
    private void edtCodFornecedorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtCodFornecedorKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!PessoaDao.buscaFornecedor(Integer.parseInt(Validador.soNumeros(edtCodFornecedor.getText().trim())), Variaveis.getEmpresa()).getNomePessoa().trim().equals("")) {
                fornecedor.setCdPessoa(Integer.parseInt(Validador.soNumeros(edtCodFornecedor.getText().trim())));
                PessoaDao.buscaFornecedor(fornecedor, Variaveis.getEmpresa());
            } else {
                Extra.Informacao(this, "Fornecedor não encontrado!");
            }
        }
    }//GEN-LAST:event_edtCodFornecedorKeyPressed
    
    private void mItemProdRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mItemProdRemoverActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
            listaNFItems.remove(tabelaItensProd.getSelectedRow());
            recalculaTotais();
        }
    }//GEN-LAST:event_mItemProdRemoverActionPerformed
    
    private void mItemProdAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mItemProdAlterarActionPerformed
        try {
            limpaProd();
            setNFItem(listaNFItems.get(tabelaItensProd.getSelectedRow()));
            tabelaItensProd.setEnabled(false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_mItemProdAlterarActionPerformed
    
    private void btnCancProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancProdActionPerformed
        limpaNFItem();
    }//GEN-LAST:event_btnCancProdActionPerformed
    
    private void mRemoverFaturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mRemoverFaturaActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
            listaNFFatura.remove(tabelaFaturas.getSelectedRow());
        }
    }//GEN-LAST:event_mRemoverFaturaActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddFatura;
    private javax.swing.JButton btnAddProd;
    private javax.swing.JButton btnAdicionarProduto;
    private javax.swing.JButton btnBuscaFornecedor;
    private javax.swing.JButton btnBuscarNF;
    private javax.swing.JButton btnCancNF;
    private javax.swing.JButton btnCancProd;
    private javax.swing.JButton btnEditNF;
    private javax.swing.JButton btnExcluirNF;
    private javax.swing.JButton btnNovaTrasp;
    private javax.swing.JButton btnNovoNF;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSaveNF;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbUF;
    private javax.swing.JComboBox cboxEnderFornecedor;
    private javax.swing.JComboBox cboxEnderTransp;
    private javax.swing.JComboBox cboxTipoFrete;
    private javax.swing.JTextField edtCargaEsp;
    private javax.swing.JTextField edtCargaMarca;
    private javax.swing.JTextField edtCargaNro;
    private javax.swing.JTextField edtCargaPesoB;
    private javax.swing.JTextField edtCargaPesoL;
    private javax.swing.JTextField edtCargaQtd;
    private javax.swing.JTextField edtChave;
    private javax.swing.JTextField edtCodFornecedor;
    private javax.swing.JTextField edtContaAntt;
    private javax.swing.JTextArea edtDadosAdicionais;
    private javax.swing.JFormattedTextField edtDataEmissao;
    private javax.swing.JFormattedTextField edtDataEntrada;
    private javax.swing.JTextField edtDesconto;
    private javax.swing.JFormattedTextField edtDtVencimentoFatura;
    private javax.swing.JTextField edtImpBIcmsSubs;
    private javax.swing.JTextField edtImpIssqnBase;
    private javax.swing.JTextField edtImpIssqnInscMun;
    private javax.swing.JTextField edtImpIssqnVTot;
    private javax.swing.JTextField edtImpIssqnValor;
    private javax.swing.JTextField edtImpVIcmsSubs;
    private javax.swing.JTextField edtImpVIpi;
    private javax.swing.JTextField edtNatureza;
    private javax.swing.JTextField edtNomeFornecedor;
    private javax.swing.JTextField edtNomeProd;
    private javax.swing.JTextField edtNroFatura;
    private javax.swing.JTextField edtNroNf;
    private javax.swing.JTextField edtOutrasDesp;
    private javax.swing.JTextField edtPlacaVeiculo;
    private javax.swing.JTextField edtProtocolo;
    private javax.swing.JTextField edtQtdProd;
    private javax.swing.JTextField edtSerie;
    private javax.swing.JTextField edtTransportadora;
    private javax.swing.JTextField edtValorAliqProd;
    private javax.swing.JTextField edtValorBICMS;
    private javax.swing.JTextField edtValorFatura;
    private javax.swing.JTextField edtValorFrete;
    private javax.swing.JTextField edtValorICMS;
    private javax.swing.JTextField edtValorIcmsProd;
    private javax.swing.JTextField edtValorSeguro;
    private javax.swing.JTextField edtValorTotNF;
    private javax.swing.JTextField edtValorTotProd;
    private javax.swing.JTextField edtValorUnProd;
    private beans.Fornecedor fornecedor;
    private javax.swing.JLabel iconProd;
    private beans.Imagem imagemProd;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblPrecoProd;
    private javax.swing.JLabel lblSubTotalProd;
    private java.util.List<NFFatura> listaNFFatura;
    private java.util.List<NFItemProd> listaNFItems;
    private javax.swing.JMenuItem mItemProdAlterar;
    private javax.swing.JMenuItem mItemProdRemover;
    private javax.swing.JMenuItem mRemoverFatura;
    private javax.swing.JPopupMenu menuFaturas;
    private javax.swing.JPopupMenu menuItensProd;
    public beans.NFEntrada nfEntrada;
    private javax.swing.JPanel pnlBotoes;
    private javax.swing.JPanel pnlCarga;
    private javax.swing.JPanel pnlFaturas;
    private javax.swing.JPanel pnlImpostos;
    private javax.swing.JPanel pnlInfoBasic;
    private javax.swing.JPanel pnlInfoFornecedor;
    private javax.swing.JPanel pnlItensProd;
    private javax.swing.JPanel pnlPrincipal;
    private javax.swing.JTabbedPane pnlTabSub;
    private javax.swing.JTabbedPane pnlTabulado;
    private javax.swing.JPanel pnlValores;
    public beans.Produto produto;
    private javax.swing.JTable tabelaFaturas;
    private javax.swing.JTable tabelaItensProd;
    private beans.Transportadora transportadora;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof NFEntrada) {
            limpaCampos();
            ativaEdits(false);
            ativaButtons(true, true, true, true, true, false);
            alterado = false;
            setNFEntrada();
        } else if (o instanceof Produto) {
            setProduto();
        } else if (o instanceof Fornecedor) {
            setFornecedor();
        } else if (o instanceof Transportadora) {
            setTransportadora();
        }
    }
}
