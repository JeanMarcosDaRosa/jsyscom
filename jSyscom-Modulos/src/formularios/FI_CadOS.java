/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import base.FrmPrincipal;
import beans.*;
import dao.EquipamentoDao;
import dao.ImagemDao;
import dao.NextDaos;
import dao.OrdemServicoDao;
import dao.PessoaDao;
import dao.ProdutoDao;
import dao.ServicoDao;
import funcoes.Util;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import utilitarios.*;

/**
 *
 * @author Jean
 */
public final class FI_CadOS extends javax.swing.JInternalFrame implements Observer, ListSelectionListener {

    private static Integer numInstancias = 0;
    private Integer numMaxInstancias = 65535;
    private static final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private static Integer seqInstancias = 0;
    boolean alterado = false;
    private Usuario usuario = null;
    private JFileChooser fc;
    private String nomeArq = null;
    boolean aa = true;
    private TipoEquipamento tipoEquipAnterior = null;
    private DefaultTableModel modelo = null;
    public ArrayList<Imagem> listaImagensCurrence = new ArrayList<>();
    private static final int sizeDefaultImg = 300;
    public JLabel label = null;
    private int widthImage = 0;
    private int column = 2;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }

    public FI_CadOS(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title + (seqInstancias > 0 ? (" - [" + String.valueOf(seqInstancias) + "]") : ""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            ativaEdits(false);
            ativaButtons(true, true, false, false, false, false);
            loadLstLocalProd(ProdutoDao.getAllLocaisProdutos(Variaveis.getEmpresa()));
            loadLstTipoEquipamento(EquipamentoDao.getAllTipoEquipamento(Variaveis.getEmpresa()));
            loadLstTransportadora(PessoaDao.getAllTransportadoras(Variaveis.getEmpresa()));
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
            pnlTabulado.setEnabledAt(4, false);
            imagemConexao.addObserver(this);
            ordemServico.addObserver(this);
            tipoEquipamento.addObserver(this);
            transportadora.addObserver(this);
            localProd.addObserver(this);
            cliente.addObserver(this);
            produto.addObserver(this);
            servico.addObserver(this);
            iconProd.setHorizontalAlignment(JLabel.CENTER);
            iconConexao.setHorizontalAlignment(JLabel.CENTER);
            tabelaItensProd.getSelectionModel().addListSelectionListener(this);
            tabelaItensProd.getColumnModel().getSelectionModel().addListSelectionListener(this);
            limpaCampos();
            widthImage = sizeDefaultImg;
            tabelaImagens.setModel(getModel());
        }
    }

    public DefaultTableModel getModel() {
        if (modelo == null) {
            modelo = new DefaultTableModel() {
                @Override
                public Class getColumnClass(int columnIndex) {
                    Object o = getValueAt(0, columnIndex);
                    if (o == null) {
                        return Object.class;
                    }
                    return o.getClass();
                }

                @Override
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
            for (int i = 0; i < column; i++) {
                modelo.addColumn("");
            }
        }
        return modelo;
    }

    public void limpaModelo(DefaultTableModel m) {
        while (m.getRowCount() > 0) {
            m.removeRow(0);
        }
        m.setColumnCount(0);
        for (int i = 0; i < column; i++) {
            m.addColumn("");
        }
    }

    // Returns the preferred height of a row.
// The result is equal to the tallest cell in the row.
    public int getPreferredRowHeight(JTable table, int rowIndex, int margin) {
        // Get the current default height for all rows
        int height = table.getRowHeight();

        // Determine highest cell in the row
        for (int c = 0; c < table.getColumnCount(); c++) {
            TableCellRenderer renderer = table.getCellRenderer(rowIndex, c);
            Component comp = table.prepareRenderer(renderer, rowIndex, c);
            int h = comp.getPreferredSize().height + 2 * margin;
            height = Math.max(height, h);
        }
        return height;
    }

// The height of each row is set to the preferred height of the
// tallest cell in that row.
    public void packRows(JTable table, int margin) {
        packRows(table, 0, table.getRowCount(), margin);
    }

// For each row >= start and < end, the height of a
// row is set to the preferred height of the tallest cell
// in that row.
    public void packRows(JTable table, int start, int end, int margin) {
        for (int r = 0; r < table.getRowCount(); r++) {
            // Get the preferred height
            int h = getPreferredRowHeight(table, r, margin);

            // Now set the row height using the preferred height
            if (table.getRowHeight(r) != h) {
                table.setRowHeight(r, h);
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == tabelaItensProd.getSelectionModel()
                && tabelaItensProd.getRowSelectionAllowed() && pnlTabulado.getSelectedComponent() == pnlItensProd) {
            try {
                if (tabelaItensProd.getRowCount() > 0 && tabelaItensProd.getSelectedRow() >= 0) {
                    lblPrecoProd.setText(Format.FormataValor(Format.doubleToString(((ItemProdOS) listaItensProd.get(tabelaItensProd.getSelectedRow())).getValorUnitario()), Variaveis.getDecimal()));
                    setImagemProd(((ItemProdOS) listaItensProd.get(tabelaItensProd.getSelectedRow())).getProduto());
                }
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        }
    }

    public void loadLstLocalProd(List<LocalProd> lista) {
        String selected = "";
        if (cboxLocal.getSelectedItem() != null) {
            selected = cboxLocal.getSelectedItem().toString();
        }
        cboxLocal.removeAllItems();
        cboxLocal.addItem("");
        for (LocalProd cp : lista) {
            cboxLocal.addItem(cp);
        }
        cboxLocal.setSelectedItem(selected);
    }

    public void loadLstTipoEquipamento(List<TipoEquipamento> lista) {
        String selected = "";
        if (cboxTipoEquip.getSelectedItem() != null) {
            selected = cboxTipoEquip.getSelectedItem().toString();
        }
        cboxTipoEquip.removeAllItems();
        cboxTipoEquip.addItem("");
        for (TipoEquipamento cp : lista) {
            cboxTipoEquip.addItem(cp);
        }
        cboxTipoEquip.setSelectedItem(selected);
    }

    public void loadLstTransportadora(List<Transportadora> lista) {
        String selected = "";
        if (cboxTransp.getSelectedItem() != null) {
            selected = cboxTransp.getSelectedItem().toString();
        }
        cboxTransp.removeAllItems();
        cboxTransp.addItem("");

        for (Transportadora cp : lista) {
            cboxTransp.addItem(cp);
        }

        cboxTransp.setSelectedItem(selected);

    }

    public void setImagemConexao() {
        iconConexao.setText("");
        if (imagemConexao.getBlobImagem() != null) {
            if (imagemConexao.getBlobImagem().getIconWidth() > iconConexao.getWidth()) {
                imagemConexao.setBlobImagem(new ImageIcon(imagemConexao.getBlobImagem().getImage().getScaledInstance(iconConexao.getWidth(), -1, Image.SCALE_DEFAULT)));
            }
        } else {
            iconConexao.setText("Conexão Equipamento");
        }
        //equipamento.setConexao(imagemConexao);
        iconConexao.setIcon(imagemConexao.getBlobImagem());
    }

    public void setImagemProd(Produto prod) {
        if (prod != null) {
            if (prod.getImagemProduto().getBlobImagem() != null) {
                if (prod.getImagemProduto().getBlobImagem().getIconWidth() > 256) {
                    imagemProd.setBlobImagem(new ImageIcon(prod.getImagemProduto().getBlobImagem().getImage().getScaledInstance(256, -1, Image.SCALE_DEFAULT)));
                    iconProd.setIcon(imagemProd.getBlobImagem());
                } else {
                    iconProd.setIcon(prod.getImagemProduto().getBlobImagem());
                }
            }
        }
    }

    public void atualizaImagens() {
        //try {
        limpaModelo(getModel());
//            listaImagensCurrence.clear();
//            listaImagensCurrence = ImagemDao.getListaImagemBanco("select "
//                    + "   c.cd_conexao_equip as cod, "
//                    + "   c.blob_conexao_equip as blob, "
//                    + "   c.md5_conexao_equip as md5, "
//                    + "   c.nome_conexao_equip as nome "
//                    + "from "
//                    + "   conexao_equip c, "
//                    + "   imagens_equip i "
//                    + "where "
//                    + "   c.cd_empresa = i.cd_empresa and "
//                    + "   c.cd_conexao_equip = i.cd_conexao_equip and "
//                    + "   c.cd_empresa = " + Variaveis.getEmpresa().getCodigo() + " and "
//                    + "   i.cd_equip = " + equipamento.getCodEquipamento(), 0, 4);
        if (listaImagensCurrence != null) {
            int e = 0;
            while (e < listaImagensCurrence.size()) {
                Object[] o = new Object[column];
                int c = e;
                for (int g = 0; g < column; g++) {
                    if (c < listaImagensCurrence.size()) {
                        try {
                            Imagem i = (Imagem) listaImagensCurrence.get(c).clone();
                            ImageIcon image = i.getBlobImagem();
                            if (i.getBlobImagem().getIconWidth() > widthImage) {
                                i.setBlobImagem(new ImageIcon(image.getImage().getScaledInstance(widthImage, -1, Image.SCALE_DEFAULT)));
                            }
                            o[g] = i.getBlobImagem();
                        } catch (CloneNotSupportedException ex) {
                            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
                        }
                    } else {
                        o[g] = null;
                    }
                    c++;
                }
                getModel().addRow(o);
                e += column;
            }
            packRows(tabelaImagens, 0);
        }
        //} catch (IOException ex) {
        //    Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        //}
    }

    private void adicionaImagem(Imagem img) {
        if (img != null) {
            if (img.getBlobImagem() != null) {
                try {
                    if (!listaImagensCurrence.contains(img)) {
                        listaImagensCurrence.add((Imagem) img.clone());
                    }
                    atualizaImagens();
                } catch (CloneNotSupportedException ex) {
                    Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
                }
            }
        } else {
            Extra.Informacao(this, "Selecione a imagem do banco de dados ou de arquivos!");
        }
    }

    public void setLocalizacao() {
        loadLstLocalProd(ProdutoDao.getAllLocaisProdutos(Variaveis.getEmpresa()));
        cboxLocal.setSelectedItem(localProd);
    }

    public void setTipoEquip() {
        aa = false;
        loadLstTipoEquipamento(EquipamentoDao.getAllTipoEquipamento(Variaveis.getEmpresa()));
        cboxTipoEquip.setSelectedItem(tipoEquipamento);
        populaTabelaPropDef();
    }

    public void setTransportadora() {
        loadLstTransportadora(PessoaDao.getAllTransportadoras(Variaveis.getEmpresa()));
        cboxTransp.setSelectedItem(transportadora);
        cboxEnderTransp.removeAllItems();
        Endereco endPrinc = null;
        for (Endereco o : transportadora.getEnderecos()) {
            cboxEnderTransp.addItem(o);
            if (o.getPrincipal()) {
                endPrinc = o;
            }
        }
        if (endPrinc != null) {
            cboxEnderTransp.setSelectedItem(endPrinc);
        }
        if (cliente.getEnderecos().size() <= 0) {
            Extra.Informacao(this, "Aviso!\n\n"
                    + "Esta transportadora não tem nenhum endereço associado!\n"
                    + "Não será possivel salvar a OS sem associar um endereço a transportadora!");
            cboxEnderTransp.addItem("");
            cboxEnderTransp.setSelectedIndex(0);
        }
    }

    public void setCliente() {
        try {
            edtCodCliente.setText(String.valueOf(cliente.getCdPessoa()));
            edtNomeCliente.setText(cliente.getNomePessoa());
            edtCpfCliente.setText(cliente.getCpfCnpjPessoa());
            edtTelefoneCliente.setText(cliente.getTelefoneFixoPessoa());
            edtMargemCliente.setText(Format.FormataValor(Format.doubleToString(cliente.getDescontoCliente()), Variaveis.getDecimal()));
            cboxEnderCliente.removeAllItems();
            Endereco endPrinc = null;
            for (Endereco o : cliente.getEnderecos()) {
                cboxEnderCliente.addItem(o);
                if (o.getPrincipal()) {
                    endPrinc = o;
                }
            }
            if (endPrinc != null) {
                cboxEnderCliente.setSelectedItem(endPrinc);
            }
            cboxContatoCliente.removeAllItems();
            for (Contato o : cliente.getContatos()) {
                cboxContatoCliente.addItem(o);
            }
            if (cliente.getEnderecos().size() <= 0) {
                Extra.Informacao(this, "Aviso!\n\n"
                        + "Este cliente não tem nenhum endereço associado!\n"
                        + "Não será possivel salvar a OS sem associar um endereço ao cliente!");
                cboxEnderCliente.addItem("");
                cboxEnderCliente.setSelectedIndex(0);
            }
            if (cliente.getContatos().size() <= 0) {
                cboxContatoCliente.addItem("");
                cboxContatoCliente.setSelectedIndex(0);
            }
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }

    public void setProduto() {
        try {
            edtQtdeProduto.setText("1,00");
            chkProdOriginal.setSelected(true);
            edtDescrProduto.setText(produto.getDescricaoProduto());
            edtPrecoProd.setText(Format.FormataValor(Format.doubleToString(produto.getValorVendaProduto()), Variaveis.getDecimal()));
            lblPrecoProd.setText(Format.FormataValor(Format.doubleToString(produto.getValorVendaProduto()), Variaveis.getDecimal()));
            btnAddItemProd.setEnabled(true);
            btnCancelarItemProd.setEnabled(true);
            edtQtdeProduto.setEnabled(true);
            chkProdOriginal.setEnabled(true);
            setImagemProd(produto);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }

    public void setServico() {
        try {
            edtHoras.setText("0,00");
            chkMostraQtdHoras.setSelected(true);
            edtDescServ.setText(servico.getDescServico());
            edtValorUnitServ.setText(Format.FormataValor(Format.doubleToString(servico.getValorServico()), Variaveis.getDecimal()));
            edtValorBrutoServ.setText(Format.FormataValor(Format.doubleToString(servico.getValorServico()), Variaveis.getDecimal()));
            edtObservServ.setText("");
            btnAddItemServ.setEnabled(true);
            btnCancelarItemServ.setEnabled(true);
            edtHoras.setEnabled(true);
            chkMostraQtdHoras.setEnabled(true);
            edtValorBrutoServ.setEnabled(true);
            edtObservServ.setEnabled(true);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }

    public void setOS() {
        try {
            limpaCampos();
            for (Endereco o : ordemServico.getCliente().getEnderecos()) {
                cboxEnderCliente.addItem(o);
            }
            for (Endereco o : ordemServico.getTransportadora().getEnderecos()) {
                cboxEnderTransp.addItem(o);
            }
            for (Contato o : ordemServico.getCliente().getContatos()) {
                cboxContatoCliente.addItem(o);
            }
            cliente.setCdPessoa(ordemServico.getCliente().getCdPessoa());
            cliente.setDescontoCliente(ordemServico.getCliente().getDescontoCliente());
            cliente.setCpfCnpjPessoa(ordemServico.getCliente().getCpfCnpjPessoa());
            cliente.setNomePessoa(ordemServico.getCliente().getNomePessoa());
            cliente.setTelefoneFixoPessoa(ordemServico.getCliente().getTelefoneFixoPessoa());
            cliente.setEnderecos(ordemServico.getCliente().getEnderecos());
            cliente.setContatos(ordemServico.getCliente().getContatos());
            setCliente();
            lblCodigo.setText(String.valueOf(ordemServico.getCodOS()));
            edtConserto.setText(ordemServico.getConsertoDetalhado());
            if (ordemServico.getDataCadastro() != null) {
                edtDataCad.setText(Format.DateToStr(ordemServico.getDataCadastro()));
            }
            if (ordemServico.getDataEntrada() != null) {
                edtDataEntrada.setText(Format.DateToStr(ordemServico.getDataEntrada()));
            }
            if (ordemServico.getDataSaida() != null) {
                edtDataSaida.setText(Format.DateToStr(ordemServico.getDataSaida()));
            }
            equipamento = ordemServico.getEquipamento();
            setEquipamento();


            edtEstadoEquip.setText(ordemServico.getEstadoEquipamento());
            edtDefeito.setText(ordemServico.getDefeitoReclamado());
            edtNroOCcliente.setText(String.valueOf(ordemServico.getNroOcCliente() != null ? ordemServico.getNroOcCliente() : ""));
            edtParecer.setText(ordemServico.getParecer());
            chkGarantia.setSelected(ordemServico.getSobGarantia());


            listaItensProd.clear();
            listaItensServico.clear();
            listaEnvolvidos.clear();
            listaEquipamentoTestes.clear();
            listaOrcamentos.clear();

            listaItensProd.addAll(ordemServico.getItensProdutos());
            listaItensServico.addAll(ordemServico.getItensServicos());
            listaEnvolvidos.addAll(ordemServico.getEnvolvidos());
            listaEquipamentoTestes.addAll(ordemServico.getTestesEquipamentos());
            listaOrcamentos.addAll(ordemServico.getOrcamentos());
            listaItensPropriedades.clear();
            listaItensPropriedades.addAll(this.ordemServico.getItemsPropriedades());

            if (ordemServico.getOrcamentos().size() > 0) {
                btnImprOrc.setEnabled(true);
                btnAuthOrc.setEnabled(true);
            }
            for (Orcamento o : listaOrcamentos) {
                if (o.getAutorizado()) {
                    btnManutReal.setEnabled(true);
                }
            }
            if (ordemServico.getContatoCliente() != null) {
                cboxContatoCliente.setSelectedItem(ordemServico.getContatoCliente());
            }
            if (ordemServico.getEnderecoCliente() != null) {
                cboxEnderCliente.setSelectedItem(ordemServico.getEnderecoCliente());
            }
            if (ordemServico.getEnderecoTransp() != null) {
                cboxEnderTransp.setSelectedItem(ordemServico.getEnderecoTransp());
            }
            if (ordemServico.getLocalizacao() != null) {
                cboxLocal.setSelectedItem(ordemServico.getLocalizacao());
            }
            if (ordemServico.getMesesGarantia() != null) {
                cboxGarantia.setSelectedItem(String.valueOf(ordemServico.getMesesGarantia()));
            }
            if (ordemServico.getTransportadora() != null) {
                cboxTransp.setSelectedItem(ordemServico.getTransportadora());
            }
            if (ordemServico.getStatusOS() != null) {
                cboxStatus.setSelectedItem(ordemServico.getStatusOS());
                if (ordemServico.getStatusOS().equals("AGUARDANDO PGTO")) {
                    btnFinalPgto.setEnabled(true);
                }
            }
            if (ordemServico.getUltimoOrcamento() != null) {
                if (ordemServico.getUltimoOrcamento().getTipoFrete() != null) {
                    cboxTipoFrete.setSelectedItem(ordemServico.getUltimoOrcamento().getTipoFrete());
                }
                if (ordemServico.getUltimoOrcamento().getDataAutorizacao() != null) {
                    edtDataAuth.setText(Format.DateToStr(ordemServico.getUltimoOrcamento().getDataAutorizacao()));
                }
                if (ordemServico.getUltimoOrcamento().getDataPrevisao() != null) {
                    edtDataPrev.setText(Format.DateToStr(ordemServico.getUltimoOrcamento().getDataPrevisao()));
                }
                chkAplicarDesconto.setSelected(ordemServico.getUltimoOrcamento().getUsoDescontoCliente());
                edtDesconto.setText(Format.FormataValor(Format.doubleToString(ordemServico.getUltimoOrcamento().getValorDesconto()), Variaveis.getDecimal()));
                edtValorFrete.setText(Format.FormataValor(Format.doubleToString(ordemServico.getUltimoOrcamento().getValorFrete()), Variaveis.getDecimal()));
                edtVlrBruto.setText(Format.FormataValor(Format.doubleToString(ordemServico.getUltimoOrcamento().getValorBruto()), Variaveis.getDecimal()));
                edtVlrTotal.setText(Format.FormataValor(Format.doubleToString(ordemServico.getUltimoOrcamento().getValorLiquido()), Variaveis.getDecimal()));
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }

    public void setEquipamento() {
        try {
            if (equipamento.getTipoEquipamento() != null) {
                cboxTipoEquip.setSelectedItem(equipamento.getTipoEquipamento());
            }
            edtDescricaoEquip.setText(equipamento.getDescricao());
            edtMarca.setText(equipamento.getMarca());
            edtModelo.setText(equipamento.getModelo());
            edtNroRastreioInt.setText(String.valueOf(equipamento.getNroRastreioInt() != null ? equipamento.getNroRastreioInt() : ""));
            edtNroRastreioCli.setText(String.valueOf(equipamento.getNroRastreioCli() != null ? equipamento.getNroRastreioCli() : ""));
            edtNroSerie.setText(equipamento.getNroSerie());
            if (equipamento.getDataFabricacao() != null) {
                edtDataFbr.setText(Format.DateToStr(equipamento.getDataFabricacao()));
            }
            imagemConexao.clearImagem();
            for (Imagem i : equipamento.getImagens()) {
                adicionaImagem(i);
            }
            DefaultTableModel dm = (DefaultTableModel) this.tabelaHistEquipEntr.getModel();
            while (dm.getRowCount() > 0) {
                dm.removeRow(0);
            }
            for (OrdemServico o : EquipamentoDao.getHistEntrada(this.equipamento, Variaveis.getEmpresa())) {
                if (o.getCodOS() != this.ordemServico.getCodOS()) {
                    dm.addRow(new Object[]{o.getCodOS(), o.getDataEntrada(), o.getDataSaida(), o.getSobGarantia(), o.getMesesGarantia()});
                }
            }
            //setImagemConexao();
            atualizaImagens();
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }

    public void populaOS() {
        try {
            Integer cdOS = null;
            if (ordemServico.getCodOS() != null) {
                cdOS = ordemServico.getCodOS();
            }
            Integer cdEq = null;
            if (equipamento.getCodEquipamento() != null) {
                cdEq = equipamento.getCodEquipamento();
            }
            Usuario uT = (Usuario) ordemServico.getUsuario().clone();
            ordemServico.clearOrdemServico();
            ordemServico.setUsuario(uT);
            equipamento.clearEquipamento();
            equipamento.setCodEquipamento(cdEq);
            equipamento.setCliente(cliente);
            //equipamento.setConexao(imagemConexao);
            equipamento.getImagens().clear();
            equipamento.getImagens().addAll(listaImagensCurrence);
            if (!Validador.soNumeros(edtDataFbr.getText()).equals("")) {
                equipamento.setDataFabricacao(Format.StrToDate(edtDataFbr.getText()));
            } else {
                equipamento.setDataFabricacao(null);
            }
            equipamento.setDescricao(edtDescricaoEquip.getText());
            equipamento.setMarca(edtMarca.getText().trim());
            equipamento.setModelo(edtModelo.getText().trim());
            if (!Validador.soNumeros(edtNroRastreioInt.getText()).trim().equals("")) {
                equipamento.setNroRastreioInt(Integer.parseInt(Validador.soNumeros(edtNroRastreioInt.getText().trim())));
            }
            if (!Validador.soNumeros(edtNroRastreioCli.getText()).trim().equals("")) {
                equipamento.setNroRastreioCli(Integer.parseInt(Validador.soNumeros(edtNroRastreioCli.getText().trim())));
            }
            equipamento.setNroSerie(edtNroSerie.getText().trim());
            equipamento.setTipoEquipamento((TipoEquipamento) cboxTipoEquip.getSelectedItem());
            ordemServico.setCodOS(cdOS);
            ordemServico.setCliente(cliente);
            ordemServico.setLocalizacao(cboxLocal.getSelectedItem().toString().trim().equals("") ? (new LocalProd()) : (LocalProd) cboxLocal.getSelectedItem());
            ordemServico.setTransportadora(cboxTransp.getSelectedItem().toString().trim().equals("") ? (new Transportadora()) : (Transportadora) cboxTransp.getSelectedItem());
            ordemServico.setContatoCliente(cboxContatoCliente.getSelectedItem().toString().trim().equals("") ? (new Contato()) : (Contato) cboxContatoCliente.getSelectedItem());
            ordemServico.setEnderecoCliente((Endereco) cboxEnderCliente.getSelectedItem());
            ordemServico.setEnderecoTransp(cboxEnderTransp.getSelectedItem().toString().trim().equals("") ? (new Endereco()) : (Endereco) cboxEnderTransp.getSelectedItem());
            ordemServico.setConsertoDetalhado(edtConserto.getText());
            ordemServico.setDefeitoReclamado(edtDefeito.getText());
            ordemServico.setEstadoEquipamento(edtEstadoEquip.getText());
            ordemServico.setEquipamento(equipamento);
            ordemServico.setMesesGarantia(Integer.parseInt(cboxGarantia.getSelectedItem().toString()));
            ordemServico.setSobGarantia(chkGarantia.isSelected());
            ordemServico.setStatusOS(cboxStatus.getSelectedItem().toString().trim());
            ordemServico.setParecer(edtParecer.getText());
            if (!Validador.soNumeros(edtNroOCcliente.getText()).trim().equals("")) {
                ordemServico.setNroOcCliente(Integer.parseInt(Validador.soNumeros(edtNroOCcliente.getText().trim())));
            }
            if (!Validador.soNumeros(edtDataEntrada.getText()).equals("")) {
                ordemServico.setDataEntrada(Format.StrToDate(edtDataEntrada.getText()));
            } else {
                ordemServico.setDataEntrada(null);
            }
            if (!Validador.soNumeros(edtDataCad.getText()).equals("")) {
                ordemServico.setDataCadastro(Format.StrToDate(edtDataCad.getText()));
            } else {
                ordemServico.setDataCadastro(null);
            }
            if (!Validador.soNumeros(edtDataSaida.getText()).equals("")) {
                ordemServico.setDataSaida(Format.StrToDate(edtDataSaida.getText()));
            } else {
                ordemServico.setDataSaida(null);
            }
            ordemServico.getEnvolvidos().clear();
            ordemServico.getItensProdutos().clear();
            ordemServico.getItensServicos().clear();
            ordemServico.getOrcamentos().clear();
            ordemServico.getTestesEquipamentos().clear();
            ordemServico.getEnvolvidos().addAll(listaEnvolvidos);
            ordemServico.getItensProdutos().addAll(listaItensProd);
            ordemServico.getItensServicos().addAll(listaItensServico);
            ordemServico.getOrcamentos().addAll(listaOrcamentos);
            ordemServico.getTestesEquipamentos().addAll(listaEquipamentoTestes);
            ordemServico.getItemsPropriedades().addAll(listaItensPropriedades);
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }

    @SuppressWarnings("empty-statement")
    private void cancelar() {
        if (alterado) {
            if (Extra.Pergunta(this, "Descartar alterações não salvas?")) {
                limpaCampos();
                ativaEdits(false);
                ativaButtons(true, true, false, false, false, false);
                alterado = false;
                ordemServico.clearOrdemServico();
                equipamento.clearEquipamento();
                pnlTabulado.setSelectedComponent(pnlPrincipalOS);
                pnlAbasEquipamento.setSelectedComponent(pnlDEquipamento);
                pnlTabulado.setEnabledAt(1, false);
                pnlTabulado.setEnabledAt(2, false);
                pnlTabulado.setEnabledAt(3, false);
                pnlTabulado.setEnabledAt(4, false);
            }
        } else {
            limpaCampos();
            ativaEdits(false);
            ativaButtons(true, true, false, false, false, false);
            alterado = false;
            ordemServico.clearOrdemServico();
            equipamento.clearEquipamento();
            pnlTabulado.setSelectedComponent(pnlPrincipalOS);
            pnlAbasEquipamento.setSelectedComponent(pnlDEquipamento);
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
            pnlTabulado.setEnabledAt(4, false);
        }
    }

    private void ativaEdits(Boolean ativa) {
        edtCodCliente.setEnabled(ativa);
        btnBuscaCliente.setEnabled(ativa);
        cboxContatoCliente.setEnabled(ativa);
        cboxEnderCliente.setEnabled(ativa);
        pnlAbasEquipamento.setEnabled(ativa);
        edtDataPrev.setEnabled(ativa);
        cboxTipoFrete.setEnabled(ativa);
        chkAplicarDesconto.setEnabled(ativa);
        chkGarantia.setEnabled(ativa);
        btnGerarOrc.setEnabled(ativa);
        edtNroSerie.setEnabled(ativa);
        edtValorFrete.setEnabled(ativa);
        ativaEditsEquip(ativa);
        if (ativa) {
            edtValorFrete.setEnabled(cboxTipoFrete.getSelectedItem().toString().toUpperCase().trim().equals("FOB"));
        }
    }

    private void ativaEditsEquip(Boolean ativa) {
        edtMarca.setEnabled(ativa);
        edtModelo.setEnabled(ativa);
        cboxTipoEquip.setEnabled(ativa);
        edtNroRastreioInt.setEnabled(ativa);
        edtNroRastreioCli.setEnabled(ativa);
        edtDataFbr.setEnabled(ativa);
        btnNovoTipoEquip.setEnabled(ativa);
        edtDescricaoEquip.setEnabled(ativa);
        tabelaPropriedades.setEnabled(ativa);
    }

    private void ativaButtons(Boolean buscar, Boolean novo, Boolean alterar,
            Boolean excluir, Boolean cancelar, Boolean salvar) {
        btnBuscarOS.setEnabled(buscar);
        btnCancOS.setEnabled(cancelar);
        btnExcluirOS.setEnabled(excluir);
        btnEditOS.setEnabled(alterar);
        btnNovaOS.setEnabled(novo);
        btnSaveOS.setEnabled(salvar);
    }

    private void limpaCampos() {
        lblCodigo.setText("0");
        edtCodCliente.setText("");
        edtCpfCliente.setText("");
        edtConserto.setText("");
        edtDataAuth.setText("");
        edtDataCad.setText("");
        edtDataEntrada.setText("");
        edtDataFbr.setText("");
        edtDataPrev.setText("");
        edtDataSaida.setText("");
        edtDefeito.setText("");
        edtDesconto.setText("0,00");
        edtDescrTeste.setText("");
        edtDescricaoEquip.setText("");
        edtEstadoEquip.setText("");
        edtMarca.setText("");
        edtMargemCliente.setText("0,00");
        edtModelo.setText("");
        edtNomeCliente.setText("");
        edtNroOCcliente.setText("");
        edtNroRastreioInt.setText("");
        edtNroRastreioCli.setText("");
        edtNroSerie.setText("");
        edtParecer.setText("");
        edtResultTest.setText("");
        edtTelefoneCliente.setText("");
        edtValorFrete.setText("0,00");
        edtVlrBruto.setText("0,00");
        edtVlrTotal.setText("0,00");
        chkAplicarDesconto.setSelected(false);
        chkGarantia.setSelected(false);
        btnImprOrc.setEnabled(false);
        btnAuthOrc.setEnabled(false);
        btnManutReal.setEnabled(false);
        btnFinalPgto.setEnabled(false);
        cboxContatoCliente.removeAllItems();
        cboxEnderCliente.removeAllItems();
        cboxEnderTransp.removeAllItems();
        tipoEquipAnterior = null;
        if (cboxLocal.getItemCount() > 0) {
            cboxLocal.setSelectedIndex(0);
        }
        if (cboxGarantia.getItemCount() > 0) {
            cboxGarantia.setSelectedIndex(0);
        }
        if (cboxStatus.getItemCount() > 0) {
            cboxStatus.setSelectedIndex(0);
        }
        if (cboxTipoEquip.getItemCount() > 0) {
            cboxTipoEquip.setSelectedIndex(0);
        }
        if (cboxTransp.getItemCount() > 0) {
            cboxTransp.setSelectedIndex(0);
        }
        if (cboxTipoFrete.getItemCount() > 0) {
            cboxTipoFrete.setSelectedIndex(0);
        }
        listaEnvolvidos.clear();
        listaEquipamentoTestes.clear();
        listaItensProd.clear();
        listaItensServico.clear();
        listaOrcamentos.clear();
        listaItensPropriedades.clear();
        limpaImagemConexao();
        limpaProd();
        limpaServico();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        buttonGroup1 = new javax.swing.ButtonGroup();
        listaItensProd = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<ItemProdOS>());
        listaItensServico = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<ItemServicoOS>());
        listaEquipamentoTestes = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<EquipamentoTeste>());
        listaEnvolvidos = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Pessoa>());
        localProd = new beans.LocalProd();
        tipoEquipamento = new beans.TipoEquipamento();
        ordemServico = new beans.OrdemServico();
        imagemConexao = new beans.Imagem();
        menuItensProd = new javax.swing.JPopupMenu();
        mItemProdRemover = new javax.swing.JMenuItem();
        mItemProdAlterar = new javax.swing.JMenuItem();
        menuItensServ = new javax.swing.JPopupMenu();
        mItemServRemover = new javax.swing.JMenuItem();
        mItemServAlterar = new javax.swing.JMenuItem();
        menuAnalize = new javax.swing.JPopupMenu();
        mAnalizeRemover = new javax.swing.JMenuItem();
        menuEnvolvidos = new javax.swing.JPopupMenu();
        mEnvolvidosRemover = new javax.swing.JMenuItem();
        cliente = new beans.Cliente();
        produto = new beans.Produto();
        servico = new beans.Servico();
        listaOrcamentos = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Orcamento>());
        equipamento = new beans.Equipamento();
        jToggleButton1 = new javax.swing.JToggleButton();
        imagemProd = new beans.Imagem();
        transportadora = new beans.Transportadora();
        listaItensPropriedades = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<ItemPropTipoEquip>());
        menuProp = new javax.swing.JPopupMenu();
        mPropRemove = new javax.swing.JMenuItem();
        popupImage = new javax.swing.JPopupMenu();
        menuRemoverImage = new javax.swing.JMenuItem();
        jScrollPane15 = new javax.swing.JScrollPane();
        pnlTabulado = new javax.swing.JTabbedPane();
        pnlPrincipalOS = new javax.swing.JPanel();
        pnlInfoBasicas = new javax.swing.JPanel();
        lblDataCad = new javax.swing.JLabel();
        edtDataCad = new javax.swing.JFormattedTextField();
        pnlCodPessoa = new javax.swing.JPanel();
        lblCodigo = new javax.swing.JLabel();
        pnlDCliente = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        edtCodCliente = new javax.swing.JTextField();
        btnBuscaCliente = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        edtNomeCliente = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        edtCpfCliente = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        edtTelefoneCliente = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        cboxEnderCliente = new javax.swing.JComboBox();
        cboxContatoCliente = new javax.swing.JComboBox();
        jLabel22 = new javax.swing.JLabel();
        pnlAbasEquipamento = new javax.swing.JTabbedPane();
        pnlDEquipamento = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        edtNroSerie = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cboxTipoEquip = new javax.swing.JComboBox();
        btnNovoTipoEquip = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        edtModelo = new javax.swing.JTextField();
        edtMarca = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        edtNroRastreioInt = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        edtDataFbr = new javax.swing.JFormattedTextField();
        jLabel23 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        edtDescricaoEquip = new javax.swing.JTextArea();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        tabelaPropriedades = new javax.swing.JTable();
        jLabel14 = new javax.swing.JLabel();
        edtNroRastreioCli = new javax.swing.JTextField();
        pnlDetalhes = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        cboxLocal = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        btnNovaLocalizacao = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        edtDefeito = new javax.swing.JTextArea();
        jLabel25 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        edtEstadoEquip = new javax.swing.JTextArea();
        jLabel45 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        cboxGarantia = new javax.swing.JComboBox();
        lblDataNasc2 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        edtNroOCcliente = new javax.swing.JTextField();
        edtDataSaida = new javax.swing.JFormattedTextField();
        lblDataNasc3 = new javax.swing.JLabel();
        edtDataEntrada = new javax.swing.JFormattedTextField();
        pnlComplementar = new javax.swing.JPanel();
        iconConexao = new javax.swing.JLabel();
        btnLimparImagem = new javax.swing.JButton();
        btnCarregaImagemBD = new javax.swing.JButton();
        btnCarregaImagemPasta = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane16 = new javax.swing.JScrollPane();
        tabelaImagens = new javax.swing.JTable();
        btnAddImage = new javax.swing.JButton();
        pnlHistEquipEntr = new javax.swing.JPanel();
        jScrollPane13 = new javax.swing.JScrollPane();
        tabelaHistEquipEntr = new javax.swing.JTable();
        cboxStatus = new javax.swing.JComboBox();
        jLabel26 = new javax.swing.JLabel();
        lblDataAuth = new javax.swing.JLabel();
        edtDataAuth = new javax.swing.JFormattedTextField();
        pnlItensProd = new javax.swing.JPanel();
        pnlAjustItensProd = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelaItensProd = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        btnAddProd = new javax.swing.JButton();
        lblPrecoProd = new javax.swing.JLabel();
        iconProd = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        edtDescrProduto = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        btnAddItemProd = new javax.swing.JButton();
        edtQtdeProduto = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        lblSubTotalProd = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        edtPrecoProd = new javax.swing.JTextField();
        chkProdOriginal = new javax.swing.JCheckBox();
        btnAtualizarValoresProd = new javax.swing.JButton();
        btnCancelarItemProd = new javax.swing.JButton();
        pnlItensServ = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelaItensServico = new javax.swing.JTable();
        btnAddServ = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        edtDescServ = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        edtValorBrutoServ = new javax.swing.JTextField();
        btnAddItemServ = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        edtObservServ = new javax.swing.JTextArea();
        jLabel30 = new javax.swing.JLabel();
        lblSubtotalServ = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        edtHoras = new javax.swing.JTextField();
        chkMostraQtdHoras = new javax.swing.JCheckBox();
        jLabel43 = new javax.swing.JLabel();
        edtValorUnitServ = new javax.swing.JTextField();
        btnAtualizarValoresServ = new javax.swing.JButton();
        btnCancelarItemServ = new javax.swing.JButton();
        pnlManutencao = new javax.swing.JPanel();
        pnlEnvolvidos = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tabelaEnvolvidos = new javax.swing.JTable();
        btnAddEnvolvidos = new javax.swing.JButton();
        pnlTabManutencao = new javax.swing.JTabbedPane();
        pnlAnalize = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        edtDescrTeste = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        edtResultTest = new javax.swing.JTextField();
        btnAddTeste = new javax.swing.JButton();
        jScrollPane12 = new javax.swing.JScrollPane();
        tabelaTestes = new javax.swing.JTable();
        pnlConserto = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        edtConserto = new javax.swing.JTextArea();
        jScrollPane8 = new javax.swing.JScrollPane();
        edtParecer = new javax.swing.JTextArea();
        jLabel36 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        pnlFinalizacao = new javax.swing.JPanel();
        pnlOCPendentes = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        tabelaOCPendentes = new javax.swing.JTable();
        btnVerificar = new javax.swing.JButton();
        pnlTabuladoFinalizacao = new javax.swing.JTabbedPane();
        pnlInformacoes = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        edtVlrBruto = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        edtMargemCliente = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        edtDesconto = new javax.swing.JTextField();
        chkAplicarDesconto = new javax.swing.JCheckBox();
        jLabel33 = new javax.swing.JLabel();
        edtVlrTotal = new javax.swing.JTextField();
        btnGerarOrc = new javax.swing.JButton();
        jLabel37 = new javax.swing.JLabel();
        cboxTipoFrete = new javax.swing.JComboBox();
        jLabel38 = new javax.swing.JLabel();
        edtValorFrete = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        edtDataPrev = new javax.swing.JFormattedTextField();
        chkGarantia = new javax.swing.JCheckBox();
        btnRecalcularTotais = new javax.swing.JButton();
        btnImprConserto = new javax.swing.JButton();
        cboxTransp = new javax.swing.JComboBox();
        jLabel47 = new javax.swing.JLabel();
        btnNovaTrasp = new javax.swing.JButton();
        btnImprConserto1 = new javax.swing.JButton();
        jLabel48 = new javax.swing.JLabel();
        cboxEnderTransp = new javax.swing.JComboBox();
        pnlOrcamento = new javax.swing.JPanel();
        btnImprOrc = new javax.swing.JButton();
        btnAuthOrc = new javax.swing.JButton();
        btnFinalPgto = new javax.swing.JButton();
        jScrollPane10 = new javax.swing.JScrollPane();
        tabelaOrcamentos = new javax.swing.JTable();
        btnManutReal = new javax.swing.JButton();
        pnlBotoes = new javax.swing.JPanel();
        btnSair = new javax.swing.JButton();
        btnBuscarOS = new javax.swing.JButton();
        btnNovaOS = new javax.swing.JButton();
        btnEditOS = new javax.swing.JButton();
        btnExcluirOS = new javax.swing.JButton();
        btnCancOS = new javax.swing.JButton();
        btnSaveOS = new javax.swing.JButton();

        mItemProdRemover.setText("Remover Selecionado");
        mItemProdRemover.setToolTipText("");
        mItemProdRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mItemProdRemoverActionPerformed(evt);
            }
        });
        menuItensProd.add(mItemProdRemover);

        mItemProdAlterar.setText("Alterar");
        mItemProdAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mItemProdAlterarActionPerformed(evt);
            }
        });
        menuItensProd.add(mItemProdAlterar);

        mItemServRemover.setText("Remover Selecionado");
        mItemServRemover.setToolTipText("");
        mItemServRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mItemServRemoverActionPerformed(evt);
            }
        });
        menuItensServ.add(mItemServRemover);

        mItemServAlterar.setText("Alterar");
        mItemServAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mItemServAlterarActionPerformed(evt);
            }
        });
        menuItensServ.add(mItemServAlterar);

        mAnalizeRemover.setText("Remover Selecionado");
        mAnalizeRemover.setToolTipText("");
        mAnalizeRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mAnalizeRemoverActionPerformed(evt);
            }
        });
        menuAnalize.add(mAnalizeRemover);

        mEnvolvidosRemover.setText("Remover Selecionado(s)");
        mEnvolvidosRemover.setToolTipText("");
        mEnvolvidosRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mEnvolvidosRemoverActionPerformed(evt);
            }
        });
        menuEnvolvidos.add(mEnvolvidosRemover);

        jToggleButton1.setText("jToggleButton1");

        mPropRemove.setText("Remover Selecionado");
        mPropRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mPropRemoveActionPerformed(evt);
            }
        });
        menuProp.add(mPropRemove);

        menuRemoverImage.setText("Remover Imagem");
        menuRemoverImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverImageActionPerformed(evt);
            }
        });
        popupImage.add(menuRemoverImage);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        pnlInfoBasicas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblDataCad.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lblDataCad.setText("Data Cadastro:");

        try {
            edtDataCad.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        edtDataCad.setEnabled(false);

        pnlCodPessoa.setBorder(javax.swing.BorderFactory.createTitledBorder("Nro. Doc"));
        pnlCodPessoa.setForeground(java.awt.Color.black);

        lblCodigo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblCodigo.setForeground(java.awt.Color.blue);
        lblCodigo.setText("0");

        javax.swing.GroupLayout pnlCodPessoaLayout = new javax.swing.GroupLayout(pnlCodPessoa);
        pnlCodPessoa.setLayout(pnlCodPessoaLayout);
        pnlCodPessoaLayout.setHorizontalGroup(
            pnlCodPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCodPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCodigo)
                .addContainerGap(69, Short.MAX_VALUE))
        );
        pnlCodPessoaLayout.setVerticalGroup(
            pnlCodPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCodPessoaLayout.createSequentialGroup()
                .addComponent(lblCodigo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlDCliente.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados do Cliente"));

        jLabel17.setText("Código:");

        edtCodCliente.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        edtCodCliente.setForeground(java.awt.Color.blue);
        edtCodCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtCodClienteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtCodClienteKeyReleased(evt);
            }
        });

        btnBuscaCliente.setText("...");
        btnBuscaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaClienteActionPerformed(evt);
            }
        });

        jLabel18.setText("Nome:");

        edtNomeCliente.setEnabled(false);

        jLabel19.setText("CPF/CNPJ:");

        edtCpfCliente.setEnabled(false);

        jLabel20.setText("Telefone:");

        edtTelefoneCliente.setEnabled(false);

        jLabel21.setText("Endereço:");

        jLabel22.setText("Contato:");

        javax.swing.GroupLayout pnlDClienteLayout = new javax.swing.GroupLayout(pnlDCliente);
        pnlDCliente.setLayout(pnlDClienteLayout);
        pnlDClienteLayout.setHorizontalGroup(
            pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel21)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlDClienteLayout.createSequentialGroup()
                        .addComponent(edtCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscaCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cboxEnderCliente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel19, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlDClienteLayout.createSequentialGroup()
                        .addComponent(edtCpfCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtTelefoneCliente))
                    .addComponent(cboxContatoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlDClienteLayout.setVerticalGroup(
            pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(edtCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscaCliente)
                    .addComponent(jLabel18)
                    .addComponent(edtNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(edtCpfCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(edtTelefoneCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21)
                        .addComponent(cboxEnderCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel22)
                        .addComponent(cboxContatoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlDEquipamento.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel7.setText("Nro Série:");

        edtNroSerie.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtNroSerieKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtNroSerieKeyReleased(evt);
            }
        });

        jLabel8.setText("Tipo de Equip:");

        cboxTipoEquip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxTipoEquipActionPerformed(evt);
            }
        });

        btnNovoTipoEquip.setText("...");
        btnNovoTipoEquip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoTipoEquipActionPerformed(evt);
            }
        });

        jLabel9.setText("Modelo:");

        jLabel10.setText("Marca:");

        jLabel11.setText("Nro Rast Int:");

        jLabel12.setText("Data Fab:");

        try {
            edtDataFbr.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel23.setText("Descrição:");

        edtDescricaoEquip.setColumns(20);
        edtDescricaoEquip.setLineWrap(true);
        edtDescricaoEquip.setRows(5);
        edtDescricaoEquip.setWrapStyleWord(true);
        edtDescricaoEquip.setDragEnabled(true);
        jScrollPane1.setViewportView(edtDescricaoEquip);

        jLabel13.setText("Propriedades:");

        tabelaPropriedades.setComponentPopupMenu(menuProp);
        tabelaPropriedades.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaItensPropriedades, tabelaPropriedades);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${propTipoEquip}"));
        columnBinding.setColumnName("Descrição");
        columnBinding.setColumnClass(beans.PropriedadeTipoEquip.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valor}"));
        columnBinding.setColumnName("Valor");
        columnBinding.setColumnClass(String.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane14.setViewportView(tabelaPropriedades);

        jLabel14.setText("Nro Rast Cliente:");

        javax.swing.GroupLayout pnlDEquipamentoLayout = new javax.swing.GroupLayout(pnlDEquipamento);
        pnlDEquipamento.setLayout(pnlDEquipamentoLayout);
        pnlDEquipamentoLayout.setHorizontalGroup(
            pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addComponent(jScrollPane14)))
                    .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(edtNroRastreioCli)
                            .addComponent(cboxTipoEquip, 0, 179, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNovoTipoEquip, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtNroRastreioInt)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtDataFbr, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDEquipamentoLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtNroSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(213, Short.MAX_VALUE))
        );
        pnlDEquipamentoLayout.setVerticalGroup(
            pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel12)
                        .addComponent(edtDataFbr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(edtModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9)
                        .addComponent(jLabel10)
                        .addComponent(edtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(edtNroRastreioCli, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14)))
                .addGap(11, 11, 11)
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnNovoTipoEquip)
                        .addComponent(cboxTipoEquip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8))
                    .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(edtNroRastreioInt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11)
                        .addComponent(edtNroSerie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(0, 307, Short.MAX_VALUE))
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE))
                .addContainerGap())
        );

        pnlAbasEquipamento.addTab("Dados do Equipamento", pnlDEquipamento);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel1.setText("Localização:");

        btnNovaLocalizacao.setText("...");
        btnNovaLocalizacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaLocalizacaoActionPerformed(evt);
            }
        });

        jLabel24.setText("Defeito Reclamado:");

        edtDefeito.setColumns(20);
        edtDefeito.setLineWrap(true);
        edtDefeito.setRows(5);
        jScrollPane5.setViewportView(edtDefeito);

        jLabel25.setText("Estado Equipamento:");

        edtEstadoEquip.setColumns(20);
        edtEstadoEquip.setLineWrap(true);
        edtEstadoEquip.setRows(5);
        jScrollPane6.setViewportView(edtEstadoEquip);

        jLabel45.setText("Prazo de Garantia:");

        jLabel34.setText("NF Remessa:");

        cboxGarantia.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "3", "6", "12" }));

        lblDataNasc2.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        lblDataNasc2.setText("Data Entrada:");

        jLabel46.setText("meses");

        try {
            edtDataSaida.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        lblDataNasc3.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        lblDataNasc3.setText("Data Saida:");

        try {
            edtDataEntrada.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout pnlDetalhesLayout = new javax.swing.GroupLayout(pnlDetalhes);
        pnlDetalhes.setLayout(pnlDetalhesLayout);
        pnlDetalhesLayout.setHorizontalGroup(
            pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetalhesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel34, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel45, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblDataNasc3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblDataNasc2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtNroOCcliente, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtDataSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlDetalhesLayout.createSequentialGroup()
                        .addComponent(cboxGarantia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel46)))
                .addGap(158, 158, 158)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(83, 83, 83)
                .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel24)
                    .addComponent(jLabel25)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 530, Short.MAX_VALUE)
                    .addGroup(pnlDetalhesLayout.createSequentialGroup()
                        .addComponent(cboxLocal, 0, 490, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNovaLocalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6))
                .addContainerGap())
        );
        pnlDetalhesLayout.setVerticalGroup(
            pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(pnlDetalhesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDetalhesLayout.createSequentialGroup()
                        .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboxLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNovaLocalizacao)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDetalhesLayout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel25)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)))
                    .addGroup(pnlDetalhesLayout.createSequentialGroup()
                        .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel34)
                            .addComponent(edtNroOCcliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDataNasc2)
                            .addComponent(edtDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDataNasc3)
                            .addComponent(edtDataSaida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDetalhesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboxGarantia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel45)
                            .addComponent(jLabel46))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pnlAbasEquipamento.addTab("Detalhes", pnlDetalhes);

        iconConexao.setBackground(new java.awt.Color(153, 204, 255));
        iconConexao.setForeground(new java.awt.Color(0, 102, 102));
        iconConexao.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        iconConexao.setText("Conexão Equipamento");
        iconConexao.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 51, 51)));

        btnLimparImagem.setText("Limpar");
        btnLimparImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparImagemActionPerformed(evt);
            }
        });

        btnCarregaImagemBD.setText("Imagens BD");
        btnCarregaImagemBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaImagemBDActionPerformed(evt);
            }
        });

        btnCarregaImagemPasta.setText("Imagens Pasta");
        btnCarregaImagemPasta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaImagemPastaActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        tabelaImagens.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tabelaImagens.setCellSelectionEnabled(true);
        tabelaImagens.setComponentPopupMenu(popupImage);
        tabelaImagens.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tabelaImagens.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaImagensMouseClicked(evt);
            }
        });
        tabelaImagens.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaImagensKeyPressed(evt);
            }
        });
        jScrollPane16.setViewportView(tabelaImagens);

        btnAddImage.setText(">>>>");
        btnAddImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddImageActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlComplementarLayout = new javax.swing.GroupLayout(pnlComplementar);
        pnlComplementar.setLayout(pnlComplementarLayout);
        pnlComplementarLayout.setHorizontalGroup(
            pnlComplementarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlComplementarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlComplementarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlComplementarLayout.createSequentialGroup()
                        .addComponent(btnLimparImagem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCarregaImagemBD)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCarregaImagemPasta))
                    .addComponent(iconConexao, javax.swing.GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
                    .addComponent(btnAddImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(28, 28, 28)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 753, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlComplementarLayout.setVerticalGroup(
            pnlComplementarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlComplementarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(iconConexao, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlComplementarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCarregaImagemPasta)
                    .addComponent(btnLimparImagem)
                    .addComponent(btnCarregaImagemBD))
                .addGap(18, 18, 18)
                .addComponent(btnAddImage, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(62, Short.MAX_VALUE))
            .addComponent(jSeparator3)
            .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE)
        );

        pnlAbasEquipamento.addTab("Imagens", pnlComplementar);

        tabelaHistEquipEntr.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro Doc", "Data Entrada", "Data Saída", "Fez uso da Garantia", "Prazo da Garantia (Mês)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaHistEquipEntr.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane13.setViewportView(tabelaHistEquipEntr);
        tabelaHistEquipEntr.getColumnModel().getColumn(0).setHeaderValue("Nro Doc");
        tabelaHistEquipEntr.getColumnModel().getColumn(1).setHeaderValue("Data Entrada");
        tabelaHistEquipEntr.getColumnModel().getColumn(2).setHeaderValue("Data Saída");
        tabelaHistEquipEntr.getColumnModel().getColumn(3).setHeaderValue("Fez uso da Garantia");
        tabelaHistEquipEntr.getColumnModel().getColumn(4).setHeaderValue("Prazo da Garantia (Mês)");

        javax.swing.GroupLayout pnlHistEquipEntrLayout = new javax.swing.GroupLayout(pnlHistEquipEntr);
        pnlHistEquipEntr.setLayout(pnlHistEquipEntrLayout);
        pnlHistEquipEntrLayout.setHorizontalGroup(
            pnlHistEquipEntrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHistEquipEntrLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 1204, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
        pnlHistEquipEntrLayout.setVerticalGroup(
            pnlHistEquipEntrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHistEquipEntrLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 531, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlAbasEquipamento.addTab("Hitórico de Entradas", pnlHistEquipEntr);

        cboxStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ABERTO", "AGUARDANDO OC", "AGUARDANDO AUTH", "EM ANDAMENTO", "AGUARDANDO PGTO", "FINALIZADO" }));
        cboxStatus.setEnabled(false);

        jLabel26.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel26.setText("Status OS:");

        lblDataAuth.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lblDataAuth.setText("Data Autorização:");

        try {
            edtDataAuth.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        edtDataAuth.setEnabled(false);

        javax.swing.GroupLayout pnlInfoBasicasLayout = new javax.swing.GroupLayout(pnlInfoBasicas);
        pnlInfoBasicas.setLayout(pnlInfoBasicasLayout);
        pnlInfoBasicasLayout.setHorizontalGroup(
            pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInfoBasicasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlAbasEquipamento, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 1238, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlInfoBasicasLayout.createSequentialGroup()
                        .addComponent(pnlCodPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboxStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblDataCad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtDataCad, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblDataAuth)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtDataAuth, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnlDCliente, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlInfoBasicasLayout.setVerticalGroup(
            pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                        .addComponent(pnlCodPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInfoBasicasLayout.createSequentialGroup()
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblDataAuth)
                                .addComponent(edtDataAuth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblDataCad)
                                .addComponent(edtDataCad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel26)
                                .addComponent(cboxStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16)))
                .addComponent(pnlDCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlAbasEquipamento)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlPrincipalOSLayout = new javax.swing.GroupLayout(pnlPrincipalOS);
        pnlPrincipalOS.setLayout(pnlPrincipalOSLayout);
        pnlPrincipalOSLayout.setHorizontalGroup(
            pnlPrincipalOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlInfoBasicas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlPrincipalOSLayout.setVerticalGroup(
            pnlPrincipalOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlInfoBasicas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pnlTabulado.addTab("Ordem de Serviço", pnlPrincipalOS);

        tabelaItensProd.setComponentPopupMenu(menuItensProd);
        tabelaItensProd.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaItensProd, tabelaItensProd);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${produto}"));
        columnBinding.setColumnName("Produto");
        columnBinding.setColumnClass(beans.Produto.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${quantidade}"));
        columnBinding.setColumnName("Quantidade");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorUnitario}"));
        columnBinding.setColumnName("Valor Unitario");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorBruto}"));
        columnBinding.setColumnName("Valor Bruto");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${original}"));
        columnBinding.setColumnName("Original");
        columnBinding.setColumnClass(Boolean.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        tabelaItensProd.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tabelaItensProdPropertyChange(evt);
            }
        });
        jScrollPane2.setViewportView(tabelaItensProd);

        jLabel2.setText("Preço Unitario:");

        btnAddProd.setText("...");
        btnAddProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddProdActionPerformed(evt);
            }
        });

        lblPrecoProd.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblPrecoProd.setForeground(java.awt.Color.blue);
        lblPrecoProd.setText("0,00");

        iconProd.setBackground(new java.awt.Color(153, 204, 255));
        iconProd.setForeground(new java.awt.Color(0, 102, 102));
        iconProd.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 51, 51)));

        jLabel27.setText("Produto:");

        edtDescrProduto.setEnabled(false);

        jLabel28.setText("Qtde:");

        btnAddItemProd.setText("Adicionar à Lista");
        btnAddItemProd.setEnabled(false);
        btnAddItemProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddItemProdActionPerformed(evt);
            }
        });

        edtQtdeProduto.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtQtdeProduto.setForeground(new java.awt.Color(0, 0, 255));
        edtQtdeProduto.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtQtdeProduto.setText("0,00");
        edtQtdeProduto.setEnabled(false);
        edtQtdeProduto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtQtdeProdutoKeyReleased(evt);
            }
        });

        jLabel29.setText("Subtotal:");

        lblSubTotalProd.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblSubTotalProd.setForeground(java.awt.Color.blue);
        lblSubTotalProd.setText("0,00");

        jLabel44.setText("Valor Unitário:");

        edtPrecoProd.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtPrecoProd.setForeground(new java.awt.Color(0, 0, 255));
        edtPrecoProd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtPrecoProd.setText("0,00");
        edtPrecoProd.setEnabled(false);
        edtPrecoProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtPrecoProdKeyReleased(evt);
            }
        });

        chkProdOriginal.setSelected(true);
        chkProdOriginal.setText("Produto Original");
        chkProdOriginal.setEnabled(false);

        btnAtualizarValoresProd.setText("Atualizar Valores");
        btnAtualizarValoresProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarValoresProdActionPerformed(evt);
            }
        });

        btnCancelarItemProd.setText("Cancelar");
        btnCancelarItemProd.setEnabled(false);
        btnCancelarItemProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarItemProdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlAjustItensProdLayout = new javax.swing.GroupLayout(pnlAjustItensProd);
        pnlAjustItensProd.setLayout(pnlAjustItensProdLayout);
        pnlAjustItensProdLayout.setHorizontalGroup(
            pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAjustItensProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel44)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtDescrProduto)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAjustItensProdLayout.createSequentialGroup()
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlAjustItensProdLayout.createSequentialGroup()
                                .addComponent(edtPrecoProd, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtQtdeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(chkProdOriginal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnCancelarItemProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnAddItemProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAddProd, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlAjustItensProdLayout.createSequentialGroup()
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSubTotalProd)
                            .addComponent(lblPrecoProd))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAjustItensProdLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 934, Short.MAX_VALUE)
                .addGap(318, 318, 318))
            .addGroup(pnlAjustItensProdLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAtualizarValoresProd, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlAjustItensProdLayout.setVerticalGroup(
            pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAjustItensProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlAjustItensProdLayout.createSequentialGroup()
                        .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(lblPrecoProd))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel29)
                            .addComponent(lblSubTotalProd)))
                    .addGroup(pnlAjustItensProdLayout.createSequentialGroup()
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel27)
                            .addComponent(edtDescrProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddProd))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAddItemProd)
                            .addComponent(jLabel28)
                            .addComponent(edtQtdeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edtPrecoProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel44))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlAjustItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCancelarItemProd)
                            .addComponent(chkProdOriginal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 641, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAtualizarValoresProd))
        );

        javax.swing.GroupLayout pnlItensProdLayout = new javax.swing.GroupLayout(pnlItensProd);
        pnlItensProd.setLayout(pnlItensProdLayout);
        pnlItensProdLayout.setHorizontalGroup(
            pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlAjustItensProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlItensProdLayout.setVerticalGroup(
            pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlAjustItensProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pnlTabulado.addTab("Itens de Produto", pnlItensProd);

        tabelaItensServico.setComponentPopupMenu(menuItensServ);
        tabelaItensServico.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaItensServico, tabelaItensServico);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${servico}"));
        columnBinding.setColumnName("Servico");
        columnBinding.setColumnClass(beans.Servico.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${quantidade}"));
        columnBinding.setColumnName("Qtde Horas");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorUnitario}"));
        columnBinding.setColumnName("Valor Unitario");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorBruto}"));
        columnBinding.setColumnName("Valor Bruto");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${observacoes}"));
        columnBinding.setColumnName("Observacoes");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${mostrarQtde}"));
        columnBinding.setColumnName("Mostrar Qtde");
        columnBinding.setColumnClass(Boolean.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane3.setViewportView(tabelaItensServico);

        btnAddServ.setText("...");
        btnAddServ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddServActionPerformed(evt);
            }
        });

        jLabel3.setText("Descrição do Serviço:");

        edtDescServ.setEnabled(false);

        jLabel5.setText("Valor Bruto:");

        edtValorBrutoServ.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtValorBrutoServ.setForeground(java.awt.Color.blue);
        edtValorBrutoServ.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorBrutoServ.setText("0,00");
        edtValorBrutoServ.setEnabled(false);
        edtValorBrutoServ.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorBrutoServKeyReleased(evt);
            }
        });

        btnAddItemServ.setText("Adicionar à lista");
        btnAddItemServ.setEnabled(false);
        btnAddItemServ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddItemServActionPerformed(evt);
            }
        });

        jLabel6.setText("Observações:");

        edtObservServ.setColumns(20);
        edtObservServ.setRows(5);
        edtObservServ.setEnabled(false);
        jScrollPane4.setViewportView(edtObservServ);

        jLabel30.setText("Subtotal:");

        lblSubtotalServ.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblSubtotalServ.setForeground(java.awt.Color.blue);
        lblSubtotalServ.setText("0,00");

        jLabel40.setText("Horas:");

        edtHoras.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtHoras.setForeground(java.awt.Color.blue);
        edtHoras.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtHoras.setText("0,00");
        edtHoras.setEnabled(false);
        edtHoras.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtHorasKeyReleased(evt);
            }
        });

        chkMostraQtdHoras.setSelected(true);
        chkMostraQtdHoras.setText("Mostrar Qtde Hr");
        chkMostraQtdHoras.setEnabled(false);

        jLabel43.setText("Valor Unitário:");

        edtValorUnitServ.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtValorUnitServ.setForeground(java.awt.Color.blue);
        edtValorUnitServ.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorUnitServ.setText("0,00");
        edtValorUnitServ.setEnabled(false);
        edtValorUnitServ.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorUnitServKeyReleased(evt);
            }
        });

        btnAtualizarValoresServ.setText("Atualizar Valores");
        btnAtualizarValoresServ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarValoresServActionPerformed(evt);
            }
        });

        btnCancelarItemServ.setText("Cancelar");
        btnCancelarItemServ.setEnabled(false);
        btnCancelarItemServ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarItemServActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlItensServLayout = new javax.swing.GroupLayout(pnlItensServ);
        pnlItensServ.setLayout(pnlItensServLayout);
        pnlItensServLayout.setHorizontalGroup(
            pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlItensServLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlItensServLayout.createSequentialGroup()
                        .addGap(167, 167, 167)
                        .addComponent(jScrollPane4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnAtualizarValoresServ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnAddItemServ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnCancelarItemServ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(pnlItensServLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel43)
                    .addComponent(jLabel6))
                .addGap(6, 6, 6)
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlItensServLayout.createSequentialGroup()
                        .addComponent(edtValorUnitServ, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtValorBrutoServ, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlItensServLayout.createSequentialGroup()
                        .addComponent(edtDescServ, javax.swing.GroupLayout.PREFERRED_SIZE, 499, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAddServ, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(edtHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chkMostraQtdHoras)
                .addGap(44, 227, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlItensServLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSubtotalServ)
                .addGap(71, 71, 71))
        );
        pnlItensServLayout.setVerticalGroup(
            pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlItensServLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(edtDescServ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddServ)
                    .addComponent(jLabel40)
                    .addComponent(edtHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkMostraQtdHoras))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtValorBrutoServ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(edtValorUnitServ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43))
                .addGap(16, 16, 16)
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlItensServLayout.createSequentialGroup()
                        .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlItensServLayout.createSequentialGroup()
                                .addComponent(btnAddItemServ)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCancelarItemServ)
                                .addGap(8, 8, 8)
                                .addComponent(btnAtualizarValoresServ))
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 531, Short.MAX_VALUE))
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(lblSubtotalServ))
                .addGap(35, 35, 35))
        );

        pnlTabulado.addTab("Itens de Serviços", pnlItensServ);

        pnlEnvolvidos.setBorder(javax.swing.BorderFactory.createTitledBorder("Envolvidos"));

        tabelaEnvolvidos.setComponentPopupMenu(menuEnvolvidos);
        tabelaEnvolvidos.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaEnvolvidos, tabelaEnvolvidos);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${cdPessoa}"));
        columnBinding.setColumnName("Código");
        columnBinding.setColumnClass(Integer.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nomePessoa}"));
        columnBinding.setColumnName("Nome");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${profissaoPessoa}"));
        columnBinding.setColumnName("Cargo");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorHoraFuncionario}"));
        columnBinding.setColumnName("Valor Hora");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane9.setViewportView(tabelaEnvolvidos);

        btnAddEnvolvidos.setText("Adicionar Envolvido(s)");
        btnAddEnvolvidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddEnvolvidosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlEnvolvidosLayout = new javax.swing.GroupLayout(pnlEnvolvidos);
        pnlEnvolvidos.setLayout(pnlEnvolvidosLayout);
        pnlEnvolvidosLayout.setHorizontalGroup(
            pnlEnvolvidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEnvolvidosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlEnvolvidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9)
                    .addGroup(pnlEnvolvidosLayout.createSequentialGroup()
                        .addComponent(btnAddEnvolvidos)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlEnvolvidosLayout.setVerticalGroup(
            pnlEnvolvidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEnvolvidosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                .addGap(8, 8, 8)
                .addComponent(btnAddEnvolvidos))
        );

        jLabel41.setText("Descrição do Teste:");

        edtDescrTeste.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtDescrTesteKeyReleased(evt);
            }
        });

        jLabel42.setText("Defeito/Resultado:");

        edtResultTest.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtResultTestKeyReleased(evt);
            }
        });

        btnAddTeste.setText("Incluir");
        btnAddTeste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddTesteActionPerformed(evt);
            }
        });

        tabelaTestes.setComponentPopupMenu(menuAnalize);
        tabelaTestes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaEquipamentoTestes, tabelaTestes);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${descricao}"));
        columnBinding.setColumnName("Descricao");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${defeito}"));
        columnBinding.setColumnName("Resultado");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane12.setViewportView(tabelaTestes);

        javax.swing.GroupLayout pnlAnalizeLayout = new javax.swing.GroupLayout(pnlAnalize);
        pnlAnalize.setLayout(pnlAnalizeLayout);
        pnlAnalizeLayout.setHorizontalGroup(
            pnlAnalizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAnalizeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAnalizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlAnalizeLayout.createSequentialGroup()
                        .addComponent(jLabel41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtDescrTeste, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel42)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtResultTest, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAddTeste, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 1208, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlAnalizeLayout.setVerticalGroup(
            pnlAnalizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAnalizeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAnalizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtDescrTeste, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel41)
                    .addComponent(jLabel42)
                    .addComponent(edtResultTest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddTeste))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlTabManutencao.addTab("Análize/Testes", pnlAnalize);

        jLabel35.setText("Conserto Detalhado:");

        edtConserto.setColumns(20);
        edtConserto.setLineWrap(true);
        edtConserto.setRows(5);
        jScrollPane7.setViewportView(edtConserto);

        edtParecer.setColumns(20);
        edtParecer.setLineWrap(true);
        edtParecer.setRows(5);
        jScrollPane8.setViewportView(edtParecer);

        jLabel36.setText("Parecer:");

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout pnlConsertoLayout = new javax.swing.GroupLayout(pnlConserto);
        pnlConserto.setLayout(pnlConsertoLayout);
        pnlConsertoLayout.setHorizontalGroup(
            pnlConsertoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlConsertoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlConsertoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addGap(45, 45, 45)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(pnlConsertoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addContainerGap())
        );
        pnlConsertoLayout.setVerticalGroup(
            pnlConsertoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlConsertoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlConsertoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2)
                    .addGroup(pnlConsertoLayout.createSequentialGroup()
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE))
                    .addGroup(pnlConsertoLayout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane7)))
                .addContainerGap())
        );

        pnlTabManutencao.addTab("Conserto", pnlConserto);

        javax.swing.GroupLayout pnlManutencaoLayout = new javax.swing.GroupLayout(pnlManutencao);
        pnlManutencao.setLayout(pnlManutencaoLayout);
        pnlManutencaoLayout.setHorizontalGroup(
            pnlManutencaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlManutencaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlManutencaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTabManutencao, javax.swing.GroupLayout.DEFAULT_SIZE, 1240, Short.MAX_VALUE)
                    .addComponent(pnlEnvolvidos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlManutencaoLayout.setVerticalGroup(
            pnlManutencaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlManutencaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlTabManutencao, javax.swing.GroupLayout.PREFERRED_SIZE, 363, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlEnvolvidos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlTabulado.addTab("Manutenção", pnlManutencao);

        pnlOCPendentes.setBorder(javax.swing.BorderFactory.createTitledBorder("Ordens de Compra Pendentes"));

        tabelaOCPendentes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane11.setViewportView(tabelaOCPendentes);

        btnVerificar.setText("Verificar OCs");

        javax.swing.GroupLayout pnlOCPendentesLayout = new javax.swing.GroupLayout(pnlOCPendentes);
        pnlOCPendentes.setLayout(pnlOCPendentesLayout);
        pnlOCPendentesLayout.setHorizontalGroup(
            pnlOCPendentesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOCPendentesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOCPendentesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11)
                    .addGroup(pnlOCPendentesLayout.createSequentialGroup()
                        .addComponent(btnVerificar)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlOCPendentesLayout.setVerticalGroup(
            pnlOCPendentesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOCPendentesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnVerificar)
                .addGap(6, 6, 6))
        );

        pnlInformacoes.setBorder(null);

        jLabel4.setText("Valor Bruto:");

        edtVlrBruto.setForeground(java.awt.Color.blue);
        edtVlrBruto.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtVlrBruto.setText("0,00");
        edtVlrBruto.setEnabled(false);
        edtVlrBruto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtVlrBrutoKeyReleased(evt);
            }
        });

        jLabel31.setText("Margem do Cliente:");

        edtMargemCliente.setForeground(java.awt.Color.blue);
        edtMargemCliente.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtMargemCliente.setText("0,00");
        edtMargemCliente.setEnabled(false);
        edtMargemCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtMargemClienteKeyReleased(evt);
            }
        });

        jLabel32.setText("Desconto:");

        edtDesconto.setForeground(java.awt.Color.blue);
        edtDesconto.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtDesconto.setText("0,00");
        edtDesconto.setEnabled(false);
        edtDesconto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtDescontoKeyReleased(evt);
            }
        });

        chkAplicarDesconto.setText("Desconto do cliente");
        chkAplicarDesconto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkAplicarDescontoItemStateChanged(evt);
            }
        });

        jLabel33.setText("Valor Total:");

        edtVlrTotal.setForeground(java.awt.Color.blue);
        edtVlrTotal.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtVlrTotal.setText("0,00");
        edtVlrTotal.setEnabled(false);
        edtVlrTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtVlrTotalKeyReleased(evt);
            }
        });

        btnGerarOrc.setText("Gerar Orçamento");
        btnGerarOrc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGerarOrcActionPerformed(evt);
            }
        });

        jLabel37.setText("Tipo de Frete:");

        cboxTipoFrete.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FOB", "CIF" }));
        cboxTipoFrete.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboxTipoFreteItemStateChanged(evt);
            }
        });

        jLabel38.setText("Valor Frete:");

        edtValorFrete.setForeground(java.awt.Color.blue);
        edtValorFrete.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorFrete.setText("0,00");
        edtValorFrete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorFreteKeyReleased(evt);
            }
        });

        jLabel39.setText("Previsão de Conclusão:");

        try {
            edtDataPrev.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        chkGarantia.setText("Uso da Garantia");
        chkGarantia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkGarantiaActionPerformed(evt);
            }
        });

        btnRecalcularTotais.setText("Recalcular Totais");
        btnRecalcularTotais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecalcularTotaisActionPerformed(evt);
            }
        });

        btnImprConserto.setText("Ficha de Conserto");
        btnImprConserto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprConsertoActionPerformed(evt);
            }
        });

        cboxTransp.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboxTranspItemStateChanged(evt);
            }
        });
        cboxTransp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxTranspActionPerformed(evt);
            }
        });

        jLabel47.setText("Transportadora:");

        btnNovaTrasp.setText("...");
        btnNovaTrasp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaTraspActionPerformed(evt);
            }
        });

        btnImprConserto1.setText("Ficha para Cliente");
        btnImprConserto1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprConserto1ActionPerformed(evt);
            }
        });

        jLabel48.setText("Endereço Transp:");

        javax.swing.GroupLayout pnlInformacoesLayout = new javax.swing.GroupLayout(pnlInformacoes);
        pnlInformacoes.setLayout(pnlInformacoesLayout);
        pnlInformacoesLayout.setHorizontalGroup(
            pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInformacoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel48, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel47, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel37, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInformacoesLayout.createSequentialGroup()
                        .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlInformacoesLayout.createSequentialGroup()
                                .addComponent(edtVlrBruto, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel33))
                            .addGroup(pnlInformacoesLayout.createSequentialGroup()
                                .addComponent(edtMargemCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addComponent(jLabel32))
                            .addGroup(pnlInformacoesLayout.createSequentialGroup()
                                .addComponent(cboxTipoFrete, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel38)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlInformacoesLayout.createSequentialGroup()
                                .addComponent(edtDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 438, Short.MAX_VALUE)
                                .addComponent(jLabel39)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtDataPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlInformacoesLayout.createSequentialGroup()
                                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(edtVlrTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(edtValorFrete, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 736, Short.MAX_VALUE))))
                    .addGroup(pnlInformacoesLayout.createSequentialGroup()
                        .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cboxEnderTransp, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(pnlInformacoesLayout.createSequentialGroup()
                                    .addComponent(cboxTransp, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnNovaTrasp, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(pnlInformacoesLayout.createSequentialGroup()
                                    .addComponent(chkAplicarDesconto)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(chkGarantia))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(pnlInformacoesLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(btnRecalcularTotais)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGerarOrc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnImprConserto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnImprConserto1)
                .addContainerGap(656, Short.MAX_VALUE))
        );

        pnlInformacoesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnGerarOrc, btnImprConserto, btnRecalcularTotais});

        pnlInformacoesLayout.setVerticalGroup(
            pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInformacoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(edtMargemCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32)
                    .addComponent(edtDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39)
                    .addComponent(edtDataPrev, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtVlrTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(edtVlrBruto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboxTipoFrete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37)
                    .addComponent(edtValorFrete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNovaTrasp)
                    .addComponent(cboxTransp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel47))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel48)
                    .addComponent(cboxEnderTransp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkAplicarDesconto)
                    .addComponent(chkGarantia))
                .addGap(62, 62, 62)
                .addGroup(pnlInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGerarOrc)
                    .addComponent(btnRecalcularTotais)
                    .addComponent(btnImprConserto)
                    .addComponent(btnImprConserto1))
                .addContainerGap())
        );

        pnlTabuladoFinalizacao.addTab("Finalização", pnlInformacoes);

        pnlOrcamento.setBorder(null);

        btnImprOrc.setText("Imprimir último orçamento");
        btnImprOrc.setEnabled(false);
        btnImprOrc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprOrcActionPerformed(evt);
            }
        });

        btnAuthOrc.setText("Autorizar último orçamento");
        btnAuthOrc.setEnabled(false);
        btnAuthOrc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAuthOrcActionPerformed(evt);
            }
        });

        btnFinalPgto.setText("Finalizar Pagamento");
        btnFinalPgto.setEnabled(false);
        btnFinalPgto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalPgtoActionPerformed(evt);
            }
        });

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaOrcamentos, tabelaOrcamentos);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nroRevisao}"));
        columnBinding.setColumnName("Nro Revisão");
        columnBinding.setColumnClass(Integer.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${dataCadastro}"));
        columnBinding.setColumnName("Cadastro");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorBruto}"));
        columnBinding.setColumnName("Valor Bruto");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorDesconto}"));
        columnBinding.setColumnName("Desconto");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorLiquido}"));
        columnBinding.setColumnName("Valor Final");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${valorFrete}"));
        columnBinding.setColumnName("Valor Frete");
        columnBinding.setColumnClass(Double.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${tipoFrete}"));
        columnBinding.setColumnName("Frete");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${dataPrevisao}"));
        columnBinding.setColumnName("Previsão");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${diasValidade}"));
        columnBinding.setColumnName("Dias Validade");
        columnBinding.setColumnClass(Integer.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${dataAutorizacao}"));
        columnBinding.setColumnName("Data Autorização");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${autorizado}"));
        columnBinding.setColumnName("Autorizado");
        columnBinding.setColumnClass(Boolean.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane10.setViewportView(tabelaOrcamentos);

        btnManutReal.setText("Manutenção Realizada");
        btnManutReal.setEnabled(false);
        btnManutReal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManutRealActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlOrcamentoLayout = new javax.swing.GroupLayout(pnlOrcamento);
        pnlOrcamento.setLayout(pnlOrcamentoLayout);
        pnlOrcamentoLayout.setHorizontalGroup(
            pnlOrcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrcamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOrcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlOrcamentoLayout.createSequentialGroup()
                        .addComponent(btnImprOrc)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAuthOrc)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnManutReal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 433, Short.MAX_VALUE)
                        .addComponent(btnFinalPgto))
                    .addComponent(jScrollPane10))
                .addContainerGap())
        );

        pnlOrcamentoLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAuthOrc, btnImprOrc, btnManutReal});

        pnlOrcamentoLayout.setVerticalGroup(
            pnlOrcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlOrcamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnImprOrc)
                    .addComponent(btnAuthOrc)
                    .addComponent(btnFinalPgto)
                    .addComponent(btnManutReal))
                .addGap(6, 6, 6))
        );

        pnlTabuladoFinalizacao.addTab("Orçamentos", pnlOrcamento);

        javax.swing.GroupLayout pnlFinalizacaoLayout = new javax.swing.GroupLayout(pnlFinalizacao);
        pnlFinalizacao.setLayout(pnlFinalizacaoLayout);
        pnlFinalizacaoLayout.setHorizontalGroup(
            pnlFinalizacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFinalizacaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFinalizacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlTabuladoFinalizacao, javax.swing.GroupLayout.DEFAULT_SIZE, 1240, Short.MAX_VALUE)
                    .addComponent(pnlOCPendentes, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlFinalizacaoLayout.setVerticalGroup(
            pnlFinalizacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFinalizacaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlTabuladoFinalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlOCPendentes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlTabulado.addTab("Finalização", pnlFinalizacao);

        jScrollPane15.setViewportView(pnlTabulado);
        pnlTabulado.getAccessibleContext().setAccessibleName("Ordem de Serviço");

        pnlBotoes.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnBuscarOS.setText("Buscar");
        btnBuscarOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarOSActionPerformed(evt);
            }
        });

        btnNovaOS.setText("Novo");
        btnNovaOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaOSActionPerformed(evt);
            }
        });

        btnEditOS.setText("Alterar");
        btnEditOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditOSActionPerformed(evt);
            }
        });

        btnExcluirOS.setText("Excluir");
        btnExcluirOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirOSActionPerformed(evt);
            }
        });

        btnCancOS.setText("Cancelar");
        btnCancOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancOSActionPerformed(evt);
            }
        });

        btnSaveOS.setText("Confirmar");
        btnSaveOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveOSActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBotoesLayout = new javax.swing.GroupLayout(pnlBotoes);
        pnlBotoes.setLayout(pnlBotoesLayout);
        pnlBotoesLayout.setHorizontalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBuscarOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNovaOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExcluirOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSaveOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlBotoesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBuscarOS, btnCancOS, btnEditOS, btnExcluirOS, btnNovaOS, btnSair, btnSaveOS});

        pnlBotoesLayout.setVerticalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addGroup(pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarOS)
                    .addComponent(btnNovaOS)
                    .addComponent(btnEditOS)
                    .addComponent(btnExcluirOS)
                    .addComponent(btnCancOS)
                    .addComponent(btnSaveOS)
                    .addComponent(btnSair))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane15, javax.swing.GroupLayout.DEFAULT_SIZE, 1287, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane15, javax.swing.GroupLayout.DEFAULT_SIZE, 826, Short.MAX_VALUE)
                .addGap(2, 2, 2)
                .addComponent(pnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        if (alterado) {
            if (!Extra.Pergunta(this, "Sair sem salvar?")) {
                return;
            }
        }
        dispose();
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        try {
            setClosed(true);
        } catch (PropertyVetoException e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
	}//GEN-LAST:event_btnSairActionPerformed

    private void btnCancOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancOSActionPerformed
        cancelar();
    }//GEN-LAST:event_btnCancOSActionPerformed

    public void setAtivoButtonBuscar(boolean ativar) {
        this.btnBuscarOS.setEnabled(ativar);
        this.btnBuscarOS.setVisible(ativar);
    }

    public void executaButtonEditar() {
        btnEditOSActionPerformed(null);
    }

    private void btnNovaOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaOSActionPerformed
        cancelar();
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true);
        alterado = true;
        //ordemServico.clearOrdemServico();
        ordemServico.setUsuario(usuario);
        lblCodigo.setText(String.valueOf(NextDaos.nextGenerico(Variaveis.getEmpresa(), "os_cab")));
        edtCodCliente.requestFocus();
    }//GEN-LAST:event_btnNovaOSActionPerformed

    private void btnBuscarOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarOSActionPerformed
        FI_CadFindOS frmOS = (FI_CadFindOS) Util.execucaoDiretaClass("formularios.FI_CadFindOS", "Modulos-jSyscom.jar",
                "Localiza Ordem de Serviço - " + Util.getSource(seqInstancias, FI_CadFindOS.class),
                new OpcoesTela(true, true, true, true, true, false), usuario);
        if (frmOS != null) {
            frmOS.setAtivoButtonNovo(false);
            frmOS.ordemServico = ordemServico;
        }
    }//GEN-LAST:event_btnBuscarOSActionPerformed

    private void btnEditOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditOSActionPerformed
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true);
        alterado = true;
        pnlTabulado.setEnabledAt(1, true);
        pnlTabulado.setEnabledAt(2, true);
        pnlTabulado.setEnabledAt(3, true);
        pnlTabulado.setEnabledAt(4, true);
    }//GEN-LAST:event_btnEditOSActionPerformed

    private Boolean salvarOS() {
        if (cliente.getCdPessoa() == null) {
            Extra.Informacao(this, "Cliente é obrigatório!");
            return false;
        }
        if (cboxTipoEquip.getSelectedItem().toString().trim().equals("")) {
            Extra.Informacao(this, "Tipo de Equipamento é obrigatório!");
            return false;
        }
        if (cboxEnderCliente.getSelectedItem().toString().trim().equals("")) {
            Extra.Informacao(this, "Endereço do Cliente é obrigatório!");
            return false;
        }
        equipamento.setCliente(cliente);
        equipamento.setNroSerie(edtNroSerie.getText().toUpperCase().trim());
        if (!EquipamentoDao.alreadyExistsEquip(equipamento, Variaveis.getEmpresa())) {
            equipamento.setCodEquipamento(null);
        }

        try {
            populaOS();
            if (OrdemServicoDao.salvarOS(ordemServico, Variaveis.getEmpresa())) {
                Imagem i = (Imagem) imagemConexao.clone();
                setOS();
                imagemConexao = i;
                setImagemConexao();
                return true;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
        return false;
    }

    private void salvarOrdemServico() {
        if (salvarOS()) {
            ativaEdits(false);
            ativaButtons(true, true, true, true, true, false);
            alterado = false;
            pnlTabulado.setSelectedComponent(pnlPrincipalOS);
            pnlAbasEquipamento.setSelectedComponent(pnlDEquipamento);
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
            pnlTabulado.setEnabledAt(4, false);
            Extra.Informacao(this, "Salvo com sucesso!");
        } else {
            Extra.Informacao(this, "Falha ao salvar Ordem de Serviço!");
        }
    }

    private void btnSaveOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveOSActionPerformed
        salvarOrdemServico();
    }//GEN-LAST:event_btnSaveOSActionPerformed

    private void btnExcluirOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirOSActionPerformed
        //if (usuario.getCodigoUsuario().equals(ordemServico.getUsuario().getCodigoUsuario())) {
        if (ordemServico.getCodOS() != null) {
            if (Extra.Pergunta(this, "Excluir registro atual?")) {
                if (OrdemServicoDao.excluirOS(ordemServico, Variaveis.getEmpresa())) {
                    alterado = false;
                    cancelar();
                    Extra.Informacao(this, "Registro apagado com sucesso!");
                } else {
                    Extra.Informacao(this, "Falha ao apagar registro!");
                }
            }
        } else {
            alterado = false;
            cancelar();
        }
        //} else {
        //    Extra.Informacao(this, "Este usuário não tem permissão para excluir esta OS!");
        //}
    }//GEN-LAST:event_btnExcluirOSActionPerformed

    private void limpaProd() {
        produto = new Produto();
        produto.addObserver(this);
        edtDescrProduto.setText("");
        edtPrecoProd.setText("0,00");
        edtQtdeProduto.setText("0,00");
        chkProdOriginal.setSelected(true);
        lblPrecoProd.setText("0,00");
        lblSubTotalProd.setText("0,00");
        btnAddItemProd.setEnabled(false);
        btnCancelarItemProd.setEnabled(false);
        edtQtdeProduto.setEnabled(false);
        chkProdOriginal.setEnabled(false);
        tabelaItensProd.setEnabled(true);
        limpaImagemProd();
    }

    private void limpaServico() {
        servico = new Servico();
        servico.addObserver(this);
        edtValorBrutoServ.setText("0,00");
        edtValorUnitServ.setText("0,00");
        lblSubtotalServ.setText("0,00");
        edtHoras.setText("0,00");
        edtObservServ.setText("");
        edtDescServ.setText("");
        chkMostraQtdHoras.setSelected(true);
        btnAddItemServ.setEnabled(false);
        btnCancelarItemServ.setEnabled(false);
        edtHoras.setEnabled(false);
        chkMostraQtdHoras.setEnabled(false);
        edtValorBrutoServ.setEnabled(false);
        edtObservServ.setEnabled(false);
        tabelaItensServico.setEnabled(true);
    }

    private void limpaImagemProd() {
        iconProd.setIcon(null);
    }

    private void limpaImagemConexao() {
        iconConexao.setIcon(null);
        iconConexao.setText("Conexão Equipamento");
        nomeArq = null;
        if (fc != null) {
            fc.setSelectedFile(null);
        }
        if (imagemConexao != null) {
            imagemConexao.clearImagem();
        }
        atualizaImagens();
    }

    private void recalculaTotais() {
        try {
            //recalcula os totais aqui, e ativa a obrigacao de gerar outro orçamento
            Double valorItensProd = 0.00;
            Double valorItensServ = 0.00;
            Double valorFrete = Format.stringToDouble(edtValorFrete.getText().trim());
            for (ItemProdOS o : listaItensProd) {
                valorItensProd += o.getValorBruto();
            }
            for (ItemServicoOS o : listaItensServico) {
                valorItensServ += o.getValorBruto();
            }
            Double valorBrutoTotal = valorItensProd + valorItensServ;
            Double percDescontoCliente = Format.stringToDouble(edtMargemCliente.getText().trim());
            Double totalDesconto = 0.00;
            if (chkAplicarDesconto.isSelected()) {
                totalDesconto = valorBrutoTotal * (percDescontoCliente / 100);
            }
            lblSubTotalProd.setText(Format.FormataValor(Format.doubleToString(valorItensProd), Variaveis.getDecimal()));
            lblSubtotalServ.setText(Format.FormataValor(Format.doubleToString(valorItensServ), Variaveis.getDecimal()));
            edtVlrBruto.setText(Format.FormataValor(Format.doubleToString(valorBrutoTotal + valorFrete), Variaveis.getDecimal()));
            edtDesconto.setText(Format.FormataValor(Format.doubleToString(totalDesconto), Variaveis.getDecimal()));
            edtVlrTotal.setText(Format.FormataValor(Format.doubleToString((valorBrutoTotal - totalDesconto) + valorFrete), Variaveis.getDecimal()));
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }

    private void btnNovaLocalizacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaLocalizacaoActionPerformed
        FI_CadLocalizacao o = (FI_CadLocalizacao) Util.execucaoDiretaClass("formularios.FI_CadLocalizacao", "Modulos-jSyscom.jar",
                "Cadastro de Locais/Gôndolas - " + Util.getSource(seqInstancias, FI_CadLocalizacao.class),
                new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.localProd = localProd;
        }
    }//GEN-LAST:event_btnNovaLocalizacaoActionPerformed

    private void btnNovoTipoEquipActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoTipoEquipActionPerformed
        FI_CadTipoEquip o = (FI_CadTipoEquip) Util.execucaoDiretaClass("formularios.FI_CadTipoEquip", "Modulos-jSyscom.jar", "Cadastro de Tipos de Equipamento - " + Util.getSource(seqInstancias, FI_CadTipoEquip.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.tipoEquipamento = tipoEquipamento;
        }
    }//GEN-LAST:event_btnNovoTipoEquipActionPerformed

    private void btnLimparImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparImagemActionPerformed
        limpaImagemConexao();
    }//GEN-LAST:event_btnLimparImagemActionPerformed

    private void btnCarregaImagemBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaImagemBDActionPerformed
        FI_SysGaleryImages galery = (FI_SysGaleryImages) Util.execucaoDiretaClass("formularios.FI_SysGaleryImages", "Modulos-jSyscom.jar", "Localiza Imagens - " + Util.getSource(seqInstancias, FI_SysGaleryImages.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (galery != null) {
            galery.setWidthMaxExport(0);
            galery.setSqlDefault("select cd_conexao_equip as cod, blob_conexao_equip as blob, md5_conexao_equip as md5, nome_conexao_equip as nome from conexao_equip where cd_empresa = " + Variaveis.getEmpresa().getCodigo());
            galery.exportImagem = imagemConexao;
        }
    }//GEN-LAST:event_btnCarregaImagemBDActionPerformed

    private void btnCarregaImagemPastaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaImagemPastaActionPerformed
        if (fc == null) {
            fc = new JFileChooser();
            fc.addChoosableFileFilter(new ImageFilter());
            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileView(new ImageFileView());
            fc.setAccessory(new ImagePreview(fc));
        }
        int returnVal = fc.showDialog(this, "Abrir");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                imagemConexao.clearImagem();
                imagemConexao.setBlobImagem((ImageIcon) Util.setImagen(fc.getSelectedFile().toString(), iconProd.getWidth(), -1));
                imagemConexao.setMd5Imagem(Arquivo.getMD5Checksum(fc.getSelectedFile().toString()));
                imagemConexao.setNomeImagem(fc.getSelectedFile().getName());
                nomeArq = fc.getSelectedFile().toString();
                Integer a = OrdemServicoDao.imagemConexaoInDB(Variaveis.getEmpresa(), imagemConexao);
                if (a != null) {
                    if (Extra.Pergunta(this, "Esta imagem parece já existir no Banco de Dados, gostaria de usar a que esta no banco?")) {
                        nomeArq = null;
                        Imagem i = OrdemServicoDao.getImagemConexao(Variaveis.getEmpresa(), new Imagem(a));
                        imagemConexao.clearImagem();
                        imagemConexao.setCodImagem(i.getCodImagem());
                        imagemConexao.setBlobImagem(i.getBlobImagem());
                        imagemConexao.setMd5Imagem(i.getMd5Imagem());
                        imagemConexao.setNomeImagem(i.getNomeImagem());
                    }
                }
                imagemConexao.setFlagPronto(true);
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        }
    }//GEN-LAST:event_btnCarregaImagemPastaActionPerformed

    private void edtQtdeProdutoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtQtdeProdutoKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtQtdeProdutoKeyReleased

    private void edtValorFreteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorFreteKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            recalculaTotais();
        }
    }//GEN-LAST:event_edtValorFreteKeyReleased

    private void edtVlrBrutoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtVlrBrutoKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtVlrBrutoKeyReleased

    private void edtMargemClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtMargemClienteKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtMargemClienteKeyReleased

    private void edtDescontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtDescontoKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtDescontoKeyReleased

    private void edtVlrTotalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtVlrTotalKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtVlrTotalKeyReleased

    private void edtValorBrutoServKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorBrutoServKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorBrutoServKeyReleased

    private void edtHorasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtHorasKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                if (Format.stringToDouble(edtHoras.getText().trim()) > 0) {
                    edtValorBrutoServ.setText(Format.FormataValor(Format.doubleToString((servico.getValorServico() * Format.stringToDouble(edtHoras.getText().trim()))), Variaveis.getDecimal()));
                }
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        } else {
            try {
                Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        }
    }//GEN-LAST:event_edtHorasKeyReleased

    private void edtValorUnitServKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorUnitServKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorUnitServKeyReleased

    private void edtPrecoProdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtPrecoProdKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtPrecoProdKeyReleased

    private void btnGerarOrcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGerarOrcActionPerformed
        if (Extra.Pergunta(this, "Para gerar o orçamento, é nessessário salvar a ordem de serviço, deseja continuar?")) {
            if (!Validador.soNumeros(edtDataPrev.getText()).equals("")) {
                Boolean auth = false;
                for (Orcamento o : listaOrcamentos) {
                    if (o.getAutorizado()) {
                        auth = true;
                    }
                }
                if (auth) {
                    if (!Extra.Pergunta(this, "Já existe uma revisão de orçamento autorizada pelo cliente!\n"
                            + "Gostaria de cancelar a revisão anterior para gerar um novo orçamento?")) {
                        return;
                    } else {
                        for (Orcamento o : listaOrcamentos) {
                            o.setAutorizado(false);
                        }
                    }
                }
                String dias = JOptionPane.showInputDialog("Dias de validade do orçamento:");
                try {
                    Integer rdias = Integer.parseInt(Validador.soNumeros(dias));
                    Orcamento o = new Orcamento(ordemServico);
                    recalculaTotais();
                    o.setDiasValidade(rdias);
                    o.setAutorizado(false);
                    o.setDataPrevisao(Format.StrToDate(edtDataPrev.getText()));
                    o.setTipoFrete(cboxTipoFrete.getSelectedItem().toString());
                    o.setUsoDescontoCliente(chkAplicarDesconto.isSelected());
                    o.setValorBruto(Format.stringToDouble(edtVlrBruto.getText().trim()));
                    o.setValorDesconto(Format.stringToDouble(edtDesconto.getText().trim()));
                    o.setValorLiquido(Format.stringToDouble(edtVlrTotal.getText().trim()));
                    o.setValorFrete(Format.stringToDouble(edtValorFrete.getText().trim()));
                    o.setOrdemServico(ordemServico);
                    listaOrcamentos.add(o);
                    ordemServico.getOrcamentos().clear();
                    ordemServico.getOrcamentos().addAll(listaOrcamentos);
                    ordemServico.setUltimoOrcamento(o);
                    cboxStatus.setSelectedItem("AGUARDANDO AUTH");
                    btnImprOrc.setEnabled(true);
                    btnAuthOrc.setEnabled(true);
                    salvarOrdemServico();
                } catch (Exception e) {
                    Extra.Informacao(this, "Nro de dias inválido!");
                }

            } else {
                Extra.Informacao(this, "Data de previsão inválida!");
            }
        }
    }//GEN-LAST:event_btnGerarOrcActionPerformed

    private void chkGarantiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkGarantiaActionPerformed
    }//GEN-LAST:event_chkGarantiaActionPerformed

    private void cboxTipoFreteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboxTipoFreteItemStateChanged
        if (cboxTipoFrete.getSelectedItem().toString().toUpperCase().trim().equals("FOB")) {
            edtValorFrete.setEnabled(true);
        } else {
            edtValorFrete.setText("0,00");
            edtValorFrete.setEnabled(false);
        }
    }//GEN-LAST:event_cboxTipoFreteItemStateChanged

    private void btnBuscaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaClienteActionPerformed
        FI_CadFindPessoa frmFC = (FI_CadFindPessoa) Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", "Localiza Cliente - " + Util.getSource(seqInstancias, FI_CadFindPessoa.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (frmFC != null) {
            frmFC.setTipoObrigatorioBusca(cliente);
            frmFC.pessoa = cliente;
        }
    }//GEN-LAST:event_btnBuscaClienteActionPerformed

    private void edtCodClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtCodClienteKeyReleased
    }//GEN-LAST:event_edtCodClienteKeyReleased

    private void edtCodClienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtCodClienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!PessoaDao.buscaCliente(Integer.parseInt(Validador.soNumeros(edtCodCliente.getText().trim())), Variaveis.getEmpresa()).getNomePessoa().trim().equals("")) {
                cliente.setCdPessoa(Integer.parseInt(Validador.soNumeros(edtCodCliente.getText().trim())));
                PessoaDao.buscaCliente(cliente, Variaveis.getEmpresa());
            } else {
                Extra.Informacao(this, "Cliente não encontrado!");
            }
        }
    }//GEN-LAST:event_edtCodClienteKeyPressed

    private void btnAddProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddProdActionPerformed
        edtQtdeProduto.requestFocus();
        FI_CadFindProduto o = (FI_CadFindProduto) Util.execucaoDiretaClass("formularios.FI_CadFindProduto", "Modulos-jSyscom.jar", "Localiza Produtos - " + Util.getSource(seqInstancias, FI_CadFindProduto.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.produto = produto;
        }
    }//GEN-LAST:event_btnAddProdActionPerformed

    private void btnAddServActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddServActionPerformed
        edtHoras.requestFocus();
        FI_CadServico o = (FI_CadServico) Util.execucaoDiretaClass("formularios.FI_CadServico", "Modulos-jSyscom.jar", "Localiza Serviços - " + Util.getSource(seqInstancias, FI_CadServico.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.servico = servico;
        }
    }//GEN-LAST:event_btnAddServActionPerformed

    private void btnAddEnvolvidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddEnvolvidosActionPerformed
        FI_CadFindPessoa o = (FI_CadFindPessoa) Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", "Localiza Funcionarios - " + Util.getSource(seqInstancias, FI_CadFindPessoa.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.setTipoObrigatorioBusca(new Funcionario());
            o.listaExport = listaEnvolvidos;
        }
    }//GEN-LAST:event_btnAddEnvolvidosActionPerformed

    private void btnAddItemProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddItemProdActionPerformed
        try {
            if (produto.getCodProduto() != null) {
                ItemProdOS item = new ItemProdOS();
                if (!tabelaItensProd.isEnabled()) {
                    item = listaItensProd.get(tabelaItensProd.getSelectedRow());
                    listaItensProd.remove(item);
                }
                item.setOrdemServico(ordemServico);
                item.setOriginal(chkProdOriginal.isSelected());
                item.setProduto(produto);
                item.setQuantidade(Format.stringToDouble(edtQtdeProduto.getText().trim()));
                item.setValorUnitario(produto.getValorVendaProduto());
                item.setValorBruto(produto.getValorVendaProduto() * Format.stringToDouble(edtQtdeProduto.getText().trim()));
                for (ItemProdOS i : listaItensProd) {
                    if (item.equals(i)) {
                        item.setQuantidade(item.getQuantidade() + i.getQuantidade());
                        item.setValorBruto(item.getValorBruto() + i.getValorBruto());
                        listaItensProd.remove(i);
                        break;
                    }
                }
                listaItensProd.add(item);
                limpaProd();
                recalculaTotais();
            } else {
                Extra.Informacao(this, "Nenhum produto selecionado!");
            }
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_btnAddItemProdActionPerformed

    private void tabelaItensProdPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tabelaItensProdPropertyChange
    }//GEN-LAST:event_tabelaItensProdPropertyChange

    private void btnAddItemServActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddItemServActionPerformed
        try {
            if (servico.getCodServico() != null) {
                ItemServicoOS item = new ItemServicoOS();
                if (!tabelaItensServico.isEnabled()) {
                    item = listaItensServico.get(tabelaItensServico.getSelectedRow());
                    listaItensServico.remove(item);
                }
                item.setOrdemServico(ordemServico);
                item.setServico((Servico) servico.clone());
                item.setMostrarQtde(chkMostraQtdHoras.isSelected());
                item.setObservacoes(edtObservServ.getText());
                item.setQuantidade(Format.stringToDouble(edtHoras.getText().trim()));
                item.setValorUnitario(servico.getValorServico());
                item.setValorBruto(Format.stringToDouble(edtValorBrutoServ.getText().trim()));
                for (ItemServicoOS i : listaItensServico) {
                    if (item.equals(i)) {
                        item.setQuantidade(item.getQuantidade() + i.getQuantidade());
                        item.setValorBruto(item.getValorBruto() + i.getValorBruto());
                        listaItensServico.remove(i);
                        break;
                    }
                }
                listaItensServico.add(item);
                limpaServico();
                recalculaTotais();
            }
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_btnAddItemServActionPerformed

    private void btnAddTesteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddTesteActionPerformed
        if (!edtDescrTeste.getText().trim().equals("") && !edtResultTest.getText().trim().equals("")) {
            EquipamentoTeste teste = new EquipamentoTeste();
            teste.setDescricao(edtDescrTeste.getText().trim());
            teste.setDefeito(edtResultTest.getText().trim());
            teste.setEquipamento(equipamento);
            listaEquipamentoTestes.add(teste);
            edtDescrTeste.setText("");
            edtResultTest.setText("");
        }
        edtDescrTeste.requestFocus();
    }//GEN-LAST:event_btnAddTesteActionPerformed

    private void edtResultTestKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtResultTestKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnAddTesteActionPerformed(null);
        }
    }//GEN-LAST:event_edtResultTestKeyReleased

    private void edtDescrTesteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtDescrTesteKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            edtResultTest.requestFocus();
        }
    }//GEN-LAST:event_edtDescrTesteKeyReleased

    private void edtNroSerieKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtNroSerieKeyReleased
    }//GEN-LAST:event_edtNroSerieKeyReleased

    private void edtNroSerieKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtNroSerieKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (cliente.getCdPessoa() != null) {
                equipamento.setCliente(cliente);
                equipamento.setNroSerie(edtNroSerie.getText().toUpperCase().trim());
                if (EquipamentoDao.alreadyExistsEquip(equipamento, Variaveis.getEmpresa())) {
                    Extra.Informacao(this, "Equipamento encontrado!");
                    if (EquipamentoDao.emGarantia(equipamento, Variaveis.getEmpresa())) {
                        Extra.Informacao(this, "Este equipamento parece estar na garantia!\n"
                                + "Favor verificar seu histórico!");
                    }
                    EquipamentoDao.buscaEquipamentoSerie(equipamento, Variaveis.getEmpresa(), true);
                    setEquipamento();
                } else {
                    equipamento.setCodEquipamento(null);
                }
                ativaEditsEquip(true);
            } else {
                Extra.Informacao(this, "Primeiro defina o cliente!");
            }
        }
    }//GEN-LAST:event_edtNroSerieKeyPressed

    private void btnAtualizarValoresProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarValoresProdActionPerformed
        if (Extra.Pergunta(this, "Esta opção ira recalcular todos os itens de produtos com os seus respectivos valores atualizados!\n"
                + "Após a execução desta opção será necessaria a geração de uma nova revisão de orçamento!\n"
                + "Recalcular todos os itens de produtos ?")) {
            for (ItemProdOS o : listaItensProd) {
                ProdutoDao.buscaProduto(o.getProduto(), Variaveis.getEmpresa(), true);
                o.setValorUnitario(o.getProduto().getValorVendaProduto());
                o.setValorBruto(o.getValorUnitario() * o.getQuantidade());
            }
            recalculaTotais();
        }
    }//GEN-LAST:event_btnAtualizarValoresProdActionPerformed

    private void btnAtualizarValoresServActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarValoresServActionPerformed
        if (Extra.Pergunta(this, "Esta opção ira recalcular todos os itens de serviço com os seus respectivos valores atualizados!\n"
                + "Após a execução desta opção será necessaria a geração de uma nova revisão de orçamento!\n"
                + "Recalcular todos os itens de serviço ?")) {
            for (ItemServicoOS o : listaItensServico) {
                ServicoDao.buscaServico(o.getServico(), Variaveis.getEmpresa());
                o.setValorUnitario(o.getServico().getValorServico());
                o.setValorBruto(o.getValorUnitario() * o.getQuantidade());
            }
            recalculaTotais();
        }
    }//GEN-LAST:event_btnAtualizarValoresServActionPerformed

    private void mItemProdRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mItemProdRemoverActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
            listaItensProd.remove(tabelaItensProd.getSelectedRow());
            recalculaTotais();
        }
    }//GEN-LAST:event_mItemProdRemoverActionPerformed

    private void mItemServRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mItemServRemoverActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
            listaItensServico.remove(tabelaItensServico.getSelectedRow());
            recalculaTotais();
        }
    }//GEN-LAST:event_mItemServRemoverActionPerformed

    private void mEnvolvidosRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mEnvolvidosRemoverActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
            listaEnvolvidos.remove(tabelaEnvolvidos.getSelectedRow());
        }
    }//GEN-LAST:event_mEnvolvidosRemoverActionPerformed

    private void mAnalizeRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mAnalizeRemoverActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
            listaEquipamentoTestes.remove(tabelaTestes.getSelectedRow());
        }
    }//GEN-LAST:event_mAnalizeRemoverActionPerformed

    private void mItemProdAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mItemProdAlterarActionPerformed
        try {
            limpaProd();
            ItemProdOS item = listaItensProd.get(tabelaItensProd.getSelectedRow());
            produto = item.getProduto();
            setProduto();
            edtQtdeProduto.setText(Format.FormataValor(Format.doubleToString(item.getQuantidade()), Variaveis.getDecimal()));
            chkProdOriginal.setSelected(item.getOriginal());
            tabelaItensProd.setEnabled(false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_mItemProdAlterarActionPerformed

    private void mItemServAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mItemServAlterarActionPerformed
        try {
            limpaServico();
            ItemServicoOS item = listaItensServico.get(tabelaItensServico.getSelectedRow());
            servico = item.getServico();
            setServico();
            edtObservServ.setText(item.getObservacoes());
            edtValorBrutoServ.setText(Format.FormataValor(Format.doubleToString(item.getValorBruto()), Variaveis.getDecimal()));
            edtHoras.setText(Format.FormataValor(Format.doubleToString(item.getQuantidade()), Variaveis.getDecimal()));
            chkMostraQtdHoras.setSelected(item.getMostrarQtde());
            tabelaItensServico.setEnabled(false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_mItemServAlterarActionPerformed

    private void btnCancelarItemServActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarItemServActionPerformed
        limpaServico();
    }//GEN-LAST:event_btnCancelarItemServActionPerformed

    private void btnCancelarItemProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarItemProdActionPerformed
        limpaProd();
    }//GEN-LAST:event_btnCancelarItemProdActionPerformed

    private void chkAplicarDescontoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkAplicarDescontoItemStateChanged
        recalculaTotais();
    }//GEN-LAST:event_chkAplicarDescontoItemStateChanged

    private void btnRecalcularTotaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecalcularTotaisActionPerformed
        recalculaTotais();
        Extra.Informacao(this, "Os totais estão atualizados!");
    }//GEN-LAST:event_btnRecalcularTotaisActionPerformed

    private void btnImprConsertoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprConsertoActionPerformed
        HashMap h = new HashMap();
        List ordems = new ArrayList();
        ordems.add(this.ordemServico);
        h.put("USUARIO", this.usuario);
        Util.imprimeRelatorioJarFile(ordems, h, "rel/fichaconserto/relFichaConc.jasper", Variaveis.getEmpresa());
    }//GEN-LAST:event_btnImprConsertoActionPerformed

    private void btnAuthOrcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAuthOrcActionPerformed
        if (Extra.Pergunta(this, "Confirma autorização do cliente para execução da Ordem de Serviço por meio do orçamento [ Revisão: " + String.valueOf(ordemServico.getUltimoOrcamento().getNroRevisao()) + " ] ?")) {
            if (ordemServico.getUltimoOrcamento().getDataCadastro() == null) {
                ordemServico.getUltimoOrcamento().setDataCadastro(new Date());
            }
            if (Format.getDiffDay(ordemServico.getUltimoOrcamento().getDataCadastro(), new Date()) <= ordemServico.getUltimoOrcamento().getDiasValidade()) {
                if (!ordemServico.getUltimoOrcamento().getAutorizado()) {
                    Orcamento item = ordemServico.getUltimoOrcamento();
                    listaOrcamentos.remove(item);
                    item.setAutorizado(true);
                    item.setDataAutorizacao(new Date());
                    listaOrcamentos.add(item);
                }
                cboxStatus.setSelectedItem("EM ANDAMENTO");
                btnManutReal.setEnabled(true);
                Extra.Informacao(this, "Orçamento Autorizado!, Aguardando retorno da manuteção!");
            } else {
                Extra.Informacao(this, "Praso de validade do último orçamento venceu! Favor gerar um novo orçamento!");
            }
        }
    }//GEN-LAST:event_btnAuthOrcActionPerformed

    private void btnImprOrcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprOrcActionPerformed
        if (listaOrcamentos.size() > 0) {
            if (ordemServico.getUltimoOrcamento() != null) {
                HashMap h = new HashMap();
                List ordems = new ArrayList();
                ordems.add(this.ordemServico);
                h.put("USUARIO", this.usuario);
                Util.imprimeRelatorioJarFile(ordems, h, "rel/orcamento/relOrcCab.jasper", Variaveis.getEmpresa());
            }
        } else {
            Extra.Informacao(this, "Não há orçamentos gerados para esta Ordem de Serviço!");
        }
    }//GEN-LAST:event_btnImprOrcActionPerformed

    private void btnManutRealActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManutRealActionPerformed
        if (Extra.Pergunta(this, "Confirma realização da manutenção ?")) {
            cboxStatus.setSelectedItem("AGUARDANDO PGTO");
            btnFinalPgto.setEnabled(true);
            btnManutReal.setEnabled(false);
            Extra.Informacao(this, "Manutenção realizada!, Aguardando Pagamento!");
        }
    }//GEN-LAST:event_btnManutRealActionPerformed

    private void btnFinalPgtoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalPgtoActionPerformed
        if (Extra.Pergunta(this, "Finalizar pagamanto da ordem de serviço?")) {
            cboxStatus.setSelectedItem("FINALIZADO");
            btnFinalPgto.setEnabled(false);
            Extra.Informacao(this, "Ordem de Serviço finalizada!");
        }
    }//GEN-LAST:event_btnFinalPgtoActionPerformed

    private void btnNovaTraspActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaTraspActionPerformed
        FI_CadFindPessoa transpForm = (FI_CadFindPessoa) Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", "Localiza Transportadora - " + Util.getSource(numInstancias, FI_CadFindPessoa.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (transpForm != null) {
            transpForm.pessoa = transportadora;
            transpForm.setTipoObrigatorioBusca(new Transportadora());
        }
    }//GEN-LAST:event_btnNovaTraspActionPerformed

    private void cboxTranspItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboxTranspItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cboxTranspItemStateChanged

    private void cboxTipoEquipActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxTipoEquipActionPerformed
        if (aa) {
            if ((!cboxTipoEquip.getSelectedItem().equals(tipoEquipAnterior)) && (tipoEquipAnterior != null)) {
                if (Extra.Pergunta(this, "Descartar Propriedades?")) {
                    if (cboxTipoEquip.getSelectedIndex() == 0) {
                        tipoEquipAnterior = null;
                        listaItensPropriedades.clear();
                    } else {
                        populaTabelaPropDef();
                    }
                } else {
                    cboxTipoEquip.setSelectedItem(tipoEquipAnterior);
                }
            } else if ((tipoEquipAnterior == null) && (cboxTipoEquip.getSelectedIndex() != 0)) {
                populaTabelaPropDef();
            }
        } else {
            aa = true;
        }
    }//GEN-LAST:event_cboxTipoEquipActionPerformed

    private void btnImprConserto1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprConserto1ActionPerformed
        HashMap h = new HashMap();
        List ordems = new ArrayList();
        ordems.add(ordemServico);
        h.put("USUARIO", usuario);
        Util.imprimeRelatorioJarFile(ordems, h, "rel/fichacliente/relFichaCliConc.jasper", Variaveis.getEmpresa());
    }//GEN-LAST:event_btnImprConserto1ActionPerformed

    private void mPropRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mPropRemoveActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
            listaItensPropriedades.remove(tabelaPropriedades.getSelectedRow());
        }
    }//GEN-LAST:event_mPropRemoveActionPerformed

    private void tabelaImagensMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaImagensMouseClicked
        if (evt.getClickCount() >= 2) {
            if (tabelaImagens.getSelectedRowCount() > 0) {
                if (imagemConexao != null) {
                    int index = ((tabelaImagens.getSelectedRow() * column) + tabelaImagens.getSelectedColumn());
                    Imagem o = listaImagensCurrence.get(index);
                    ImageIcon iFim = o.getBlobImagem();
                    imagemConexao.setBlobImagem(iFim);
                    imagemConexao.setCodImagem(o.getCodImagem());
                    imagemConexao.setMd5Imagem(o.getMd5Imagem());
                    imagemConexao.setNomeImagem(o.getNomeImagem());
                    imagemConexao.setFlagPronto(isIcon);
                }
            } else {
                Extra.Informacao(this, "Selecione a imagem na tabela!");
            }
        }
    }//GEN-LAST:event_tabelaImagensMouseClicked

    private void tabelaImagensKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaImagensKeyPressed
    }//GEN-LAST:event_tabelaImagensKeyPressed

    private void btnAddImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddImageActionPerformed
        if (imagemConexao != null) {
            if (imagemConexao.getBlobImagem() != null) {
                if (imagemConexao.getCodImagem() == null) {
                    Integer a = OrdemServicoDao.imagemConexaoInDB(Variaveis.getEmpresa(), imagemConexao);
                    try {
                        if (a != null) {
                            if (!Extra.Pergunta(this, "A imagem utilizada como conexão parece já existir no Banco de Dados, gostaria de salva-la mesmo assim?\n"
                                    + "Para procura-la no banco de dados basta clicar em 'Imagem DB'")) {
                                return;
                            }
                        }
                        String qry = "INSERT INTO conexao_equip (cd_empresa, nome_conexao_equip, md5_conexao_equip, blob_conexao_equip) "
                                + "VALUES (" + Variaveis.getEmpresa().getCodigo() + ", '" + imagemConexao.getNomeImagem() + "', '" + imagemConexao.getMd5Imagem() + "', ?) RETURNING cd_conexao_equip";
                        imagemConexao.setCodImagem(ImagemDao.setImagemBanco(qry, new File(nomeArq)));
                    } catch (IOException ex) {
                        Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
                    }

                }
            }
        }
        adicionaImagem(imagemConexao);
        limpaImagemConexao();
    }//GEN-LAST:event_btnAddImageActionPerformed

    private void menuRemoverImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverImageActionPerformed
        if (tabelaImagens.getSelectedRowCount() > 0) {
            if (Extra.Pergunta(this, "Confirma remoção da Imagem?")) {
                int index = ((tabelaImagens.getSelectedRow() * column) + tabelaImagens.getSelectedColumn());
                listaImagensCurrence.remove(index);
                atualizaImagens();
            }
        } else {
            Extra.Informacao(this, "Selecione a imagem na tabela!");
        }
    }//GEN-LAST:event_menuRemoverImageActionPerformed

    private void cboxTranspActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxTranspActionPerformed
        if (cboxTransp.getSelectedIndex() == 0) {
            cboxEnderTransp.removeAllItems();
            cboxEnderTransp.addItem("");
        } else {
            Transportadora t = (Transportadora) cboxTransp.getSelectedItem();
            if (t.getEnderecos().size() > 0) {
                String selected = "";
                if (cboxEnderTransp.getSelectedItem() != null) {
                    selected = cboxEnderTransp.getSelectedItem().toString();
                }
                cboxEnderTransp.removeAllItems();

                for (Endereco e : t.getEnderecos()) {
                    cboxEnderTransp.addItem(e);
                }
                if (!selected.trim().equals("")) {
                    cboxTransp.setSelectedItem(selected);
                }
            } else {
                cboxEnderTransp.removeAllItems();
                cboxEnderTransp.addItem("");
            }
        }
    }//GEN-LAST:event_cboxTranspActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddEnvolvidos;
    private javax.swing.JButton btnAddImage;
    private javax.swing.JButton btnAddItemProd;
    private javax.swing.JButton btnAddItemServ;
    private javax.swing.JButton btnAddProd;
    private javax.swing.JButton btnAddServ;
    private javax.swing.JButton btnAddTeste;
    private javax.swing.JButton btnAtualizarValoresProd;
    private javax.swing.JButton btnAtualizarValoresServ;
    private javax.swing.JButton btnAuthOrc;
    private javax.swing.JButton btnBuscaCliente;
    private javax.swing.JButton btnBuscarOS;
    private javax.swing.JButton btnCancOS;
    private javax.swing.JButton btnCancelarItemProd;
    private javax.swing.JButton btnCancelarItemServ;
    private javax.swing.JButton btnCarregaImagemBD;
    private javax.swing.JButton btnCarregaImagemPasta;
    private javax.swing.JButton btnEditOS;
    private javax.swing.JButton btnExcluirOS;
    private javax.swing.JButton btnFinalPgto;
    private javax.swing.JButton btnGerarOrc;
    private javax.swing.JButton btnImprConserto;
    private javax.swing.JButton btnImprConserto1;
    private javax.swing.JButton btnImprOrc;
    private javax.swing.JButton btnLimparImagem;
    private javax.swing.JButton btnManutReal;
    private javax.swing.JButton btnNovaLocalizacao;
    private javax.swing.JButton btnNovaOS;
    private javax.swing.JButton btnNovaTrasp;
    private javax.swing.JButton btnNovoTipoEquip;
    private javax.swing.JButton btnRecalcularTotais;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSaveOS;
    private javax.swing.JButton btnVerificar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cboxContatoCliente;
    private javax.swing.JComboBox cboxEnderCliente;
    private javax.swing.JComboBox cboxEnderTransp;
    private javax.swing.JComboBox cboxGarantia;
    private javax.swing.JComboBox cboxLocal;
    private javax.swing.JComboBox cboxStatus;
    private javax.swing.JComboBox cboxTipoEquip;
    private javax.swing.JComboBox cboxTipoFrete;
    private javax.swing.JComboBox cboxTransp;
    private javax.swing.JCheckBox chkAplicarDesconto;
    private javax.swing.JCheckBox chkGarantia;
    private javax.swing.JCheckBox chkMostraQtdHoras;
    private javax.swing.JCheckBox chkProdOriginal;
    private beans.Cliente cliente;
    private javax.swing.JTextField edtCodCliente;
    private javax.swing.JTextArea edtConserto;
    private javax.swing.JTextField edtCpfCliente;
    private javax.swing.JFormattedTextField edtDataAuth;
    private javax.swing.JFormattedTextField edtDataCad;
    private javax.swing.JFormattedTextField edtDataEntrada;
    private javax.swing.JFormattedTextField edtDataFbr;
    private javax.swing.JFormattedTextField edtDataPrev;
    private javax.swing.JFormattedTextField edtDataSaida;
    private javax.swing.JTextArea edtDefeito;
    private javax.swing.JTextField edtDescServ;
    private javax.swing.JTextField edtDesconto;
    private javax.swing.JTextField edtDescrProduto;
    private javax.swing.JTextField edtDescrTeste;
    private javax.swing.JTextArea edtDescricaoEquip;
    private javax.swing.JTextArea edtEstadoEquip;
    private javax.swing.JTextField edtHoras;
    private javax.swing.JTextField edtMarca;
    private javax.swing.JTextField edtMargemCliente;
    private javax.swing.JTextField edtModelo;
    private javax.swing.JTextField edtNomeCliente;
    private javax.swing.JTextField edtNroOCcliente;
    private javax.swing.JTextField edtNroRastreioCli;
    private javax.swing.JTextField edtNroRastreioInt;
    private javax.swing.JTextField edtNroSerie;
    private javax.swing.JTextArea edtObservServ;
    private javax.swing.JTextArea edtParecer;
    private javax.swing.JTextField edtPrecoProd;
    private javax.swing.JTextField edtQtdeProduto;
    private javax.swing.JTextField edtResultTest;
    private javax.swing.JTextField edtTelefoneCliente;
    private javax.swing.JTextField edtValorBrutoServ;
    private javax.swing.JTextField edtValorFrete;
    private javax.swing.JTextField edtValorUnitServ;
    private javax.swing.JTextField edtVlrBruto;
    private javax.swing.JTextField edtVlrTotal;
    private beans.Equipamento equipamento;
    private javax.swing.JLabel iconConexao;
    private javax.swing.JLabel iconProd;
    private beans.Imagem imagemConexao;
    private beans.Imagem imagemProd;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblDataAuth;
    private javax.swing.JLabel lblDataCad;
    private javax.swing.JLabel lblDataNasc2;
    private javax.swing.JLabel lblDataNasc3;
    private javax.swing.JLabel lblPrecoProd;
    private javax.swing.JLabel lblSubTotalProd;
    private javax.swing.JLabel lblSubtotalServ;
    private java.util.List<Pessoa> listaEnvolvidos;
    private java.util.List<EquipamentoTeste> listaEquipamentoTestes;
    private java.util.List<ItemProdOS> listaItensProd;
    private java.util.List<ItemPropTipoEquip> listaItensPropriedades;
    private java.util.List<ItemServicoOS> listaItensServico;
    private java.util.List<Orcamento> listaOrcamentos;
    private beans.LocalProd localProd;
    private javax.swing.JMenuItem mAnalizeRemover;
    private javax.swing.JMenuItem mEnvolvidosRemover;
    private javax.swing.JMenuItem mItemProdAlterar;
    private javax.swing.JMenuItem mItemProdRemover;
    private javax.swing.JMenuItem mItemServAlterar;
    private javax.swing.JMenuItem mItemServRemover;
    private javax.swing.JMenuItem mPropRemove;
    private javax.swing.JPopupMenu menuAnalize;
    private javax.swing.JPopupMenu menuEnvolvidos;
    private javax.swing.JPopupMenu menuItensProd;
    private javax.swing.JPopupMenu menuItensServ;
    private javax.swing.JPopupMenu menuProp;
    private javax.swing.JMenuItem menuRemoverImage;
    public beans.OrdemServico ordemServico;
    private javax.swing.JTabbedPane pnlAbasEquipamento;
    private javax.swing.JPanel pnlAjustItensProd;
    private javax.swing.JPanel pnlAnalize;
    private javax.swing.JPanel pnlBotoes;
    private javax.swing.JPanel pnlCodPessoa;
    private javax.swing.JPanel pnlComplementar;
    private javax.swing.JPanel pnlConserto;
    private javax.swing.JPanel pnlDCliente;
    private javax.swing.JPanel pnlDEquipamento;
    private javax.swing.JPanel pnlDetalhes;
    private javax.swing.JPanel pnlEnvolvidos;
    private javax.swing.JPanel pnlFinalizacao;
    private javax.swing.JPanel pnlHistEquipEntr;
    private javax.swing.JPanel pnlInfoBasicas;
    private javax.swing.JPanel pnlInformacoes;
    private javax.swing.JPanel pnlItensProd;
    private javax.swing.JPanel pnlItensServ;
    private javax.swing.JPanel pnlManutencao;
    private javax.swing.JPanel pnlOCPendentes;
    private javax.swing.JPanel pnlOrcamento;
    private javax.swing.JPanel pnlPrincipalOS;
    private javax.swing.JTabbedPane pnlTabManutencao;
    private javax.swing.JTabbedPane pnlTabulado;
    private javax.swing.JTabbedPane pnlTabuladoFinalizacao;
    private javax.swing.JPopupMenu popupImage;
    private beans.Produto produto;
    private beans.Servico servico;
    private javax.swing.JTable tabelaEnvolvidos;
    private javax.swing.JTable tabelaHistEquipEntr;
    private javax.swing.JTable tabelaImagens;
    private javax.swing.JTable tabelaItensProd;
    private javax.swing.JTable tabelaItensServico;
    private javax.swing.JTable tabelaOCPendentes;
    private javax.swing.JTable tabelaOrcamentos;
    private javax.swing.JTable tabelaPropriedades;
    private javax.swing.JTable tabelaTestes;
    private beans.TipoEquipamento tipoEquipamento;
    private beans.Transportadora transportadora;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

    private void populaTabelaPropDef() {
        tipoEquipAnterior = ((TipoEquipamento) cboxTipoEquip.getSelectedItem());
        listaItensPropriedades.clear();
        tipoEquipAnterior.getPropriedades().clear();
        tipoEquipAnterior.getPropriedades().addAll(EquipamentoDao.getAllPropTipoEquipamento(tipoEquipAnterior, Variaveis.getEmpresa()));
        for (PropriedadeTipoEquip p : tipoEquipAnterior.getPropriedades()) {
            ItemPropTipoEquip a = new ItemPropTipoEquip(p, "");
            listaItensPropriedades.add(a);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof OrdemServico) {
            limpaCampos();
            ativaEdits(false);
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
            ativaButtons(true, true, true, true, true, false);
            alterado = false;
            setOS();
        } else if (o instanceof Imagem) {
            setImagemConexao();
        } else if (o instanceof LocalProd) {
            setLocalizacao();
        } else if (o instanceof TipoEquipamento) {
            setTipoEquip();
        } else if (o instanceof Transportadora) {
            setTransportadora();
        } else if (o instanceof Cliente) {
            setCliente();
        } else if (o instanceof Produto) {
            setProduto();
        } else if (o instanceof Servico) {
            setServico();
        }
    }
}
