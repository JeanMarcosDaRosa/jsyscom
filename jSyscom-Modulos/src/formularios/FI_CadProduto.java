/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import base.FrmPrincipal;
import beans.*;
import dao.ImagemDao;
import dao.NFEntradaDao;
import dao.NextDaos;
import dao.ProdutoDao;
import funcoes.Util;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import utilitarios.*;

/**
 *
 * @author Jean
 */
public final class FI_CadProduto extends javax.swing.JInternalFrame implements Observer {

    private static Integer numInstancias = 0;
    private Integer numMaxInstancias = 65535;
    private static final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private static Integer seqInstancias = 0;
    boolean alterado = false;
    private Usuario usuario = null;
    private JFileChooser fc;
    private String nomeArq = null;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }

    public FI_CadProduto(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title + (seqInstancias > 0 ? (" - [" + String.valueOf(seqInstancias) + "]") : ""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            ativaEdits(false);
            ativaButtons(true, true, false, false, false, false);
            imagemProd.addObserver(this);
            //Populo o combo de Categorias de Produtos
            loadLstCategoria(ProdutoDao.getAllCategorias(Variaveis.getEmpresa()));
            //Populo o combo de Unidades de Medida
            loadLstUnidade(ProdutoDao.getAllUnidades(Variaveis.getEmpresa()));
            //Populo o combo de Locais dos Produtos
            loadLstLocalProd(ProdutoDao.getAllLocaisProdutos(Variaveis.getEmpresa()));
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
            produto.addObserver(this);
            fornecedor.addObserver(this);
            unidade.addObserver(this);
            categoriaProd.addObserver(this);
            localProd.addObserver(this);
            iconProd.setHorizontalAlignment(JLabel.CENTER);
            produto.setImagemProduto(imagemProd);
            limpaCampos();
        }
    }

    public void loadLstCategoria(List<CategoriaProd> lista) {
        String selected = "";
        if (cboxCategoria.getSelectedItem() != null) {
            selected = cboxCategoria.getSelectedItem().toString();
        }
        cboxCategoria.removeAllItems();
        cboxCategoria.addItem("");
        for (CategoriaProd cp : lista) {
            cboxCategoria.addItem(cp);
        }
        cboxCategoria.setSelectedItem(selected);
    }

    public void loadLstUnidade(List<Unidade> lista) {
        String selected = "";
        if (cboxUnidade.getSelectedItem() != null) {
            selected = cboxUnidade.getSelectedItem().toString();
        }
        cboxUnidade.removeAllItems();
        cboxUnidade.addItem("");
        for (Unidade cp : lista) {
            cboxUnidade.addItem(cp);
        }
        cboxUnidade.setSelectedItem(selected);
    }

    public void loadLstLocalProd(List<LocalProd> lista) {
        String selected = "";
        if (cboxLocal.getSelectedItem() != null) {
            selected = cboxLocal.getSelectedItem().toString();
        }
        cboxLocal.removeAllItems();
        cboxLocal.addItem("");
        for (LocalProd cp : lista) {
            cboxLocal.addItem(cp);
        }
        cboxLocal.setSelectedItem(selected);
    }

    public void setImagemProd() {
        if (imagemProd.getBlobImagem() != null) {
            if (imagemProd.getBlobImagem().getIconWidth() > 256) {
                imagemProd.setBlobImagem(new ImageIcon(imagemProd.getBlobImagem().getImage().getScaledInstance(256, -1, Image.SCALE_DEFAULT)));
            }
        }
        produto.setImagemProduto(imagemProd);
        iconProd.setIcon(imagemProd.getBlobImagem());
    }

    public void setUnidade() {
        loadLstUnidade(ProdutoDao.getAllUnidades(Variaveis.getEmpresa()));
        cboxUnidade.setSelectedItem(unidade);
    }

    public void setCategoria() {
        loadLstCategoria(ProdutoDao.getAllCategorias(Variaveis.getEmpresa()));
        cboxCategoria.setSelectedItem(categoriaProd);
    }

    public void setLocalizacao() {
        loadLstLocalProd(ProdutoDao.getAllLocaisProdutos(Variaveis.getEmpresa()));
        cboxLocal.setSelectedItem(localProd);
    }

    public void setProduto() {
        try {
            limpaCampos();
            lblCodigo.setText(String.valueOf(produto.getCodProduto()));
            edtDescricao.setText(produto.getDescricaoProduto());
            edtDetalhes.setText(produto.getDetalhesProduto());
            if (produto.getDataCadastroProduto() != null) {
                edtDataCad.setText(Format.DateToStr(produto.getDataCadastroProduto()));
            }
            edtPreco.setText(Format.FormataValor(Format.doubleToString(produto.getValorVendaProduto()), Variaveis.getDecimal()));
            edtEstMin.setText(Format.FormataValor(Format.doubleToString(produto.getEstMinimo()), Variaveis.getDecimal()));
            edtQtdEstoque.setText(Format.FormataValor(Format.doubleToString(this.produto.getQtdEstoque()), Variaveis.getDecimal()));
            if (produto.getCategoriaProd() != null) {
                cboxCategoria.setSelectedItem(produto.getCategoriaProd());
                Double marg = ((CategoriaProd) cboxCategoria.getSelectedItem()).getMargemCategoria();
                lblMargemC.setText(Format.FormataValor(Format.doubleToString(marg), Variaveis.getDecimal()));
                lblPrecoFinal.setText(Format.FormataValor(Format.doubleToString(produto.getValorCompraProduto()+(produto.getValorCompraProduto() * (marg / 100))), Variaveis.getDecimal()));
            }
            if (produto.getLocalProd() != null) {
                cboxLocal.setSelectedItem(produto.getLocalProd());
            }
            if (produto.getUnidade() != null) {
                cboxUnidade.setSelectedItem(produto.getUnidade());
            }
            cboxSituacao.setSelectedIndex(produto.getSituacaoProduto() ? 0 : 1);
            listaEan.clear();
            listaEan.addAll(produto.getEans());
            listaPessoaFornecedor.clear();
            listaPessoaFornecedor.addAll(produto.getFornecedores());
            imagemProd.clearImagem();
            imagemProd.setCodImagem(produto.getImagemProduto().getCodImagem());
            imagemProd.setNomeImagem(produto.getImagemProduto().getNomeImagem());
            imagemProd.setMd5Imagem(produto.getImagemProduto().getMd5Imagem());
            imagemProd.setBlobImagem(produto.getImagemProduto().getBlobImagem());
            listaHistCompra.clear();
            listaHistCompra.addAll(NFEntradaDao.buscaNFEntradaProduto(produto, Variaveis.getEmpresa(), false));
            setImagemProd();
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }

    public void populaProduto() {
        try {
            Integer cd = null;
            if (produto.getCodProduto() != null) {
                cd = produto.getCodProduto();
            }
            produto.clearProduto();
            produto.setCodProduto(cd);
            produto.setCategoriaProd((CategoriaProd) cboxCategoria.getSelectedItem());
            if (!Validador.soNumeros(edtDataCad.getText()).equals("")) {
                produto.setDataCadastroProduto(Format.StrToDate(edtDataCad.getText()));
            } else {
                produto.setDataCadastroProduto(null);
            }
            produto.setDescricaoProduto(edtDescricao.getText().trim());
            produto.setDetalhesProduto(edtDetalhes.getText().trim());
            produto.setImagemProduto(imagemProd);
            produto.setLocalProd((LocalProd) cboxLocal.getSelectedItem());
            produto.setSituacaoProduto(cboxSituacao.getSelectedIndex() == 0);
            produto.setUnidade((Unidade) cboxUnidade.getSelectedItem());
            produto.setValorVendaProduto(Format.stringToDouble(edtPreco.getText().trim()));
            produto.setEstMinimo(Format.stringToDouble(edtEstMin.getText().trim()));
            produto.setQtdEstoque(Format.stringToDouble(this.edtQtdEstoque.getText().trim()));
            produto.getEans().clear();
            produto.getEans().addAll(listaEan);
            produto.getFornecedores().clear();
            for (Pessoa p : listaPessoaFornecedor) {
                produto.getFornecedores().add((Fornecedor) p);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }

    private void cancelar() {
        if (alterado) {
            if (Extra.Pergunta(this, "Descartar alterações não salvas?")) {
                pnlTabulado.setSelectedComponent(pnlPrincipalProduto);
                limpaCampos();
                ativaEdits(false);
                ativaButtons(true, true, false, false, false, false);
                alterado = false;
                produto.clearProduto();
                fornecedor.clearPessoa();
                pnlTabulado.setEnabledAt(1, false);
                pnlTabulado.setEnabledAt(2, false);
                pnlTabulado.setEnabledAt(3, false);
            }
        } else {
            pnlTabulado.setSelectedComponent(pnlPrincipalProduto);
            limpaCampos();
            ativaEdits(false);
            ativaButtons(true, true, false, false, false, false);
            alterado = false;
            produto.clearProduto();
            fornecedor.clearPessoa();
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
        }
    }

    private void ativaEdits(Boolean ativa) {
        edtDescricao.setEnabled(ativa);
        edtDetalhes.setEnabled(ativa);
        edtEAN.setEnabled(ativa);
        edtPreco.setEnabled(ativa);
        edtEstMin.setEnabled(ativa);
        edtQtdEstoque.setEnabled(ativa);
        cboxCategoria.setEnabled(ativa);
        cboxLocal.setEnabled(ativa);
        cboxSituacao.setEnabled(ativa);
        cboxUnidade.setEnabled(ativa);
        btnCarregaImagemBD.setEnabled(ativa);
        btnCarregaImagemPasta.setEnabled(ativa);
        btnLimparImagem.setEnabled(ativa);
        btnNovaCategoria.setEnabled(ativa);
        btnNovaLocalizacao.setEnabled(ativa);
        btnNovaUnidade.setEnabled(ativa);
    }

    private void ativaButtons(Boolean buscar, Boolean novo, Boolean alterar,
            Boolean excluir, Boolean cancelar, Boolean salvar) {
        btnBuscarProduto.setEnabled(buscar);
        btnCancProduto.setEnabled(cancelar);
        btnExcluirProduto.setEnabled(excluir);
        btnEditProduto.setEnabled(alterar);
        btnNovoProduto.setEnabled(novo);
        btnSaveProduto.setEnabled(salvar);
    }

    private void limpaCampos() {
        lblCodigo.setText("0");
        lblMargemC.setText("0,00");
        lblPrecoFinal.setText("0,00");
        edtDescricao.setText("");
        edtDetalhes.setText("");
        edtEAN.setText("");
        edtPreco.setText("0,00");
        edtEstMin.setText("0,00");
        edtQtdEstoque.setText("0,00");
        edtDataCad.setText("");
        if (cboxCategoria.getItemCount() > 0) {
            cboxCategoria.setSelectedIndex(0);
        }
        if (cboxLocal.getItemCount() > 0) {
            cboxLocal.setSelectedIndex(0);
        }
        if (cboxSituacao.getItemCount() > 0) {
            cboxSituacao.setSelectedIndex(0);
        }
        if (cboxUnidade.getItemCount() > 0) {
            cboxUnidade.setSelectedIndex(0);
        }
        listaEan.clear();
        listaPessoaFornecedor.clear();
        limpaImagem();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        buttonGroup1 = new javax.swing.ButtonGroup();
        listaEan = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Ean>());
        popupEan = new javax.swing.JPopupMenu();
        menuRemoverSelEan = new javax.swing.JMenuItem();
        menuRemoverTodosEan = new javax.swing.JMenuItem();
        listaPessoaFornecedor = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Pessoa>());
        popupFornecedor = new javax.swing.JPopupMenu();
        menuRemoverSelFornecedor = new javax.swing.JMenuItem();
        menuRemoverTodosFornecedor = new javax.swing.JMenuItem();
        produto = new beans.Produto();
        imagemProd = new beans.Imagem();
        fornecedor = new beans.Fornecedor();
        unidade = new beans.Unidade();
        categoriaProd = new beans.CategoriaProd();
        localProd = new beans.LocalProd();
        listaHistCompra = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<NFEntrada>());
        jScrollPane6 = new javax.swing.JScrollPane();
        pnlTabulado = new javax.swing.JTabbedPane();
        pnlPrincipalProduto = new javax.swing.JPanel();
        pnlTipoPessoa = new javax.swing.JPanel();
        edtDescricao = new javax.swing.JTextField();
        pnlCodPessoa = new javax.swing.JPanel();
        lblCodigo = new javax.swing.JLabel();
        pnlInfoBasicas = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDataNasc = new javax.swing.JLabel();
        lblTF = new javax.swing.JLabel();
        edtDataCad = new javax.swing.JFormattedTextField();
        cboxUnidade = new javax.swing.JComboBox();
        cboxCategoria = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        cboxLocal = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        edtPreco = new javax.swing.JTextField();
        iconProd = new javax.swing.JLabel();
        btnCarregaImagemPasta = new javax.swing.JButton();
        btnLimparImagem = new javax.swing.JButton();
        pnlDemonstrativo = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblMargemC = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblPrecoFinal = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        edtEstMin = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cboxSituacao = new javax.swing.JComboBox();
        btnNovaUnidade = new javax.swing.JButton();
        btnNovaCategoria = new javax.swing.JButton();
        btnNovaLocalizacao = new javax.swing.JButton();
        btnCarregaImagemBD = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        edtQtdEstoque = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        edtDetalhes = new javax.swing.JTextArea();
        pnlPrincipalFornecedor = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaFornecedor = new javax.swing.JTable();
        btnBuscaFornecedor = new javax.swing.JButton();
        pnlPrincipalEAN = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        edtEAN = new javax.swing.JTextField();
        btnInserir = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelaEan = new javax.swing.JTable();
        pnlHistCompra = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelaHistCompra = new javax.swing.JTable();
        pnlBotoes = new javax.swing.JPanel();
        btnSair = new javax.swing.JButton();
        btnBuscarProduto = new javax.swing.JButton();
        btnNovoProduto = new javax.swing.JButton();
        btnEditProduto = new javax.swing.JButton();
        btnExcluirProduto = new javax.swing.JButton();
        btnCancProduto = new javax.swing.JButton();
        btnSaveProduto = new javax.swing.JButton();

        menuRemoverSelEan.setText("Remover Selecionado(s)");
        menuRemoverSelEan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverSelEanActionPerformed(evt);
            }
        });
        popupEan.add(menuRemoverSelEan);

        menuRemoverTodosEan.setText("Remover Todos");
        menuRemoverTodosEan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverTodosEanActionPerformed(evt);
            }
        });
        popupEan.add(menuRemoverTodosEan);

        menuRemoverSelFornecedor.setText("Remover Selecionado(s)");
        menuRemoverSelFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverSelFornecedorActionPerformed(evt);
            }
        });
        popupFornecedor.add(menuRemoverSelFornecedor);

        menuRemoverTodosFornecedor.setText("Remover Todos");
        menuRemoverTodosFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverTodosFornecedorActionPerformed(evt);
            }
        });
        popupFornecedor.add(menuRemoverTodosFornecedor);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
        });

        pnlTipoPessoa.setBorder(javax.swing.BorderFactory.createTitledBorder("Descrição Produto"));

        edtDescricao.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                edtDescricaoFocusLost(evt);
            }
        });
        edtDescricao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtDescricaoKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout pnlTipoPessoaLayout = new javax.swing.GroupLayout(pnlTipoPessoa);
        pnlTipoPessoa.setLayout(pnlTipoPessoaLayout);
        pnlTipoPessoaLayout.setHorizontalGroup(
            pnlTipoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTipoPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(edtDescricao)
                .addContainerGap())
        );
        pnlTipoPessoaLayout.setVerticalGroup(
            pnlTipoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTipoPessoaLayout.createSequentialGroup()
                .addComponent(edtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 18, Short.MAX_VALUE))
        );

        pnlCodPessoa.setBorder(javax.swing.BorderFactory.createTitledBorder("Código"));
        pnlCodPessoa.setForeground(java.awt.Color.black);

        lblCodigo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblCodigo.setForeground(java.awt.Color.blue);
        lblCodigo.setText("0");

        javax.swing.GroupLayout pnlCodPessoaLayout = new javax.swing.GroupLayout(pnlCodPessoa);
        pnlCodPessoa.setLayout(pnlCodPessoaLayout);
        pnlCodPessoaLayout.setHorizontalGroup(
            pnlCodPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCodPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCodigo)
                .addContainerGap(81, Short.MAX_VALUE))
        );
        pnlCodPessoaLayout.setVerticalGroup(
            pnlCodPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCodPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCodigo)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pnlInfoBasicas.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações Basicas"));

        lblNome.setText("Unidade:");

        lblDataNasc.setText("Data Cadastro:");

        lblTF.setText("Categoria:");

        edtDataCad.setEditable(false);
        try {
            edtDataCad.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        edtDataCad.setEnabled(false);

        cboxCategoria.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboxCategoriaItemStateChanged(evt);
            }
        });

        jLabel1.setText("Local/Gôndola:");

        jLabel4.setText("Preço:");

        edtPreco.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtPreco.setForeground(new java.awt.Color(0, 0, 255));
        edtPreco.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtPreco.setText("0,00");
        edtPreco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtPrecoKeyReleased(evt);
            }
        });

        iconProd.setBackground(new java.awt.Color(153, 204, 255));
        iconProd.setForeground(new java.awt.Color(0, 102, 102));
        iconProd.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 51, 51)));

        btnCarregaImagemPasta.setText("Imagens Pasta");
        btnCarregaImagemPasta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaImagemPastaActionPerformed(evt);
            }
        });

        btnLimparImagem.setText("Limpar");
        btnLimparImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparImagemActionPerformed(evt);
            }
        });

        pnlDemonstrativo.setBorder(javax.swing.BorderFactory.createTitledBorder("Demonstrativo de Preços"));

        jLabel2.setText("Margem da Categoria(%):");

        lblMargemC.setForeground(new java.awt.Color(235, 8, 8));
        lblMargemC.setText("0,00");

        jLabel6.setText("Valor final com margem sobre a última compra (R$):");

        lblPrecoFinal.setForeground(new java.awt.Color(235, 8, 8));
        lblPrecoFinal.setText("0,00");

        javax.swing.GroupLayout pnlDemonstrativoLayout = new javax.swing.GroupLayout(pnlDemonstrativo);
        pnlDemonstrativo.setLayout(pnlDemonstrativoLayout);
        pnlDemonstrativoLayout.setHorizontalGroup(
            pnlDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDemonstrativoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel6))
                .addGap(115, 115, 115)
                .addGroup(pnlDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPrecoFinal)
                    .addComponent(lblMargemC))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlDemonstrativoLayout.setVerticalGroup(
            pnlDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDemonstrativoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblMargemC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lblPrecoFinal))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jLabel7.setText("Estoque Mínimo:");

        edtEstMin.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtEstMin.setForeground(new java.awt.Color(0, 0, 255));
        edtEstMin.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtEstMin.setText("0,00");
        edtEstMin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtEstMinKeyReleased(evt);
            }
        });

        jLabel8.setText("Situação:");

        cboxSituacao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ATIVO", "INATIVO" }));

        btnNovaUnidade.setText("...");
        btnNovaUnidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaUnidadeActionPerformed(evt);
            }
        });

        btnNovaCategoria.setText("...");
        btnNovaCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaCategoriaActionPerformed(evt);
            }
        });

        btnNovaLocalizacao.setText("...");
        btnNovaLocalizacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaLocalizacaoActionPerformed(evt);
            }
        });

        btnCarregaImagemBD.setText("Imagens BD");
        btnCarregaImagemBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaImagemBDActionPerformed(evt);
            }
        });

        jLabel9.setText("Qtd em Estoque:");

        edtQtdEstoque.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtQtdEstoque.setForeground(new java.awt.Color(0, 0, 255));
        edtQtdEstoque.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtQtdEstoque.setText("0,00");
        edtQtdEstoque.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtQtdEstoqueKeyReleased(evt);
            }
        });

        jLabel5.setText("Detalhes:");

        edtDetalhes.setColumns(20);
        edtDetalhes.setRows(5);
        jScrollPane5.setViewportView(edtDetalhes);

        javax.swing.GroupLayout pnlInfoBasicasLayout = new javax.swing.GroupLayout(pnlInfoBasicas);
        pnlInfoBasicas.setLayout(pnlInfoBasicasLayout);
        pnlInfoBasicasLayout.setHorizontalGroup(
            pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblNome, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblTF, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                                    .addComponent(cboxUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnNovaUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(lblDataNasc)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(edtDataCad, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                                    .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                                            .addComponent(edtPreco, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel7)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(edtEstMin, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(cboxCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnNovaCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                                    .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                                            .addComponent(cboxSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel9)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(edtQtdEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(cboxLocal, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnNovaLocalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInfoBasicasLayout.createSequentialGroup()
                                .addComponent(btnLimparImagem)
                                .addGap(44, 44, 44)
                                .addComponent(btnCarregaImagemBD)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCarregaImagemPasta))
                            .addComponent(iconProd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                        .addComponent(pnlDemonstrativo, javax.swing.GroupLayout.PREFERRED_SIZE, 543, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlInfoBasicasLayout.setVerticalGroup(
            pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlInfoBasicasLayout.createSequentialGroup()
                        .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCarregaImagemPasta)
                            .addComponent(btnLimparImagem)
                            .addComponent(btnCarregaImagemBD)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlInfoBasicasLayout.createSequentialGroup()
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNome)
                            .addComponent(lblDataNasc)
                            .addComponent(edtDataCad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboxUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNovaUnidade))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboxCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNovaCategoria)
                            .addComponent(lblTF))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(edtPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edtEstMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(cboxLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNovaLocalizacao))
                        .addGap(6, 6, 6)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(edtQtdEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel9))
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel8)
                                .addComponent(cboxSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jScrollPane5))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addComponent(pnlDemonstrativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlPrincipalProdutoLayout = new javax.swing.GroupLayout(pnlPrincipalProduto);
        pnlPrincipalProduto.setLayout(pnlPrincipalProdutoLayout);
        pnlPrincipalProdutoLayout.setHorizontalGroup(
            pnlPrincipalProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPrincipalProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlInfoBasicas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlPrincipalProdutoLayout.createSequentialGroup()
                        .addComponent(pnlTipoPessoa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlCodPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlPrincipalProdutoLayout.setVerticalGroup(
            pnlPrincipalProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalProdutoLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(pnlPrincipalProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlCodPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnlTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlInfoBasicas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pnlPrincipalProdutoLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {pnlCodPessoa, pnlTipoPessoa});

        pnlTipoPessoa.getAccessibleContext().setAccessibleName("");

        pnlTabulado.addTab("Produto", pnlPrincipalProduto);

        tabelaFornecedor.setComponentPopupMenu(popupFornecedor);
        tabelaFornecedor.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaPessoaFornecedor, tabelaFornecedor);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${cpfCnpjPessoa}"));
        columnBinding.setColumnName("Cpf/Cnpj");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nomePessoa}"));
        columnBinding.setColumnName("Nome");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${descricaoPessoa}"));
        columnBinding.setColumnName("Descricao");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${telefoneFixoPessoa}"));
        columnBinding.setColumnName("Telefone Fixo");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane1.setViewportView(tabelaFornecedor);

        btnBuscaFornecedor.setText("Buscar Fornecedor");
        btnBuscaFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaFornecedorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlPrincipalFornecedorLayout = new javax.swing.GroupLayout(pnlPrincipalFornecedor);
        pnlPrincipalFornecedor.setLayout(pnlPrincipalFornecedorLayout);
        pnlPrincipalFornecedorLayout.setHorizontalGroup(
            pnlPrincipalFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalFornecedorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPrincipalFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1006, Short.MAX_VALUE)
                    .addGroup(pnlPrincipalFornecedorLayout.createSequentialGroup()
                        .addComponent(btnBuscaFornecedor)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlPrincipalFornecedorLayout.setVerticalGroup(
            pnlPrincipalFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalFornecedorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 622, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBuscaFornecedor)
                .addGap(16, 16, 16))
        );

        pnlTabulado.addTab("Fornecedor", pnlPrincipalFornecedor);

        jLabel3.setText("EAN:");

        edtEAN.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtEANKeyPressed(evt);
            }
        });

        btnInserir.setText("Inserir");
        btnInserir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInserirActionPerformed(evt);
            }
        });

        tabelaEan.setComponentPopupMenu(popupEan);
        tabelaEan.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaEan, tabelaEan);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${ean}"));
        columnBinding.setColumnName("Ean");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${dataCad}"));
        columnBinding.setColumnName("Data Cadastro");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane2.setViewportView(tabelaEan);

        javax.swing.GroupLayout pnlPrincipalEANLayout = new javax.swing.GroupLayout(pnlPrincipalEAN);
        pnlPrincipalEAN.setLayout(pnlPrincipalEANLayout);
        pnlPrincipalEANLayout.setHorizontalGroup(
            pnlPrincipalEANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalEANLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPrincipalEANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1006, Short.MAX_VALUE)
                    .addGroup(pnlPrincipalEANLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(edtEAN, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnInserir)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlPrincipalEANLayout.setVerticalGroup(
            pnlPrincipalEANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrincipalEANLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPrincipalEANLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(edtEAN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnInserir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 626, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlTabulado.addTab("EAN", pnlPrincipalEAN);

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaHistCompra, tabelaHistCompra);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${numeroNf}"));
        columnBinding.setColumnName("Numero Nf");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${fornecedor}"));
        columnBinding.setColumnName("Fornecedor");
        columnBinding.setColumnClass(beans.Fornecedor.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${dataEmissao}"));
        columnBinding.setColumnName("Data Emissao");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${dataEntrada}"));
        columnBinding.setColumnName("Data Entrada");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${chaveAcesso}"));
        columnBinding.setColumnName("Chave Acesso");
        columnBinding.setColumnClass(String.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane3.setViewportView(tabelaHistCompra);

        javax.swing.GroupLayout pnlHistCompraLayout = new javax.swing.GroupLayout(pnlHistCompra);
        pnlHistCompra.setLayout(pnlHistCompraLayout);
        pnlHistCompraLayout.setHorizontalGroup(
            pnlHistCompraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1030, Short.MAX_VALUE)
        );
        pnlHistCompraLayout.setVerticalGroup(
            pnlHistCompraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 685, Short.MAX_VALUE)
        );

        pnlTabulado.addTab("Histórico de Compra", pnlHistCompra);

        jScrollPane6.setViewportView(pnlTabulado);

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnBuscarProduto.setText("Buscar");
        btnBuscarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProdutoActionPerformed(evt);
            }
        });

        btnNovoProduto.setText("Novo");
        btnNovoProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoProdutoActionPerformed(evt);
            }
        });

        btnEditProduto.setText("Alterar");
        btnEditProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditProdutoActionPerformed(evt);
            }
        });

        btnExcluirProduto.setText("Excluir");
        btnExcluirProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirProdutoActionPerformed(evt);
            }
        });

        btnCancProduto.setText("Cancelar");
        btnCancProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancProdutoActionPerformed(evt);
            }
        });

        btnSaveProduto.setText("Confirmar");
        btnSaveProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveProdutoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBotoesLayout = new javax.swing.GroupLayout(pnlBotoes);
        pnlBotoes.setLayout(pnlBotoesLayout);
        pnlBotoesLayout.setHorizontalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(btnBuscarProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNovoProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExcluirProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSaveProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlBotoesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBuscarProduto, btnCancProduto, btnEditProduto, btnExcluirProduto, btnNovoProduto, btnSair, btnSaveProduto});

        pnlBotoesLayout.setVerticalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addGroup(pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarProduto)
                    .addComponent(btnNovoProduto)
                    .addComponent(btnEditProduto)
                    .addComponent(btnExcluirProduto)
                    .addComponent(btnCancProduto)
                    .addComponent(btnSaveProduto)
                    .addComponent(btnSair))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6)
            .addComponent(pnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        if (alterado) {
            if (!Extra.Pergunta(this, "Sair sem salvar?")) {
                return;
            }
        }
        dispose();
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        try {
            setClosed(true);
        } catch (PropertyVetoException e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
	}//GEN-LAST:event_btnSairActionPerformed

    private void btnCancProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancProdutoActionPerformed
        cancelar();
    }//GEN-LAST:event_btnCancProdutoActionPerformed

    public void setAtivoButtonBuscar(boolean ativar) {
        this.btnBuscarProduto.setEnabled(ativar);
        this.btnBuscarProduto.setVisible(ativar);
    }

    public void executaButtonEditar() {
        btnEditProdutoActionPerformed(null);
    }

    private void btnNovoProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoProdutoActionPerformed
        cancelar();
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true);
        alterado = true;
        produto.clearProduto();
        lblCodigo.setText(String.valueOf(NextDaos.nextGenerico(Variaveis.getEmpresa(), "produto")));
        edtDescricao.requestFocus();
    }//GEN-LAST:event_btnNovoProdutoActionPerformed

    private void btnBuscarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProdutoActionPerformed
        FI_CadFindProduto frmProduto = (FI_CadFindProduto) Util.execucaoDiretaClass("formularios.FI_CadFindProduto", "Modulos-jSyscom.jar", "Localiza Produto - " + Util.getSource(seqInstancias, FI_CadFindProduto.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (frmProduto != null) {
            frmProduto.setAtivoButtonNovo(false);
            frmProduto.produto = produto;
        }
    }//GEN-LAST:event_btnBuscarProdutoActionPerformed

    private void btnEditProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditProdutoActionPerformed
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true);
        alterado = true;
        pnlTabulado.setEnabledAt(1, true);
        pnlTabulado.setEnabledAt(2, true);
        pnlTabulado.setEnabledAt(3, true);
    }//GEN-LAST:event_btnEditProdutoActionPerformed

    private Boolean salvar() {
        if (edtDescricao.getText().trim().equals("")) {
            Extra.Informacao(this, "Campo descrição é obrigatório!");
            return false;
        }
        if (cboxUnidade.getSelectedItem().toString().trim().equals("")) {
            Extra.Informacao(this, "Unidade é obrigatória!");
            return false;
        }
        if (cboxCategoria.getSelectedItem().toString().trim().equals("")) {
            Extra.Informacao(this, "Categoria é obrigatória!");
            return false;
        }
        if (cboxLocal.getSelectedItem().toString().trim().equals("")) {
            Extra.Informacao(this, "Local é obrigatório!");
            return false;
        }
        if (cboxLocal.getSelectedItem().toString().trim().equals("")) {
            Extra.Informacao(this, "Local é obrigatório!");
            return false;
        }
        if (iconProd.getIcon() == null) {
            Extra.Informacao(this, "Imagem é obrigatório!");
            return false;
        }
        try {
            populaProduto();
            if (ProdutoDao.salvarProduto(produto, Variaveis.getEmpresa())) {
                Imagem i = (Imagem) imagemProd.clone();
                setProduto();
                imagemProd = i;
                setImagemProd();
                return true;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
        return false;
    }

    private void btnSaveProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveProdutoActionPerformed
        if (imagemProd.getBlobImagem() != null) {
            if (imagemProd.getCodImagem() == null) {
                try {
                    Integer a = ProdutoDao.imagemProdInDB(Variaveis.getEmpresa(), imagemProd);
                    if (a != null) {
                        if (!Extra.Pergunta(this, "A imagem utilizada parece já existir no Banco de Dados, gostaria de salva-la mesmo assim?\n"
                                + "Para procura-la no banco de dados basta clicar em 'Imagem DB'")) {
                            return;
                        }
                    }
                    String qry = "INSERT INTO imagem_prod (cd_empresa, nome_imagem_prod, md5_imagem_prod, blob_imagem_prod) "
                            + "VALUES (" + Variaveis.getEmpresa().getCodigo() + ", '" + imagemProd.getNomeImagem() + "', '" + imagemProd.getMd5Imagem() + "', ?) RETURNING cd_imagem_prod";
                    imagemProd.setCodImagem(ImagemDao.setImagemBanco(qry, new File(nomeArq)));
                } catch (IOException ex) {
                    Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
                }
            }
        }
        if (salvar()) {
            ativaEdits(false);
            ativaButtons(true, true, true, true, true, false);
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
            alterado = false;
            Extra.Informacao(this, "Salvo com sucesso! \n"
                    + "Clique em 'Alterar' para adicionar o(s) EAN(s) e Fornecedor(es) do produto!");
        } else {
            Extra.Informacao(this, "Falha ao salvar produto!");
        }
    }//GEN-LAST:event_btnSaveProdutoActionPerformed

    private void btnExcluirProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirProdutoActionPerformed
        if (produto.getCodProduto() != null) {
            if (Extra.Pergunta(this, "Excluir registro atual?")) {
                if (ProdutoDao.excluirProduto(produto, Variaveis.getEmpresa())) {
                    alterado = false;
                    cancelar();
                    Extra.Informacao(this, "Registro apagado com sucesso!");
                } else {
                    Extra.Informacao(this, "Falha ao apagar registro!");
                }
            }
        } else {
            alterado = false;
            cancelar();
        }
    }//GEN-LAST:event_btnExcluirProdutoActionPerformed

    private void btnInserirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInserirActionPerformed
        if (!Validador.soNumeros(edtEAN.getText().trim()).equals("")) {
            Ean ean = new Ean();
            ean.setEan(Validador.soNumeros(edtEAN.getText().trim()));
            ean.setProduto(produto);
            if (!ProdutoDao.isBusyEan(ean, Variaveis.getEmpresa())) {
                listaEan.add(ean);
                edtEAN.setText("");
            } else {
                Extra.Informacao(this, "Este EAN já está em uso!");
            }
            edtEAN.requestFocus();
        }
    }//GEN-LAST:event_btnInserirActionPerformed

    private void menuRemoverSelEanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverSelEanActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todos os items selecionados?")) {
            int[] array = tabelaEan.getSelectedRows();
            ArrayList<Ean> v = new ArrayList<>();
            for (int i : array) {
                v.add(listaEan.get(i));
            }
            for (Ean ean : v) {
                listaEan.remove(ean);
            }
        }
    }//GEN-LAST:event_menuRemoverSelEanActionPerformed

    private void menuRemoverTodosEanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverTodosEanActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todos os items?")) {
            listaEan.clear();
        }
    }//GEN-LAST:event_menuRemoverTodosEanActionPerformed

    private void menuRemoverSelFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverSelFornecedorActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todos os items selecionados?")) {
            int[] array = tabelaFornecedor.getSelectedRows();
            ArrayList<Pessoa> v = new ArrayList<>();
            for (int i : array) {
                v.add(listaPessoaFornecedor.get(i));
            }
            for (Pessoa f : v) {
                listaPessoaFornecedor.remove(f);
            }
        }
    }//GEN-LAST:event_menuRemoverSelFornecedorActionPerformed

    private void menuRemoverTodosFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverTodosFornecedorActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todos os items?")) {
            listaPessoaFornecedor.clear();
        }
    }//GEN-LAST:event_menuRemoverTodosFornecedorActionPerformed

    private void btnBuscaFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaFornecedorActionPerformed
        FI_CadFindPessoa fornecedorForm = (FI_CadFindPessoa) Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", "Localiza Fornecedor - " + Util.getSource(numInstancias, FI_CadFindPessoa.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (fornecedorForm != null) {
            fornecedorForm.pessoa = fornecedor;
            fornecedorForm.listaExport = listaPessoaFornecedor;
            fornecedorForm.setTipoObrigatorioBusca(new Fornecedor());
        }
    }//GEN-LAST:event_btnBuscaFornecedorActionPerformed

    private void btnCarregaImagemPastaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaImagemPastaActionPerformed
        if (fc == null) {
            fc = new JFileChooser();
            fc.addChoosableFileFilter(new ImageFilter());
            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileView(new ImageFileView());
            fc.setAccessory(new ImagePreview(fc));
        }
        int returnVal = fc.showDialog(this, "Abrir");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                imagemProd.clearImagem();
                imagemProd.setBlobImagem((ImageIcon) Util.setImagen(fc.getSelectedFile().toString(), iconProd.getWidth(), -1));
                imagemProd.setMd5Imagem(Arquivo.getMD5Checksum(fc.getSelectedFile().toString())); //Unable to load library 'BEMAFI32': libBEMAFI32.so: cannot open shared object file
                imagemProd.setNomeImagem(fc.getSelectedFile().getName());
                nomeArq = fc.getSelectedFile().toString();
                Integer a = ProdutoDao.imagemProdInDB(Variaveis.getEmpresa(), imagemProd);
                if (a != null) {
                    if (Extra.Pergunta(this, "Esta imagem parece já existir no Banco de Dados, gostaria de usar a que esta no banco?")) {
                        nomeArq = null;
                        Imagem i = ProdutoDao.getImagemProduto(Variaveis.getEmpresa(), new Imagem(a));
                        imagemProd.clearImagem();
                        imagemProd.setCodImagem(i.getCodImagem());
                        imagemProd.setBlobImagem(i.getBlobImagem());
                        imagemProd.setMd5Imagem(i.getMd5Imagem());
                        imagemProd.setNomeImagem(i.getNomeImagem());
                    }
                }
                imagemProd.setFlagPronto(true);
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        }
    }//GEN-LAST:event_btnCarregaImagemPastaActionPerformed

    private void limpaImagem() {
        iconProd.setIcon(null);
        nomeArq = null;
        if (fc != null) {
            fc.setSelectedFile(null);
        }
        if (imagemProd != null) {
            imagemProd.clearImagem();
        }
    }

    private void btnLimparImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparImagemActionPerformed
        limpaImagem();
    }//GEN-LAST:event_btnLimparImagemActionPerformed

    private void edtEANKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtEANKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnInserirActionPerformed(null);
        }
    }//GEN-LAST:event_edtEANKeyPressed

    private void btnCarregaImagemBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaImagemBDActionPerformed
        FI_SysGaleryImages galery = (FI_SysGaleryImages) Util.execucaoDiretaClass("formularios.FI_SysGaleryImages", "Modulos-jSyscom.jar", "Localiza Imagens - " + Util.getSource(seqInstancias, FI_SysGaleryImages.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (galery != null) {
            galery.setWidthMaxExport(256);
            galery.setSqlDefault("select cd_imagem_prod as cod, blob_imagem_prod as blob, md5_imagem_prod as md5, nome_imagem_prod as nome from imagem_prod where cd_empresa = " + Variaveis.getEmpresa().getCodigo());
            galery.exportImagem = imagemProd;
        }
    }//GEN-LAST:event_btnCarregaImagemBDActionPerformed

    private void edtPrecoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtPrecoKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtPrecoKeyReleased

    private void edtEstMinKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtEstMinKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtEstMinKeyReleased

    private void btnNovaUnidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaUnidadeActionPerformed
        FI_CadUnidade o = (FI_CadUnidade) Util.execucaoDiretaClass("formularios.FI_CadUnidade", "Modulos-jSyscom.jar", "Cadastro de Unidades - " + Util.getSource(seqInstancias, FI_CadUnidade.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.unidade = unidade;
        }
    }//GEN-LAST:event_btnNovaUnidadeActionPerformed

    private void btnNovaCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaCategoriaActionPerformed
        FI_CadCategoriaProd o = (FI_CadCategoriaProd) Util.execucaoDiretaClass("formularios.FI_CadCategoriaProd", "Modulos-jSyscom.jar", "Cadastro de Categorias - " + Util.getSource(seqInstancias, FI_CadCategoriaProd.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.categoriaProd = categoriaProd;
        }
    }//GEN-LAST:event_btnNovaCategoriaActionPerformed

    private void btnNovaLocalizacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaLocalizacaoActionPerformed
        FI_CadLocalizacao o = (FI_CadLocalizacao) Util.execucaoDiretaClass("formularios.FI_CadLocalizacao", "Modulos-jSyscom.jar", "Cadastro de Locais/Gôndolas - " + Util.getSource(seqInstancias, FI_CadLocalizacao.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.localProd = localProd;
        }
    }//GEN-LAST:event_btnNovaLocalizacaoActionPerformed

    private void cboxCategoriaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboxCategoriaItemStateChanged
        try {
            if (cboxCategoria.getSelectedItem() instanceof CategoriaProd) {
                Double marg = ((CategoriaProd) cboxCategoria.getSelectedItem()).getMargemCategoria();
                lblMargemC.setText(Format.FormataValor(Format.doubleToString(marg), Variaveis.getDecimal()));
                lblPrecoFinal.setText(Format.FormataValor(Format.doubleToString(produto.getValorCompraProduto() * (marg / 100)), Variaveis.getDecimal()));
            }
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_cboxCategoriaItemStateChanged

    private void edtDescricaoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtDescricaoKeyReleased
    }//GEN-LAST:event_edtDescricaoKeyReleased

    private void edtDescricaoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_edtDescricaoFocusLost
        edtDescricao.setText(edtDescricao.getText().toUpperCase());
    }//GEN-LAST:event_edtDescricaoFocusLost

    private void edtQtdEstoqueKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtQtdEstoqueKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtQtdEstoqueKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscaFornecedor;
    private javax.swing.JButton btnBuscarProduto;
    private javax.swing.JButton btnCancProduto;
    private javax.swing.JButton btnCarregaImagemBD;
    private javax.swing.JButton btnCarregaImagemPasta;
    private javax.swing.JButton btnEditProduto;
    private javax.swing.JButton btnExcluirProduto;
    private javax.swing.JButton btnInserir;
    private javax.swing.JButton btnLimparImagem;
    private javax.swing.JButton btnNovaCategoria;
    private javax.swing.JButton btnNovaLocalizacao;
    private javax.swing.JButton btnNovaUnidade;
    private javax.swing.JButton btnNovoProduto;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSaveProduto;
    private javax.swing.ButtonGroup buttonGroup1;
    private beans.CategoriaProd categoriaProd;
    private javax.swing.JComboBox cboxCategoria;
    private javax.swing.JComboBox cboxLocal;
    private javax.swing.JComboBox cboxSituacao;
    private javax.swing.JComboBox cboxUnidade;
    private javax.swing.JFormattedTextField edtDataCad;
    private javax.swing.JTextField edtDescricao;
    private javax.swing.JTextArea edtDetalhes;
    private javax.swing.JTextField edtEAN;
    private javax.swing.JTextField edtEstMin;
    private javax.swing.JTextField edtPreco;
    private javax.swing.JTextField edtQtdEstoque;
    private beans.Fornecedor fornecedor;
    private javax.swing.JLabel iconProd;
    private beans.Imagem imagemProd;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblDataNasc;
    private javax.swing.JLabel lblMargemC;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPrecoFinal;
    private javax.swing.JLabel lblTF;
    private java.util.List<Ean> listaEan;
    private java.util.List<NFEntrada> listaHistCompra;
    private java.util.List<Pessoa> listaPessoaFornecedor;
    private beans.LocalProd localProd;
    private javax.swing.JMenuItem menuRemoverSelEan;
    private javax.swing.JMenuItem menuRemoverSelFornecedor;
    private javax.swing.JMenuItem menuRemoverTodosEan;
    private javax.swing.JMenuItem menuRemoverTodosFornecedor;
    private javax.swing.JPanel pnlBotoes;
    private javax.swing.JPanel pnlCodPessoa;
    private javax.swing.JPanel pnlDemonstrativo;
    private javax.swing.JPanel pnlHistCompra;
    private javax.swing.JPanel pnlInfoBasicas;
    private javax.swing.JPanel pnlPrincipalEAN;
    private javax.swing.JPanel pnlPrincipalFornecedor;
    private javax.swing.JPanel pnlPrincipalProduto;
    private javax.swing.JTabbedPane pnlTabulado;
    private javax.swing.JPanel pnlTipoPessoa;
    private javax.swing.JPopupMenu popupEan;
    private javax.swing.JPopupMenu popupFornecedor;
    public beans.Produto produto;
    private javax.swing.JTable tabelaEan;
    private javax.swing.JTable tabelaFornecedor;
    private javax.swing.JTable tabelaHistCompra;
    private beans.Unidade unidade;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Produto) {
            limpaCampos();
            ativaEdits(false);
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
            ativaButtons(true, true, true, true, true, false);
            alterado = false;
            setProduto();
        } else if (o instanceof Imagem) {
            setImagemProd();
        } else if (o instanceof Unidade) {
            setUnidade();
        } else if (o instanceof CategoriaProd) {
            setCategoria();
        } else if (o instanceof LocalProd) {
            setLocalizacao();
        }
    }
}
