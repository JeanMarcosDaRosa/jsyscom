/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Jean
 */
public class Conexao implements Serializable {

    public static final int FIREBIRD    = 3050;
    public static final int MYSQL       = 3306;
    public static final int POSTGRES    = 5432;
    private String driver = "org.postgresql.Driver"; //Local do Driver
    private String usuario = "postgres";
    private String senha = "nobug95";
    private String ip = "localhost"; 
    private Integer port = 0; 
    private String localDB = "";
    private Integer TYPE_DB = 0;
    private Connection con;

    private String getUrl(Integer TYPE_DB, String IpServ, String LocalBanco, Integer Port) {
        String url = "";
        if (TYPE_DB.equals(FIREBIRD)) {
            driver = "org.firebirdsql.jdbc.FBDriver";
            url = "jdbc:firebirdsql://" + IpServ + ":" + Port + "/" + LocalBanco + "?lc_ctype=ISO8859_1";//Port 3050
        } else if (TYPE_DB.equals(MYSQL)) {
            driver = "com.mysql.jdbc.Driver";
            url = "jdbc:mysql://" + IpServ + ":" + Port + "/" + LocalBanco;//Port 3306
        } else if (TYPE_DB.equals(POSTGRES)) {
            driver = "org.postgresql.Driver";
            url = "jdbc:postgresql://" + IpServ + ":" + Port + "/" + LocalBanco;//Port 5432
        }
        return url;
    }
    
    private void conecta(String ip, Integer port, String localDB, Integer TYPE_DB){
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(getUrl(TYPE_DB, ip, localDB, port), usuario, senha);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PreparedStatement getPreparedStatement(String sql, String ip, Integer port, String localDB, String user, String pass, Integer TYPE_DB) throws SQLException {
        if(!(ip.equals(this.ip)&&(port.equals(this.port)&&(localDB.equals(this.localDB)&&(user.equals(this.usuario))&&(pass.equals(this.senha))&&(!con.isClosed()))))){
            this.ip         = ip;
            this.port       = port;
            this.localDB    = localDB;
            this.usuario    = user;
            this.senha      = pass;
            this.TYPE_DB    = TYPE_DB;
            conecta(this.ip, this.port, this.localDB, this.TYPE_DB);
        }
        return con.prepareStatement(sql);
    }

    public Connection getCon(String ip, Integer port, String localDB, String user, String pass, Integer TYPE_DB) throws SQLException {
        if(!(ip.equals(this.ip)&&(port.equals(this.port)&&(localDB.equals(this.localDB)&&(user.equals(this.usuario))&&(pass.equals(this.senha))&&(!con.isClosed()))))){
            this.ip         = ip;
            this.port       = port;
            this.localDB    = localDB;
            this.usuario    = user;
            this.senha      = pass;
            this.TYPE_DB    = TYPE_DB;
            conecta(this.ip, this.port, this.localDB, this.TYPE_DB);
        }
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public Conexao() {
    }

    public static Conexao getInstance() {
        return ConexaoHolder.INSTANCE;
    }

    private static class ConexaoHolder {

        private static final Conexao INSTANCE = new Conexao();
    }

    public static Connection conecta(String IpServ, Integer Port, String LocalBanco, String user, String password, Integer TYPE_DB) {
        try {
            String classe = "";
            String url = "";
            if (TYPE_DB.equals(FIREBIRD)) {
                classe = "org.firebirdsql.jdbc.FBDriver";
                url = "jdbc:firebirdsql:" + IpServ + "/" + Port + ":" + LocalBanco + "?lc_ctype=ISO8859_1";//Port 3050
            } else if (TYPE_DB.equals(MYSQL)) {
                classe = "com.mysql.jdbc.Driver";
                url = "jdbc:mysql://" + IpServ + ":" + Port + "/" + LocalBanco;//Port 3306
            } else if (TYPE_DB.equals(POSTGRES)) {
                classe = "org.postgresql.Driver";
                url = "jdbc:postgresql://" + IpServ + ":" + Port + "/" + LocalBanco;//Port 5432
            }
            Class.forName(classe);
            return DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException Driver) {
            JOptionPane.showMessageDialog(null, "Driver não Localizado: " + Driver, "Informação", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException Fonte) {
            JOptionPane.showMessageDialog(null, "Erro de Conexão: " + Fonte, "Informação", JOptionPane.INFORMATION_MESSAGE);
        }
        return null;
    }
}
