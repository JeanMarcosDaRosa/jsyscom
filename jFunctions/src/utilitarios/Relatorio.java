/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

/*
 * @(#)Relatorio.java   1.0 28/04/2006
 *
 * Copyright 2006 Marcos Vinícius Soares. Todos os direitos reservados. 
 * mvsoares@estadao.com.br
 *
 */
import java.io.InputStream;
import java.net.URL;
import java.sql.*;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 * Relatorio.java&lt;br&gt;
 *
 * &lt;p&gt;A classe Relatorio deve ser utilizada para emissão de relatórios.
 * Após a criação de algum relatório é possível exibí-lo na tela, exportá-lo
 * para um arquivo pdf/html ou enviá-lo direto para impressão.&lt;/p&gt;
 *
 * &lt;p&gt;A classe manipula relatórios desenvolvidos utilizando-se a
 * ferramenta iReport e utiliza a ferramenta JasperReports para emissão dos
 * relatórios&lt;/p&gt;
 *
 * @author Marcos Vinícius Soares
 */
public class Relatorio {

    /**
     * Representa o relatório gerado.
     */
    private JasperPrint jasperPrint_;

    /**
     * Cria um novo Relatorio.
     *
     * @param conn Conexão com o banco de dados.
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     */
    public Relatorio(Connection conn, HashMap parameters, URL localRelatorio) throws JRException {
        try {
            // O objeto JasperReport representa o objeto JasperDesign (arquivo .jrxml) compilado.
            // Ou seja, o arquivo .jasper
            JasperReport jr = (JasperReport) JRLoader.loadObject(localRelatorio);
            // JasperPrint representa o relatório gerado.
            // É criado um JasperPrint a partir de um JasperReport, contendo o relatório preenchido.
            this.jasperPrint_ = JasperFillManager.fillReport(jr, parameters, conn);

        } catch (JRException e) {
            throw e;
        }
    }

    /**
     * Cria um novo Relatorio.
     *
     * @param conn Conexão com o banco de dados.
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     */
    public Relatorio(Connection conn, HashMap parameters, String localRelatorio) throws JRException {
        try {
            // O objeto JasperReport representa o objeto JasperDesign (arquivo .jrxml) compilado.
            // Ou seja, o arquivo .jasper
            JasperReport jr = (JasperReport) JRLoader.loadObject(localRelatorio);
            // JasperPrint representa o relatório gerado.
            // É criado um JasperPrint a partir de um JasperReport, contendo o relatório preenchido.
            this.jasperPrint_ = JasperFillManager.fillReport(jr, parameters, conn);
        } catch (JRException e) {
            throw e;
        }
    }
    
    
    /**
     * Cria um novo Relatorio.
     *
     * @param conn Conexão com o banco de dados.
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     */
    public Relatorio(Connection conn, HashMap parameters, InputStream localRelatorio) throws JRException {
        try {
            this.jasperPrint_ = JasperFillManager.fillReport(localRelatorio, parameters, conn);
        } catch (JRException e) {
            throw e;
        }
    }

    /**
     * Cria um novo Relatorio.
     *
     * @param conn Conexão com o banco de dados.
     * @param sql Expressão SQL (SELECT...) a ser utilizada para preenchimento
     * do relatório
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     * @throws SQLException Caso exista alguma divergência ou problema com a
     * Expressão SQL passada como parâmetro, uma exceção é gerada.
     */
    public Relatorio(Connection conn, String sql,
            HashMap parameters, URL localRelatorio) throws SQLException, JRException {
        try {
            // O objeto JasperReport representa o objeto JasperDesign (arquivo .jrxml) compilado.
            // Ou seja, o arquivo .jasper
            JasperReport jr = (JasperReport) JRLoader.loadObject(localRelatorio);
            // Resultado da consulta
            ResultSet rs = conn.prepareStatement(sql).executeQuery();
            // JRResultSetDataSource é uma implementaçao de JRDataSource, o qual é requerido
            // como parametro para preencher o relatório criado.
            // Ele armazena o dados do ResultSet
            JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
            // JasperPrint representa o relatório gerado.
            // É criado um JasperPrint a partir de um JasperReport, contendo o
            // relatório preenchido.
            this.jasperPrint_ = JasperFillManager.fillReport(jr, parameters, jrRS);
            rs.close();
        } catch (SQLException e) {
            throw e;
        } catch (JRException e) {
            throw e;
        }
    }

    /**
     * Cria um novo Relatorio.
     *
     * @param conn Conexão com o banco de dados.
     * @param sql Expressão SQL (SELECT...) a ser utilizada para preenchimento
     * do relatório
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     * @throws SQLException Caso exista alguma divergência ou problema com a
     * Expressão SQL passada como parâmetro, uma exceção é gerada.
     */
    public Relatorio(Connection conn, String sql,
            HashMap parameters, String localRelatorio) throws SQLException, JRException {
        try {
            // O objeto JasperReport representa o objeto JasperDesign (arquivo .jrxml) compilado.
            // Ou seja, o arquivo .jasper
            JasperReport jr = (JasperReport) JRLoader.loadObject(localRelatorio);
            // Resultado da consulta
            ResultSet rs = conn.prepareStatement(sql).executeQuery();
            // JRResultSetDataSource é uma implementaçao de JRDataSource, o qual é requerido
            // como parametro para preencher o relatório criado.
            // Ele armazena o dados do ResultSet
            JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
            // JasperPrint representa o relatório gerado.
            // É criado um JasperPrint a partir de um JasperReport, contendo o
            // relatório preenchido.
            this.jasperPrint_ = JasperFillManager.fillReport(jr, parameters, jrRS);
            rs.close();
        } catch (SQLException e) {
            throw e;
        } catch (JRException e) {
            throw e;
        }
    }

    /**
     * Cria um novo Relatorio.
     *
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     */
    public Relatorio(HashMap parameters, URL localRelatorio) throws JRException {
        try {
            // O objeto JasperReport representa o objeto JasperDesign (arquivo .jrxml) compilado.
            // Ou seja, o arquivo .jasper
            JasperReport jr = (JasperReport) JRLoader.loadObject(localRelatorio);

            // JREmptyDataSource é uma implementaçao de JRDataSource, o qual é requerido
            // como parametro para preencher o relatório criado.
            // Ele armazena o dados do ResultSet, que, neste caso, é vazio
            JREmptyDataSource jrEDS = new JREmptyDataSource();

            // Jasper Print representa o relatório gerado.
            // É criado um JasperPrint a partir de um JasperReport, contendo o
            // relatório preenchido.
            this.jasperPrint_ = JasperFillManager.fillReport(jr, parameters, jrEDS);
        } catch (JRException e) {
            throw e;
        }
    }

    /**
     * Cria um novo Relatorio
     *
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     */
    public Relatorio(HashMap parameters, String localRelatorio) throws JRException {
        try {
            // O objeto JasperReport representa o objeto JasperDesign (arquivo .jrxml) compilado.
            // Ou seja, o arquivo .jasper
            JasperReport jr = (JasperReport) JRLoader.loadObject(localRelatorio);

            // JREmptyDataSource é uma implementaçao de JRDataSource, o qual é requerido
            // como parametro para preencher o relatório criado.
            // Ele armazena o dados do ResultSet, que, neste caso, é vazio
            JREmptyDataSource jrEDS = new JREmptyDataSource();

            // Jasper Print representa o relatório gerado.
            // É criado um JasperPrint a partir de um JasperReport, contendo o
            // relatório preenchido.
            this.jasperPrint_ = JasperFillManager.fillReport(jr, parameters, jrEDS);

        } catch (JRException e) {
            throw e;
        }
    }

    
    /**
     * Cria um novo Relatorio
     *
     * @param lista Lista de Objetos a serem exibidos no relatório.
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     */
    public Relatorio(List<Object> lista, HashMap parameters, String localRelatorio) throws JRException {
        try {
            // O objeto JasperReport representa o objeto JasperDesign (arquivo .jrxml) compilado.
            // Ou seja, o arquivo .jasper
            JasperReport jr = (JasperReport) JRLoader.loadObject(localRelatorio);
            JRBeanCollectionDataSource jrBCDS = new JRBeanCollectionDataSource(lista);
            // Jasper Print representa o relatório gerado.
            // É criado um JasperPrint a partir de um JasperReport, contendo o
            // relatório preenchido.
            this.jasperPrint_ = JasperFillManager.fillReport(jr, parameters, jrBCDS);

        } catch (JRException e) {
            throw e;
        }
    }
    
    
    /**
     * Cria um novo Relatorio
     *
     * @param lista Lista de Objetos a serem exibidos no relatório.
     * @param parameters Parâmetros a serem exibidos no relatório.
     * @param localRelatorio Localização do relatório.
     * @throws JRException Caso o relatório não seja encontrado ou haja algum
     * problema com ele, uma exceção é gerada.
     */
    public Relatorio(List<Object> lista, HashMap parameters, InputStream localRelatorio) throws JRException {
        try {
            JRBeanCollectionDataSource jrBCDS = new JRBeanCollectionDataSource(lista);
            this.jasperPrint_ = JasperFillManager.fillReport(localRelatorio, parameters, jrBCDS);
        } catch (JRException e) {
            throw e;
        }
    }
    
    
    /**
     * Exibe o relatório na tela.
     */
    public void exibirRelatorio() {
        // emite o relatório na tela
        // false indica que a aplicação não será finalizada caso o relatório seja fechado
        JasperViewer.viewReport(this.jasperPrint_, false);
    }

    /**
     * Grava o relatório em um arquivo de formato pdf.
     *
     * @param caminhoDestino Caminho onde o arquivo será gravado.
     */
    public void exportaParaPdf(String caminhoDestino) throws JRException {
        try {
            // Gera o arquivo PDF
            JasperExportManager.exportReportToPdfFile(this.jasperPrint_, caminhoDestino);
        } catch (JRException e) {
            throw e;
        }
    }

    /**
     * Grava o relatório em um arquivo de formato html.
     *
     * @param caminhoDestino Caminho onde o arquivo será gravado.
     */
    public void exportaParaHtml(String caminhoDestino) throws JRException {
        try {
            // Gera o arquivo PDF
            JasperExportManager.exportReportToHtmlFile(this.jasperPrint_, caminhoDestino);
        } catch (JRException e) {
            throw e;
        }
    }

    /**
     * Envia o relatório para impressão, exibindo uma caixa de dialogo de
     * impressão ou não.
     *
     * @param exibeCaixaDialogo Boolean indicando se será exibida uma caixa de
     * diálogo ou não.
     */
    public void imprimir(boolean exibeCaixaDialogo) throws JRException {
        try {
            // Imprime o relatório
            // o segundo parâmetro indica se existirá uma caixa de dialogo antes ou nao
            JasperPrintManager.printReport(this.jasperPrint_, exibeCaixaDialogo);
        } catch (JRException e) {
            throw e;
        }

    }
}
