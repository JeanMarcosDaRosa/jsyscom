/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

import beans.ResultSetTableModel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

/**
 *
 * @author Jean
 */
public abstract class Format {

    public static final int LOWER = 21;
    public static final int UPPER = 22;
    public static final int BINARY = 10;
    public static final int OCTAL = 11;
    public static final int DECIMAL = 12;
    public static final int HEXADECIMAL = 13;

    public static String ponto(String Vlr) {
        StringBuilder buf = new StringBuilder();
        if ("".equals(Vlr)) {
            Vlr = "0";
        }
        Vlr = String.valueOf(Integer.parseInt(Vlr));
        if (Vlr.length() >= 3) {
            //int qt = ((Vlr.length() - 2) / 3);
            char[] temp = Vlr.toCharArray();
            for (int i = 0; i < Vlr.length(); i++) {
                if (i == (Vlr.length() - 2)) {
                    buf.append(".").append(temp[i]);
                } else {
                    buf.append(temp[i]);
                }
            }
        } else {
            if (Vlr.length() == 1) {
                return "0.0" + Vlr;
            }
            if (Vlr.length() == 2) {
                return "0." + Vlr;
            }
        }
        return buf.toString();
    }

    public static String[] StringToArray(String s, String sep) {
        // convert a String s to an Array, the elements
        // are delimited by sep
        // NOTE : for old JDK only (<1.4).
        //        for JDK 1.4 +, use String.split() instead
        StringBuffer buf = new StringBuffer(s);
        int arraysize = 1;
        for (int i = 0; i < buf.length(); i++) {
            if (sep.indexOf(buf.charAt(i)) != -1) {
                arraysize++;
            }
        }
        String[] elements = new String[arraysize];
        int y, z = 0;
        if (buf.toString().indexOf(sep) != -1) {
            while (buf.length() > 0) {
                if (buf.toString().indexOf(sep) != -1) {
                    y = buf.toString().indexOf(sep);
                    if (y != buf.toString().lastIndexOf(sep)) {
                        elements[z] = buf.toString().substring(0, y);
                        z++;
                        buf.delete(0, y + 1);
                    } else if (buf.toString().lastIndexOf(sep) == y) {
                        elements[z] = buf.toString().substring(0, buf.toString().indexOf(sep));
                        z++;
                        buf.delete(0, buf.toString().indexOf(sep) + 1);
                        elements[z] = buf.toString();
                        z++;
                        buf.delete(0, buf.length());
                    }
                }
            }
        } else {
            elements[0] = buf.toString();
        }
        buf = null;
        return elements;
    }

    public static String getCepFormatted(Integer cep) {
        String parte1, parte2;
        if (cep.toString().length() < 8) {
            //primeiro numero =0
            parte1 = "0".concat(Integer.toString(cep).substring(0, 4));
            parte2 = Integer.toString(cep).substring(4);

        } else {
            //primeiro numero !=0
            parte1 = Integer.toString(cep).substring(0, 5);
            parte2 = Integer.toString(cep).substring(5);
        }

        return parte1.concat("-").concat(parte2);
    }

    public static String getF(String it) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < it.length(); i++) {
            if (it.charAt(i) == ',') {
                buf.append(".");
            } else {
                buf.append(it.charAt(i));
            }
        }
        return buf.toString();
    }

    public static String getF2(String it) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < it.length(); i++) {
            if (it.charAt(i) == '.') {
                buf.append(",");
            } else {
                buf.append(it.charAt(i));
            }
        }
        return buf.toString();
    }

    public static String RemoveUltimosChar(String texto, Integer qtde) {
        if (texto.length() > 0) {
            texto = texto.substring(0, texto.length() - qtde);
        }
        return texto;
    }

    public static String RetornaSomenteNumero(String valor) {
        StringBuilder buf = new StringBuilder();
        char[] c = valor.trim().toCharArray();
        for (int i = 0; i < c.length; i++) {
            if ((c[i] >= 0) && (c[i] <= 9)) {
                buf.append(c[i]);
            }
        }
        return buf.toString();
    }

    public static String LimpaString(String passa) {
        passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
        passa = passa.replaceAll("[âãàáä]", "a");
        passa = passa.replaceAll("[ÊÈÉË]", "E");
        passa = passa.replaceAll("[êèéë]", "e");
        passa = passa.replaceAll("[ÎÍÌÏ]", "I");
        passa = passa.replaceAll("[îíìï]", "i");
        passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
        passa = passa.replaceAll("[ôõòóö]", "o");
        passa = passa.replaceAll("[ÛÙÚÜ]", "U");
        passa = passa.replaceAll("[ûúùü]", "u");
        passa = passa.replaceAll("Ç", "C");
        passa = passa.replaceAll("ç", "c");
        passa = passa.replaceAll("[ýÿ]", "y");
        passa = passa.replaceAll("Ý", "Y");
        passa = passa.replaceAll("ñ", "n");
        passa = passa.replaceAll("Ñ", "N");
        passa = passa.replaceAll("['<>\\|/]", "");
        return passa;
    }

    //Substituindo um caractere em uma posição especificada:
    public static String substituiCharPosicao(String s, int pos, char c) {
        return s.substring(0, pos) + c + s.substring(pos + 1);
    }

//Removendo um caractere:
    public static String removeChar(String s, char c) {

        String r = "";

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c) {
                r += s.charAt(i);
            }
        }

        return r;
    }

//Removendo um caractere em determinada posição:
    public static String removeCharAt(String s, int pos) {
        return s.substring(0, pos) + s.substring(pos + 1);
    }

    public static String ArrayToString(char[] array) {
        String R = "";
        for (int i = 0; i < array.length; i++) {
            R += array[i];
        }
        return R;
    }

    public static char UpperLowerCaseChar(char caracter, Integer tipo) {
        if (tipo.equals(LOWER)) {
            Character.toLowerCase(caracter);
        } else {
            if (tipo.equals(UPPER)) {
                Character.toUpperCase(caracter);
            }
        }
        return caracter;
    }

    public static String RemoveCaracteres(String texto, String caracteres) {
        StringBuilder buf = new StringBuilder();
        if (texto.length() > 0) {
            for (int i = 0; i < texto.length(); i++) {
                boolean fnd = false;
                for (int a = 0; a < caracteres.length(); a++) {
                    if (texto.charAt(i) == caracteres.charAt(a)) {
                        fnd = true;
                        break;
                    }
                }
                if (!fnd) {
                    buf.append(texto.charAt(i));
                }
            }
        }
        return buf.toString();
    }

    public static String pegaTexto(String texto, int chInicial, int chFinal) {
        StringBuilder buf = new StringBuilder();
        char[] t = texto.toCharArray();
        for (int i = chInicial; i <= chFinal; i++) {
            if (t.length > i) {
                buf.append(t[i]);
            }
        }
        return buf.toString();
    }

    public static String Completa_Esquerda(String texto, String caracter, Integer tamanho) {
        if (texto.length() > tamanho) {
            texto = texto.substring(0, tamanho);
        }
        for (int i = texto.length(); i < tamanho; i++) {
            texto = caracter + texto;
        }
        return texto;
    }

    public static String Completa_Direita(String texto, String caracter, Integer tamanho) {
        if (texto.length() > tamanho) {
            texto = texto.substring(0, tamanho);
        }
        StringBuilder buf = new StringBuilder();
        for (int i = texto.length(); i < tamanho; i++) {
            buf.append(caracter);
        }
        return texto;
    }

    public static TableModel getResultSetTableModal(ResultSet rs) {
        try {
            return (new ResultSetTableModel(rs));
        } catch (SQLException ex) {
            Logger.getLogger(Extra.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Extra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static java.sql.Date StrToDate(String data) throws Exception {
        if (data == null || data.equals("")) {
            return null;
        }
        java.sql.Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            date = new java.sql.Date(((java.util.Date) formatter.parse(data)).getTime());
        } catch (ParseException e) {
            throw e;
        }
        return date;
    }

    public static String DateToStr(Date dtData) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date today = dtData;
        return df.format(today);
    }

    public static String dtos(Date dt) {
        SimpleDateFormat Form = new SimpleDateFormat("dd/MM/yyyy");
        return Form.format(dt);
    }

    public static String dtos2(Date dt) {
        SimpleDateFormat Form = new SimpleDateFormat("yyyyMMdd");
        return Form.format(dt);
    }

    public static String dtos3(Date dt) {
        SimpleDateFormat Form = new SimpleDateFormat("yyyymmdd");
        return Form.format(dt);
    }

    public static String decConverter(int decimalToConvert, int base)
            throws IllegalArgumentException {
        String result;
        if (base == BINARY) {
            result = Integer.toBinaryString(decimalToConvert);
        } else if (base == OCTAL) {
            result = Integer.toOctalString(decimalToConvert);
        } else if (base == DECIMAL) {
            result = "" + decimalToConvert;
        } else if (base == HEXADECIMAL) {
            result = Integer.toHexString(decimalToConvert);
        } else {
            throw new IllegalArgumentException(
                    "A base deve ser Binaria, Octal, Decimal ou Hexadecimal!");
        }
        return result;
    }

    public static void formatTextField(JTextField field, int decimal, boolean multiplePoints) throws Exception {
        if (field != null && decimal > 0) {
            String str = field.getText().trim();
            if (!str.equals("")) {
                str = Validador.soNumeros(str);
                if (str.length() > decimal) {
                    str = str.substring(0, (str.length() - decimal)) + "," + str.substring((str.length() - decimal), str.length());
                } else {
                    str = "0," + Format.Completa_Esquerda(str, "0", decimal);
                    str = str.replace(".", ",");
                }
                str = Format.doubleToString(Format.stringToDouble(str));
                str = FormataValor(str, decimal);
                if (!multiplePoints) {
                    str = str.replace(".", "");
                }
                if (str.trim().equals("0")) {
                    str = "0," + Format.Completa_Esquerda(str, "0", decimal);
                }
                field.setText(str);
            } else {
                field.setText("0," + Format.Completa_Direita(str, "0", decimal + 1));
            }
        }
    }

    public static String FormataValor(String Vlr, int decimais) {
        StringBuilder buf = new StringBuilder();
        buf.append("0.");
        for (int i = 0; i < decimais; i++) {
            buf.append("0");
        }
        if (Vlr.contains(".") && Vlr.contains(".")) {
            while (Vlr.contains(".")) {
                Vlr = Vlr.replace(".", "");
            }
        }
        Vlr = Vlr.replace(",", ".");
        DecimalFormat valor = new DecimalFormat(buf.toString());
        double aux = Double.parseDouble(Vlr);
        return valor.format(aux);
    }

    public static long DateToLong(Date date) {
        long longDate = 0;
        try {
            longDate = date.getTime();
        } catch (Exception e) {
            System.out.println("Exception :" + e);
        }
        return longDate;
    }

    public static Date SubtraiData(Date hoje, int menos) {
        Calendar calendarData = Calendar.getInstance();
        calendarData.setTime(hoje);
        calendarData.add(Calendar.DATE, menos);
        Date dataInicial = calendarData.getTime();
        return dataInicial;
    }

    public static String getDate() {
        DateFormat dateFormats2 = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        return dateFormats2.format(calendar.getTime());
    }

    public static String getTime() {
        DateFormat dateFormats2 = new SimpleDateFormat("hh:mm:ss");
        Calendar calendar = Calendar.getInstance();
        return dateFormats2.format(calendar.getTime());
    }

    public static String getDate2() {
        DateFormat dateFormats2 = new SimpleDateFormat("ddMMyyyy");
        Calendar calendar = Calendar.getInstance();
        return dateFormats2.format(calendar.getTime());
    }

    public static Date addDays(Date date, int days) {
        java.util.Calendar cal = new java.util.GregorianCalendar(TimeZone.getTimeZone("GMT-3:00"));
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static java.sql.Date ConverteDataBema(String sd) {
        String aux = "";
        java.sql.Date data = new java.sql.Date(System.currentTimeMillis());
        DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
        if ("000000".equals(sd) || "00000000".equals(sd)) {
            return data;
        }
        char[] c = sd.toCharArray();
        if (sd.length() == 6) {
            aux = "";
            aux += c[0];
            aux += c[1];
            aux += "/";
            aux += c[2];
            aux += c[3];
            aux += "/20";
            aux += c[4];
            aux += c[5];
            try {
                data = new java.sql.Date(fmt.parse(aux).getTime());
            } catch (ParseException ex) {
                Logger.getLogger(Extra.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (sd.length() == 8) {
                aux = "";
                aux += c[0];
                aux += c[1];
                aux += "/";
                aux += c[2];
                aux += c[3];
                aux += "/";
                aux += c[4];
                aux += c[5];
                aux += c[6];
                aux += c[7];
                try {
                    data = new java.sql.Date(fmt.parse(aux).getTime());
                } catch (ParseException ex) {
                    Logger.getLogger(Extra.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return data;
    }

    public static String format(double x) {
        /*
         * retorna o valor como string formatado com 2 casas após a virgula
         */
        return String.format("%.2f", x);
    }

    /**
     * Método para converter uma string para inteiro
     */
    public static Integer stringToInteger(String s) throws Exception {
        return Integer.parseInt(s);
    }

    /**
     * Método para testar se uma string pode ser convertida para inteiro
     */
    public static Boolean isInt(String s) {
        try {
            stringToInteger(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Método para converter string para double
     */
    public static Double stringToDouble(String s) throws Exception {
        NumberFormat nf = NumberFormat.getInstance();
        return nf.parse(s).doubleValue();
        //return Double.parseDouble(s);
    }

    /**
     * Método para converter string para float
     */
    public static Float stringToFloat(String s) throws Exception {
        NumberFormat nf = NumberFormat.getInstance();
        return nf.parse(s).floatValue();
    }

    /**
     * Método para converter double para string
     */
    public static String doubleToString(Double d) throws Exception {
        NumberFormat nf = NumberFormat.getInstance();
        return nf.format(d);
    }

    /**
     * Método para converter float para string
     */
    public static String floatToString(Float d) throws Exception {
        NumberFormat nf = NumberFormat.getInstance();
        return nf.format(d);
    }

    /**
     * Método para converter uma string em data
     */
    public static Date stringToDate(String s) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.parse(s);
    }

    /**
     * Método para converter uma string em hora
     */
    public static Date stringToHour(String s) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.parse(s);
    }

    /**
     * Método para converter uma string em data e hora
     */
    public static Date stringToDateHour(String s) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return sdf.parse(s);
    }

    public static int getDiffYear(Date ini, Date fim) {
        long i = ini.getTime();
        long f = fim.getTime();
        int a = (int) ((f - i) / 1000 / 60 / 60 / 24 / 365);
        return a;
    }

    public static int getDiffDay(Date ini, Date fim) {
        long i = ini.getTime();
        long f = fim.getTime();
        int a = (int) ((f - i) / 1000 / 60 / 60 / 24);
        return a;
    }
}
