/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

import java.awt.Dimension;
import java.awt.Toolkit;

/**
 *
 * @author jean-marcos
 */
public class CapturaTamanhoTela {

    public static int getWidthMonitor() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        int x = screenSize.width;
        return x - 100;
    }

    public static int getHeightMonitor() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        int y = screenSize.height;
        return y - 100;
    }
}
