/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

import java.io.File;
import javax.swing.JOptionPane;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

public class LeitorXML {

    private SAXBuilder sb;
    private Document d;

    public LeitorXML(String arquivo) {
        try {
            sb = new SAXBuilder();
            d = sb.build(new File(arquivo));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Exceção ao processar arquivo! " + e.getMessage());
        }
    }

    public String getInfo(String path) {
        try {
            XPath xPath = XPath.newInstance(path);
            xPath.addNamespace("k", d.getRootElement().getNamespaceURI());
            Element node = (Element) xPath.selectSingleNode(d.getRootElement());
            return node.getText();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public String getInfo(String path, String attribute) {
        try {
            XPath xPath = XPath.newInstance(path);
            xPath.addNamespace("k", d.getRootElement().getNamespaceURI());
            Element node = (Element) xPath.selectSingleNode(d.getRootElement());
            return node.getAttributeValue(attribute);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}