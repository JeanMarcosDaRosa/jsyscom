/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

/**
 *
 * @author root
 */
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * This class takes the image objects and saves as png. It creates the
 * BufferedImage of the given image and save it as .png
 *
 * @author Rahul Sapkal(rahul@javareference.com)
 */
public class SaveAsPng {

    /**
     *
     * @param img
     */
    public static void saveAsPng(Image img, String file) throws IOException {
        //get the bufferedImage 
        BufferedImage bufferedImage = getBufferedImageFromImage(img);
        //Save as PNG using the ImageIO object 
        File f = new File(file);
        ImageIO.write(bufferedImage, "png", f);
    }

    /**
     * This method takes the Image object and creates BufferedImage of it
     *
     * @param img
     * @return
     */
    public static BufferedImage getBufferedImageFromImage(Image img) {
        //This line is important, this makes sure that the image is 
        //loaded fully 
        img = new ImageIcon(img).getImage();

        //Create the BufferedImage object with the width and height of the Image 
        BufferedImage bufferedImage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);

        //Create the graphics object from the BufferedImage 
        Graphics g = bufferedImage.createGraphics();

        //Draw the image on the graphics of the BufferedImage 
        g.drawImage(img, 0, 0, null);

        //Dispose the Graphics 
        g.dispose();

        //return the BufferedImage 
        return bufferedImage;
    }
//     public   static   void  main(String[] arg)
//    {
//        Image img  =  Toolkit.getDefaultToolkit().createImage("puppy.jpg");
//         
//        SaveAsPNG savePNG  =   new  SaveAsPNG(img);
//    }
}
