/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author jean-marcos
 */
public class ManipularArquivoProperties {

    public static Properties lerProperties(InputStream caminhoProperties) {
        try {
            Properties properties = new Properties();
            properties.load(caminhoProperties);
            caminhoProperties.close();
            return properties;
        } catch (Exception ex) {
            return null;
        }
    }

    public static void gravarProperties(String caminhoProperties, Properties properties, String header) {
        try {
            FileOutputStream fis = new FileOutputStream(new File(caminhoProperties));
            properties.store(fis, header);
            fis.close();
        } catch (IOException ex) {
        }
    }
}
