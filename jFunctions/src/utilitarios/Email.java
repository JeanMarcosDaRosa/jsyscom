/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

/**
 *
 * @author Jean
 */
import java.io.File;
import java.net.MalformedURLException;
import org.apache.commons.mail.*;

public abstract class Email {

    /**
     * envia email simples(somente texto)
     *
     * @throws EmailException
     */
    public static void enviaEmailSimples(String hostName, Integer port, String user, String pass,
            String[] to, String from, String subject, String msg) throws EmailException {
        SimpleEmail email = new SimpleEmail();
        email.setHostName(hostName); // o servidor SMTP para envio do e-mail  
        for (String s : to) {
            email.addTo(s); //destinatário  
        }
        email.setFrom(from); // remetente  
        email.setSubject(subject); // assunto do e-mail  
        email.setMsg(msg); //conteudo do e-mail  
        email.setAuthentication(user, pass);
        email.setSmtpPort(port);
        email.setSSL(true);
        email.setTLS(true);
        email.send();
    }

    /**
     * envia email com arquivo anexo
     *
     * @throws EmailException
     */
    public static void enviaEmailComAnexo(String hostName, Integer port, String user, String pass,
            String[] to, String from, String subject, String msg, String[] arqs) throws EmailException {
        // configura o email  
        MultiPartEmail email = new MultiPartEmail();
        email.setHostName(hostName); // o servidor SMTP para envio do e-mail: "smtp.gmail.com"  
        for (String s : to) {
            email.addTo(s); //destinatário  
        }
        email.setFrom(from); // remetente  
        email.setSubject(subject); // assunto do e-mail  
        email.setMsg(msg); //conteudo do e-mail  
        email.setAuthentication(user, pass);
        email.setSmtpPort(port);//465
        email.setSSL(true);
        email.setTLS(true);
        for (String arq : arqs) {
            EmailAttachment anexo1 = new EmailAttachment();
            anexo1.setPath(arq); //caminho do arquivo (RAIZ_PROJETO/teste/teste.txt)  
            anexo1.setDisposition(EmailAttachment.ATTACHMENT);
            anexo1.setDescription("Anexo");
            File file = new File(arq);
            String fileName = file.getName();
            anexo1.setName(fileName);
            email.attach(anexo1);
        }
        email.send();
    }

    /**
     * envia email com arquivo anexo
     *
     * @throws EmailException
     */
    public static void enviaEmailComAnexoFormatoHtml(String hostName, Integer port, String user, String pass,
            String[] to, String from, String subject, String msg, String[] arqs) throws EmailException {
        // configura o email  
        HtmlEmail email = new HtmlEmail();
        email.setHostName(hostName); // o servidor SMTP para envio do e-mail: "smtp.gmail.com"  
        for (String s : to) {
            email.addTo(s); //destinatário  
        }
        email.setFrom(from); // remetente  
        email.setSubject(subject); // assunto do e-mail  
        email.setHtmlMsg(msg);
        // configure uma mensagem alternativa caso o servidor não suporte HTML  
        email.setTextMsg("Seu servidor de e-mail não suporta mensagem HTML");
        //email.setMsg(msg); //conteudo do e-mail  
        email.setAuthentication(user, pass);
        email.setSmtpPort(port);//465
        email.setSSL(true);
        email.setTLS(true);
        for (String arq : arqs) {
            if (!arq.trim().equals("")) {
                EmailAttachment anexo1 = new EmailAttachment();
                anexo1.setPath(arq); //caminho do arquivo (RAIZ_PROJETO/teste/teste.txt)  
                anexo1.setDisposition(EmailAttachment.ATTACHMENT);
                anexo1.setDescription("Anexo");
                File file = new File(arq);
                String fileName = file.getName();
                anexo1.setName(fileName);
                email.attach(anexo1);
            }
        }
        email.send();
    }

    /**
     * Envia email no formato HTML
     *
     * @throws EmailException
     * @throws MalformedURLException
     */
    public static void enviaEmailFormatoHtml(String hostName, Integer port, String user, String pass,
            String[] to, String from, String subject, String msg) throws EmailException, MalformedURLException {
        HtmlEmail email = new HtmlEmail();
        // adiciona uma imagem ao corpo da mensagem e retorna seu id  
        //URL url = new URL("http://www.apache.org/images/asf_logo_wide.gif");
        //String cid = email.embed(url, "Apache logo");
        // configura a mensagem para o formato HTML  
        email.setHtmlMsg("<html>Logo do Apache - <img ></html>");
        // configure uma mensagem alternativa caso o servidor não suporte HTML  
        email.setTextMsg("Seu servidor de e-mail não suporta mensagem HTML");
        email.setHostName(hostName); // o servidor SMTP para envio do e-mail  
        for (String s : to) {
            email.addTo(s); //destinatário  
        }
        email.setFrom(from); // remetente  
        email.setSubject(subject); // assunto do e-mail  
        email.setMsg(msg); //conteudo do e-mail  
        email.setAuthentication(user, pass);
        email.setSmtpPort(port);
        email.setSSL(true);
        email.setTLS(true);
        // envia email  
        email.send();
    }
    /**
     * @param args
     * @throws EmailException
     * @throws MalformedURLException
     */
}