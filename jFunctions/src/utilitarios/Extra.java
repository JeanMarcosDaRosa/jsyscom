package utilitarios;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
import beans.PWSec;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

/**
 *
 * @author Jean Marcos
 */
public abstract class Extra {

    public static final String CRIPTOGRAFA = "C";
    public static final String DECRIPTOGRAFA = "D";
    private static final char[] HEX = "0123456789abcdef".toCharArray();

    //=================MANIPULADOR DE STRINGs===================

    static String bytesToHex(byte[] bytes) {
        if (bytes == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder(2 * bytes.length);
        for (int i = 0; i < bytes.length; ++i) {
            sb.append(HEX[(bytes[i] >> 4) & 0xF]);
            sb.append(HEX[bytes[i] & 0xF]);
        }
        return sb.toString();
    }

    public static byte[] keyedMD5(byte[] key, byte[] challenge) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac hmac = Mac.getInstance("HmacMD5");
        SecretKey sk = new SecretKeySpec(key, "HmacMD5");
        hmac.init(sk);
        byte[] result = hmac.doFinal(challenge);
        return result;
    }

    public static String getMD5(String texto) {
        byte[] challenge = texto.getBytes();
        String ret = "";
        try {
            try {
                ret = bytesToHex(keyedMD5("tanstaaftanstaaf".getBytes("ISO-8859-1"), challenge));
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Extra.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(Extra.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Extra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
        // deve imprimir b913a602c7eda7a495b4e6e7334d3890  
    }

    

    public static String CriptoDecripto(String tipo, String texto) {
        String ret = "";
        try {
            if (tipo.equals(CRIPTOGRAFA)) {
                ret = PWSec.encrypt(texto);
            } else if (tipo.equals(DECRIPTOGRAFA)) {
                ret = PWSec.decrypt(texto);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
        return ret;
    }

    public static String CriptoDecritoSenhaInteiro(String Tipo, String Org) {
        String aa = "7812639054";
        String bb = "0123456789";
        char[] a1 = aa.toCharArray();
        char[] b1 = bb.toCharArray();
        char[] Texto = Org.trim().toCharArray();
        if (Texto.length > 0) {
            if ("C".equals(Tipo.trim().toUpperCase())) {
                for (int x = 0; x < Texto.length; x++) {
                    if (aa.indexOf(Texto[x]) > (-1)) {
                        Texto[x] = b1[aa.indexOf(Texto[x])];
                    }
                }
                return Format.ArrayToString(Texto);
            } else {
                for (int x = 0; x < Texto.length; x++) {
                    if (bb.indexOf(Texto[x]) > (-1)) {
                        Texto[x] = a1[bb.indexOf(Texto[x])];
                    }
                }
                return Format.ArrayToString(Texto);
            }
        }
        return Format.ArrayToString(Texto);
    }

    public static String qtdPesados(String precoEan, String precoBanco) {
        String qtd = "";
        double pEan = Double.parseDouble(precoEan.replace(",", "."));
        double pBan = Double.parseDouble(precoBanco.replace(",", "."));
        double calc = pEan / pBan;
        qtd = Format.FormataValor(String.valueOf(calc), 3);
        return qtd;
    }

    //=================MANIPULADOR DE FORMATOS===================
    public static String PegaSenha() {
        // Cria campo onde o usuario entra com a senha  
        JPasswordField password = new JPasswordField(10);
        password.setEchoChar('*');
        // Cria um rótulo para o campo  
        JLabel rotulo = new JLabel("Digite a senha:  ");
        // Coloca o rótulo e a caixa de entrada numa JPanel:  
        JPanel entUsuario = new JPanel();
        entUsuario.add(rotulo);
        entUsuario.add(password);
        // Mostra o rótulo e a caixa de entrada de password para o usuario fornecer a senha:  
        JOptionPane.showMessageDialog(null, entUsuario, "Acesso restrito", JOptionPane.PLAIN_MESSAGE);
        // O programa só prossegue quando o usuário clicar o botao de OK do showMessageDialog.   
        // Aí, é só pegar a senha:  
        // Captura a senha:  
        String senha = password.getText();
        // mostra a senha no terminal:  
        System.out.println(senha);
        return senha;
    }

    public static String CalcDigEanPesado(String nprod, int flg) {
        Integer impar, par, indice, ind2, lp, ex;
        String kk = "";
        if (flg > 0) {
            nprod = nprod + "0";
        }
        if (flg == -1) {
            flg = 2;
        }
        par = 0;
        impar = 0;
        char[] cod2 = nprod.toCharArray();
        indice = (nprod.length());
        for (lp = 2; lp <= indice; lp++) {
            if ((lp % 2) == 0) {
                par = par + (Integer.parseInt(String.valueOf(cod2[indice - lp])));
            } else {
                impar = impar + (Integer.parseInt(String.valueOf(cod2[indice - lp])));
            }
        }
        par = par * 3;
        impar = impar + par;
        ex = ((impar / 10) * 10) + 10 - impar;
        if (ex >= 10) {
            ex = 0;
        }
        if (flg > 0) {
            kk = ex.toString();
        } else {
            kk = ex.toString();
            kk = "2";
            if (cod2[indice] != kk.charAt(0)) {
                Informacao(null, "EAN INVÁLIDO !");
            }
        }
        return kk;
    }

    public static int dataDiff(java.util.Date dataLow, java.util.Date dataHigh) {
        GregorianCalendar startTime = new GregorianCalendar();
        GregorianCalendar endTime = new GregorianCalendar();
        GregorianCalendar curTime = new GregorianCalendar();
        GregorianCalendar baseTime = new GregorianCalendar();
        startTime.setTime(dataLow);
        endTime.setTime(dataHigh);
        int dif_multiplier = 1;
        // Verifica a ordem de inicio das datas  
        if (dataLow.compareTo(dataHigh) < 0) {
            baseTime.setTime(dataHigh);
            curTime.setTime(dataLow);
            dif_multiplier = 1;
        } else {
            baseTime.setTime(dataLow);
            curTime.setTime(dataHigh);
            dif_multiplier = -1;
        }
        int result_years = 0;
        int result_months = 0;
        int result_days = 0;
        // Para cada mes e ano, vai de mes em mes pegar o ultimo dia para import acumulando  
        // no total de dias. Ja leva em consideracao ano bissesto  
        while (curTime.get(GregorianCalendar.YEAR) < baseTime.get(GregorianCalendar.YEAR)
                || curTime.get(GregorianCalendar.MONTH) < baseTime.get(GregorianCalendar.MONTH)) {
            int max_day = curTime.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
            result_months += max_day;
            curTime.add(GregorianCalendar.MONTH, 1);
        }
        // Marca que é um saldo negativo ou positivo  
        result_months = result_months * dif_multiplier;
        // Retirna a diferenca de dias do total dos meses  
        result_days += (endTime.get(GregorianCalendar.DAY_OF_MONTH) - startTime.get(GregorianCalendar.DAY_OF_MONTH));
        return result_years + result_months + result_days;
    }

    //=================OUTROS MANIPULADORES===================
    public static boolean Executar(String comando, Integer timer) {
        try {
            Runtime.getRuntime().exec(comando).getInputStream();
            if (timer > 0) {
                try {
                    Thread.sleep(timer);


                } catch (InterruptedException ex) {
                    Logger.getLogger(Extra.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return true;
        } catch (IOException ex) {
        }
        return false;
    }

    public static char getSO() {
        String osName = System.getProperty("os.name");
        char SO;
        if (osName.indexOf("Windows") > -1) {
            SO = 'W';
        } else {
            SO = 'L';
        }
        return SO;
    }

    public static void Informacao(Component comp, String Msg) {
        JOptionPane.showMessageDialog(comp, Msg, "Informação", JOptionPane.INFORMATION_MESSAGE);
    }

    public static String ImputDialog(Component comp, String Msg) {
        String result = "";
        String retorno = JOptionPane.showInputDialog(comp, Msg);
        if (retorno != null) {
            if (retorno.length() > 0) {
                result = retorno;
            } else {
                result = "";
            }
        }
        return result;
    }

    public static boolean Pergunta(Component comp, String Msg) {
        Object[] options = {"Sim", "Não"};
        int i = JOptionPane.showOptionDialog(comp, Msg, "Pergunta",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                options, options[0]);
        if (i == JOptionPane.YES_OPTION) {
            return true;
        } else {
            return false;
        }
    }
}
