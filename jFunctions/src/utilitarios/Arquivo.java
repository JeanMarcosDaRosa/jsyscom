package utilitarios;

import beans.INIFile;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import javax.swing.tree.DefaultMutableTreeNode;

public abstract class Arquivo {

    public static void removeLineInFile(String oldstring, File in, File out)
            throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(in));
        PrintWriter writer = new PrintWriter(new FileWriter(out));
        String line = null;
        while ((line = reader.readLine()) != null) {
            if (!line.contains(oldstring)) {
                writer.println(line);
            }
        }
        reader.close();
        writer.close();
    }

    public static void replaceStringInFile(String oldstring, String newstring, File in, File out) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(in));
        PrintWriter writer = new PrintWriter(new FileWriter(out));
        String line = null;
        while ((line = reader.readLine()) != null) {
            writer.println(line.replaceAll(oldstring, newstring));
        }
        reader.close();
        writer.close();

    }

    public static byte[] createChecksum(String filename) throws Exception {
        InputStream fis = new FileInputStream(filename);
        Throwable localThrowable2 = null;
        MessageDigest complete;
        try {
            byte[] buffer = new byte[1024];
            complete = MessageDigest.getInstance("MD5");
            int numRead;
            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);
        } catch (Throwable localThrowable1) {
            localThrowable2 = localThrowable1;
            throw localThrowable1;
        } finally {
            if (fis != null) {
                if (localThrowable2 != null) {
                    try {
                        fis.close();
                    } catch (Throwable x2) {
                        localThrowable2.addSuppressed(x2);
                    }
                } else {
                    fis.close();
                }
            }
        }
        return complete.digest();
    }

    public static String getMD5Checksum(String filename)
            throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result = new StringBuilder().append(result).append(Integer.toString((b[i] & 0xFF) + 256, 16).substring(1)).toString();
        }
        return result;
    }

    public static void printFiles(File f) {
        if (f.isDirectory()) {
            File[] files = f.listFiles();
            for (File file : files) {
                if (!file.isDirectory()) {
                    System.out.println(file.getAbsolutePath());
                }
            }
        }
    }

    public static boolean atu(String NArq1, String NArq2) {
        String c = "";
        if (FileExists(NArq2)) {
            DeleteFile(NArq2);
        }
        try {
            FileChannel oriChannel = new FileInputStream(NArq1).getChannel();
            FileChannel destChannel = new FileOutputStream(NArq2).getChannel();
            destChannel.transferFrom(oriChannel, 0L, oriChannel.size());
            oriChannel.close();
            destChannel.close();
            File arq1 = new File(NArq1);
            File arq2 = new File(NArq2);
            return arq2.setLastModified(arq1.lastModified());
        } catch (IOException e) {
        }

        return false;
    }

    public static boolean FileExists(String File) {
        File f = new File(File);
        return f.exists();
    }

    public static boolean TemTef(String TEF_PATH, String Num_Cupom, int num) {
        Num_Cupom = Format.Completa_Esquerda(Num_Cupom, "0", Integer.valueOf(8));
        if (FileExists(new StringBuilder().append(TEF_PATH).append("CTEF_").append(String.valueOf(num)).append("_").append(Num_Cupom).append("_1.txt").toString())) {
            return true;
        }
        return false;
    }

    public static void CopyFile(String origem, String destino) {
        try {
            InputStream in = new FileInputStream(origem);
            OutputStream out = new FileOutputStream(destino);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static boolean DeleteFile(String File) {
        boolean result = false;
        if (FileExists(File)) {
            File f = new File(File);
            return f.delete();
        }
        return result;
    }

    public static String LePrimeiraLinhaArqTXT(String Caminho) {
        String line = "";
        try {
            FileInputStream fis = new FileInputStream(Caminho);
            DataInputStream dis = new DataInputStream(fis);
            if (dis.available() != 0) {
                line = dis.readLine();
            }
            dis.close();
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return line.trim();
    }

    public static void compress(String zipFileName, String file2zip, String[] filesToZip) {
        byte[] buffer = new byte[18024];
        try {
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

            out.setLevel(-1);
            for (int i = 0; i < filesToZip.length; i++) {
                File f = new File(filesToZip[i]);
                FileInputStream in = new FileInputStream(f);
                out.putNextEntry(new ZipEntry(f.getName()));
                int len;
                while ((len = in.read(buffer)) > 0) {
                    out.write(buffer, 0, len);
                }
                out.closeEntry();
                in.close();
            }
            out.close();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void extract(File input, File output)
            throws IOException {
        if (input.exists()) {
            if (input.isDirectory()) {
                throw new IllegalArgumentException(new StringBuilder().append("\"").append(input.getAbsolutePath()).append("\" não é um arquivo!").toString());
            }
        } else {
            throw new IllegalArgumentException(new StringBuilder().append("\"").append(input.getAbsolutePath()).append("\" não existe!").toString());
        }
        if ((output.exists())
                && (output.isFile())) {
            throw new IllegalArgumentException(new StringBuilder().append("\"").append(output.getAbsolutePath()).append("\" não é um diretório!").toString());
        }

        ZipFile zip = new ZipFile(input);
        extract(zip, output);
        zip.close();
    }

    private static void compress(String caminho, File arquivo, ZipOutputStream saida)
            throws IOException {
        boolean dir = arquivo.isDirectory();
        String nome = arquivo.getName();
        ZipEntry elemento = new ZipEntry(new StringBuilder().append(caminho).append('/').append(nome).append(dir ? "/" : "").toString());

        elemento.setSize(arquivo.length());
        elemento.setTime(arquivo.lastModified());
        saida.putNextEntry(elemento);
        if (dir) {
            File[] arquivos = arquivo.listFiles();
            for (int i = 0; i < arquivos.length; i++) {
                compress(new StringBuilder().append(caminho).append('/').append(nome).toString(), arquivos[i], saida);
            }
        } else {
            FileInputStream entrada = new FileInputStream(arquivo);
            copy(entrada, saida);
            entrada.close();
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        int n = 4096;
        byte[] b = new byte[4096];
        for (int r = -1; (r = in.read(b, 0, 4096)) != -1; out.write(b, 0, r));
        out.flush();
    }

    private static void extract(ZipFile zip, File pasta) throws IOException {
        InputStream entrada = null;
        OutputStream saida = null;
        String nome = null;
        File arquivo = null;
        ZipEntry elemento = null;
        Enumeration elementos = zip.entries();
        while (elementos.hasMoreElements()) {
            elemento = (ZipEntry) elementos.nextElement();
            nome = elemento.getName();
            nome = nome.replace('/', File.separatorChar);
            nome = nome.replace('\\', File.separatorChar);
            arquivo = new File(pasta, nome);
            if (elemento.isDirectory()) {
                if (!arquivo.mkdirs()) {
                    System.out.println(new StringBuilder().append("erro ao criar diretorio: ").append(arquivo.getCanonicalPath()).toString());
                }
            } else {
                if (!arquivo.exists()) {
                    File parent = arquivo.getParentFile();
                    if ((parent != null)
                            && (!parent.mkdirs())) {
                        System.out.println(new StringBuilder().append("erro ao criar diretorio: ").append(parent.getCanonicalPath()).toString());
                    }

                    if (!arquivo.createNewFile()) {
                        System.out.println(new StringBuilder().append("erro ao criar arquivo: ").append(arquivo.getCanonicalPath()).toString());
                    }
                }
                saida = new FileOutputStream(arquivo);
                entrada = zip.getInputStream(elemento);
                copy(entrada, saida);
                saida.flush();
                saida.close();
                entrada.close();
            }
            if (!arquivo.setLastModified(elemento.getTime())) {
                System.out.println(new StringBuilder().append("erro ao setar data de modificacao: ").append(arquivo.getCanonicalPath()).toString());
            }
        }
    }

    public static String Le_N_LinhaArqTXT(String Caminho, int NumLine) {
        try {
            int c = 0;
            String line = "";
            FileInputStream fis = new FileInputStream(Caminho);
            DataInputStream dis = new DataInputStream(fis);
            while (dis.available() != 0) {
                c++;
                line = dis.readLine();
                if (c == NumLine) {
                    dis.close();
                    fis.close();
                    return line.trim();
                }
            }
            dis.close();
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    public static int ContaLinhasTXT(String Caminho) {
        int num = 0;
        try {
            FileInputStream fis = new FileInputStream(Caminho);
            DataInputStream dis = new DataInputStream(fis);
            while (dis.available() != 0) {
                dis.readLine();
                num++;
            }
            dis.close();
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return num;
    }

    public static String LeArqTXT(String Caminho) {
        StringBuilder buf = new StringBuilder();
        try {
            FileInputStream fis = new FileInputStream(Caminho);
            DataInputStream dis = new DataInputStream(fis);
            while (dis.available() != 0) {
                buf.append(dis.readLine());
                buf.append("\n");
            }
            dis.close();
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return buf.toString();
    }

    public static long GetDateFile(String Caminho) {
        long data = 0L;
        File arquivo = new File(Caminho);
        data = arquivo.lastModified();
        return data;
    }

    public static void MoveFile(String arqO, String arqD) {
        File arquivoO = new File(arqO);
        if (!arquivoO.renameTo(new File(arqD))) {
            System.out.println(new StringBuilder().append("erro ao mover arquivo: ").append(arqO).append(" para: ").append(arqD).toString());
        }
    }

    public static String[] LeArrayTXT(String arq) {
        int c = 0;
        String[] ret = new String[ContaLinhasTXT(arq)];
        c = 0;
        try {
            FileInputStream fis = new FileInputStream(arq);
            DataInputStream dis = new DataInputStream(fis);
            while (dis.available() != 0) {
                ret[c] = dis.readLine();
                c++;
            }
            dis.close();
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }

    public static boolean GravaArrayTXT(String arq, String[] linhas) throws IOException {
        boolean ok = true;
        BufferedWriter wr = new BufferedWriter(new FileWriter(arq));
        for (int i = 0; i < linhas.length; i++) {
            wr.write(new StringBuilder().append(linhas[i]).append("\n").toString());
        }
        wr.close();
        return ok;
    }

    public static boolean GravaListTXT(String arq, ArrayList<String> lista) throws IOException {
        boolean ok = true;
        BufferedWriter wr = new BufferedWriter(new FileWriter(arq));
        for (String string : lista) {
            wr.write(new StringBuilder().append(string).append("\n").toString());
        }
        wr.close();
        return ok;
    }

    public static ArrayList<String> getListTXT(String arq) {
        ArrayList lista = new ArrayList();
        try {
            FileInputStream fis = new FileInputStream(arq);
            DataInputStream dis = new DataInputStream(fis);
            while (dis.available() != 0) {
                lista.add(dis.readLine());
            }
            dis.close();
            fis.close();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        return lista;
    }

    public static boolean GravaLog(String arq, String informacao) {
        boolean ok = true;
        try {
            FileOutputStream out = new FileOutputStream(arq, true);
            PrintStream p = new PrintStream(out);
            p.print(new StringBuilder().append(informacao).append('\n').toString());
            p.close();
        } catch (Exception e) {
            ok = false;
            System.err.println(e);
        }
        return ok;
    }

    public static String[] listaArqsDir(String DIR, String mask, boolean usamask) {
        File diretorio = new File(DIR);
        File[] arquivos = diretorio.listFiles();
        if (arquivos != null) {
            String[] lista = new String[arquivos.length];
            int length = arquivos.length;
            if (usamask) {
                for (int i = 0; i < length; i++) {
                    File f = arquivos[i];
                    if (f.isFile()) {
                        int res = f.getName().indexOf(mask);
                        if (res >= 0) {
                            lista[i] = f.getName();
                        }
                    }
                }
            } else {
                for (int i = 0; i < length; i++) {
                    File f = arquivos[i];
                    if (f.isFile()) {
                        lista[i] = f.getName();
                    }
                }
            }
            return lista;
        }
        return null;
    }

    public static String[] listaArqsMaskDir(String DIR, String inicio, String fim) {
        int num = 0;
        File fild = new File(DIR.substring(0, DIR.length() - 1));
        String[] fils = fild.list();
        for (String sf : fils) {
            if ((sf.startsWith(inicio)) && (sf.endsWith(fim))) {
                num++;
            }
        }
        String[] lista = new String[num];
        num = 0;
        for (String sf : fils) {
            if ((sf.startsWith(inicio)) && (sf.endsWith(fim))) {
                lista[num] = sf;
                num++;
            }
        }
        return lista;
    }

    public static void montaArvoreArqs(String base, DefaultMutableTreeNode no) {
        File diretorio = new File(base);
        File[] conteudo = diretorio.listFiles();
        for (int i = 0; i < conteudo.length; i++) {
            if (conteudo[i].isFile()) {
                DefaultMutableTreeNode arquivo = new DefaultMutableTreeNode(conteudo[i].getName());
                no.add(arquivo);
            } else {
                DefaultMutableTreeNode dir = new DefaultMutableTreeNode(conteudo[i].getName());
                montaArvoreArqs(conteudo[i].toString(), dir);
                no.add(dir);
            }
        }
    }

    public static boolean renomear(String NArq1, String NArq2) {
        File arq1 = new File(NArq1);
        File arq2 = new File(NArq2);
        return arq1.renameTo(arq2);
    }

    public static int numArqs(String pasta, String inicio, String fim) {
        int num = 0;
        File fild = new File(pasta);
        String[] fils = fild.list();
        for (String sf : fils) {
            if ((sf.startsWith(inicio)) && (sf.endsWith(fim))) {
                num++;
            }
        }
        return num;
    }

    public static boolean GravaINI(String arquivo, String sessao, String propriedade, String valor) {
        if (!FileExists(arquivo)) {
            GravaLog(arquivo, "");
        }
        if (FileExists(arquivo)) {
            try {
                INIFile IniFile = new INIFile(arquivo);
                IniFile.setString(sessao, propriedade, valor);
                return true;
            } catch (Exception e) {
                System.out.println(e);
                return false;
            }
        }
        return false;
    }

    public static String LeINI(String arquivo, String sessao, String propriedade) {
        if (FileExists(arquivo)) {
            try {
                INIFile IniFile = new INIFile(arquivo);
                return IniFile.getString(sessao, propriedade);
            } catch (Exception e) {
                System.out.println(e);
                return null;
            }
        }
        return null;
    }

    public static void ApagaArqsOfPath(String PATH, int Dias) {
        String[] n = listaArqsDir(PATH, "", false);
        Date dataFinal = new Date();
        for (int i = 0; i < n.length; i++) {
            String vDestino = new StringBuilder().append(PATH).append(n[i]).toString();
            if (FileExists(vDestino)) {
                Date dataArq = new Date(GetDateFile(vDestino));
                int dif = Extra.dataDiff(dataArq, dataFinal);
                if (dif > Dias) {
                    DeleteFile(vDestino);
                }
            }
        }
    }

    public static String getPathBase() {
        String path = "";
        File dir = new File(".");
        try {
            if (Extra.getSO() == 'W') {
                path = new StringBuilder().append(dir.getCanonicalPath()).append("\\").toString();
            } else {
                path = new StringBuilder().append(dir.getCanonicalPath()).append("/").toString();
            }
        } catch (Exception e) {
            path = null;
        }
        return path;
    }

    public static String getBarra() {
        return File.separator;
    }

    public static String getNomeFile(String arq) {
        File f = new File(arq);
        return f.getName();
    }

    public static String getPathPai() {
        String path = "";
        File dir = new File("..");
        try {
            if (Extra.getSO() == 'W') {
                path = new StringBuilder().append(dir.getCanonicalPath()).append("\\").toString();
            } else {
                path = new StringBuilder().append(dir.getCanonicalPath()).append("/").toString();
            }
        } catch (Exception e) {
            path = null;
        }
        return path;
    }
}
