package utilitarios;

import beans.Conexao;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

public abstract class DataBase {

    public static final int RESULT = 2;
    public static final int EXECUTE = 1;
    public static final int FIREBIRD = 3050;
    public static final int MYSQL = 3306;
    public static final int POSTGRES = 5432;

    public static String trataSqlNull(String sql, Integer indice) {
        if (!sql.toLowerCase().contains(" set ")) {
            if (sql.toLowerCase().contains("insert into ")) {
                if (!sql.contains("where")) {
                    sql = Str.substStrOcorrence(sql, "?", "null", indice);
                }
            } else {
                sql = Str.substCharNotEmptyAnt(sql, "?", " is ", indice);
                sql = Str.substStrOcorrence(sql, "?", "null", indice);
            }
        } else if (sql.contains("where")) {
            if (Str.getIdxStrOcorrence(sql, "?", indice).intValue() < sql.indexOf("where")) {
                sql = Str.substStrOcorrence(sql, "?", "null", indice);
            } else {
                sql = Str.substCharNotEmptyAnt(sql, "?", " is ", indice);
                sql = Str.substStrOcorrence(sql, "?", "null", indice);
            }
        } else {
            sql = Str.substStrOcorrence(sql, "?", "null", indice);
        }

        return sql;
    }

    public static ResultSet executeQuery(String sql, Object[] params, String ip, String localDB, String user, String pass, int tipo_retorno, Integer TYPE_DB) throws SQLException {
        return executeQuery(sql, params, ip, TYPE_DB, localDB, user, pass, tipo_retorno, TYPE_DB);
    }

    public static ResultSet executeQuery(String sql, Object[] params, String ip, Integer port, String localDB, String user, String pass, int tipo_retorno, Integer TYPE_DB)
            throws SQLException {
        if (params != null) {
            int count = 1;
            for (Object object : params) {
                if (object == null) {
                    sql = trataSqlNull(sql, Integer.valueOf(count));
                } else {
                    count++;
                }
            }
        }

        PreparedStatement pstm = Conexao.getInstance().getPreparedStatement(sql, ip, port, localDB, user, pass, TYPE_DB);
        if (params != null) {
            Boolean binary = Boolean.valueOf(false);
            int t = 1;
            for (int i = 0; i < params.length; i++) {
                if (params[i] != null) {
                    if (!binary.booleanValue()) {
                        if (params[i].getClass() == Integer.class) {
                            pstm.setInt(t, ((Integer) params[i]).intValue());
                        }
                        if (params[i].getClass() == Float.class) {
                            pstm.setFloat(t, ((Float) params[i]).floatValue());
                        }
                        if (params[i].getClass() == Double.class) {
                            pstm.setDouble(t, ((Double) params[i]).doubleValue());
                        }
                        if (params[i].getClass() == Boolean.class) {
                            pstm.setBoolean(t, ((Boolean) params[i]).booleanValue());
                        }
                        if (params[i].getClass() == java.sql.Date.class) {
                            pstm.setDate(t, (java.sql.Date) params[i]);
                        }
                        if (params[i].getClass() == java.util.Date.class) {
                            java.util.Date d = (java.util.Date) params[i];
                            pstm.setDate(t, new java.sql.Date(d.getTime()));
                        }
                        if (params[i].getClass() == Time.class) {
                            pstm.setTime(t, (Time) params[i]);
                        }
                        if (params[i].getClass() == String.class) {
                            pstm.setString(t, (String) params[i]);
                        }
                        if (params[i].getClass() == InputStream.class) {
                            pstm.setBinaryStream(t, (InputStream) params[i], ((Integer) params[(i + 1)]).intValue());
                            binary = Boolean.valueOf(true);
                        }
                        t++;
                    } else {
                        binary = Boolean.valueOf(false);
                    }
                }
            }
        }
        ResultSet rs;
        if (tipo_retorno == 2) {
            rs = pstm.executeQuery();
        } else {
            pstm.executeUpdate();
            rs = null;
        }
        return rs;
    }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jFunctions.jar
 * Qualified Name:     utilitarios.DataBase
 * JD-Core Version:    0.6.2
 */