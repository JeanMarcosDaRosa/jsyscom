/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

/**
 *
 * @author Jean
 */
public abstract class Validador {

    public static Integer positivo(Integer i) {
        if (i < 0) {
            i = i * -1;
        }
        return i;
    }

    public static Boolean isNumber(String texto) {
        Boolean result = true;
        if (texto.length() > 0) {
            try {
                Long.parseLong(texto);
                result = true;
            } catch (NumberFormatException ex) {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }

    public static String soNumeros(String v) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < v.length(); i++) {
            if (v.charAt(i) == '0' || v.charAt(i) == '1' || v.charAt(i) == '2' || v.charAt(i) == '3' || v.charAt(i) == '4' || v.charAt(i) == '5'
                    || v.charAt(i) == '6' || v.charAt(i) == '7' || v.charAt(i) == '8' || v.charAt(i) == '9') {
                buf.append(v.charAt(i));
            }
        }
        return buf.toString();
    }
    
    public static String soLetras(String v) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < v.length(); i++) {
            if (!(v.charAt(i) == '0' || v.charAt(i) == '1' || v.charAt(i) == '2' || v.charAt(i) == '3' || v.charAt(i) == '4' || v.charAt(i) == '5'
                    || v.charAt(i) == '6' || v.charAt(i) == '7' || v.charAt(i) == '8' || v.charAt(i) == '9')) {
                buf.append(v.charAt(i));
            }
        }
        return buf.toString();
    }

    public static Boolean isEmail(String email) {
        if (email.contains("@")) {
            String[] arroba = email.split("@");
            if (arroba.length == 2) {
                if (arroba[1].contains(".")) {
                    return true;
                }
            }
        }
        return false;
    }

    //Método que valida o CPF
    public static Boolean ValidaCPF(String vrCPF) {
        String valor = vrCPF.replace(".", "");
        valor = valor.replace("-", "");

        if (valor.length() != 11) {
            return false;
        }

        Boolean igual = true;

        char[] tmp = valor.toCharArray();

        for (int i = 1; i < 11 && igual; i++) {
            if (tmp[i] != tmp[0]) {
                igual = false;
            }
        }

        if (igual || "12345678909".equals(valor)) {
            return false;
        }

        int[] numeros = new int[11];



        for (int i = 0; i < 11; i++) {
            String tmp2 = "" + tmp[i];
            numeros[i] = Integer.parseInt(tmp2);
        }

        int soma = 0;
        for (int i = 0; i < 9; i++) {
            soma += (10 - i) * numeros[i];
        }

        int resultado = soma % 11;
        if (resultado == 1 || resultado == 0) {
            if (numeros[9] != 0) {
                return false;
            }
        } else if (numeros[9] != 11 - resultado) {
            return false;
        }

        soma = 0;
        for (int i = 0; i < 10; i++) {
            soma += (11 - i) * numeros[i];
        }

        resultado = soma % 11;

        if (resultado == 1 || resultado == 0) {
            if (numeros[10] != 0) {
                return false;
            }

        } else if (numeros[10] != 11 - resultado) {
            return false;
        }
        return true;
    }

    //Método que valida o CNPJ 
    public static Boolean ValidaCNPJ(String vrCNPJ) {

        String CNPJ = vrCNPJ.replace(".", "");
        CNPJ = CNPJ.replace("/", "");
        CNPJ = CNPJ.replace("-", "");

        int[] digitos, soma, resultado;
        int nrDig;
        String ftmt;
        Boolean[] CNPJOk;

        ftmt = "6543298765432";
        digitos = new int[14];
        soma = new int[2];
        soma[0] = 0;
        soma[1] = 0;
        resultado = new int[2];
        resultado[0] = 0;
        resultado[1] = 0;
        CNPJOk = new Boolean[2];
        CNPJOk[0] = false;
        CNPJOk[1] = false;

        try {
            for (nrDig = 0; nrDig < 14; nrDig++) {
                digitos[nrDig] = Integer.parseInt(CNPJ.substring(nrDig, 1));
                if (nrDig <= 11) {
                    soma[0] += (digitos[nrDig]
                            * Integer.parseInt(ftmt.substring(nrDig + 1, 1)));
                }
                if (nrDig <= 12) {
                    soma[1] += (digitos[nrDig]
                            * Integer.parseInt(ftmt.substring(nrDig, 1)));
                }
            }

            for (nrDig = 0; nrDig < 2; nrDig++) {
                resultado[nrDig] = (soma[nrDig] % 11);
                if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1)) {
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                } else {
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
                }

            }

            return (CNPJOk[0] && CNPJOk[1]);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

    }
}