INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (1, 1, 'AC', 'Acre');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (2, 1, 'AL', 'Alagoas');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (3, 1, 'AM', 'Amazonas');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (4, 1, 'AP', 'Amapá');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (5, 1, 'BA', 'Bahia');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (6, 1, 'CE', 'Ceará');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (7, 1, 'DF', 'Distrito Federal');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (8, 1, 'ES', 'Espírito Santo');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (9, 1, 'GO', 'Goiás');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (10, 1, 'MA', 'Maranhão');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (11, 1, 'MG', 'Minas Gerais');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (12, 1, 'MS', 'Mato Grosso do Sul');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (13, 1, 'MT', 'Mato Grosso');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (14, 1, 'PA', 'Pará');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (15, 1, 'PB', 'Paraíba');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (16, 1, 'PE', 'Pernambuco');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (17, 1, 'PI', 'Piauí');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (18, 1, 'PR', 'Paraná');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (19, 1, 'RJ', 'Rio de Janeiro');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (20, 1, 'RN', 'Rio Grande do Norte');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (21, 1, 'RO', 'Rondônia');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (22, 1, 'RR', 'Roraima');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (23, 1, 'RS', 'Rio Grande do Sul');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (24, 1, 'SC', 'Santa Catarina');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (25, 1, 'SE', 'Sergipe');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (26, 1, 'SP', 'São Paulo');

INSERT INTO uf
 (cd_uf, cd_pais, sigla_uf, nome_uf)
VALUES (27, 1, 'TO', 'Tocantins');