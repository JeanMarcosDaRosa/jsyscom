--
-- Data for table public.pais (OID = 16619) (LIMIT 0,1)
--
INSERT INTO pais (cd_pais, sigla_pais, nome_pais, zip_code_pais)
VALUES (1, 'BR', 'Brasil', '55');

--
-- Data for table public.uf (OID = 16666) (LIMIT 0,27)
--
INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'AC', 'Acre');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'AL', 'Alagoas');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'AM', 'Amazonas');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'AP', 'Amapá');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'BA', 'Bahia');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'CE', 'Ceará');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'DF', 'Distrito Federal');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'ES', 'Espírito Santo');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'GO', 'Goiás');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'MA', 'Maranhão');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'MG', 'Minas Gerais');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'MS', 'Mato Grosso do Sul');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'MT', 'Mato Grosso');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'PA', 'Pará');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'PB', 'Paraíba');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'PE', 'Pernambuco');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'PI', 'Piauí');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'PR', 'Paraná');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'RJ', 'Rio de Janeiro');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'RN', 'Rio Grande do Norte');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'RO', 'Rondônia');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'RR', 'Roraima');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'RS', 'Rio Grande do Sul');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'SC', 'Santa Catarina');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'SE', 'Sergipe');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'SP', 'São Paulo');

INSERT INTO uf (cd_pais, sigla_uf, nome_uf)
VALUES (1, 'TO', 'Tocantins');

