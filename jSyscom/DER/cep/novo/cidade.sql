--
-- Data for table public.cidade (OID = 16427) (LIMIT 0,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1, 'AC', 'Acrelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2, 'AC', 'Assis Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3, 'AC', 'Brasiléia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4, 'AC', 'Bujari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5, 'AC', 'Capixaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6, 'AC', 'Cruzeiro do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7, 'AC', 'Epitaciolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8, 'AC', 'Feijó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9, 'AC', 'Jordão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10, 'AC', 'Mâncio Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11, 'AC', 'Manoel Urbano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12, 'AC', 'Marechal Thaumaturgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (13, 'AC', 'Plácido de Castro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (14, 'AC', 'Porto Acre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (15, 'AC', 'Porto Walter');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (16, 'AC', 'Rio Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (17, 'AC', 'Rodrigues Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (18, 'AC', 'Santa Rosa do Purus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (19, 'AC', 'Sena Madureira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (20, 'AC', 'Senador Guiomard');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (21, 'AC', 'Tarauacá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (22, 'AC', 'Xapuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (23, 'AL', 'Água Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (24, 'AL', 'Alazão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (25, 'AL', 'Alecrim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (26, 'AL', 'Anadia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (27, 'AL', 'Anel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (28, 'AL', 'Anum Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (29, 'AL', 'Anum Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (30, 'AL', 'Arapiraca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (31, 'AL', 'Atalaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (32, 'AL', 'Baixa da Onça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (33, 'AL', 'Baixa do Capim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (34, 'AL', 'Bálsamo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (35, 'AL', 'Bananeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (36, 'AL', 'Barra de Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (37, 'AL', 'Barra de São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (38, 'AL', 'Barra do Bonifácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (39, 'AL', 'Barra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (40, 'AL', 'Batalha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (41, 'AL', 'Batingas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (42, 'AL', 'Belém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (43, 'AL', 'Belo Monte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (44, 'AL', 'Boa Sorte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (45, 'AL', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (46, 'AL', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (47, 'AL', 'Boca da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (48, 'AL', 'Bom Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (49, 'AL', 'Bonifácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (50, 'AL', 'Branquinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (51, 'AL', 'Cacimbinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (52, 'AL', 'Cajarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (53, 'AL', 'Cajueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (54, 'AL', 'Caldeirões de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (55, 'AL', 'Camadanta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (56, 'AL', 'Campestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (57, 'AL', 'Campo Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (58, 'AL', 'Campo Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (59, 'AL', 'Canaã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (60, 'AL', 'Canafístula');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (61, 'AL', 'Canapi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (62, 'AL', 'Canastra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (63, 'AL', 'Cangandu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (64, 'AL', 'Capela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (65, 'AL', 'Carneiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (66, 'AL', 'Carrasco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (67, 'AL', 'Chã Preta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (68, 'AL', 'Coité do Nóia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (69, 'AL', 'Colônia Leopoldina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (70, 'AL', 'Coqueiro Seco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (71, 'AL', 'Coruripe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (72, 'AL', 'Coruripe da Cal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (73, 'AL', 'Craíbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (74, 'AL', 'Delmiro Gouveia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (75, 'AL', 'Dois Riachos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (76, 'AL', 'Entremontes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (77, 'AL', 'Estrela de Alagoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (78, 'AL', 'Feira Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (79, 'AL', 'Feliz Deserto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (81, 'AL', 'Flexeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (83, 'AL', 'Gaspar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (84, 'AL', 'Girau do Ponciano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (85, 'AL', 'Ibateguara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (86, 'AL', 'Igaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (87, 'AL', 'Igreja Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (88, 'AL', 'Inhapi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (89, 'AL', 'Jacaré dos Homens');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (90, 'AL', 'Jacuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (91, 'AL', 'Japaratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (92, 'AL', 'Jaramataia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (93, 'AL', 'Jenipapo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (94, 'AL', 'Joaquim Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (95, 'AL', 'Jundiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (96, 'AL', 'Junqueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (97, 'AL', 'Lagoa da Areia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (98, 'AL', 'Lagoa da Canoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (99, 'AL', 'Lagoa da Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (100, 'AL', 'Lagoa Dantas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (101, 'AL', 'Lagoa do Caldeirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (102, 'AL', 'Lagoa do Canto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (103, 'AL', 'Lagoa do Exú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (104, 'AL', 'Lagoa do Rancho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (105, 'AL', 'Lagoa do Rancho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (106, 'AL', 'Lajes do Caldeirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (107, 'AL', 'Laranjal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (108, 'AL', 'Limoeiro de Anadia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (109, 'AL', 'Maceió');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (110, 'AL', 'Major Isidoro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (111, 'AL', 'Mar Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (112, 'AL', 'Maragogi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (113, 'AL', 'Maravilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (114, 'AL', 'Marechal Deodoro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (115, 'AL', 'Maribondo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (116, 'AL', 'Massaranduba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (117, 'AL', 'Mata Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (118, 'AL', 'Matriz de Camaragibe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (119, 'AL', 'Messias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (120, 'AL', 'Minador do Negrão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (121, 'AL', 'Monteirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (122, 'AL', 'Moreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (123, 'AL', 'Munguba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (124, 'AL', 'Murici');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (125, 'AL', 'Novo Lino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (126, 'AL', 'Olho D Água Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (127, 'AL', 'Olho D Água das Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (128, 'AL', 'Olho D Água de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (129, 'AL', 'Olho D Água do Casado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (130, 'AL', 'Olho D Água dos Dandanhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (131, 'AL', 'Olivença');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (132, 'AL', 'Ouro Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (133, 'AL', 'Palestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (134, 'AL', 'Palmeira de Fora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (135, 'AL', 'Palmeira dos Índios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (136, 'AL', 'Pão de Açúcar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (137, 'AL', 'Pariconha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (138, 'AL', 'Paripueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (139, 'AL', 'Passo de Camaragibe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (140, 'AL', 'Pau D Arco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (141, 'AL', 'Pau Ferro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (142, 'AL', 'Paulo Jacinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (143, 'AL', 'Penedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (144, 'AL', 'Piaçabuçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (145, 'AL', 'Pilar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (146, 'AL', 'Pindoba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (147, 'AL', 'Piranhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (148, 'AL', 'Poção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (149, 'AL', 'Poço da Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (150, 'AL', 'Poço das Trincheiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (151, 'AL', 'Porto Calvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (152, 'AL', 'Porto de Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (153, 'AL', 'Porto Real do Colégio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (154, 'AL', 'Poxim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (155, 'AL', 'Quebrangulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (156, 'AL', 'Riacho do Sertão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (157, 'AL', 'Riacho Fundo de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (158, 'AL', 'Rio Largo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (159, 'AL', 'Rocha Cavalcante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (160, 'AL', 'Roteiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (161, 'AL', 'Santa Efigênia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (162, 'AL', 'Santa Luzia do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (163, 'AL', 'Santana do Ipanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (164, 'AL', 'Santana do Mundaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (165, 'AL', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (166, 'AL', 'São Brás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (167, 'AL', 'São José da Laje');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (168, 'AL', 'São José da Tapera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (169, 'AL', 'São Luís do Quitunde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (170, 'AL', 'São Miguel dos Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (171, 'AL', 'São Miguel dos Milagres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (172, 'AL', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (173, 'AL', 'Sapucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (174, 'AL', 'Sapucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (175, 'AL', 'Satuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (176, 'AL', 'Senador Rui Palmeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (177, 'AL', 'Serra da Mandioca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (178, 'AL', 'Serra do São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (179, 'AL', 'Taboleiro do Pinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (180, 'AL', 'Taboquinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (181, 'AL', 'Tanque D Arca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (182, 'AL', 'Taquarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (183, 'AL', 'Tatuamunha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (184, 'AL', 'Teotônio Vilela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (185, 'AL', 'Traipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (186, 'AL', 'União dos Palmares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (187, 'AL', 'Usina Camaçari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (188, 'AL', 'Viçosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (189, 'AL', 'Vila Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (190, 'AL', 'Vila São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (191, 'AM', 'Alvarães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (192, 'AM', 'Amatari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (193, 'AM', 'Amaturá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (194, 'AM', 'Anamã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (195, 'AM', 'Anori');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (196, 'AM', 'Apuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (197, 'AM', 'Ariaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (198, 'AM', 'Atalaia do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (199, 'AM', 'Augusto Montenegro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (200, 'AM', 'Autazes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (201, 'AM', 'Axinim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (202, 'AM', 'Badajós');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (203, 'AM', 'Balbina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (204, 'AM', 'Barcelos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (205, 'AM', 'Barreirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (206, 'AM', 'Benjamin Constant');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (207, 'AM', 'Beruri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (208, 'AM', 'Boa Vista do Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (209, 'AM', 'Boca do Acre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (210, 'AM', 'Borba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (211, 'AM', 'Caapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (212, 'AM', 'Cametá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (213, 'AM', 'Canumã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (214, 'AM', 'Canutama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (215, 'AM', 'Carauari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (216, 'AM', 'Careiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (217, 'AM', 'Careiro da Várzea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (218, 'AM', 'Carvoeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (219, 'AM', 'Coari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (220, 'AM', 'Codajás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (221, 'AM', 'Cucuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (222, 'AM', 'Eirunepé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (223, 'AM', 'Envira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (224, 'AM', 'Floriano Peixoto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (225, 'AM', 'Fonte Boa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (226, 'AM', 'Freguesia do Andirá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (227, 'AM', 'Guajará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (228, 'AM', 'Humaitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (229, 'AM', 'Iauaretê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (230, 'AM', 'Içanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (231, 'AM', 'Ipixuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (232, 'AM', 'Iranduba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (233, 'AM', 'Itacoatiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (234, 'AM', 'Itamarati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (235, 'AM', 'Itapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (236, 'AM', 'Japurá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (237, 'AM', 'Juruá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (238, 'AM', 'Jutaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (239, 'AM', 'Lábrea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (240, 'AM', 'Lago Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (241, 'AM', 'Manacapuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (242, 'AM', 'Manaquiri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (243, 'AM', 'Manaus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (244, 'AM', 'Manicoré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (245, 'AM', 'Maraã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (246, 'AM', 'Massauari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (247, 'AM', 'Maués');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (248, 'AM', 'Mocambo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (249, 'AM', 'Moura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (250, 'AM', 'Murutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (251, 'AM', 'Nhamundá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (252, 'AM', 'Nova Olinda do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (253, 'AM', 'Novo Airão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (254, 'AM', 'Novo Aripuanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (255, 'AM', 'Osório da Fonseca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (256, 'AM', 'Parintins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (257, 'AM', 'Pauini');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (258, 'AM', 'Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (259, 'AM', 'Presidente Figueiredo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (260, 'AM', 'Repartimento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (261, 'AM', 'Rio Preto da Eva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (262, 'AM', 'Santa Isabel do Rio Negro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (263, 'AM', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (264, 'AM', 'Santo Antônio do Içá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (265, 'AM', 'São Felipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (266, 'AM', 'São Gabriel da Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (267, 'AM', 'São Paulo de Olivença');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (268, 'AM', 'São Sebastião do Uatumã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (269, 'AM', 'Silves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (270, 'AM', 'Tabatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (271, 'AM', 'Tapauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (272, 'AM', 'Tefé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (273, 'AM', 'Tonantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (274, 'AM', 'Uarini');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (275, 'AM', 'Urucará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (276, 'AM', 'Urucurituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (277, 'AM', 'Vila Pitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (278, 'AP', 'Abacate da Pedreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (280, 'AP', 'Amapá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (281, 'AP', 'Amapari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (282, 'AP', 'Ambé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (283, 'AP', 'Aporema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (284, 'AP', 'Ariri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (285, 'AP', 'Bailique');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (286, 'AP', 'Boca do Jari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (287, 'AP', 'Calçoene');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (288, 'AP', 'Cantanzal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (289, 'AP', 'Carmo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (290, 'AP', 'Clevelândia do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (291, 'AP', 'Corre Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (292, 'AP', 'Cunani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (293, 'AP', 'Curiaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (294, 'AP', 'Cutias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (295, 'AP', 'Fazendinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (296, 'AP', 'Ferreira Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (297, 'AP', 'Fortaleza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (298, 'AP', 'Gaivota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (299, 'AP', 'Gurupora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (300, 'AP', 'Igarapé do Lago');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (301, 'AP', 'Ilha de Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (302, 'AP', 'Inajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (303, 'AP', 'Itaubal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (304, 'AP', 'Laranjal do Jari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (305, 'AP', 'Livramento do Pacuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (306, 'AP', 'Lourenço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (307, 'AP', 'Macapá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (308, 'AP', 'Mazagão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (309, 'AP', 'Mazagão Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (310, 'AP', 'Oiapoque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (311, 'AP', 'Paredão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (312, 'AP', 'Porto Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (313, 'AP', 'Pracuúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (314, 'AP', 'Santa Luzia do Pacuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (315, 'AP', 'Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (316, 'AP', 'Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (317, 'AP', 'São Joaquim do Pacuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (318, 'AP', 'São Sebastião do Livramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (319, 'AP', 'São Tomé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (321, 'AP', 'Sucuriju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (322, 'AP', 'Tartarugalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (323, 'AP', 'Vila Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (324, 'AP', 'Vitória do Jari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (325, 'BA', 'Abadia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (326, 'BA', 'Abaíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (327, 'BA', 'Abaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (328, 'BA', 'Abelhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (329, 'BA', 'Abóbora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (330, 'BA', 'Abrantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (331, 'BA', 'Acajutiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (332, 'BA', 'Açu da Torre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (333, 'BA', 'Açudina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (334, 'BA', 'Acupe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (335, 'BA', 'Adustina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (336, 'BA', 'Afligidos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (337, 'BA', 'Afrânio Peixoto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (338, 'BA', 'Água Doce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (339, 'BA', 'Água Fria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (340, 'BA', 'Águas do Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (341, 'BA', 'Aiquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (342, 'BA', 'Alagoinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (343, 'BA', 'Alcobaça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (344, 'BA', 'Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (345, 'BA', 'Algodão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (346, 'BA', 'Algodões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (347, 'BA', 'Almadina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (348, 'BA', 'Alto Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (349, 'BA', 'Amado Bahia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (350, 'BA', 'Amaniú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (351, 'BA', 'Amargosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (352, 'BA', 'Amélia Rodrigues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (353, 'BA', 'América Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (354, 'BA', 'Américo Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (355, 'BA', 'Anagé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (356, 'BA', 'Andaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (357, 'BA', 'Andorinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (358, 'BA', 'Angical');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (359, 'BA', 'Angico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (360, 'BA', 'Anguera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (361, 'BA', 'Antas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (362, 'BA', 'Antônio Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (363, 'BA', 'Antônio Gonçalves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (364, 'BA', 'Aporá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (365, 'BA', 'Apuarema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (366, 'BA', 'Araçás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (367, 'BA', 'Aracatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (368, 'BA', 'Araci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (369, 'BA', 'Aramari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (370, 'BA', 'Arapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (371, 'BA', 'Arataca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (372, 'BA', 'Aratuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (373, 'BA', 'Areias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (374, 'BA', 'Arembepe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (375, 'BA', 'Argoim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (376, 'BA', 'Argolo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (377, 'BA', 'Aribice');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (378, 'BA', 'Aritaguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (379, 'BA', 'Aurelino Leal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (380, 'BA', 'Baianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (381, 'BA', 'Baixa do Palmeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (382, 'BA', 'Baixa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (383, 'BA', 'Baixão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (384, 'BA', 'Baixinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (385, 'BA', 'Baluarte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (386, 'BA', 'Banco Central');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (387, 'BA', 'Banco da Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (388, 'BA', 'Bandeira do Almada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (389, 'BA', 'Bandeira do Colônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (390, 'BA', 'Bandiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (391, 'BA', 'Banzaê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (392, 'BA', 'Baraúnas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (393, 'BA', 'Barcelos do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (394, 'BA', 'Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (395, 'BA', 'Barra da Estiva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (396, 'BA', 'Barra do Choça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (397, 'BA', 'Barra do Jacuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (398, 'BA', 'Barra do Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (399, 'BA', 'Barra do Pojuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (400, 'BA', 'Barra do Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (401, 'BA', 'Barra do Tarrachil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (402, 'BA', 'Barracas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (403, 'BA', 'Barreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (404, 'BA', 'Barro Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (405, 'BA', 'Barro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (406, 'BA', 'Barro Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (407, 'BA', 'Barrocas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (408, 'BA', 'Bastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (409, 'BA', 'Bate Pé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (410, 'BA', 'Batinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (411, 'BA', 'Bela Flor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (412, 'BA', 'Belém da Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (413, 'BA', 'Belmonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (414, 'BA', 'Belo Campo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (415, 'BA', 'Belo Campo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (416, 'BA', 'Bem-Bom');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (417, 'BA', 'Bendegó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (418, 'BA', 'Bento Simões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (419, 'BA', 'Biritinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (420, 'BA', 'Boa Espera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (421, 'BA', 'Boa Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (422, 'BA', 'Boa União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (423, 'BA', 'Boa Vista do Lagamar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (424, 'BA', 'Boa Vista do Tupim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (425, 'BA', 'Boaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (426, 'BA', 'Boca do Córrego');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (427, 'BA', 'Bom Jesus da Lapa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (428, 'BA', 'Bom Jesus da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (429, 'BA', 'Bom Sossego');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (430, 'BA', 'Bonfim da Feira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (431, 'BA', 'Boninal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (432, 'BA', 'Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (433, 'BA', 'Boquira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (434, 'BA', 'Botuporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (435, 'BA', 'Botuquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (436, 'BA', 'Brejinho das Ametistas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (437, 'BA', 'Brejo da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (438, 'BA', 'Brejo Luíza de Brito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (439, 'BA', 'Brejo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (440, 'BA', 'Brejões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (441, 'BA', 'Brejolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (442, 'BA', 'Brotas de Macaúbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (443, 'BA', 'Brumado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (444, 'BA', 'Bucuituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (445, 'BA', 'Buerarema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (446, 'BA', 'Buracica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (447, 'BA', 'Buranhém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (448, 'BA', 'Buril');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (449, 'BA', 'Buris de Abrantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (450, 'BA', 'Buritirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (451, 'BA', 'Caatiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (452, 'BA', 'Cabaceiras do Paraguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (453, 'BA', 'Cabrália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (454, 'BA', 'Cacha Pregos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (455, 'BA', 'Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (456, 'BA', 'Cachoeira do Mato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (457, 'BA', 'Caculé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (458, 'BA', 'Caém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (459, 'BA', 'Caetanos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (460, 'BA', 'Caeté-Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (461, 'BA', 'Caetité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (462, 'BA', 'Cafarnaum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (463, 'BA', 'Caiçara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (464, 'BA', 'Caimbé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (465, 'BA', 'Cairu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (466, 'BA', 'Caiubi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (467, 'BA', 'Cajuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (468, 'BA', 'Caldas do Jorro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (469, 'BA', 'Caldeirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (470, 'BA', 'Caldeirão Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (471, 'BA', 'Caldeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (472, 'BA', 'Camacan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (473, 'BA', 'Camaçari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (474, 'BA', 'Camamu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (475, 'BA', 'Camassandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (476, 'BA', 'Camirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (477, 'BA', 'Campinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (478, 'BA', 'Campo Alegre de Lourdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (479, 'BA', 'Campo Formoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (480, 'BA', 'Campo Formoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (481, 'BA', 'Camurugi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (482, 'BA', 'Canabravinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (483, 'BA', 'Canápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (484, 'BA', 'Canarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (485, 'BA', 'Canatiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (486, 'BA', 'Canavieiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (487, 'BA', 'Canché');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (488, 'BA', 'Candeal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (489, 'BA', 'Candeias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (490, 'BA', 'Candiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (491, 'BA', 'Cândido Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (492, 'BA', 'Canoão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (493, 'BA', 'Cansanção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (494, 'BA', 'Canto do Sol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (495, 'BA', 'Canudos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (496, 'BA', 'Canudos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (497, 'BA', 'Capão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (498, 'BA', 'Capela do Alto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (499, 'BA', 'Capim Grosso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (500, 'BA', 'Caraguataí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (501, 'BA', 'Caraíbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (502, 'BA', 'Caraibuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (503, 'BA', 'Caraípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (504, 'BA', 'Caraiva');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (505, 'BA', 'Caravelas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (506, 'BA', 'Cardeal da Silva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (507, 'BA', 'Carinhanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (508, 'BA', 'Caripare');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (509, 'BA', 'Carnaíba do Sertão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (510, 'BA', 'Carrapichel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (511, 'BA', 'Casa Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (512, 'BA', 'Castelo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (513, 'BA', 'Castro Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (514, 'BA', 'Catinga do Moura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (515, 'BA', 'Catingal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (516, 'BA', 'Catolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (517, 'BA', 'Catolés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (518, 'BA', 'Catolezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (519, 'BA', 'Catu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (520, 'BA', 'Catu de Abrantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (521, 'BA', 'Caturama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (522, 'BA', 'Cavunge');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (523, 'BA', 'Central');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (524, 'BA', 'Ceraima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (525, 'BA', 'Chorrochó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (526, 'BA', 'Cícero Dantas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (527, 'BA', 'Cinco Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (528, 'BA', 'Cipó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (529, 'BA', 'Coaraci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (530, 'BA', 'Cocos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (531, 'BA', 'Colônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (532, 'BA', 'Comércio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (533, 'BA', 'Conceição da Feira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (534, 'BA', 'Conceição do Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (535, 'BA', 'Conceição do Coité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (536, 'BA', 'Conceição do Jacuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (537, 'BA', 'Conde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (538, 'BA', 'Condeúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (539, 'BA', 'Contendas do Sincorá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (540, 'BA', 'Copixaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (541, 'BA', 'Coqueiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (542, 'BA', 'Coquinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (543, 'BA', 'Coração de Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (544, 'BA', 'Cordeiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (545, 'BA', 'Coribe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (546, 'BA', 'Coronel João Sá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (547, 'BA', 'Correntina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (548, 'BA', 'Corta Mão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (549, 'BA', 'Cotegipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (550, 'BA', 'Coutos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (551, 'BA', 'Cravolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (552, 'BA', 'Crisópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (553, 'BA', 'Cristalândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (554, 'BA', 'Cristópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (555, 'BA', 'Crussaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (556, 'BA', 'Cruz das Almas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (557, 'BA', 'Cumuruxatiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (558, 'BA', 'Cunhangi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (559, 'BA', 'Curaçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (560, 'BA', 'Curral Falso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (561, 'BA', 'Dário Meira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (562, 'BA', 'Delfino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (563, 'BA', 'Descoberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (564, 'BA', 'Dias Coelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (565, 'BA', 'Dias D Ávila');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (566, 'BA', 'Diógenes Sampaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (567, 'BA', 'Dom Basílio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (568, 'BA', 'Dom Macedo Costa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (569, 'BA', 'Dona Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (570, 'BA', 'Duas Barras do Morro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (571, 'BA', 'Elísio Medrado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (572, 'BA', 'Encruzilhada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (573, 'BA', 'Engenheiro França');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (574, 'BA', 'Engenheiro Pontes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (575, 'BA', 'Entre Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (576, 'BA', 'Érico Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (577, 'BA', 'Esplanada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (578, 'BA', 'Euclides da Cunha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (579, 'BA', 'Eunápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (580, 'BA', 'Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (581, 'BA', 'Feira da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (582, 'BA', 'Feira de Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (583, 'BA', 'Ferradas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (584, 'BA', 'Filadélfia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (585, 'BA', 'Filanésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (586, 'BA', 'Firmino Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (587, 'BA', 'Floresta Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (588, 'BA', 'Formosa do Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (589, 'BA', 'França');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (590, 'BA', 'Gabiarra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (591, 'BA', 'Galeão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (592, 'BA', 'Gamboa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (593, 'BA', 'Gameleira da Lapa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (594, 'BA', 'Gameleira do Assuruá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (595, 'BA', 'Gandu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (596, 'BA', 'Gavião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (597, 'BA', 'Gentio do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (598, 'BA', 'Geolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (599, 'BA', 'Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (600, 'BA', 'Gongogi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (601, 'BA', 'Governador João Durval Carneiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (602, 'BA', 'Governador Mangabeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (603, 'BA', 'Guagirus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (604, 'BA', 'Guaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (605, 'BA', 'Guajeru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (606, 'BA', 'Guanambi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (607, 'BA', 'Guapira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (608, 'BA', 'Guarajuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (609, 'BA', 'Guaratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (610, 'BA', 'Guerém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (611, 'BA', 'Guiné');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (612, 'BA', 'Guirapa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (613, 'BA', 'Gurupá Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (614, 'BA', 'Heliópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (615, 'BA', 'Helvecia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (616, 'BA', 'Hidrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (617, 'BA', 'Humildes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (618, 'BA', 'Iaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (619, 'BA', 'Ibatui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (620, 'BA', 'Ibiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (621, 'BA', 'Ibiajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (622, 'BA', 'Ibiapora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (623, 'BA', 'Ibiassucê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (624, 'BA', 'Ibicaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (625, 'BA', 'Ibicoara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (626, 'BA', 'Ibicuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (627, 'BA', 'Ibipeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (628, 'BA', 'Ibipetum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (629, 'BA', 'Ibipitanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (630, 'BA', 'Ibiquera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (631, 'BA', 'Ibiraba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (632, 'BA', 'Ibirajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (633, 'BA', 'Ibiranhém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (634, 'BA', 'Ibirapitanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (635, 'BA', 'Ibirapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (636, 'BA', 'Ibirataia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (637, 'BA', 'Ibitiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (638, 'BA', 'Ibitiguira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (639, 'BA', 'Ibitira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (640, 'BA', 'Ibititá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (641, 'BA', 'Ibitunane');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (642, 'BA', 'Ibitupa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (643, 'BA', 'Ibó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (644, 'BA', 'Ibotirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (645, 'BA', 'Ichu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (646, 'BA', 'Icó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (647, 'BA', 'Igaporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (648, 'BA', 'Igará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (649, 'BA', 'Igarité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (650, 'BA', 'Igatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (651, 'BA', 'Igrapiúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (652, 'BA', 'Iguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (653, 'BA', 'Iguaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (654, 'BA', 'Iguaibi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (655, 'BA', 'Iguatemi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (656, 'BA', 'Iguira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (657, 'BA', 'Iguitu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (658, 'BA', 'Ilha de Maré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (659, 'BA', 'Ilhéus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (660, 'BA', 'Indaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (661, 'BA', 'Inema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (662, 'BA', 'Inhambupe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (663, 'BA', 'Inhata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (664, 'BA', 'Inhaúmas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (665, 'BA', 'Inhobim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (666, 'BA', 'Inúbia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (667, 'BA', 'Ipecaetá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (668, 'BA', 'Ipiaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (669, 'BA', 'Ipirá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (670, 'BA', 'Ipiúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (671, 'BA', 'Ipucaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (672, 'BA', 'Ipupiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (673, 'BA', 'Irajuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (674, 'BA', 'Iramaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (675, 'BA', 'Iraporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (676, 'BA', 'Iraquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (677, 'BA', 'Irará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (678, 'BA', 'Irecê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (679, 'BA', 'Irundiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (680, 'BA', 'Ita-Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (681, 'BA', 'Itabela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (682, 'BA', 'Itaberaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (683, 'BA', 'Itabuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (684, 'BA', 'Itacaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (685, 'BA', 'Itacava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (686, 'BA', 'Itachama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (687, 'BA', 'Itacimirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (688, 'BA', 'Itaeté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (689, 'BA', 'Itagi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (690, 'BA', 'Itagibá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (691, 'BA', 'Itagimirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (692, 'BA', 'Itaguaçu da Bahia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (693, 'BA', 'Itaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (694, 'BA', 'Itaibó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (695, 'BA', 'Itaipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (696, 'BA', 'Itaítu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (697, 'BA', 'Itajaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (698, 'BA', 'Itaju do Colônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (699, 'BA', 'Itajubaquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (700, 'BA', 'Itajuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (701, 'BA', 'Itajuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (702, 'BA', 'Itamaraju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (703, 'BA', 'Itamari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (704, 'BA', 'Itambé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (705, 'BA', 'Itamira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (706, 'BA', 'Itamotinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (707, 'BA', 'Itanagé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (708, 'BA', 'Itanagra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (709, 'BA', 'Itanhém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (710, 'BA', 'Itanhi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (711, 'BA', 'Itaparica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (712, 'BA', 'Itapé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (713, 'BA', 'Itapebi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (714, 'BA', 'Itapeipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (715, 'BA', 'Itapetinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (716, 'BA', 'Itapicuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (717, 'BA', 'Itapirema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (718, 'BA', 'Itapitanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (719, 'BA', 'Itapora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (720, 'BA', 'Itapura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (721, 'BA', 'Itaquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (722, 'BA', 'Itaquaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (723, 'BA', 'Itarantim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (724, 'BA', 'Itati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (725, 'BA', 'Itatim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (726, 'BA', 'Itatingui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (727, 'BA', 'Itiruçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (728, 'BA', 'Itiúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (729, 'BA', 'Itororó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (730, 'BA', 'Ituaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (731, 'BA', 'Ituberá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (732, 'BA', 'Itupeva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (733, 'BA', 'Iuiu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (734, 'BA', 'Jaborandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (735, 'BA', 'Jacaraci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (736, 'BA', 'Jacobina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (737, 'BA', 'Jacu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (738, 'BA', 'Jacuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (739, 'BA', 'Jacuruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (740, 'BA', 'Jaguaquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (741, 'BA', 'Jaguara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (742, 'BA', 'Jaguarari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (743, 'BA', 'Jaguaripe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (744, 'BA', 'Jaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (745, 'BA', 'Jandaíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (746, 'BA', 'Japomirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (747, 'BA', 'Japu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (748, 'BA', 'Jauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (749, 'BA', 'Jequié');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (750, 'BA', 'Jequiriçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (751, 'BA', 'Jeremoabo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (752, 'BA', 'Jiribatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (753, 'BA', 'Jitaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (754, 'BA', 'João Amaro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (755, 'BA', 'João Correia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (756, 'BA', 'João Dourado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (757, 'BA', 'José Gonçalves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (758, 'BA', 'Juacema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (759, 'BA', 'Juazeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (760, 'BA', 'Jucuruçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (761, 'BA', 'Juerana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (762, 'BA', 'Junco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (763, 'BA', 'Jupaguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (764, 'BA', 'Juraci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (765, 'BA', 'Juremal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (766, 'BA', 'Jussara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (767, 'BA', 'Jussari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (768, 'BA', 'Jussiape');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (769, 'BA', 'Km Sete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (770, 'BA', 'Lafaiete Coutinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (771, 'BA', 'Lagoa Clara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (772, 'BA', 'Lagoa de Melquíades');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (773, 'BA', 'Lagoa do Boi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (774, 'BA', 'Lagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (775, 'BA', 'Lagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (776, 'BA', 'Lagoa José Luís');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (777, 'BA', 'Lagoa Preta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (778, 'BA', 'Lagoa Real');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (779, 'BA', 'Laje');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (780, 'BA', 'Laje do Banco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (781, 'BA', 'Lajedão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (782, 'BA', 'Lajedinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (783, 'BA', 'Lajedo Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (784, 'BA', 'Lajedo do Tabocal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (785, 'BA', 'Lamarão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (786, 'BA', 'Lamarão do Passe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (787, 'BA', 'Lapão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (788, 'BA', 'Largo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (789, 'BA', 'Lauro de Freitas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (790, 'BA', 'Lençóis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (791, 'BA', 'Licínio de Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (792, 'BA', 'Limoeiro do Bom Viver');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (793, 'BA', 'Livramento de Nossa Senhora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (794, 'BA', 'Lucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (795, 'BA', 'Luís Viana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (796, 'BA', 'Lustosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (797, 'BA', 'Macajuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (798, 'BA', 'Macarani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (799, 'BA', 'Macaúbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (800, 'BA', 'Macururé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (801, 'BA', 'Madre de Deus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (802, 'BA', 'Maetinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (803, 'BA', 'Maiquinique');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (804, 'BA', 'Mairi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (805, 'BA', 'Malhada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (806, 'BA', 'Malhada de Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (807, 'BA', 'Mandiroba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (808, 'BA', 'Mangue Seco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (809, 'BA', 'Maniaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (810, 'BA', 'Manoel Vitorino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (811, 'BA', 'Mansidão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (812, 'BA', 'Mantiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (813, 'BA', 'Mar Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (814, 'BA', 'Maracás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (815, 'BA', 'Maragogipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (816, 'BA', 'Maragogipinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (817, 'BA', 'Maraú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (818, 'BA', 'Marcionílio Souza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (819, 'BA', 'Marcolino Moura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (820, 'BA', 'Maria Quitéria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (821, 'BA', 'Maricoabo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (822, 'BA', 'Mariquita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (823, 'BA', 'Mascote');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (824, 'BA', 'Massacara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (825, 'BA', 'Massaroca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (826, 'BA', 'Mata da Aliança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (827, 'BA', 'Mata de São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (828, 'BA', 'Mataripe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (829, 'BA', 'Matina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (830, 'BA', 'Matinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (831, 'BA', 'Medeiros Neto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (832, 'BA', 'Miguel Calmon');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (833, 'BA', 'Milagres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (834, 'BA', 'Luís Eduardo Magalhães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (835, 'BA', 'Minas do Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (836, 'BA', 'Minas do Mimoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (837, 'BA', 'Mirandela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (838, 'BA', 'Miranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (839, 'BA', 'Mirangaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (840, 'BA', 'Mirante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (841, 'BA', 'Mocambo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (842, 'BA', 'Mogiquiçaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (843, 'BA', 'Monte Cruzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (844, 'BA', 'Monte Gordo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (845, 'BA', 'Monte Recôncavo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (846, 'BA', 'Monte Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (847, 'BA', 'Morpará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (848, 'BA', 'Morrinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (849, 'BA', 'Morro das Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (850, 'BA', 'Morro de São Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (851, 'BA', 'Morro do Chapéu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (852, 'BA', 'Mortugaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (853, 'BA', 'Mucugê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (854, 'BA', 'Mucuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (855, 'BA', 'Mulungu do Morro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (856, 'BA', 'Mundo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (857, 'BA', 'Muniz Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (858, 'BA', 'Muquém de São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (859, 'BA', 'Muritiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (860, 'BA', 'Mutas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (861, 'BA', 'Mutuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (862, 'BA', 'Nagé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (863, 'BA', 'Narandiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (864, 'BA', 'Nazaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (865, 'BA', 'Nilo Peçanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (866, 'BA', 'Nordestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (867, 'BA', 'Nova Alegria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (868, 'BA', 'Nova Brasília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (869, 'BA', 'Nova Canaã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (870, 'BA', 'Nova Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (871, 'BA', 'Nova Ibiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (872, 'BA', 'Nova Itaipê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (873, 'BA', 'Nova Itarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (874, 'BA', 'Nova Lídice');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (875, 'BA', 'Nova Redenção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (876, 'BA', 'Nova Soure');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (877, 'BA', 'Nova Viçosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (878, 'BA', 'Novo Acre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (879, 'BA', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (880, 'BA', 'Novo Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (881, 'BA', 'Núcleo Residencial Pilar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (882, 'BA', 'Nuguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (883, 'BA', 'Olhos D Água do Seco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (884, 'BA', 'Olhos D Água do Serafim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (885, 'BA', 'Olindina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (886, 'BA', 'Oliveira dos Brejinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (887, 'BA', 'Olivença');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (888, 'BA', 'Onha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (889, 'BA', 'Oriente Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (890, 'BA', 'Ouricana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (891, 'BA', 'Ouriçangas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (892, 'BA', 'Ouricuri do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (893, 'BA', 'Ourolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (894, 'BA', 'Outeiro Redondo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (895, 'BA', 'Paiol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (896, 'BA', 'Pajeú do Vento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (897, 'BA', 'Palame');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (898, 'BA', 'Palmas de Monte Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (899, 'BA', 'Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (900, 'BA', 'Parafuso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (901, 'BA', 'Paramirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (902, 'BA', 'Parateca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (903, 'BA', 'Paratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (904, 'BA', 'Paripiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (905, 'BA', 'Pataíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (906, 'BA', 'Patamuté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (907, 'BA', 'Pau a Pique');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (908, 'BA', 'Pau Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (909, 'BA', 'Paulo Afonso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (910, 'BA', 'Pé de Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (911, 'BA', 'Pedrão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (912, 'BA', 'Pedras Altas do Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (913, 'BA', 'Pedro Alexandre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (914, 'BA', 'Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (915, 'BA', 'Petim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (916, 'BA', 'Piabanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (917, 'BA', 'Piatã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (918, 'BA', 'Piçarrão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (919, 'BA', 'Pilão Arcado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (920, 'BA', 'Pimenteira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (921, 'BA', 'Pindaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (922, 'BA', 'Pindobaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (923, 'BA', 'Pinhões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (924, 'BA', 'Pintadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (925, 'BA', 'Piragi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (926, 'BA', 'Piraí do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (927, 'BA', 'Pirajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (928, 'BA', 'Pirajuia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (929, 'BA', 'Piri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (930, 'BA', 'Piripá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (931, 'BA', 'Piritiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (932, 'BA', 'Pituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (933, 'BA', 'Planaltino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (934, 'BA', 'Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (935, 'BA', 'Poço Central');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (936, 'BA', 'Poço de Fora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (937, 'BA', 'Poções');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (938, 'BA', 'Poços');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (939, 'BA', 'Pojuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (940, 'BA', 'Polo Petroquímico de Camaçari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (941, 'BA', 'Ponta da Areia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (942, 'BA', 'Ponto Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (943, 'BA', 'Porto Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (944, 'BA', 'Porto Seguro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (945, 'BA', 'Posto da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (946, 'BA', 'Potiraguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (947, 'BA', 'Poxim do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (948, 'BA', 'Prado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (949, 'BA', 'Presidente Dutra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (950, 'BA', 'Presidente Jânio Quadros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (951, 'BA', 'Presidente Tancredo Neves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (952, 'BA', 'Prevenido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (953, 'BA', 'Quaraçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (954, 'BA', 'Queimadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (955, 'BA', 'Quijingue');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (956, 'BA', 'Quixabá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (957, 'BA', 'Quixabeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (958, 'BA', 'Rafael Jambeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (959, 'BA', 'Recife');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (960, 'BA', 'Remanso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (961, 'BA', 'Remédios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (962, 'BA', 'Retirolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (963, 'BA', 'Riachão das Neves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (964, 'BA', 'Riachão do Jacuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (965, 'BA', 'Riachão do Utinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (966, 'BA', 'Riacho da Guia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (967, 'BA', 'Riacho de Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (968, 'BA', 'Riacho Seco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (969, 'BA', 'Ribeira do Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (970, 'BA', 'Ribeira do Pombal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (971, 'BA', 'Ribeirão do Largo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (972, 'BA', 'Ribeirão do Salto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (973, 'BA', 'Rio da Dona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (974, 'BA', 'Rio de Contas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (975, 'BA', 'Rio do Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (976, 'BA', 'Rio do Braço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (977, 'BA', 'Rio do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (978, 'BA', 'Rio do Pires');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (979, 'BA', 'Rio Fundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (980, 'BA', 'Rio Real');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (981, 'BA', 'Rodelas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (982, 'BA', 'Ruy Barbosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (983, 'BA', 'Saldanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (984, 'BA', 'Salgadália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (985, 'BA', 'Salinas da Margarida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (986, 'BA', 'Salobrinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (987, 'BA', 'Salobro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (988, 'BA', 'Salvador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (989, 'BA', 'Sambaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (990, 'BA', 'Santa Bárbara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (991, 'BA', 'Santa Brígida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (992, 'BA', 'Santa Cruz Cabrália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (993, 'BA', 'Santa Cruz da Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (994, 'BA', 'Santa Inês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (995, 'BA', 'Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (996, 'BA', 'Santa Maria da Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (997, 'BA', 'Santa Rita de Cássia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (998, 'BA', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (999, 'BA', 'Santaluz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1000, 'BA', 'Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1001, 'BA', 'Santana do Sobrado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1002, 'BA', 'Santanópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1003, 'BA', 'Santiago do Iguapé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1004, 'BA', 'Santo Amaro');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 1000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1005, 'BA', 'Santo Antônio de Barcelona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1006, 'BA', 'Santo Antônio de Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1007, 'BA', 'Santo Estevão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1008, 'BA', 'Santo Inácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1009, 'BA', 'São Desidério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1010, 'BA', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1011, 'BA', 'São Felipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1012, 'BA', 'São Félix');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1013, 'BA', 'São Félix do Coribe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1014, 'BA', 'São Francisco do Conde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1015, 'BA', 'São Gabriel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1016, 'BA', 'São Gonçalo dos Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1017, 'BA', 'São João da Fortaleza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1018, 'BA', 'São João da Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1019, 'BA', 'São José da Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1020, 'BA', 'São José do Colônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1021, 'BA', 'São José do Jacuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1022, 'BA', 'São José do Prado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1023, 'BA', 'São José do Rio Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1024, 'BA', 'São Miguel das Matas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1025, 'BA', 'São Paulinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1026, 'BA', 'São Roque do Paraguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1027, 'BA', 'São Sebastião do Passe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1028, 'BA', 'São Timóteo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1029, 'BA', 'Sapeaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1030, 'BA', 'Sátiro Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1031, 'BA', 'Saubara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1032, 'BA', 'Saudável');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1033, 'BA', 'Saúde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1034, 'BA', 'Seabra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1035, 'BA', 'Sebastião Laranjeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1036, 'BA', 'Senhor do Bonfim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1037, 'BA', 'Sento Sé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1038, 'BA', 'Sergi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1039, 'BA', 'Serra da Canabrava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1040, 'BA', 'Serra do Ramalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1041, 'BA', 'Serra Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1042, 'BA', 'Serra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1043, 'BA', 'Serra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1044, 'BA', 'Serra Preta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1045, 'BA', 'Serrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1046, 'BA', 'Serrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1047, 'BA', 'Simões Filho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1048, 'BA', 'Sítio da Baraúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1049, 'BA', 'Sítio do Mato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1050, 'BA', 'Sítio do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1051, 'BA', 'Sítio do Quinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1052, 'BA', 'Sítio Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1053, 'BA', 'Sítio Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1054, 'BA', 'Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1055, 'BA', 'Sobradinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1056, 'BA', 'Souto Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1057, 'BA', 'Subaúma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1058, 'BA', 'Sussuarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1059, 'BA', 'Tabocas do Brejo Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1060, 'BA', 'Taboleiro do Castro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1061, 'BA', 'Taboquinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1062, 'BA', 'Taguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1063, 'BA', 'Tamburil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1064, 'BA', 'Tanhaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1065, 'BA', 'Tanque Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1066, 'BA', 'Tanquinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1067, 'BA', 'Tanquinho do Poço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1068, 'BA', 'Taperoá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1069, 'BA', 'Tapiraípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1070, 'BA', 'Tapirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1071, 'BA', 'Tapiramutá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1072, 'BA', 'Tapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1073, 'BA', 'Tapúia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1074, 'BA', 'Taquarendi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1075, 'BA', 'Taquarinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1076, 'BA', 'Tartaruga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1077, 'BA', 'Tauapé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1078, 'BA', 'Teixeira de Freitas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1079, 'BA', 'Teodoro Sampaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1080, 'BA', 'Teofilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1081, 'BA', 'Teolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1082, 'BA', 'Terra Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1083, 'BA', 'Tijuaçú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1084, 'BA', 'Tiquaruçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1085, 'BA', 'Tremedal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1086, 'BA', 'Triunfo do Sincorá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1087, 'BA', 'Tucano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1088, 'BA', 'Uauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1089, 'BA', 'Ubaíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1090, 'BA', 'Ubaitaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1091, 'BA', 'Ubatã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1092, 'BA', 'Ubiracaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1093, 'BA', 'Ubiraitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1094, 'BA', 'Uibaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1095, 'BA', 'Umburanas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1096, 'BA', 'Umbuzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1097, 'BA', 'Una');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1098, 'BA', 'Urandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1099, 'BA', 'Uruçuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1100, 'BA', 'Utinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1101, 'BA', 'Vale Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1102, 'BA', 'Valença');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1103, 'BA', 'Valente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1104, 'BA', 'Várzea da Roça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1105, 'BA', 'Várzea do Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1106, 'BA', 'Várzea do Cerco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1107, 'BA', 'Várzea do Poço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1108, 'BA', 'Várzea Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1109, 'BA', 'Várzeas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1110, 'BA', 'Varzedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1111, 'BA', 'Velha Boipeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1112, 'BA', 'Ventura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1113, 'BA', 'Vera Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1114, 'BA', 'Vereda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1115, 'BA', 'Vila do Café');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1116, 'BA', 'Vitória da Conquista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1117, 'BA', 'Volta Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1118, 'BA', 'Wagner');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1119, 'BA', 'Wanderley');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1120, 'BA', 'Wenceslau Guimarães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1121, 'BA', 'Xique-Xique');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1122, 'CE', 'Abaiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1123, 'CE', 'Abílio Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1124, 'CE', 'Acarape');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1125, 'CE', 'Acaraú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1126, 'CE', 'Acopiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1127, 'CE', 'Adrianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1128, 'CE', 'Água Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1129, 'CE', 'Aguaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1130, 'CE', 'Aiuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1131, 'CE', 'Aiuaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1132, 'CE', 'Alagoinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1133, 'CE', 'Alagoinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1134, 'CE', 'Alcântaras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1135, 'CE', 'Algodões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1136, 'CE', 'Almofala');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1137, 'CE', 'Altaneira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1138, 'CE', 'Alto Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1139, 'CE', 'Amanaiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1140, 'CE', 'Amanari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1141, 'CE', 'Amaniutuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1142, 'CE', 'Amarelas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1143, 'CE', 'Amaro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1144, 'CE', 'América');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1145, 'CE', 'Amontada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1146, 'CE', 'Anauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1147, 'CE', 'Aningás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1148, 'CE', 'Anjinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1149, 'CE', 'Antonina do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1150, 'CE', 'Antônio Bezerra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1151, 'CE', 'Antônio Diogo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1152, 'CE', 'Antônio Marques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1153, 'CE', 'Aprazível');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1154, 'CE', 'Apuiarés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1155, 'CE', 'Aquinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1156, 'CE', 'Aquiraz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1157, 'CE', 'Araçás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1158, 'CE', 'Aracati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1159, 'CE', 'Aracatiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1160, 'CE', 'Aracatiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1161, 'CE', 'Aracoiaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1162, 'CE', 'Arajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1163, 'CE', 'Aranaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1164, 'CE', 'Arapá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1165, 'CE', 'Arapari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1166, 'CE', 'Araporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1167, 'CE', 'Araquém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1168, 'CE', 'Ararendá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1169, 'CE', 'Araripe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1170, 'CE', 'Arariús');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1171, 'CE', 'Aratama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1172, 'CE', 'Araticum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1173, 'CE', 'Aratuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1174, 'CE', 'Areial');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1175, 'CE', 'Ariscos dos Marianos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1176, 'CE', 'Arneiroz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1177, 'CE', 'Aroeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1178, 'CE', 'Arrojado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1179, 'CE', 'Aruaru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1180, 'CE', 'Assaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1181, 'CE', 'Assunção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1182, 'CE', 'Assunção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1183, 'CE', 'Aurora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1184, 'CE', 'Baixa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1185, 'CE', 'Baixio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1186, 'CE', 'Baixio da Donana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1187, 'CE', 'Banabuiú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1188, 'CE', 'Bandeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1189, 'CE', 'Barão de Aquiraz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1190, 'CE', 'Barbalha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1191, 'CE', 'Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1192, 'CE', 'Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1193, 'CE', 'Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1194, 'CE', 'Barra do Sotero');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1195, 'CE', 'Barra Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1196, 'CE', 'Barreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1197, 'CE', 'Barreira dos Vianas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1198, 'CE', 'Barreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1199, 'CE', 'Barreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1200, 'CE', 'Barreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1201, 'CE', 'Barreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1202, 'CE', 'Barrento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1203, 'CE', 'Barro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1204, 'CE', 'Barro Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1205, 'CE', 'Barroquinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1206, 'CE', 'Baturité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1207, 'CE', 'Baú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1208, 'CE', 'Beberibe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1209, 'CE', 'Bela Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1210, 'CE', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1211, 'CE', 'Betânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1212, 'CE', 'Betânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1213, 'CE', 'Betânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1214, 'CE', 'Bitupitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1215, 'CE', 'Bixopa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1216, 'CE', 'Boa Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1217, 'CE', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1218, 'CE', 'Boa Viagem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1219, 'CE', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1220, 'CE', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1221, 'CE', 'Boa Vista do Caxitoré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1222, 'CE', 'Bonfim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1223, 'CE', 'Bonfim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1224, 'CE', 'Bonhu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1225, 'CE', 'Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1226, 'CE', 'Borges');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1227, 'CE', 'Brejinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1228, 'CE', 'Brejo Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1229, 'CE', 'Brejo Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1230, 'CE', 'Brotas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1231, 'CE', 'Buritizal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1232, 'CE', 'Buritizinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1233, 'CE', 'Cabreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1234, 'CE', 'Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1235, 'CE', 'Cachoeira Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1236, 'CE', 'Caiçara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1237, 'CE', 'Caiçarinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1238, 'CE', 'Caio Prado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1239, 'CE', 'Caioca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1240, 'CE', 'Caipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1241, 'CE', 'Calabaça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1242, 'CE', 'Caldeirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1243, 'CE', 'Califórnia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1244, 'CE', 'Camará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1245, 'CE', 'Camboas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1246, 'CE', 'Camilos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1247, 'CE', 'Camocim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1248, 'CE', 'Campanário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1249, 'CE', 'Campestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1250, 'CE', 'Campos Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1251, 'CE', 'Canaan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1252, 'CE', 'Canafistula');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1253, 'CE', 'Canafistula');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1254, 'CE', 'Cangati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1255, 'CE', 'Cangati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1256, 'CE', 'Canindé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1257, 'CE', 'Canindezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1258, 'CE', 'Canindezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1259, 'CE', 'Canindezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1260, 'CE', 'Capistrano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1261, 'CE', 'Caponga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1262, 'CE', 'Caponga da Bernarda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1263, 'CE', 'Caracará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1264, 'CE', 'Caridade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1265, 'CE', 'Cariré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1266, 'CE', 'Caririaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1267, 'CE', 'Cariús');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1268, 'CE', 'Cariutaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1269, 'CE', 'Carmelópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1270, 'CE', 'Carnaubal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1271, 'CE', 'Carnaúbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1272, 'CE', 'Carnaubinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1273, 'CE', 'Carquejo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1274, 'CE', 'Carrapateiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1275, 'CE', 'Caruataí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1276, 'CE', 'Carvalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1277, 'CE', 'Carvoeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1278, 'CE', 'Cascavel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1279, 'CE', 'Castanhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1280, 'CE', 'Catarina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1281, 'CE', 'Catolé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1282, 'CE', 'Catuana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1283, 'CE', 'Catunda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1284, 'CE', 'Caucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1285, 'CE', 'Caxitoré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1286, 'CE', 'Caxitoré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1287, 'CE', 'Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1288, 'CE', 'Cemoaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1289, 'CE', 'Chaval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1290, 'CE', 'Choró');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1291, 'CE', 'Chorozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1292, 'CE', 'Cipó dos Anjos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1293, 'CE', 'Cococi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1294, 'CE', 'Codiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1295, 'CE', 'Coité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1296, 'CE', 'Colina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1297, 'CE', 'Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1298, 'CE', 'Coreaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1299, 'CE', 'Córrego dos Fernandes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1300, 'CE', 'Crateús');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1301, 'CE', 'Crato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1302, 'CE', 'Crioulos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1303, 'CE', 'Cristais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1304, 'CE', 'Croatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1305, 'CE', 'Croatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1306, 'CE', 'Croatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1307, 'CE', 'Cruxati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1308, 'CE', 'Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1309, 'CE', 'Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1310, 'CE', 'Cruz de Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1311, 'CE', 'Cruzeirinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1312, 'CE', 'Cuncas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1313, 'CE', 'Curatis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1314, 'CE', 'Curupira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1315, 'CE', 'Custódio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1316, 'CE', 'Daniel de Queirós');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1317, 'CE', 'Delmiro Gouveia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1318, 'CE', 'Deputado Irapuan Pinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1319, 'CE', 'Deserto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1320, 'CE', 'Dom Leme');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1321, 'CE', 'Dom Maurício');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1322, 'CE', 'Dom Quintino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1323, 'CE', 'Domingos da Costa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1324, 'CE', 'Donato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1325, 'CE', 'Dourado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1326, 'CE', 'Ebron');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1327, 'CE', 'Ema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1328, 'CE', 'Ematuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1329, 'CE', 'Encantado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1330, 'CE', 'Engenheiro João Tomé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1331, 'CE', 'Engenheiro José Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1332, 'CE', 'Engenho Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1333, 'CE', 'Ererê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1334, 'CE', 'Espacinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1335, 'CE', 'Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1336, 'CE', 'Várzea dos Espinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1337, 'CE', 'Eusébio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1338, 'CE', 'Farias Brito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1339, 'CE', 'Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1340, 'CE', 'Feiticeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1341, 'CE', 'Feitosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1342, 'CE', 'Felizardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1343, 'CE', 'Flamengo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1344, 'CE', 'Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1345, 'CE', 'Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1346, 'CE', 'Forquilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1347, 'CE', 'Fortaleza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1348, 'CE', 'Fortim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1349, 'CE', 'Frecheirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1350, 'CE', 'Gado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1351, 'CE', 'Gado dos Rodrigues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1352, 'CE', 'Gameleira de São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1353, 'CE', 'Garças');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1354, 'CE', 'Gázea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1355, 'CE', 'General Sampaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1356, 'CE', 'General Tibúrcio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1357, 'CE', 'Jenipapeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1358, 'CE', 'Gereraú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1359, 'CE', 'Giqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1360, 'CE', 'Girau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1361, 'CE', 'Graça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1362, 'CE', 'Granja');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1363, 'CE', 'Granjeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1364, 'CE', 'Groairas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1365, 'CE', 'Guaiúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1366, 'CE', 'Guajiru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1367, 'CE', 'Guanacés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1368, 'CE', 'Guaraciaba do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1369, 'CE', 'Guaramiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1370, 'CE', 'Guararu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1371, 'CE', 'Guassi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1372, 'CE', 'Guassossé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1373, 'CE', 'Guia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1374, 'CE', 'Guriú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1375, 'CE', 'Hidrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1376, 'CE', 'Holanda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1377, 'CE', 'Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1378, 'CE', 'Iapi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1379, 'CE', 'Iara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1380, 'CE', 'Ibaretama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1381, 'CE', 'Ibiapaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1382, 'CE', 'Ibiapina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1383, 'CE', 'Ibicatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1384, 'CE', 'Ibicuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1385, 'CE', 'Ibicuitaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1386, 'CE', 'Ibicuitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1387, 'CE', 'Iborepi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1388, 'CE', 'Ibuaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1389, 'CE', 'Ibuguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1390, 'CE', 'Icapuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1391, 'CE', 'Icaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1392, 'CE', 'Icó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1393, 'CE', 'Icozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1394, 'CE', 'Ideal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1395, 'CE', 'Igaroi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1396, 'CE', 'Iguatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1397, 'CE', 'Independência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1398, 'CE', 'Ingazeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1399, 'CE', 'Inhamuns');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1400, 'CE', 'Inhuçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1401, 'CE', 'Inhuporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1402, 'CE', 'Ipaporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1403, 'CE', 'Ipaumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1404, 'CE', 'Ipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1405, 'CE', 'Ipueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1406, 'CE', 'Ipueiras dos Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1407, 'CE', 'Iracema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1408, 'CE', 'Irajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1409, 'CE', 'Irapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1410, 'CE', 'Iratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1411, 'CE', 'Irauçuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1412, 'CE', 'Isidoro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1413, 'CE', 'Itacima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1414, 'CE', 'Itaguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1415, 'CE', 'Itaiçaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1416, 'CE', 'Itaipaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1417, 'CE', 'Itaitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1418, 'CE', 'Itans');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1419, 'CE', 'Itapajé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1420, 'CE', 'Itapebussu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1421, 'CE', 'Itapeim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1422, 'CE', 'Itapipoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1423, 'CE', 'Itapiúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1424, 'CE', 'Itapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1425, 'CE', 'Itarema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1426, 'CE', 'Itatira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1427, 'CE', 'Jaburuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1428, 'CE', 'Jacampari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1429, 'CE', 'Jacarecoara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1430, 'CE', 'Jacaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1431, 'CE', 'Jaguarão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1432, 'CE', 'Jaguaretama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1433, 'CE', 'Jaguaribara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1434, 'CE', 'Jaguaribe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1435, 'CE', 'Jaguaruana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1436, 'CE', 'Jaibaras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1437, 'CE', 'Jamacaru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1438, 'CE', 'Jandrangoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1439, 'CE', 'Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1440, 'CE', 'Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1441, 'CE', 'Jardimirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1442, 'CE', 'Jati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1443, 'CE', 'Jijoca de Jericoacoara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1444, 'CE', 'João Cordeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1445, 'CE', 'Jordão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1446, 'CE', 'José de Alencar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1447, 'CE', 'Juá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1448, 'CE', 'Juá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1449, 'CE', 'Juatama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1450, 'CE', 'Juazeiro de Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1451, 'CE', 'Juazeiro do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1452, 'CE', 'Jubaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1453, 'CE', 'Jucás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1454, 'CE', 'Jurema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1455, 'CE', 'Justiniano Serpa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1456, 'CE', 'Lacerda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1457, 'CE', 'Ladeira Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1458, 'CE', 'Lagoa de São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1459, 'CE', 'Lagoa do Juvenal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1460, 'CE', 'Lagoa do Mato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1461, 'CE', 'Lagoa dos Crioulos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1462, 'CE', 'Lagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1463, 'CE', 'Lagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1464, 'CE', 'Lagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1465, 'CE', 'Lagoinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1466, 'CE', 'Lambedouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1467, 'CE', 'Lameiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1468, 'CE', 'Lavras da Mangabeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1469, 'CE', 'Lima Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1470, 'CE', 'Limoeiro do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1471, 'CE', 'Lisieux');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1472, 'CE', 'Livramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1473, 'CE', 'Logradouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1474, 'CE', 'Macambira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1475, 'CE', 'Macaóca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1476, 'CE', 'Macaraú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1477, 'CE', 'Maceió');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1478, 'CE', 'Madalena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1479, 'CE', 'Major Simplício');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1480, 'CE', 'Majorlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1481, 'CE', 'Malhada Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1482, 'CE', 'Mangabeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1483, 'CE', 'Manibu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1484, 'CE', 'Manituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1485, 'CE', 'Mapuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1486, 'CE', 'Maracanaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1487, 'CE', 'Maraguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1488, 'CE', 'Maranguape');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1489, 'CE', 'Mararupá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1490, 'CE', 'Marco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1491, 'CE', 'Marinheiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1492, 'CE', 'Marrecas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1493, 'CE', 'Marrocos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1494, 'CE', 'Marruás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1495, 'CE', 'Martinópole');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1496, 'CE', 'Massapê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1497, 'CE', 'Mata Fresca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1498, 'CE', 'Matias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1499, 'CE', 'Matriz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1500, 'CE', 'Mauriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1501, 'CE', 'Mel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1502, 'CE', 'Meruoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1503, 'CE', 'Messejana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1504, 'CE', 'Miguel Xavier');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 1500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1505, 'CE', 'Milagres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1506, 'CE', 'Milhã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1507, 'CE', 'Milton Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1508, 'CE', 'Mineirolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1509, 'CE', 'Miragem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1510, 'CE', 'Miraíma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1511, 'CE', 'Mirambé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1512, 'CE', 'Missão Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1513, 'CE', 'Missão Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1514, 'CE', 'Missi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1515, 'CE', 'Moitas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1516, 'CE', 'Mombaça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1517, 'CE', 'Mondubim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1518, 'CE', 'Monguba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1519, 'CE', 'Monsenhor Tabosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1520, 'CE', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1521, 'CE', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1522, 'CE', 'Monte Castelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1523, 'CE', 'Monte Grave');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1524, 'CE', 'Monte Sion');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1525, 'CE', 'Montenebo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1526, 'CE', 'Morada Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1527, 'CE', 'Moraújo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1528, 'CE', 'Morrinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1529, 'CE', 'Morrinhos Novos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1530, 'CE', 'Morro Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1531, 'CE', 'Mucambo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1532, 'CE', 'Mulungu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1533, 'CE', 'Mulungu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1534, 'CE', 'Mumbaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1535, 'CE', 'Mundau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1536, 'CE', 'Muribeca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1537, 'CE', 'Muriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1538, 'CE', 'Mutambeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1539, 'CE', 'Naraniu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1540, 'CE', 'Nascente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1541, 'CE', 'Nenenlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1542, 'CE', 'Nossa Senhora do Livramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1543, 'CE', 'Nova Betânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1544, 'CE', 'Nova Betânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1545, 'CE', 'Nova Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1546, 'CE', 'Nova Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1547, 'CE', 'Nova Olinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1548, 'CE', 'Nova Russas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1549, 'CE', 'Nova Vida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1550, 'CE', 'Novo Assis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1551, 'CE', 'Novo Oriente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1552, 'CE', 'Novo Oriente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1553, 'CE', 'Ocara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1554, 'CE', 'Oiticica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1555, 'CE', 'Oiticica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1556, 'CE', 'Olho-D Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1557, 'CE', 'Olho-D Água da Bica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1558, 'CE', 'Oliveiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1559, 'CE', 'Orós');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1560, 'CE', 'Pacajus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1561, 'CE', 'Pacatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1562, 'CE', 'Pacoti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1563, 'CE', 'Pacujá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1564, 'CE', 'Padre Cícero');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1565, 'CE', 'Padre Linhares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1566, 'CE', 'Padre Vieira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1567, 'CE', 'Pajeú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1569, 'CE', 'Palestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1570, 'CE', 'Palestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1571, 'CE', 'Palestina do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1572, 'CE', 'Palhano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1573, 'CE', 'Palmácia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1574, 'CE', 'Palmatória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1575, 'CE', 'Panacuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1576, 'CE', 'Paracuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1577, 'CE', 'Paracuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1578, 'CE', 'Paraipaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1579, 'CE', 'Parajuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1580, 'CE', 'Parambu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1581, 'CE', 'Paramoti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1582, 'CE', 'Parangaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1583, 'CE', 'Parapuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1584, 'CE', 'Parazinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1585, 'CE', 'Paripueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1586, 'CE', 'Passagem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1587, 'CE', 'Passagem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1588, 'CE', 'Passagem Funda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1589, 'CE', 'Pasta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1590, 'CE', 'Patacas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1591, 'CE', 'Patriarca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1592, 'CE', 'Pavuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1593, 'CE', 'Pecém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1594, 'CE', 'Pedra Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1595, 'CE', 'Pedra Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1596, 'CE', 'Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1597, 'CE', 'Pedrinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1598, 'CE', 'Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1599, 'CE', 'Peixe Gordo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1600, 'CE', 'Penaforte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1601, 'CE', 'Pentecoste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1602, 'CE', 'Pereiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1603, 'CE', 'Pernambuquinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1604, 'CE', 'Pessoa Anta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1605, 'CE', 'Pindoguaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1606, 'CE', 'Pindoretama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1607, 'CE', 'Pio X');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1608, 'CE', 'Piquet Carneiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1609, 'CE', 'Pirabibu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1610, 'CE', 'Pirangi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1611, 'CE', 'Pires Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1612, 'CE', 'Pitombeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1613, 'CE', 'Pitombeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1614, 'CE', 'Plácido Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1615, 'CE', 'Poço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1616, 'CE', 'Poço Comprido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1617, 'CE', 'Poço Comprido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1618, 'CE', 'Poço Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1619, 'CE', 'Podimirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1620, 'CE', 'Ponta da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1621, 'CE', 'Poranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1622, 'CE', 'Porfirio Sampaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1623, 'CE', 'Porteiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1624, 'CE', 'Potengi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1625, 'CE', 'Poti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1626, 'CE', 'Potiretama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1627, 'CE', 'Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1628, 'CE', 'Prudente de Morais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1629, 'CE', 'Quatiguaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1630, 'CE', 'Queimados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1631, 'CE', 'Quimami');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1632, 'CE', 'Quincoé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1633, 'CE', 'Quincuncá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1634, 'CE', 'Quitaiús');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1635, 'CE', 'Quiterianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1636, 'CE', 'Quixadá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1637, 'CE', 'Quixariú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1638, 'CE', 'Quixelô');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1639, 'CE', 'Quixeramobim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1640, 'CE', 'Quixeré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1641, 'CE', 'Gadelha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1642, 'CE', 'Raimundo Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1643, 'CE', 'Redenção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1644, 'CE', 'Reriutaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1645, 'CE', 'Riachão do Banabuiú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1646, 'CE', 'Riacho Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1647, 'CE', 'Riacho Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1648, 'CE', 'Riacho Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1649, 'CE', 'Rinaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1650, 'CE', 'Roldão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1651, 'CE', 'Russas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1652, 'CE', 'Sabiaguaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1653, 'CE', 'Saboeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1654, 'CE', 'Sacramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1655, 'CE', 'Salão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1656, 'CE', 'Salitre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1657, 'CE', 'Sambaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1658, 'CE', 'Santa Ana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1659, 'CE', 'Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1660, 'CE', 'Santa Felícia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1661, 'CE', 'Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1662, 'CE', 'Santa Quitéria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1663, 'CE', 'Santa Tereza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1664, 'CE', 'Santa Tereza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1665, 'CE', 'Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1666, 'CE', 'Santana do Acaraú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1667, 'CE', 'Santana do Cariri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1668, 'CE', 'Santarém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1669, 'CE', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1670, 'CE', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1671, 'CE', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1672, 'CE', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1673, 'CE', 'Santo Antônio da Pindoba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1674, 'CE', 'Santo Antônio dos Fernandes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1675, 'CE', 'São Bartolomeu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1676, 'CE', 'São Benedito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1677, 'CE', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1678, 'CE', 'São Felipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1679, 'CE', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1680, 'CE', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1681, 'CE', 'São Gerardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1682, 'CE', 'São Gonçalo do Amarante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1683, 'CE', 'São Gonçalo do Umari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1684, 'CE', 'São João de Deus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1685, 'CE', 'São João do Jaguaribe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1686, 'CE', 'São João dos Queiroz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1687, 'CE', 'São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1688, 'CE', 'São Joaquim do Salgado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1689, 'CE', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1690, 'CE', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1691, 'CE', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1692, 'CE', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1693, 'CE', 'São José das Lontras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1694, 'CE', 'São José de Solonópole');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1695, 'CE', 'São José do Torto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1696, 'CE', 'São Luís do Curu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1697, 'CE', 'São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1698, 'CE', 'São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1699, 'CE', 'São Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1700, 'CE', 'São Pedro do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1701, 'CE', 'São Pedro do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1702, 'CE', 'São Romão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1703, 'CE', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1704, 'CE', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1705, 'CE', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1706, 'CE', 'Sapo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1707, 'CE', 'Sapupara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1708, 'CE', 'Sebastião de Abreu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1709, 'CE', 'Senador Carlos Jereissati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1710, 'CE', 'Senador Pompeu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1711, 'CE', 'Senador Sá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1712, 'CE', 'Sereno de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1713, 'CE', 'Serra do Félix');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1714, 'CE', 'Serragem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1715, 'CE', 'Serrota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1716, 'CE', 'Serrota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1717, 'CE', 'Serrote');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1718, 'CE', 'Serrote');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1719, 'CE', 'Sitiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1720, 'CE', 'Sítios Novos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1721, 'CE', 'Siupê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1722, 'CE', 'Sobral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1723, 'CE', 'Soledade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1724, 'CE', 'Solonópole');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1725, 'CE', 'Suassurana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1726, 'CE', 'Sucatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1727, 'CE', 'Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1728, 'CE', 'Sussuanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1729, 'CE', 'Tabainha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1730, 'CE', 'Taboleiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1731, 'CE', 'Tabuleiro do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1732, 'CE', 'Taíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1733, 'CE', 'Tamboril');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1734, 'CE', 'Tanques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1735, 'CE', 'Tapera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1736, 'CE', 'Taperuaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1737, 'CE', 'Tapuiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1738, 'CE', 'Targinos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1739, 'CE', 'Tarrafas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1740, 'CE', 'Tauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1741, 'CE', 'Tejuçuoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1742, 'CE', 'Tianguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1743, 'CE', 'Timonha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1744, 'CE', 'Tipi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1745, 'CE', 'Tomé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1746, 'CE', 'Trairi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1747, 'CE', 'Trapiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1748, 'CE', 'Trapiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1749, 'CE', 'Trici');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1750, 'CE', 'Tróia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1751, 'CE', 'Trussu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1752, 'CE', 'Tucunduba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1753, 'CE', 'Tucuns');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1754, 'CE', 'Tuína');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1755, 'CE', 'Tururu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1756, 'CE', 'Ubajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1757, 'CE', 'Ubaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1758, 'CE', 'Ubiraçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1759, 'CE', 'Uiraponga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1760, 'CE', 'Umari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1761, 'CE', 'Umarituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1762, 'CE', 'Umburanas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1763, 'CE', 'Umirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1764, 'CE', 'Uruburetama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1765, 'CE', 'Uruoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1766, 'CE', 'Uruquê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1767, 'CE', 'Varjota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1768, 'CE', 'Várzea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1769, 'CE', 'Várzea Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1770, 'CE', 'Várzea da Volta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1771, 'CE', 'Várzea do Gilo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1772, 'CE', 'Vazantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1773, 'CE', 'Ventura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1774, 'CE', 'Vertentes do Lagedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1775, 'CE', 'Viçosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1776, 'CE', 'Viçosa do Ceará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1777, 'CE', 'Vila Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1778, 'DF', 'Brasília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1779, 'DF', 'Brazlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1780, 'DF', 'Candangolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1781, 'DF', 'Ceilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1782, 'DF', 'Cruzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1783, 'DF', 'Gama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1784, 'DF', 'Guará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1785, 'DF', 'Lago Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1786, 'DF', 'Lago Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1787, 'DF', 'Núcleo Bandeirante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1788, 'DF', 'Paranoá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1789, 'DF', 'Planaltina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1790, 'DF', 'Recanto das Emas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1791, 'DF', 'Riacho Fundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1792, 'DF', 'Samambaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1793, 'DF', 'Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1794, 'DF', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1795, 'DF', 'Sobradinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1796, 'DF', 'Taguatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1797, 'ES', 'Acioli');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1798, 'ES', 'Afonso Cláudio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1799, 'ES', 'Aghá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1800, 'ES', 'Água Doce do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1801, 'ES', 'Águia Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1802, 'ES', 'Airituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1803, 'ES', 'Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1804, 'ES', 'Alfredo Chaves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1805, 'ES', 'Alto Calçado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1806, 'ES', 'Alto Caldeirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1807, 'ES', 'Alto Mutum Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1808, 'ES', 'Alto Rio Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1809, 'ES', 'Alto Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1810, 'ES', 'Anchieta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1811, 'ES', 'Ângelo Frechiani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1812, 'ES', 'Anutiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1813, 'ES', 'Apiacá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1814, 'ES', 'Araçatiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1815, 'ES', 'Aracê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1816, 'ES', 'Aracruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1817, 'ES', 'Aracui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1818, 'ES', 'Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1819, 'ES', 'Araraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1820, 'ES', 'Argolas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1821, 'ES', 'Atílio Vivácqua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1822, 'ES', 'Baía Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1823, 'ES', 'Baixo Guandu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1824, 'ES', 'Barra de Novo Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1825, 'ES', 'Barra de São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1827, 'ES', 'Barra Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1828, 'ES', 'Barra Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1829, 'ES', 'Baunilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1830, 'ES', 'Bebedouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1831, 'ES', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1832, 'ES', 'Boapaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1833, 'ES', 'Bom Jesus do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1834, 'ES', 'Bonsucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1835, 'ES', 'Braço do Rio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1836, 'ES', 'Brejetuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1837, 'ES', 'Burarama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1838, 'ES', 'Cachoeirinha de Itaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1839, 'ES', 'Cachoeiro de Itapemirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1840, 'ES', 'Café');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1841, 'ES', 'Calogi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1842, 'ES', 'Câmara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1843, 'ES', 'Carapina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1844, 'ES', 'Cariacica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1845, 'ES', 'Castelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1846, 'ES', 'Celina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1847, 'ES', 'Colatina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1848, 'ES', 'Conceição da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1849, 'ES', 'Conceição do Castelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1850, 'ES', 'Conceição do Muqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1851, 'ES', 'Conduru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1852, 'ES', 'Praia do Coqueiral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1853, 'ES', 'Córrego dos Monos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1854, 'ES', 'Cotaxé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1855, 'ES', 'Cristal do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1856, 'ES', 'Crubixá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1857, 'ES', 'Desengano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1858, 'ES', 'Divino de São Lourenço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1859, 'ES', 'Divino Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1860, 'ES', 'Djalma Coutinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1861, 'ES', 'Domingos Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1862, 'ES', 'Dona América');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1863, 'ES', 'Dores do Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1864, 'ES', 'Duas Barras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1865, 'ES', 'Ecoporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1866, 'ES', 'Estrela do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1867, 'ES', 'Fartura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1868, 'ES', 'Fazenda Guandu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1869, 'ES', 'Fundão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1870, 'ES', 'Garrafão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1871, 'ES', 'Gironda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1872, 'ES', 'Goiabeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1873, 'ES', 'Governador Lacerda de Aguiar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1874, 'ES', 'Governador Lindenberg');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1875, 'ES', 'Graça Aranha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1876, 'ES', 'Gruta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1877, 'ES', 'Guaçuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1878, 'ES', 'Vila Guaraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1879, 'ES', 'Guarapari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1880, 'ES', 'Guararema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1881, 'ES', 'Ibatiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1883, 'ES', 'Ibicaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1884, 'ES', 'Ibiraçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1885, 'ES', 'Ibitirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1886, 'ES', 'Ibitiruí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1887, 'ES', 'Ibituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1888, 'ES', 'Iconha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1889, 'ES', 'Imburana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1890, 'ES', 'Iriritiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1891, 'ES', 'Irundi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1892, 'ES', 'Irupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1893, 'ES', 'Isabel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1894, 'ES', 'Itabaiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1895, 'ES', 'Itaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1896, 'ES', 'Itaguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1897, 'ES', 'Itaicí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1898, 'ES', 'Itaimbé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1899, 'ES', 'Itaipava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1900, 'ES', 'Itamira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1901, 'ES', 'Itaóca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1902, 'ES', 'Itapecoá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1903, 'ES', 'Itapemirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1904, 'ES', 'Itaperuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1905, 'ES', 'Itapina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1906, 'ES', 'Itaquari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1907, 'ES', 'Itarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1908, 'ES', 'Itaúnas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1909, 'ES', 'Itauninhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1910, 'ES', 'Iúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1911, 'ES', 'Jabaquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1912, 'ES', 'Jacaraipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1913, 'ES', 'Jaciguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1914, 'ES', 'Jacupemba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1915, 'ES', 'Jaguaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1916, 'ES', 'Jerônimo Monteiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1917, 'ES', 'Joaçuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1918, 'ES', 'João Neiva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1919, 'ES', 'Joatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1920, 'ES', 'José Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1922, 'ES', 'Lajinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1923, 'ES', 'Laranja da Terra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1924, 'ES', 'Limoeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1925, 'ES', 'Linhares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1926, 'ES', 'Mangaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1927, 'ES', 'Mantenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1928, 'ES', 'Marataízes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1929, 'ES', 'Marechal Floriano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1930, 'ES', 'Marilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1931, 'ES', 'Matilde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1932, 'ES', 'Melgaço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1933, 'ES', 'Menino Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1934, 'ES', 'Mimoso do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1935, 'ES', 'Montanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1936, 'ES', 'Monte Carmelo do Rio Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1937, 'ES', 'Monte Pio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1938, 'ES', 'Monte Sinai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1939, 'ES', 'Mucurici');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1940, 'ES', 'Mundo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1941, 'ES', 'Muniz Freire');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1942, 'ES', 'Muqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1943, 'ES', 'Nestor Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1944, 'ES', 'Nova Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1946, 'ES', 'Nova Venécia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1947, 'ES', 'Nova Verona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1948, 'ES', 'Novo Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1949, 'ES', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1950, 'ES', 'Pacotuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1951, 'ES', 'Paineiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1952, 'ES', 'Palmeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1953, 'ES', 'Palmerino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1954, 'ES', 'Pancas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1955, 'ES', 'Paraju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1956, 'ES', 'Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1957, 'ES', 'Pedro Canário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1958, 'ES', 'Pendanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1959, 'ES', 'Pequiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1960, 'ES', 'Nossa Senhora das Graças');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1961, 'ES', 'Piaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1962, 'ES', 'Pinheiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1963, 'ES', 'Piracema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1964, 'ES', 'Piúma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1965, 'ES', 'Ponte de Itabapoana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1966, 'ES', 'Ponto Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1967, 'ES', 'Pontões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1968, 'ES', 'Porangá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1969, 'ES', 'Barra do Riacho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1970, 'ES', 'Praia Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1971, 'ES', 'Presidente Kennedy');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1972, 'ES', 'Princesa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1973, 'ES', 'Queimado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1974, 'ES', 'Quilômetro 14 do Mutum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1975, 'ES', 'Regência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1976, 'ES', 'Riacho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1977, 'ES', 'Ribeirão do Cristo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1978, 'ES', 'Rio Bananal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1979, 'ES', 'Rio Calçado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1980, 'ES', 'Rio Muqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1981, 'ES', 'Rio Novo do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1982, 'ES', 'Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1983, 'ES', 'Rive');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1984, 'ES', 'Sagrada Família');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1985, 'ES', 'Santa Angélica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1986, 'ES', 'Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1987, 'ES', 'Santa Júlia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1988, 'ES', 'Santa Leopoldina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1989, 'ES', 'Santa Luzia de Mantenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1990, 'ES', 'Santa Luzia do Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1991, 'ES', 'Santa Luzia do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1992, 'ES', 'Santa Maria de Jetibá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1993, 'ES', 'Santa Marta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1994, 'ES', 'Santa Teresa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1995, 'ES', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1996, 'ES', 'Santíssima Trindade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1997, 'ES', 'Santo Agostinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1998, 'ES', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (1999, 'ES', 'Santo Antônio do Canaã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2000, 'ES', 'Santo Antônio do Muqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2001, 'ES', 'Santo Antônio do Pousalegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2002, 'ES', 'Santo Antônio do Quinze');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2003, 'ES', 'São Domingos do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2004, 'ES', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2005, 'ES', 'São Francisco Xavier do Guandu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2006, 'ES', 'São Gabriel da Palha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2007, 'ES', 'São Geraldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2008, 'ES', 'São Jacinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2009, 'ES', 'São João de Petrópolis');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 2000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2010, 'ES', 'São João de Viçosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2011, 'ES', 'São João do Sobrado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2013, 'ES', 'São Jorge do Oliveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2014, 'ES', 'São Jorge do Tiradentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2015, 'ES', 'São José das Torres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2016, 'ES', 'São José de Mantenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2017, 'ES', 'São José do Calçado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2018, 'ES', 'São José do Sobradinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2019, 'ES', 'São Mateus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2020, 'ES', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2021, 'ES', 'São Pedro de Itabapoana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2022, 'ES', 'São Pedro de Rates');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2023, 'ES', 'São Rafael');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2024, 'ES', 'São Roque do Canaã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2025, 'ES', 'São Tiago');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2027, 'ES', 'Sapucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2028, 'ES', 'Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2029, 'ES', 'Serra Pelada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2030, 'ES', 'Sobreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2031, 'ES', 'Sooretama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2032, 'ES', 'Timbuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2033, 'ES', 'Todos os Santos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2034, 'ES', 'Urânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2035, 'ES', 'Vargem Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2036, 'ES', 'Vargem Grande do Soturno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2037, 'ES', 'Venda Nova do Imigrante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2038, 'ES', 'Viana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2039, 'ES', 'Vieira Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2040, 'ES', 'Vila Nelita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2041, 'ES', 'Vila Nova de Bananal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2042, 'ES', 'Vila Pavão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2043, 'ES', 'Vila Valério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2044, 'ES', 'Vila Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2045, 'ES', 'Vila Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2046, 'ES', 'Vinhático');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2047, 'ES', 'Vinte e Cinco de Julho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2048, 'ES', 'Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2049, 'GO', 'Abadia de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2050, 'GO', 'Abadiânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2051, 'GO', 'Acreúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2052, 'GO', 'Adelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2053, 'GO', 'Água Fria de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2054, 'GO', 'Água Limpa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2055, 'GO', 'Águas Lindas de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2056, 'GO', 'Alexânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2057, 'GO', 'Aloândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2058, 'GO', 'Alto Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2059, 'GO', 'Alto Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2060, 'GO', 'Alto Paraíso de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2061, 'GO', 'Alvorada do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2062, 'GO', 'Amaralina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2063, 'GO', 'Americano do Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2064, 'GO', 'Amorinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2065, 'GO', 'Anápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2066, 'GO', 'Anhangüera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2067, 'GO', 'Anicuns');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2068, 'GO', 'Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2069, 'GO', 'Aparecida de Goiânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2070, 'GO', 'Aparecida de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2071, 'GO', 'Aparecida do Rio Claro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2072, 'GO', 'Aparecida do Rio Doce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2073, 'GO', 'Aporé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2074, 'GO', 'Araçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2075, 'GO', 'Aragarças');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2076, 'GO', 'Aragoiânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2077, 'GO', 'Araguapaz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2078, 'GO', 'Arenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2079, 'GO', 'Aruanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2080, 'GO', 'Aurilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2081, 'GO', 'Auriverde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2082, 'GO', 'Avelinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2083, 'GO', 'Bacilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2084, 'GO', 'Baliza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2085, 'GO', 'Bandeirantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2086, 'GO', 'Barbosilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2087, 'GO', 'Barro Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2088, 'GO', 'Bela Vista de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2089, 'GO', 'Bom Jardim de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2090, 'GO', 'Bom Jesus de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2091, 'GO', 'Bonfinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2092, 'GO', 'Bonópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2093, 'GO', 'Brazabrantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2094, 'GO', 'Britânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2095, 'GO', 'Buenolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2096, 'GO', 'Buriti Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2097, 'GO', 'Buriti de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2098, 'GO', 'Buritinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2099, 'GO', 'Cabeceiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2100, 'GO', 'Cachoeira Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2101, 'GO', 'Cachoeira de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2102, 'GO', 'Cachoeira Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2103, 'GO', 'Caçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2104, 'GO', 'Caiapônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2105, 'GO', 'Caiçara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2106, 'GO', 'Calcilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2107, 'GO', 'Caldas Novas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2108, 'GO', 'Caldazinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2109, 'GO', 'Calixto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2110, 'GO', 'Campestre de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2111, 'GO', 'Campinaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2112, 'GO', 'Campinorte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2113, 'GO', 'Campo Alegre de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2114, 'GO', 'Campo Limpo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2115, 'GO', 'Campolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2116, 'GO', 'Campos Belos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2117, 'GO', 'Campos Verdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2118, 'GO', 'Cana Brava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2119, 'GO', 'Canadá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2120, 'GO', 'Capelinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2121, 'GO', 'Caraíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2122, 'GO', 'Carmo do Rio Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2123, 'GO', 'Castelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2124, 'GO', 'Castrinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2125, 'GO', 'Catalão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2126, 'GO', 'Caturaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2127, 'GO', 'Cavalcante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2128, 'GO', 'Cavalheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2129, 'GO', 'Cebrasa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2130, 'GO', 'Ceres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2131, 'GO', 'Cezarina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2132, 'GO', 'Chapadão do Céu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2133, 'GO', 'Choupana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2134, 'GO', 'Cibele');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2135, 'GO', 'Cidade Ocidental');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2136, 'GO', 'Cirilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2137, 'GO', 'Cocalzinho de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2138, 'GO', 'Colinas do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2139, 'GO', 'Córrego do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2140, 'GO', 'Córrego Rico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2141, 'GO', 'Corumbá de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2142, 'GO', 'Corumbaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2143, 'GO', 'Cristalina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2144, 'GO', 'Cristianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2145, 'GO', 'Crixás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2146, 'GO', 'Cromínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2147, 'GO', 'Cruzeiro do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2148, 'GO', 'Cumari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2149, 'GO', 'Damianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2150, 'GO', 'Damolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2151, 'GO', 'Davidópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2152, 'GO', 'Davinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2153, 'GO', 'Diolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2154, 'GO', 'Diorama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2155, 'GO', 'Divinópolis de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2156, 'GO', 'Domiciano Ribeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2157, 'GO', 'Doverlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2158, 'GO', 'Edealina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2159, 'GO', 'Edéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2160, 'GO', 'Estrela do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2161, 'GO', 'Faina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2162, 'GO', 'Fazenda Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2163, 'GO', 'Firminópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2164, 'GO', 'Flores de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2165, 'GO', 'Formosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2166, 'GO', 'Formoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2167, 'GO', 'Forte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2168, 'GO', 'Gameleira de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2169, 'GO', 'Geriaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2170, 'GO', 'Goialândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2171, 'GO', 'Goianápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2172, 'GO', 'Goiandira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2173, 'GO', 'Goianésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2174, 'GO', 'Goiânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2175, 'GO', 'Goianira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2176, 'GO', 'Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2177, 'GO', 'Goiatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2178, 'GO', 'Gouvelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2179, 'GO', 'Guapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2180, 'GO', 'Guaraíta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2181, 'GO', 'Guarani de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2182, 'GO', 'Guarinos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2183, 'GO', 'Heitoraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2184, 'GO', 'Hidrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2185, 'GO', 'Hidrolina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2186, 'GO', 'Iaciara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2187, 'GO', 'Inaciolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2188, 'GO', 'Indiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2189, 'GO', 'Inhumas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2190, 'GO', 'Interlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2191, 'GO', 'Ipameri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2192, 'GO', 'Iporá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2193, 'GO', 'Israelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2194, 'GO', 'Itaberaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2195, 'GO', 'Itaguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2196, 'GO', 'Itaguari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2197, 'GO', 'Itaguaru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2198, 'GO', 'Itajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2199, 'GO', 'Itapaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2200, 'GO', 'Itapirapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2201, 'GO', 'Itapuranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2202, 'GO', 'Itarumã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2203, 'GO', 'Itauçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2204, 'GO', 'Itumbiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2205, 'GO', 'Ivolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2206, 'GO', 'Jacilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2207, 'GO', 'Jandaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2208, 'GO', 'Jaraguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2209, 'GO', 'Jataí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2210, 'GO', 'Jaupaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2211, 'GO', 'Jeroaquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2212, 'GO', 'Jesúpolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2213, 'GO', 'Joanápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2214, 'GO', 'Joviânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2216, 'GO', 'Juscelino Kubitschek');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2217, 'GO', 'Jussara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2218, 'GO', 'Lagoa do Bauzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2219, 'GO', 'Lagolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2220, 'GO', 'Leopoldo de Bulhões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2221, 'GO', 'Lucilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2222, 'GO', 'Luziânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2223, 'GO', 'Mairipotaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2224, 'GO', 'Mambaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2225, 'GO', 'Mara Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2226, 'GO', 'Marcianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2227, 'GO', 'Marzagão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2228, 'GO', 'Matrinchã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2229, 'GO', 'Maurilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2230, 'GO', 'Meia Ponte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2231, 'GO', 'Messianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2232, 'GO', 'Mimoso de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2233, 'GO', 'Minaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2234, 'GO', 'Mineiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2235, 'GO', 'Moiporá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2236, 'GO', 'Monte Alegre de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2237, 'GO', 'Montes Claros de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2238, 'GO', 'Montividiu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2239, 'GO', 'Montividiu do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2240, 'GO', 'Morrinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2241, 'GO', 'Morro Agudo de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2242, 'GO', 'Mossâmedes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2243, 'GO', 'Mozarlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2244, 'GO', 'Mundo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2245, 'GO', 'Mutunópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2246, 'GO', 'Natinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2247, 'GO', 'Nazário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2248, 'GO', 'Nerópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2249, 'GO', 'Niquelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2250, 'GO', 'Nova América');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2251, 'GO', 'Nova Aurora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2252, 'GO', 'Nova Crixás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2253, 'GO', 'Nova Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2254, 'GO', 'Nova Iguaçu de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2255, 'GO', 'Nova Roma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2256, 'GO', 'Nova Veneza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2257, 'GO', 'Novo Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2258, 'GO', 'Novo Gama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2259, 'GO', 'Novo Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2260, 'GO', 'Olaria do Angico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2261, 'GO', 'Olhos D Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2262, 'GO', 'Orizona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2263, 'GO', 'Ouro Verde de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2264, 'GO', 'Ouroana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2265, 'GO', 'Ouvidor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2266, 'GO', 'Padre Bernardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2267, 'GO', 'Palestina de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2268, 'GO', 'Palmeiras de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2269, 'GO', 'Palmelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2270, 'GO', 'Palminópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2271, 'GO', 'Panamá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2272, 'GO', 'Paranaiguara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2273, 'GO', 'Paraúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2274, 'GO', 'Colinaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2275, 'GO', 'Pedra Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2276, 'GO', 'Perolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2277, 'GO', 'Petrolina de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2278, 'GO', 'Pilar de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2279, 'GO', 'Piloândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2280, 'GO', 'Piracanjuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2281, 'GO', 'Piranhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2282, 'GO', 'Pirenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2283, 'GO', 'Pires Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2284, 'GO', 'Pires do Rio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2285, 'GO', 'Planaltina de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2286, 'GO', 'Pontalina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2287, 'GO', 'Porangatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2288, 'GO', 'Porteirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2289, 'GO', 'Portelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2290, 'GO', 'Posse');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2291, 'GO', 'Posse D Abadia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2292, 'GO', 'Professor Jamil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2293, 'GO', 'Quirinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2294, 'GO', 'Registro do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2295, 'GO', 'Rianápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2297, 'GO', 'Rio Quente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2298, 'GO', 'Rio Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2299, 'GO', 'Riverlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2300, 'GO', 'Campo Limpo de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2301, 'GO', 'Rosalândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2302, 'GO', 'Rubiataba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2303, 'GO', 'Sanclerlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2304, 'GO', 'Santa Bárbara de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2305, 'GO', 'Santa Cruz das Lajes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2306, 'GO', 'Santa Cruz de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2307, 'GO', 'Santa Fé de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2308, 'GO', 'Santa Helena de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2309, 'GO', 'Santa Isabel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2310, 'GO', 'Santa Rita do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2311, 'GO', 'Santa Rita do Novo Destino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2312, 'GO', 'Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2313, 'GO', 'Santa Rosa de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2314, 'GO', 'Santa Tereza de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2315, 'GO', 'Santa Terezinha de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2316, 'GO', 'Santo Antônio da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2317, 'GO', 'Santo Antônio de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2318, 'GO', 'Santo Antônio do Descoberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2319, 'GO', 'Santo Antônio do Rio Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2320, 'GO', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2321, 'GO', 'São Francisco de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2322, 'GO', 'São Gabriel de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2323, 'GO', 'São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2324, 'GO', 'São João D Aliança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2325, 'GO', 'São João da Paraúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2326, 'GO', 'São Luís de Montes Belos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2327, 'GO', 'São Luiz do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2328, 'GO', 'São Luiz do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2329, 'GO', 'São Miguel do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2330, 'GO', 'São Miguel do Passa Quatro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2331, 'GO', 'São Patrício');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2332, 'GO', 'São Sebastião do Rio Claro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2333, 'GO', 'São Simão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2334, 'GO', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2335, 'GO', 'Sarandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2336, 'GO', 'Senador Canedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2337, 'GO', 'Serra Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2338, 'GO', 'Serranópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2339, 'GO', 'Silvânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2340, 'GO', 'Simolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2341, 'GO', 'Sítio D Abadia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2342, 'GO', 'Sousânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2343, 'GO', 'Taquaral de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2344, 'GO', 'Taveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2345, 'GO', 'Teresina de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2346, 'GO', 'Terezópolis de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2347, 'GO', 'Lagoa Santa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2348, 'GO', 'Três Ranchos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2349, 'GO', 'Trindade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2350, 'GO', 'Trombas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2351, 'GO', 'Tupiracaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2352, 'GO', 'Turvânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2353, 'GO', 'Turvelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2354, 'GO', 'Uirapuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2355, 'GO', 'Uruaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2356, 'GO', 'Uruana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2357, 'GO', 'Uruita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2358, 'GO', 'Urutaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2359, 'GO', 'Uvá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2360, 'GO', 'Valdelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2361, 'GO', 'Valparaíso de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2362, 'GO', 'Varjão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2363, 'GO', 'Vianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2364, 'GO', 'Vicentinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2365, 'GO', 'Vila Boa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2367, 'GO', 'Vila Propício');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2368, 'GO', 'Vila Sertaneja');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2369, 'MA', 'Açailândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2370, 'MA', 'Afonso Cunha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2371, 'MA', 'Água Doce do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2372, 'MA', 'Alcântara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2373, 'MA', 'Aldeias Altas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2374, 'MA', 'Altamira do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2375, 'MA', 'Alto Alegre do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2376, 'MA', 'Alto Alegre do Pindaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2377, 'MA', 'Alto Parnaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2378, 'MA', 'Amapá do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2379, 'MA', 'Amarante do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2380, 'MA', 'Anajatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2381, 'MA', 'Anapurus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2382, 'MA', 'Anil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2383, 'MA', 'Apicum-Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2384, 'MA', 'Araguanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2385, 'MA', 'Araióses');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2386, 'MA', 'Arame');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2387, 'MA', 'Arari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2388, 'MA', 'Aurizona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2389, 'MA', 'Axixá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2390, 'MA', 'Bacabal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2391, 'MA', 'Bacabeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2392, 'MA', 'Bacatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2393, 'MA', 'Bacuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2394, 'MA', 'Bacurituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2395, 'MA', 'Balsas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2396, 'MA', 'Barão de Grajaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2397, 'MA', 'Barão de Tromai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2398, 'MA', 'Barra do Corda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2399, 'MA', 'Barreirinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2400, 'MA', 'Barro Duro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2401, 'MA', 'Bela Vista do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2402, 'MA', 'Belágua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2403, 'MA', 'Benedito Leite');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2404, 'MA', 'Bequimão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2405, 'MA', 'Bernardo do Mearim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2406, 'MA', 'Boa Vista do Gurupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2407, 'MA', 'Boa Vista do Pindaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2408, 'MA', 'Bom Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2409, 'MA', 'Bom Jesus das Selvas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2410, 'MA', 'Bom Lugar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2411, 'MA', 'Bonfim do Arari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2412, 'MA', 'Brejo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2413, 'MA', 'Brejo de Areia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2414, 'MA', 'Brejo de São Félix');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2415, 'MA', 'Buriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2416, 'MA', 'Buriti Bravo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2417, 'MA', 'Buriti Cortado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2418, 'MA', 'Buriticupu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2419, 'MA', 'Buritirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2420, 'MA', 'Cachoeira Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2421, 'MA', 'Cajapió');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2422, 'MA', 'Cajari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2423, 'MA', 'Campestre do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2424, 'MA', 'Cândido Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2425, 'MA', 'Cantanhede');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2426, 'MA', 'Capinzal do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2427, 'MA', 'Caraíba do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2428, 'MA', 'Carolina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2429, 'MA', 'Carutapera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2430, 'MA', 'Caxias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2431, 'MA', 'Cedral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2432, 'MA', 'Central do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2433, 'MA', 'Centro do Guilherme');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2434, 'MA', 'Centro Novo do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2435, 'MA', 'Chapadinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2436, 'MA', 'Cidelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2437, 'MA', 'Codó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2438, 'MA', 'Codozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2439, 'MA', 'Coelho Neto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2440, 'MA', 'Colinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2441, 'MA', 'Conceição do Lago-Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2442, 'MA', 'Coroatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2443, 'MA', 'Curupa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2444, 'MA', 'Cururupu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2445, 'MA', 'Curva Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2446, 'MA', 'Custódio Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2447, 'MA', 'Davinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2448, 'MA', 'Dom Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2449, 'MA', 'Duque Bacelar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2450, 'MA', 'Esperantinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2451, 'MA', 'Estandarte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2452, 'MA', 'Estreito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2453, 'MA', 'Feira Nova do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2454, 'MA', 'Fernando Falcão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2455, 'MA', 'Formosa da Serra Negra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2456, 'MA', 'Fortaleza dos Nogueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2457, 'MA', 'Fortuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2458, 'MA', 'Frecheiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2459, 'MA', 'Godofredo Viana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2460, 'MA', 'Gonçalves Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2461, 'MA', 'Governador Archer');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2462, 'MA', 'Governador Edson Lobão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2463, 'MA', 'Governador Eugênio Barros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2464, 'MA', 'Governador Luiz Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2465, 'MA', 'Governador Newton Bello');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2466, 'MA', 'Governador Nunes Freire');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2467, 'MA', 'Graça Aranha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2468, 'MA', 'Grajaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2469, 'MA', 'Guimarães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2470, 'MA', 'Humberto de Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2471, 'MA', 'Ibipira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2472, 'MA', 'Icatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2473, 'MA', 'Igarapé do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2474, 'MA', 'Igarapé Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2475, 'MA', 'Imperatriz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2476, 'MA', 'Itaipava do Grajaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2477, 'MA', 'Itamataré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2478, 'MA', 'Itapecuru Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2479, 'MA', 'Itapera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2480, 'MA', 'Itinga do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2481, 'MA', 'Jatobá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2482, 'MA', 'Jenipapo dos Vieiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2483, 'MA', 'João Lisboa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2484, 'MA', 'Joselândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2485, 'MA', 'Junco do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2486, 'MA', 'Lago da Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2487, 'MA', 'Lago do Junco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2488, 'MA', 'Lago dos Rodrigues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2489, 'MA', 'Lago Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2490, 'MA', 'Lagoa do Mato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2491, 'MA', 'Lagoa Grande do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2492, 'MA', 'Lajeado Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2493, 'MA', 'Lapela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2494, 'MA', 'Leandro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2495, 'MA', 'Lima Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2496, 'MA', 'Loreto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2497, 'MA', 'Luís Domingues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2498, 'MA', 'Magalhães de Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2499, 'MA', 'Maioba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2500, 'MA', 'Maracaçumé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2501, 'MA', 'Marajá do Sena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2502, 'MA', 'Maranhãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2503, 'MA', 'Marianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2504, 'MA', 'Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2505, 'MA', 'Mata Roma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2506, 'MA', 'Matinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2507, 'MA', 'Matões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2508, 'MA', 'Matões do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2509, 'MA', 'Milagres do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2510, 'MA', 'Mirador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2511, 'MA', 'Miranda do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2512, 'MA', 'Mirinzal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2513, 'MA', 'Monção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2514, 'MA', 'Montes Altos');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 2500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2515, 'MA', 'Morros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2516, 'MA', 'Nina Rodrigues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2517, 'MA', 'Nova Colinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2518, 'MA', 'Nova Iorque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2519, 'MA', 'Nova Olinda do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2520, 'MA', 'Olho D Água das Cunhãs');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2521, 'MA', 'Olinda Nova do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2522, 'MA', 'Paço do Lumiar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2523, 'MA', 'Palmeirândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2524, 'MA', 'Papagaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2525, 'MA', 'Paraibano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2526, 'MA', 'Parnarama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2527, 'MA', 'Passagem Franca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2528, 'MA', 'Pastos Bons');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2529, 'MA', 'Paulino Neves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2530, 'MA', 'Paulo Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2531, 'MA', 'Pedreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2532, 'MA', 'Pedro do Rosário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2533, 'MA', 'Penalva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2534, 'MA', 'Peri Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2535, 'MA', 'Peritoró');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2536, 'MA', 'Pimentel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2537, 'MA', 'Pindaré Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2538, 'MA', 'Pinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2539, 'MA', 'Pio XII');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2540, 'MA', 'Pirapemas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2541, 'MA', 'Poção de Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2542, 'MA', 'Porto das Gabarras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2543, 'MA', 'Porto Franco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2544, 'MA', 'Porto Rico do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2545, 'MA', 'Presidente Dutra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2546, 'MA', 'Presidente Juscelino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2547, 'MA', 'Presidente Médici');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2548, 'MA', 'Presidente Sarney');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2549, 'MA', 'Presidente Vargas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2550, 'MA', 'Primeira Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2551, 'MA', 'Raposa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2552, 'MA', 'Resplandes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2553, 'MA', 'Riachão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2554, 'MA', 'Ribamar Fiquene');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2555, 'MA', 'Ribeirão Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2556, 'MA', 'Rocado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2557, 'MA', 'Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2558, 'MA', 'Rosário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2559, 'MA', 'Sambaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2560, 'MA', 'Santa Filomena do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2561, 'MA', 'Santa Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2562, 'MA', 'Santa Inês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2563, 'MA', 'Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2564, 'MA', 'Santa Luzia do Paruá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2565, 'MA', 'Santa Quitéria do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2566, 'MA', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2567, 'MA', 'Santana do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2568, 'MA', 'Santo Amaro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2569, 'MA', 'Santo Antônio dos Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2570, 'MA', 'São Benedito do Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2571, 'MA', 'São Bento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2572, 'MA', 'São Bernardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2573, 'MA', 'São Domingos do Azeitão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2574, 'MA', 'São Domingos do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2575, 'MA', 'São Félix de Balsas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2576, 'MA', 'São Francisco do Brejão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2577, 'MA', 'São Francisco do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2578, 'MA', 'São João Batista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2579, 'MA', 'São João de Cortes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2580, 'MA', 'São João do Carú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2581, 'MA', 'São João do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2582, 'MA', 'São João do Sóter');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2583, 'MA', 'São João dos Patos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2584, 'MA', 'São Joaquim dos Melos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2585, 'MA', 'São José de Ribamar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2586, 'MA', 'São José dos Basílios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2587, 'MA', 'São Luís');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2588, 'MA', 'São Luís Gonzaga do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2589, 'MA', 'São Mateus do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2590, 'MA', 'São Pedro da Água Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2591, 'MA', 'São Pedro dos Crentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2592, 'MA', 'São Raimundo das Mangabeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2593, 'MA', 'São Raimundo de Codó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2594, 'MA', 'São Raimundo do Doca Bezerra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2595, 'MA', 'São Roberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2596, 'MA', 'São Vicente Ferrer');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2597, 'MA', 'Satubinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2598, 'MA', 'Senador Alexandre Costa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2599, 'MA', 'Senador La Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2600, 'MA', 'Serrano do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2601, 'MA', 'Sítio Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2602, 'MA', 'Sucupira do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2603, 'MA', 'Sucupira do Riachão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2604, 'MA', 'Tasso Fragoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2605, 'MA', 'Timbiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2606, 'MA', 'Timon');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2607, 'MA', 'Trizidela do Vale');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2608, 'MA', 'Tufilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2609, 'MA', 'Tuntum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2610, 'MA', 'Turiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2611, 'MA', 'Turilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2612, 'MA', 'Tutóia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2613, 'MA', 'Urbano Santos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2614, 'MA', 'Vargem Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2615, 'MA', 'Viana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2616, 'MA', 'Vila Nova dos Martírios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2617, 'MA', 'Vitória do Mearim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2618, 'MA', 'Vitorino Freire');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2619, 'MA', 'Zé Doca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2620, 'MG', 'Abadia dos Dourados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2621, 'MG', 'Abaeté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2622, 'MG', 'Abaeté dos Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2623, 'MG', 'Abaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2624, 'MG', 'Abre Campo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2625, 'MG', 'Abreus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2626, 'MG', 'Acaiaca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2627, 'MG', 'Açucena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2628, 'MG', 'Acuruí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2629, 'MG', 'Adão Colares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2630, 'MG', 'Água Boa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2631, 'MG', 'Água Branca de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2632, 'MG', 'Água Comprida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2633, 'MG', 'Água Viva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2634, 'MG', 'Aguanil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2635, 'MG', 'Águas de Araxá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2636, 'MG', 'Águas de Contendas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2637, 'MG', 'Águas Férreas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2638, 'MG', 'Águas Formosas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2639, 'MG', 'Águas Vermelhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2640, 'MG', 'Aimorés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2641, 'MG', 'Aiuruoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2642, 'MG', 'Alagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2643, 'MG', 'Albertina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2644, 'MG', 'Alberto Isaacson');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2645, 'MG', 'Albertos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2646, 'MG', 'Aldeia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2647, 'MG', 'Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2648, 'MG', 'Alegria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2649, 'MG', 'Além Paraíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2650, 'MG', 'Alexandrita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2651, 'MG', 'Alfenas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2652, 'MG', 'Alfredo Vasconcelos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2653, 'MG', 'Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2654, 'MG', 'Almenara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2655, 'MG', 'Alpercata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2656, 'MG', 'Alpinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2657, 'MG', 'Alterosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2658, 'MG', 'Alto Caparaó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2659, 'MG', 'Alto Capim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2660, 'MG', 'Alto de Santa Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2661, 'MG', 'Alto Jequitibá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2662, 'MG', 'Alto Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2663, 'MG', 'Alto Rio Doce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2664, 'MG', 'Altolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2665, 'MG', 'Alvação');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2666, 'MG', 'Alvarenga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2667, 'MG', 'Alvinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2668, 'MG', 'Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2669, 'MG', 'Alvorada de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2670, 'MG', 'Amanda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2671, 'MG', 'Amanhece');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2672, 'MG', 'Amarantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2673, 'MG', 'Amparo da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2674, 'MG', 'Andiroba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2675, 'MG', 'Andradas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2676, 'MG', 'Andrelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2677, 'MG', 'Andrequicé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2678, 'MG', 'Angaturama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2679, 'MG', 'Angelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2680, 'MG', 'Angicos de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2681, 'MG', 'Angueretá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2682, 'MG', 'Angustura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2683, 'MG', 'Antônio Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2684, 'MG', 'Antônio Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2685, 'MG', 'Antônio dos Santos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2686, 'MG', 'Antônio Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2687, 'MG', 'Antônio Pereira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2688, 'MG', 'Antônio Prado de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2689, 'MG', 'Antunes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2690, 'MG', 'Aparecida de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2691, 'MG', 'Araçaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2692, 'MG', 'Aracati de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2693, 'MG', 'Aracitaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2694, 'MG', 'Araçuaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2695, 'MG', 'Araguari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2696, 'MG', 'Aramirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2697, 'MG', 'Aranha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2698, 'MG', 'Arantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2699, 'MG', 'Araponga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2700, 'MG', 'Araporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2701, 'MG', 'Arapuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2702, 'MG', 'Araújos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2703, 'MG', 'Araúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2704, 'MG', 'Araxá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2705, 'MG', 'Arcângelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2706, 'MG', 'Arceburgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2707, 'MG', 'Arcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2708, 'MG', 'Areado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2709, 'MG', 'Argenita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2710, 'MG', 'Argirita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2711, 'MG', 'Aricanduva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2712, 'MG', 'Arinos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2713, 'MG', 'Aristides Batista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2714, 'MG', 'Ascenção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2715, 'MG', 'Assaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2716, 'MG', 'Astolfo Dutra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2717, 'MG', 'Ataléia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2718, 'MG', 'Augusto de Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2719, 'MG', 'Avaí do Jacinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2720, 'MG', 'Azurita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2721, 'MG', 'Babilônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2722, 'MG', 'Bação');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2723, 'MG', 'Baependi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2724, 'MG', 'Baguari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2725, 'MG', 'Baiões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2726, 'MG', 'Baixa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2727, 'MG', 'Balbinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2728, 'MG', 'Baldim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2729, 'MG', 'Bambuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2730, 'MG', 'Bandeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2731, 'MG', 'Bandeira do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2732, 'MG', 'Bandeirantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2733, 'MG', 'Barão de Cocais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2734, 'MG', 'Barão de Monte Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2735, 'MG', 'Barbacena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2736, 'MG', 'Barra Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2737, 'MG', 'Barra da Figueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2738, 'MG', 'Barra do Ariranha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2739, 'MG', 'Barra do Cuieté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2740, 'MG', 'Barra Feliz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2741, 'MG', 'Barra Longa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2742, 'MG', 'Barranco Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2743, 'MG', 'Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2744, 'MG', 'Barreiro Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2745, 'MG', 'Barreiro da Raiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2746, 'MG', 'Barreiro do Rio Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2747, 'MG', 'Barretos de Alvinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2748, 'MG', 'Barrocão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2749, 'MG', 'Barroso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2750, 'MG', 'Baú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2751, 'MG', 'Bela Vista de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2752, 'MG', 'Belisário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2753, 'MG', 'Belmiro Braga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2754, 'MG', 'Belo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2755, 'MG', 'Belo Oriente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2756, 'MG', 'Belo Oriente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2757, 'MG', 'Belo Vale');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2758, 'MG', 'Bentópolis de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2759, 'MG', 'Berilo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2760, 'MG', 'Berizal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2761, 'MG', 'Bertópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2762, 'MG', 'Betim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2763, 'MG', 'Bias Fortes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2764, 'MG', 'Bicas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2765, 'MG', 'Bicuíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2766, 'MG', 'Biquinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2767, 'MG', 'Bituri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2768, 'MG', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2769, 'MG', 'Boa Família');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2770, 'MG', 'Boa União de Itabirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2771, 'MG', 'Boa Vista de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2772, 'MG', 'Bocaina de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2773, 'MG', 'Bocaiúva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2774, 'MG', 'Bom Despacho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2775, 'MG', 'Bom Jardim de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2776, 'MG', 'Bom Jesus da Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2777, 'MG', 'Bom Jesus da Penha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2778, 'MG', 'Bom Jesus de Cardosos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2779, 'MG', 'Bom Jesus do Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2780, 'MG', 'Bom Jesus do Divino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2781, 'MG', 'Bom Jesus do Galho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2782, 'MG', 'Bom Jesus do Madeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2783, 'MG', 'Bom Pastor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2784, 'MG', 'Bom Repouso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2785, 'MG', 'Bom Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2786, 'MG', 'Bom Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2787, 'MG', 'Bom Sucesso de Patos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2788, 'MG', 'Bonança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2789, 'MG', 'Bonfim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2790, 'MG', 'Bonfinópolis de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2791, 'MG', 'Bonito de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2792, 'MG', 'Borba Gato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2793, 'MG', 'Borda da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2794, 'MG', 'Botelhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2795, 'MG', 'Botumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2796, 'MG', 'Brás Pires');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2797, 'MG', 'Brasilândia de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2798, 'MG', 'Brasília de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2799, 'MG', 'Brasópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2800, 'MG', 'Braúnas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2801, 'MG', 'Brejaúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2802, 'MG', 'Brejaubinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2803, 'MG', 'Brejo Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2804, 'MG', 'Brejo do Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2805, 'MG', 'Brumadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2806, 'MG', 'Brumal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2807, 'MG', 'Buarque de Macedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2808, 'MG', 'Bueno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2809, 'MG', 'Bueno Brandão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2810, 'MG', 'Buenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2811, 'MG', 'Bugre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2812, 'MG', 'Buritis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2813, 'MG', 'Buritizeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2814, 'MG', 'Caatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2815, 'MG', 'Cabeceira Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2816, 'MG', 'Cabo Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2817, 'MG', 'Caburu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2818, 'MG', 'Caçaratiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2819, 'MG', 'Caçarema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2820, 'MG', 'Cachoeira Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2821, 'MG', 'Cachoeira da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2822, 'MG', 'Cachoeira de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2823, 'MG', 'Cachoeira de Pajeú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2824, 'MG', 'Cachoeira de Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2825, 'MG', 'Cachoeira do Brumado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2826, 'MG', 'Cachoeira do Campo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2827, 'MG', 'Cachoeira do Manteiga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2828, 'MG', 'Cachoeira do Vale');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2829, 'MG', 'Cachoeira dos Antunes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2830, 'MG', 'Cachoeira Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2831, 'MG', 'Cachoeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2832, 'MG', 'Caetano Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2833, 'MG', 'Caetanópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2834, 'MG', 'Caeté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2835, 'MG', 'Caeté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2836, 'MG', 'Caiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2837, 'MG', 'Caiapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2838, 'MG', 'Cajuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2839, 'MG', 'Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2840, 'MG', 'Calixto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2841, 'MG', 'Camacho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2842, 'MG', 'Camanducaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2843, 'MG', 'Camargos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2844, 'MG', 'Cambuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2845, 'MG', 'Cambuquira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2846, 'MG', 'Campanário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2847, 'MG', 'Campanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2848, 'MG', 'Campestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2849, 'MG', 'Campestrinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2850, 'MG', 'Campina Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2851, 'MG', 'Campo Alegre de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2852, 'MG', 'Campo Alegre de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2853, 'MG', 'Campo Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2854, 'MG', 'Campo Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2855, 'MG', 'Campo do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2856, 'MG', 'Campo Florido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2857, 'MG', 'Campo Redondo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2858, 'MG', 'Campolide');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2859, 'MG', 'Campos Altos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2860, 'MG', 'Campos Gerais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2861, 'MG', 'Cana Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2862, 'MG', 'Canaã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2863, 'MG', 'Canabrava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2864, 'MG', 'Canabrava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2865, 'MG', 'Canápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2866, 'MG', 'Canastrão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2867, 'MG', 'Candeias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2868, 'MG', 'Canoeiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2869, 'MG', 'Cantagalo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2870, 'MG', 'Caparaó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2871, 'MG', 'Capela Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2872, 'MG', 'Capelinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2873, 'MG', 'Capetinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2874, 'MG', 'Capim Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2875, 'MG', 'Capinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2876, 'MG', 'Capitânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2877, 'MG', 'Capitão Andrade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2878, 'MG', 'Capitão Enéas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2879, 'MG', 'Capitólio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2880, 'MG', 'Caputira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2881, 'MG', 'Caraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2882, 'MG', 'Caranaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2883, 'MG', 'Carandaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2884, 'MG', 'Carangola');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2885, 'MG', 'Caratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2886, 'MG', 'Carbonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2887, 'MG', 'Serra do Cipó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2888, 'MG', 'Careaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2889, 'MG', 'Carioca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2890, 'MG', 'Carlos Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2891, 'MG', 'Carlos Chagas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2892, 'MG', 'Carmésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2893, 'MG', 'Carmo da Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2894, 'MG', 'Carmo da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2895, 'MG', 'Carmo de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2896, 'MG', 'Carmo do Cajuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2897, 'MG', 'Carmo do Paranaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2898, 'MG', 'Carmo do Rio Claro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2899, 'MG', 'Carmópolis de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2900, 'MG', 'Carneirinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2901, 'MG', 'Carrancas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2902, 'MG', 'Carvalho de Brito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2903, 'MG', 'Carvalhópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2904, 'MG', 'Carvalhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2905, 'MG', 'Casa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2906, 'MG', 'Cascalho Rico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2907, 'MG', 'Cássia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2908, 'MG', 'Cataguarino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2909, 'MG', 'Cataguases');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2910, 'MG', 'Catajás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2911, 'MG', 'Catas Altas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2912, 'MG', 'Catas Altas da Noruega');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2913, 'MG', 'Catiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2914, 'MG', 'Catuji');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2915, 'MG', 'Catuné');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2916, 'MG', 'Catuni');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2917, 'MG', 'Catuti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2918, 'MG', 'Caxambu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2919, 'MG', 'Cedro do Abaeté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2920, 'MG', 'Centenário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2921, 'MG', 'Central de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2922, 'MG', 'Central de Santa Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2923, 'MG', 'Centralina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2924, 'MG', 'Cervo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2925, 'MG', 'Chácara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2926, 'MG', 'Chalé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2927, 'MG', 'Chapada de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2928, 'MG', 'Chapada do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2929, 'MG', 'Chapada Gaúcha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2930, 'MG', 'Chaveslândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2931, 'MG', 'Chiador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2932, 'MG', 'Chonim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2933, 'MG', 'Chumbo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2934, 'MG', 'Cipotânea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2935, 'MG', 'Cisneiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2937, 'MG', 'Claraval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2938, 'MG', 'Claro de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2939, 'MG', 'Claro dos Poções');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2940, 'MG', 'Cláudio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2941, 'MG', 'Cláudio Manuel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2942, 'MG', 'Cocais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2943, 'MG', 'Coco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2944, 'MG', 'Coimbra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2945, 'MG', 'Coluna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2946, 'MG', 'Comendador Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2947, 'MG', 'Comercinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2948, 'MG', 'Conceição da Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2949, 'MG', 'Conceição da Barra de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2950, 'MG', 'Conceição da Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2951, 'MG', 'Conceição da Brejaúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2952, 'MG', 'Conceição da Ibitipoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2953, 'MG', 'Conceição das Alagoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2954, 'MG', 'Conceição das Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2955, 'MG', 'Conceição de Ipanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2956, 'MG', 'Conceição de Itaguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2957, 'MG', 'Conceição de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2958, 'MG', 'Conceição de Piracicaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2959, 'MG', 'Conceição de Tronqueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2960, 'MG', 'Conceição do Capim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2961, 'MG', 'Conceição do Formoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2962, 'MG', 'Conceição do Mato Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2963, 'MG', 'Conceição do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2964, 'MG', 'Conceição do Rio Acima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2965, 'MG', 'Conceição do Rio Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2966, 'MG', 'Conceição dos Ouros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2967, 'MG', 'Concórdia de Mucuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2968, 'MG', 'Condado do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2969, 'MG', 'Cônego João Pio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2970, 'MG', 'Cônego Marinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2971, 'MG', 'Confins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2972, 'MG', 'Congonhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2973, 'MG', 'Congonhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2974, 'MG', 'Congonhas do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2975, 'MG', 'Conquista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2976, 'MG', 'Conselheiro Lafaiete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2977, 'MG', 'Conselheiro Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2978, 'MG', 'Conselheiro Pena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2979, 'MG', 'Consolação');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2980, 'MG', 'Contagem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2981, 'MG', 'Contrato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2982, 'MG', 'Contria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2983, 'MG', 'Coqueiral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2984, 'MG', 'Coração de Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2985, 'MG', 'Cordisburgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2986, 'MG', 'Cordislândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2987, 'MG', 'Corinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2988, 'MG', 'Coroaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2989, 'MG', 'Coromandel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2990, 'MG', 'Coronel Fabriciano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2991, 'MG', 'Coronel Murta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2992, 'MG', 'Coronel Pacheco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2993, 'MG', 'Coronel Xavier Chaves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2994, 'MG', 'Córrego Danta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2995, 'MG', 'Córrego do Barro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2996, 'MG', 'Córrego do Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2997, 'MG', 'Córrego do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2998, 'MG', 'Córrego Fundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (2999, 'MG', 'Córrego Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3000, 'MG', 'Córregos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3001, 'MG', 'Correia de Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3002, 'MG', 'Correntinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3003, 'MG', 'Costa Sena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3004, 'MG', 'Costas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3005, 'MG', 'Costas da Mantiqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3006, 'MG', 'Couto de Magalhães de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3007, 'MG', 'Crisólia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3008, 'MG', 'Crisólita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3009, 'MG', 'Crispim Jaques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3010, 'MG', 'Cristais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3011, 'MG', 'Cristália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3012, 'MG', 'Cristiano Otoni');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3013, 'MG', 'Cristina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3014, 'MG', 'Crucilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3015, 'MG', 'Cruzeiro da Fortaleza');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 3000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3016, 'MG', 'Cruzeiro dos Peixotos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3017, 'MG', 'Cruzília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3018, 'MG', 'Cubas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3019, 'MG', 'Cuité Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3020, 'MG', 'Cuparaque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3021, 'MG', 'Curimataí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3022, 'MG', 'Curral de Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3023, 'MG', 'Curvelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3024, 'MG', 'Datas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3025, 'MG', 'Delfim Moreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3026, 'MG', 'Delfinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3027, 'MG', 'Delta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3028, 'MG', 'Deputado Augusto Clementino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3029, 'MG', 'Derribadinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3030, 'MG', 'Descoberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3031, 'MG', 'Desembargador Otoni');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3032, 'MG', 'Desemboque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3033, 'MG', 'Desterro de Entre Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3034, 'MG', 'Desterro do Melo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3035, 'MG', 'Diamante de Ubá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3036, 'MG', 'Diamantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3037, 'MG', 'Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3038, 'MG', 'Dias Tavares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3039, 'MG', 'Diogo de Vasconcelos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3040, 'MG', 'Dionísio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3041, 'MG', 'Divinésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3042, 'MG', 'Divino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3043, 'MG', 'Divino das Laranjeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3044, 'MG', 'Divino de Virgolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3045, 'MG', 'Divino Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3046, 'MG', 'Divinolândia de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3047, 'MG', 'Divinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3048, 'MG', 'Divisa Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3049, 'MG', 'Divisa Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3050, 'MG', 'Divisópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3051, 'MG', 'Dois de Abril');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3052, 'MG', 'Dom Bosco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3053, 'MG', 'Dom Cavati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3054, 'MG', 'Dom Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3055, 'MG', 'Dom Lara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3056, 'MG', 'Dom Modesto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3057, 'MG', 'Dom Silvério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3058, 'MG', 'Dom Viçoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3059, 'MG', 'Dona Euzébia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3060, 'MG', 'Dores da Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3061, 'MG', 'Dores de Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3062, 'MG', 'Dores de Guanhães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3063, 'MG', 'Dores do Indaiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3064, 'MG', 'Dores do Paraibuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3065, 'MG', 'Dores do Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3066, 'MG', 'Doresópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3067, 'MG', 'Douradinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3068, 'MG', 'Douradoquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3069, 'MG', 'Doutor Campolina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3070, 'MG', 'Doutor Lund');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3071, 'MG', 'Durandé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3072, 'MG', 'Edgard Melo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3073, 'MG', 'Elói Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3074, 'MG', 'Emboabas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3075, 'MG', 'Engenheiro Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3076, 'MG', 'Engenheiro Correia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3077, 'MG', 'Engenheiro Navarro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3078, 'MG', 'Engenheiro Schnoor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3079, 'MG', 'Engenho do Ribeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3080, 'MG', 'Engenho Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3081, 'MG', 'Entre Folhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3082, 'MG', 'Entre Rios de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3083, 'MG', 'Epaminondas Otoni');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3084, 'MG', 'Ermidinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3085, 'MG', 'Ervália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3086, 'MG', 'Esmeraldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3087, 'MG', 'Esmeraldas de Ferros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3088, 'MG', 'Espera Feliz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3089, 'MG', 'Espinosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3090, 'MG', 'Espírito Santo do Dourado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3091, 'MG', 'Esteios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3092, 'MG', 'Estevão de Araújo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3093, 'MG', 'Estiva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3094, 'MG', 'Estrela da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3095, 'MG', 'Estrela Dalva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3096, 'MG', 'Estrela de Jordânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3097, 'MG', 'Estrela do Indaiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3098, 'MG', 'Estrela do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3099, 'MG', 'Eugenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3100, 'MG', 'Euxenita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3101, 'MG', 'Ewbank da Câmara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3102, 'MG', 'Expedicionário Alício');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3103, 'MG', 'Extração');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3104, 'MG', 'Extrema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3105, 'MG', 'Fama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3106, 'MG', 'Faria Lemos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3107, 'MG', 'Farias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3108, 'MG', 'Fechados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3109, 'MG', 'Felicina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3110, 'MG', 'Felício dos Santos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3111, 'MG', 'Felisburgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3112, 'MG', 'Felixlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3113, 'MG', 'Fernandes Tourinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3114, 'MG', 'Fernão Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3115, 'MG', 'Ferreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3116, 'MG', 'Ferreirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3117, 'MG', 'Ferros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3118, 'MG', 'Ferruginha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3119, 'MG', 'Fervedouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3120, 'MG', 'Fidalgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3121, 'MG', 'Fidelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3122, 'MG', 'Flor de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3123, 'MG', 'Florália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3124, 'MG', 'Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3125, 'MG', 'Florestal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3126, 'MG', 'Florestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3127, 'MG', 'Fonseca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3128, 'MG', 'Formiga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3129, 'MG', 'Formoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3130, 'MG', 'Fortaleza de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3131, 'MG', 'Fortuna de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3132, 'MG', 'Francisco Badaró');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3133, 'MG', 'Francisco Dumont');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3134, 'MG', 'Francisco Sá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3135, 'MG', 'Franciscópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3136, 'MG', 'Frei Eustáquio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3137, 'MG', 'Frei Gaspar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3138, 'MG', 'Frei Inocêncio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3139, 'MG', 'Frei Lagonegro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3140, 'MG', 'Frei Orlando');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3141, 'MG', 'Frei Serafim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3142, 'MG', 'Freire Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3143, 'MG', 'Fronteira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3144, 'MG', 'Fronteira dos Vales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3145, 'MG', 'Fruta de Leite');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3146, 'MG', 'Frutal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3147, 'MG', 'Funchal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3148, 'MG', 'Funilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3150, 'MG', 'Furquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3151, 'MG', 'Galego');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3152, 'MG', 'Galena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3153, 'MG', 'Galiléia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3154, 'MG', 'Gama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3155, 'MG', 'Gameleiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3156, 'MG', 'Garapuava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3157, 'MG', 'Gavião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3158, 'MG', 'Glaucilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3159, 'MG', 'Glaura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3160, 'MG', 'Glucínio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3161, 'MG', 'Goiabeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3162, 'MG', 'Goianá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3163, 'MG', 'Goianases');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3164, 'MG', 'Gonçalves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3165, 'MG', 'Gonzaga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3166, 'MG', 'Gororós');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3167, 'MG', 'Gorutuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3168, 'MG', 'Gouvea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3169, 'MG', 'Governador Valadares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3170, 'MG', 'Gramínea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3171, 'MG', 'Granada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3172, 'MG', 'Grão Mogol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3173, 'MG', 'Grota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3174, 'MG', 'Grupiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3175, 'MG', 'Guaicui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3176, 'MG', 'Guaipava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3177, 'MG', 'Guanhães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3178, 'MG', 'Guapé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3179, 'MG', 'Guaraciaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3180, 'MG', 'Guaraciama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3181, 'MG', 'Guaranésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3182, 'MG', 'Guarani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3183, 'MG', 'Guaranilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3184, 'MG', 'Guarará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3185, 'MG', 'Guarataia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3186, 'MG', 'Guarda dos Ferreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3187, 'MG', 'Guarda-Mor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3188, 'MG', 'Guardinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3189, 'MG', 'Guaxima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3190, 'MG', 'Guaxupé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3191, 'MG', 'Guidoval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3192, 'MG', 'Guimarânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3193, 'MG', 'Guinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3194, 'MG', 'Guiricema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3195, 'MG', 'Gurinhatã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3196, 'MG', 'Heliodora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3197, 'MG', 'Hematita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3198, 'MG', 'Hermilo Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3199, 'MG', 'Honorópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3200, 'MG', 'Iapu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3201, 'MG', 'Ibertioga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3202, 'MG', 'Ibiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3203, 'MG', 'Ibiaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3204, 'MG', 'Ibiracatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3205, 'MG', 'Ibiraci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3206, 'MG', 'Ibirité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3207, 'MG', 'Ibitira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3208, 'MG', 'Ibitiúra de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3209, 'MG', 'Ibituruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3210, 'MG', 'Icaraí de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3211, 'MG', 'Igarapé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3212, 'MG', 'Igaratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3213, 'MG', 'Iguatama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3214, 'MG', 'Ijaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3215, 'MG', 'Ilhéus do Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3216, 'MG', 'Ilicínea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3217, 'MG', 'Imbé de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3218, 'MG', 'Inconfidentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3219, 'MG', 'Indaiabira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3220, 'MG', 'Independência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3221, 'MG', 'Indianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3222, 'MG', 'Ingaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3223, 'MG', 'Inhaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3224, 'MG', 'Inhapim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3225, 'MG', 'Inhaúma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3226, 'MG', 'Inimutaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3227, 'MG', 'Ipaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3228, 'MG', 'Ipanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3229, 'MG', 'Ipatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3230, 'MG', 'Ipiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3231, 'MG', 'Ipoema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3232, 'MG', 'Ipuiúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3233, 'MG', 'Iraí de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3234, 'MG', 'Itabira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3235, 'MG', 'Itabirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3236, 'MG', 'Itabirito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3237, 'MG', 'Itaboca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3238, 'MG', 'Itacambira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3239, 'MG', 'Itacarambi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3240, 'MG', 'Itaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3241, 'MG', 'Itacolomi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3242, 'MG', 'Itaguara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3243, 'MG', 'Itaim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3244, 'MG', 'Itaipé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3245, 'MG', 'Itajubá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3246, 'MG', 'Itajutiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3247, 'MG', 'Itamarandiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3248, 'MG', 'Itamarati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3249, 'MG', 'Itamarati de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3250, 'MG', 'Itambacuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3251, 'MG', 'Itambé do Mato Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3252, 'MG', 'Itamirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3253, 'MG', 'Itamogi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3254, 'MG', 'Itamonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3255, 'MG', 'Itamuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3256, 'MG', 'Itanhandu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3257, 'MG', 'Itanhomi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3258, 'MG', 'Itaobim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3259, 'MG', 'Itapagipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3260, 'MG', 'Itapanhoacanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3261, 'MG', 'Itapecerica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3262, 'MG', 'Itapeva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3263, 'MG', 'Itapiru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3264, 'MG', 'Itapirucu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3265, 'MG', 'Itatiaiuçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3266, 'MG', 'Itaú de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3267, 'MG', 'Itaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3268, 'MG', 'Itauninha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3269, 'MG', 'Itaverava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3270, 'MG', 'Itererê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3271, 'MG', 'Itinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3272, 'MG', 'Itira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3273, 'MG', 'Itueta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3274, 'MG', 'Ituí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3275, 'MG', 'Ituiutaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3276, 'MG', 'Itumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3277, 'MG', 'Iturama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3278, 'MG', 'Itutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3279, 'MG', 'Jaboticatubas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3280, 'MG', 'Jacarandira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3281, 'MG', 'Jacaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3282, 'MG', 'Jacinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3283, 'MG', 'Jacuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3284, 'MG', 'Jacutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3285, 'MG', 'Jaguaraçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3286, 'MG', 'Jaguarão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3287, 'MG', 'Jaguaritira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3288, 'MG', 'Jaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3289, 'MG', 'Jampruca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3290, 'MG', 'Janaúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3291, 'MG', 'Januária');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3292, 'MG', 'Japaraíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3293, 'MG', 'Japonvar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3294, 'MG', 'Jardinésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3295, 'MG', 'Jeceaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3296, 'MG', 'Jenipapo de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3297, 'MG', 'Jequeri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3298, 'MG', 'Jequitaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3299, 'MG', 'Jequitibá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3300, 'MG', 'Jequitinhonha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3301, 'MG', 'Jesuânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3302, 'MG', 'Joaíma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3303, 'MG', 'Joanésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3304, 'MG', 'João Monlevade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3305, 'MG', 'João Pinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3306, 'MG', 'Joaquim Felício');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3307, 'MG', 'Jordânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3308, 'MG', 'José Gonçalves de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3309, 'MG', 'José Raydan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3310, 'MG', 'Joselândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3311, 'MG', 'Josenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3312, 'MG', 'Juatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3313, 'MG', 'Jubaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3314, 'MG', 'Juiraçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3315, 'MG', 'Juiz de Fora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3316, 'MG', 'Junco de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3317, 'MG', 'Juramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3318, 'MG', 'Juréia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3319, 'MG', 'Juruaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3320, 'MG', 'Jurumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3321, 'MG', 'Justinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3322, 'MG', 'Juvenília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3323, 'MG', 'Lacerdinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3324, 'MG', 'Ladainha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3325, 'MG', 'Lagamar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3326, 'MG', 'Lagoa Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3327, 'MG', 'Lagoa da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3328, 'MG', 'Lagoa dos Patos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3329, 'MG', 'Lagoa Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3330, 'MG', 'Lagoa Formosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3331, 'MG', 'Lagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3332, 'MG', 'Lagoa Santa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3333, 'MG', 'Lajinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3334, 'MG', 'Lambari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3335, 'MG', 'Lamim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3336, 'MG', 'Lamounier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3337, 'MG', 'Lapinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3338, 'MG', 'Laranjal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3339, 'MG', 'Laranjeiras de Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3340, 'MG', 'Lassance');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3341, 'MG', 'Lavras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3342, 'MG', 'Leandro Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3343, 'MG', 'Leme do Prado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3344, 'MG', 'Leopoldina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3345, 'MG', 'Levinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3346, 'MG', 'Liberdade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3347, 'MG', 'Lima Duarte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3348, 'MG', 'Limeira do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3349, 'MG', 'Limeira de Mantena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3350, 'MG', 'Lobo Leite');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3351, 'MG', 'Lontra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3352, 'MG', 'Lourenço Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3353, 'MG', 'Lufa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3354, 'MG', 'Luisburgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3355, 'MG', 'Luislândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3356, 'MG', 'Luiz Pires de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3357, 'MG', 'Luizlândia do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3358, 'MG', 'Luminárias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3359, 'MG', 'Luminosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3360, 'MG', 'Luz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3361, 'MG', 'Macaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3362, 'MG', 'Machacalis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3363, 'MG', 'Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3364, 'MG', 'Macuco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3365, 'MG', 'Macuco de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3366, 'MG', 'Madre de Deus de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3367, 'MG', 'Mãe dos Homens');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3368, 'MG', 'Major Ezequiel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3369, 'MG', 'Major Porto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3370, 'MG', 'Malacacheta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3371, 'MG', 'Mamonas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3372, 'MG', 'Manga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3373, 'MG', 'Manhuaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3374, 'MG', 'Manhumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3375, 'MG', 'Mantena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3376, 'MG', 'Mantiqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3377, 'MG', 'Mantiqueira do Palmital');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3378, 'MG', 'Mar de Espanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3379, 'MG', 'Marambainha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3380, 'MG', 'Maravilhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3381, 'MG', 'Maria da Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3382, 'MG', 'Mariana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3383, 'MG', 'Marilac');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3384, 'MG', 'Marilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3385, 'MG', 'Mário Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3386, 'MG', 'Maripá de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3387, 'MG', 'Marliéria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3388, 'MG', 'Marmelópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3389, 'MG', 'Martinésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3390, 'MG', 'Martinho Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3391, 'MG', 'Martins Guimarães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3392, 'MG', 'Martins Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3393, 'MG', 'Mata Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3394, 'MG', 'Materlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3395, 'MG', 'Mateus Leme');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3396, 'MG', 'Mathias Lobato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3397, 'MG', 'Matias Barbosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3398, 'MG', 'Matias Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3399, 'MG', 'Matipó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3400, 'MG', 'Mato Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3401, 'MG', 'Matozinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3402, 'MG', 'Matutina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3403, 'MG', 'Medeiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3404, 'MG', 'Medina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3405, 'MG', 'Melo Viana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3406, 'MG', 'Mendanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3407, 'MG', 'Mendes Pimentel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3408, 'MG', 'Mendonça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3409, 'MG', 'Mercês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3410, 'MG', 'Mercês de Água Limpa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3411, 'MG', 'Mesquita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3412, 'MG', 'Mestre Caetano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3413, 'MG', 'Miguel Burnier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3414, 'MG', 'Milagre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3415, 'MG', 'Milho Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3416, 'MG', 'Minas Novas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3417, 'MG', 'Minduri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3418, 'MG', 'Mirabela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3419, 'MG', 'Miradouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3420, 'MG', 'Miragaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3421, 'MG', 'Miraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3422, 'MG', 'Miralta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3423, 'MG', 'Mirantão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3424, 'MG', 'Miraporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3425, 'MG', 'Miravânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3426, 'MG', 'Missionário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3427, 'MG', 'Mocambeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3428, 'MG', 'Mocambinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3429, 'MG', 'Moeda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3430, 'MG', 'Moema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3431, 'MG', 'Monjolinho de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3432, 'MG', 'Monjolos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3433, 'MG', 'Monsenhor Horta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3434, 'MG', 'Monsenhor Isidro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3435, 'MG', 'Monsenhor João Alexandre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3436, 'MG', 'Monsenhor Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3437, 'MG', 'Montalvânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3438, 'MG', 'Monte Alegre de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3439, 'MG', 'Monte Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3440, 'MG', 'Monte Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3441, 'MG', 'Monte Carmelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3442, 'MG', 'Monte Celeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3443, 'MG', 'Monte Formoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3444, 'MG', 'Monte Rei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3445, 'MG', 'Monte Santo de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3446, 'MG', 'Monte Sião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3447, 'MG', 'Monte Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3448, 'MG', 'Montes Claros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3449, 'MG', 'Montezuma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3450, 'MG', 'Morada Nova de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3451, 'MG', 'Morro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3452, 'MG', 'Morro da Garça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3453, 'MG', 'Morro do Ferro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3454, 'MG', 'Morro do Pilar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3455, 'MG', 'Morro Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3456, 'MG', 'Mucuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3457, 'MG', 'Mundo Novo de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3458, 'MG', 'Munhoz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3459, 'MG', 'Muquém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3460, 'MG', 'Muriaé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3461, 'MG', 'Mutum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3462, 'MG', 'Muzambinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3463, 'MG', 'Nacip Raydan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3464, 'MG', 'Nanuque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3465, 'MG', 'Naque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3466, 'MG', 'Naque-Nanuque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3467, 'MG', 'Natalândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3468, 'MG', 'Natércia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3469, 'MG', 'Nazaré de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3470, 'MG', 'Nazareno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3471, 'MG', 'Nelson de Sena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3472, 'MG', 'Neolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3473, 'MG', 'Nepomuceno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3474, 'MG', 'Nhandutiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3475, 'MG', 'Nicolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3476, 'MG', 'Ninheira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3477, 'MG', 'Nova Belém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3478, 'MG', 'Nova Era');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3479, 'MG', 'Nova Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3480, 'MG', 'Nova Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3481, 'MG', 'Nova Minda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3482, 'MG', 'Nova Módica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3483, 'MG', 'Nova Ponte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3484, 'MG', 'Nova Porteirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3485, 'MG', 'Nova Resende');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3486, 'MG', 'Nova Serrana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3487, 'MG', 'Nova União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3488, 'MG', 'Novilhona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3489, 'MG', 'Novo Cruzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3490, 'MG', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3491, 'MG', 'Novo Oriente de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3492, 'MG', 'Novorizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3493, 'MG', 'Ocidente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3494, 'MG', 'Olaria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3495, 'MG', 'Olegário Maciel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3496, 'MG', 'Olhos D Água do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3497, 'MG', 'Olhos D Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3498, 'MG', 'Olímpio Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3499, 'MG', 'Olímpio Noronha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3500, 'MG', 'Oliveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3501, 'MG', 'Oliveira Fortes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3502, 'MG', 'Onça de Pitangui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3503, 'MG', 'Oratórios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3504, 'MG', 'Orizânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3505, 'MG', 'Ouro Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3506, 'MG', 'Ouro Fino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3507, 'MG', 'Ouro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3508, 'MG', 'Ouro Verde de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3509, 'MG', 'Paciência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3510, 'MG', 'Padre Brito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3511, 'MG', 'Padre Carvalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3512, 'MG', 'Padre Felisberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3513, 'MG', 'Padre Fialho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3514, 'MG', 'Padre João Afonso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3515, 'MG', 'Padre Júlio Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3516, 'MG', 'Padre Paraíso');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 3500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3517, 'MG', 'Padre Pinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3518, 'MG', 'Padre Viegas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3519, 'MG', 'Pai Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3520, 'MG', 'Paineiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3521, 'MG', 'Pains');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3522, 'MG', 'Paiolinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3523, 'MG', 'Paiva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3524, 'MG', 'Palma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3525, 'MG', 'Palmeiral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3526, 'MG', 'Palmeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3527, 'MG', 'Palmital dos Carvalhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3528, 'MG', 'Palmópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3529, 'MG', 'Pântano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3530, 'MG', 'Papagaios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3531, 'MG', 'Pará de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3532, 'MG', 'Paracatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3533, 'MG', 'Paraguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3534, 'MG', 'Paraguai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3535, 'MG', 'Paraíso Garcia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3536, 'MG', 'Paraisópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3537, 'MG', 'Paraopeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3538, 'MG', 'Paredão de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3539, 'MG', 'Parque Durval de Barros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3540, 'MG', 'Parque Industrial');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3541, 'MG', 'Passa Dez');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3542, 'MG', 'Passa Quatro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3543, 'MG', 'Passa Tempo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3544, 'MG', 'Passa Vinte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3545, 'MG', 'Passabém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3546, 'MG', 'Passagem de Mariana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3547, 'MG', 'Passos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3548, 'MG', 'Patis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3549, 'MG', 'Patos de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3550, 'MG', 'Patrimônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3551, 'MG', 'Patrocínio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3552, 'MG', 'Patrocínio do Muriaé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3553, 'MG', 'Paula Cândido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3554, 'MG', 'Paula Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3555, 'MG', 'Paulistas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3556, 'MG', 'Pavão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3557, 'MG', 'Pé do Morro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3558, 'MG', 'Peçanha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3559, 'MG', 'Pedra Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3560, 'MG', 'Pedra Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3561, 'MG', 'Pedra Corrida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3562, 'MG', 'Pedra do Anta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3563, 'MG', 'Pedra do Indaiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3564, 'MG', 'Pedra do Sino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3565, 'MG', 'Pedra Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3566, 'MG', 'Pedra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3567, 'MG', 'Pedra Menina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3568, 'MG', 'Pedralva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3569, 'MG', 'Pedras de Maria da Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3570, 'MG', 'Pedrinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3571, 'MG', 'Pedro Leopoldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3572, 'MG', 'Pedro Lessa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3573, 'MG', 'Pedro Teixeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3574, 'MG', 'Pedro Versiani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3575, 'MG', 'Penédia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3576, 'MG', 'Penha de França');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3577, 'MG', 'Penha do Capim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3578, 'MG', 'Penha do Cassiano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3579, 'MG', 'Penha do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3580, 'MG', 'Penha Longa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3581, 'MG', 'Pequeri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3582, 'MG', 'Pequi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3583, 'MG', 'Perdigão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3584, 'MG', 'Perdilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3585, 'MG', 'Perdizes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3586, 'MG', 'Perdões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3587, 'MG', 'Pereirinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3588, 'MG', 'Periquito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3589, 'MG', 'Perpétuo Socorro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3590, 'MG', 'Pescador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3591, 'MG', 'Petúnia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3592, 'MG', 'Piacatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3593, 'MG', 'Pião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3594, 'MG', 'Piau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3595, 'MG', 'Pic Sagarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3596, 'MG', 'Piedade de Caratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3597, 'MG', 'Piedade de Ponte Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3598, 'MG', 'Piedade do Paraopeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3599, 'MG', 'Piedade do Rio Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3600, 'MG', 'Piedade dos Gerais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3601, 'MG', 'Pilar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3602, 'MG', 'Pimenta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3603, 'MG', 'Pindaíbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3604, 'MG', 'Pingo-D Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3605, 'MG', 'Pinheirinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3606, 'MG', 'Pinheiros Altos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3607, 'MG', 'Pinhotiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3608, 'MG', 'Pintópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3609, 'MG', 'Pintos Negreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3610, 'MG', 'Piracaiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3611, 'MG', 'Piracema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3612, 'MG', 'Pirajuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3613, 'MG', 'Piranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3614, 'MG', 'Piranguçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3615, 'MG', 'Piranguinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3616, 'MG', 'Piranguita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3617, 'MG', 'Pirapanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3618, 'MG', 'Pirapetinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3619, 'MG', 'Pirapora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3620, 'MG', 'Piraúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3621, 'MG', 'Alto Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3622, 'MG', 'Piscamba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3623, 'MG', 'Pitangui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3624, 'MG', 'Pitarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3625, 'MG', 'Piumhi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3626, 'MG', 'Planalto de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3627, 'MG', 'Planura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3628, 'MG', 'Plautino Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3629, 'MG', 'Poaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3630, 'MG', 'Poço Fundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3631, 'MG', 'Poções de Paineiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3632, 'MG', 'Poços de Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3633, 'MG', 'Pocrane');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3634, 'MG', 'Pompéu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3635, 'MG', 'Poncianos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3636, 'MG', 'Pontalete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3637, 'MG', 'Ponte Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3638, 'MG', 'Ponte Alta de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3639, 'MG', 'Ponte dos Ciganos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3640, 'MG', 'Ponte Firme');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3641, 'MG', 'Ponte Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3642, 'MG', 'Ponte Segura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3643, 'MG', 'Pontevila');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3644, 'MG', 'Ponto Chique');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3645, 'MG', 'Ponto Chique do Martelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3646, 'MG', 'Ponto do Marambaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3647, 'MG', 'Ponto dos Volantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3648, 'MG', 'Porteirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3649, 'MG', 'Porto Agrário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3650, 'MG', 'Porto das Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3651, 'MG', 'Porto dos Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3652, 'MG', 'Porto Firme');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3653, 'MG', 'Poté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3654, 'MG', 'Pouso Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3655, 'MG', 'Pouso Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3656, 'MG', 'Prados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3657, 'MG', 'Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3658, 'MG', 'Pratápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3659, 'MG', 'Pratinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3660, 'MG', 'Presidente Bernardes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3661, 'MG', 'Presidente Juscelino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3662, 'MG', 'Presidente Kubitschek');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3663, 'MG', 'Presidente Olegário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3664, 'MG', 'Presidente Pena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3665, 'MG', 'Professor Sperber');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3666, 'MG', 'Providência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3667, 'MG', 'Prudente de Morais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3668, 'MG', 'Quartel de São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3669, 'MG', 'Quartel do Sacramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3670, 'MG', 'Quartel Geral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3671, 'MG', 'Quatituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3672, 'MG', 'Queixada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3673, 'MG', 'Queluzita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3674, 'MG', 'Quem-Quem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3675, 'MG', 'Quilombo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3676, 'MG', 'Quintinos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3677, 'MG', 'Raposos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3678, 'MG', 'Raul Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3679, 'MG', 'Ravena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3680, 'MG', 'Realeza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3681, 'MG', 'Recreio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3682, 'MG', 'Reduto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3683, 'MG', 'Resende Costa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3684, 'MG', 'Resplendor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3685, 'MG', 'Ressaquinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3686, 'MG', 'Riachinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3687, 'MG', 'Riacho da Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3688, 'MG', 'Riacho dos Machados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3689, 'MG', 'Ribeirão das Neves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3690, 'MG', 'Ribeirão de São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3691, 'MG', 'Ribeirão Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3692, 'MG', 'Ribeiro Junqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3693, 'MG', 'Ribeiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3694, 'MG', 'Rio Acima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3695, 'MG', 'Rio Casca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3696, 'MG', 'Rio das Mortes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3697, 'MG', 'Rio do Prado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3698, 'MG', 'Rio Doce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3699, 'MG', 'Rio Espera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3700, 'MG', 'Rio Manso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3701, 'MG', 'Rio Melo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3702, 'MG', 'Rio Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3703, 'MG', 'Rio Paranaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3704, 'MG', 'Rio Pardo de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3705, 'MG', 'Rio Piracicaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3706, 'MG', 'Rio Pomba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3707, 'MG', 'Rio Pretinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3708, 'MG', 'Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3709, 'MG', 'Rio Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3710, 'MG', 'Ritápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3711, 'MG', 'Roça Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3712, 'MG', 'Roças Novas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3713, 'MG', 'Rochedo de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3714, 'MG', 'Rodeador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3715, 'MG', 'Rodeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3716, 'MG', 'Rodrigo Silva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3717, 'MG', 'Romaria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3718, 'MG', 'Rosário da Limeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3719, 'MG', 'Rosário de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3720, 'MG', 'Rosário do Pontal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3721, 'MG', 'Roseiral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3722, 'MG', 'Rubelita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3723, 'MG', 'Rubim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3724, 'MG', 'Sabará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3725, 'MG', 'Sabinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3726, 'MG', 'Sacramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3727, 'MG', 'Salinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3728, 'MG', 'Salitre de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3729, 'MG', 'Salto da Divisa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3730, 'MG', 'Sanatório Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3731, 'MG', 'Santa Bárbara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3732, 'MG', 'Santa Bárbara do Leste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3733, 'MG', 'Santa Bárbara do Monte Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3734, 'MG', 'Santa Bárbara do Tugúrio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3735, 'MG', 'Santa Cruz da Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3736, 'MG', 'Santa Cruz de Botumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3737, 'MG', 'Santa Cruz de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3738, 'MG', 'Santa Cruz de Salinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3739, 'MG', 'Santa Cruz do Escalvado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3740, 'MG', 'Santa Cruz do Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3741, 'MG', 'Santa da Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3742, 'MG', 'Santa Efigênia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3743, 'MG', 'Santa Efigênia de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3744, 'MG', 'Santa Fé de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3745, 'MG', 'Santa Filomena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3746, 'MG', 'Santa Helena de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3748, 'MG', 'Santa Juliana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3749, 'MG', 'Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3750, 'MG', 'Santa Luzia da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3751, 'MG', 'Santa Margarida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3752, 'MG', 'Santa Maria de Itabira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3753, 'MG', 'Santa Maria do Salto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3754, 'MG', 'Santa Maria do Suaçuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3755, 'MG', 'Santa Rita da Estrela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3756, 'MG', 'Santa Rita de Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3757, 'MG', 'Santa Rita de Jacutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3758, 'MG', 'Santa Rita de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3759, 'MG', 'Santa Rita de Ouro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3760, 'MG', 'Santa Rita do Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3761, 'MG', 'Santa Rita do Ibitipoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3762, 'MG', 'Santa Rita do Itueto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3763, 'MG', 'Santa Rita do Rio do Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3764, 'MG', 'Santa Rita do Sapucaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3765, 'MG', 'Santa Rita Durão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3766, 'MG', 'Santa Rosa da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3767, 'MG', 'Santa Rosa de Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3768, 'MG', 'Santa Rosa dos Dourados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3769, 'MG', 'Santa Teresa do Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3770, 'MG', 'Santa Terezinha de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3771, 'MG', 'Santa Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3772, 'MG', 'Santana da Vargem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3773, 'MG', 'Santana de Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3774, 'MG', 'Santana de Cataguases');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3775, 'MG', 'Santana de Patos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3776, 'MG', 'Santana de Pirapama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3777, 'MG', 'Santana do Alfié');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3778, 'MG', 'Santana do Araçuaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3779, 'MG', 'Santana do Campestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3780, 'MG', 'Santana do Capivari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3781, 'MG', 'Santana do Deserto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3782, 'MG', 'Santana do Garambéu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3783, 'MG', 'Santana do Jacaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3784, 'MG', 'Santana do Manhuaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3785, 'MG', 'Santana do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3786, 'MG', 'Santana do Paraopeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3787, 'MG', 'Santana do Riacho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3788, 'MG', 'Santana do Tabuleiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3789, 'MG', 'Santana dos Montes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3790, 'MG', 'Santo Antônio da Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3791, 'MG', 'Santo Antônio da Fortaleza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3792, 'MG', 'Santo Antônio da Vargem Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3793, 'MG', 'Santo Antônio do Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3794, 'MG', 'Santo Antônio do Aventureiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3795, 'MG', 'Santo Antônio do Boqueirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3796, 'MG', 'Santo Antônio do Cruzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3797, 'MG', 'Santo Antônio do Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3798, 'MG', 'Santo Antônio do Grama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3799, 'MG', 'Santo Antônio do Itambé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3800, 'MG', 'Santo Antônio do Jacinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3801, 'MG', 'Santo Antônio do Leite');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3802, 'MG', 'Santo Antônio do Manhuaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3803, 'MG', 'Santo Antônio do Monte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3804, 'MG', 'Santo Antônio do Mucuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3805, 'MG', 'Santo Antônio do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3806, 'MG', 'Santo Antônio do Pirapetinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3807, 'MG', 'Santo Antônio do Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3808, 'MG', 'Santo Antônio do Rio Abaixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3809, 'MG', 'Santo Antônio dos Araújos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3810, 'MG', 'Santo Antônio dos Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3811, 'MG', 'Santo Hilário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3812, 'MG', 'Santo Hipólito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3813, 'MG', 'Santos Dumont');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3814, 'MG', 'São Bartolomeu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3815, 'MG', 'São Benedito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3816, 'MG', 'São Bento Abade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3817, 'MG', 'São Bento de Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3818, 'MG', 'São Brás do Suaçuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3819, 'MG', 'São Braz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3820, 'MG', 'São Cândido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3821, 'MG', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3822, 'MG', 'São Domingos da Bocaina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3823, 'MG', 'São Domingos das Dores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3824, 'MG', 'São Domingos do Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3825, 'MG', 'São Félix de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3826, 'MG', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3827, 'MG', 'São Francisco de Paula');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3828, 'MG', 'São Francisco de Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3829, 'MG', 'São Francisco do Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3830, 'MG', 'São Francisco do Humaitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3831, 'MG', 'São Geraldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3832, 'MG', 'São Geraldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3833, 'MG', 'São Geraldo da Piedade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3834, 'MG', 'São Geraldo de Tumiritinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3835, 'MG', 'São Geraldo do Baguari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3836, 'MG', 'São Geraldo do Baixio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3837, 'MG', 'São Gonçalo de Botelhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3838, 'MG', 'São Gonçalo do Abaeté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3839, 'MG', 'São Gonçalo do Monte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3840, 'MG', 'São Gonçalo do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3841, 'MG', 'São Gonçalo do Rio Abaixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3842, 'MG', 'São Gonçalo do Rio das Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3843, 'MG', 'São Gonçalo do Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3844, 'MG', 'São Gonçalo do Sapucaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3845, 'MG', 'São Gotardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3846, 'MG', 'São Jerônimo dos Poções');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3847, 'MG', 'São João Batista do Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3848, 'MG', 'São João da Chapada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3849, 'MG', 'São João da Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3850, 'MG', 'São João da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3851, 'MG', 'São João da Ponte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3852, 'MG', 'São João da Sapucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3853, 'MG', 'São João da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3854, 'MG', 'São João da Serra Negra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3855, 'MG', 'São João da Vereda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3856, 'MG', 'São João das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3857, 'MG', 'São João Del Rei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3858, 'MG', 'São João do Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3859, 'MG', 'São João do Jacutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3860, 'MG', 'São João do Manhuaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3861, 'MG', 'São João do Manteninha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3862, 'MG', 'São João do Oriente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3863, 'MG', 'São João do Pacuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3864, 'MG', 'São João do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3865, 'MG', 'São João Evangelista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3866, 'MG', 'São João Nepomuceno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3867, 'MG', 'São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3868, 'MG', 'São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3869, 'MG', 'São Joaquim de Bicas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3870, 'MG', 'São José da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3871, 'MG', 'São José da Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3872, 'MG', 'São José da Lapa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3873, 'MG', 'São José da Pedra Menina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3874, 'MG', 'São José da Safira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3875, 'MG', 'São José da Varginha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3876, 'MG', 'Goiabal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3877, 'MG', 'São José do Acácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3878, 'MG', 'São José do Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3879, 'MG', 'São José do Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3880, 'MG', 'São José do Batatal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3881, 'MG', 'São José do Buriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3882, 'MG', 'São José do Divino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3883, 'MG', 'São José do Goiabal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3884, 'MG', 'São José do Itueto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3885, 'MG', 'São José do Jacuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3886, 'MG', 'São José do Mantimento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3887, 'MG', 'São José do Mato Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3888, 'MG', 'São José do Pântano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3889, 'MG', 'São José do Paraopeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3890, 'MG', 'São José dos Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3891, 'MG', 'São José dos Salgados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3892, 'MG', 'São Lourenço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3893, 'MG', 'São Manoel do Guaiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3894, 'MG', 'São Mateus de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3895, 'MG', 'São Miguel do Anta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3896, 'MG', 'São Pedro da Garça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3897, 'MG', 'São Pedro da União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3898, 'MG', 'São Pedro das Tabocas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3899, 'MG', 'São Pedro de Caldas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3900, 'MG', 'São Pedro do Avaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3901, 'MG', 'São Pedro do Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3902, 'MG', 'São Pedro do Jequitinhonha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3903, 'MG', 'São Pedro do Suaçuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3904, 'MG', 'São Pedro dos Ferros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3905, 'MG', 'São Roberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3906, 'MG', 'São Romão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3907, 'MG', 'São Roque de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3908, 'MG', 'São Sebastião da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3909, 'MG', 'São Sebastião da Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3910, 'MG', 'São Sebastião da Vala');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3911, 'MG', 'São Sebastião da Vargem Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3912, 'MG', 'São Sebastião da Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3913, 'MG', 'São Sebastião de Braúnas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3914, 'MG', 'São Sebastião do Anta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3915, 'MG', 'São Sebastião do Baixio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3916, 'MG', 'São Sebastião do Barreado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3917, 'MG', 'São Sebastião do Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3918, 'MG', 'São Sebastião do Bonsucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3919, 'MG', 'São Sebastião do Bugre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3920, 'MG', 'São Sebastião do Gil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3921, 'MG', 'São Sebastião do Maranhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3922, 'MG', 'São Sebastião do Óculo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3923, 'MG', 'São Sebastião do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3924, 'MG', 'São Sebastião do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3925, 'MG', 'São Sebastião do Pontal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3926, 'MG', 'São Sebastião do Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3927, 'MG', 'São Sebastião do Rio Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3928, 'MG', 'São Sebastião do Sacramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3929, 'MG', 'São Sebastião do Soberbo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3930, 'MG', 'São Sebastião dos Poções');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3931, 'MG', 'São Sebastião dos Robertos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3932, 'MG', 'São Sebastião dos Torres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3933, 'MG', 'São Tiago');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3934, 'MG', 'São Tomás de Aquino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3935, 'MG', 'São Thomé das Letras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3936, 'MG', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3937, 'MG', 'São Vicente da Estrela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3938, 'MG', 'São Vicente de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3939, 'MG', 'São Vicente do Grama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3940, 'MG', 'São Vicente do Rio Doce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3941, 'MG', 'São Vítor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3942, 'MG', 'Sapucaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3943, 'MG', 'Sapucaí-Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3944, 'MG', 'Sapucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3945, 'MG', 'Sapucaia de Guanhães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3946, 'MG', 'Sapucaia do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3947, 'MG', 'Sarandira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3948, 'MG', 'Sardoá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3949, 'MG', 'Sarzedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3950, 'MG', 'Saudade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3951, 'MG', 'Sem Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3952, 'MG', 'Senador Amaral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3953, 'MG', 'Senador Cortes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3954, 'MG', 'Senador Firmino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3955, 'MG', 'Senador José Bento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3956, 'MG', 'Senador Melo Viana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3957, 'MG', 'Senador Modestino Gonçalves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3958, 'MG', 'Senador Mourão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3959, 'MG', 'Senhora da Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3960, 'MG', 'Senhora da Penha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3961, 'MG', 'Senhora das Dores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3962, 'MG', 'Senhora de Oliveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3963, 'MG', 'Senhora do Carmo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3964, 'MG', 'Senhora do Porto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3965, 'MG', 'Senhora dos Remédios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3966, 'MG', 'Sereno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3967, 'MG', 'Sericita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3968, 'MG', 'Seritinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3969, 'MG', 'Serra Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3970, 'MG', 'Serra Azul de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3971, 'MG', 'Serra Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3972, 'MG', 'Serra da Canastra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3973, 'MG', 'Serra da Saudade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3974, 'MG', 'Serra das Araras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3975, 'MG', 'Serra do Camapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3976, 'MG', 'Serra do Salitre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3977, 'MG', 'Serra dos Aimorés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3978, 'MG', 'Serra dos Lemes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3979, 'MG', 'Serra Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3980, 'MG', 'Serrania');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3981, 'MG', 'Serranópolis de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3982, 'MG', 'Serranos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3983, 'MG', 'Serro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3984, 'MG', 'Sertãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3985, 'MG', 'Sete Cachoeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3986, 'MG', 'Sete Lagoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3987, 'MG', 'Setubinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3988, 'MG', 'Silva Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3989, 'MG', 'Silva Xavier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3990, 'MG', 'Silvano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3991, 'MG', 'Silveira Carvalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3992, 'MG', 'Silveirânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3993, 'MG', 'Silvestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3994, 'MG', 'Silvianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3995, 'MG', 'Simão Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3996, 'MG', 'Simão Pereira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3997, 'MG', 'Simonésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3998, 'MG', 'Sobral Pinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (3999, 'MG', 'Sobrália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4000, 'MG', 'Soledade de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4001, 'MG', 'Sopa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4002, 'MG', 'Tabajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4003, 'MG', 'Tabaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4004, 'MG', 'Tabuão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4005, 'MG', 'Tabuleiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4006, 'MG', 'Taiobeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4007, 'MG', 'Taparuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4008, 'MG', 'Tapira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4009, 'MG', 'Tapiraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4010, 'MG', 'Tapuirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4011, 'MG', 'Taquaraçu de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4012, 'MG', 'Taruaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4013, 'MG', 'Tarumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4014, 'MG', 'Tebas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4015, 'MG', 'Teixeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4016, 'MG', 'Tejuco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4017, 'MG', 'Teófilo Otoni');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 4000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4018, 'MG', 'Terra Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4019, 'MG', 'Timóteo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4020, 'MG', 'Tiradentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4021, 'MG', 'Tiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4022, 'MG', 'Tobati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4023, 'MG', 'Tocandira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4024, 'MG', 'Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4025, 'MG', 'Tocos do Moji');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4026, 'MG', 'Toledo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4027, 'MG', 'Tomás Gonzaga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4028, 'MG', 'Tombos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4029, 'MG', 'Topázio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4030, 'MG', 'Torneiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4031, 'MG', 'Torreões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4032, 'MG', 'Três Corações');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4033, 'MG', 'Três Ilhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4034, 'MG', 'Três Marias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4035, 'MG', 'Três Pontas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4036, 'MG', 'Trimonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4037, 'MG', 'Tuiutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4038, 'MG', 'Tumiritinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4039, 'MG', 'Tupaciguara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4040, 'MG', 'Tuparecê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4041, 'MG', 'Turmalina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4042, 'MG', 'Turvolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4043, 'MG', 'Ubá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4044, 'MG', 'Ubaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4045, 'MG', 'Ubaporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4046, 'MG', 'Ubari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4047, 'MG', 'Uberaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4048, 'MG', 'Uberlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4049, 'MG', 'Umburatiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4050, 'MG', 'Umbuzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4051, 'MG', 'Unaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4052, 'MG', 'União de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4053, 'MG', 'Uruana de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4054, 'MG', 'Urucânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4055, 'MG', 'Urucuia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4056, 'MG', 'Usina Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4057, 'MG', 'Vai-Volta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4058, 'MG', 'Valadares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4059, 'MG', 'Valão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4060, 'MG', 'Vale Verde de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4061, 'MG', 'Valo Fundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4062, 'MG', 'Vargem Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4063, 'MG', 'Vargem Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4064, 'MG', 'Vargem Grande do Rio Pardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4065, 'MG', 'Vargem Linda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4066, 'MG', 'Varginha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4067, 'MG', 'Varjão de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4068, 'MG', 'Várzea da Palma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4069, 'MG', 'Varzelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4070, 'MG', 'Vau-Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4071, 'MG', 'Vazante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4072, 'MG', 'Vera Cruz de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4073, 'MG', 'Verdelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4074, 'MG', 'Vereda do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4075, 'MG', 'Veredas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4076, 'MG', 'Veredinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4077, 'MG', 'Veríssimo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4078, 'MG', 'Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4079, 'MG', 'Vermelho Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4080, 'MG', 'Vermelho Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4081, 'MG', 'Vespasiano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4082, 'MG', 'Viçosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4083, 'MG', 'Vieiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4084, 'MG', 'Vila Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4085, 'MG', 'Vila Costina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4086, 'MG', 'Vila Nova de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4087, 'MG', 'Vila Nova dos Poções');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4088, 'MG', 'Vila Pereira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4089, 'MG', 'Vilas Boas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4090, 'MG', 'Virgem da Lapa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4091, 'MG', 'Virgínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4092, 'MG', 'Virginópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4093, 'MG', 'Virgolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4094, 'MG', 'Visconde do Rio Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4095, 'MG', 'Vista Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4096, 'MG', 'Vista Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4097, 'MG', 'Vitorinos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4098, 'MG', 'Volta Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4099, 'MG', 'Wenceslau Braz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4100, 'MG', 'Zelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4101, 'MG', 'Zito Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4102, 'MS', 'Água Boa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4103, 'MS', 'Água Clara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4104, 'MS', 'Albuquerque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4105, 'MS', 'Alcinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4106, 'MS', 'Alto Sucuriú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4107, 'MS', 'Amambaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4108, 'MS', 'Amandina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4109, 'MS', 'Amolar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4110, 'MS', 'Anastácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4111, 'MS', 'Anaurilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4112, 'MS', 'Angélica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4113, 'MS', 'Anhandui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4114, 'MS', 'Antônio João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4115, 'MS', 'Aparecida do Taboado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4116, 'MS', 'Aquidauana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4117, 'MS', 'Aral Moreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4118, 'MS', 'Arapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4119, 'MS', 'Areado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4120, 'MS', 'Árvore Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4121, 'MS', 'Baianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4122, 'MS', 'Bálsamo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4123, 'MS', 'Bandeirantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4124, 'MS', 'Bataguassu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4125, 'MS', 'Batayporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4126, 'MS', 'Baús');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4127, 'MS', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4128, 'MS', 'Bocajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4129, 'MS', 'Bocajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4130, 'MS', 'Bodoquena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4131, 'MS', 'Bom Fim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4132, 'MS', 'Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4133, 'MS', 'Boqueirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4134, 'MS', 'Brasilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4135, 'MS', 'Caarapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4136, 'MS', 'Cabeceira do Apá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4137, 'MS', 'Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4138, 'MS', 'Camapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4139, 'MS', 'Camisão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4140, 'MS', 'Campestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4141, 'MS', 'Campo Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4142, 'MS', 'Capão Seco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4143, 'MS', 'Caracol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4144, 'MS', 'Carumbé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4145, 'MS', 'Cassilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4146, 'MS', 'Chapadão do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4147, 'MS', 'Cipolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4148, 'MS', 'Coimbra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4149, 'MS', 'Congonhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4150, 'MS', 'Corguinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4151, 'MS', 'Coronel Sapucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4152, 'MS', 'Corumbá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4153, 'MS', 'Costa Rica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4154, 'MS', 'Coxim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4155, 'MS', 'Cristalina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4156, 'MS', 'Cruzaltina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4157, 'MS', 'Culturama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4158, 'MS', 'Cupins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4159, 'MS', 'Debrasa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4160, 'MS', 'Deodápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4161, 'MS', 'Dois Irmãos do Buriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4162, 'MS', 'Douradina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4163, 'MS', 'Dourados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4164, 'MS', 'Eldorado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4165, 'MS', 'Fátima do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4166, 'MS', 'Figueirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4167, 'MS', 'Garcias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4168, 'MS', 'Glória de Dourados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4169, 'MS', 'Guaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4170, 'MS', 'Guaçulândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4171, 'MS', 'Guadalupe do Alto Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4172, 'MS', 'Guia Lopes da Laguna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4173, 'MS', 'Iguatemi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4174, 'MS', 'Ilha Comprida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4175, 'MS', 'Ilha Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4176, 'MS', 'Indaiá do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4177, 'MS', 'Indaiá Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4178, 'MS', 'Indápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4179, 'MS', 'Inocência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4180, 'MS', 'Ipezal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4181, 'MS', 'Itahum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4182, 'MS', 'Itaporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4183, 'MS', 'Itaquiraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4184, 'MS', 'Ivinhema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4185, 'MS', 'Jabuti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4186, 'MS', 'Jacareí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4187, 'MS', 'Japorã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4188, 'MS', 'Jaraguari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4189, 'MS', 'Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4190, 'MS', 'Jateí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4191, 'MS', 'Jauru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4192, 'MS', 'Juscelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4193, 'MS', 'Jutí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4194, 'MS', 'Ladário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4195, 'MS', 'Lagoa Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4196, 'MS', 'Laguna Carapã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4197, 'MS', 'Maracaju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4198, 'MS', 'Miranda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4199, 'MS', 'Montese');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4200, 'MS', 'Morangas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4201, 'MS', 'Morraria do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4202, 'MS', 'Morumbi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4203, 'MS', 'Mundo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4204, 'MS', 'Naviraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4205, 'MS', 'Nhecolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4206, 'MS', 'Nioaque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4207, 'MS', 'Nossa Senhora de Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4208, 'MS', 'Nova Alvorada do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4209, 'MS', 'Nova América');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4210, 'MS', 'Nova Andradina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4211, 'MS', 'Nova Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4212, 'MS', 'Nova Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4213, 'MS', 'Nova Jales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4214, 'MS', 'Novo Horizonte do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4215, 'MS', 'Oriente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4216, 'MS', 'Paiaguás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4217, 'MS', 'Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4218, 'MS', 'Panambi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4219, 'MS', 'Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4220, 'MS', 'Paranaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4221, 'MS', 'Paranhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4222, 'MS', 'Pedro Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4223, 'MS', 'Picadinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4224, 'MS', 'Pirapora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4225, 'MS', 'Piraputanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4226, 'MS', 'Ponta Porã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4227, 'MS', 'Ponte Vermelha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4228, 'MS', 'Pontinha do Cocho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4229, 'MS', 'Porto Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4230, 'MS', 'Porto Murtinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4231, 'MS', 'Porto Vilma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4232, 'MS', 'Porto XV de Novembro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4233, 'MS', 'Presidente Castelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4234, 'MS', 'Prudêncio Thomaz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4235, 'MS', 'Quebra Côco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4236, 'MS', 'Quebracho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4237, 'MS', 'Ribas do Rio Pardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4238, 'MS', 'Rio Brilhante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4239, 'MS', 'Rio Negro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4240, 'MS', 'Rio Verde de Mato Grosso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4241, 'MS', 'Rochedinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4242, 'MS', 'Rochedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4243, 'MS', 'Sanga Puitã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4244, 'MS', 'Santa Rita do Pardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4245, 'MS', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4246, 'MS', 'São Gabriel do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4247, 'MS', 'São João do Apore');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4248, 'MS', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4249, 'MS', 'São José do Sucuriú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4250, 'MS', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4251, 'MS', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4252, 'MS', 'São Romão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4253, 'MS', 'Selvíria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4254, 'MS', 'Sete Quedas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4255, 'MS', 'Sidrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4256, 'MS', 'Sonora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4257, 'MS', 'Tacuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4258, 'MS', 'Tamandaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4259, 'MS', 'Taquari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4260, 'MS', 'Taquarussu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4261, 'MS', 'Taunay');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4262, 'MS', 'Terenos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4263, 'MS', 'Três Lagoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4264, 'MS', 'Velhacaria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4265, 'MS', 'Vicentina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4266, 'MS', 'Vila Formosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4267, 'MS', 'Vila Marques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4268, 'MS', 'Vila Rica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4269, 'MS', 'Vila União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4270, 'MS', 'Vila Vargas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4271, 'MS', 'Vista Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4272, 'MT', 'Acorizal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4273, 'MT', 'Água Boa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4274, 'MT', 'Água Fria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4275, 'MT', 'Aguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4276, 'MT', 'Aguapeí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4277, 'MT', 'Águas Claras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4278, 'MT', 'Ainhumas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4279, 'MT', 'Alcantilado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4280, 'MT', 'Alta Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4281, 'MT', 'Alto Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4282, 'MT', 'Alto Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4283, 'MT', 'Alto Coité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4284, 'MT', 'Alto Garças');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4285, 'MT', 'Alto Juruena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4286, 'MT', 'Alto Paraguai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4287, 'MT', 'Alto Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4288, 'MT', 'Alto Taquari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4289, 'MT', 'Analândia do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4290, 'MT', 'Apiacás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4291, 'MT', 'Araguaiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4292, 'MT', 'Araguainha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4293, 'MT', 'Araputanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4294, 'MT', 'Arenápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4295, 'MT', 'Aripuanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4296, 'MT', 'Arruda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4297, 'MT', 'Assari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4298, 'MT', 'Barão de Melgaço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4299, 'MT', 'Barra do Bugres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4300, 'MT', 'Barra do Garças');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4301, 'MT', 'Batovi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4302, 'MT', 'Baús');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4303, 'MT', 'Bauxi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4304, 'MT', 'Bel Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4305, 'MT', 'Bezerro Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4306, 'MT', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4307, 'MT', 'Boa União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4308, 'MT', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4309, 'MT', 'Bom Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4310, 'MT', 'Brasnorte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4311, 'MT', 'Buriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4312, 'MT', 'Buriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4313, 'MT', 'Cáceres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4314, 'MT', 'Caite');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4315, 'MT', 'Campinápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4316, 'MT', 'Campo Novo do Parecis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4317, 'MT', 'Campo Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4318, 'MT', 'Campos de Júlio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4319, 'MT', 'Campos Novos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4320, 'MT', 'Canabrava do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4321, 'MT', 'Canarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4322, 'MT', 'Cangas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4323, 'MT', 'Capão Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4324, 'MT', 'Capão Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4325, 'MT', 'Caramujo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4326, 'MT', 'Caravagio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4327, 'MT', 'Carlinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4328, 'MT', 'Cassununga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4329, 'MT', 'Castanheira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4330, 'MT', 'Catuai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4331, 'MT', 'Chapada dos Guimarães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4332, 'MT', 'Cidade Morena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4333, 'MT', 'Cláudia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4334, 'MT', 'Cocalinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4335, 'MT', 'Colíder');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4336, 'MT', 'Colorado do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4337, 'MT', 'Comodoro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4338, 'MT', 'Confresa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4339, 'MT', 'Coronel Ponce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4340, 'MT', 'Cotrel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4341, 'MT', 'Cotriguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4342, 'MT', 'Coxipó Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4343, 'MT', 'Coxipó da Ponte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4344, 'MT', 'Coxipó do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4345, 'MT', 'Cristinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4346, 'MT', 'Cristo Rei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4347, 'MT', 'Cuiabá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4348, 'MT', 'Curvelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4349, 'MT', 'Del Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4350, 'MT', 'Denise');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4351, 'MT', 'Diamantino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4352, 'MT', 'Dom Aquino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4353, 'MT', 'Engenho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4354, 'MT', 'Engenho Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4355, 'MT', 'Entre Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4356, 'MT', 'Estrela do Leste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4357, 'MT', 'Faval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4358, 'MT', 'Fazenda de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4359, 'MT', 'Feliz Natal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4360, 'MT', 'Figueirópolis D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4361, 'MT', 'Filadélfia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4362, 'MT', 'Flor da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4363, 'MT', 'Fontanilhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4364, 'MT', 'Gaúcha do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4365, 'MT', 'General Carneiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4366, 'MT', 'Glória D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4367, 'MT', 'Guarantã do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4368, 'MT', 'Guarita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4369, 'MT', 'Guiratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4370, 'MT', 'Horizonte do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4371, 'MT', 'Indianápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4372, 'MT', 'Indiavaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4373, 'MT', 'Irenópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4374, 'MT', 'Itamarati Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4375, 'MT', 'Itaúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4376, 'MT', 'Itiquira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4377, 'MT', 'Jaciara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4378, 'MT', 'Jangada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4379, 'MT', 'Jarudore');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4380, 'MT', 'Jatobá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4381, 'MT', 'Jauru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4382, 'MT', 'Joselândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4383, 'MT', 'Juara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4384, 'MT', 'Juína');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4385, 'MT', 'Juruena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4386, 'MT', 'Juscimeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4387, 'MT', 'Lambari D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4388, 'MT', 'Lavouras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4389, 'MT', 'Lucas do Rio Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4390, 'MT', 'Lucialva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4391, 'MT', 'Luciara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4392, 'MT', 'Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4393, 'MT', 'Marcelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4394, 'MT', 'Marzagão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4395, 'MT', 'Mata Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4396, 'MT', 'Matupá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4397, 'MT', 'Mimoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4398, 'MT', 'Mirassol D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4399, 'MT', 'Nobres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4400, 'MT', 'Nonoai do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4401, 'MT', 'Nortelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4402, 'MT', 'Nossa Senhora da Guia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4403, 'MT', 'Nossa Senhora do Livramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4404, 'MT', 'Nova Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4405, 'MT', 'Nova Bandeirantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4406, 'MT', 'Nova Brasilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4407, 'MT', 'Nova Brasília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4408, 'MT', 'Nova Canaã do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4409, 'MT', 'Nova Catanduva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4410, 'MT', 'Nova Galileia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4411, 'MT', 'Nova Guarita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4412, 'MT', 'Nova Lacerda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4413, 'MT', 'Nova Marilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4414, 'MT', 'Nova Maringá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4415, 'MT', 'Nova Monte Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4416, 'MT', 'Nova Mutum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4417, 'MT', 'Nova Olímpia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4418, 'MT', 'Nova Ubiratã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4419, 'MT', 'Nova Xavantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4420, 'MT', 'Novo Diamantino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4421, 'MT', 'Novo Eldorado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4422, 'MT', 'Novo Horizonte do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4423, 'MT', 'Novo Mundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4424, 'MT', 'Novo Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4425, 'MT', 'Novo São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4426, 'MT', 'Padronal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4427, 'MT', 'Pai André');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4428, 'MT', 'Paraíso do Leste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4429, 'MT', 'Paranaitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4430, 'MT', 'Paranatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4431, 'MT', 'Passagem da Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4432, 'MT', 'Pedra Preta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4433, 'MT', 'Peixoto de Azevedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4434, 'MT', 'Pirizal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4435, 'MT', 'Placa de Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4436, 'MT', 'Planalto da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4437, 'MT', 'Poconé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4438, 'MT', 'Pombas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4439, 'MT', 'Pontal do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4440, 'MT', 'Ponte Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4441, 'MT', 'Ponte de Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4442, 'MT', 'Pontes e Lacerda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4443, 'MT', 'Pontinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4444, 'MT', 'Porto Alegre do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4445, 'MT', 'Porto dos Gaúchos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4446, 'MT', 'Porto Esperidião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4447, 'MT', 'Porto Estrela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4448, 'MT', 'Poxoréu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4449, 'MT', 'Praia Rica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4450, 'MT', 'Primavera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4451, 'MT', 'Primavera do Leste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4452, 'MT', 'Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4453, 'MT', 'Querência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4454, 'MT', 'Rancharia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4455, 'MT', 'Reserva do Cabaçal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4456, 'MT', 'Ribeirão Cascalheira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4457, 'MT', 'Ribeirão dos Cocais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4458, 'MT', 'Ribeirãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4459, 'MT', 'Rio Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4460, 'MT', 'Rio Manso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4461, 'MT', 'Riolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4462, 'MT', 'Rondonópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4463, 'MT', 'Rosário Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4464, 'MT', 'Salto do Céu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4465, 'MT', 'Sangradouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4466, 'MT', 'Santa Carmem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4467, 'MT', 'Santa Elvira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4468, 'MT', 'Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4469, 'MT', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4470, 'MT', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4471, 'MT', 'Santaninha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4472, 'MT', 'Santo Afonso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4473, 'MT', 'Santo Antônio do Leverger');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4474, 'MT', 'Santo Antônio do Rio Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4475, 'MT', 'São Cristovão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4476, 'MT', 'Vale de São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4477, 'MT', 'São Félix do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4478, 'MT', 'São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4479, 'MT', 'São Jorge');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4480, 'MT', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4481, 'MT', 'São José do Apuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4482, 'MT', 'São José do Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4483, 'MT', 'São José do Povo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4484, 'MT', 'São José do Rio Claro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4485, 'MT', 'São José do Xingu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4486, 'MT', 'São José dos Quatro Marcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4487, 'MT', 'São Lourenço de Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4488, 'MT', 'São Pedro da Cipa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4489, 'MT', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4490, 'MT', 'Sapezal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4491, 'MT', 'Selma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4492, 'MT', 'Serra Nova Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4493, 'MT', 'Sinop');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4494, 'MT', 'Sonho Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4495, 'MT', 'Sorriso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4496, 'MT', 'Sumidouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4497, 'MT', 'Tabaporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4498, 'MT', 'Tangará da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4499, 'MT', 'Tapirapua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4500, 'MT', 'Tapurah');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4501, 'MT', 'Terra Nova do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4502, 'MT', 'Terra Roxa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4503, 'MT', 'Tesouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4504, 'MT', 'Toricueyje');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4505, 'MT', 'Torixoréu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4506, 'MT', 'Três Pontes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4507, 'MT', 'União do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4508, 'MT', 'Vale dos Sonhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4509, 'MT', 'Vale Rico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4510, 'MT', 'Varginha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4511, 'MT', 'Várzea Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4512, 'MT', 'Vera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4513, 'MT', 'Vila Atlântica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4514, 'MT', 'Vila Bela da Santíssima Trindade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4515, 'MT', 'Vila Bueno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4516, 'MT', 'Vila Mutum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4517, 'MT', 'Vila Operária');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 4500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4518, 'MT', 'Vila Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4519, 'MT', 'Vila Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4520, 'MT', 'Vila Rica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4521, 'MT', 'Novo Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4522, 'PA', 'Abaetetuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4523, 'PA', 'Abel Figueiredo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4524, 'PA', 'Acará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4525, 'PA', 'Afuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4526, 'PA', 'Agrópolis Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4527, 'PA', 'Água Azul do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4528, 'PA', 'Água Fria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4529, 'PA', 'Alenquer');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4530, 'PA', 'Algodoal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4531, 'PA', 'Almeirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4532, 'PA', 'Almoço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4533, 'PA', 'Alta Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4534, 'PA', 'Altamira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4535, 'PA', 'Alter do Chão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4536, 'PA', 'Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4537, 'PA', 'Americano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4538, 'PA', 'Anajás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4539, 'PA', 'Ananindeua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4540, 'PA', 'Anapu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4541, 'PA', 'Antônio Lemos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4542, 'PA', 'Apeú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4544, 'PA', 'Apinagés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4545, 'PA', 'Arapixuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4546, 'PA', 'Araquaim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4547, 'PA', 'Arco-Íris');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4548, 'PA', 'Areias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4549, 'PA', 'Arumanduba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4550, 'PA', 'Aruri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4551, 'PA', 'Aturiaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4552, 'PA', 'Augusto Corrêa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4553, 'PA', 'Aurora do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4554, 'PA', 'Aveiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4555, 'PA', 'Bagre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4556, 'PA', 'Baião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4557, 'PA', 'Bannach');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4558, 'PA', 'Barcarena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4559, 'PA', 'Barreira Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4560, 'PA', 'Barreira dos Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4561, 'PA', 'Barreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4562, 'PA', 'Baturité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4563, 'PA', 'Beja');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4564, 'PA', 'Bela Vista do Caracol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4565, 'PA', 'Belém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4566, 'PA', 'Belterra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4567, 'PA', 'Benevides');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4568, 'PA', 'Benfica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4569, 'PA', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4570, 'PA', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4571, 'PA', 'Boa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4572, 'PA', 'Boa Sorte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4573, 'PA', 'Boa Vista do Iririteua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4574, 'PA', 'Boim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4575, 'PA', 'Bom Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4576, 'PA', 'Bom Jesus do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4577, 'PA', 'Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4578, 'PA', 'Bragança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4579, 'PA', 'Brasil Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4580, 'PA', 'Brasília Legal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4581, 'PA', 'Brejo do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4582, 'PA', 'Brejo Grande do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4583, 'PA', 'Breu Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4584, 'PA', 'Breves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4585, 'PA', 'Bujaru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4586, 'PA', 'Cachoeira do Piriá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4587, 'PA', 'Cachoeira do Arari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4588, 'PA', 'Cafezal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4589, 'PA', 'Cairari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4590, 'PA', 'Caju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4591, 'PA', 'Câmara do Marajó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4592, 'PA', 'Cambuquira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4593, 'PA', 'Cametá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4594, 'PA', 'Camiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4595, 'PA', 'Canaã dos Carajás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4596, 'PA', 'Capanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4597, 'PA', 'Capitão Poço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4598, 'PA', 'Caracará do Arari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4599, 'PA', 'Carajás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4600, 'PA', 'Carapajó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4601, 'PA', 'Caraparu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4602, 'PA', 'Caratateua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4603, 'PA', 'Caripi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4604, 'PA', 'Carrazedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4605, 'PA', 'Castanhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4606, 'PA', 'Castelo dos Sonhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4607, 'PA', 'Chaves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4608, 'PA', 'Colares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4609, 'PA', 'Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4610, 'PA', 'Conceição do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4611, 'PA', 'Concórdia do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4612, 'PA', 'Condeixa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4613, 'PA', 'Coqueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4614, 'PA', 'Cripurizão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4615, 'PA', 'Cripurizinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4616, 'PA', 'Cuiú-Cuiú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4617, 'PA', 'Cumaru do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4618, 'PA', 'Curionópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4619, 'PA', 'Curralinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4620, 'PA', 'Curuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4621, 'PA', 'Curuaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4622, 'PA', 'Curuçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4623, 'PA', 'Curuçambaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4624, 'PA', 'Curumu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4625, 'PA', 'Dom Eliseu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4626, 'PA', 'Eldorado dos Carajás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4627, 'PA', 'Emboraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4628, 'PA', 'Espírito Santo do Tauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4629, 'PA', 'Faro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4630, 'PA', 'Fernandes Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4631, 'PA', 'Flexal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4632, 'PA', 'Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4633, 'PA', 'Floresta do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4634, 'PA', 'Garrafão do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4635, 'PA', 'Goianésia do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4636, 'PA', 'Gradaus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4637, 'PA', 'Guajará-Açú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4638, 'PA', 'Guajará-Miri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4639, 'PA', 'Gurupá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4640, 'PA', 'Gurupizinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4641, 'PA', 'Hidrelétrica Tucuruí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4642, 'PA', 'Iataí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4643, 'PA', 'Icoaraci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4644, 'PA', 'Igarapé da Lama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4645, 'PA', 'Igarapé-Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4646, 'PA', 'Igarapé-Miri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4647, 'PA', 'Inanu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4648, 'PA', 'Inhangapi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4649, 'PA', 'Ipixuna do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4650, 'PA', 'Irituia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4651, 'PA', 'Itaituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4652, 'PA', 'Itapixuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4653, 'PA', 'Itatupã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4654, 'PA', 'Itupiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4655, 'PA', 'Jacareacanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4656, 'PA', 'Jacundá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4657, 'PA', 'Jaguarari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4658, 'PA', 'Jamanxinzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4659, 'PA', 'Jambuaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4660, 'PA', 'Jandiaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4661, 'PA', 'Japerica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4662, 'PA', 'Joana Coeli');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4663, 'PA', 'Joana Peres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4664, 'PA', 'Joanes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4665, 'PA', 'Juabá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4666, 'PA', 'Jubim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4667, 'PA', 'Juruti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4668, 'PA', 'Km 19');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4669, 'PA', 'Km 26');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4670, 'PA', 'Lauro Sodré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4671, 'PA', 'Ligação do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4672, 'PA', 'Limoeiro do Ajuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4673, 'PA', 'Mãe do Rio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4674, 'PA', 'Magalhães Barata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4675, 'PA', 'Maiauata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4676, 'PA', 'Manjeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4677, 'PA', 'Marabá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4678, 'PA', 'Maracanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4679, 'PA', 'Marajoara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4680, 'PA', 'Marapanim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4681, 'PA', 'Marituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4682, 'PA', 'Marudá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4683, 'PA', 'Mata Geral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4684, 'PA', 'Matapiquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4685, 'PA', 'Medicilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4686, 'PA', 'Melgaço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4687, 'PA', 'Menino Deus do Anapu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4688, 'PA', 'Meruú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4689, 'PA', 'Mirasselvas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4690, 'PA', 'Miritituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4691, 'PA', 'Mocajuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4692, 'PA', 'Moiraba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4693, 'PA', 'Moju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4694, 'PA', 'Monsaras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4695, 'PA', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4696, 'PA', 'Monte Alegre do Mau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4697, 'PA', 'Monte Dourado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4698, 'PA', 'Morada Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4699, 'PA', 'Mosqueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4700, 'PA', 'Muaná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4701, 'PA', 'Mujuí dos Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4702, 'PA', 'Murajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4703, 'PA', 'Murucupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4704, 'PA', 'Murumuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4705, 'PA', 'Muta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4706, 'PA', 'Mutucal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4707, 'PA', 'Nazaré de Mocajuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4708, 'PA', 'Nazaré dos Patos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4709, 'PA', 'Nova Esperança do Piriá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4710, 'PA', 'Nova Ipixuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4711, 'PA', 'Nova Mocajuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4712, 'PA', 'Nova Timboteua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4713, 'PA', 'Novo Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4714, 'PA', 'Novo Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4715, 'PA', 'Novo Repartimento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4716, 'PA', 'Núcleo Urbano Quilômetro 30');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4717, 'PA', 'Óbidos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4718, 'PA', 'Oeiras do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4719, 'PA', 'Oriximiná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4720, 'PA', 'Osvaldilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4722, 'PA', 'Ourém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4723, 'PA', 'Ourilândia do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4724, 'PA', 'Outeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4725, 'PA', 'Pacajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4726, 'PA', 'Pacoval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4727, 'PA', 'Palestina do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4728, 'PA', 'Paragominas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4729, 'PA', 'Paratins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4730, 'PA', 'Parauapebas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4731, 'PA', 'Pau D Arco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4732, 'PA', 'Pedreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4733, 'PA', 'Peixe-Boi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4734, 'PA', 'Penhalonga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4735, 'PA', 'Perseverança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4736, 'PA', 'Pesqueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4737, 'PA', 'Piabas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4738, 'PA', 'Piçarra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4739, 'PA', 'Piçarra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4740, 'PA', 'Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4741, 'PA', 'Piraquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4742, 'PA', 'Piriá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4743, 'PA', 'Placas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4744, 'PA', 'Ponta de Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4745, 'PA', 'Ponta de Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4746, 'PA', 'Portel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4747, 'PA', 'Porto de Moz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4748, 'PA', 'Porto Salvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4749, 'PA', 'Porto Trombetas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4750, 'PA', 'Prainha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4751, 'PA', 'Primavera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4752, 'PA', 'Quatipuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4753, 'PA', 'Quatro Bocas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4754, 'PA', 'Redenção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4755, 'PA', 'Remansão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4756, 'PA', 'Repartimento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4757, 'PA', 'Rio Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4758, 'PA', 'Rio Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4759, 'PA', 'Riozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4760, 'PA', 'Rondon do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4761, 'PA', 'Rurópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4762, 'PA', 'Salinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4763, 'PA', 'Salvaterra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4764, 'PA', 'Santa Bárbara do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4765, 'PA', 'Santa Cruz do Arari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4766, 'PA', 'Santa Isabel do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4767, 'PA', 'Santa Luzia do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4768, 'PA', 'Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4769, 'PA', 'Santa Maria das Barreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4770, 'PA', 'Santa Maria do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4771, 'PA', 'Santa Rosa da Vigia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4772, 'PA', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4773, 'PA', 'Santana do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4774, 'PA', 'Santarém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4775, 'PA', 'Santarém Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4776, 'PA', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4777, 'PA', 'Santo Antônio do Tauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4778, 'PA', 'São Caetano de Odivelas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4779, 'PA', 'São Domingos do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4780, 'PA', 'São Domingos do Capim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4781, 'PA', 'São Félix do Xingu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4782, 'PA', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4783, 'PA', 'São Francisco da Jararaca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4784, 'PA', 'São Francisco do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4785, 'PA', 'São Geraldo do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4786, 'PA', 'São João da Ponta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4787, 'PA', 'São João de Pirabas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4788, 'PA', 'São João do Acangata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4789, 'PA', 'São João do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4790, 'PA', 'São João do Piriá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4791, 'PA', 'São João dos Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4792, 'PA', 'São Joaquim do Tapará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4793, 'PA', 'São Jorge');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4794, 'PA', 'São José do Gurupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4795, 'PA', 'São José do Piriá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4796, 'PA', 'São Luiz do Tapajós');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4797, 'PA', 'São Miguel do Guamá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4798, 'PA', 'São Miguel dos Macacos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4799, 'PA', 'São Pedro de Viseu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4800, 'PA', 'São Pedro do Capim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4801, 'PA', 'São Raimundo de Borralhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4802, 'PA', 'São Raimundo do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4803, 'PA', 'São Raimundo dos Furtados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4804, 'PA', 'São Roberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4805, 'PA', 'São Sebastião da Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4806, 'PA', 'São Sebastião de Viçosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4807, 'PA', 'Sapucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4808, 'PA', 'Senador José Porfírio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4809, 'PA', 'Serra Pelada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4810, 'PA', 'Soure');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4811, 'PA', 'Tailândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4812, 'PA', 'Tatuteua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4813, 'PA', 'Tauari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4814, 'PA', 'Tauarizinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4815, 'PA', 'Tentugal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4816, 'PA', 'Terra Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4817, 'PA', 'Terra Santa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4818, 'PA', 'Tijoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4819, 'PA', 'Timboteua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4820, 'PA', 'Tomé-Açú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4821, 'PA', 'Tracuateua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4822, 'PA', 'Trairão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4823, 'PA', 'Tucumã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4824, 'PA', 'Tucuruí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4825, 'PA', 'Ulianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4826, 'PA', 'Uruará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4827, 'PA', 'Urucuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4828, 'PA', 'Urucuriteua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4829, 'PA', 'Val-de-Cães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4830, 'PA', 'Veiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4831, 'PA', 'Vigia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4832, 'PA', 'Vila do Carmo do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4833, 'PA', 'Vila dos Cabanos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4834, 'PA', 'Vila França');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4835, 'PA', 'Vila Goreth');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4836, 'PA', 'Vila Isol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4837, 'PA', 'Vila Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4838, 'PA', 'Vila Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4839, 'PA', 'Vila Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4840, 'PA', 'Vila Socorro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4841, 'PA', 'Vilarinho do Monte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4842, 'PA', 'Viseu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4843, 'PA', 'Vista Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4844, 'PA', 'Vista Alegre do Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4845, 'PA', 'Vitória do Xingu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4846, 'PA', 'Xinguara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4847, 'PA', 'Xinguarinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4848, 'PB', 'Água Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4849, 'PB', 'Aguiar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4850, 'PB', 'Alagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4851, 'PB', 'Alagoa Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4852, 'PB', 'Alagoinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4853, 'PB', 'Alcantil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4854, 'PB', 'Algodão de Jandaíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4855, 'PB', 'Alhandra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4856, 'PB', 'Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4857, 'PB', 'Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4858, 'PB', 'Araçagi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4859, 'PB', 'Arara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4860, 'PB', 'Araruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4861, 'PB', 'Areia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4862, 'PB', 'Areia de Baraúnas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4863, 'PB', 'Areial');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4864, 'PB', 'Areias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4865, 'PB', 'Aroeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4866, 'PB', 'Riachão do Bacamarte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4867, 'PB', 'Assunção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4868, 'PB', 'Baía da Traição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4869, 'PB', 'Balanços');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4870, 'PB', 'Bananeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4871, 'PB', 'Baraúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4872, 'PB', 'Barra de Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4873, 'PB', 'Barra de Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4874, 'PB', 'Barra de São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4875, 'PB', 'Barra do Camaratuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4876, 'PB', 'Bayeux');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4877, 'PB', 'Belém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4878, 'PB', 'Belém do Brejo do Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4879, 'PB', 'Bernardino Batista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4880, 'PB', 'Boa Ventura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4881, 'PB', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4882, 'PB', 'Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4883, 'PB', 'Bom Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4884, 'PB', 'Bom Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4885, 'PB', 'Bonito de Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4886, 'PB', 'Boqueirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4887, 'PB', 'Borborema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4888, 'PB', 'Brejo do Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4889, 'PB', 'Brejo dos Santos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4890, 'PB', 'Caaporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4891, 'PB', 'Cabaceiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4892, 'PB', 'Cabedelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4893, 'PB', 'Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4894, 'PB', 'Cachoeira dos Índios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4895, 'PB', 'Cachoeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4896, 'PB', 'Cacimba de Areia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4897, 'PB', 'Cacimba de Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4898, 'PB', 'Cacimbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4899, 'PB', 'Caiçara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4900, 'PB', 'Cajazeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4901, 'PB', 'Cajazeirinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4902, 'PB', 'Caldas Brandão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4903, 'PB', 'Camalaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4904, 'PB', 'Campina Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4905, 'PB', 'Campo Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4906, 'PB', 'Campo Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4907, 'PB', 'Camurupim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4908, 'PB', 'Capim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4909, 'PB', 'Caraúbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4910, 'PB', 'Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4911, 'PB', 'Carrapateira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4912, 'PB', 'Casinha do Homem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4913, 'PB', 'Casserengue');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4914, 'PB', 'Catingueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4915, 'PB', 'Catolé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4916, 'PB', 'Catolé do Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4917, 'PB', 'Caturité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4918, 'PB', 'Cepilho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4919, 'PB', 'Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4920, 'PB', 'Condado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4921, 'PB', 'Conde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4922, 'PB', 'Congo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4923, 'PB', 'Coremas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4924, 'PB', 'Coronel Maia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4925, 'PB', 'Coxixola');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4926, 'PB', 'Cruz do Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4927, 'PB', 'Cubati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4928, 'PB', 'Cuité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4929, 'PB', 'Cuité de Mamanguape');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4930, 'PB', 'Cuitegi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4931, 'PB', 'Cupissura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4932, 'PB', 'Curral de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4933, 'PB', 'Curral Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4934, 'PB', 'Damião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4935, 'PB', 'Desterro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4936, 'PB', 'Diamante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4937, 'PB', 'Dona Inês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4938, 'PB', 'Duas Estradas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4939, 'PB', 'Emas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4940, 'PB', 'Engenheiro Ávidos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4941, 'PB', 'Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4942, 'PB', 'Fagundes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4943, 'PB', 'Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4944, 'PB', 'Fazenda Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4945, 'PB', 'Forte Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4946, 'PB', 'Frei Martinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4947, 'PB', 'Gado Bravo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4948, 'PB', 'Galante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4949, 'PB', 'Guarabira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4950, 'PB', 'Guarita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4951, 'PB', 'Gurinhém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4952, 'PB', 'Gurjão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4953, 'PB', 'Ibiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4954, 'PB', 'Igaracy');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4955, 'PB', 'Imaculada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4956, 'PB', 'Ingá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4957, 'PB', 'Itabaiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4958, 'PB', 'Itajubatiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4959, 'PB', 'Itaporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4960, 'PB', 'Itapororoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4961, 'PB', 'Itatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4962, 'PB', 'Jacaraú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4963, 'PB', 'Jericó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4964, 'PB', 'João Pessoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4965, 'PB', 'Juarez Távora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4966, 'PB', 'Juazeirinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4967, 'PB', 'Junco do Seridó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4968, 'PB', 'Juripiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4969, 'PB', 'Juru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4970, 'PB', 'Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4971, 'PB', 'Lagoa de Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4972, 'PB', 'Lagoa de Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4973, 'PB', 'Lagoa Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4974, 'PB', 'Lastro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4975, 'PB', 'Lerolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4976, 'PB', 'Livramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4977, 'PB', 'Logradouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4978, 'PB', 'Lucena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4979, 'PB', 'Mãe D Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4980, 'PB', 'Maia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4981, 'PB', 'Malta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4982, 'PB', 'Mamanguape');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4983, 'PB', 'Manaíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4984, 'PB', 'Marcação');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4985, 'PB', 'Mari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4986, 'PB', 'Marizópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4987, 'PB', 'Massaranduba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4988, 'PB', 'Mata Limpa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4989, 'PB', 'Mata Virgem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4990, 'PB', 'Mataraca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4991, 'PB', 'Matinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4992, 'PB', 'Mato Grosso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4993, 'PB', 'Maturéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4994, 'PB', 'Melo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4995, 'PB', 'Mogeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4996, 'PB', 'Montadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4997, 'PB', 'Monte Horebe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4998, 'PB', 'Monteiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (4999, 'PB', 'Montevidéu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5000, 'PB', 'Mulungu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5001, 'PB', 'Muquém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5002, 'PB', 'Natuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5003, 'PB', 'Nazaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5004, 'PB', 'Nazarezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5005, 'PB', 'Nossa Senhora do Livramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5006, 'PB', 'Nova Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5007, 'PB', 'Nova Olinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5008, 'PB', 'Nova Palmeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5009, 'PB', 'Núcleo N 2');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5010, 'PB', 'Núcleo N 3');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5011, 'PB', 'Odilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5012, 'PB', 'Olho D Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5013, 'PB', 'Olivedos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5014, 'PB', 'Ouro Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5015, 'PB', 'Parari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5016, 'PB', 'Passagem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5017, 'PB', 'Patos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5018, 'PB', 'Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5019, 'PB', 'Pedra Branca');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 5000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5020, 'PB', 'Pedra Lavrada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5021, 'PB', 'Pedras de Fogo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5022, 'PB', 'Pedro Régis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5023, 'PB', 'Pelo Sinal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5024, 'PB', 'Pereiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5025, 'PB', 'Piancó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5026, 'PB', 'Picuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5027, 'PB', 'Pilar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5028, 'PB', 'Pilões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5029, 'PB', 'Pilõezinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5030, 'PB', 'Pindurão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5031, 'PB', 'Pio X');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5032, 'PB', 'Pirauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5033, 'PB', 'Pirpirituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5034, 'PB', 'Pitanga de Estrada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5035, 'PB', 'Pitimbu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5036, 'PB', 'Pocinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5037, 'PB', 'Poço Dantas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5038, 'PB', 'Poço de José de Moura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5039, 'PB', 'Pombal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5040, 'PB', 'Porteirinha de Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5041, 'PB', 'Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5042, 'PB', 'Princesa Isabel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5043, 'PB', 'Puxinanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5044, 'PB', 'Queimadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5045, 'PB', 'Quixabá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5046, 'PB', 'Quixadá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5047, 'PB', 'Remígio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5048, 'PB', 'Riachão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5049, 'PB', 'Riachão do Poço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5050, 'PB', 'Riacho de Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5051, 'PB', 'Riacho dos Cavalos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5052, 'PB', 'Ribeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5053, 'PB', 'Rio Tinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5054, 'PB', 'Rua Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5055, 'PB', 'Salema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5056, 'PB', 'Salgadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5057, 'PB', 'Salgado de São Félix');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5058, 'PB', 'Santa Cecília de Umbuzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5059, 'PB', 'Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5060, 'PB', 'Santa Gertrudes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5061, 'PB', 'Santa Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5062, 'PB', 'Santa Inês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5063, 'PB', 'Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5064, 'PB', 'Santa Luzia do Cariri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5065, 'PB', 'Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5066, 'PB', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5067, 'PB', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5068, 'PB', 'Santa Teresinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5069, 'PB', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5070, 'PB', 'Santana de Mangueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5071, 'PB', 'Santana dos Garrotes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5072, 'PB', 'Santarém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5073, 'PB', 'Santo André');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5074, 'PB', 'São Bento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5075, 'PB', 'São Bentinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5076, 'PB', 'São Domingos de Pombal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5077, 'PB', 'São Domingos do Cariri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5078, 'PB', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5079, 'PB', 'São Gonçalo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5080, 'PB', 'São João Bosco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5081, 'PB', 'São João do Cariri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5082, 'PB', 'São João do Rio do Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5083, 'PB', 'São João do Tigre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5084, 'PB', 'São José da Lagoa Tapada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5085, 'PB', 'São José da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5086, 'PB', 'São José de Caiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5087, 'PB', 'São José de Espinharas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5088, 'PB', 'São José de Marimbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5089, 'PB', 'São José de Piranhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5090, 'PB', 'São José de Princesa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5091, 'PB', 'São José do Bonfim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5092, 'PB', 'São José do Brejo do Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5093, 'PB', 'São José do Sabugi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5094, 'PB', 'São José dos Cordeiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5095, 'PB', 'São José dos Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5096, 'PB', 'São Mamede');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5097, 'PB', 'São Miguel de Taipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5098, 'PB', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5099, 'PB', 'São Sebastião de Lagoa de Roça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5100, 'PB', 'São Sebastião do Umbuzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5101, 'PB', 'Sapé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5102, 'PB', 'Seridó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5103, 'PB', 'São Vicente do Seridó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5104, 'PB', 'Serra Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5105, 'PB', 'Serra da Raiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5106, 'PB', 'Serra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5107, 'PB', 'Serra Redonda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5108, 'PB', 'Serraria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5109, 'PB', 'Sertãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5110, 'PB', 'Sobrado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5111, 'PB', 'Solânea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5112, 'PB', 'Soledade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5113, 'PB', 'Sossego');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5114, 'PB', 'Sousa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5115, 'PB', 'Sucuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5116, 'PB', 'Sumé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5117, 'PB', 'Campo de Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5119, 'PB', 'Tambauzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5120, 'PB', 'Taperoá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5121, 'PB', 'Tavares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5122, 'PB', 'Teixeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5123, 'PB', 'Tenório');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5124, 'PB', 'Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5125, 'PB', 'Uiraúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5126, 'PB', 'Umari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5127, 'PB', 'Umbuzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5128, 'PB', 'Várzea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5129, 'PB', 'Várzea Comprida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5130, 'PB', 'Várzea Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5131, 'PB', 'Vazante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5132, 'PB', 'Vieirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5133, 'PB', 'Vista Serrana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5134, 'PB', 'Zabelê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5135, 'PE', 'Abreu e Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5136, 'PE', 'Afogados da Ingazeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5137, 'PE', 'Afrânio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5138, 'PE', 'Agrestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5139, 'PE', 'Água Fria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5140, 'PE', 'Água Preta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5141, 'PE', 'Águas Belas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5142, 'PE', 'Airi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5143, 'PE', 'Alagoinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5144, 'PE', 'Albuquerque Né');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5145, 'PE', 'Algodões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5146, 'PE', 'Aliança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5147, 'PE', 'Altinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5148, 'PE', 'Amaraji');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5149, 'PE', 'Ameixas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5150, 'PE', 'Angelim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5151, 'PE', 'Apoti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5152, 'PE', 'Araçoiaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5153, 'PE', 'Araripina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5154, 'PE', 'Arcoverde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5155, 'PE', 'Aripibu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5156, 'PE', 'Arizona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5157, 'PE', 'Barra de Farias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5158, 'PE', 'Barra de Guabiraba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5159, 'PE', 'Barra de São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5160, 'PE', 'Barra do Brejo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5161, 'PE', 'Barra do Chata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5162, 'PE', 'Barra do Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5163, 'PE', 'Barra do Riachão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5164, 'PE', 'Barra do Sirinhaém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5165, 'PE', 'Barreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5166, 'PE', 'Batateira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5167, 'PE', 'Belém de Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5168, 'PE', 'Belém do São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5169, 'PE', 'Belo Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5170, 'PE', 'Bengalas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5171, 'PE', 'Bentivi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5172, 'PE', 'Bernardo Vieira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5173, 'PE', 'Betânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5174, 'PE', 'Bezerros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5175, 'PE', 'Bizarra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5176, 'PE', 'Boas Novas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5177, 'PE', 'Bodocó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5178, 'PE', 'Bom Conselho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5179, 'PE', 'Bom Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5180, 'PE', 'Bom Nome');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5181, 'PE', 'Bonfim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5182, 'PE', 'Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5183, 'PE', 'Brejão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5184, 'PE', 'Brejinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5185, 'PE', 'Brejo da Madre de Deus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5186, 'PE', 'Buenos Aires');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5187, 'PE', 'Buíque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5188, 'PE', 'Cabanas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5189, 'PE', 'Cabo de Santo Agostinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5190, 'PE', 'Cabrobó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5191, 'PE', 'Cachoeira do Roberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5192, 'PE', 'Cachoeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5193, 'PE', 'Caetés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5194, 'PE', 'Caiçarinha da Penha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5195, 'PE', 'Calçado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5196, 'PE', 'Caldeirões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5197, 'PE', 'Calumbi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5198, 'PE', 'Camaragibe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5199, 'PE', 'Camela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5200, 'PE', 'Camocim de São Félix');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5201, 'PE', 'Camutanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5202, 'PE', 'Canaã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5203, 'PE', 'Canhotinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5204, 'PE', 'Capoeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5205, 'PE', 'Caraíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5206, 'PE', 'Caraibeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5207, 'PE', 'Carapotos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5208, 'PE', 'Carice');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5209, 'PE', 'Carima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5210, 'PE', 'Caririmirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5211, 'PE', 'Carnaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5212, 'PE', 'Carnaubeira da Penha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5213, 'PE', 'Carneiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5214, 'PE', 'Carpina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5215, 'PE', 'Carqueja');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5216, 'PE', 'Caruaru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5217, 'PE', 'Casinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5218, 'PE', 'Catende');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5219, 'PE', 'Catimbaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5220, 'PE', 'Catolé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5222, 'PE', 'Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5223, 'PE', 'Chã de Alegria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5224, 'PE', 'Chã do Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5225, 'PE', 'Chã Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5226, 'PE', 'Cimbres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5227, 'PE', 'Clarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5228, 'PE', 'Cocau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5229, 'PE', 'Cocau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5230, 'PE', 'Conceição das Crioulas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5231, 'PE', 'Condado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5232, 'PE', 'Correntes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5233, 'PE', 'Cortês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5234, 'PE', 'Couro D Antas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5235, 'PE', 'Cristália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5236, 'PE', 'Cruanji');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5237, 'PE', 'Cruzes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5238, 'PE', 'Cuiambuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5239, 'PE', 'Cumaru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5240, 'PE', 'Cupira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5241, 'PE', 'Curral Queimado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5242, 'PE', 'Custódia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5243, 'PE', 'Dois Leões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5244, 'PE', 'Dormentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5245, 'PE', 'Entroncamento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5246, 'PE', 'Escada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5247, 'PE', 'Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5248, 'PE', 'Exu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5249, 'PE', 'Fazenda Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5250, 'PE', 'Feira Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5251, 'PE', 'Feitoria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5252, 'PE', 'Fernando de Noronha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5253, 'PE', 'Ferreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5254, 'PE', 'Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5255, 'PE', 'Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5256, 'PE', 'Frei Miguelinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5257, 'PE', 'Frexeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5258, 'PE', 'Gameleira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5259, 'PE', 'Garanhuns');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5260, 'PE', 'Glória do Goitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5261, 'PE', 'Goiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5262, 'PE', 'Gonçalves Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5263, 'PE', 'Granito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5264, 'PE', 'Gravatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5265, 'PE', 'Gravatá do Ibiapina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5266, 'PE', 'Grotão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5267, 'PE', 'Guanumbi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5268, 'PE', 'Henrique Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5269, 'PE', 'Iateca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5270, 'PE', 'Iati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5271, 'PE', 'Ibimirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5272, 'PE', 'Ibirajuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5273, 'PE', 'Ibiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5274, 'PE', 'Ibiratinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5275, 'PE', 'Ibitiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5276, 'PE', 'Ibó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5277, 'PE', 'Icaiçara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5278, 'PE', 'Igapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5279, 'PE', 'Igarapeassu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5280, 'PE', 'Igarapeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5281, 'PE', 'Igarassu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5282, 'PE', 'Iguaraci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5283, 'PE', 'Inajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5284, 'PE', 'Ingazeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5285, 'PE', 'Ipojuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5286, 'PE', 'Ipubi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5287, 'PE', 'Ipuera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5288, 'PE', 'Iraguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5289, 'PE', 'Irajaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5290, 'PE', 'Iratama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5291, 'PE', 'Itacuruba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5292, 'PE', 'Itaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5293, 'PE', 'Ilha de Itamaracá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5294, 'PE', 'Itambé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5295, 'PE', 'Itapetim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5296, 'PE', 'Itapissuma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5297, 'PE', 'Itaquitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5298, 'PE', 'Ituguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5299, 'PE', 'Iuiteporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5300, 'PE', 'Jabitaca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5302, 'PE', 'Jaboatão dos Guararapes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5303, 'PE', 'Japecanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5304, 'PE', 'Jaqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5305, 'PE', 'Jataúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5306, 'PE', 'Jatiúca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5307, 'PE', 'Jatobá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5308, 'PE', 'Jenipapo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5309, 'PE', 'João Alfredo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5310, 'PE', 'Joaquim Nabuco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5311, 'PE', 'José da Costa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5312, 'PE', 'José Mariano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5313, 'PE', 'Juçaral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5314, 'PE', 'Jucati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5315, 'PE', 'Jupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5316, 'PE', 'Jurema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5317, 'PE', 'Jutaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5318, 'PE', 'Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5319, 'PE', 'Lagoa de São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5320, 'PE', 'Lagoa do Barro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5321, 'PE', 'Lagoa do Carro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5322, 'PE', 'Lagoa do Itaenga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5323, 'PE', 'Lagoa do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5324, 'PE', 'Lagoa do Souza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5325, 'PE', 'Lagoa dos Gatos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5326, 'PE', 'Lagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5327, 'PE', 'Laje de São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5328, 'PE', 'Laje Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5329, 'PE', 'Lajedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5330, 'PE', 'Lajedo do Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5331, 'PE', 'Limoeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5332, 'PE', 'Livramento do Tiúma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5333, 'PE', 'Luanda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5334, 'PE', 'Macaparana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5335, 'PE', 'Machados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5336, 'PE', 'Macujé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5337, 'PE', 'Manari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5338, 'PE', 'Mandacaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5339, 'PE', 'Mandacaru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5340, 'PE', 'Maniçoba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5341, 'PE', 'Maraial');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5342, 'PE', 'Maravilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5343, 'PE', 'Mimoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5344, 'PE', 'Miracica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5345, 'PE', 'Mirandiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5346, 'PE', 'Morais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5347, 'PE', 'Moreilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5348, 'PE', 'Moreno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5349, 'PE', 'Moxotó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5350, 'PE', 'Mulungu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5351, 'PE', 'Murupé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5352, 'PE', 'Mutuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5353, 'PE', 'Nascente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5355, 'PE', 'Nazaré da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5356, 'PE', 'Negras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5357, 'PE', 'Nossa Senhora da Luz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5358, 'PE', 'Nossa Senhora do Carmo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5359, 'PE', 'Nossa Senhora do Ó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5360, 'PE', 'Nova Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5361, 'PE', 'Olho D Água de Dentro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5362, 'PE', 'Olinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5363, 'PE', 'Oratório');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5364, 'PE', 'Ori');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5365, 'PE', 'Orobó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5366, 'PE', 'Orocó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5367, 'PE', 'Ouricuri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5368, 'PE', 'Pajeú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5369, 'PE', 'Palmares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5370, 'PE', 'Palmeirina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5371, 'PE', 'Panelas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5372, 'PE', 'Pão de Açúcar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5373, 'PE', 'Pão de Açúcar do Poção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5374, 'PE', 'Papagaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5375, 'PE', 'Paquevira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5376, 'PE', 'Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5377, 'PE', 'Paranatama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5379, 'PE', 'Parnamirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5380, 'PE', 'Passagem do Tó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5381, 'PE', 'Passira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5382, 'PE', 'Pau Ferro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5383, 'PE', 'Paudalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5384, 'PE', 'Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5385, 'PE', 'Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5386, 'PE', 'Perpétuo Socorro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5387, 'PE', 'Pesqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5388, 'PE', 'Petrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5389, 'PE', 'Petrolina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5390, 'PE', 'Pirituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5391, 'PE', 'Poção');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5392, 'PE', 'Poção de Afrânio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5393, 'PE', 'Poço Comprido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5394, 'PE', 'Poço Fundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5395, 'PE', 'Pombos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5396, 'PE', 'Pontas de Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5399, 'PE', 'Primavera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5400, 'PE', 'Quipapá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5401, 'PE', 'Quitimbu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5402, 'PE', 'Quixabá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5403, 'PE', 'Rainha Isabel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5404, 'PE', 'Rajada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5405, 'PE', 'Rancharia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5406, 'PE', 'Recife');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5407, 'PE', 'Riacho das Almas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5408, 'PE', 'Riacho do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5409, 'PE', 'Riacho Fechado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5410, 'PE', 'Riacho Pequeno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5411, 'PE', 'Ribeirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5412, 'PE', 'Rio da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5413, 'PE', 'Rio Formoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5414, 'PE', 'Sairé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5415, 'PE', 'Salgadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5416, 'PE', 'Salgueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5417, 'PE', 'Saloá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5418, 'PE', 'Salobro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5419, 'PE', 'Sanharó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5420, 'PE', 'Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5421, 'PE', 'Santa Cruz da Baixa Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5422, 'PE', 'Santa Cruz do Capibaribe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5423, 'PE', 'Santa Filomena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5424, 'PE', 'Santa Maria da Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5425, 'PE', 'Santa Maria do Cambucá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5426, 'PE', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5427, 'PE', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5428, 'PE', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5429, 'PE', 'Santana de São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5430, 'PE', 'Santo Agostinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5431, 'PE', 'Santo Antônio das Queimadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5432, 'PE', 'Santo Antônio dos Palmares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5433, 'PE', 'São Benedito do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5434, 'PE', 'São Bento do Una');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5435, 'PE', 'São Caetano do Navio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5436, 'PE', 'São Caitano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5437, 'PE', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5438, 'PE', 'São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5439, 'PE', 'São Joaquim do Monte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5440, 'PE', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5441, 'PE', 'São José da Coroa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5442, 'PE', 'São José do Belmonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5443, 'PE', 'São José do Egito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5444, 'PE', 'São Lázaro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5445, 'PE', 'São Lourenço da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5446, 'PE', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5447, 'PE', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5448, 'PE', 'São Vicente Ferrer');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5449, 'PE', 'Sapucarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5450, 'PE', 'Saué');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5451, 'PE', 'Serra Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5452, 'PE', 'Serra do Vento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5453, 'PE', 'Serra Talhada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5454, 'PE', 'Serrita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5455, 'PE', 'Serrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5456, 'PE', 'Sertânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5457, 'PE', 'Sertãozinho de Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5458, 'PE', 'Siriji');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5459, 'PE', 'Sirinhaém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5460, 'PE', 'Sítio dos Nunes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5461, 'PE', 'Solidão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5462, 'PE', 'Surubim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5463, 'PE', 'Tabira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5464, 'PE', 'Tabocas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5465, 'PE', 'Tacaimbó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5466, 'PE', 'Tacaratu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5467, 'PE', 'Tamandaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5468, 'PE', 'Tamboatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5469, 'PE', 'Tapiraim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5470, 'PE', 'Taquaritinga do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5471, 'PE', 'Tara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5472, 'PE', 'Tauapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5473, 'PE', 'Tejucupapo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5474, 'PE', 'Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5475, 'PE', 'Terra Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5476, 'PE', 'Timbaúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5477, 'PE', 'Timorante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5478, 'PE', 'Toritama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5479, 'PE', 'Tracunhaém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5480, 'PE', 'Trapiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5481, 'PE', 'Três Ladeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5482, 'PE', 'Trindade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5483, 'PE', 'Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5484, 'PE', 'Tupanaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5485, 'PE', 'Tupanatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5486, 'PE', 'Tupaóca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5487, 'PE', 'Tuparetama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5488, 'PE', 'Umãs');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5489, 'PE', 'Umburetama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5490, 'PE', 'Upatininga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5491, 'PE', 'Urimama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5492, 'PE', 'Uruçu-Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5493, 'PE', 'Urucubá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5494, 'PE', 'Vasques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5495, 'PE', 'Veneza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5496, 'PE', 'Venturosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5497, 'PE', 'Verdejante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5498, 'PE', 'Vertente do Lério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5499, 'PE', 'Vertentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5500, 'PE', 'Vicência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5501, 'PE', 'Vila Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5502, 'PE', 'Viração');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5503, 'PE', 'Vitória de Santo Antão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5504, 'PE', 'Volta do Moxotó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5505, 'PE', 'Xexéu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5506, 'PE', 'Xucuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5507, 'PE', 'Zé Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5508, 'PI', 'Acauã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5509, 'PI', 'Agricolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5510, 'PI', 'Água Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5511, 'PI', 'Alagoinha do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5512, 'PI', 'Alegrete do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5513, 'PI', 'Alto Longá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5514, 'PI', 'Altos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5515, 'PI', 'Alvorada do Gurguéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5516, 'PI', 'Amarante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5517, 'PI', 'Angical do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5518, 'PI', 'Anísio de Abreu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5519, 'PI', 'Antônio Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5520, 'PI', 'Aroazes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5521, 'PI', 'Arraial');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5522, 'PI', 'Assunção do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5523, 'PI', 'Avelino Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5524, 'PI', 'Baixa Grande do Ribeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5525, 'PI', 'Barra D Alcântara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5526, 'PI', 'Barras');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 5500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5527, 'PI', 'Barreiras do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5528, 'PI', 'Barro Duro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5529, 'PI', 'Batalha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5530, 'PI', 'Bela Vista do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5531, 'PI', 'Belém do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5532, 'PI', 'Beneditinos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5533, 'PI', 'Bertolínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5534, 'PI', 'Betânia do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5535, 'PI', 'Boa Hora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5536, 'PI', 'Bocaina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5537, 'PI', 'Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5538, 'PI', 'Bom Princípio do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5539, 'PI', 'Bonfim do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5540, 'PI', 'Boqueirão do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5541, 'PI', 'Brasileira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5542, 'PI', 'Brejo do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5543, 'PI', 'Buriti dos Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5544, 'PI', 'Buriti dos Montes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5545, 'PI', 'Cabeceiras do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5546, 'PI', 'Cajazeiras do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5547, 'PI', 'Cajueiro da Praia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5548, 'PI', 'Caldeirão Grande do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5549, 'PI', 'Campinas do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5550, 'PI', 'Campo Alegre do Fidalgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5551, 'PI', 'Campo Grande do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5552, 'PI', 'Campo Largo do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5553, 'PI', 'Campo Maior');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5554, 'PI', 'Canavieira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5555, 'PI', 'Canto do Buriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5556, 'PI', 'Capitão de Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5557, 'PI', 'Capitão Gervásio Oliveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5558, 'PI', 'Caracol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5559, 'PI', 'Caraúbas do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5560, 'PI', 'Caridade do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5561, 'PI', 'Castelo do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5562, 'PI', 'Caxingó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5563, 'PI', 'Cocal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5564, 'PI', 'Cocal de Telha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5565, 'PI', 'Cocal dos Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5566, 'PI', 'Coivaras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5567, 'PI', 'Colônia do Gurguéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5568, 'PI', 'Colônia do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5569, 'PI', 'Conceição do Canindé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5570, 'PI', 'Coronel José Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5571, 'PI', 'Corrente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5572, 'PI', 'Cristalândia do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5573, 'PI', 'Cristino Castro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5574, 'PI', 'Curimatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5575, 'PI', 'Currais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5576, 'PI', 'Curral Novo do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5577, 'PI', 'Curralinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5578, 'PI', 'Demerval Lobão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5579, 'PI', 'Dirceu Arcoverde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5580, 'PI', 'Dom Expedito Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5581, 'PI', 'Dom Inocêncio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5582, 'PI', 'Domingos Mourão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5583, 'PI', 'Elesbão Veloso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5584, 'PI', 'Eliseu Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5585, 'PI', 'Esperantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5586, 'PI', 'Fartura do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5587, 'PI', 'Flores do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5588, 'PI', 'Floresta do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5589, 'PI', 'Floriano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5590, 'PI', 'Francinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5591, 'PI', 'Francisco Ayres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5592, 'PI', 'Francisco Macedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5593, 'PI', 'Francisco Santos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5594, 'PI', 'Fronteiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5595, 'PI', 'Geminiano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5596, 'PI', 'Gilbués');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5597, 'PI', 'Guadalupe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5598, 'PI', 'Guaribas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5599, 'PI', 'Hugo Napoleão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5600, 'PI', 'Ilha Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5601, 'PI', 'Inhuma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5602, 'PI', 'Ipiranga do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5603, 'PI', 'Isaías Coelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5604, 'PI', 'Itainópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5605, 'PI', 'Itaueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5606, 'PI', 'Jacobina do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5607, 'PI', 'Jaicós');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5608, 'PI', 'Jardim do Mulato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5609, 'PI', 'Jatobá do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5610, 'PI', 'Jerumenha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5611, 'PI', 'João Costa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5612, 'PI', 'Joaquim Pires');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5613, 'PI', 'Joca Marques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5614, 'PI', 'José de Freitas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5615, 'PI', 'Juazeiro do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5616, 'PI', 'Júlio Borges');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5617, 'PI', 'Jurema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5618, 'PI', 'Lagoa Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5619, 'PI', 'Lagoa de São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5620, 'PI', 'Lagoa do Barro do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5621, 'PI', 'Lagoa do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5622, 'PI', 'Lagoa do Sítio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5623, 'PI', 'Lagoinha do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5624, 'PI', 'Landri Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5625, 'PI', 'Luís Correia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5626, 'PI', 'Luzilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5627, 'PI', 'Madeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5628, 'PI', 'Manoel Emídio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5629, 'PI', 'Marcolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5630, 'PI', 'Marcos Parente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5631, 'PI', 'Massapê do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5632, 'PI', 'Matias Olímpio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5633, 'PI', 'Miguel Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5634, 'PI', 'Miguel Leão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5635, 'PI', 'Milton Brandão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5636, 'PI', 'Monsenhor Gil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5637, 'PI', 'Monsenhor Hipólito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5638, 'PI', 'Monte Alegre do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5639, 'PI', 'Morro Cabeça no Tempo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5640, 'PI', 'Morro do Chapéu do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5641, 'PI', 'Murici dos Portelas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5642, 'PI', 'Nazaré do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5643, 'PI', 'Nossa Senhora de Nazaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5644, 'PI', 'Nossa Senhora dos Remédios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5645, 'PI', 'Nova Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5646, 'PI', 'Novo Nilo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5647, 'PI', 'Novo Oriente do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5648, 'PI', 'Novo Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5649, 'PI', 'Oeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5650, 'PI', 'Olho D Água do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5651, 'PI', 'Padre Marcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5652, 'PI', 'Paes Landim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5653, 'PI', 'Pajeú do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5654, 'PI', 'Palmeira do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5655, 'PI', 'Palmeirais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5656, 'PI', 'Paquetá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5657, 'PI', 'Parnaguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5658, 'PI', 'Parnaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5659, 'PI', 'Passagem Franca do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5660, 'PI', 'Patos do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5661, 'PI', 'Paulistana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5662, 'PI', 'Pavussu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5663, 'PI', 'Pedro II');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5664, 'PI', 'Pedro Laurentino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5665, 'PI', 'Picos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5666, 'PI', 'Pimenteiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5667, 'PI', 'Pio IX');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5668, 'PI', 'Piracuruca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5669, 'PI', 'Piripiri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5670, 'PI', 'Porto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5671, 'PI', 'Porto Alegre do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5672, 'PI', 'Prata do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5673, 'PI', 'Queimada Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5674, 'PI', 'Redenção do Gurguéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5675, 'PI', 'Regeneração');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5676, 'PI', 'Riacho Frio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5677, 'PI', 'Ribeira do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5678, 'PI', 'Ribeiro Gonçalves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5679, 'PI', 'Rio Grande do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5680, 'PI', 'Santa Cruz do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5681, 'PI', 'Santa Cruz dos Milagres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5682, 'PI', 'Santa Filomena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5683, 'PI', 'Santa Luz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5684, 'PI', 'Santa Rosa do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5685, 'PI', 'Santana do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5686, 'PI', 'Santo Antônio de Lisboa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5687, 'PI', 'Santo Antônio dos Milagres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5688, 'PI', 'Santo Inácio do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5689, 'PI', 'São Braz do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5690, 'PI', 'São Félix do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5691, 'PI', 'São Francisco de Assis do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5692, 'PI', 'São Francisco do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5693, 'PI', 'São Gonçalo do Gurguéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5694, 'PI', 'São Gonçalo do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5695, 'PI', 'São João da Canabrava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5696, 'PI', 'São João da Fronteira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5697, 'PI', 'São João da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5698, 'PI', 'São João da Varjota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5699, 'PI', 'São João do Arraial');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5700, 'PI', 'São João do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5701, 'PI', 'São José do Divino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5702, 'PI', 'São José do Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5703, 'PI', 'São José do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5704, 'PI', 'São Julião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5705, 'PI', 'São Lourenço do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5706, 'PI', 'São Luís do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5707, 'PI', 'São Miguel da Baixa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5708, 'PI', 'São Miguel do Fidalgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5709, 'PI', 'São Miguel do Tapuio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5710, 'PI', 'São Pedro do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5711, 'PI', 'São Raimundo Nonato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5712, 'PI', 'Sebastião Barros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5713, 'PI', 'Sebastião Leal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5714, 'PI', 'Sigefredo Pacheco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5715, 'PI', 'Simões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5716, 'PI', 'Simplício Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5717, 'PI', 'Socorro do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5718, 'PI', 'Sussuapara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5719, 'PI', 'Tamboril do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5720, 'PI', 'Tanque do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5721, 'PI', 'Teresina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5722, 'PI', 'União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5723, 'PI', 'Uruçuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5724, 'PI', 'Valença do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5725, 'PI', 'Várzea Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5726, 'PI', 'Várzea Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5727, 'PI', 'Vera Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5728, 'PI', 'Vila Nova do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5729, 'PI', 'Wall Ferraz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5730, 'PR', 'Abapã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5731, 'PR', 'Abatiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5732, 'PR', 'Acampamento das Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5733, 'PR', 'Açungui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5734, 'PR', 'Adhemar de Barros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5735, 'PR', 'Adrianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5736, 'PR', 'Agostinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5737, 'PR', 'Água Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5738, 'PR', 'Água Boa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5739, 'PR', 'Água Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5741, 'PR', 'Água Mineral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5742, 'PR', 'Água Vermelha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5743, 'PR', 'Agudos do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5744, 'PR', 'Alecrim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5745, 'PR', 'Alexandra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5746, 'PR', 'Almirante Tamandaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5747, 'PR', 'Altamira do Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5748, 'PR', 'Altaneira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5749, 'PR', 'Alto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5750, 'PR', 'Alto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5751, 'PR', 'Alto Alegre do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5752, 'PR', 'Alto Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5753, 'PR', 'Alto do Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5754, 'PR', 'Alto Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5755, 'PR', 'Alto Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5756, 'PR', 'Alto Piquiri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5757, 'PR', 'Alto Porã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5758, 'PR', 'Alto Sabiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5759, 'PR', 'Alto Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5760, 'PR', 'Alto São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5761, 'PR', 'Altônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5762, 'PR', 'Alvorada do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5763, 'PR', 'Alvorada do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5764, 'PR', 'Amaporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5765, 'PR', 'Amorinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5766, 'PR', 'Ampére');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5767, 'PR', 'Anahy');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5768, 'PR', 'Andirá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5769, 'PR', 'Andorinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5770, 'PR', 'Angai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5771, 'PR', 'Ângulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5772, 'PR', 'Antas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5773, 'PR', 'Antas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5774, 'PR', 'Antonina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5775, 'PR', 'Antônio Brandão de Oliveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5776, 'PR', 'Antônio Olinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5777, 'PR', 'Anunciação');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5778, 'PR', 'Aparecida do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5779, 'PR', 'Apiaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5780, 'PR', 'Apucarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5781, 'PR', 'Aquidaban');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5782, 'PR', 'Aranha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5783, 'PR', 'Arapongas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5784, 'PR', 'Arapoti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5785, 'PR', 'Arapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5786, 'PR', 'Arapuan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5787, 'PR', 'Ararapira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5788, 'PR', 'Araruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5789, 'PR', 'Araucária');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5790, 'PR', 'Areia Branca dos Assis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5791, 'PR', 'Areias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5792, 'PR', 'Aricanduva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5793, 'PR', 'Ariranha do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5794, 'PR', 'Aroeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5795, 'PR', 'Arquimedes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5796, 'PR', 'Assaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5797, 'PR', 'Assis Chateaubriand');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5798, 'PR', 'Astorga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5799, 'PR', 'Atalaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5800, 'PR', 'Aurora do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5801, 'PR', 'Bairro Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5803, 'PR', 'Bairro Limoeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5804, 'PR', 'Balsa Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5805, 'PR', 'Bandeirantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5806, 'PR', 'Bandeirantes d Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5807, 'PR', 'Banhado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5808, 'PR', 'Barão de Lucena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5809, 'PR', 'Barbosa Ferraz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5810, 'PR', 'Barra Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5811, 'PR', 'Barra Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5812, 'PR', 'Barra do Jacaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5813, 'PR', 'Barra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5815, 'PR', 'Barra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5816, 'PR', 'Barra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5818, 'PR', 'Barra Santa Salete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5819, 'PR', 'Barracão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5821, 'PR', 'Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5822, 'PR', 'Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5823, 'PR', 'Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5825, 'PR', 'Barreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5826, 'PR', 'Barrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5827, 'PR', 'Barro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5828, 'PR', 'Barro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5829, 'PR', 'Barro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5830, 'PR', 'Bateias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5831, 'PR', 'Baulândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5832, 'PR', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5833, 'PR', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5834, 'PR', 'Bela Vista da Caroba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5835, 'PR', 'Bela Vista do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5836, 'PR', 'Bela Vista do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5837, 'PR', 'Bela Vista do Piquiri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5838, 'PR', 'Bela Vista do Tapiracui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5839, 'PR', 'Bentópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5840, 'PR', 'Bernardelli');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5841, 'PR', 'Betaras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5842, 'PR', 'Bituruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5843, 'PR', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5845, 'PR', 'Boa Esperança do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5846, 'PR', 'Boa Ventura de São Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5847, 'PR', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5848, 'PR', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5849, 'PR', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5850, 'PR', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5851, 'PR', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5852, 'PR', 'Boa Vista da Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5853, 'PR', 'Bocaina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5854, 'PR', 'Bocaiúva do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5855, 'PR', 'Bom Jardim do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5856, 'PR', 'Bom Jesus do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5857, 'PR', 'Bom Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5858, 'PR', 'Bom Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5859, 'PR', 'Bom Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5860, 'PR', 'Bom Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5861, 'PR', 'Bom Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5862, 'PR', 'Bom Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5863, 'PR', 'Bom Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5864, 'PR', 'Bom Sucesso do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5865, 'PR', 'Borda do Campo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5867, 'PR', 'Borman');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5868, 'PR', 'Borrazópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5869, 'PR', 'Botuquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5870, 'PR', 'Bourbonia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5871, 'PR', 'Braganey');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5872, 'PR', 'Bragantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5873, 'PR', 'Brasilândia do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5874, 'PR', 'Bugre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5875, 'PR', 'Bulcão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5876, 'PR', 'Cabrito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5877, 'PR', 'Cacatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5878, 'PR', 'Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5879, 'PR', 'Cachoeira de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5880, 'PR', 'Cachoeira de São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5881, 'PR', 'Cachoeira do Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5882, 'PR', 'Colônia Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5883, 'PR', 'Cachoeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5884, 'PR', 'Cadeadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5885, 'PR', 'Caetano Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5886, 'PR', 'Cafeara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5887, 'PR', 'Cafeeiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5888, 'PR', 'Cafelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5889, 'PR', 'Cafezal do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5890, 'PR', 'Caitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5892, 'PR', 'Califórnia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5893, 'PR', 'Calógeras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5894, 'PR', 'Cambará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5895, 'PR', 'Cambé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5896, 'PR', 'Cambiju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5897, 'PR', 'Cambira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5898, 'PR', 'Campestrinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5899, 'PR', 'Campina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5900, 'PR', 'Campina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5901, 'PR', 'Campina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5902, 'PR', 'Campina da Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5903, 'PR', 'Campina do Miranguava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5904, 'PR', 'Campina do Simão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5905, 'PR', 'Campina dos Furtados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5906, 'PR', 'Campina Grande do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5907, 'PR', 'Campinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5908, 'PR', 'Campo Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5909, 'PR', 'Campo do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5910, 'PR', 'Campo do Tenente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5911, 'PR', 'Campo Largo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5912, 'PR', 'Campo Largo da Roseira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5913, 'PR', 'Campo Magro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5914, 'PR', 'Campo Mourão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5915, 'PR', 'Cândido de Abreu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5916, 'PR', 'Candói');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5917, 'PR', 'Canela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5918, 'PR', 'Cantagalo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5919, 'PR', 'Canzianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5920, 'PR', 'Capanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5921, 'PR', 'Capão Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5922, 'PR', 'Capão Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5923, 'PR', 'Capão Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5924, 'PR', 'Capão da Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5925, 'PR', 'Capão Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5926, 'PR', 'Capão Rico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5927, 'PR', 'Capitão Leônidas Marques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5928, 'PR', 'Capivara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5929, 'PR', 'Capoeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5930, 'PR', 'Cara Pintado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5931, 'PR', 'Cará-Cará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5932, 'PR', 'Carajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5933, 'PR', 'Carambeí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5934, 'PR', 'Caramuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5935, 'PR', 'Caratuva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5936, 'PR', 'Carazinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5937, 'PR', 'Carbonera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5938, 'PR', 'Carlópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5939, 'PR', 'Casa Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5940, 'PR', 'Cascatinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5941, 'PR', 'Cascavel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5942, 'PR', 'Castro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5943, 'PR', 'Catanduvas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5944, 'PR', 'Catanduvas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5945, 'PR', 'Catanduvas do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5946, 'PR', 'Catarinenses');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5947, 'PR', 'Caxambu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5948, 'PR', 'Centenário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5949, 'PR', 'Centenário do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5950, 'PR', 'Central Lupion');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5951, 'PR', 'Centralito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5952, 'PR', 'Centro Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5953, 'PR', 'Cerne');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5954, 'PR', 'Cerrado Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5955, 'PR', 'Cerro Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5956, 'PR', 'Cerro Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5957, 'PR', 'Céu Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5958, 'PR', 'Chopinzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5959, 'PR', 'Cianorte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5960, 'PR', 'Cidade Gaúcha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5961, 'PR', 'Cintra Pimentel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5962, 'PR', 'Clevelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5963, 'PR', 'Coitinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5964, 'PR', 'Colombo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5965, 'PR', 'Colônia Acioli');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5966, 'PR', 'Colônia Castelhanos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5967, 'PR', 'Colônia Castrolânda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5968, 'PR', 'Colônia Centenário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5969, 'PR', 'Colônia Cristina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5970, 'PR', 'Colônia Dom Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5971, 'PR', 'Colônia Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5972, 'PR', 'Colônia General Carneiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5973, 'PR', 'Colônia Iapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5974, 'PR', 'Colônia Melissa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5975, 'PR', 'Colônia Murici');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5976, 'PR', 'Colônia Padre Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5977, 'PR', 'Colônia Pereira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5978, 'PR', 'Colônia Santos Andrade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5979, 'PR', 'Colônia São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5980, 'PR', 'Colônia Sapucaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5981, 'PR', 'Colônia Saúde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5982, 'PR', 'Colônia Tapera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5983, 'PR', 'Colorado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5984, 'PR', 'Comur');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5986, 'PR', 'Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5987, 'PR', 'Conchas Velhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5988, 'PR', 'Conciolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5989, 'PR', 'Congonhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5990, 'PR', 'Congonhinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5991, 'PR', 'Conselheiro Mairinck');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5992, 'PR', 'Conselheiro Zacarias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5993, 'PR', 'Contenda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5994, 'PR', 'Copacabana do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5995, 'PR', 'Corbélia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5996, 'PR', 'Cornélio Procópio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5997, 'PR', 'Coronel Domingos Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5998, 'PR', 'Coronel Firmino Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (5999, 'PR', 'Coronel Vivida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6000, 'PR', 'Correia de Freitas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6001, 'PR', 'Corumbataí do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6002, 'PR', 'Corvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6003, 'PR', 'Costeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6004, 'PR', 'Covó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6005, 'PR', 'Coxilha Rica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6006, 'PR', 'Cristo Rei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6007, 'PR', 'Cristo Rei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6008, 'PR', 'Cruz Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6009, 'PR', 'Cruzeiro do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6010, 'PR', 'Cruzeiro do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6011, 'PR', 'Cruzeiro do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6012, 'PR', 'Cruzeiro do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6013, 'PR', 'Cruzmaltina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6014, 'PR', 'Cunhaporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6015, 'PR', 'Curitiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6016, 'PR', 'Curiúva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6017, 'PR', 'Curucaca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6018, 'PR', 'Deputado José Afonso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6019, 'PR', 'Despique');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6021, 'PR', 'Dez de Maio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6023, 'PR', 'Diamante d Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6024, 'PR', 'Diamante do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6025, 'PR', 'Diamante do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6026, 'PR', 'Doce Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6027, 'PR', 'Dois Irmãos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6028, 'PR', 'Dois Irmãos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6029, 'PR', 'Dois Marcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6030, 'PR', 'Dois Vizinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6031, 'PR', 'Dom Rodrigo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6032, 'PR', 'Dorizon');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6033, 'PR', 'Douradina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6034, 'PR', 'Doutor Antônio Paranhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6035, 'PR', 'Doutor Camargo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6036, 'PR', 'Doutor Ernesto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6037, 'PR', 'Doutor Oliveira Castro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6040, 'PR', 'Emboguaçu');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 6000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6041, 'PR', 'Emboque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6042, 'PR', 'Emboque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6043, 'PR', 'Encantado d Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6044, 'PR', 'Encruzilhada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6045, 'PR', 'Encruzilhada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6046, 'PR', 'Enéas Marques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6047, 'PR', 'Engenheiro Beltrão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6048, 'PR', 'Entre Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6049, 'PR', 'Entre Rios do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6050, 'PR', 'Esperança do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6051, 'PR', 'Esperança Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6052, 'PR', 'Espigão Alto do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6053, 'PR', 'Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6054, 'PR', 'Estação General Lúcio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6055, 'PR', 'Estação Roça Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6056, 'PR', 'Europa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6057, 'PR', 'Euzébio de Oliveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6058, 'PR', 'Faisqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6059, 'PR', 'Farol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6060, 'PR', 'Faxina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6061, 'PR', 'Faxinal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6062, 'PR', 'Faxinal do Céu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6063, 'PR', 'Faxinal dos Elias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6064, 'PR', 'Faxinal Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6065, 'PR', 'Fazenda do Brigadeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6066, 'PR', 'Fazenda dos Barbosas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6067, 'PR', 'Fazenda Jangada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6068, 'PR', 'Fazenda Rio Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6069, 'PR', 'Fazenda Salmo 23');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6070, 'PR', 'Fazendinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6071, 'PR', 'Felpudo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6072, 'PR', 'Fênix');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6073, 'PR', 'Fernandes Pinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6074, 'PR', 'Fernão Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6075, 'PR', 'Ferraria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6076, 'PR', 'Ferreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6077, 'PR', 'Figueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6078, 'PR', 'Figueira do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6079, 'PR', 'Fiusas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6080, 'PR', 'Flor da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6081, 'PR', 'Flor da Serra do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6082, 'PR', 'Floraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6083, 'PR', 'Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6084, 'PR', 'Florestópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6085, 'PR', 'Floriano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6086, 'PR', 'Flórida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6087, 'PR', 'Florópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6088, 'PR', 'Fluviópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6089, 'PR', 'Formigone');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6090, 'PR', 'Formosa do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6091, 'PR', 'Foz do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6092, 'PR', 'Foz do Jordão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6093, 'PR', 'Francisco Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6094, 'PR', 'Francisco Beltrão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6095, 'PR', 'Francisco Frederico Teixeira Guimarães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6096, 'PR', 'Frei Timóteo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6097, 'PR', 'Fueros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6098, 'PR', 'Fundão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6099, 'PR', 'Gamadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6100, 'PR', 'Gamela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6101, 'PR', 'Gaúcha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6102, 'PR', 'Gavião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6103, 'PR', 'Gavião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6104, 'PR', 'General Carneiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6105, 'PR', 'General Osório');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6106, 'PR', 'Geremia Lunardelli');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6107, 'PR', 'Godoy Moreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6108, 'PR', 'Goioerê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6109, 'PR', 'Goioxim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6110, 'PR', 'Góis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6111, 'PR', 'Gonçalves Júnior');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6112, 'PR', 'Graciosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6113, 'PR', 'Grandes Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6114, 'PR', 'Guaiporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6115, 'PR', 'Guaíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6116, 'PR', 'Guairaçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6117, 'PR', 'Guairaçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6118, 'PR', 'Guairaçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6119, 'PR', 'Guajuvira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6120, 'PR', 'Guamiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6121, 'PR', 'Guamirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6122, 'PR', 'Guapirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6123, 'PR', 'Guaporé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6124, 'PR', 'Guaporema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6125, 'PR', 'Guará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6126, 'PR', 'Guaraci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6127, 'PR', 'Guaragi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6128, 'PR', 'Guaraituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6129, 'PR', 'Guarani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6130, 'PR', 'Guaraniaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6131, 'PR', 'Guarapuava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6132, 'PR', 'Guarapuavinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6133, 'PR', 'Guaraqueçaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6134, 'PR', 'Guararema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6135, 'PR', 'Guaratuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6136, 'PR', 'Guaraúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6137, 'PR', 'Guaravera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6138, 'PR', 'Guardamoria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6139, 'PR', 'Harmonia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6140, 'PR', 'Herculândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6141, 'PR', 'Herval Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6142, 'PR', 'Herveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6143, 'PR', 'Herveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6144, 'PR', 'Hidrelétrica Itaipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6145, 'PR', 'Honório Serpa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6146, 'PR', 'Iarama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6147, 'PR', 'Ibaiti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6148, 'PR', 'Ibema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6149, 'PR', 'Ibiaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6150, 'PR', 'Ibiporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6151, 'PR', 'Içara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6152, 'PR', 'Icaraíma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6153, 'PR', 'Icatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6154, 'PR', 'Igrejinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6155, 'PR', 'Iguaraçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6156, 'PR', 'Iguatemi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6157, 'PR', 'Iguatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6158, 'PR', 'Iguiporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6159, 'PR', 'Ilha do Mel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6160, 'PR', 'Ilha dos Valadares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6161, 'PR', 'Imbaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6162, 'PR', 'Imbauzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6163, 'PR', 'Imbituva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6164, 'PR', 'Inácio Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6165, 'PR', 'Inajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6166, 'PR', 'Independência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6167, 'PR', 'Indianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6168, 'PR', 'Inspetor Carvalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6169, 'PR', 'Invernada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6170, 'PR', 'Invernadinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6171, 'PR', 'Iolópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6172, 'PR', 'Ipiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6173, 'PR', 'Ipiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6174, 'PR', 'Vila Ipiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6175, 'PR', 'Iporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6176, 'PR', 'Iracema do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6177, 'PR', 'Irapuan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6178, 'PR', 'Irati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6179, 'PR', 'Irerê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6180, 'PR', 'Iretama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6181, 'PR', 'Itaguajé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6182, 'PR', 'Itaiacoca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6183, 'PR', 'Itaipulândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6184, 'PR', 'Itambaracá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6185, 'PR', 'Itambé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6186, 'PR', 'Itambé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6187, 'PR', 'Itapanhacanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6188, 'PR', 'Itapara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6189, 'PR', 'Itapejara d Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6190, 'PR', 'Itaperuçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6191, 'PR', 'Itaqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6192, 'PR', 'Itaúna do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6193, 'PR', 'Itinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6194, 'PR', 'Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6195, 'PR', 'Ivailândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6196, 'PR', 'Ivaiporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6197, 'PR', 'Ivaitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6198, 'PR', 'Ivaté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6199, 'PR', 'Ivatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6200, 'PR', 'Jaborandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6201, 'PR', 'Jaboti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6202, 'PR', 'Jaboticabal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6203, 'PR', 'Jaburu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6204, 'PR', 'Jacaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6205, 'PR', 'Jacarezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6206, 'PR', 'Jaciaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6207, 'PR', 'Jacutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6208, 'PR', 'Jacutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6209, 'PR', 'Jacutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6210, 'PR', 'Jacutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6211, 'PR', 'Jaguapitã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6212, 'PR', 'Jaguariaíva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6213, 'PR', 'Jandaia do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6214, 'PR', 'Jangada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6215, 'PR', 'Jangada do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6216, 'PR', 'Janiópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6217, 'PR', 'Japira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6218, 'PR', 'Japurá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6219, 'PR', 'Jaracatiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6220, 'PR', 'Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6221, 'PR', 'Jardim Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6222, 'PR', 'Jardim Olinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6223, 'PR', 'Jardim Paredão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6225, 'PR', 'Jandinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6226, 'PR', 'Jataizinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6227, 'PR', 'Javacaé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6228, 'PR', 'Jesuítas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6229, 'PR', 'Joá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6230, 'PR', 'Joaquim Távora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6231, 'PR', 'Colônia Jordãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6232, 'PR', 'José Lacerda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6233, 'PR', 'Juciara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6234, 'PR', 'Jundiaí do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6235, 'PR', 'Juranda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6236, 'PR', 'Jussara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6237, 'PR', 'Juvinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6238, 'PR', 'Kaloré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6239, 'PR', 'km 30');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6240, 'PR', 'Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6241, 'PR', 'Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6242, 'PR', 'Lagoa Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6243, 'PR', 'Lagoa dos Ribas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6244, 'PR', 'Lagoa Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6245, 'PR', 'Lagoa Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6246, 'PR', 'Lagoa Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6247, 'PR', 'Lagoinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6248, 'PR', 'Lajeado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6249, 'PR', 'Lajeado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6250, 'PR', 'Lajeado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6251, 'PR', 'Lajeado Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6252, 'PR', 'Lajeado Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6253, 'PR', 'Lambari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6254, 'PR', 'Lapa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6255, 'PR', 'Laranja Azeda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6256, 'PR', 'Laranjal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6257, 'PR', 'Laranjeiras do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6258, 'PR', 'Lavra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6259, 'PR', 'Lavrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6260, 'PR', 'Lavrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6261, 'PR', 'Leópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6262, 'PR', 'Lerroville');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6263, 'PR', 'Lidianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6264, 'PR', 'Lindoeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6265, 'PR', 'Linha Giacomini');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6266, 'PR', 'Loanda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6267, 'PR', 'Lobato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6268, 'PR', 'Londrina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6269, 'PR', 'Lopei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6270, 'PR', 'Lovat');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6271, 'PR', 'Luar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6272, 'PR', 'Luiziana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6273, 'PR', 'Lunardelli');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6274, 'PR', 'Lupionópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6275, 'PR', 'Macaco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6276, 'PR', 'Macucos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6277, 'PR', 'Mairá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6278, 'PR', 'Maitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6279, 'PR', 'Mallet');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6280, 'PR', 'Malu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6281, 'PR', 'Mamborê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6282, 'PR', 'Mandaçaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6283, 'PR', 'Mandaguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6284, 'PR', 'Mandaguari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6285, 'PR', 'Mandiocaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6286, 'PR', 'Mandirituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6287, 'PR', 'Manfrinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6288, 'PR', 'Mangueirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6289, 'PR', 'Manoel Ribas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6290, 'PR', 'Marabá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6291, 'PR', 'Maracanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6292, 'PR', 'Marajó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6293, 'PR', 'Maravilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6294, 'PR', 'Maravilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6295, 'PR', 'Marcelino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6296, 'PR', 'Marcionópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6297, 'PR', 'Marechal Cândido Rondon');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6298, 'PR', 'Margarida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6299, 'PR', 'Maria Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6300, 'PR', 'Maria Luiza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6301, 'PR', 'Marialva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6302, 'PR', 'Mariental');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6303, 'PR', 'Marilândia do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6304, 'PR', 'Marilena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6305, 'PR', 'Marilu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6306, 'PR', 'Mariluz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6307, 'PR', 'Marimbondo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6308, 'PR', 'Maringá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6309, 'PR', 'Mariópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6310, 'PR', 'Maripá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6311, 'PR', 'Maristela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6312, 'PR', 'Mariza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6313, 'PR', 'Marmelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6314, 'PR', 'Marmeleiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6315, 'PR', 'Marquês de Abrantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6316, 'PR', 'Marquinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6317, 'PR', 'Marrecas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6318, 'PR', 'Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6319, 'PR', 'Marumbi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6320, 'PR', 'Matelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6321, 'PR', 'Matinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6322, 'PR', 'Matinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6323, 'PR', 'Mato Queimado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6324, 'PR', 'Mato Rico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6325, 'PR', 'Mauá da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6326, 'PR', 'Medianeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6327, 'PR', 'Meia-Lua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6328, 'PR', 'Memória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6329, 'PR', 'Mendeslândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6330, 'PR', 'Mercedes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6331, 'PR', 'Mirador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6332, 'PR', 'Miranda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6333, 'PR', 'Mirante do Piquiri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6334, 'PR', 'Miraselva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6335, 'PR', 'Missal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6336, 'PR', 'Monjolinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6337, 'PR', 'Monte Real');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6338, 'PR', 'Moreira Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6339, 'PR', 'Morretes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6340, 'PR', 'Morro Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6341, 'PR', 'Morro Inglês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6342, 'PR', 'Munhoz de Melo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6343, 'PR', 'Natingui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6344, 'PR', 'Nilza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6345, 'PR', 'Nordestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6346, 'PR', 'Nossa Senhora Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6347, 'PR', 'Nossa Senhora da Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6348, 'PR', 'Nossa Senhora da Candelária');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6349, 'PR', 'Nossa Senhora das Graças');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6350, 'PR', 'Nossa Senhora de Lourdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6351, 'PR', 'Nossa Senhora do Carmo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6352, 'PR', 'Nova Aliança do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6353, 'PR', 'Nova Altamira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6354, 'PR', 'Nova América da Colina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6355, 'PR', 'Nova Amoreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6356, 'PR', 'Nova Aurora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6357, 'PR', 'Nova Bilac');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6358, 'PR', 'Nova Brasília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6359, 'PR', 'Nova Brasília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6360, 'PR', 'Nova Brasília do Itararé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6361, 'PR', 'Nova Cantu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6362, 'PR', 'Nova Concórdia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6363, 'PR', 'Concórdia do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6364, 'PR', 'Nova Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6365, 'PR', 'Nova Esperança do Sudoeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6366, 'PR', 'Nova Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6367, 'PR', 'Nova Laranjeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6368, 'PR', 'Nova Londrina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6369, 'PR', 'Nova Lourdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6370, 'PR', 'Nova Olímpia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6371, 'PR', 'Nova Prata do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6372, 'PR', 'Nova Riqueza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6373, 'PR', 'Nova Santa Bárbara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6374, 'PR', 'Nova Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6375, 'PR', 'Nova Tebas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6376, 'PR', 'Nova Tirol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6377, 'PR', 'Nova Videira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6378, 'PR', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6379, 'PR', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6380, 'PR', 'Novo Itacolomi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6381, 'PR', 'Novo Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6382, 'PR', 'Novo Sarandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6383, 'PR', 'Novo Sobradinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6384, 'PR', 'Novo Três Passos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6385, 'PR', 'Olaria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6386, 'PR', 'Olaria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6387, 'PR', 'Olho Agudo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6388, 'PR', 'Olho d Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6389, 'PR', 'Oroité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6390, 'PR', 'Ortigueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6391, 'PR', 'Ourilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6392, 'PR', 'Ourizona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6393, 'PR', 'Ouro Verde do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6394, 'PR', 'Ouro Verde do Piquiri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6395, 'PR', 'Padre Ponciano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6396, 'PR', 'Paiçandu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6397, 'PR', 'Paiol de Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6398, 'PR', 'Paiol Queimado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6400, 'PR', 'Paiquerê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6401, 'PR', 'Palmas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6402, 'PR', 'Palmeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6403, 'PR', 'Palmeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6404, 'PR', 'Palmeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6405, 'PR', 'Palmira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6406, 'PR', 'Palmital');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6407, 'PR', 'Palmital');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6408, 'PR', 'Palmital de São Silvestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6409, 'PR', 'Palmitópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6410, 'PR', 'Palotina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6411, 'PR', 'Panema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6412, 'PR', 'Pangaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6413, 'PR', 'Papagaios Novos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6414, 'PR', 'Paraíso do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6415, 'PR', 'Paraná d Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6416, 'PR', 'Paranacity');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6417, 'PR', 'Paranagi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6418, 'PR', 'Paranaguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6419, 'PR', 'Paranapoema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6420, 'PR', 'Paranavaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6421, 'PR', 'Passa Una');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6422, 'PR', 'Passo da Ilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6423, 'PR', 'Passo dos Pupos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6424, 'PR', 'Passo Fundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6425, 'PR', 'Passo Liso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6426, 'PR', 'Pato Bragado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6427, 'PR', 'Pato Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6428, 'PR', 'Patos Velhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6429, 'PR', 'Pau d Alho do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6430, 'PR', 'Paula Freitas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6431, 'PR', 'Paulistânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6432, 'PR', 'Paulo Frontin');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6433, 'PR', 'Peabiru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6434, 'PR', 'Pedra Branca do Araraquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6435, 'PR', 'Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6436, 'PR', 'Pedro Lustosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6437, 'PR', 'Pelado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6438, 'PR', 'Perobal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6439, 'PR', 'Pérola');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6440, 'PR', 'Pérola d Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6441, 'PR', 'Pérola Independente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6442, 'PR', 'Piassuguera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6443, 'PR', 'Piên');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6444, 'PR', 'Pinaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6445, 'PR', 'Pinhais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6446, 'PR', 'Pinhal de São Bento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6448, 'PR', 'Pinhalão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6449, 'PR', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6450, 'PR', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6451, 'PR', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6452, 'PR', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6453, 'PR', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6454, 'PR', 'Pinhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6455, 'PR', 'Pinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6456, 'PR', 'Piquirivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6457, 'PR', 'Piracema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6458, 'PR', 'Piraí do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6459, 'PR', 'Pirapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6460, 'PR', 'Piraquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6461, 'PR', 'Piriquitos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6462, 'PR', 'Pitanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6463, 'PR', 'Pitangueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6464, 'PR', 'Pitangui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6465, 'PR', 'Planaltina do Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6466, 'PR', 'Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6467, 'PR', 'Pocinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6468, 'PR', 'Pocinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6469, 'PR', 'Poema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6470, 'PR', 'Ponta do Pasto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6471, 'PR', 'Ponta Grossa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6472, 'PR', 'Pontal do Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6473, 'PR', 'Porecatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6474, 'PR', 'Portão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6475, 'PR', 'Porteira Preta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6476, 'PR', 'Porto Amazonas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6477, 'PR', 'Porto Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6478, 'PR', 'Porto Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6479, 'PR', 'Porto Brasílio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6480, 'PR', 'Porto Camargo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6481, 'PR', 'Porto de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6482, 'PR', 'Porto Meira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6483, 'PR', 'Porto Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6484, 'PR', 'Porto Rico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6485, 'PR', 'Porto San Juan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6486, 'PR', 'Porto Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6487, 'PR', 'Porto São Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6488, 'PR', 'Porto São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6489, 'PR', 'Porto Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6490, 'PR', 'Prado Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6491, 'PR', 'Pranchita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6492, 'PR', 'Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6493, 'PR', 'Prata Um');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6494, 'PR', 'Presidente Castelo Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6495, 'PR', 'Presidente Kennedy');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6496, 'PR', 'Primeiro de Maio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6497, 'PR', 'Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6498, 'PR', 'Prudentópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6499, 'PR', 'Pulinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6500, 'PR', 'Quatiguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6501, 'PR', 'Quatro Barras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6502, 'PR', 'Quatro Pontes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6503, 'PR', 'Quebra Freio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6504, 'PR', 'Quedas do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6505, 'PR', 'Queimados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6506, 'PR', 'Querência do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6507, 'PR', 'Quinta do Sol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6508, 'PR', 'Quinzópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6509, 'PR', 'Quitandinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6510, 'PR', 'Ramilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6511, 'PR', 'Rancho Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6512, 'PR', 'Rancho Alegre d Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6513, 'PR', 'Realeza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6514, 'PR', 'Rebouças');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6515, 'PR', 'Região dos Valos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6516, 'PR', 'Reianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6517, 'PR', 'Renascença');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6518, 'PR', 'Reserva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6519, 'PR', 'Reserva do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6520, 'PR', 'Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6521, 'PR', 'Retiro Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6522, 'PR', 'Ribeirão Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6523, 'PR', 'Ribeirão Claro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6524, 'PR', 'Ribeirão do Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6525, 'PR', 'Ribeirão do Pinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6526, 'PR', 'Rio Abaixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6527, 'PR', 'Rio Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6528, 'PR', 'Rio Bom');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6529, 'PR', 'Rio Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6530, 'PR', 'Rio Bonito do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6531, 'PR', 'Rio Branco do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6532, 'PR', 'Rio Branco do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6533, 'PR', 'Rio Claro do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6534, 'PR', 'Rio da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6535, 'PR', 'Rio da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6536, 'PR', 'Rio das Antas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6537, 'PR', 'Rio das Mortes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6538, 'PR', 'Rio das Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6539, 'PR', 'Rio das Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6540, 'PR', 'Rio das Pombas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6541, 'PR', 'Rio do Mato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6542, 'PR', 'Rio do Salto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6543, 'PR', 'Rio Negro');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 6500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6544, 'PR', 'Rio Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6545, 'PR', 'Rio Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6546, 'PR', 'Rio Pinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6547, 'PR', 'Rio Quatorze');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6548, 'PR', 'Rio Saltinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6549, 'PR', 'Rio Saudade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6550, 'PR', 'Rio Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6551, 'PR', 'Roberto Silveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6552, 'PR', 'Roça Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6553, 'PR', 'Roça Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6554, 'PR', 'Rolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6555, 'PR', 'Romeópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6556, 'PR', 'Roncador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6557, 'PR', 'Rondinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6558, 'PR', 'Rondon');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6559, 'PR', 'Rosário do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6560, 'PR', 'Sabáudia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6561, 'PR', 'Sagrada Família');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6562, 'PR', 'Salgado Filho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6563, 'PR', 'Salles de Oliveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6564, 'PR', 'Saltinho do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6565, 'PR', 'Salto do Itararé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6566, 'PR', 'Salto do Lontra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6567, 'PR', 'Salto Portão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6568, 'PR', 'Colônia Samambaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6569, 'PR', 'Santa Amélia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6570, 'PR', 'Santa Cecília do Pavão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6571, 'PR', 'Santa Clara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6572, 'PR', 'Santa Cruz de Monte Castelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6573, 'PR', 'Santa Eliza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6574, 'PR', 'Santa Esmeralda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6575, 'PR', 'Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6576, 'PR', 'Santa Fé do Pirapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6577, 'PR', 'Santa Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6578, 'PR', 'Santa Inês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6579, 'PR', 'Santa Isabel do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6580, 'PR', 'Santa Izabel do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6581, 'PR', 'Santa Lúcia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6582, 'PR', 'Santa Lurdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6583, 'PR', 'Santa Luzia da Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6584, 'PR', 'Santa Margarida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6585, 'PR', 'Santa Margarida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6586, 'PR', 'Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6587, 'PR', 'Santa Maria do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6588, 'PR', 'Santa Maria do Rio do Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6589, 'PR', 'Santa Mariana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6590, 'PR', 'Santa Mônica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6591, 'PR', 'Santa Quitéria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6592, 'PR', 'Santa Quitéria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6593, 'PR', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6594, 'PR', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6595, 'PR', 'Santa Rita do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6596, 'PR', 'Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6597, 'PR', 'Santa Tereza do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6598, 'PR', 'Santa Terezinha de Itaipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6599, 'PR', 'Santa Zélia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6600, 'PR', 'Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6601, 'PR', 'Santana do Itararé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6602, 'PR', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6603, 'PR', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6604, 'PR', 'Santo Antônio da Platina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6605, 'PR', 'Santo Antônio do Caiuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6606, 'PR', 'Santo Antônio do Iratim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6607, 'PR', 'Santo Antônio do Palmital');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6608, 'PR', 'Santo Antônio do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6609, 'PR', 'Santo Antônio do Sudoeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6610, 'PR', 'Santo Inácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6611, 'PR', 'Santo Rei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6613, 'PR', 'São Braz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6614, 'PR', 'São Braz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6615, 'PR', 'São Camilo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6616, 'PR', 'São Carlos do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6617, 'PR', 'São Cirilo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6618, 'PR', 'São Clemente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6619, 'PR', 'São Cristovão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6620, 'PR', 'São Cristovão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6621, 'PR', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6622, 'PR', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6623, 'PR', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6624, 'PR', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6625, 'PR', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6626, 'PR', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6627, 'PR', 'São Francisco de Imbaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6628, 'PR', 'São Francisco de Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6629, 'PR', 'São Gabriel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6630, 'PR', 'São Gabriel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6631, 'PR', 'São Gotardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6632, 'PR', 'São Jerônimo da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6633, 'PR', 'São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6634, 'PR', 'São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6636, 'PR', 'São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6637, 'PR', 'São João d Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6639, 'PR', 'São João do Caiuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6640, 'PR', 'São João do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6641, 'PR', 'São João do Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6642, 'PR', 'São João do Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6643, 'PR', 'São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6644, 'PR', 'São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6645, 'PR', 'São Joaquim do Pontal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6646, 'PR', 'São Jorge D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6647, 'PR', 'São Jorge do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6648, 'PR', 'São Jorge do Patrocínio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6649, 'PR', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6650, 'PR', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6651, 'PR', 'São José da Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6652, 'PR', 'São José das Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6653, 'PR', 'São José do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6654, 'PR', 'São José do Itavo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6655, 'PR', 'São José do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6656, 'PR', 'São José dos Pinhais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6657, 'PR', 'São Judas Tadeu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6658, 'PR', 'São Leonardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6659, 'PR', 'São Lourenço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6660, 'PR', 'São Lourenço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6662, 'PR', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6663, 'PR', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6664, 'PR', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6665, 'PR', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6666, 'PR', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6667, 'PR', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6668, 'PR', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6669, 'PR', 'São Luiz do Purunã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6670, 'PR', 'São Manoel do Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6671, 'PR', 'São Marcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6672, 'PR', 'São Marcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6673, 'PR', 'São Martinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6674, 'PR', 'São Mateus do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6675, 'PR', 'São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6676, 'PR', 'São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6677, 'PR', 'São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6678, 'PR', 'São Miguel do Cambui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6679, 'PR', 'São Miguel do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6680, 'PR', 'São Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6681, 'PR', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6682, 'PR', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6683, 'PR', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6684, 'PR', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6685, 'PR', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6686, 'PR', 'São Pedro do Florido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6687, 'PR', 'São Pedro do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6688, 'PR', 'São Pedro do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6689, 'PR', 'São Pedro do Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6690, 'PR', 'São Pedro Lopei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6691, 'PR', 'São Pio X');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6692, 'PR', 'São Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6693, 'PR', 'São Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6694, 'PR', 'São Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6695, 'PR', 'São Roque do Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6696, 'PR', 'São Salvador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6697, 'PR', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6698, 'PR', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6699, 'PR', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6700, 'PR', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6701, 'PR', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6702, 'PR', 'São Sebastião da Amoreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6703, 'PR', 'São Silvestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6704, 'PR', 'São Tomé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6705, 'PR', 'São Valentim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6706, 'PR', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6707, 'PR', 'Sapé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6708, 'PR', 'Sapopema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6709, 'PR', 'Sarandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6710, 'PR', 'Sarandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6711, 'PR', 'Saudade do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6712, 'PR', 'Sede Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6713, 'PR', 'Sede Chaparral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6714, 'PR', 'Sede Nova Sant Ana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6715, 'PR', 'Sede Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6716, 'PR', 'Sede Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6717, 'PR', 'Selva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6718, 'PR', 'Sengés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6719, 'PR', 'Senhor Bom Jesus dos Gramados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6720, 'PR', 'Serra dos Dourados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6721, 'PR', 'Serra Negra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6722, 'PR', 'Serranópolis do Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6723, 'PR', 'Serraria Klas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6724, 'PR', 'Serrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6726, 'PR', 'Serrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6727, 'PR', 'Sertaneja');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6728, 'PR', 'Sertanópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6729, 'PR', 'Sertãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6731, 'PR', 'Sete Saltos de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6733, 'PR', 'Siqueira Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6734, 'PR', 'Siqueira Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6735, 'PR', 'Socavão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6736, 'PR', 'Colônia Socorro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6737, 'PR', 'Sulina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6738, 'PR', 'Sumaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6739, 'PR', 'Sussui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6740, 'PR', 'Sutis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6741, 'PR', 'Taipa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6742, 'PR', 'Tamarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6743, 'PR', 'Tambarutaca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6744, 'PR', 'Tamboara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6745, 'PR', 'Tanque Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6746, 'PR', 'Tapejara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6747, 'PR', 'Tapira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6748, 'PR', 'Tapui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6749, 'PR', 'Taquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6750, 'PR', 'Taquari dos Polacos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6751, 'PR', 'Taquari dos Russos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6752, 'PR', 'Taquaruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6753, 'PR', 'Teixeira Soares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6754, 'PR', 'Telêmaco Borba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6755, 'PR', 'Teolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6756, 'PR', 'Tereza Breda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6757, 'PR', 'Tereza Cristina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6758, 'PR', 'Terra Boa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6759, 'PR', 'Terra Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6760, 'PR', 'Terra Rica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6761, 'PR', 'Terra Roxa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6762, 'PR', 'Tibagi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6763, 'PR', 'Tijucas do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6764, 'PR', 'Tijuco Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6765, 'PR', 'Tijuco Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6766, 'PR', 'Timbu Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6767, 'PR', 'Tindiquera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6768, 'PR', 'Tiradentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6769, 'PR', 'Tiradentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6770, 'PR', 'Toledo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6771, 'PR', 'Tomaz Coelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6772, 'PR', 'Tomazina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6774, 'PR', 'Três Barras do Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6775, 'PR', 'Três Bicos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6776, 'PR', 'Três Bocas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6777, 'PR', 'Três Capões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6778, 'PR', 'Três Córregos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6779, 'PR', 'Três Lagoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6780, 'PR', 'Três Pinheiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6781, 'PR', 'Três Placas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6782, 'PR', 'Triângulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6783, 'PR', 'Trindade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6784, 'PR', 'Triolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6785, 'PR', 'Tronco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6786, 'PR', 'Tunas do Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6787, 'PR', 'Tuneiras do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6788, 'PR', 'Tupãssi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6789, 'PR', 'Tupinambá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6790, 'PR', 'Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6791, 'PR', 'Ubaldino Taques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6792, 'PR', 'Ubauna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6793, 'PR', 'Ubiratã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6794, 'PR', 'Umuarama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6795, 'PR', 'União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6796, 'PR', 'União da Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6797, 'PR', 'União do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6798, 'PR', 'Uniflor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6799, 'PR', 'Uraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6801, 'PR', 'Uvaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6802, 'PR', 'Valentins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6803, 'PR', 'Valério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6804, 'PR', 'Vargeado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6805, 'PR', 'Vassoural');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6806, 'PR', 'Ventania');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6807, 'PR', 'Vera Cruz do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6808, 'PR', 'Vera Guarani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6809, 'PR', 'Verê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6810, 'PR', 'Vida Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6811, 'PR', 'Vidigal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6812, 'PR', 'Alto Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6813, 'PR', 'Vila Diniz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6814, 'PR', 'Vila dos Roldos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6815, 'PR', 'Vila Flórida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6816, 'PR', 'Vila Gandhi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6817, 'PR', 'Vila Guay');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6818, 'PR', 'Vila Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6819, 'PR', 'Vila Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6820, 'PR', 'Vila Nova de Florença');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6821, 'PR', 'Vila Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6822, 'PR', 'Vila Reis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6823, 'PR', 'Vila Rica do Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6825, 'PR', 'Vila Silva Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6826, 'PR', 'Vila Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6827, 'PR', 'Virmond');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6828, 'PR', 'Vista Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6829, 'PR', 'Vista Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6830, 'PR', 'Vista Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6831, 'PR', 'Colônia Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6832, 'PR', 'Vitorino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6833, 'PR', 'Warta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6834, 'PR', 'Wenceslau Braz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6835, 'PR', 'Xambrê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6836, 'PR', 'Xaxim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6837, 'PR', 'Yolanda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6838, 'PR', '4º Centenário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6839, 'RJ', 'Abarracamento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6840, 'RJ', 'Ilha Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6841, 'RJ', 'Afonso Arinos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6842, 'RJ', 'Agulhas Negras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6843, 'RJ', 'Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6844, 'RJ', 'Andrade Pinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6845, 'RJ', 'Angra dos Reis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6846, 'RJ', 'Anta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6847, 'RJ', 'Aperibé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6848, 'RJ', 'Araruama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6849, 'RJ', 'Areal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6850, 'RJ', 'Armação dos Búzios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6851, 'RJ', 'Arraial do Cabo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6852, 'RJ', 'Arrozal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6853, 'RJ', 'Avelar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6854, 'RJ', 'Bacaxá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6855, 'RJ', 'Baltazar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6856, 'RJ', 'Banquete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6857, 'RJ', 'Barão de Juparana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6858, 'RJ', 'Barcelos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6859, 'RJ', 'Barra Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6860, 'RJ', 'Barra de Macaé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6861, 'RJ', 'Barra de São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6862, 'RJ', 'Barra do Piraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6863, 'RJ', 'Barra Mansa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6864, 'RJ', 'Barra Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6865, 'RJ', 'Belford Roxo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6866, 'RJ', 'Bemposta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6867, 'RJ', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6868, 'RJ', 'Boa Sorte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6869, 'RJ', 'Boa Ventura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6870, 'RJ', 'Bom Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6871, 'RJ', 'Bom Jesus do Itabapoana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6872, 'RJ', 'Bom Jesus do Querendo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6873, 'RJ', 'Cabo Frio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6874, 'RJ', 'Cabuçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6875, 'RJ', 'Cachoeiras de Macacu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6876, 'RJ', 'Cachoeiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6877, 'RJ', 'Calheiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6878, 'RJ', 'Cambiasca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6879, 'RJ', 'Cambuci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6880, 'RJ', 'Campo do Coelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6881, 'RJ', 'Campos dos Goytacazes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6882, 'RJ', 'Campos Elíseos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6883, 'RJ', 'Cantagalo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6884, 'RJ', 'Carabuçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6885, 'RJ', 'Carapebus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6886, 'RJ', 'Cardoso Moreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6887, 'RJ', 'Carmo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6888, 'RJ', 'Cascatinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6889, 'RJ', 'Casimiro de Abreu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6890, 'RJ', 'Cava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6891, 'RJ', 'Coelho da Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6892, 'RJ', 'Colônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6893, 'RJ', 'Comendador Levy Gasparian');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6894, 'RJ', 'Comendador Venâncio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6895, 'RJ', 'Conceição de Jacareí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6896, 'RJ', 'Conceição de Macabu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6897, 'RJ', 'Conrado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6898, 'RJ', 'Conselheiro Paulino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6899, 'RJ', 'Conservatória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6900, 'RJ', 'Cordeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6901, 'RJ', 'Coroa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6902, 'RJ', 'Correas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6903, 'RJ', 'Córrego da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6904, 'RJ', 'Córrego do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6905, 'RJ', 'Correntezas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6906, 'RJ', 'Cunhambebe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6907, 'RJ', 'Dorândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6908, 'RJ', 'Dores de Macabu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6909, 'RJ', 'Doutor Elias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6910, 'RJ', 'Doutor Loreti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6911, 'RJ', 'Duas Barras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6912, 'RJ', 'Duque de Caxias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6913, 'RJ', 'Engenheiro Passos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6914, 'RJ', 'Engenheiro Paulo de Frontin');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6915, 'RJ', 'Engenheiro Pedreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6916, 'RJ', 'Estrada Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6917, 'RJ', 'Euclidelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6918, 'RJ', 'Falcão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6919, 'RJ', 'Floriano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6920, 'RJ', 'Fumaça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6921, 'RJ', 'Funil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6922, 'RJ', 'Gaviões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6923, 'RJ', 'Getulândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6924, 'RJ', 'Glicério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6925, 'RJ', 'Goitacazes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6926, 'RJ', 'Governador Portela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6927, 'RJ', 'Guapimirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6928, 'RJ', 'Guia de Pacobaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6929, 'RJ', 'Ibitiguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6930, 'RJ', 'Ibitioca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6931, 'RJ', 'Ibituporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6932, 'RJ', 'Iguaba Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6933, 'RJ', 'Imbariê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6934, 'RJ', 'Inconfidência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6935, 'RJ', 'Inhomirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6936, 'RJ', 'Inoã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6937, 'RJ', 'Ipiabás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6938, 'RJ', 'Ipiiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6939, 'RJ', 'Ipuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6940, 'RJ', 'Itabapoana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6941, 'RJ', 'Itaboraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6942, 'RJ', 'Itacurussá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6943, 'RJ', 'Itaguaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6944, 'RJ', 'Itaipava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6945, 'RJ', 'Itaipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6946, 'RJ', 'Itajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6947, 'RJ', 'Italva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6948, 'RJ', 'Itambi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6949, 'RJ', 'Itaocara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6950, 'RJ', 'Itaperuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6951, 'RJ', 'Itatiaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6953, 'RJ', 'Jaguarembé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6954, 'RJ', 'Jamapará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6955, 'RJ', 'Japeri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6956, 'RJ', 'Japuíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6957, 'RJ', 'Laje do Muriaé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6958, 'RJ', 'Laranjais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6959, 'RJ', 'Lídice');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6960, 'RJ', 'Lumiar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6961, 'RJ', 'Macabuzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6962, 'RJ', 'Macaé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6963, 'RJ', 'Macuco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6964, 'RJ', 'Magé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6965, 'RJ', 'Mambucaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6966, 'RJ', 'Mangaratiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6967, 'RJ', 'Maniva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6968, 'RJ', 'Manoel Ribeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6969, 'RJ', 'Manuel Duarte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6970, 'RJ', 'Marangatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6971, 'RJ', 'Maricá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6972, 'RJ', 'Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6973, 'RJ', 'Mesquita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6974, 'RJ', 'Miguel Pereira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6975, 'RJ', 'Miracema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6976, 'RJ', 'Monnerat');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6977, 'RJ', 'Monjolo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6978, 'RJ', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6979, 'RJ', 'Monte Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6980, 'RJ', 'Monumento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6981, 'RJ', 'Morangaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6982, 'RJ', 'Morro do Côco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6983, 'RJ', 'Morro Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6984, 'RJ', 'Mussurepe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6985, 'RJ', 'Natividade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6986, 'RJ', 'Neves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6987, 'RJ', 'Nhunguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6988, 'RJ', 'Nilópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6989, 'RJ', 'Niterói');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6990, 'RJ', 'Nossa Senhora da Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6991, 'RJ', 'Nossa Senhora da Penha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6992, 'RJ', 'Nossa Senhora do Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6993, 'RJ', 'Nova Friburgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6994, 'RJ', 'Nova Iguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6995, 'RJ', 'Olinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6996, 'RJ', 'Ourânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6997, 'RJ', 'Papucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6998, 'RJ', 'Paquequer Pequeno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (6999, 'RJ', 'Paracambi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7000, 'RJ', 'Paraíba do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7001, 'RJ', 'Paraíso do Tobias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7002, 'RJ', 'Paraoquena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7003, 'RJ', 'Parapeúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7004, 'RJ', 'Parati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7005, 'RJ', 'Parati Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7006, 'RJ', 'Passa Três');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7007, 'RJ', 'Paty do Alferes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7008, 'RJ', 'Pedra Selada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7009, 'RJ', 'Pedro do Rio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7010, 'RJ', 'Penedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7011, 'RJ', 'Pentagna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7012, 'RJ', 'Petrópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7013, 'RJ', 'Piabetá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7014, 'RJ', 'Pião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7015, 'RJ', 'Pinheiral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7016, 'RJ', 'Pipeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7017, 'RJ', 'Piraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7018, 'RJ', 'Pirangaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7019, 'RJ', 'Pirapetinga de Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7020, 'RJ', 'Porciúncula');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7021, 'RJ', 'Portela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7023, 'RJ', 'Porto Real');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7024, 'RJ', 'Porto Velho do Cunha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7025, 'RJ', 'Posse');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7027, 'RJ', 'Pureza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7028, 'RJ', 'Purilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7029, 'RJ', 'Quartéis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7030, 'RJ', 'Quatis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7031, 'RJ', 'Queimados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7032, 'RJ', 'Quissamã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7033, 'RJ', 'Raposo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7034, 'RJ', 'Renascença');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7035, 'RJ', 'Resende');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7036, 'RJ', 'Retiro do Muriaé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7037, 'RJ', 'Rialto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7038, 'RJ', 'Ribeirão de São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7039, 'RJ', 'Rio Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7040, 'RJ', 'Rio Claro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7041, 'RJ', 'Rio das Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7042, 'RJ', 'Rio das Ostras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7043, 'RJ', 'Rio de Janeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7044, 'RJ', 'Riograndina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7045, 'RJ', 'Rosal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7046, 'RJ', 'Sacra Família do Tinguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7047, 'RJ', 'Salutaris');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7048, 'RJ', 'Sambaetiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7049, 'RJ', 'Sampaio Correia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7050, 'RJ', 'Sana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7051, 'RJ', 'Santa Clara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7052, 'RJ', 'Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7053, 'RJ', 'Santa Cruz da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7054, 'RJ', 'Santa Isabel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7055, 'RJ', 'Santa Isabel do Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7056, 'RJ', 'Santa Maria');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 7000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7057, 'RJ', 'Santa Maria Madalena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7058, 'RJ', 'Santa Rita da Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7059, 'RJ', 'Santanésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7060, 'RJ', 'Santo Aleixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7061, 'RJ', 'Santo Amaro de Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7062, 'RJ', 'Santo Antônio de Pádua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7063, 'RJ', 'Santo Antônio do Imbé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7064, 'RJ', 'Santo Eduardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7065, 'RJ', 'São Fidélis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7066, 'RJ', 'São Francisco de Itabapoana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7067, 'RJ', 'São Gonçalo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7068, 'RJ', 'São João da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7069, 'RJ', 'São João de Meriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7070, 'RJ', 'São João do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7071, 'RJ', 'São João Marcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7072, 'RJ', 'São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7073, 'RJ', 'São José de Ubá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7074, 'RJ', 'São José do Ribeirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7075, 'RJ', 'São José do Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7076, 'RJ', 'São José do Vale do Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7077, 'RJ', 'São Mateus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7078, 'RJ', 'São Pedro da Aldeia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7079, 'RJ', 'São Sebastião de Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7080, 'RJ', 'São Sebastião do Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7081, 'RJ', 'São Sebastião do Paraíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7082, 'RJ', 'São Sebastião dos Ferreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7083, 'RJ', 'São Vicente de Paula');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7084, 'RJ', 'Sapucaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7085, 'RJ', 'Saquarema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7086, 'RJ', 'Saracuruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7087, 'RJ', 'Sebastião de Lacerda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7088, 'RJ', 'Seropédica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7089, 'RJ', 'Serrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7090, 'RJ', 'Sete Pontes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7091, 'RJ', 'Silva Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7092, 'RJ', 'Sodrelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7093, 'RJ', 'Sossego');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7094, 'RJ', 'Subaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7095, 'RJ', 'Sumidouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7096, 'RJ', 'Suruí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7097, 'RJ', 'Taboas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7098, 'RJ', 'Tamoios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7099, 'RJ', 'Tanguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7100, 'RJ', 'Tapera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7101, 'RJ', 'Tarituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7102, 'RJ', 'Teresópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7103, 'RJ', 'Tocos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7104, 'RJ', 'Trajano de Morais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7105, 'RJ', 'Travessão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7106, 'RJ', 'Três Irmãos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7107, 'RJ', 'Três Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7108, 'RJ', 'Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7109, 'RJ', 'Valão do Barro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7110, 'RJ', 'Valença');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7111, 'RJ', 'Vargem Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7112, 'RJ', 'Varre-Sai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7113, 'RJ', 'Vassouras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7114, 'RJ', 'Venda das Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7116, 'RJ', 'Vila da Grama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7118, 'RJ', 'Vila Muriqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7119, 'RJ', 'Vila Nova de Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7120, 'RJ', 'Visconde de Imbé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7121, 'RJ', 'Volta Redonda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7122, 'RJ', 'Werneck');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7123, 'RJ', 'Xerém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7124, 'RN', 'Acari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7125, 'RN', 'Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7126, 'RN', 'Afonso Bezerra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7127, 'RN', 'Água Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7128, 'RN', 'Alexandria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7129, 'RN', 'Almino Afonso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7130, 'RN', 'Alto do Rodrigues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7131, 'RN', 'Angicos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7132, 'RN', 'Antônio Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7133, 'RN', 'Apodi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7134, 'RN', 'Areia Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7135, 'RN', 'Arez');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7136, 'RN', 'Baía Formosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7137, 'RN', 'Barão de Serra Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7138, 'RN', 'Baraúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7139, 'RN', 'Barcelona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7140, 'RN', 'Belo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7141, 'RN', 'Bento Fernandes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7142, 'RN', 'Boa Saúde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7143, 'RN', 'Bodó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7144, 'RN', 'Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7145, 'RN', 'Brejinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7146, 'RN', 'Caiçara do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7147, 'RN', 'Caiçara do Rio do Vento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7148, 'RN', 'Caicó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7149, 'RN', 'Campo Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7150, 'RN', 'Campo Redondo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7151, 'RN', 'Canguaretama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7152, 'RN', 'Caraúbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7153, 'RN', 'Carnaúba dos Dantas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7154, 'RN', 'Carnaubais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7155, 'RN', 'Ceará-Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7156, 'RN', 'Cerro Corá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7157, 'RN', 'Coronel Ezequiel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7158, 'RN', 'Coronel João Pessoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7159, 'RN', 'Córrego de São Mateus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7160, 'RN', 'Cruzeta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7161, 'RN', 'Currais Novos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7162, 'RN', 'Doutor Severiano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7163, 'RN', 'Encanto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7164, 'RN', 'Equador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7165, 'RN', 'Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7166, 'RN', 'Paraú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7167, 'RN', 'Extremoz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7168, 'RN', 'Felipe Guerra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7169, 'RN', 'Fernando Pedroza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7170, 'RN', 'Firmamento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7171, 'RN', 'Florânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7172, 'RN', 'Francisco Dantas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7173, 'RN', 'Frutuoso Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7174, 'RN', 'Galinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7175, 'RN', 'Gameleira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7176, 'RN', 'Goianinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7177, 'RN', 'Governador Dix-Sept Rosado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7178, 'RN', 'Grossos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7179, 'RN', 'Guamaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7180, 'RN', 'Ielmo Marinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7181, 'RN', 'Igreja Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7182, 'RN', 'Ipanguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7183, 'RN', 'Ipiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7184, 'RN', 'Ipueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7185, 'RN', 'Itajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7186, 'RN', 'Itaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7187, 'RN', 'Jaçanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7188, 'RN', 'Jandaíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7189, 'RN', 'Janduís');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7190, 'RN', 'Japi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7191, 'RN', 'Jardim de Angicos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7192, 'RN', 'Jardim de Piranhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7193, 'RN', 'Jardim do Seridó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7194, 'RN', 'João Câmara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7195, 'RN', 'João Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7196, 'RN', 'José da Penha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7197, 'RN', 'Jucurutu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7198, 'RN', 'Jundiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7199, 'RN', 'Lagoa D Anta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7200, 'RN', 'Lagoa de Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7201, 'RN', 'Lagoa de Velhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7202, 'RN', 'Lagoa Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7203, 'RN', 'Lagoa Salgada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7204, 'RN', 'Lajes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7205, 'RN', 'Lajes Pintadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7206, 'RN', 'Lucrécia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7207, 'RN', 'Luís Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7208, 'RN', 'Macaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7209, 'RN', 'Macau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7210, 'RN', 'Major Felipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7211, 'RN', 'Major Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7212, 'RN', 'Marcelino Vieira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7213, 'RN', 'Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7214, 'RN', 'Mata de São Braz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7215, 'RN', 'Maxaranguape');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7216, 'RN', 'Messias Targino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7217, 'RN', 'Montanhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7218, 'RN', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7219, 'RN', 'Monte das Gameleiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7220, 'RN', 'Mossoró');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7221, 'RN', 'Natal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7222, 'RN', 'Nísia Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7223, 'RN', 'Nova Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7224, 'RN', 'Olho-D Água do Borges');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7225, 'RN', 'Ouro Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7226, 'RN', 'Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7227, 'RN', 'Parazinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7228, 'RN', 'Parelhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7229, 'RN', 'Parnamirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7230, 'RN', 'Passa e Fica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7231, 'RN', 'Passagem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7232, 'RN', 'Patu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7233, 'RN', 'Pau dos Ferros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7234, 'RN', 'Pedra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7235, 'RN', 'Pedra Preta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7236, 'RN', 'Pedro Avelino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7237, 'RN', 'Pedro Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7238, 'RN', 'Pendências');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7239, 'RN', 'Pilões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7240, 'RN', 'Poço Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7241, 'RN', 'Portalegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7242, 'RN', 'Porto do Mangue');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7243, 'RN', 'Pureza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7244, 'RN', 'Rafael Fernandes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7245, 'RN', 'Rafael Godeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7246, 'RN', 'Riacho da Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7247, 'RN', 'Riacho de Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7248, 'RN', 'Riachuelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7249, 'RN', 'Rio do Fogo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7250, 'RN', 'Rodolfo Fernandes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7251, 'RN', 'Rosário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7252, 'RN', 'Ruy Barbosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7253, 'RN', 'Salva Vida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7254, 'RN', 'Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7255, 'RN', 'Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7256, 'RN', 'Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7257, 'RN', 'Santa Teresa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7258, 'RN', 'Santana do Matos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7259, 'RN', 'Santana do Seridó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7260, 'RN', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7261, 'RN', 'Santo Antônio do Potengi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7262, 'RN', 'São Bento do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7263, 'RN', 'São Bento do Trairi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7264, 'RN', 'São Bernardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7265, 'RN', 'São Fernando');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7266, 'RN', 'São Francisco do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7267, 'RN', 'São Geraldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7268, 'RN', 'São Gonçalo do Amarante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7269, 'RN', 'São João do Sabugi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7270, 'RN', 'São José da Passagem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7271, 'RN', 'São José de Mipibu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7272, 'RN', 'São José do Campestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7273, 'RN', 'São José do Seridó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7274, 'RN', 'São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7275, 'RN', 'São Miguel de Touros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7276, 'RN', 'São Paulo do Potengi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7277, 'RN', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7278, 'RN', 'São Rafael');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7279, 'RN', 'São Tomé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7280, 'RN', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7281, 'RN', 'Senador Elói de Souza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7282, 'RN', 'Senador Georgino Avelino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7283, 'RN', 'Serra Caiada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7284, 'RN', 'Serra da Tapuia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7285, 'RN', 'Serra de São Bento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7286, 'RN', 'Serra do Mel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7287, 'RN', 'Serra Negra do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7288, 'RN', 'Serrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7289, 'RN', 'Serrinha dos Pintos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7290, 'RN', 'Severiano Melo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7291, 'RN', 'Sítio Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7292, 'RN', 'Taboleiro Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7293, 'RN', 'Taipu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7294, 'RN', 'Tangará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7295, 'RN', 'Tenente Ananias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7296, 'RN', 'Tenente Laurentino Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7297, 'RN', 'Tibau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7298, 'RN', 'Tibau do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7299, 'RN', 'Timbaúba dos Batistas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7300, 'RN', 'Touros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7301, 'RN', 'Trairi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7302, 'RN', 'Triunfo Potiguar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7303, 'RN', 'Umarizal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7304, 'RN', 'Upanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7305, 'RN', 'Várzea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7306, 'RN', 'Venha-Ver');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7307, 'RN', 'Vera Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7308, 'RN', 'Viçosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7309, 'RN', 'Vila Flor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7310, 'RO', 'Abunã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7311, 'RO', 'Alto Alegre dos Parecis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7312, 'RO', 'Alta Floresta D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7313, 'RO', 'Alto Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7314, 'RO', 'Alvorada D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7315, 'RO', 'Ariquemes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7316, 'RO', 'Buritis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7317, 'RO', 'Cabixi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7318, 'RO', 'Cacaulândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7319, 'RO', 'Cacoal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7320, 'RO', 'Calama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7321, 'RO', 'Campo Novo de Rondônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7322, 'RO', 'Candeias do Jamari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7323, 'RO', 'Castanheiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7324, 'RO', 'Cerejeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7325, 'RO', 'Chupinguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7326, 'RO', 'Colorado do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7327, 'RO', 'Corumbiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7328, 'RO', 'Costa Marques');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7329, 'RO', 'Cujubim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7330, 'RO', 'Espigão do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7331, 'RO', 'Governador Jorge Teixeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7332, 'RO', 'Guajará-Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7333, 'RO', 'Jaci Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7334, 'RO', 'Itapuã do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7335, 'RO', 'Jaru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7336, 'RO', 'Ji-Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7337, 'RO', 'Machadinho D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7338, 'RO', 'Marco Rondon');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7339, 'RO', 'Ministro Andreazza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7340, 'RO', 'Mirante da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7341, 'RO', 'Monte Negro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7342, 'RO', 'Nova Brasilândia D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7343, 'RO', 'Nova Mamoré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7344, 'RO', 'Nova União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7345, 'RO', 'Nova Vida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7346, 'RO', 'Novo Horizonte do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7347, 'RO', 'Ouro Preto do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7348, 'RO', 'Parecis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7349, 'RO', 'Pedras Negras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7350, 'RO', 'Pimenta Bueno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7351, 'RO', 'Pimenteiras do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7352, 'RO', 'Porto Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7353, 'RO', 'Presidente Médici');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7354, 'RO', 'Primavera de Rondônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7355, 'RO', 'Príncipe da Beira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7356, 'RO', 'Rio Crespo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7357, 'RO', 'Riozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7358, 'RO', 'Rolim de Moura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7359, 'RO', 'Santa Luzia D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7360, 'RO', 'São Felipe D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7361, 'RO', 'São Francisco do Guaporé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7362, 'RO', 'São Miguel do Guaporé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7363, 'RO', 'Seringueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7364, 'RO', 'Tabajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7365, 'RO', 'Teixeirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7366, 'RO', 'Theobroma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7367, 'RO', 'Urupá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7368, 'RO', 'Vale do Anari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7369, 'RO', 'Vale do Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7370, 'RO', 'Extrema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7371, 'RO', 'Vilhena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7372, 'RO', 'Vista Alegre do Abunã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7373, 'RR', 'Alto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7374, 'RR', 'Amajari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7375, 'RR', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7376, 'RR', 'Bonfim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7377, 'RR', 'Cantá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7378, 'RR', 'Caracaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7379, 'RR', 'Caroebe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7380, 'RR', 'Iracema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7381, 'RR', 'Mucajaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7382, 'RR', 'Normandia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7383, 'RR', 'Pacaraima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7384, 'RR', 'Rorainópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7385, 'RR', 'São João da Baliza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7386, 'RR', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7387, 'RR', 'Uiramutã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7388, 'RS', 'Aceguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7389, 'RS', 'Afonso Rodrigues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7390, 'RS', 'Água Santa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7391, 'RS', 'Águas Claras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7392, 'RS', 'Agudo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7393, 'RS', 'Ajuricaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7394, 'RS', 'Albardão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7395, 'RS', 'Alecrim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7396, 'RS', 'Alegrete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7397, 'RS', 'Alegria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7398, 'RS', 'Alfredo Brenner');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7399, 'RS', 'Almirante Tamandaré do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7400, 'RS', 'Alpestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7401, 'RS', 'Alto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7402, 'RS', 'Alto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7403, 'RS', 'Alto da União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7404, 'RS', 'Alto Feliz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7405, 'RS', 'Alto Paredão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7406, 'RS', 'Alto Recreio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7407, 'RS', 'Alto Uruguai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7408, 'RS', 'Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7409, 'RS', 'Amaral Ferrador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7410, 'RS', 'Ametista do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7411, 'RS', 'André da Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7412, 'RS', 'Anta Gorda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7413, 'RS', 'Antônio Kerpel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7414, 'RS', 'Antônio Prado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7415, 'RS', 'Arambaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7416, 'RS', 'Araricá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7417, 'RS', 'Aratiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7418, 'RS', 'Arco Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7419, 'RS', 'Arco-Íris');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7420, 'RS', 'Arroio Canoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7421, 'RS', 'Arroio do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7422, 'RS', 'Arroio do Padre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7423, 'RS', 'Arroio do Sal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7424, 'RS', 'Arroio do Só');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7425, 'RS', 'Arroio do Tigre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7426, 'RS', 'Arroio dos Ratos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7427, 'RS', 'Arroio Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7428, 'RS', 'Arroio Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7429, 'RS', 'Arroio Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7430, 'RS', 'Árvore Só');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7431, 'RS', 'Arvorezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7432, 'RS', 'Atafona');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7433, 'RS', 'Atiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7434, 'RS', 'Augusto Pestana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7435, 'RS', 'Áurea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7436, 'RS', 'Avelino Paranhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7437, 'RS', 'Azevedo Sodré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7438, 'RS', 'Bacupari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7439, 'RS', 'Bagé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7440, 'RS', 'Baliza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7441, 'RS', 'Balneário Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7442, 'RS', 'Banhado do Colégio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7443, 'RS', 'Barão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7444, 'RS', 'Barão de Cotegipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7445, 'RS', 'Barão do Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7446, 'RS', 'Barra do Guarita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7447, 'RS', 'Barra do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7448, 'RS', 'Barra do Quaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7449, 'RS', 'Barra do Ribeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7450, 'RS', 'Barra do Rio Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7451, 'RS', 'Barra Funda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7452, 'RS', 'Barracão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7453, 'RS', 'Barreirinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7454, 'RS', 'Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7455, 'RS', 'Barro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7456, 'RS', 'Barro Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7457, 'RS', 'Barro Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7458, 'RS', 'Barros Cassal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7459, 'RS', 'Basílio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7460, 'RS', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7461, 'RS', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7462, 'RS', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7463, 'RS', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7464, 'RS', 'Beluno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7465, 'RS', 'Benjamin Constant do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7466, 'RS', 'Bento Gonçalves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7467, 'RS', 'Bexiga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7468, 'RS', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7469, 'RS', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7470, 'RS', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7471, 'RS', 'Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7472, 'RS', 'Boa Vista das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7473, 'RS', 'Boa Vista do Buricá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7474, 'RS', 'Boa Vista do Cadeado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7475, 'RS', 'Boa Vista do Incra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7476, 'RS', 'Boa Vista do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7477, 'RS', 'Boca do Monte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7478, 'RS', 'Boi Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7479, 'RS', 'Bojuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7480, 'RS', 'Bom Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7481, 'RS', 'Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7482, 'RS', 'Bom Princípio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7483, 'RS', 'Bom Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7484, 'RS', 'Bom Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7485, 'RS', 'Bom Retiro do Guaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7486, 'RS', 'Bom Retiro do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7487, 'RS', 'Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7488, 'RS', 'Boqueirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7489, 'RS', 'Vila Boqueirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7490, 'RS', 'Boqueirão do Leão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7491, 'RS', 'Bororé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7492, 'RS', 'Bossoroca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7493, 'RS', 'Botucaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7494, 'RS', 'Braga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7495, 'RS', 'Brochier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7496, 'RS', 'Buriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7497, 'RS', 'Butiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7498, 'RS', 'Butiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7499, 'RS', 'Caçapava do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7500, 'RS', 'Cacequi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7501, 'RS', 'Cachoeira do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7502, 'RS', 'Cachoeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7503, 'RS', 'Cacique Doble');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7504, 'RS', 'Cadorna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7505, 'RS', 'Caibaté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7506, 'RS', 'Caiçara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7507, 'RS', 'Camaquã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7508, 'RS', 'Camargo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7509, 'RS', 'Cambará do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7510, 'RS', 'Camobi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7511, 'RS', 'Campestre Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7512, 'RS', 'Campestre da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7513, 'RS', 'Campina das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7514, 'RS', 'Campina Redonda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7515, 'RS', 'Campinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7516, 'RS', 'Campinas do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7517, 'RS', 'Campo Bom');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7518, 'RS', 'Campo Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7519, 'RS', 'Campo do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7520, 'RS', 'Campo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7521, 'RS', 'Campo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7522, 'RS', 'Campo Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7523, 'RS', 'Campo Seco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7524, 'RS', 'Campo Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7525, 'RS', 'Campos Borges');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7526, 'RS', 'Candelária');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7527, 'RS', 'Cândido Freire');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7528, 'RS', 'Cândido Godói');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7529, 'RS', 'Candiota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7530, 'RS', 'Canela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7531, 'RS', 'Canguçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7532, 'RS', 'Canhembora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7533, 'RS', 'Canoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7534, 'RS', 'Canudos do Vale');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7535, 'RS', 'Capané');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7536, 'RS', 'Capão Bonito do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7537, 'RS', 'Capão Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7538, 'RS', 'Capão Comprido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7539, 'RS', 'Capão da Canoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7540, 'RS', 'Capão da Porteira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7541, 'RS', 'Capão do Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7542, 'RS', 'Capão do Cipó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7543, 'RS', 'Capão do Leão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7544, 'RS', 'Capela de Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7545, 'RS', 'Capela Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7546, 'RS', 'Capinzal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7547, 'RS', 'Capitão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7548, 'RS', 'Capivari do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7549, 'RS', 'Capivarita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7550, 'RS', 'Capo-Erê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7551, 'RS', 'Capoeira Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7552, 'RS', 'Caraá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7553, 'RS', 'Carajá Seival');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7554, 'RS', 'Carazinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7555, 'RS', 'Carlos Barbosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7556, 'RS', 'Carlos Gomes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7557, 'RS', 'Carovi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7558, 'RS', 'Casca');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 7500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7559, 'RS', 'Cascata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7560, 'RS', 'Cascata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7561, 'RS', 'Caseiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7562, 'RS', 'Castelinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7563, 'RS', 'Catuípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7564, 'RS', 'Cavajureta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7565, 'RS', 'Caverá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7566, 'RS', 'Caxias do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7567, 'RS', 'Cazuza Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7568, 'RS', 'Cedro Marcado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7569, 'RS', 'Centenário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7570, 'RS', 'Centenário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7571, 'RS', 'Centro Linha Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7572, 'RS', 'Cerrito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7573, 'RS', 'Cerrito Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7574, 'RS', 'Cerrito do Ouro ou Vila do Cerrito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7575, 'RS', 'Cerro Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7576, 'RS', 'Cerro Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7577, 'RS', 'Cerro Claro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7578, 'RS', 'Cerro do Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7579, 'RS', 'Cerro do Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7580, 'RS', 'Cerro Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7581, 'RS', 'Cerro Grande do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7582, 'RS', 'Cerro Largo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7583, 'RS', 'Chapada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7584, 'RS', 'Chapada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7585, 'RS', 'Charqueadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7586, 'RS', 'Charrua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7587, 'RS', 'Chiapetta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7588, 'RS', 'Chicolomã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7589, 'RS', 'Chimarrão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7590, 'RS', 'Chorão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7591, 'RS', 'Chuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7592, 'RS', 'Chuvisca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7593, 'RS', 'Cidreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7594, 'RS', 'Cinqüentenário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7595, 'RS', 'Ciríaco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7596, 'RS', 'Clara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7597, 'RS', 'Clemente Argolo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7598, 'RS', 'Coimbra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7599, 'RS', 'Colinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7600, 'RS', 'Colônia das Almas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7601, 'RS', 'Colônia Medeiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7602, 'RS', 'Colônia Municipal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7603, 'RS', 'Colônia Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7604, 'RS', 'Colônia São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7605, 'RS', 'Colônia Z-3');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7606, 'RS', 'Coloninha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7607, 'RS', 'Colorado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7608, 'RS', 'Comandai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7609, 'RS', 'Condor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7610, 'RS', 'Consolata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7611, 'RS', 'Constantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7612, 'RS', 'Coqueiro Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7613, 'RS', 'Coqueiros do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7614, 'RS', 'Cordilheira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7615, 'RS', 'Coroados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7616, 'RS', 'Coronel Barros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7617, 'RS', 'Coronel Bicaco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7618, 'RS', 'Coronel Finzito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7619, 'RS', 'Coronel Pilar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7620, 'RS', 'Coronel Teixeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7621, 'RS', 'Cortado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7622, 'RS', 'Costa da Cadeia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7623, 'RS', 'Costão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7624, 'RS', 'Cotiporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7625, 'RS', 'Coxilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7626, 'RS', 'Coxilha Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7627, 'RS', 'Cr-1');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7628, 'RS', 'Crissiumal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7629, 'RS', 'Cristal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7630, 'RS', 'Cristal do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7631, 'RS', 'Criúva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7632, 'RS', 'Cruz Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7633, 'RS', 'Cruzaltense');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7634, 'RS', 'Cruzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7635, 'RS', 'Cruzeiro do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7636, 'RS', 'Curral Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7637, 'RS', 'Curumim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7638, 'RS', 'Daltro Filho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7639, 'RS', 'Daltro Filho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7640, 'RS', 'Daltro Filho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7641, 'RS', 'Dário Lassance');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7642, 'RS', 'David Canabarro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7643, 'RS', 'Delfina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7644, 'RS', 'Deodoro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7645, 'RS', 'Depósito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7646, 'RS', 'Derrubadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7647, 'RS', 'Dezesseis de Novembro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7648, 'RS', 'Dilermando de Aguiar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7649, 'RS', 'Divino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7650, 'RS', 'Dois Irmãos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7651, 'RS', 'Dois Irmãos das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7652, 'RS', 'Dois Lajeados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7653, 'RS', 'São José do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7654, 'RS', 'Dom Feliciano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7655, 'RS', 'Dom Feliciano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7656, 'RS', 'Dom Pedrito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7657, 'RS', 'Dom Pedro de Alcântara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7658, 'RS', 'Dona Francisca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7659, 'RS', 'Dona Otília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7660, 'RS', 'Dourado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7661, 'RS', 'Bozano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7662, 'RS', 'Doutor Edgardo Pereira Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7663, 'RS', 'Doutor Maurício Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7664, 'RS', 'Doutor Ricardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7665, 'RS', 'Eldorado do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7666, 'RS', 'Eletra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7667, 'RS', 'Encantado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7668, 'RS', 'Encruzilhada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7669, 'RS', 'Encruzilhada do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7670, 'RS', 'Engenho Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7671, 'RS', 'Entre Rios do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7672, 'RS', 'Entre-Ijuís');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7673, 'RS', 'Entrepelado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7674, 'RS', 'Erebango');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7675, 'RS', 'Erechim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7676, 'RS', 'Ernestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7677, 'RS', 'Ernesto Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7678, 'RS', 'Erval Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7679, 'RS', 'Erval Seco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7680, 'RS', 'Erveiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7681, 'RS', 'Esmeralda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7682, 'RS', 'Esperança do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7683, 'RS', 'Espigão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7684, 'RS', 'Espigão Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7685, 'RS', 'Espinilho Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7686, 'RS', 'Espírito Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7687, 'RS', 'Espumoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7688, 'RS', 'Esquina Araújo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7689, 'RS', 'Esquina Bom Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7690, 'RS', 'Esquina Gaúcha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7691, 'RS', 'Esquina Gaúcha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7692, 'RS', 'Esquina Ipiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7693, 'RS', 'Esquina Piratini');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7694, 'RS', 'Estação');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7695, 'RS', 'Estância Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7696, 'RS', 'Estância Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7697, 'RS', 'Esteio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7698, 'RS', 'Esteira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7699, 'RS', 'Estreito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7700, 'RS', 'Estrela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7701, 'RS', 'Estrela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7702, 'RS', 'Estrela Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7703, 'RS', 'Eugênio de Castro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7704, 'RS', 'Evangelista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7705, 'RS', 'Fagundes Varela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7706, 'RS', 'Fão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7707, 'RS', 'Faria Lemos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7708, 'RS', 'Farinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7709, 'RS', 'Farrapos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7710, 'RS', 'Farroupilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7711, 'RS', 'Faxinal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7712, 'RS', 'Faxinal do Soturno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7713, 'RS', 'Faxinalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7714, 'RS', 'Fazenda Fialho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7715, 'RS', 'Fazenda Souza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7716, 'RS', 'Fazenda Vilanova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7717, 'RS', 'Feliz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7718, 'RS', 'Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7719, 'RS', 'Flores da Cunha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7720, 'RS', 'Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7721, 'RS', 'Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7722, 'RS', 'Floriano Peixoto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7723, 'RS', 'Flórida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7724, 'RS', 'Fontoura Xavier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7725, 'RS', 'Formigueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7726, 'RS', 'Formosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7727, 'RS', 'Forninho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7728, 'RS', 'Forquetinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7729, 'RS', 'Fortaleza dos Valos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7730, 'RS', 'Frederico Westphalen');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7731, 'RS', 'Frei Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7732, 'RS', 'Freire');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7733, 'RS', 'Garibaldi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7734, 'RS', 'Garibaldina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7735, 'RS', 'Garruchos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7736, 'RS', 'Gaurama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7737, 'RS', 'General Câmara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7738, 'RS', 'Gentil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7739, 'RS', 'Getúlio Vargas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7740, 'RS', 'Giruá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7741, 'RS', 'Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7742, 'RS', 'Glorinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7743, 'RS', 'Goio-En');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7744, 'RS', 'Gramado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7745, 'RS', 'Gramado dos Loureiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7746, 'RS', 'Gramado São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7747, 'RS', 'Gramado Xavier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7748, 'RS', 'Gravataí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7749, 'RS', 'Guabiju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7750, 'RS', 'Guaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7751, 'RS', 'Guajuviras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7752, 'RS', 'Guaporé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7753, 'RS', 'Guarani das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7754, 'RS', 'Guassupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7755, 'RS', 'Harmonia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7756, 'RS', 'Herval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7757, 'RS', 'Herveiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7758, 'RS', 'Hidráulica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7759, 'RS', 'Horizontina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7760, 'RS', 'Hulha Negra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7761, 'RS', 'Humaitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7762, 'RS', 'Ibarama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7763, 'RS', 'Ibaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7764, 'RS', 'Ibiaçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7765, 'RS', 'Ibiraiaras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7766, 'RS', 'Ibirapuitã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7767, 'RS', 'Ibirubá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7768, 'RS', 'Igrejinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7769, 'RS', 'Igrejinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7770, 'RS', 'Ijucapirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7771, 'RS', 'Ijuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7772, 'RS', 'Ilha dos Marinheiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7773, 'RS', 'Ilópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7774, 'RS', 'Imbé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7775, 'RS', 'Imigrante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7776, 'RS', 'Independência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7777, 'RS', 'Inhacorá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7778, 'RS', 'Ipê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7779, 'RS', 'Ipiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7780, 'RS', 'Ipiranga do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7781, 'RS', 'Ipuaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7782, 'RS', 'Iraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7783, 'RS', 'Iruí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7784, 'RS', 'Itaara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7785, 'RS', 'Itacolomi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7786, 'RS', 'Itacurubi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7787, 'RS', 'Itacurubi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7788, 'RS', 'Itaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7789, 'RS', 'Itaimbezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7790, 'RS', 'Itão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7791, 'RS', 'Itapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7792, 'RS', 'Itapucá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7793, 'RS', 'Itapucá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7794, 'RS', 'Itaqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7795, 'RS', 'Itati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7796, 'RS', 'Itatiba do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7797, 'RS', 'Itaúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7798, 'RS', 'Ituim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7799, 'RS', 'Ivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7800, 'RS', 'Ivorá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7801, 'RS', 'Ivoti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7802, 'RS', 'Jaboticaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7803, 'RS', 'Jacuizinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7804, 'RS', 'Jacutinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7805, 'RS', 'Jaguarão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7806, 'RS', 'Jaguarete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7807, 'RS', 'Jaguari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7808, 'RS', 'Jansen');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7809, 'RS', 'Jaquirana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7810, 'RS', 'Jari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7811, 'RS', 'Jazidas ou Capela São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7812, 'RS', 'João Arregui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7813, 'RS', 'João Rodrigues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7814, 'RS', 'Joça Tavares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7815, 'RS', 'Jóia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7816, 'RS', 'José Otávio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7817, 'RS', 'Juá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7818, 'RS', 'Júlio de Castilhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7819, 'RS', 'Lagoa Bonita do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7820, 'RS', 'Lagoa dos Três Cantos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7821, 'RS', 'Lagoa Vermelha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7822, 'RS', 'Lagoão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7823, 'RS', 'Lajeado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7824, 'RS', 'Lajeado Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7825, 'RS', 'Lajeado Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7826, 'RS', 'Lajeado Cerne');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7827, 'RS', 'Lajeado do Bugre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7828, 'RS', 'Lajeado Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7829, 'RS', 'Lajeado Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7830, 'RS', 'Lara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7831, 'RS', 'Laranjeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7832, 'RS', 'Lava-Pés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7833, 'RS', 'Lavras do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7834, 'RS', 'Leonel Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7835, 'RS', 'Liberato Salzano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7836, 'RS', 'Lindolfo Collor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7837, 'RS', 'Linha Comprida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7838, 'RS', 'Linha Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7839, 'RS', 'Linha Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7840, 'RS', 'Loreto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7841, 'RS', 'Maçambará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7842, 'RS', 'Machadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7843, 'RS', 'Magistério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7844, 'RS', 'Mampituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7845, 'RS', 'Manchinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7846, 'RS', 'Mangueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7847, 'RS', 'Manoel Viana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7848, 'RS', 'Maquiné');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7849, 'RS', 'Maratá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7850, 'RS', 'Marau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7851, 'RS', 'Marcelino Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7852, 'RS', 'Marcorama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7853, 'RS', 'Mariana Pimentel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7854, 'RS', 'Mariano Moro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7855, 'RS', 'Mariante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7856, 'RS', 'Mariápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7857, 'RS', 'Marques de Souza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7858, 'RS', 'Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7859, 'RS', 'Matarazzo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7860, 'RS', 'Mato Castelhano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7861, 'RS', 'Mato Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7862, 'RS', 'Mato Leitão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7863, 'RS', 'Mato Queimado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7864, 'RS', 'Mauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7865, 'RS', 'Mauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7866, 'RS', 'Maximiliano de Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7867, 'RS', 'Medianeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7868, 'RS', 'Minas do Leão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7869, 'RS', 'Miraguaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7870, 'RS', 'Miraguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7871, 'RS', 'Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7872, 'RS', 'Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7873, 'RS', 'Montauri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7874, 'RS', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7875, 'RS', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7876, 'RS', 'Monte Alegre dos Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7877, 'RS', 'Monte Alverne');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7878, 'RS', 'Monte Belo do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7879, 'RS', 'Monte Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7880, 'RS', 'Montenegro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7881, 'RS', 'Mormaço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7882, 'RS', 'Morrinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7883, 'RS', 'Morrinhos do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7884, 'RS', 'Morro Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7885, 'RS', 'Morro Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7886, 'RS', 'Morro Redondo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7887, 'RS', 'Morro Reuter');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7888, 'RS', 'Morungava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7889, 'RS', 'Mostardas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7890, 'RS', 'Muçum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7891, 'RS', 'Muitos Capões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7892, 'RS', 'Muliterno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7893, 'RS', 'Não-Me-Toque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7894, 'RS', 'Nazaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7895, 'RS', 'Nicolau Vergueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7896, 'RS', 'Nonoai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7897, 'RS', 'Nossa Senhora Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7898, 'RS', 'Nossa Senhora da Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7899, 'RS', 'Nova Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7900, 'RS', 'Nova Araçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7901, 'RS', 'Nova Bassano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7902, 'RS', 'Nova Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7903, 'RS', 'Nova Bréscia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7904, 'RS', 'Nova Candelária');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7905, 'RS', 'Nova Esperança do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7906, 'RS', 'Nova Hartz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7907, 'RS', 'Nova Milano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7908, 'RS', 'Nova Pádua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7909, 'RS', 'Nova Palma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7910, 'RS', 'Nova Petrópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7911, 'RS', 'Nova Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7912, 'RS', 'Nova Ramada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7913, 'RS', 'Nova Roma do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7914, 'RS', 'Nova Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7915, 'RS', 'Nova Sardenha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7916, 'RS', 'Novo Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7917, 'RS', 'Novo Cabrais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7918, 'RS', 'Novo Hamburgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7919, 'RS', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7920, 'RS', 'Novo Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7921, 'RS', 'Novo Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7922, 'RS', 'Novo Tiradentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7923, 'RS', 'Vila Oliva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7924, 'RS', 'Oralina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7925, 'RS', 'Osório');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7926, 'RS', 'Osvaldo Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7927, 'RS', 'Osvaldo Kroeff');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7928, 'RS', 'Otávio Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7929, 'RS', 'Pacheca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7930, 'RS', 'Padilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7931, 'RS', 'Padre Gonzales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7932, 'RS', 'Paim Filho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7933, 'RS', 'Palmares do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7934, 'RS', 'Palmas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7935, 'RS', 'Palmeira das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7936, 'RS', 'Palmitinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7937, 'RS', 'Pampeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7938, 'RS', 'Panambi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7939, 'RS', 'Pântano Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7940, 'RS', 'Paraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7941, 'RS', 'Paraíso do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7942, 'RS', 'Pareci Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7943, 'RS', 'Parobé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7944, 'RS', 'Passa Sete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7945, 'RS', 'Passinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7946, 'RS', 'Passo Burmann');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7947, 'RS', 'Passo da Areia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7948, 'RS', 'Passo da Caveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7949, 'RS', 'Passo das Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7950, 'RS', 'Passo do Adão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7951, 'RS', 'Passo do Sabão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7952, 'RS', 'Passo do Sobrado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7953, 'RS', 'Passo Fundo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7954, 'RS', 'Passo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7955, 'RS', 'Passo Raso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7956, 'RS', 'Paulo Bento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7957, 'RS', 'Pavão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7958, 'RS', 'Paverama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7959, 'RS', 'Pedras Altas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7960, 'RS', 'Pedreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7961, 'RS', 'Pedro Garcia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7962, 'RS', 'Pedro Osório');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7963, 'RS', 'Pedro Paiva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7964, 'RS', 'Pejuçara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7965, 'RS', 'Pelotas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7966, 'RS', 'Picada Café');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7967, 'RS', 'Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7968, 'RS', 'Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7969, 'RS', 'Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7970, 'RS', 'Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7971, 'RS', 'Pinhal Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7972, 'RS', 'Pinhal da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7973, 'RS', 'Pinhal Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7974, 'RS', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7975, 'RS', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7976, 'RS', 'Pinheirinho do Vale');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7977, 'RS', 'Pinheiro Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7978, 'RS', 'Pinheiro Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7979, 'RS', 'Pinheiro Marcado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7980, 'RS', 'Pinto Bandeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7981, 'RS', 'Piraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7982, 'RS', 'Pirapó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7983, 'RS', 'Piratini');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7984, 'RS', 'Pitanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7985, 'RS', 'Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7986, 'RS', 'Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7987, 'RS', 'Plano Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7988, 'RS', 'Poço das Antas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7989, 'RS', 'Polígono do Erval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7990, 'RS', 'Pólo Petroquímico de Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7991, 'RS', 'Pontão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7992, 'RS', 'Ponte Preta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7993, 'RS', 'Portão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7994, 'RS', 'Porto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7995, 'RS', 'Porto Batista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7996, 'RS', 'Porto Lucena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7997, 'RS', 'Porto Mauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7998, 'RS', 'Porto Vera Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (7999, 'RS', 'Porto Xavier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8000, 'RS', 'Pouso Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8001, 'RS', 'Povo Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8002, 'RS', 'Povoado Tozzo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8003, 'RS', 'Pranchada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8004, 'RS', 'Pratos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8005, 'RS', 'Presidente Lucena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8006, 'RS', 'Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8007, 'RS', 'Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8008, 'RS', 'Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8009, 'RS', 'Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8010, 'RS', 'Protásio Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8011, 'RS', 'Pulador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8012, 'RS', 'Putinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8013, 'RS', 'Quaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8014, 'RS', 'Quaraim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8015, 'RS', 'Quatro Irmãos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8016, 'RS', 'Quevedos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8017, 'RS', 'Quilombo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8018, 'RS', 'Quinta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8019, 'RS', 'Quintão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8020, 'RS', 'Quinze de Novembro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8021, 'RS', 'Quitéria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8022, 'RS', 'Rancho Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8023, 'RS', 'Redentora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8024, 'RS', 'Refugiado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8025, 'RS', 'Relvado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8026, 'RS', 'Restinga Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8027, 'RS', 'Restinga Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8028, 'RS', 'Rincão de São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8029, 'RS', 'Rincão Del Rei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8030, 'RS', 'Rincão do Cristóvão Pereira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8031, 'RS', 'Rincão do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8032, 'RS', 'Rincão do Segredo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8033, 'RS', 'Rincão Doce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8034, 'RS', 'Rincão dos Kroeff');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8035, 'RS', 'Rincão dos Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8036, 'RS', 'Rincão Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8037, 'RS', 'Rio Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8038, 'RS', 'Rio Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8039, 'RS', 'Rio da Ilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8040, 'RS', 'Rio dos Índios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8041, 'RS', 'Rio Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8042, 'RS', 'Rio Pardinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8043, 'RS', 'Rio Pardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8044, 'RS', 'Rio Telha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8045, 'RS', 'Rio Tigre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8046, 'RS', 'Rio Toldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8047, 'RS', 'Riozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8048, 'RS', 'Roca Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8049, 'RS', 'Rodeio Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8050, 'RS', 'Rolador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8051, 'RS', 'Rolante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8052, 'RS', 'Rolantinho da Figueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8053, 'RS', 'Ronda Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8054, 'RS', 'Rondinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8055, 'RS', 'Roque Gonzales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8056, 'RS', 'Rosário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8057, 'RS', 'Rosário do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8058, 'RS', 'Sagrada Família');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 8000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8059, 'RS', 'Saicã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8060, 'RS', 'Saldanha Marinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8061, 'RS', 'Saltinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8062, 'RS', 'Saltinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8063, 'RS', 'Salto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8064, 'RS', 'Salto do Jacuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8065, 'RS', 'Salvador das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8066, 'RS', 'Salvador do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8067, 'RS', 'Sananduva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8068, 'RS', 'Sant auta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8069, 'RS', 'Santa Bárbara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8070, 'RS', 'Santa Bárbara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8071, 'RS', 'Santa Bárbara do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8072, 'RS', 'Santa Catarina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8073, 'RS', 'Santa Cecília do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8074, 'RS', 'Santa Clara do Ingaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8075, 'RS', 'Santa Clara do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8076, 'RS', 'Santa Cristina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8077, 'RS', 'Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8078, 'RS', 'Santa Cruz da Concórdia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8079, 'RS', 'Santa Cruz do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8080, 'RS', 'Santa Flora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8081, 'RS', 'Santa Inês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8082, 'RS', 'Santa Izabel do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8083, 'RS', 'Santa Lúcia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8084, 'RS', 'Santa Lúcia do Piaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8085, 'RS', 'Santa Luíza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8086, 'RS', 'Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8087, 'RS', 'Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8088, 'RS', 'Santa Maria do Herval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8089, 'RS', 'Santa Rita do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8090, 'RS', 'Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8091, 'RS', 'Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8092, 'RS', 'Santa Silvana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8093, 'RS', 'Santa Teresinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8094, 'RS', 'Santa Tereza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8095, 'RS', 'Santa Tereza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8096, 'RS', 'Santa Vitória do Palmar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8097, 'RS', 'Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8098, 'RS', 'Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8099, 'RS', 'Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8100, 'RS', 'Santana da Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8101, 'RS', 'Santana do Livramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8102, 'RS', 'Santiago');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8103, 'RS', 'Santo Amaro do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8104, 'RS', 'Santo Ângelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8105, 'RS', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8106, 'RS', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8107, 'RS', 'Santo Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8108, 'RS', 'Santo Antônio da Patrulha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8109, 'RS', 'Santo Antônio das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8110, 'RS', 'Santo Antônio de Castro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8111, 'RS', 'Santo Antônio do Bom Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8112, 'RS', 'Santo Antônio do Palma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8113, 'RS', 'Santo Antônio do Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8114, 'RS', 'Santo Augusto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8115, 'RS', 'Santo Cristo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8116, 'RS', 'Santo Expedito do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8117, 'RS', 'Santo Inácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8118, 'RS', 'São Bento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8119, 'RS', 'São Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8120, 'RS', 'São Borja');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8121, 'RS', 'São Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8122, 'RS', 'São Domingos do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8123, 'RS', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8124, 'RS', 'São Francisco de Assis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8125, 'RS', 'São Francisco de Paula');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8126, 'RS', 'São Gabriel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8127, 'RS', 'São Jerônimo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8128, 'RS', 'São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8129, 'RS', 'São João Batista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8130, 'RS', 'São João Bosco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8131, 'RS', 'São João da Urtiga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8132, 'RS', 'São João do Polesine');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8133, 'RS', 'São Jorge');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8134, 'RS', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8135, 'RS', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8136, 'RS', 'São José da Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8137, 'RS', 'São José das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8138, 'RS', 'São José de Castro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8139, 'RS', 'São José do Centro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8140, 'RS', 'São José do Herval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8141, 'RS', 'São José do Hortêncio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8142, 'RS', 'São José do Inhacorá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8143, 'RS', 'São José do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8144, 'RS', 'São José do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8145, 'RS', 'São José dos Ausentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8146, 'RS', 'São Leopoldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8147, 'RS', 'São Lourenço das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8148, 'RS', 'São Lourenço do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8149, 'RS', 'São Luís Rei');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8150, 'RS', 'São Luiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8151, 'RS', 'São Luiz Gonzaga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8152, 'RS', 'São Manuel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8153, 'RS', 'São Marcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8154, 'RS', 'São Marcos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8155, 'RS', 'São Martinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8156, 'RS', 'São Martinho da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8157, 'RS', 'São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8158, 'RS', 'São Miguel das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8159, 'RS', 'São Nicolau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8160, 'RS', 'São Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8161, 'RS', 'São Paulo das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8162, 'RS', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8163, 'RS', 'São Pedro das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8164, 'RS', 'São Pedro da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8165, 'RS', 'São Pedro do Butiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8166, 'RS', 'São Pedro do Iraxim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8167, 'RS', 'São Pedro do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8168, 'RS', 'São Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8169, 'RS', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8170, 'RS', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8171, 'RS', 'São Sebastião do Caí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8172, 'RS', 'São Sepé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8173, 'RS', 'São Simão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8174, 'RS', 'São Valentim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8175, 'RS', 'São Valentim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8176, 'RS', 'São Valentim do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8177, 'RS', 'São Valério do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8178, 'RS', 'São Vendelino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8179, 'RS', 'São Vicente do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8180, 'RS', 'Sapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8181, 'RS', 'Sapucaia do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8182, 'RS', 'Sarandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8183, 'RS', 'Scharlau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8184, 'RS', 'Seberi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8185, 'RS', 'Vila Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8186, 'RS', 'Sede Aurora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8187, 'RS', 'Sede Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8188, 'RS', 'Segredo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8189, 'RS', 'Segredo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8190, 'RS', 'Seival');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8191, 'RS', 'Selbach');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8192, 'RS', 'Senador Salgado Filho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8193, 'RS', 'Sentinela do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8194, 'RS', 'Serafim Schmidt');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8195, 'RS', 'Serafina Corrêa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8196, 'RS', 'Sério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8197, 'RS', 'Serra dos Gregórios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8198, 'RS', 'Serrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8199, 'RS', 'Sertão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8200, 'RS', 'Sertão Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8201, 'RS', 'Sertãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8202, 'RS', 'Sete de Setembro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8203, 'RS', 'Sete de Setembro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8204, 'RS', 'Sete de Setembro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8205, 'RS', 'Sete Lagoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8206, 'RS', 'Severiano de Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8207, 'RS', 'Silva Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8208, 'RS', 'Silveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8209, 'RS', 'Silveira Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8210, 'RS', 'Sinimbu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8211, 'RS', 'Sírio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8212, 'RS', 'Sítio Gabriel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8213, 'RS', 'Sobradinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8214, 'RS', 'Soledade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8215, 'RS', 'Souza Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8216, 'RS', 'Suspiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8217, 'RS', 'Tabaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8218, 'RS', 'Tabajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8219, 'RS', 'Taim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8220, 'RS', 'Tainhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8221, 'RS', 'Tamanduá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8222, 'RS', 'Tanque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8223, 'RS', 'Tapejara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8224, 'RS', 'Tapera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8225, 'RS', 'Tapera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8226, 'RS', 'Tapes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8227, 'RS', 'Taquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8228, 'RS', 'Taquari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8229, 'RS', 'Taquarichim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8230, 'RS', 'Taquaruçu do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8231, 'RS', 'Tavares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8232, 'RS', 'Tenente Portela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8233, 'RS', 'Terra de Areia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8234, 'RS', 'Tesouras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8235, 'RS', 'Teutônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8236, 'RS', 'Tiaraju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8237, 'RS', 'Timbaúva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8238, 'RS', 'Tiradentes do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8239, 'RS', 'Toropi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8240, 'RS', 'Toroquá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8241, 'RS', 'Torquato Severo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8242, 'RS', 'Torres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8243, 'RS', 'Torrinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8244, 'RS', 'Touro Passo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8245, 'RS', 'Tramandaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8246, 'RS', 'Travesseiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8247, 'RS', 'Trentin');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8248, 'RS', 'Três Arroios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8249, 'RS', 'Três Barras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8250, 'RS', 'Três Cachoeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8251, 'RS', 'Três Coroas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8252, 'RS', 'Três de Maio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8253, 'RS', 'Três Forquilhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8254, 'RS', 'Três Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8255, 'RS', 'Três Passos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8256, 'RS', 'Três Vendas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8257, 'RS', 'Trindade do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8258, 'RS', 'Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8259, 'RS', 'Tronqueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8260, 'RS', 'Tucunduva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8261, 'RS', 'Tuiuti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8262, 'RS', 'Tunas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8263, 'RS', 'Túnel Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8264, 'RS', 'Tupanci do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8265, 'RS', 'Tupanciretã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8266, 'RS', 'Tupancy ou Vila Block');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8267, 'RS', 'Tupandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8268, 'RS', 'Tupantuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8269, 'RS', 'Tuparendi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8270, 'RS', 'Tupi Silveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8271, 'RS', 'Tupinambá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8272, 'RS', 'Turuçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8273, 'RS', 'Turvinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8274, 'RS', 'Ubiretama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8275, 'RS', 'Umbu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8276, 'RS', 'União da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8277, 'RS', 'Unistalda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8278, 'RS', 'Uruguaiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8279, 'RS', 'Vacacai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8280, 'RS', 'Vacaria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8281, 'RS', 'Valdástico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8282, 'RS', 'Vale do Rio Cai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8283, 'RS', 'Vale do Sol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8284, 'RS', 'Vale Real');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8285, 'RS', 'Vale Veneto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8286, 'RS', 'Vale Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8287, 'RS', 'Vanini');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8288, 'RS', 'Venâncio Aires');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8289, 'RS', 'Vera Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8290, 'RS', 'Veranópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8291, 'RS', 'Vertentes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8292, 'RS', 'Vespasiano Correa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8293, 'RS', 'Viadutos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8294, 'RS', 'Viamão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8295, 'RS', 'Vicente Dutra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8296, 'RS', 'Victor Graeff');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8297, 'RS', 'Vila Bender');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8298, 'RS', 'Vila Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8299, 'RS', 'Vila Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8300, 'RS', 'Vila Langaro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8301, 'RS', 'Vila Laranjeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8302, 'RS', 'Vila Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8303, 'RS', 'Vila Nova do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8304, 'RS', 'Vila Rica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8305, 'RS', 'Vila Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8306, 'RS', 'Vila Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8307, 'RS', 'Vista Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8308, 'RS', 'Vista Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8309, 'RS', 'Vista Alegre do Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8310, 'RS', 'Vista Gaúcha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8311, 'RS', 'Vitória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8312, 'RS', 'Vitória das Missões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8313, 'RS', 'Volta Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8314, 'RS', 'Volta Fechada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8315, 'RS', 'Volta Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8316, 'RS', 'Xadrez');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8317, 'RS', 'Xangri-Lá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8318, 'RS', 'Novo Xingu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8319, 'SC', 'Abdon Batista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8320, 'SC', 'Abelardo Luz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8321, 'SC', 'Agrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8322, 'SC', 'Agronômica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8323, 'SC', 'Água Doce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8324, 'SC', 'Águas Brancas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8325, 'SC', 'Águas Claras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8326, 'SC', 'Águas de Chapecó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8327, 'SC', 'Águas Frias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8328, 'SC', 'Águas Mornas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8329, 'SC', 'Aguti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8330, 'SC', 'Aiurê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8331, 'SC', 'Alfredo Wagner');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8332, 'SC', 'Alto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8333, 'SC', 'Alto Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8334, 'SC', 'Alto da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8335, 'SC', 'Anchieta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8336, 'SC', 'Angelina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8337, 'SC', 'Anita Garibaldi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8338, 'SC', 'Anitápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8339, 'SC', 'Anta Gorda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8340, 'SC', 'Antônio Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8341, 'SC', 'Apiúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8342, 'SC', 'Arabutã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8343, 'SC', 'Araquari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8344, 'SC', 'Araranguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8345, 'SC', 'Armazém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8346, 'SC', 'Arnópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8347, 'SC', 'Arroio Trinta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8348, 'SC', 'Arvoredo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8349, 'SC', 'Ascurra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8350, 'SC', 'Atalanta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8351, 'SC', 'Aterrado Torto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8352, 'SC', 'Aurora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8353, 'SC', 'Azambuja');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8354, 'SC', 'Baia Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8355, 'SC', 'Balneário Arroio do Silva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8356, 'SC', 'Balneário Barra do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8357, 'SC', 'Balneário Camboriú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8358, 'SC', 'Balneário Gaivota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8359, 'SC', 'Balneário Morro dos Conventos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8360, 'SC', 'Bandeirante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8361, 'SC', 'Barra Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8362, 'SC', 'Barra Clara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8363, 'SC', 'Barra da Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8364, 'SC', 'Barra da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8365, 'SC', 'Barra Fria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8366, 'SC', 'Barra Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8367, 'SC', 'Barra Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8368, 'SC', 'Barreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8369, 'SC', 'Barro Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8370, 'SC', 'Bateias de Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8371, 'SC', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8372, 'SC', 'Bela Vista do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8373, 'SC', 'Bela Vista do Toldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8374, 'SC', 'Belmonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8375, 'SC', 'Benedito Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8376, 'SC', 'Biguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8377, 'SC', 'Blumenau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8378, 'SC', 'Bocaína do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8379, 'SC', 'Boiteuxburgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8380, 'SC', 'Bom Jardim da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8381, 'SC', 'Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8382, 'SC', 'Bom Jesus do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8383, 'SC', 'Bom Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8384, 'SC', 'Bom Sucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8385, 'SC', 'Bombinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8386, 'SC', 'Botuverá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8387, 'SC', 'Braço do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8388, 'SC', 'Braço do Trombudo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8389, 'SC', 'Brunópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8390, 'SC', 'Brusque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8391, 'SC', 'Caçador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8392, 'SC', 'Cachoeira de Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8393, 'SC', 'Cachoeira do Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8394, 'SC', 'Caibi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8395, 'SC', 'Calmon');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8396, 'SC', 'Camboriú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8397, 'SC', 'Cambuinzal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8398, 'SC', 'Campeche');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8399, 'SC', 'Campinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8400, 'SC', 'Campo Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8401, 'SC', 'Campo Belo do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8402, 'SC', 'Campo Erê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8403, 'SC', 'Campos Novos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8404, 'SC', 'Canasvieiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8405, 'SC', 'Canelinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8406, 'SC', 'Canoas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8407, 'SC', 'Canoinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8408, 'SC', 'Capão Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8409, 'SC', 'Capinzal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8410, 'SC', 'Capivari de Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8411, 'SC', 'Caraíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8412, 'SC', 'Catanduvas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8413, 'SC', 'Catuíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8414, 'SC', 'Caxambu do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8415, 'SC', 'Cedro Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8416, 'SC', 'Celso Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8417, 'SC', 'Cerro Negro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8418, 'SC', 'Chapadão do Lageado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8419, 'SC', 'Chapecó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8420, 'SC', 'Claraíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8421, 'SC', 'Cocal do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8422, 'SC', 'Colônia Santa Tereza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8424, 'SC', 'Concórdia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8425, 'SC', 'Cordilheira Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8426, 'SC', 'Coronel Freitas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8427, 'SC', 'Coronel Martins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8428, 'SC', 'Correia Pinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8429, 'SC', 'Corupá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8430, 'SC', 'Criciúma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8431, 'SC', 'Cunha Porã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8432, 'SC', 'Cunhataí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8433, 'SC', 'Curitibanos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8434, 'SC', 'Dal Pai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8435, 'SC', 'Dalbérgia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8436, 'SC', 'Descanso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8437, 'SC', 'Dionísio Cerqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8438, 'SC', 'Dona Emma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8439, 'SC', 'Doutor Pedrinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8440, 'SC', 'Engenho Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8441, 'SC', 'Enseada de Brito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8442, 'SC', 'Entre Rios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8443, 'SC', 'Ermo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8444, 'SC', 'Erval Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8445, 'SC', 'Espinilho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8446, 'SC', 'Estação Cocal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8447, 'SC', 'Faxinal dos Guedes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8448, 'SC', 'Fazenda Zandavalli');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8449, 'SC', 'Felipe Schmidt');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8450, 'SC', 'Figueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8451, 'SC', 'Flor do Sertão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8452, 'SC', 'Florianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8453, 'SC', 'Formosa do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8454, 'SC', 'Forquilhinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8455, 'SC', 'Fragosos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8456, 'SC', 'Fraiburgo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8457, 'SC', 'Frederico Wastner');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8458, 'SC', 'Frei Rogério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8459, 'SC', 'Galvão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8460, 'SC', 'Garcia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8461, 'SC', 'Garopaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8462, 'SC', 'Garuva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8463, 'SC', 'Gaspar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8464, 'SC', 'Goio-En');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8465, 'SC', 'Governador Celso Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8466, 'SC', 'Grão Pará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8467, 'SC', 'Grápia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8468, 'SC', 'Gravatal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8469, 'SC', 'Guabiruba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8470, 'SC', 'Guaporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8471, 'SC', 'Guaraciaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8472, 'SC', 'Guaramirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8473, 'SC', 'Guarujá do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8474, 'SC', 'Guatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8475, 'SC', 'Guatambú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8476, 'SC', 'Hercílio Luz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8477, 'SC', 'Herciliópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8478, 'SC', 'Herval D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8479, 'SC', 'Ibiam');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8480, 'SC', 'Ibicaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8481, 'SC', 'Ibicuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8482, 'SC', 'Ibirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8483, 'SC', 'Içara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8484, 'SC', 'Ilhota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8485, 'SC', 'Imaruí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8486, 'SC', 'Imbituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8487, 'SC', 'Imbuia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8488, 'SC', 'Indaial');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8489, 'SC', 'Índios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8490, 'SC', 'Ingleses do Rio Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8491, 'SC', 'Invernada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8492, 'SC', 'Iomerê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8493, 'SC', 'Ipira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8494, 'SC', 'Ipoméia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8495, 'SC', 'Iporã do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8496, 'SC', 'Ipuaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8497, 'SC', 'Ipumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8498, 'SC', 'Iraceminha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8499, 'SC', 'Irakitan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8500, 'SC', 'Irani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8501, 'SC', 'Iraputã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8502, 'SC', 'Irati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8503, 'SC', 'Irineópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8504, 'SC', 'Itá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8505, 'SC', 'Itaió');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8506, 'SC', 'Itaiópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8507, 'SC', 'Itajaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8508, 'SC', 'Itajubá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8509, 'SC', 'Itapema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8510, 'SC', 'Itapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8511, 'SC', 'Itapoá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8512, 'SC', 'Itapocu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8513, 'SC', 'Itoupava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8514, 'SC', 'Ituporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8515, 'SC', 'Jaborá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8516, 'SC', 'Jacinto Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8517, 'SC', 'Jaguaruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8518, 'SC', 'Jaraguá do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8519, 'SC', 'Jardinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8520, 'SC', 'Joaçaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8521, 'SC', 'Joinville');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8522, 'SC', 'José Boiteux');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8523, 'SC', 'Jupiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8524, 'SC', 'Lacerdópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8525, 'SC', 'Lages');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8526, 'SC', 'Lagoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8527, 'SC', 'Lagoa da Estiva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8528, 'SC', 'Laguna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8529, 'SC', 'Lajeado Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8530, 'SC', 'Laurentino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8531, 'SC', 'Lauro Müller');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8532, 'SC', 'Leão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8533, 'SC', 'Lebon Régis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8534, 'SC', 'Leoberto Leal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8535, 'SC', 'Lindóia do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8536, 'SC', 'Linha das Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8537, 'SC', 'Lontras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8538, 'SC', 'Lourdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8539, 'SC', 'Luiz Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8540, 'SC', 'Luzerna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8541, 'SC', 'Machados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8542, 'SC', 'Macieira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8543, 'SC', 'Mafra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8544, 'SC', 'Major Gercino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8545, 'SC', 'Major Vieira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8546, 'SC', 'Maracajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8547, 'SC', 'Marari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8548, 'SC', 'Maratá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8549, 'SC', 'Maravilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8550, 'SC', 'Marcílio Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8551, 'SC', 'Marechal Bormann');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8552, 'SC', 'Marema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8553, 'SC', 'Mariflor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8554, 'SC', 'Marombas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8555, 'SC', 'Marombas Bossardi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8556, 'SC', 'Massaranduba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8557, 'SC', 'Matos Costa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8558, 'SC', 'Meleiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8559, 'SC', 'Mirador');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 8500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8560, 'SC', 'Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8561, 'SC', 'Mirim Doce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8562, 'SC', 'Modelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8563, 'SC', 'Mondaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8564, 'SC', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8565, 'SC', 'Monte Carlo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8566, 'SC', 'Monte Castelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8567, 'SC', 'Morro Chato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8568, 'SC', 'Morro da Fumaça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8570, 'SC', 'Morro Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8571, 'SC', 'Navegantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8572, 'SC', 'Nossa Senhora de Caravaggio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8573, 'SC', 'Nova Cultura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8574, 'SC', 'Nova Erechim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8575, 'SC', 'Nova Guarita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8576, 'SC', 'Nova Itaberaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8577, 'SC', 'Nova Petrópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8578, 'SC', 'Nova Teutônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8579, 'SC', 'Nova Trento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8580, 'SC', 'Nova Veneza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8581, 'SC', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8582, 'SC', 'Orleans');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8583, 'SC', 'Otacílio Costa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8584, 'SC', 'Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8585, 'SC', 'Ouro Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8586, 'SC', 'Ouro Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8587, 'SC', 'Paial');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8588, 'SC', 'Painel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8589, 'SC', 'Palhoça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8590, 'SC', 'Palma Sola');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8591, 'SC', 'Palmeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8592, 'SC', 'Palmitos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8593, 'SC', 'Pântano do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8594, 'SC', 'Papanduva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8595, 'SC', 'Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8596, 'SC', 'Passo de Torres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8597, 'SC', 'Passo Manso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8598, 'SC', 'Passos Maia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8599, 'SC', 'Paula Pereira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8600, 'SC', 'Paulo Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8601, 'SC', 'Pedras Grandes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8602, 'SC', 'Penha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8603, 'SC', 'Pericó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8604, 'SC', 'Peritiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8605, 'SC', 'Pescaria Brava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8606, 'SC', 'Petrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8607, 'SC', 'Balneário Piçarras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8608, 'SC', 'Pindotiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8609, 'SC', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8610, 'SC', 'Pinheiral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8611, 'SC', 'Pinheiro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8612, 'SC', 'Pinheiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8613, 'SC', 'Pirabeiraba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8614, 'SC', 'Piratuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8615, 'SC', 'Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8616, 'SC', 'Planalto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8617, 'SC', 'Poço Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8618, 'SC', 'Pomerode');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8619, 'SC', 'Ponte Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8620, 'SC', 'Ponte Alta do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8621, 'SC', 'Ponte Serrada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8622, 'SC', 'Porto Belo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8623, 'SC', 'Porto União');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8624, 'SC', 'Pouso Redondo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8625, 'SC', 'Praia Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8626, 'SC', 'Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8627, 'SC', 'Presidente Castelo Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8628, 'SC', 'Presidente Getúlio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8629, 'SC', 'Presidente Juscelino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8630, 'SC', 'Presidente Kennedy');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8631, 'SC', 'Presidente Nereu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8632, 'SC', 'Princesa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8633, 'SC', 'Quarta Linha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8634, 'SC', 'Quilombo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8635, 'SC', 'Quilômetro Doze');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8636, 'SC', 'Rancho Queimado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8637, 'SC', 'Ratones');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8638, 'SC', 'Residência Fuck');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8639, 'SC', 'Ribeirão da Ilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8640, 'SC', 'Ribeirão Pequeno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8641, 'SC', 'Rio Antinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8642, 'SC', 'Rio Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8643, 'SC', 'Rio D Una');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8644, 'SC', 'Rio da Anta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8645, 'SC', 'Rio da Luz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8646, 'SC', 'Rio das Antas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8647, 'SC', 'Rio das Furnas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8648, 'SC', 'Rio do Campo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8649, 'SC', 'Rio do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8650, 'SC', 'Rio do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8651, 'SC', 'Rio dos Bugres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8652, 'SC', 'Rio dos Cedros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8653, 'SC', 'Rio Fortuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8654, 'SC', 'Rio Maina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8655, 'SC', 'Rio Negrinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8656, 'SC', 'Rio Preto do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8657, 'SC', 'Rio Rufino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8658, 'SC', 'Riqueza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8659, 'SC', 'Rodeio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8660, 'SC', 'Romelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8661, 'SC', 'Sai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8662, 'SC', 'Salete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8663, 'SC', 'Saltinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8664, 'SC', 'Salto Veloso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8665, 'SC', 'Sanga da Toca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8666, 'SC', 'Sangão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8667, 'SC', 'Santa Cecília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8668, 'SC', 'Santa Cruz do Timbó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8669, 'SC', 'Santa Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8670, 'SC', 'Santa Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8671, 'SC', 'Santa Izabel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8672, 'SC', 'Santa Lúcia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8673, 'SC', 'Santa Lúcia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8674, 'SC', 'Santa Maria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8675, 'SC', 'Santa Rosa de Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8676, 'SC', 'Santa Rosa do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8677, 'SC', 'Santa Terezinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8678, 'SC', 'Santa Terezinha do Progresso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8679, 'SC', 'Santa Terezinha do Salto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8680, 'SC', 'Santiago do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8681, 'SC', 'Santo Amaro da Imperatriz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8682, 'SC', 'Santo Antônio de Lisboa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8683, 'SC', 'São Bento Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8684, 'SC', 'São Bento do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8685, 'SC', 'São Bernardino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8686, 'SC', 'São Bonifácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8687, 'SC', 'São Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8688, 'SC', 'São Cristóvão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8689, 'SC', 'São Cristóvão do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8690, 'SC', 'São Defende');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8691, 'SC', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8692, 'SC', 'São Francisco do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8693, 'SC', 'São Gabriel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8694, 'SC', 'São João Batista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8695, 'SC', 'São João do Itaperiú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8696, 'SC', 'São João do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8697, 'SC', 'São João do Rio Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8698, 'SC', 'São João do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8699, 'SC', 'São Joaquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8700, 'SC', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8701, 'SC', 'São José do Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8702, 'SC', 'São José do Cerrito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8703, 'SC', 'São José do Laranjal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8704, 'SC', 'São Leonardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8705, 'SC', 'São Lourenço do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8706, 'SC', 'São Ludgero');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8707, 'SC', 'São Martinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8708, 'SC', 'São Miguel do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8709, 'SC', 'São Miguel da Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8710, 'SC', 'São Miguel da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8711, 'SC', 'São Pedro de Alcântara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8712, 'SC', 'São Pedro Tobias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8713, 'SC', 'São Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8714, 'SC', 'São Sebastião do Arvoredo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8715, 'SC', 'São Sebastião do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8716, 'SC', 'Sapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8717, 'SC', 'Saudades');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8718, 'SC', 'Schroeder');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8719, 'SC', 'Seara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8720, 'SC', 'Sede Oldemburg');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8721, 'SC', 'Serra Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8723, 'SC', 'Siderópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8724, 'SC', 'Sombrio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8725, 'SC', 'Sorocaba do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8726, 'SC', 'Sul Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8727, 'SC', 'Taió');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8728, 'SC', 'Tangará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8729, 'SC', 'Taquara Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8730, 'SC', 'Taquaras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8731, 'SC', 'Tigipió');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8732, 'SC', 'Tigrinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8733, 'SC', 'Tijucas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8734, 'SC', 'Timbé do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8735, 'SC', 'Timbó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8736, 'SC', 'Timbó Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8737, 'SC', 'Três Barras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8738, 'SC', 'Treviso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8739, 'SC', 'Treze de Maio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8740, 'SC', 'Treze Tílias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8741, 'SC', 'Trombudo Central');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8742, 'SC', 'Tubarão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8743, 'SC', 'Tunápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8744, 'SC', 'Tupitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8745, 'SC', 'Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8746, 'SC', 'União do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8747, 'SC', 'Urubici');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8748, 'SC', 'Uruguai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8749, 'SC', 'Urupema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8750, 'SC', 'Urussanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8751, 'SC', 'Vargeão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8752, 'SC', 'Vargem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8753, 'SC', 'Vargem Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8754, 'SC', 'Vargem do Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8755, 'SC', 'Vidal Ramos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8756, 'SC', 'Videira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8757, 'SC', 'Vila Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8758, 'SC', 'Vila de Volta Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8759, 'SC', 'Vila Milani');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8760, 'SC', 'Vila Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8761, 'SC', 'Vítor Meireles');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8762, 'SC', 'Witmarsum');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8763, 'SC', 'Xanxerê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8764, 'SC', 'Xavantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8765, 'SC', 'Xaxim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8766, 'SC', 'Zortéa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8767, 'SE', 'Altos Verdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8768, 'SE', 'Amparo de São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8769, 'SE', 'Aquidabã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8770, 'SE', 'Aracaju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8771, 'SE', 'Arauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8772, 'SE', 'Areia Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8773, 'SE', 'Areia Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8774, 'SE', 'Barra dos Coqueiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8775, 'SE', 'Barracas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8776, 'SE', 'Boquim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8777, 'SE', 'Brejo Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8778, 'SE', 'Campo do Brito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8779, 'SE', 'Canhoba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8780, 'SE', 'Canindé de São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8781, 'SE', 'Capela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8782, 'SE', 'Carira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8783, 'SE', 'Carmópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8784, 'SE', 'Cedro de São João');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8785, 'SE', 'Cristinápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8786, 'SE', 'Cumbe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8787, 'SE', 'Divina Pastora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8788, 'SE', 'Estância');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8789, 'SE', 'Feira Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8790, 'SE', 'Frei Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8791, 'SE', 'Gararu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8792, 'SE', 'General Maynard');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8793, 'SE', 'Graccho Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8794, 'SE', 'Ilha das Flores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8795, 'SE', 'Indiaroba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8796, 'SE', 'Itabaiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8797, 'SE', 'Itabaianinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8798, 'SE', 'Itabi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8799, 'SE', 'Itaporanga D Ajuda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8800, 'SE', 'Japaratuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8801, 'SE', 'Japoatã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8802, 'SE', 'Lagarto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8803, 'SE', 'Lagoa Funda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8804, 'SE', 'Laranjeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8805, 'SE', 'Macambira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8806, 'SE', 'Malhada dos Bois');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8807, 'SE', 'Malhador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8808, 'SE', 'Maruim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8809, 'SE', 'Miranda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8810, 'SE', 'Moita Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8811, 'SE', 'Monte Alegre de Sergipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8812, 'SE', 'Mosqueiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8813, 'SE', 'Muribeca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8814, 'SE', 'Neópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8815, 'SE', 'Nossa Senhora Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8816, 'SE', 'Nossa Senhora da Glória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8817, 'SE', 'Nossa Senhora das Dores');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8818, 'SE', 'Nossa Senhora de Lourdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8819, 'SE', 'Nossa Senhora do Socorro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8820, 'SE', 'Pacatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8821, 'SE', 'Palmares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8822, 'SE', 'Pedra Mole');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8823, 'SE', 'Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8824, 'SE', 'Pedrinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8825, 'SE', 'Pinhão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8826, 'SE', 'Pirambu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8827, 'SE', 'Poço Redondo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8828, 'SE', 'Poço Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8829, 'SE', 'Porto da Folha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8830, 'SE', 'Propriá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8831, 'SE', 'Riachão do Dantas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8832, 'SE', 'Riachuelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8833, 'SE', 'Ribeirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8834, 'SE', 'Rosário do Catete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8835, 'SE', 'Salgado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8836, 'SE', 'Samambaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8837, 'SE', 'Santa Luzia do Itanhy');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8838, 'SE', 'Santa Rosa de Lima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8839, 'SE', 'Santana do São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8840, 'SE', 'Santo Amaro das Brotas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8841, 'SE', 'São Cristóvão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8842, 'SE', 'São Domingos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8843, 'SE', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8844, 'SE', 'São José');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8845, 'SE', 'São Mateus da Palestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8846, 'SE', 'São Miguel do Aleixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8847, 'SE', 'Simão Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8848, 'SE', 'Siriri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8849, 'SE', 'Telha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8850, 'SE', 'Tobias Barreto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8851, 'SE', 'Tomar do Geru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8852, 'SE', 'Umbaúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8853, 'SP', 'Adamantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8854, 'SP', 'Adolfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8855, 'SP', 'Agisse');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8856, 'SP', 'Água Vermelha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8857, 'SP', 'Aguaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8858, 'SP', 'Águas da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8859, 'SP', 'Águas de Lindóia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8860, 'SP', 'Águas de Santa Bárbara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8861, 'SP', 'Águas de São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8862, 'SP', 'Agudos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8863, 'SP', 'Agulha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8864, 'SP', 'Ajapi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8865, 'SP', 'Alambari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8866, 'SP', 'Alberto Moreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8867, 'SP', 'Aldeia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8868, 'SP', 'Aldeia de Carapicuíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8869, 'SP', 'Alfredo Guedes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8870, 'SP', 'Alfredo Marcondes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8871, 'SP', 'Altair');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8872, 'SP', 'Altinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8873, 'SP', 'Alto Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8874, 'SP', 'Alto Porã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8875, 'SP', 'Alumínio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8876, 'SP', 'Álvares Florence');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8877, 'SP', 'Álvares Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8878, 'SP', 'Álvaro de Carvalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8879, 'SP', 'Alvinlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8880, 'SP', 'Amadeu Amaral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8881, 'SP', 'Amandaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8882, 'SP', 'Ameliópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8883, 'SP', 'Americana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8884, 'SP', 'Américo Brasiliense');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8885, 'SP', 'Américo de Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8886, 'SP', 'Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8887, 'SP', 'Ana Dias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8888, 'SP', 'Analândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8889, 'SP', 'Anápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8890, 'SP', 'Andes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8891, 'SP', 'Andradina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8892, 'SP', 'Angatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8893, 'SP', 'Anhembi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8894, 'SP', 'Anhumas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8896, 'SP', 'Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8897, 'SP', 'Aparecida D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8898, 'SP', 'Aparecida de Monte Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8899, 'SP', 'Aparecida de São Manuel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8900, 'SP', 'Aparecida do Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8901, 'SP', 'Apiaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8902, 'SP', 'Apiaí-Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8903, 'SP', 'Arabela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8904, 'SP', 'Aracaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8905, 'SP', 'Araçaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8906, 'SP', 'Araçariguama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8907, 'SP', 'Araçatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8908, 'SP', 'Araçoiaba da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8909, 'SP', 'Aramina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8910, 'SP', 'Arandu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8911, 'SP', 'Arapeí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8912, 'SP', 'Araraquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8913, 'SP', 'Araras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8914, 'SP', 'Araxás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8915, 'SP', 'Arcadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8916, 'SP', 'Arco-Íris');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8917, 'SP', 'Arealva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8918, 'SP', 'Areias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8919, 'SP', 'Areiópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8920, 'SP', 'Ariranha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8921, 'SP', 'Ariri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8922, 'SP', 'Ártemis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8923, 'SP', 'Artur Nogueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8924, 'SP', 'Arujá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8925, 'SP', 'Aspásia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8926, 'SP', 'Assis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8927, 'SP', 'Assistência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8928, 'SP', 'Atibaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8929, 'SP', 'Atlântida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8930, 'SP', 'Auriflama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8931, 'SP', 'Avaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8932, 'SP', 'Avanhandava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8933, 'SP', 'Avaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8934, 'SP', 'Avencas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8935, 'SP', 'Bacaetava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8936, 'SP', 'Bacuriti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8937, 'SP', 'Bady Bassitt');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8938, 'SP', 'Baguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8939, 'SP', 'Bairro Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8940, 'SP', 'Balbinos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8941, 'SP', 'Bálsamo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8942, 'SP', 'Bananal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8943, 'SP', 'Bandeirantes D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8944, 'SP', 'Barão Ataliba Nogueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8945, 'SP', 'Barão de Antonina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8946, 'SP', 'Barão de Geraldo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8947, 'SP', 'Barbosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8948, 'SP', 'Bariri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8949, 'SP', 'Barra Bonita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8950, 'SP', 'Barra do Chapéu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8951, 'SP', 'Barra do Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8952, 'SP', 'Barra Dourada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8953, 'SP', 'Barrânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8954, 'SP', 'Barretos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8955, 'SP', 'Barrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8956, 'SP', 'Barueri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8957, 'SP', 'Bastos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8958, 'SP', 'Batatais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8959, 'SP', 'Batatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8960, 'SP', 'Batista Botelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8961, 'SP', 'Bauru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8962, 'SP', 'Bebedouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8963, 'SP', 'Bela Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8964, 'SP', 'Bela Vista São-Carlense');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8965, 'SP', 'Bento de Abreu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8966, 'SP', 'Bernardino de Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8967, 'SP', 'Bertioga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8968, 'SP', 'Bilac');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8969, 'SP', 'Birigüi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8970, 'SP', 'Biritiba-Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8971, 'SP', 'Biritiba-Ussu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8972, 'SP', 'Boa Esperança do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8973, 'SP', 'Boa Vista dos Andradas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8974, 'SP', 'Boa Vista Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8975, 'SP', 'Bocaina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8976, 'SP', 'Bofete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8977, 'SP', 'Boituva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8978, 'SP', 'Bom Fim do Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8979, 'SP', 'Bom Jesus dos Perdões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8980, 'SP', 'Bom Retiro da Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8981, 'SP', 'Bom Sucesso de Itararé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8982, 'SP', 'Bonfim Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8983, 'SP', 'Borá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8984, 'SP', 'Boracéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8985, 'SP', 'Borborema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8986, 'SP', 'Borebi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8987, 'SP', 'Botafogo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8988, 'SP', 'Botelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8989, 'SP', 'Botucatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8990, 'SP', 'Botujuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8991, 'SP', 'Braço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8992, 'SP', 'Bragança Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8993, 'SP', 'Brás Cubas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8994, 'SP', 'Brasitânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8995, 'SP', 'Braúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8996, 'SP', 'Brejo Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8997, 'SP', 'Brodowski');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8998, 'SP', 'Brotas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (8999, 'SP', 'Bueno de Andrada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9000, 'SP', 'Buri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9001, 'SP', 'Buritama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9002, 'SP', 'Buritizal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9003, 'SP', 'Cabrália Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9004, 'SP', 'Cabreúva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9005, 'SP', 'Caçapava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9006, 'SP', 'Cachoeira de Emas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9007, 'SP', 'Cachoeira Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9008, 'SP', 'Caconde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9009, 'SP', 'Cafelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9010, 'SP', 'Cafesópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9011, 'SP', 'Caiabu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9012, 'SP', 'Caibura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9013, 'SP', 'Caieiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9014, 'SP', 'Caiuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9015, 'SP', 'Cajamar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9016, 'SP', 'Cajati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9017, 'SP', 'Cajobi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9018, 'SP', 'Cajuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9019, 'SP', 'Cambaquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9020, 'SP', 'Cambaratiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9021, 'SP', 'Campestrinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9022, 'SP', 'Campina de Fora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9023, 'SP', 'Campina do Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9024, 'SP', 'Campinal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9025, 'SP', 'Campinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9026, 'SP', 'Campo Limpo Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9027, 'SP', 'Campos de Cunha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9028, 'SP', 'Campos do Jordão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9029, 'SP', 'Campos Novos Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9030, 'SP', 'Cananéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9031, 'SP', 'Canas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9032, 'SP', 'Candia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9033, 'SP', 'Cândido Mota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9034, 'SP', 'Cândido Rodrigues');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9035, 'SP', 'Canguera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9036, 'SP', 'Canitar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9037, 'SP', 'Capão Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9038, 'SP', 'Capela do Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9039, 'SP', 'Capivari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9040, 'SP', 'Capivari da Mata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9041, 'SP', 'Caporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9042, 'SP', 'Capuava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9043, 'SP', 'Caraguatatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9044, 'SP', 'Carapicuíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9045, 'SP', 'Cardeal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9046, 'SP', 'Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9047, 'SP', 'Nova Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9048, 'SP', 'Caruara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9049, 'SP', 'Casa Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9050, 'SP', 'Cássia dos Coqueiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9051, 'SP', 'Castilho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9052, 'SP', 'Catanduva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9053, 'SP', 'Catiguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9054, 'SP', 'Catucaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9055, 'SP', 'Caucaia do Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9056, 'SP', 'Cedral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9057, 'SP', 'Cerqueira César');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9058, 'SP', 'Cerquilho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9059, 'SP', 'Cesário Lange');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9060, 'SP', 'Cezar de Souza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9061, 'SP', 'Charqueada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9062, 'SP', 'Chavantes');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 9000,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9063, 'SP', 'Cipó-Guaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9064, 'SP', 'Clarinia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9065, 'SP', 'Clementina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9066, 'SP', 'Cocaes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9067, 'SP', 'Colina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9068, 'SP', 'Colômbia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9069, 'SP', 'Conceição de Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9070, 'SP', 'Conchal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9071, 'SP', 'Conchas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9072, 'SP', 'Cordeirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9073, 'SP', 'Coroados');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9074, 'SP', 'Coronel Goulart');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9075, 'SP', 'Coronel Macedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9076, 'SP', 'Corredeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9077, 'SP', 'Córrego Rico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9078, 'SP', 'Corumbataí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9079, 'SP', 'Cosmópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9080, 'SP', 'Cosmorama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9081, 'SP', 'Costa Machado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9082, 'SP', 'Cotia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9083, 'SP', 'Cravinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9084, 'SP', 'Cristais Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9085, 'SP', 'Cruz das Posses');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9086, 'SP', 'Cruzália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9087, 'SP', 'Cruzeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9088, 'SP', 'Cubatão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9089, 'SP', 'Cuiabá Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9090, 'SP', 'Cunha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9091, 'SP', 'Curupá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9092, 'SP', 'Dalas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9093, 'SP', 'Descalvado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9094, 'SP', 'Diadema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9095, 'SP', 'Dirce Reis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9096, 'SP', 'Dirceu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9097, 'SP', 'Divinolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9098, 'SP', 'Dobrada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9099, 'SP', 'Dois Córregos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9100, 'SP', 'Dolcinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9101, 'SP', 'Domélia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9102, 'SP', 'Dourado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9103, 'SP', 'Dracena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9104, 'SP', 'Duartina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9105, 'SP', 'Dumont');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9106, 'SP', 'Duplo Céu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9107, 'SP', 'Echaporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9108, 'SP', 'Eldorado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9109, 'SP', 'Eleutério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9110, 'SP', 'Elias Fausto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9111, 'SP', 'Elisiário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9112, 'SP', 'Embaúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9113, 'SP', 'Embu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9114, 'SP', 'Embu-Guaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9115, 'SP', 'Emilianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9116, 'SP', 'Eneida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9117, 'SP', 'Engenheiro Balduíno');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9118, 'SP', 'Engenheiro Coelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9119, 'SP', 'Engenheiro Maia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9120, 'SP', 'Engenheiro Schmitt');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9121, 'SP', 'Esmeralda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9122, 'SP', 'Esperança D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9123, 'SP', 'Espigão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9124, 'SP', 'Espírito Santo do Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9125, 'SP', 'Espírito Santo do Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9126, 'SP', 'Estiva Gerbi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9127, 'SP', 'Estrela D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9128, 'SP', 'Estrela do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9129, 'SP', 'Euclides da Cunha Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9130, 'SP', 'Eugênio de Melo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9131, 'SP', 'Fartura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9132, 'SP', 'Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9133, 'SP', 'Fátima Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9134, 'SP', 'Fazenda Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9135, 'SP', 'Fernando Prestes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9136, 'SP', 'Fernandópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9137, 'SP', 'Fernão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9138, 'SP', 'Ferraz de Vasconcelos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9139, 'SP', 'Flora Rica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9140, 'SP', 'Floreal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9141, 'SP', 'Floresta do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9142, 'SP', 'Flórida Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9143, 'SP', 'Florínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9144, 'SP', 'Franca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9145, 'SP', 'Francisco Morato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9146, 'SP', 'Franco da Rocha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9147, 'SP', 'Frutal do Campo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9148, 'SP', 'Gabriel Monteiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9149, 'SP', 'Gália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9150, 'SP', 'Garça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9151, 'SP', 'Gardênia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9152, 'SP', 'Gastão Vidigal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9153, 'SP', 'Gavião Peixoto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9154, 'SP', 'General Salgado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9155, 'SP', 'Getulina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9156, 'SP', 'Glicério');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9157, 'SP', 'Gramadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9158, 'SP', 'Guachos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9159, 'SP', 'Guaianas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9160, 'SP', 'Guaiçara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9161, 'SP', 'Guaimbê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9162, 'SP', 'Guaíra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9163, 'SP', 'Guamium');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9164, 'SP', 'Guapiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9165, 'SP', 'Guapiara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9166, 'SP', 'Guapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9167, 'SP', 'Guará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9168, 'SP', 'Guaraçaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9169, 'SP', 'Guaraci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9170, 'SP', 'Guaraciaba D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9171, 'SP', 'Guarani D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9172, 'SP', 'Guarantã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9173, 'SP', 'Guarapiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9174, 'SP', 'Guarapuá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9175, 'SP', 'Guararapes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9176, 'SP', 'Guararema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9177, 'SP', 'Guaratinguetá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9178, 'SP', 'Guareí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9179, 'SP', 'Guariba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9180, 'SP', 'Guariroba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9181, 'SP', 'Guarizinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9182, 'SP', 'Guarujá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9183, 'SP', 'Guarulhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9184, 'SP', 'Guatapará');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9185, 'SP', 'Guzolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9186, 'SP', 'Herculândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9187, 'SP', 'Holambra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9188, 'SP', 'Holambra II');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9189, 'SP', 'Hortolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9190, 'SP', 'Iacanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9191, 'SP', 'Iacri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9192, 'SP', 'Iaras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9193, 'SP', 'Ibaté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9194, 'SP', 'Ibiporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9195, 'SP', 'Ibirá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9196, 'SP', 'Ibirarema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9197, 'SP', 'Ibitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9198, 'SP', 'Ibitiruna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9199, 'SP', 'Ibitiúva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9200, 'SP', 'Ibitu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9201, 'SP', 'Ibiúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9202, 'SP', 'Icém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9203, 'SP', 'Ida Iolanda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9204, 'SP', 'Iepê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9205, 'SP', 'Igaçaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9206, 'SP', 'Igaraçu do Tietê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9207, 'SP', 'Igaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9208, 'SP', 'Igarapava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9209, 'SP', 'Igaratá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9210, 'SP', 'Iguape');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9211, 'SP', 'Ilha Comprida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9212, 'SP', 'Ilha Diana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9213, 'SP', 'Ilha Solteira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9214, 'SP', 'Ilhabela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9215, 'SP', 'Indaiá do Aguapeí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9216, 'SP', 'Indaiatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9217, 'SP', 'Indiana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9218, 'SP', 'Indiaporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9219, 'SP', 'Ingás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9220, 'SP', 'Inúbia Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9221, 'SP', 'Ipaussu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9222, 'SP', 'Iperó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9223, 'SP', 'Ipeúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9224, 'SP', 'Ipiguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9225, 'SP', 'Iporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9226, 'SP', 'Ipuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9227, 'SP', 'Iracemápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9228, 'SP', 'Irapé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9229, 'SP', 'Irapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9230, 'SP', 'Irapuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9231, 'SP', 'Itaberá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9232, 'SP', 'Itaboa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9233, 'SP', 'Itaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9234, 'SP', 'Itaiúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9235, 'SP', 'Itajobi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9236, 'SP', 'Itaju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9237, 'SP', 'Itanhaém');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9238, 'SP', 'Itaóca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9239, 'SP', 'Itapecerica da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9240, 'SP', 'Itapetininga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9241, 'SP', 'Itapeuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9242, 'SP', 'Itapeva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9243, 'SP', 'Itapevi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9244, 'SP', 'Itapira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9245, 'SP', 'Itapirapuã Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9246, 'SP', 'Itápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9247, 'SP', 'Itaporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9248, 'SP', 'Itapuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9249, 'SP', 'Itapura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9250, 'SP', 'Itaquaquecetuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9251, 'SP', 'Itaqueri da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9252, 'SP', 'Itararé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9253, 'SP', 'Itariri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9254, 'SP', 'Itatiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9255, 'SP', 'Itatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9256, 'SP', 'Itirapina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9257, 'SP', 'Itirapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9258, 'SP', 'Itobi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9259, 'SP', 'Itororó do Paranapanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9260, 'SP', 'Itu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9261, 'SP', 'Itupeva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9262, 'SP', 'Ituverava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9263, 'SP', 'Iubatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9264, 'SP', 'Jaborandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9265, 'SP', 'Jaboticabal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9266, 'SP', 'Jacaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9267, 'SP', 'Jacareí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9268, 'SP', 'Jaci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9269, 'SP', 'Jaciporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9270, 'SP', 'Jacuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9271, 'SP', 'Jacupiranga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9272, 'SP', 'Jafa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9273, 'SP', 'Jaguariúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9274, 'SP', 'Jales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9275, 'SP', 'Jamaica');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9276, 'SP', 'Jambeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9277, 'SP', 'Jandira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9278, 'SP', 'Jardim Belval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9279, 'SP', 'Jardim Presidente Dutra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9280, 'SP', 'Jardim Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9281, 'SP', 'Jardim Silveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9282, 'SP', 'Jardinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9283, 'SP', 'Jarinu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9284, 'SP', 'Jatobá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9285, 'SP', 'Jaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9286, 'SP', 'Jeriquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9287, 'SP', 'Joanópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9288, 'SP', 'João Ramalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9289, 'SP', 'Joaquim Egídio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9290, 'SP', 'Jordanésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9291, 'SP', 'José Bonifácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9292, 'SP', 'Juliânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9293, 'SP', 'Júlio Mesquita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9294, 'SP', 'Jumirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9295, 'SP', 'Jundiaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9296, 'SP', 'Jundiapeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9297, 'SP', 'Junqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9298, 'SP', 'Junqueirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9299, 'SP', 'Juquiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9300, 'SP', 'Juquiratiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9301, 'SP', 'Juquitiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9302, 'SP', 'Juritis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9303, 'SP', 'Jurucê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9304, 'SP', 'Jurupeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9305, 'SP', 'Jurupema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9306, 'SP', 'Lácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9307, 'SP', 'Lagoa Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9308, 'SP', 'Lagoa Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9309, 'SP', 'Lagoinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9310, 'SP', 'Laranjal Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9311, 'SP', 'Laras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9312, 'SP', 'Lauro Penteado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9313, 'SP', 'Lavínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9314, 'SP', 'Lavrinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9315, 'SP', 'Leme');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9316, 'SP', 'Lençóis Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9317, 'SP', 'Limeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9318, 'SP', 'Lindóia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9319, 'SP', 'Lins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9320, 'SP', 'Lobo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9321, 'SP', 'Lorena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9322, 'SP', 'Lourdes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9323, 'SP', 'Louveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9324, 'SP', 'Lucélia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9325, 'SP', 'Lucianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9326, 'SP', 'Luís Antônio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9327, 'SP', 'Luiziânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9328, 'SP', 'Lupércio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9329, 'SP', 'Lusitânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9330, 'SP', 'Lutécia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9331, 'SP', 'Macatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9332, 'SP', 'Macaubal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9333, 'SP', 'Macedônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9334, 'SP', 'Macucos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9335, 'SP', 'Mágda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9336, 'SP', 'Mailasqui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9337, 'SP', 'Mairinque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9338, 'SP', 'Mairiporã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9339, 'SP', 'Major Prado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9340, 'SP', 'Manduri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9341, 'SP', 'Mangaratú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9342, 'SP', 'Marabá Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9343, 'SP', 'Maracaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9344, 'SP', 'Marapoama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9345, 'SP', 'Marcondésia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9346, 'SP', 'Maresias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9347, 'SP', 'Mariápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9348, 'SP', 'Marília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9349, 'SP', 'Marinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9350, 'SP', 'Maristela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9351, 'SP', 'Martim Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9352, 'SP', 'Martinho Prado Júnior');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9353, 'SP', 'Martinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9354, 'SP', 'Matão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9355, 'SP', 'Mauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9356, 'SP', 'Mendonça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9357, 'SP', 'Meridiano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9358, 'SP', 'Mesópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9359, 'SP', 'Miguelópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9360, 'SP', 'Mineiros do Tietê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9361, 'SP', 'Mira Estrela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9362, 'SP', 'Miracatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9363, 'SP', 'Miraluz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9364, 'SP', 'Mirandópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9365, 'SP', 'Mirante do Paranapanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9366, 'SP', 'Mirassol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9367, 'SP', 'Mirassolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9368, 'SP', 'Mococa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9369, 'SP', 'Mogi das Cruzes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9370, 'SP', 'Mogi Guaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9371, 'SP', 'Mogi Mirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9372, 'SP', 'Mombuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9373, 'SP', 'Monções');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9374, 'SP', 'Mongaguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9375, 'SP', 'Montalvão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9376, 'SP', 'Monte Alegre do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9377, 'SP', 'Monte Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9378, 'SP', 'Monte Aprazível');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9379, 'SP', 'Monte Azul Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9380, 'SP', 'Monte Cabrão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9381, 'SP', 'Monte Castelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9382, 'SP', 'Monte Mor');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9383, 'SP', 'Monte Verde Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9384, 'SP', 'Monteiro Lobato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9385, 'SP', 'Moreira César');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9386, 'SP', 'Morro Agudo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9387, 'SP', 'Morro do Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9388, 'SP', 'Morungaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9389, 'SP', 'Mostardas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9390, 'SP', 'Motuca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9391, 'SP', 'Mourão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9392, 'SP', 'Murutinga do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9393, 'SP', 'Nantes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9394, 'SP', 'Narandiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9395, 'SP', 'Natividade da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9396, 'SP', 'Nazaré Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9397, 'SP', 'Neves Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9398, 'SP', 'Nhandeara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9399, 'SP', 'Nipoã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9400, 'SP', 'Nogueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9401, 'SP', 'Nossa Senhora do Remédio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9402, 'SP', 'Nova Alexandria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9403, 'SP', 'Nova Aliança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9404, 'SP', 'Nova América');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9405, 'SP', 'Nova Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9406, 'SP', 'Nova Campina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9407, 'SP', 'Nova Canaã Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9408, 'SP', 'Nova Castilho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9409, 'SP', 'Nova Europa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9410, 'SP', 'Nova Granada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9411, 'SP', 'Nova Guataporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9412, 'SP', 'Nova Independência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9413, 'SP', 'Nova Itapirema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9414, 'SP', 'Nova Luzitânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9415, 'SP', 'Nova Odessa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9416, 'SP', 'Nova Pátria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9417, 'SP', 'Nova Veneza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9418, 'SP', 'Novais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9419, 'SP', 'Novo Cravinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9420, 'SP', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9421, 'SP', 'Nuporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9422, 'SP', 'Oásis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9423, 'SP', 'Ocauçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9424, 'SP', 'Óleo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9425, 'SP', 'Olímpia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9426, 'SP', 'Oliveira Barros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9427, 'SP', 'Onda Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9428, 'SP', 'Onda Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9429, 'SP', 'Oriente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9430, 'SP', 'Orindiúva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9431, 'SP', 'Orlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9432, 'SP', 'Osasco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9433, 'SP', 'Oscar Bressane');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9434, 'SP', 'Osvaldo Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9435, 'SP', 'Ourinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9436, 'SP', 'Ouro Fino Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9437, 'SP', 'Ouro Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9438, 'SP', 'Ouroeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9439, 'SP', 'Pacaembu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9440, 'SP', 'Padre Nóbrega');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9441, 'SP', 'Palestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9442, 'SP', 'Palmares Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9443, 'SP', 'Palmeira D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9444, 'SP', 'Palmeiras de São Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9445, 'SP', 'Palmital');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9446, 'SP', 'Panorama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9447, 'SP', 'Paraguaçu Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9448, 'SP', 'Paraibuna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9449, 'SP', 'Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9450, 'SP', 'Paraisolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9451, 'SP', 'Paranabi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9452, 'SP', 'Paranapanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9453, 'SP', 'Paranapiacaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9454, 'SP', 'Paranapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9455, 'SP', 'Parapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9456, 'SP', 'Pardinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9457, 'SP', 'Pariquera-Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9458, 'SP', 'Parisi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9459, 'SP', 'Parnaso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9460, 'SP', 'Parque Meia Lua');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9461, 'SP', 'Paruru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9462, 'SP', 'Patrocínio Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9463, 'SP', 'Paulicéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9464, 'SP', 'Paulínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9465, 'SP', 'Paulistânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9466, 'SP', 'Paulo de Faria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9467, 'SP', 'Paulópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9468, 'SP', 'Pederneiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9469, 'SP', 'Pedra Bela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9470, 'SP', 'Pedra Branca de Itararé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9471, 'SP', 'Pedranópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9472, 'SP', 'Pedregulho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9473, 'SP', 'Pedreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9474, 'SP', 'Pedrinhas Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9475, 'SP', 'Pedro Barros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9476, 'SP', 'Pedro de Toledo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9477, 'SP', 'Penápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9478, 'SP', 'Pereira Barreto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9479, 'SP', 'Pereiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9480, 'SP', 'Peruíbe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9481, 'SP', 'Piacatu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9482, 'SP', 'Picinguaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9483, 'SP', 'Piedade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9484, 'SP', 'Pilar do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9485, 'SP', 'Pindamonhangaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9486, 'SP', 'Pindorama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9487, 'SP', 'Pinhalzinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9488, 'SP', 'Pinheiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9489, 'SP', 'Pioneiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9490, 'SP', 'Piquerobi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9491, 'SP', 'Piquete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9492, 'SP', 'Piracaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9493, 'SP', 'Piracicaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9494, 'SP', 'Piraju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9495, 'SP', 'Pirajuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9496, 'SP', 'Pirambóia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9497, 'SP', 'Pirangi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9498, 'SP', 'Pirapitingui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9499, 'SP', 'Pirapora do Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9500, 'SP', 'Pirapozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9501, 'SP', 'Pirassununga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9502, 'SP', 'Piratininga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9503, 'SP', 'Pitangueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9504, 'SP', 'Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9505, 'SP', 'Planalto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9506, 'SP', 'Planalto do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9507, 'SP', 'Platina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9508, 'SP', 'Poá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9509, 'SP', 'Poloni');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9510, 'SP', 'Polvilho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9511, 'SP', 'Pompéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9512, 'SP', 'Pongaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9513, 'SP', 'Pontal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9514, 'SP', 'Pontalinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9515, 'SP', 'Pontes Gestal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9516, 'SP', 'Populina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9517, 'SP', 'Porangaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9518, 'SP', 'Porto Feliz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9519, 'SP', 'Porto Ferreira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9520, 'SP', 'Porto Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9521, 'SP', 'Potim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9522, 'SP', 'Potirendaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9523, 'SP', 'Potunduva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9524, 'SP', 'Pracinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9525, 'SP', 'Pradínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9526, 'SP', 'Pradópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9527, 'SP', 'Praia Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9528, 'SP', 'Pratânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9529, 'SP', 'Presidente Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9530, 'SP', 'Presidente Bernardes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9531, 'SP', 'Presidente Epitácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9532, 'SP', 'Presidente Prudente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9533, 'SP', 'Presidente Venceslau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9534, 'SP', 'Primavera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9535, 'SP', 'Promissão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9536, 'SP', 'Prudêncio de Morais');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9537, 'SP', 'Quadra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9538, 'SP', 'Quatá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9539, 'SP', 'Queiroz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9540, 'SP', 'Queluz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9541, 'SP', 'Quintana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9542, 'SP', 'Quiririm');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9543, 'SP', 'Rafard');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9544, 'SP', 'Rancharia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9545, 'SP', 'Rechan');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9546, 'SP', 'Redenção da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9547, 'SP', 'Regente Feijó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9548, 'SP', 'Reginópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9549, 'SP', 'Registro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9550, 'SP', 'Restinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9551, 'SP', 'Riacho Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9552, 'SP', 'Ribeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9553, 'SP', 'Ribeirão Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9554, 'SP', 'Ribeirão Branco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9555, 'SP', 'Ribeirão Corrente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9556, 'SP', 'Ribeirão do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9557, 'SP', 'Ribeirão dos Índios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9558, 'SP', 'Ribeirão Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9559, 'SP', 'Ribeirão Pires');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9560, 'SP', 'Ribeirão Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9561, 'SP', 'Ribeiro do Vale');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9562, 'SP', 'Ribeiro dos Santos');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 9500,500)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9563, 'SP', 'Rifaina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9564, 'SP', 'Rincão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9565, 'SP', 'Rinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9566, 'SP', 'Rio Claro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9567, 'SP', 'Rio das Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9568, 'SP', 'Rio Grande da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9569, 'SP', 'Riolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9570, 'SP', 'Riversul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9571, 'SP', 'Roberto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9572, 'SP', 'Rosália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9573, 'SP', 'Rosana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9574, 'SP', 'Roseira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9575, 'SP', 'Rubiácea');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9576, 'SP', 'Rubião Júnior');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9577, 'SP', 'Rubinéia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9578, 'SP', 'Ruilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9579, 'SP', 'Sabaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9580, 'SP', 'Sabino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9581, 'SP', 'Sagres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9582, 'SP', 'Sales');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9583, 'SP', 'Sales Oliveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9584, 'SP', 'Salesópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9585, 'SP', 'Salmourão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9586, 'SP', 'Saltinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9587, 'SP', 'Salto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9588, 'SP', 'Salto de Pirapora');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9589, 'SP', 'Salto do Avanhandava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9590, 'SP', 'Salto Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9591, 'SP', 'Sandovalina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9592, 'SP', 'Santa Adélia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9593, 'SP', 'Santa Albertina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9594, 'SP', 'Santa América');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9595, 'SP', 'Santa Bárbara D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9596, 'SP', 'Santa Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9597, 'SP', 'Santa Clara D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9598, 'SP', 'Santa Cruz da Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9599, 'SP', 'Santa Cruz da Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9600, 'SP', 'Santa Cruz da Estrela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9601, 'SP', 'Santa Cruz das Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9602, 'SP', 'Santa Cruz do Rio Pardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9603, 'SP', 'Santa Cruz dos Lopes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9604, 'SP', 'Santa Ernestina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9605, 'SP', 'Santa Eudóxia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9606, 'SP', 'Santa Fé do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9607, 'SP', 'Santa Gertrudes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9608, 'SP', 'Santa Isabel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9609, 'SP', 'Santa Isabel do Marinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9610, 'SP', 'Santa Lúcia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9611, 'SP', 'Santa Margarida Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9612, 'SP', 'Santa Maria da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9613, 'SP', 'Santa Maria do Gurupá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9614, 'SP', 'Santa Mercedes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9615, 'SP', 'Santa Rita D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9616, 'SP', 'Santa Rita do Passa Quatro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9617, 'SP', 'Santa Rita do Ribeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9618, 'SP', 'Santa Rosa de Viterbo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9619, 'SP', 'Santa Salete');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9621, 'SP', 'Santana da Ponte Pensa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9622, 'SP', 'Santana de Parnaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9623, 'SP', 'Santelmo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9624, 'SP', 'Santo Anastácio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9625, 'SP', 'Santo André');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9626, 'SP', 'Santo Antônio da Alegria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9627, 'SP', 'Santo Antônio da Estiva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9628, 'SP', 'Santo Antônio de Posse');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9629, 'SP', 'Santo Antônio do Aracanguá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9630, 'SP', 'Santo Antônio do Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9631, 'SP', 'Santo Antônio do Paranapanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9632, 'SP', 'Santo Antônio do Pinhal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9633, 'SP', 'Santo Antônio Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9634, 'SP', 'Santo Expedito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9635, 'SP', 'Santópolis do Aguapeí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9636, 'SP', 'Santos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9637, 'SP', 'São Benedito da Cachoeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9638, 'SP', 'São Benedito das Areias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9639, 'SP', 'São Bento do Sapucaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9640, 'SP', 'São Bernardo do Campo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9641, 'SP', 'São Berto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9642, 'SP', 'São Caetano do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9643, 'SP', 'São Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9644, 'SP', 'São Francisco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9645, 'SP', 'São Francisco da Praia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9646, 'SP', 'São Francisco Xavier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9647, 'SP', 'São João da Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9648, 'SP', 'São João das Duas Pontes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9649, 'SP', 'São João de Iracema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9650, 'SP', 'São João de Itaguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9651, 'SP', 'São João do Marinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9652, 'SP', 'São João do Pau d Alho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9653, 'SP', 'São João Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9654, 'SP', 'São Joaquim da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9655, 'SP', 'São José da Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9656, 'SP', 'São José das Laranjeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9657, 'SP', 'São José do Barreiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9658, 'SP', 'São José do Rio Pardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9659, 'SP', 'São José do Rio Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9660, 'SP', 'São José dos Campos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9661, 'SP', 'São Lourenço da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9662, 'SP', 'São Lourenço do Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9663, 'SP', 'São Luiz do Paraitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9664, 'SP', 'São Luiz do Guaricanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9665, 'SP', 'São Manuel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9666, 'SP', 'São Martinho D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9667, 'SP', 'São Miguel Arcanjo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9668, 'SP', 'São Paulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9669, 'SP', 'São Pedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9670, 'SP', 'São Pedro do Turvo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9671, 'SP', 'São Roque');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9672, 'SP', 'São Roque da Fartura');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9673, 'SP', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9674, 'SP', 'São Sebastião da Grama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9675, 'SP', 'São Sebastião da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9676, 'SP', 'São Silvestre de Jacareí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9677, 'SP', 'São Simão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9678, 'SP', 'São Vicente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9679, 'SP', 'Sapezal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9680, 'SP', 'Sarapuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9681, 'SP', 'Sarutaiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9682, 'SP', 'Sebastianópolis do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9683, 'SP', 'Serra Azul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9684, 'SP', 'Serra Negra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9685, 'SP', 'Serrana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9686, 'SP', 'Sertãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9687, 'SP', 'Sete Barras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9688, 'SP', 'Severínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9689, 'SP', 'Silvânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9690, 'SP', 'Silveiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9691, 'SP', 'Simões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9692, 'SP', 'Simonsen');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9693, 'SP', 'Socorro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9694, 'SP', 'Sodrélia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9695, 'SP', 'Solemar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9696, 'SP', 'Sorocaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9697, 'SP', 'Sousas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9698, 'SP', 'Sud Mennucci');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9699, 'SP', 'Suinana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9700, 'SP', 'Sumaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9701, 'SP', 'Sussui');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9702, 'SP', 'Suzanápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9703, 'SP', 'Suzano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9704, 'SP', 'Tabajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9705, 'SP', 'Tabapuã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9706, 'SP', 'Tabatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9707, 'SP', 'Taboão da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9708, 'SP', 'Taciba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9709, 'SP', 'Taguaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9710, 'SP', 'Taiaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9711, 'SP', 'Taiacupeba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9712, 'SP', 'Taiúva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9713, 'SP', 'Talhado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9714, 'SP', 'Tambaú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9715, 'SP', 'Tanabi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9716, 'SP', 'Tapinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9717, 'SP', 'Tapiraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9718, 'SP', 'Tapiratiba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9719, 'SP', 'Taquaral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9720, 'SP', 'Taquaritinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9721, 'SP', 'Taquarituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9722, 'SP', 'Taquarivaí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9723, 'SP', 'Tarabai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9724, 'SP', 'Tarumã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9725, 'SP', 'Tatuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9726, 'SP', 'Taubaté');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9727, 'SP', 'Tecainda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9728, 'SP', 'Tejupá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9729, 'SP', 'Teodoro Sampaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9730, 'SP', 'Termas de Ibirá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9731, 'SP', 'Terra Nova D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9732, 'SP', 'Terra Roxa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9733, 'SP', 'Tibiriçá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9734, 'SP', 'Tibiriçá do Paranapanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9735, 'SP', 'Tietê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9736, 'SP', 'Timburi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9737, 'SP', 'Toledo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9738, 'SP', 'Torre de Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9739, 'SP', 'Torrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9740, 'SP', 'Trabiju');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9741, 'SP', 'Tremembé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9742, 'SP', 'Três Alianças');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9743, 'SP', 'Três Fronteiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9744, 'SP', 'Três Pontes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9746, 'SP', 'Tuiuti');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9747, 'SP', 'Tujuguaba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9748, 'SP', 'Tupã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9749, 'SP', 'Tupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9750, 'SP', 'Tupi Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9751, 'SP', 'Turiba do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9752, 'SP', 'Turiúba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9753, 'SP', 'Turmalina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9754, 'SP', 'Turvínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9755, 'SP', 'Ubarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9756, 'SP', 'Ubatuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9757, 'SP', 'Ubirajara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9758, 'SP', 'Uchoa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9759, 'SP', 'União Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9760, 'SP', 'Universo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9761, 'SP', 'Urânia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9762, 'SP', 'Uru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9763, 'SP', 'Urupês');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9764, 'SP', 'Ururai');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9765, 'SP', 'Utinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9766, 'SP', 'Vale Formoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9767, 'SP', 'Valentim Gentil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9768, 'SP', 'Valinhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9769, 'SP', 'Valparaíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9770, 'SP', 'Vanglória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9771, 'SP', 'Vargem');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9772, 'SP', 'Vargem Grande do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9773, 'SP', 'Vargem Grande Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9774, 'SP', 'Varpa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9775, 'SP', 'Várzea Paulista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9776, 'SP', 'Venda Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9777, 'SP', 'Vera Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9778, 'SP', 'Vicente de Carvalho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9779, 'SP', 'Vicentinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9780, 'SP', 'Vila Dirce');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9781, 'SP', 'Vila Nery');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9782, 'SP', 'Vila Xavier');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9783, 'SP', 'Vinhedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9784, 'SP', 'Viradouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9785, 'SP', 'Vista Alegre do Alto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9786, 'SP', 'Vitória Brasil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9787, 'SP', 'Vitoriana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9788, 'SP', 'Votorantim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9789, 'SP', 'Votuporanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9790, 'SP', 'Zacarias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9791, 'TO', 'Abreulândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9792, 'TO', 'Aguiarnópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9793, 'TO', 'Aliança do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9794, 'TO', 'Almas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9795, 'TO', 'Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9796, 'TO', 'Anajanópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9797, 'TO', 'Ananás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9798, 'TO', 'Angico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9799, 'TO', 'Aparecida do Rio Negro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9800, 'TO', 'Apinajé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9801, 'TO', 'Aragaçuí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9802, 'TO', 'Aragominas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9803, 'TO', 'Araguacema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9804, 'TO', 'Araguaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9805, 'TO', 'Araguaína');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9806, 'TO', 'Araguanã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9807, 'TO', 'Araguatins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9808, 'TO', 'Arapoema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9809, 'TO', 'Arraias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9810, 'TO', 'Augustinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9811, 'TO', 'Aurora do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9812, 'TO', 'Axixá do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9813, 'TO', 'Babaçulândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9814, 'TO', 'Bandeirantes do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9815, 'TO', 'Barra do Grota');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9816, 'TO', 'Barra do Ouro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9817, 'TO', 'Barrolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9818, 'TO', 'Bernardo Sayão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9819, 'TO', 'Bom Jesus do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9820, 'TO', 'Brasilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9821, 'TO', 'Brasilândia do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9822, 'TO', 'Brejinho de Nazaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9823, 'TO', 'Buriti do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9824, 'TO', 'Cachoeirinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9825, 'TO', 'Campos Lindos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9826, 'TO', 'Cana Brava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9827, 'TO', 'Cariri do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9828, 'TO', 'Carmolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9829, 'TO', 'Carrasco Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9830, 'TO', 'Cartucho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9831, 'TO', 'Caseara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9832, 'TO', 'Centenário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9833, 'TO', 'Chapada de Areia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9834, 'TO', 'Chapada da Natividade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9835, 'TO', 'Cocalândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9836, 'TO', 'Cocalinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9837, 'TO', 'Colinas do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9838, 'TO', 'Colméia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9839, 'TO', 'Combinado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9840, 'TO', 'Conceição do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9841, 'TO', 'Correinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9842, 'TO', 'Couto de Magalhães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9843, 'TO', 'Craolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9844, 'TO', 'Cristalândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9845, 'TO', 'Crixás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9846, 'TO', 'Crixás do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9847, 'TO', 'Darcinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9848, 'TO', 'Dianópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9849, 'TO', 'Divinópolis do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9850, 'TO', 'Dois Irmãos do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9851, 'TO', 'Duerê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9852, 'TO', 'Escondido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9853, 'TO', 'Esperantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9854, 'TO', 'Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9855, 'TO', 'Figueirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9856, 'TO', 'Filadélfia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9857, 'TO', 'Formoso do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9858, 'TO', 'Fortaleza do Tabocão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9859, 'TO', 'Goianorte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9860, 'TO', 'Goiatins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9861, 'TO', 'Guaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9862, 'TO', 'Gurupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9863, 'TO', 'Ilha Barreira Branca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9864, 'TO', 'Ipueiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9865, 'TO', 'Itacajá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9866, 'TO', 'Itaguatins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9867, 'TO', 'Itapiratins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9868, 'TO', 'Itaporã do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9869, 'TO', 'Jaú do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9870, 'TO', 'Juarina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9871, 'TO', 'Jussara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9872, 'TO', 'Lagoa da Confusão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9873, 'TO', 'Lagoa do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9874, 'TO', 'Lajeado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9875, 'TO', 'Lavandeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9876, 'TO', 'Lizarda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9877, 'TO', 'Luzinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9878, 'TO', 'Marianópolis do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9879, 'TO', 'Mateiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9880, 'TO', 'Maurilândia do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9881, 'TO', 'Miracema do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9882, 'TO', 'Mirandópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9883, 'TO', 'Miranorte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9884, 'TO', 'Monte do Carmo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9885, 'TO', 'Monte Lindo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9886, 'TO', 'Monte Santo do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9887, 'TO', 'Palmeiras do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9888, 'TO', 'Muricilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9889, 'TO', 'Natal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9890, 'TO', 'Natividade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9891, 'TO', 'Nazaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9892, 'TO', 'Nova Olinda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9893, 'TO', 'Nova Rosalândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9894, 'TO', 'Novo Acordo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9895, 'TO', 'Novo Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9896, 'TO', 'Novo Horizonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9897, 'TO', 'Novo Jardim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9898, 'TO', 'Oliveira de Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9899, 'TO', 'Palmas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9900, 'TO', 'Palmeirante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9901, 'TO', 'Palmeirópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9902, 'TO', 'Paraíso do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9903, 'TO', 'Paranã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9904, 'TO', 'Pau D Arco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9905, 'TO', 'Pé da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9906, 'TO', 'Pedro Afonso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9907, 'TO', 'Pedro Ludovico');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9908, 'TO', 'Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9909, 'TO', 'Peixe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9910, 'TO', 'Pequizeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9911, 'TO', 'Pilões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9912, 'TO', 'Pindorama do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9913, 'TO', 'Piraquê');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9914, 'TO', 'Pium');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9915, 'TO', 'Ponte Alta do Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9916, 'TO', 'Ponte Alta do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9917, 'TO', 'Pontes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9918, 'TO', 'Porãozinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9919, 'TO', 'Porto Alegre do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9920, 'TO', 'Porto Lemos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9921, 'TO', 'Porto Nacional');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9922, 'TO', 'Praia Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9923, 'TO', 'Presidente Kennedy');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9924, 'TO', 'Príncipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9925, 'TO', 'Pugmil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9926, 'TO', 'Recursolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9927, 'TO', 'Riachinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9928, 'TO', 'Rio da Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9929, 'TO', 'Rio dos Bois');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9930, 'TO', 'Rio Sono');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9931, 'TO', 'Sampaio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9932, 'TO', 'Sandolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9933, 'TO', 'Santa Fé do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9934, 'TO', 'Santa Maria do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9935, 'TO', 'Santa Rita do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9936, 'TO', 'Santa Rosa do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9937, 'TO', 'Santa Tereza do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9938, 'TO', 'Santa Terezinha do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9939, 'TO', 'São Bento do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9940, 'TO', 'São Félix do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9941, 'TO', 'São Miguel do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9942, 'TO', 'São Salvador do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9943, 'TO', 'São Sebastião do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9944, 'TO', 'São Valério da Natividade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9945, 'TO', 'Silvanópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9946, 'TO', 'Sítio Novo do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9947, 'TO', 'Sucupira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9948, 'TO', 'Taguatinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9949, 'TO', 'Taipas do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9950, 'TO', 'Talismã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9951, 'TO', 'Tamboril');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9952, 'TO', 'Taquaralto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9953, 'TO', 'Taquarussu do Tocantins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9954, 'TO', 'Tocantínia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9955, 'TO', 'Tocantinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9956, 'TO', 'Tupirama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9957, 'TO', 'Tupiratã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9958, 'TO', 'Tupiratins');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9959, 'TO', 'Vênus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9960, 'TO', 'Wanderlândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (9961, 'TO', 'Xambioá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10089, 'AL', 'Jequiá da Praia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10090, 'GO', 'Ipiranga de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10091, 'MT', 'Conquista D Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10092, 'MT', 'Colniza');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10093, 'MT', 'Rondolândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10094, 'MT', 'Santa Rita do Trivelato');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10095, 'MT', 'Nova Santa Helena');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10096, 'MT', 'Santo Antônio do Leste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10097, 'MT', 'Nova Nazaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10098, 'MT', 'Santa Cruz do Xingu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10099, 'MT', 'Bom Jesus do Araguaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10100, 'PI', 'Pau D Arco do Piauí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10101, 'RS', 'Westfália');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10102, 'RS', 'Santa Margarida do Sul');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10103, 'RS', 'Tio Hugo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10104, 'CE', 'Canaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10105, 'ES', 'São João do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10106, 'ES', 'São Raimundo da Pedra Menina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10107, 'ES', 'Santa Cruz de Irupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10108, 'ES', 'São João do Príncipe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10109, 'ES', 'Nossa Senhora de Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10110, 'ES', 'Santa Maria de Marechal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10111, 'ES', 'Alto Castelinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10112, 'ES', 'Prosperidade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10113, 'ES', 'São José de Fruteiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10114, 'ES', 'São Jorge da Barra Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10115, 'ES', 'Jurama');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10117, 'MG', 'Sucanga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10120, 'CE', 'Triângulo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10121, 'CE', 'Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10122, 'CE', 'Patos dos Liberatos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10123, 'CE', 'Campestre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10124, 'CE', 'Timbaúba dos Marinheiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10144, 'GO', 'Rialma');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10164, 'PE', 'Jaboatão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10184, 'PE', 'Navarro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10185, 'PE', 'Cavaleiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10186, 'PE', 'Praia da Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10187, 'PE', 'Paratibe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10205, 'MG', 'Chonin de Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10206, 'GO', 'JK');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10207, 'GO', 'Bezerra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10208, 'RJ', 'São Pedro da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10209, 'BA', 'Arraial D Ajuda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10210, 'BA', 'Trancoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10211, 'BA', 'Itabatã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10236, 'MG', 'Taquaral de Guanhães');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10237, 'GO', 'São Jorge');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10238, 'GO', 'Campo Lindo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10369, 'RJ', 'Batatal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10389, 'PR', 'Jordão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10390, 'PR', 'Biscaia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10391, 'PR', 'Campo do Bugre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10409, 'RJ', 'Serrinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10429, 'PR', 'Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10430, 'PR', 'Caiva');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10431, 'PR', 'Itambezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10469, 'GO', 'Girassol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10470, 'GO', 'Edilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10471, 'AM', 'Cacau Pirêra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10491, 'RJ', 'Ipituna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10591, 'GO', 'Juscelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10611, 'BA', 'Ilha de Bom Jesus dos Passos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10612, 'BA', 'Ilha dos Frades');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10652, 'RS', 'Passo do Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10691, 'GO', 'Jardim ABC de Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10711, 'ES', 'Barra do Sahy');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10731, 'PR', 'Bananas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10732, 'PR', 'Candeia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10733, 'PR', 'Guaraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10734, 'PR', 'Ouro Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10735, 'PR', 'São Roque do Chopim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10751, 'SC', 'Jorge Lacerda');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10752, 'SC', 'Idamar');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10771, 'CE', 'Penedo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10772, 'CE', 'Umarizeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10773, 'CE', 'Lages');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10774, 'CE', 'Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10775, 'CE', 'São João do Amanari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10776, 'CE', 'Papara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10778, 'CE', 'Manoel Guedes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10819, 'GO', 'Olhos D Água');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10839, 'RS', 'Mato Perso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10859, 'BA', 'Pilões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10860, 'BA', 'Pedra Alta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10861, 'RR', 'Santa Cecília');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10862, 'RR', 'Félix Pinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10863, 'RR', 'Taboca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10864, 'RR', 'Novo Paraíso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (10979, 'GO', 'Planalmira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11019, 'PI', 'Aroeiras do Itaim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11020, 'MT', 'Ipiranga do Norte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11021, 'MT', 'Itanhangá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11059, 'AC', 'Campinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11060, 'PA', 'Moraes Almeida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11080, 'MS', 'Nova Casa Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11081, 'AL', 'Pontal de Coruripe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11082, 'AL', 'Botafogo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11083, 'AL', 'Bonsucesso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11084, 'MG', 'Vila Acari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11085, 'MG', 'Chapéu D Uvas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11086, 'MG', 'Penido');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11087, 'MG', 'Monte Verde');

--
-- Data for table public.cidade (OID = 16427) (LIMIT 10000,322)
--
INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11088, 'MG', 'Pirapitinga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11089, 'MG', 'Toledos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11090, 'MG', 'Humaitá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11091, 'CE', 'Ipaguaçú');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11092, 'SP', 'Arabá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11093, 'AL', 'Peroba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11094, 'RS', 'Passo do Goulart');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11095, 'AM', 'Agrovila São Sebastião do Caburi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11096, 'AM', 'Novo Remanso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11097, 'RS', 'Vila Fernando Ferrari');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11098, 'RS', 'Vale dos Vinhedos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11099, 'RS', 'Borussia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11100, 'RS', 'Aguapés');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11101, 'RS', 'Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11119, 'PA', 'São Miguel de Pracuuba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11139, 'PA', 'Cachoeira da Serra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11140, 'PA', 'Caracol');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11141, 'PA', 'Alvorada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11142, 'PA', 'Belo Monte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11159, 'PE', 'Cuieiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11179, 'RS', 'Serrinha Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11199, 'PR', 'São Luiz do Oeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11219, 'PA', 'Vila Mandi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11239, 'SP', 'Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11240, 'SP', 'Lageado de Araçaíba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11241, 'AL', 'Luziápolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11261, 'MG', 'Lagoa Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11262, 'GO', 'Luiz Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11282, 'RJ', 'Frade');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11283, 'AL', 'Lagoa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11303, 'AL', 'Boqueirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11323, 'AP', 'Serra do Navio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11343, 'PR', 'Charco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11344, 'BA', 'Roda Velha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11346, 'MG', 'Chaves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11366, 'CE', 'Bom Princípio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11386, 'BA', 'Barreiros');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11406, 'RJ', 'Califórnia da Barra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11426, 'GO', 'Nova Fátima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11446, 'RO', 'São Carlos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11447, 'RO', 'Nazaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11448, 'RO', 'Demarcação');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11449, 'RO', 'Mutum Paraná');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11450, 'RO', 'Fortaleza do Abunã');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11451, 'RO', 'Nova Califórnia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11452, 'RJ', 'Visconde de Mauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11472, 'GO', 'Arantina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11473, 'MT', 'Comunidade Parque Boa Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11474, 'MG', 'Três Barras da Estrada Real');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11475, 'PR', 'Ilha Encantada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11476, 'PR', 'Ilha do Amparo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11477, 'PR', 'Ilha de Eufrasina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11478, 'PR', 'Ilha do Teixeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11479, 'PR', 'Ilha de São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11480, 'BA', 'Porto Sauípe');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11481, 'MG', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11501, 'GO', 'Trevo do José Rosário');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11521, 'GO', 'Souzalândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11522, 'CE', 'Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11523, 'CE', 'Baixio das Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11524, 'CE', 'Belmonte');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11525, 'CE', 'Bela Vista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11526, 'CE', 'Monte Alverne');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11527, 'CE', 'Campo Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11528, 'CE', 'Laranjeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11529, 'CE', 'Pedras Brancas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11530, 'BA', 'Passé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11531, 'BA', 'Caroba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11532, 'BA', 'Menino Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11533, 'BA', 'Madeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11534, 'BA', 'Passagem dos Teixeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11535, 'BA', 'Cabôto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11536, 'CE', 'Barbada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11537, 'CE', 'Maravilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11538, 'CE', 'Monte Castelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11539, 'CE', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11540, 'CE', 'Catolé da Pista');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11541, 'CE', 'Coité');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11557, 'PA', 'União da Floresta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11558, 'RS', 'Taquaral');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11559, 'RS', 'Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11560, 'RS', 'Harmonia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11561, 'RS', 'Faxinal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11562, 'RS', 'Prado Novo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11563, 'RS', 'Lagoa dos Patos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11564, 'BA', 'Barra Avenida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11584, 'GO', 'Posselândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11585, 'CE', 'Riacho Verde');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11586, 'CE', 'São Bernardo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11587, 'CE', 'Forquilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11607, 'MG', 'Ribeirão da Folha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11608, 'MG', 'Baixa Quente');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11609, 'MG', 'Cruzinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11610, 'MG', 'Lagoa Grande de Minas Novas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11611, 'MG', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11627, 'CE', 'Martinslândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11628, 'CE', 'Mocambo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11629, 'CE', 'Paraiso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11630, 'CE', 'Video');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11631, 'CE', 'Assis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11632, 'CE', 'Curral Velho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11633, 'CE', 'Lagoa das Pedras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11634, 'CE', 'Realejo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11635, 'AL', 'Barro Vermelho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11636, 'AL', 'Ingá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11637, 'AL', 'Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11638, 'AL', 'Riachão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11655, 'BA', 'Caraíbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11676, 'CE', 'Prefeita Suely Pinheiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11677, 'MG', 'Pouso Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11678, 'MG', 'Boa Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11679, 'MG', 'Engenheiro Dolabela');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11697, 'AL', 'Barreiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11698, 'AL', 'Cana Brava');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11699, 'PE', 'Pé-de-Serra dos Mendes');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11700, 'PE', 'Alverne');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11701, 'PE', 'Sítio dos Remédios');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11702, 'PE', 'Serra Negra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11703, 'PE', 'Cajazeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11704, 'PE', 'Areias');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11705, 'PE', 'Logradouro dos Leões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11706, 'PE', 'Cachoeira do Pinto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11707, 'PE', 'Igreja Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11708, 'PE', 'Cavalo Ruço de Deus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11709, 'PE', 'Caldeirão');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11710, 'PE', 'Lagoa da Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11711, 'PE', 'Junco');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11712, 'PE', 'Usina Roçadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11713, 'PE', 'Pau Amarelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11714, 'PE', 'Atapus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11715, 'PE', 'Gambá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11716, 'PE', 'Carne-de-Vaca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11717, 'PE', 'São Lorenço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11718, 'PE', 'São Severino');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11719, 'PE', 'Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11720, 'PE', 'Jirau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11721, 'PE', 'Neves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11722, 'PE', 'Campo Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11723, 'PE', 'Vermelhos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11724, 'PE', 'Matinada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11725, 'PE', 'Santa Rita');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11726, 'PE', 'Tenebre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11727, 'PE', 'Ipanema');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11728, 'PE', 'Rangel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11729, 'PE', 'Pau Ferro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11730, 'PE', 'Serrinha da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11731, 'PE', 'Caramuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11732, 'PE', 'Monte Alegre');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11733, 'PE', 'Santa Rosa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11734, 'PE', 'São Francisco do Brígida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11735, 'PE', 'Pernambuquinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11736, 'PE', 'Cruzeiro do Nordeste');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11737, 'PE', 'Moderna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11738, 'PE', 'Umburanas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11739, 'PE', 'Caroalina');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11740, 'PE', 'Valdemar Siqueira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11741, 'PE', 'Santo Amaro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11742, 'PE', 'Lagoa da Vaca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11743, 'PE', 'Juca Ferrado');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11744, 'PE', 'Lério de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11745, 'PE', 'Mimoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11746, 'PE', 'Grossos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11747, 'PE', 'São João do Ferraz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11748, 'PE', 'Serra da Cachoeira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11749, 'PE', 'Livramento');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11754, 'MG', 'Vila Bem-Querer');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11755, 'AM', 'Itapeaçu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11756, 'AM', 'Vila Lindóia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11757, 'AM', 'Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11758, 'AM', 'Vila Zé Açu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11759, 'AM', 'Vila Amazônia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11760, 'AM', 'Novo Céu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11761, 'AM', 'Purupuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11762, 'AM', 'Terra Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11763, 'AM', 'Careiro Parauá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11764, 'AM', 'Santo Antônio do Matupi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11765, 'AM', 'Sacambu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11766, 'AM', 'Caviana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11767, 'AM', 'Lago do Jacaré');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11768, 'AM', 'Tuiué');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11769, 'AM', 'Campinas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11770, 'AM', 'Araras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11771, 'AM', 'Murituba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11772, 'AM', 'Vila Bittencourt');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11773, 'AM', 'Limoeiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11774, 'AM', 'Feijoal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11775, 'AM', 'Belém de Solimões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11776, 'AM', 'Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11777, 'AM', 'Estirão do Equador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11778, 'AM', 'Platô do Piquiá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11779, 'AL', 'Baixa Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11780, 'AL', 'Farrinheira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11781, 'AL', 'Flexeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11782, 'AL', 'Mangabeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11783, 'AL', 'Mata Limpa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11784, 'AL', 'Pilões');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11785, 'AL', 'Poço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11786, 'AL', 'Poço da Pedra de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11787, 'AL', 'Poço de Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11788, 'AL', 'Poço de Santana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11794, 'AL', 'Quati');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11795, 'AL', 'Retiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11796, 'AL', 'Rio dos Bichos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11797, 'AL', 'São Lourenço');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11798, 'AL', 'Serra da Massa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11799, 'AL', 'Tabocas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11800, 'AL', 'Taquara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11801, 'AL', 'Varginha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11802, 'PE', 'Chã de Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11803, 'PE', 'Caraíbas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11804, 'PE', 'Riacho do Meio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11805, 'PE', 'Santa Luzia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11806, 'PE', 'Nossa Senhora Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11807, 'PE', 'Raiz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11808, 'PE', 'Taboquinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11809, 'PE', 'Vila Nova');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11810, 'PE', 'Caramuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11811, 'PE', 'Caraúba Torta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11812, 'PE', 'Cachoeira Seca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11813, 'PE', 'Itaúna');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11814, 'PE', 'Jacaré Grande');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11815, 'PE', 'Juá');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11816, 'PE', 'Laje');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11817, 'PE', 'Malhada Barreiras Queimadas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11818, 'PE', 'Rafael');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11819, 'PE', 'Patos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11820, 'PE', 'Palmatória');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11821, 'PE', 'Jacarezinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11822, 'PE', 'Malhada de Pedra');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11823, 'PE', 'Pau-Santo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11824, 'PE', 'Terra Vermelha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11825, 'PE', 'Pelada');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11826, 'PE', 'Xicuru');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11827, 'PE', 'Rafael');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11828, 'PE', 'Murici');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11829, 'PE', 'Serrote dos Bois de Cima');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11830, 'PE', 'Xique-Xique');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11831, 'PE', 'Nova Descoberta');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11832, 'PE', 'Ns2 Núcleo de Serviços');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11833, 'PE', 'Pedrinhas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11841, 'PE', 'Massangano');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11843, 'PE', 'Serrote do Urubu');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11844, 'PE', 'Vila Nossa Senhora Aparecida');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11845, 'PE', 'Uruás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11861, 'PE', 'Km 25');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11862, 'PE', 'Vila Nova - N5');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11874, 'GO', 'Santo Antônio da Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11875, 'MG', 'Bom Jesus do Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11876, 'RS', 'Rincão de São Miguel');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11877, 'RS', 'Itapororó');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11878, 'RS', 'Guassu Boi');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11879, 'RS', 'Inhanduí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11880, 'RS', 'Vasco Alves');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11881, 'RS', 'Catimbau');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11882, 'RS', 'Durasnal');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11896, 'PI', 'Nazária');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11897, 'RO', 'Triunfo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11898, 'GO', 'Novo Goiás');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11899, 'CE', 'São Pedro do Gavião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11900, 'MG', 'Cafemirim');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11920, 'RO', 'União Bandeirante');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11940, 'CE', 'Conceição');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11941, 'CE', 'Juritianha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11942, 'CE', 'Lagoa do Carneiro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11943, 'CE', 'Santa Fé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11960, 'PR', 'Passo Amarelo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11980, 'RO', 'Bom Futuro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11981, 'RO', 'Tarilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11982, 'RO', 'Bom Jesus');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11983, 'RO', 'Santa Cruz');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11984, 'PR', 'Cedro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11985, 'BA', 'Palmares');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11986, 'BA', 'Dantelândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11987, 'BA', 'Veredinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11988, 'BA', 'Matinha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (11989, 'MG', 'Cordeiro de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12009, 'BA', 'São Sebastião');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12010, 'BA', 'Cabeceira do Jibóia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12011, 'BA', 'Pradoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12012, 'BA', 'Cercadinho');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12031, 'PR', 'Martinópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12032, 'MT', 'Piratininga');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12033, 'MT', 'Santo Antonio do Rio Bonito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12034, 'MT', 'Novo Mato Grosso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12035, 'MT', 'Santa Terezinha do Rio Ferro');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12036, 'MT', 'Parque Água Limpa');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12037, 'PR', 'Rio XV de Baixo');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12038, 'MG', 'Palmital de Minas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12039, 'MG', 'Ruralminas');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12040, 'MG', 'Pedras de Marilândia');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12059, 'MG', 'Pinheiro Grosso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12060, 'MG', 'Ponte do Cosme');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12061, 'MG', 'Faria');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12062, 'CE', 'Várzea da Onça');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12063, 'CE', 'Mosquito');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12064, 'RO', 'Palmeiras');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12065, 'MT', 'Pedro Neca');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12066, 'MT', 'Bocaiuval');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12067, 'MT', 'Vila Cardoso');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12068, 'MT', 'Água da Prata');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12069, 'MT', 'Nova Primavera');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12070, 'MT', 'Nova Cáceres');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12071, 'MT', 'São José do Pingador');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12072, 'MT', 'Guariba');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12073, 'MT', 'Marechal Rondon');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12074, 'RO', 'Boa Vista de Pacarana');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12075, 'RO', 'Nova Esperança');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12076, 'MG', 'Glória de Cataguases');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12077, 'CE', 'Anil');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12078, 'CE', 'Faveira');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12098, 'PR', 'Ouro Preto');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12099, 'RS', 'Piquiri');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12100, 'RO', 'Guaporé');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12101, 'PR', 'Santa Bárbara');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12102, 'RS', 'Bom Recreio');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12103, 'RS', 'Sede Independência');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12104, 'MT', 'Veranópolis');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12105, 'BA', 'Antarí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12106, 'RJ', 'Pachecos');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12107, 'RJ', 'Manilha');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12108, 'RJ', 'Visconde de Itaboraí');

INSERT INTO cidade (cd_cidade, sigla_uf, nome_cidade)
VALUES (12109, 'PB', 'Bebelândia');

