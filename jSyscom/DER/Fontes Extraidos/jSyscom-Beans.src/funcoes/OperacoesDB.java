package funcoes;

import beans.Empresa;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import utilitarios.Arquivo;
import utilitarios.DataBase;
import utilitarios.Extra;
import utilitarios.Format;
import utilitarios.Utils;

public class OperacoesDB
{
  private static DefaultTableModel modeloVaziu = new DefaultTableModel(0, 0);

  public static Boolean gravaLogBanco(String titulo, String Operacao, String log) {
    try {
      String sql = "INSERT INTO logs (data_logs, hora_logs, titulo_logs, operacao_logs, descricao_logs, cd_usuario, cd_empresa, versao_js_logs) VALUES (current_date, current_time, ?, ?, ?, ?, ?, ?)";

      if (Variaveis.getUserSys() == null) {
        sql = "INSERT INTO logs (data_logs, hora_logs, titulo_logs, operacao_logs, descricao_logs, versao_js_logs) VALUES (current_date, current_time, ?, ?, ?, ?)";

        Object[] par = { titulo, Operacao, log, Variaveis.getVersao() };
        DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        return Boolean.valueOf(true);
      }
      Object[] par = { titulo, Operacao, log, Variaveis.getUserSys().getCodigoUsuario(), Variaveis.getEmpresa().getCodigo(), Variaveis.getVersao() };
      DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      return Boolean.valueOf(true);
    }
    catch (Exception e)
    {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }return Boolean.valueOf(false);
  }

  public static boolean atualizaModulos(Empresa empresa)
  {
    Variaveis.addNewLog("Atualizando...", Boolean.valueOf(false), Boolean.valueOf(false), null);
    String sql = "select * from config_empresa where cd_empresa = ?";
    Object[] par1 = { empresa.getCodigo() };
    try {
      ResultSet res1 = DataBase.executeQuery(sql, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if ((res1.next()) && 
        (res1.getBoolean("atu_mod") == true)) {
        sql = "select * from modulos where ativo_modulos = true";
        ResultSet res2 = DataBase.executeQuery(sql, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
        while (res2.next()) {
          String md5db = res2.getString("MD5_modulos").toUpperCase().trim();
          String md5lc = Arquivo.getMD5Checksum(Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim());
          Variaveis.addNewLog("md5lc:" + md5lc, Boolean.valueOf(false), Boolean.valueOf(false), null);
          Variaveis.addNewLog("md5db:" + md5db, Boolean.valueOf(false), Boolean.valueOf(false), null);
          if ((!md5lc.equals(md5db)) || ("".equals(md5lc))) {
            Variaveis.addNewLog("Hash de arquivos diferentes!", Boolean.valueOf(false), Boolean.valueOf(false), null);
            Blob blob = res2.getBlob("bind_binarys");
            InputStream input = blob.getBinaryStream();
            OutputStream output = new FileOutputStream(Arquivo.getPathBase() + "tmpAtu.jar");
            try {
              if (input != null) {
                Variaveis.addNewLog("Aplicando atualizações...", Boolean.valueOf(false), Boolean.valueOf(false), null);
                byte[] rb = new byte[1024];
                int ch;
                while ((ch = input.read(rb)) != -1) {
                  output.write(rb, 0, ch);
                }
                Variaveis.addNewLog("Apagando arquivo [" + res2.getString("nome_modulos").trim() + "]", Boolean.valueOf(false), Boolean.valueOf(false), null);
                if (Arquivo.DeleteFile(Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim())) {
                  Variaveis.addNewLog("Arquivo apagado com sucesso!", Boolean.valueOf(false), Boolean.valueOf(false), null);
                } else {
                  Extra.Informacao(Variaveis.getFormularioPrincipal(), "Falha ao apagar arquivo de módulos antigo, favor apaga-lo\nmanualmente, para DEPOIS clicar em OK!\nArquivo para ser apagado:\n" + Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim());

                  Variaveis.addNewLog("Falha ao apagar arquivo!", Boolean.valueOf(false), Boolean.valueOf(false), null);
                }
                Arquivo.MoveFile(Arquivo.getPathBase() + "tmpAtu.jar", Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim());
                Variaveis.addNewLog("Movendo atualização para [" + Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim() + "]", Boolean.valueOf(false), Boolean.valueOf(false), null);
              }
            } catch (IOException|SQLException e) {
              Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
            } finally {
              if (input != null) {
                input.close();
              }
              if (output != null) {
                output.close();
              }
            }
          }
        }
      }

      Variaveis.addNewLog("Atualização de modulos do sistema completa!", Boolean.valueOf(false), Boolean.valueOf(false), null);
      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      Variaveis.addNewLog("Falha ao atualizar modulos do sistema!", Boolean.valueOf(true), Boolean.valueOf(true), null);
    }return false;
  }

  public static Integer countQueryResult(String sql)
  {
    int c = 0;
    try {
      ResultSet res = DataBase.executeQuery(sql, null, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      while (res.next())
        c++;
    }
    catch (Exception e) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Integer.valueOf(c);
  }

  public static void analisaAutorun(Usuario usuario, Boolean start) {
    String sql = "SELECT * FROM autorun FULL OUTER JOIN acao USING(cd_acao) WHERE autorun.cd_empresa = ? and autorun.cd_usuario = ? and autorun.somente_inicio = ? and autorun.ativo_autorun = true order by cd_autorun";
    try
    {
      Object[] par = { usuario.getEmpresa().getCodigo(), usuario.getCodigoUsuario(), start };
      ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      sql = "UPDATE autorun SET ativo_autorun = ? where cd_empresa = ? and cd_usuario = ? and cd_autorun = ?";

      while (res.next()) {
        Object[] par1 = { start, usuario.getEmpresa().getCodigo(), usuario.getCodigoUsuario(), Integer.valueOf(res.getInt("cd_autorun")) };
        DataBase.executeQuery(sql, par1, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        if (res.getInt("cd_acao") != 0) {
          Util.processaAcao(Integer.valueOf(res.getInt("cd_acao")), res.getString("descricao_autorun"));
        }
        if (res.getInt("nro_operacao") != 0)
          Operacoes.executa(Integer.valueOf(res.getInt("nro_operacao")), res.getString("descricao_autorun"));
      }
    }
    catch (Exception e) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    verificaAvisos(Variaveis.getUserSys(), Boolean.valueOf(true));
  }

  public static void verificaAvisos(Usuario usuario, Boolean forcar) {
    String SQL = "select * from aviso where cd_empresa = ? and cd_usuario = ?";
    try {
      Object[] par = { usuario.getEmpresa().getCodigo(), usuario.getCodigoUsuario() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      while (rs.next())
        if (forcar.booleanValue()) {
          if (rs.getBoolean("insistente_aviso")) {
            Variaveis.setAviso(rs.getString("descricao_aviso"));
            Variaveis.setCdAviso(Integer.valueOf(rs.getInt("cd_aviso")));
            Util.execucaoDiretaClass("formularios.FI_SysAvisos", "Modulos-jSyscom.jar", "Aviso", new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), usuario);
            Object[] p = { Integer.valueOf(rs.getInt("cd_aviso")) };
            SQL = "UPDATE aviso SET visualizado_aviso = true WHERE cd_aviso = ?";
            DataBase.executeQuery(SQL, p, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
          }
        }
        else if ((rs.getBoolean("insistente_aviso")) && (!rs.getBoolean("visualizado_aviso"))) {
          Variaveis.setAviso(rs.getString("descricao_aviso"));
          Variaveis.setCdAviso(Integer.valueOf(rs.getInt("cd_aviso")));
          Util.execucaoDiretaClass("formularios.FI_SysAvisos", "Modulos-jSyscom.jar", "Aviso", new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), usuario);
          Object[] p = { Integer.valueOf(rs.getInt("cd_aviso")) };
          SQL = "UPDATE aviso SET visualizado_aviso = true WHERE cd_aviso = ?";
          DataBase.executeQuery(SQL, p, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        }
    }
    catch (SQLException ex)
    {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(false), null);
    }
  }

  public static Boolean validaLicenca(Empresa empresa) {
    try {
      String sql = "SELECT valida_licenca(?)";
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next())
        return Boolean.valueOf(rs.getBoolean(1));
    }
    catch (Exception e) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean aplicaLicenca(String chave) {
    try {
      String c = Extra.CriptoDecritoSenhaInteiro("D", chave);
      String sql = "SELECT aplica_licenca(?, ?)";
      Object[] par = { Integer.valueOf(Integer.parseInt(c.substring(0, 4))), new Date(Long.parseLong(c.substring(4, c.length()))) };
      ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next())
        return Boolean.valueOf(rs.getBoolean(1));
    }
    catch (NumberFormatException|SQLException e) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Date getDataExpLic(Empresa empresa) {
    try {
      String sql = "SELECT * FROM licenca WHERE cd_empresa = ? order by data_expira desc";
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if ((rs.next()) && 
        (rs.getDate("data_expira") != null))
        return rs.getDate("data_expira");
    }
    catch (Exception e)
    {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }

  public static Integer getDiasRestantesLic(Empresa empresa) {
    try {
      Date ex = getDataExpLic(empresa);
      if (ex != null) {
        Date hoje = new Date(System.currentTimeMillis());
        if (Format.DateToLong(hoje) <= Format.DateToLong(ex)) {
          return Integer.valueOf(Extra.dataDiff(hoje, ex));
        }
        return Integer.valueOf(Extra.dataDiff(ex, hoje));
      }
    }
    catch (Exception e)
    {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }

  public static void notInsistenteAvisos(Integer cd_aviso) {
    String SQL = "update aviso set insistente_aviso = false where cd_aviso = ?";
    try {
      Object[] par = { cd_aviso };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    } catch (Exception ex) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void visualizadoAvisos(Integer cd_aviso) {
    String SQL = "update aviso set visualizado_aviso = true where cd_aviso = ?";
    try {
      Object[] par = { cd_aviso };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    } catch (Exception ex) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static String getTitleCustom(Empresa empresa) {
    String SQL = "select titulo_software from config_empresa where cd_empresa = ?";
    String title = "";
    try {
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next()) {
        title = rs.getString("titulo_software");
      }
      title = title.replaceFirst("@title", "Sistema de Automação Comercial jSyscom");
      title = title.replaceFirst("@company", Variaveis.getEmpresa().getFantasia());
      title = title.replaceFirst("@version", Variaveis.getVersao());
    } catch (SQLException ex) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return title;
  }

  public static void executeSQL(Integer db, String SQL, String ip, Integer port, String local, String user, String pass, JTable table) {
    try {
      if (SQL.trim().toUpperCase().startsWith("SELECT")) {
        ResultSet rs = DataBase.executeQuery(SQL, null, ip, port, local, user, pass, 2, db);
        table.setAutoResizeMode(0);
        if (rs != null)
          table.setModel(Format.getResultSetTableModal(rs));
        else
          table.setModel(modeloVaziu);
      }
      else {
        DataBase.executeQuery(SQL, null, ip, port, local, user, pass, 1, db);
        table.setAutoResizeMode(0);
        table.setModel(modeloVaziu);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     funcoes.OperacoesDB
 * JD-Core Version:    0.6.2
 */