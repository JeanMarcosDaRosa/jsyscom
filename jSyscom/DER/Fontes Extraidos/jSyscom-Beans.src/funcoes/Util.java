package funcoes;

import beans.Conexao;
import beans.Empresa;
import beans.Menu;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import dao.EmpresaDao;
import dao.MenuDao;
import dao.UsuarioDao;
import java.awt.Component;
import java.awt.Image;
import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import relatorios.ClasseReferencia;
import utilitarios.Arquivo;
import utilitarios.DataBase;
import utilitarios.Extra;
import utilitarios.Format;
import utilitarios.Relatorio;
import utilitarios.Utils;

public abstract class Util
{
  public static String getSource(Integer instancia, Class a)
  {
    if (a != null) {
      return "Source{ " + a.getSimpleName() + " - [" + instancia + "]" + " }";
    }
    return null;
  }

  public static void processaAcao(Integer cd_acao, String titulo)
  {
    if (!Variaveis.getBloqueado().booleanValue()) {
      if (cd_acao != null) {
        Boolean permissao = UsuarioDao.getPermition(Variaveis.getUserSys(), cd_acao);
        try {
          Integer oper = Integer.valueOf(0);
          String descrOper = "";
          String modulo = "";
          Object[] par1 = { cd_acao };

          String sql = "SELECT * FROM acao FULL OUTER JOIN operacao USING(cd_acao) WHERE acao.cd_acao = ?";
          ResultSet rs1 = DataBase.executeQuery(sql, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

          if (rs1.next()) {
            oper = Integer.valueOf(rs1.getInt("cd_operacao"));
            descrOper = rs1.getString("descricao_operacao");
          }
          if (permissao.booleanValue())
          {
            sql = "SELECT * FROM classe WHERE cd_acao = ?";
            ResultSet res = DataBase.executeQuery(sql, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

            if (res.next()) {
              Object[] par2 = { Integer.valueOf(res.getInt("cd_modulos")) };
              sql = "select * from modulos where cd_modulos = ? and ativo_modulos = true";
              ResultSet rs = DataBase.executeQuery(sql, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

              if (rs.next()) {
                modulo = rs.getString("nome_modulos");
                sql = "select * from opcoes_tela where cd_classe=?";
                OpcoesTela opcoes = new OpcoesTela();
                Object[] par3 = { Integer.valueOf(res.getInt("cd_classe")) };
                rs = DataBase.executeQuery(sql, par3, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

                if (rs.next()) {
                  opcoes.setClosable(Boolean.valueOf(rs.getBoolean("closable")));
                  opcoes.setIconifiable(Boolean.valueOf(rs.getBoolean("iconifiable")));
                  opcoes.setMaximizable(Boolean.valueOf(rs.getBoolean("maximizable")));
                  opcoes.setResizable(Boolean.valueOf(rs.getBoolean("resizable")));
                  opcoes.setVisible(Boolean.valueOf(rs.getBoolean("visible")));
                  opcoes.setShow_version(Boolean.valueOf(rs.getBoolean("show_version")));
                }
                Variaveis.addNewLog("Iniciando Tela: [" + res.getString("NOME_CLASSE") + "]", Boolean.valueOf(false), Boolean.valueOf(false), null);
                localizaClass(res.getString("NOME_CLASSE"), modulo, titulo, opcoes, Variaveis.getUserSys());
              } else {
                Variaveis.addNewLog("Ação -> Sql -> \"Retorno do nome do modulo nulo\"", Boolean.valueOf(false), Boolean.valueOf(false), null);
                Extra.Informacao(Variaveis.getFormularioPrincipal(), "Modulo [" + modulo + "] não é válido!");
              }

            }

            sql = "SELECT * FROM relatorio WHERE cd_acao = ? ORDER BY cd_relatorio";
            ResultSet res1 = DataBase.executeQuery(sql, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

            while (res1.next()) {
              HashMap h = new HashMap();
              imprimeRelatorioJarFile(h, res1.getString("nome_relatorio"), Variaveis.getEmpresa().getCodigo());
            }

            if (oper.intValue() != 0)
            {
              Operacoes.executa(oper, descrOper);
            }

          }
          else if (oper.intValue() >= 1000) {
            Operacoes.executa(oper, descrOper);
          } else {
            Variaveis.addNewLog("Permissão NEGADA para Usuário: '" + Variaveis.getUserSys().getNomeUsuario() + "' na Empresa: '" + Variaveis.getEmpresa().getFantasia() + "' Ação: '" + cd_acao + "'", Boolean.valueOf(true), Boolean.valueOf(true), null);
          }
        }
        catch (Exception e)
        {
          Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
        }
      } else {
        Variaveis.addNewLog("Ação inválida!", Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
    }
    else Extra.Informacao(Variaveis.getFormularioPrincipal(), "O Sistema está temporariamente BLOQUEADO!\n Salve suas alterações e reinicie o programa, se o problema persistir \nentre em contato com o suporte técnico!");
  }

  public static void carregaConfiguracoesLocais(JLabel lblEmpresa, JTextField tfEmpresa, JTextField tfLogin, Boolean start)
  {
    if (Arquivo.FileExists(Arquivo.getPathBase() + "Acesso.pw")) {
      Variaveis.setDbPass(Extra.CriptoDecripto("D", Arquivo.LePrimeiraLinhaArqTXT(Arquivo.getPathBase() + "Acesso.pw").trim()));
    }
    if (Arquivo.FileExists(Arquivo.getPathBase() + "Config.ini")) {
      Variaveis.setDbLocal(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "LOCAL_BANCO"));
      Variaveis.setIpServ(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "IP_SERVIDOR"));
      Variaveis.setDbUser(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "USUARIO_BANCO"));
      Variaveis.setDbPort(Integer.valueOf(Integer.parseInt(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "PORTA_BANCO").trim())));
      if ("S".equals(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "NET").toUpperCase().trim()))
        Variaveis.setNet(Boolean.valueOf(true));
      else {
        Variaveis.setNet(Boolean.valueOf(false));
      }
      Variaveis.setNomeLogo(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "NOME_ARQUIVO_LOGO"));
      if (("S".equals(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "AUTO_ATIVO").toUpperCase().trim())) && (start.booleanValue()) && 
        (!Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "AUTO_EMP").trim().equals(""))) {
        Empresa e = EmpresaDao.buscaEmpresa(Integer.valueOf(Integer.parseInt(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "AUTO_EMP").trim())));
        if (e != null) {
          lblEmpresa.setText(Format.pegaTexto(e.getFantasia(), 0, 18));
          tfEmpresa.setText(String.valueOf(e.getCodigo()));
          tfEmpresa.setEnabled(false);
          tfLogin.requestFocus();
          if (!"".equals(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "AUTO_USER").trim())) {
            tfLogin.setText(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "CONFIG", "AUTO_USER").toUpperCase().trim());
            tfLogin.setEnabled(false);
          }
        }
      }
    }
  }

  public static Object localizaClass(String classe, String nome_modulo)
  {
    if (!Variaveis.getBloqueado().booleanValue())
      try {
        ServiceFactory.setURLs(new URL[] { new URL("file:" + Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + nome_modulo) });
        Class cls = ServiceFactory.getService(classe);
        Class[] partypes = null;
        Constructor ct = cls.getConstructor(partypes);
        Object[] arglist = null;
        return ct.newInstance(arglist);
      }
      catch (Throwable e) {
        Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
    else {
      Extra.Informacao(Variaveis.getFormularioPrincipal(), "O Sistema está temporariamente BLOQUEADO!\n Salve suas alterações e reinicie o programa, se o problema persistir \nentre em contato com o suporte técnico!");
    }

    return null;
  }

  private static Object localizaClass(String classe, String nome_modulo, String titulo, OpcoesTela parametros, Usuario u) {
    if (!Variaveis.getBloqueado().booleanValue()) {
      try {
        ServiceFactory.setURLs(new URL[] { new URL("file:" + Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + nome_modulo) });
        Class cls = ServiceFactory.getService(classe);
        Class[] partypes = new Class[3];
        partypes[0] = String.class;
        partypes[1] = OpcoesTela.class;
        partypes[2] = Usuario.class;
        Constructor ct = cls.getConstructor(partypes);
        Object[] arglist = new Object[3];
        arglist[0] = titulo;
        arglist[1] = parametros;
        arglist[2] = u;
        return ct.newInstance(arglist);
      } catch (Throwable e) {
        Variaveis.addNewLog("Falha ao carregar modulo [" + titulo + "]\n" + "Arquivo de modulos: [" + Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + nome_modulo + "]\n" + "Erro:\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
    }
    else
    {
      Extra.Informacao(Variaveis.getFormularioPrincipal(), "O Sistema está temporariamente BLOQUEADO!\n Salve suas alterações e reinicie o programa, se o problema persistir \nentre em contato com o suporte técnico!");
    }

    return null;
  }

  public static Object execucaoDiretaClass(String classe, String nome_modulo, String titulo, OpcoesTela parametros, Usuario u) {
    if (MenuDao.alreadyClass(classe).booleanValue()) {
      if (UsuarioDao.getPermition(u, classe).booleanValue()) {
        return localizaClass(classe, nome_modulo, titulo, parametros, u);
      }
      Variaveis.addNewLog("Permissão Negada para Usuário: '" + u.getNomeUsuario() + "' da empresa: '" + u.getEmpresa().getFantasia() + "' classe: '" + classe + "'", Boolean.valueOf(true), Boolean.valueOf(true), null);
    }
    else {
      return localizaClass(classe, nome_modulo, titulo, parametros, u);
    }
    return null;
  }

  public static void imprimeRelatorioFile(HashMap parametro, String jasperFile, Object empresa) {
    try {
      Variaveis.addNewLog("Iniciando Relatório: [ " + jasperFile + " ]", Boolean.valueOf(false), Boolean.valueOf(false), null);
      parametro.put("SUBREPORT_DIR", jasperFile.substring(0, jasperFile.lastIndexOf("/") + 1));
      parametro.put("EMPRESA", empresa);
      parametro.put("LOGOTIPO", Variaveis.getUrlLogo());
      Connection conexao = Conexao.getInstance().getCon(Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), Variaveis.getDbType());
      Relatorio rel = new Relatorio(conexao, parametro, jasperFile);
      rel.exibirRelatorio();
    } catch (Exception e) {
      Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void imprimeRelatorioJarFile(HashMap parametro, String path, Object empresa) {
    try {
      Variaveis.addNewLog("Iniciando Relatório: [ " + path + " ]", Boolean.valueOf(false), Boolean.valueOf(false), null);
      InputStream caminhoRelatorio = ClasseReferencia.class.getClassLoader().getResourceAsStream(path);
      parametro.put("SUBREPORT_DIR", getUrlReport(path));
      parametro.put("EMPRESA", empresa);
      parametro.put("LOGOTIPO", Variaveis.getUrlLogo());
      Connection conexao = Conexao.getInstance().getCon(Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), Variaveis.getDbType());
      Relatorio rel = new Relatorio(conexao, parametro, caminhoRelatorio);
      rel.exibirRelatorio();
    } catch (Exception e) {
      Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void imprimeRelatorioFile(List<Object> dados, HashMap parametro, String jasperFile, Object empresa) {
    try {
      Variaveis.addNewLog("Iniciando Relatório: [ " + jasperFile + " ]", Boolean.valueOf(false), Boolean.valueOf(false), null);
      parametro.put("SUBREPORT_DIR", jasperFile.substring(0, jasperFile.lastIndexOf("/") + 1));
      parametro.put("EMPRESA", empresa);
      parametro.put("LOGOTIPO", Variaveis.getUrlLogo());
      Relatorio rel = new Relatorio(dados, parametro, jasperFile);
      rel.exibirRelatorio();
    } catch (Exception e) {
      Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void imprimeRelatorioJarFile(List<Object> dados, HashMap parametro, String path, Object empresa) {
    try {
      Variaveis.addNewLog("Iniciando Relatório: [ " + path + " ]", Boolean.valueOf(false), Boolean.valueOf(false), null);
      InputStream caminhoRelatorio = ClasseReferencia.class.getClassLoader().getResourceAsStream(path);
      parametro.put("SUBREPORT_DIR", getUrlReport(path));
      parametro.put("EMPRESA", empresa);
      parametro.put("LOGOTIPO", Variaveis.getUrlLogo());
      Relatorio rel = new Relatorio(dados, parametro, caminhoRelatorio);
      rel.exibirRelatorio();
    } catch (Exception e) {
      Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public boolean isLeaf(Object node) {
    return node instanceof Menu;
  }

  private static void expandAll(JTree tree, TreePath parent, boolean expand)
  {
    TreeNode node = (TreeNode)parent.getLastPathComponent();
    Enumeration e;
    if (node.getChildCount() >= 0) {
      for (e = node.children(); e.hasMoreElements(); ) {
        TreeNode n = (TreeNode)e.nextElement();
        TreePath path = parent.pathByAddingChild(n);
        expandAll(tree, path, expand);
      }
    }

    if (expand)
      tree.expandPath(parent);
    else
      tree.collapsePath(parent);
  }

  public static void expandAll(JTree tree, boolean expand)
  {
    TreeNode raiz = (TreeNode)tree.getModel().getRoot();
    expandAll(tree, new TreePath(raiz), expand);
  }

  public static Icon setImagen(String path, Integer width, Integer height) {
    try {
      if (Arquivo.FileExists(path)) {
        ImageIcon i = new ImageIcon(path);

        Image i2 = i.getImage();
        return new ImageIcon(i2.getScaledInstance(width.intValue(), height.intValue(), 1));
      }

      Variaveis.addNewLog("Arquivo não encontrado em '" + path + "'!", Boolean.valueOf(true), Boolean.valueOf(false), null);
    }
    catch (Exception e) {
      Variaveis.addNewLog("Problemas ao carregar imagem...\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }

  public static void lookandfeel(String theme, Component c) {
    if ((theme != null) && (!theme.equals("")))
      try {
        UIManager.setLookAndFeel(theme);
        SwingUtilities.updateComponentTreeUI(c);
      } catch (ClassNotFoundException|InstantiationException|IllegalAccessException|UnsupportedLookAndFeelException erro) {
        Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(erro), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
  }

  public static void lookandfeel(String landf, String jar, Component c)
  {
    try {
      LookAndFeel l = (LookAndFeel)localizaClass(landf, jar);
      if (l != null) {
        UIManager.setLookAndFeel(l);
        SwingUtilities.updateComponentTreeUI(c);
      }
    } catch (Exception erro) {
      Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(erro), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void closeInternalFrame(JInternalFrame form) {
    try {
      form.setClosed(true);
    } catch (PropertyVetoException ex) {
      Variaveis.addNewLog(Util.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void mostraForm(String title, Boolean mostra, JDesktopPane Desk) {
    for (JInternalFrame f : Desk.getAllFrames())
      if (f.getTitle().equals(title)) {
        f.setVisible(mostra.booleanValue());
        break;
      }
  }

  public static void finalizaItem(String titulo, JDesktopPane Desk)
  {
    for (JInternalFrame form : Desk.getAllFrames())
      if (form.getTitle().equals(titulo)) {
        closeInternalFrame(form);
        break;
      }
  }

  public static void finalizaTodos(JDesktopPane Desk)
  {
    for (JInternalFrame form : Desk.getAllFrames())
      closeInternalFrame(form);
  }

  public static String getUrlReport(String report)
  {
    URL mainReport = ClasseReferencia.class.getClassLoader().getResource(report);
    String subreportDir = mainReport.getPath().substring(0, mainReport.getPath().lastIndexOf("/") + 1);
    subreportDir = mainReport.getProtocol() + ":" + subreportDir;
    return subreportDir;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     funcoes.Util
 * JD-Core Version:    0.6.2
 */