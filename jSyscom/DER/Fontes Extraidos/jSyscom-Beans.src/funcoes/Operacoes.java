package funcoes;

import beans.Variaveis;
import utilitarios.Extra;

public abstract class Operacoes
{
  public static final Integer INFO = Integer.valueOf(1000);
  public static final Integer BLOK = Integer.valueOf(1001);
  public static final Integer DBLOK = Integer.valueOf(1002);
  public static final Integer EXIT = Integer.valueOf(1003);

  public static void executa(Integer codigoOperacao, String descricao)
  {
    switch (codigoOperacao.intValue())
    {
    case 1000:
      Extra.Informacao(Variaveis.getFormularioPrincipal(), descricao);
      break;
    case 1001:
      Variaveis.setBloqueado(Boolean.valueOf(true));
      break;
    case 1002:
      Variaveis.setBloqueado(Boolean.valueOf(false));
      break;
    case 1003:
      System.exit(0);
      break;
    default:
      Variaveis.addNewLog("Operação solicitada não foi encontrada!", Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     funcoes.Operacoes
 * JD-Core Version:    0.6.2
 */