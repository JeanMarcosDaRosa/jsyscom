package funcoes;

import java.io.File;
import java.util.List;
import javax.swing.filechooser.FileFilter;

public class ExtensionFileFilter extends FileFilter
{
  String[] extensions;
  String description;

  public ExtensionFileFilter(String descr, String[] exts)
  {
    this.extensions = new String[exts.length];
    for (int i = exts.length - 1; i >= 0; i--) {
      this.extensions[i] = exts[i].toLowerCase();
    }

    this.description = (descr == null ? exts[0] + " files" : descr);
  }

  public ExtensionFileFilter(String descr, List<String> exts)
  {
    this(descr, (String[])exts.toArray(new String[exts.size()]));
  }

  public boolean accept(File f)
  {
    if (f.isDirectory()) {
      return true;
    }

    for (String extension : this.extensions) {
      if (f.getName().toLowerCase().endsWith(extension)) {
        return true;
      }
    }

    return false;
  }

  public String getDescription()
  {
    return this.description;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     funcoes.ExtensionFileFilter
 * JD-Core Version:    0.6.2
 */