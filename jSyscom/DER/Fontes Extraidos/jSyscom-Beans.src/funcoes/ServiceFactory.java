package funcoes;

import java.net.URL;
import java.net.URLClassLoader;

public class ServiceFactory
{
  private static URL[] classLoaderUrls;

  public static void setURLs(URL[] urls)
  {
    classLoaderUrls = urls;
  }

  public static Class getService(String classe) throws Exception {
    URLClassLoader loader = new URLClassLoader(classLoaderUrls);
    return Class.forName(classe, true, loader);
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     funcoes.ServiceFactory
 * JD-Core Version:    0.6.2
 */