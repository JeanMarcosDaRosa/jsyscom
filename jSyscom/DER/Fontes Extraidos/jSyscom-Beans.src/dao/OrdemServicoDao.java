package dao;

import beans.Cliente;
import beans.Contato;
import beans.Empresa;
import beans.Endereco;
import beans.Equipamento;
import beans.EquipamentoTeste;
import beans.Funcionario;
import beans.Imagem;
import beans.ItemProdOS;
import beans.ItemPropTipoEquip;
import beans.ItemServicoOS;
import beans.LocalProd;
import beans.Orcamento;
import beans.OrdemServico;
import beans.Pessoa;
import beans.Produto;
import beans.PropriedadeTipoEquip;
import beans.Servico;
import beans.TipoEquipamento;
import beans.Transportadora;
import beans.Usuario;
import beans.Variaveis;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import utilitarios.DataBase;
import utilitarios.Utils;

public class OrdemServicoDao
{
  public static String toVirtualStatus(String fisic)
  {
    if (!"".equals(fisic)) {
      if (fisic.toUpperCase().trim().equals("A")) {
        return "ABERTO";
      }
      if (fisic.toUpperCase().trim().equals("B")) {
        return "AGUARDANDO OC";
      }
      if (fisic.toUpperCase().trim().equals("C")) {
        return "AGUARDANDO AUTH";
      }
      if (fisic.toUpperCase().trim().equals("E")) {
        return "EM ANDAMENTO";
      }
      if (fisic.toUpperCase().trim().equals("D")) {
        return "AGUARDANDO PGTO";
      }
      if (fisic.toUpperCase().trim().equals("F")) {
        return "FINALIZADO";
      }
    }
    return fisic;
  }

  public static String toFisicStatus(String virtual) {
    if (!"".equals(virtual)) {
      if (virtual.toUpperCase().trim().equals("ABERTO")) {
        return "A";
      }
      if (virtual.toUpperCase().trim().equals("AGUARDANDO OC")) {
        return "B";
      }
      if (virtual.toUpperCase().trim().equals("AGUARDANDO AUTH")) {
        return "C";
      }
      if (virtual.toUpperCase().trim().equals("EM ANDAMENTO")) {
        return "E";
      }
      if (virtual.toUpperCase().trim().equals("AGUARDANDO PGTO")) {
        return "D";
      }
      if (virtual.toUpperCase().trim().equals("FINALIZADO")) {
        return "F";
      }
    }
    return virtual;
  }

  public static OrdemServico buscaOS(Integer codOrdemServico, Empresa empresa, Boolean getImage) {
    OrdemServico o = new OrdemServico(codOrdemServico);
    buscaOS(o, empresa, getImage);
    return o;
  }

  public static void buscaOS(OrdemServico ordemServico, Empresa empresa, Boolean getImage) {
    try {
      String SQL = "select * from os_cab where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        populaOSResultSet(rs, ordemServico, empresa, getImage);
    }
    catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Orcamento getUltimoOrcamento(OrdemServico os, Empresa empresa) {
    Orcamento ult = null;
    Integer idx = Integer.valueOf(0); Integer rev = Integer.valueOf(0);
    Integer localInteger1;
    Integer localInteger2;
    for (Iterator i$ = os.getOrcamentos().iterator(); i$.hasNext(); 
      localInteger2 = idx = Integer.valueOf(idx.intValue() + 1))
    {
      Orcamento o = (Orcamento)i$.next();
      if (idx.intValue() == 0) {
        ult = o;
        rev = o.getNroRevisao();
      }
      else if (o.getNroRevisao().intValue() > rev.intValue()) {
        ult = o;
      }

      localInteger1 = idx;
    }
    return ult;
  }

  public static void populaOSResultSet(ResultSet rs, OrdemServico os, Empresa empresa, Boolean getImage) {
    try {
      os.setCodOS(Integer.valueOf(rs.getInt("cd_os_cab")));
      os.setCliente(PessoaDao.buscaCliente(Integer.valueOf(rs.getInt("cd_cliente")), empresa));
      os.setLocalizacao(ProdutoDao.getLocalProd(Integer.valueOf(rs.getInt("cd_local_prod")), empresa));
      os.setTransportadora(TransportadoraDao.buscaTransportadora(Integer.valueOf(rs.getInt("cd_transportadora")), empresa));
      if (rs.getInt("cd_contato_cliente") != 0) {
        os.setContatoCliente(PessoaDao.getContato(os.getCliente(), Integer.valueOf(rs.getInt("cd_contato_cliente")), empresa));
      }
      os.setUsuario(UsuarioDao.buscaUsuario(Integer.valueOf(rs.getInt("cd_usuario")), empresa));
      os.setEnderecoCliente(PessoaDao.getEnderecoPessoa(os.getCliente(), rs.getString("cep_logradouro_cliente"), Integer.valueOf(rs.getInt("numero_endereco_cliente")), empresa));

      os.setEquipamento(EquipamentoDao.buscaEquipamento(os.getCliente(), Integer.valueOf(rs.getInt("cd_equip")), empresa, getImage));
      os.setNroOcCliente(Integer.valueOf(rs.getInt("nro_oc_cliente_os_cab")));
      os.setDefeitoReclamado(rs.getString("defeito_reclamado_equip_os_cab"));
      os.setEstadoEquipamento(rs.getString("estado_equip_os_cab"));
      os.setConsertoDetalhado(rs.getString("conserto_detalhado_os_cab"));
      os.setParecer(rs.getString("parecer_os_cab"));
      os.setMesesGarantia(Integer.valueOf(rs.getInt("meses_garantia_os_cab")));
      os.setDataCadastro(rs.getDate("data_cad_os_cab"));
      os.setDataEntrada(rs.getDate("data_ent_os_cab"));
      os.setDataSaida(rs.getDate("data_saida_os_cab"));
      os.setSobGarantia(Boolean.valueOf(rs.getBoolean("sob_garantia")));
      os.setStatusOS(toVirtualStatus(rs.getString("status_os_cab")));
      buscaTestesEquipOS(os, empresa);
      buscaItensServicoOS(os, empresa);
      buscaItensProdOS(os, empresa);
      buscaEnvolvidosOS(os, empresa);
      buscaOrcamentosOS(os, empresa);
      buscaItemsPropTipoEq(os, os.getEquipamento().getTipoEquipamento(), empresa);
      os.setFlagPronto(Boolean.valueOf(true));
    } catch (SQLException ex) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void buscaOrcamentosOS(OrdemServico ordemServico, Empresa empresa) {
    ordemServico.getOrcamentos().clear();
    try {
      String SQL = "select * from orcamento where cd_empresa = ? and cd_os_cab = ? order by nro_revisao_orcamento";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Orcamento o = new Orcamento(Integer.valueOf(rs.getInt("nro_revisao_orcamento")), ordemServico);
        o.setAutorizado(Boolean.valueOf(rs.getBoolean("autorizado_orcamento")));
        o.setDataAutorizacao(rs.getDate("data_auth_orcamento"));
        o.setDataCadastro(rs.getDate("data_cad_orcamento"));
        o.setDataPrevisao(rs.getDate("data_prev_orcamento"));
        o.setTipoFrete(rs.getString("tipo_frete_orcamento"));
        o.setUsoDescontoCliente(Boolean.valueOf(rs.getBoolean("usa_desc_cli_orcamento")));
        o.setValorBruto(Double.valueOf(rs.getDouble("vlr_bruto_orcamento")));
        o.setValorDesconto(Double.valueOf(rs.getDouble("vlr_desconto_orcamento")));
        o.setValorFrete(Double.valueOf(rs.getDouble("vlr_frete_orcamento")));
        o.setValorLiquido(Double.valueOf(rs.getDouble("vlr_liquido_orcamento")));
        o.setDiasValidade(Integer.valueOf(rs.getInt("dias_validade_orcamento")));
        ordemServico.getOrcamentos().add(o);
      }
      ordemServico.setUltimoOrcamento(getUltimoOrcamento(ordemServico, empresa));
    } catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void buscaEnvolvidosOS(OrdemServico ordemServico, Empresa empresa) {
    ordemServico.getEnvolvidos().clear();
    try {
      String SQL = "select * from os_envolvidos where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Funcionario o = new Funcionario(Integer.valueOf(rs.getInt("cd_funcionario")));
        PessoaDao.buscaFuncionario(o, empresa);
        ordemServico.getEnvolvidos().add(o);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void buscaItensProdOS(OrdemServico ordemServico, Empresa empresa) {
    ordemServico.getItensProdutos().clear();
    try {
      String SQL = "select * from os_prod_det where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        ItemProdOS o = new ItemProdOS();
        o.setOrdemServico(ordemServico);
        o.setQuantidade(Double.valueOf(rs.getDouble("qtd_os_det")));
        o.setValorBruto(Double.valueOf(rs.getDouble("vlr_bruto_os_det")));
        o.setValorUnitario(Double.valueOf(rs.getDouble("vlr_unitario_os_det")));
        o.setOriginal(Boolean.valueOf(rs.getBoolean("original_os_det")));
        o.setProduto(ProdutoDao.buscaProduto(Integer.valueOf(rs.getInt("cd_produto")), empresa, Boolean.valueOf(true)));
        ordemServico.getItensProdutos().add(o);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void buscaItensServicoOS(OrdemServico ordemServico, Empresa empresa) {
    ordemServico.getItensServicos().clear();
    try {
      String SQL = "select * from os_servico_det where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        ItemServicoOS o = new ItemServicoOS();
        o.setOrdemServico(ordemServico);
        o.setMostrarQtde(Boolean.valueOf(rs.getBoolean("mostrar_qtd_os_det")));
        o.setObservacoes(rs.getString("obs_os_det"));
        o.setQuantidade(Double.valueOf(rs.getDouble("qtd_os_det")));
        o.setValorBruto(Double.valueOf(rs.getDouble("vlr_bruto_os_det")));
        o.setValorUnitario(Double.valueOf(rs.getDouble("vlr_unitario_os_det")));
        o.setServico(ServicoDao.buscaServico(Integer.valueOf(rs.getInt("cd_servico")), empresa));
        ordemServico.getItensServicos().add(o);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void buscaTestesEquipOS(OrdemServico ordemServico, Empresa empresa) {
    ordemServico.getTestesEquipamentos().clear();
    try {
      String SQL = "select * from os_tst_equip where cd_empresa = ? and cd_os_cab = ? and cd_equip = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS(), ordemServico.getEquipamento().getCodEquipamento() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        EquipamentoTeste e = new EquipamentoTeste();
        e.setCodTeste(Integer.valueOf(rs.getInt("cd_os_tst_equip")));
        e.setDefeito(rs.getString("def_enc_os_tst_equip"));
        e.setDescricao(rs.getString("desc_os_tst_equip"));
        e.setEquipamento(ordemServico.getEquipamento());
        ordemServico.getTestesEquipamentos().add(e);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Imagem getImagemConexao(Empresa empresa, Imagem p) throws IOException, SQLException {
    return ImagemDao.getImagemBanco("select cd_conexao_equip as cod, blob_conexao_equip as blob, md5_conexao_equip as md5, nome_conexao_equip as nome from conexao_equip where cd_empresa = " + empresa.getCodigo() + " and cd_conexao_equip = " + p.getCodImagem());
  }

  public static Integer imagemConexaoInDB(Empresa empresa, Imagem imagem) {
    try {
      String sql = "select * from conexao_equip where cd_empresa = ? and md5_conexao_equip like(?)";
      Object[] par = { empresa.getCodigo(), imagem.getMd5Imagem() };
      ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (res.next())
        return Integer.valueOf(res.getInt("cd_conexao_equip"));
    }
    catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }

  public static void excluiAllEnvolvidos(OrdemServico ordemServico, Empresa empresa) {
    try {
      String SQL = "delete from os_envolvidos where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiAllTestEquip(OrdemServico ordemServico, Empresa empresa) {
    try {
      String SQL = "delete from os_tst_equip where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiAllPropTipoEquip(OrdemServico ordemServico, Empresa empresa) {
    try {
      String SQL = "delete from os_prop_tipo_equip where cd_empresa = ? and cd_tipo_equip = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getEquipamento().getTipoEquipamento().getCodTipo(), ordemServico.getCodOS() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiAllItensProdOs(OrdemServico ordemServico, Empresa empresa) {
    try {
      String SQL = "delete from os_prod_det where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiAllItensServOs(OrdemServico ordemServico, Empresa empresa) {
    try {
      String SQL = "delete from os_servico_det where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiAllOrcamentos(OrdemServico ordemServico, Empresa empresa) {
    try {
      String SQL = "delete from orcamento where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void salvaAllItensServOs(OrdemServico ordemServico, Empresa empresa) {
    try {
      excluiAllItensServOs(ordemServico, empresa);
      SQL = "INSERT INTO os_servico_det (cd_empresa, cd_os_cab, cd_servico, qtd_os_det, vlr_unitario_os_det, vlr_bruto_os_det, mostrar_qtd_os_det, obs_os_det) VALUES(?,?,?,?,?,?,?,?)";

      for (ItemServicoOS o : ordemServico.getItensServicos()) {
        Object[] par = { empresa.getCodigo(), ordemServico.getCodOS(), o.getServico().getCodServico(), o.getQuantidade(), o.getValorUnitario(), o.getValorBruto(), o.getMostrarQtde(), o.getObservacoes() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
    }
    catch (Exception e)
    {
      String SQL;
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void salvaAllItensProdOs(OrdemServico ordemServico, Empresa empresa) {
    try {
      excluiAllItensProdOs(ordemServico, empresa);
      SQL = "INSERT INTO os_prod_det (cd_empresa, cd_os_cab, cd_produto, qtd_os_det, vlr_unitario_os_det, vlr_bruto_os_det, original_os_det) VALUES(?,?,?,?,?,?,?)";

      for (ItemProdOS o : ordemServico.getItensProdutos()) {
        Object[] par = { empresa.getCodigo(), ordemServico.getCodOS(), o.getProduto().getCodProduto(), o.getQuantidade(), o.getValorUnitario(), o.getValorBruto(), o.getOriginal() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
    }
    catch (Exception e)
    {
      String SQL;
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void salvaAllEnvolvidos(OrdemServico ordemServico, Empresa empresa) {
    try {
      excluiAllEnvolvidos(ordemServico, empresa);
      SQL = "INSERT INTO os_envolvidos (cd_empresa, cd_os_cab, cd_funcionario) VALUES(?,?,?)";
      for (Pessoa o : ordemServico.getEnvolvidos()) {
        Object[] par = { empresa.getCodigo(), ordemServico.getCodOS(), o.getCdPessoa() };
        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
    }
    catch (Exception e)
    {
      String SQL;
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void salvaAllTestEquip(OrdemServico ordemServico, Empresa empresa) {
    try {
      excluiAllTestEquip(ordemServico, empresa);
      SQL = "INSERT INTO os_tst_equip (cd_empresa, cd_os_cab, cd_equip, cd_os_tst_equip, desc_os_tst_equip, def_enc_os_tst_equip) VALUES(?,?,?,?,?,?)";

      for (EquipamentoTeste o : ordemServico.getTestesEquipamentos()) {
        Object[] par = { empresa.getCodigo(), ordemServico.getCodOS(), o.getEquipamento().getCodEquipamento(), NextDaos.nextTesteEquip(ordemServico, empresa), o.getDescricao(), o.getDefeito() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
    }
    catch (Exception e)
    {
      String SQL;
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void salvaAllPropTipoEquip(OrdemServico ordemServico, Empresa empresa) {
    try {
      excluiAllPropTipoEquip(ordemServico, empresa);
      SQL = "INSERT INTO os_prop_tipo_equip (cd_empresa, cd_tipo_equip, cd_prop_tipo_equip, cd_os_cab, valor_os_prop_tipo_equip) VALUES(?,?,?,?,?)";

      for (ItemPropTipoEquip o : ordemServico.getItemsPropriedades()) {
        Object[] par = { empresa.getCodigo(), ordemServico.getEquipamento().getTipoEquipamento().getCodTipo(), o.getPropTipoEquip().getCodPropTipoEquip(), ordemServico.getCodOS(), o.getValor() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
    }
    catch (Exception e)
    {
      String SQL;
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void salvaAllOrcamentos(OrdemServico ordemServico, Empresa empresa) {
    try {
      for (Orcamento o : ordemServico.getOrcamentos())
        if (o.getNroRevisao() != null) {
          String SQL = "UPDATE orcamento SET autorizado_orcamento = ?, data_auth_orcamento = ?, data_prev_orcamento = ?, vlr_bruto_orcamento = ?, vlr_liquido_orcamento = ?, vlr_desconto_orcamento = ?, vlr_frete_orcamento = ?, tipo_frete_orcamento = ?, usa_desc_cli_orcamento = ?, dias_validade_orcamento = ? WHERE cd_empresa = ? and cd_os_cab = ? and nro_revisao_orcamento = ?";

          Object[] par = { o.getAutorizado(), o.getDataAutorizacao(), o.getDataPrevisao(), o.getValorBruto(), o.getValorLiquido(), o.getValorDesconto(), o.getValorFrete(), o.getTipoFrete(), o.getUsoDescontoCliente(), o.getDiasValidade(), empresa.getCodigo(), ordemServico.getCodOS(), o.getNroRevisao() };

          DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        }
        else {
          String SQL = "INSERT INTO orcamento (cd_empresa, cd_os_cab, nro_revisao_orcamento, autorizado_orcamento, data_cad_orcamento, data_auth_orcamento, data_prev_orcamento, vlr_bruto_orcamento, vlr_liquido_orcamento, vlr_desconto_orcamento, vlr_frete_orcamento, tipo_frete_orcamento, usa_desc_cli_orcamento, dias_validade_orcamento) VALUES(?,?,?,?,CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?)";

          Object[] par = { empresa.getCodigo(), ordemServico.getCodOS(), NextDaos.nextOrcamento(ordemServico, empresa), o.getAutorizado(), o.getDataAutorizacao(), o.getDataPrevisao(), o.getValorBruto(), o.getValorLiquido(), o.getValorDesconto(), o.getValorFrete(), o.getTipoFrete(), o.getUsoDescontoCliente(), o.getDiasValidade() };

          DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        }
    }
    catch (Exception e)
    {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static boolean salvarOS(OrdemServico ordemServico, Empresa empresa) {
    try {
      EquipamentoDao.salvaEquipamento(ordemServico.getEquipamento(), empresa);
      if (ordemServico.getCodOS() != null) {
        String SQL = "UPDATE os_cab SET                  cd_cliente = ?,                 cd_contato_cliente = ?,                 cep_logradouro_cliente = ?,                 numero_endereco_cliente = ?,                 cd_equip = ?,                 cd_local_prod = ?,                 cd_transportadora = ?,                 nro_oc_cliente_os_cab = ?,                 defeito_reclamado_equip_os_cab = ?,                 estado_equip_os_cab = ?,                 conserto_detalhado_os_cab = ?,                 parecer_os_cab = ?,                 meses_garantia_os_cab = ?,                 data_cad_os_cab = ?,                 data_ent_os_cab = ?,                 data_saida_os_cab = ?,                 sob_garantia = ?,                 status_os_cab = ? WHERE cd_empresa = ? and cd_os_cab = ?";

        Object[] par = { ordemServico.getCliente().getCdPessoa(), ordemServico.getContatoCliente() != null ? ordemServico.getContatoCliente().getCodContato() : null, ordemServico.getEnderecoCliente().getCepLogradouro(), ordemServico.getEnderecoCliente().getNumero(), ordemServico.getEquipamento().getCodEquipamento(), ordemServico.getLocalizacao().getCodLocalProd(), ordemServico.getTransportadora().getCodigo(), ordemServico.getNroOcCliente(), ordemServico.getDefeitoReclamado(), ordemServico.getEstadoEquipamento(), ordemServico.getConsertoDetalhado(), ordemServico.getParecer(), ordemServico.getMesesGarantia(), ordemServico.getDataCadastro(), ordemServico.getDataEntrada(), ordemServico.getDataSaida(), ordemServico.getSobGarantia(), toFisicStatus(ordemServico.getStatusOS()), empresa.getCodigo(), ordemServico.getCodOS() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else
      {
        String SQL = "INSERT INTO os_cab (                 cd_empresa,                 cd_usuario,                 cd_cliente,                 cd_contato_cliente,                 cep_logradouro_cliente,                 numero_endereco_cliente,                 cd_equip,                 cd_local_prod,                 cd_transportadora,                 cd_os_cab,                 nro_oc_cliente_os_cab,                 defeito_reclamado_equip_os_cab,                 estado_equip_os_cab,                 conserto_detalhado_os_cab,                 parecer_os_cab,                 meses_garantia_os_cab,                 data_cad_os_cab,                 data_ent_os_cab,                 data_saida_os_cab,                 sob_garantia,                 status_os_cab) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,?,?,?,?) RETURNING cd_os_cab, data_cad_os_cab";

        Object[] par = { empresa.getCodigo(), ordemServico.getUsuario().getCodigoUsuario(), ordemServico.getCliente().getCdPessoa(), ordemServico.getContatoCliente() != null ? ordemServico.getContatoCliente().getCodContato() : null, ordemServico.getEnderecoCliente().getCepLogradouro(), ordemServico.getEnderecoCliente().getNumero(), ordemServico.getEquipamento().getCodEquipamento(), ordemServico.getLocalizacao().getCodLocalProd(), ordemServico.getTransportadora().getCodigo(), NextDaos.nextGenerico(empresa, "os_cab"), ordemServico.getNroOcCliente(), ordemServico.getDefeitoReclamado(), ordemServico.getEstadoEquipamento(), ordemServico.getConsertoDetalhado(), ordemServico.getParecer(), ordemServico.getMesesGarantia(), ordemServico.getDataEntrada(), ordemServico.getDataSaida(), ordemServico.getSobGarantia(), toFisicStatus(ordemServico.getStatusOS()) };

        ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        if (rs.next()) {
          ordemServico.setCodOS(Integer.valueOf(rs.getInt("cd_os_cab")));
          ordemServico.setDataCadastro(rs.getDate("data_cad_os_cab"));
        }
      }
      salvaAllEnvolvidos(ordemServico, empresa);
      salvaAllItensProdOs(ordemServico, empresa);
      salvaAllItensServOs(ordemServico, empresa);
      salvaAllOrcamentos(ordemServico, empresa);
      salvaAllTestEquip(ordemServico, empresa);
      salvaAllPropTipoEquip(ordemServico, empresa);
      buscaItensServicoOS(ordemServico, empresa);
      buscaItensProdOS(ordemServico, empresa);
      buscaEnvolvidosOS(ordemServico, empresa);
      buscaTestesEquipOS(ordemServico, empresa);
      buscaOrcamentosOS(ordemServico, empresa);
      buscaItemsPropTipoEq(ordemServico, ordemServico.getEquipamento().getTipoEquipamento(), empresa);
      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }

  public static boolean excluirOS(OrdemServico ordemServico, Empresa empresa) {
    try {
      String SQL = "delete from os_cab where cd_empresa = ? and cd_os_cab = ?";
      Object[] par = { empresa.getCodigo(), ordemServico.getCodOS() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }

  public static void buscaItemsPropTipoEq(OrdemServico os, TipoEquipamento tipo, Empresa empresa) {
    os.getItemsPropriedades().clear();
    if (tipo != null)
      try
      {
        String SQL = "select \titem.valor_os_prop_tipo_equip, \tprop.* from \tos_prop_tipo_equip as item, \tprop_tipo_equip as prop where \titem.cd_empresa = prop.cd_empresa and \titem.cd_tipo_equip = prop.cd_tipo_equip and \titem.cd_prop_tipo_equip = prop.cd_prop_tipo_equip and \titem.cd_empresa = ? and \titem.cd_tipo_equip = ? and \titem.cd_os_cab = ?";

        Object[] par = { empresa.getCodigo(), tipo.getCodTipo(), os.getCodOS() };
        ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        while (rs.next())
          os.getItemsPropriedades().add(new ItemPropTipoEquip(new PropriedadeTipoEquip(Integer.valueOf(rs.getInt("cd_prop_tipo_equip")), rs.getString("descr_prop_tipo_equip"), tipo), rs.getString("valor_os_prop_tipo_equip")));
      }
      catch (Exception e) {
        Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.OrdemServicoDao
 * JD-Core Version:    0.6.2
 */