package dao;

import beans.Empresa;
import beans.Endereco;
import beans.Variaveis;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilitarios.DataBase;
import utilitarios.Utils;

public abstract class EmpresaDao
{
  public static Empresa buscaEmpresa(Integer cd_empresa)
  {
    String sql = "select * from empresa where cd_empresa = ?";
    Object[] par = { cd_empresa };
    try {
      ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next()) {
        Empresa e = new Empresa();
        e.setCodigo(Integer.valueOf(rs.getInt("cd_empresa")));
        e.setCnpj(rs.getString("cnpj_empresa"));
        e.setEmail(rs.getString("email_empresa"));
        e.setFantasia(rs.getString("fantasia_empresa"));
        e.setFax(rs.getString("fax_empresa"));
        e.setFone(rs.getString("fone_empresa"));
        e.setIe(rs.getString("ie_empresa"));
        e.setResponsavel(rs.getString("responsavel_empresa"));
        e.setRsocial(rs.getString("razao_social_empresa"));
        e.setEndereco(EnderecoDao.buscaCEP(rs.getString("cep_logradouro")));
        if (e.getEndereco() != null) {
          e.getEndereco().setComplemento(rs.getString("complemento_endereco_empresa"));
          e.getEndereco().setNumero(Integer.valueOf(rs.getInt("numero_endereco_empresa")));
        }
        return e;
      }
    } catch (SQLException|NumberFormatException e) {
      Variaveis.addNewLog(EnderecoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.EmpresaDao
 * JD-Core Version:    0.6.2
 */