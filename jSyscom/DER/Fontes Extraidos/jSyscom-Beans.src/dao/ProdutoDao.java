package dao;

import beans.CategoriaProd;
import beans.Ean;
import beans.Empresa;
import beans.Fornecedor;
import beans.Imagem;
import beans.LocalProd;
import beans.Produto;
import beans.Unidade;
import beans.Variaveis;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utilitarios.DataBase;
import utilitarios.Utils;

public abstract class ProdutoDao
{
  public static Produto buscaProduto(Integer cd_produto, Empresa empresa, Boolean getImage)
  {
    Produto p = new Produto(cd_produto);
    buscaProduto(p, empresa, getImage);
    return p;
  }

  public static void buscaProduto(Produto produto, Empresa empresa, Boolean getImage) {
    try {
      String SQL = "select p.* from produto as p where p.cd_empresa = ? and p.cd_produto = ?";
      Object[] par = { empresa.getCodigo(), produto.getCodProduto() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        populaProdutoResultSet(rs, produto, empresa, getImage);
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  private static void excluiEans(Produto produto, Empresa empresa) {
    try {
      String SQL = "delete from ean_prod where cd_empresa = ? and cd_produto = ?";
      Object[] par = { empresa.getCodigo(), produto.getCodProduto() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  private static void excluiEan(Ean ean, Empresa empresa) {
    try {
      String SQL = "delete from ean_prod where cd_empresa = ? and cd_produto = ? and numero_ean_produto = ?";
      Object[] par = { empresa.getCodigo(), ean.getProduto().getCodProduto(), ean.getEan() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  private static void excluiFornecedores(Produto produto, Empresa empresa) {
    try {
      String SQL = "delete from fornecedor_prod where cd_empresa = ? and cd_produto = ?";
      Object[] par = { empresa.getCodigo(), produto.getCodProduto() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  private static void excluiFornecedor(Produto produto, Fornecedor fornecedor, Empresa empresa) {
    try {
      String SQL = "delete from fornecedor_prod where cd_empresa = ? and cd_produto = ? and cd_fornecedor = ?";
      Object[] par = { empresa.getCodigo(), produto.getCodProduto(), fornecedor.getCdPessoa() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiLocalProd(Empresa empresa, LocalProd local) {
    try {
      String SQL = "delete from local_prod where cd_empresa = ? and cd_local_prod = ?";
      Object[] par = { empresa.getCodigo(), local.getCodLocalProd() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Boolean salvaLocalProd(LocalProd local, Empresa empresa) {
    try {
      String SQL = "select * from local_prod where cd_empresa = ? and cd_local_prod = ?";
      Object[] par1 = { empresa.getCodigo(), local.getCodLocalProd() };
      ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        SQL = "update local_prod set descricao_local_prod = ? where cd_empresa = ? and cd_local_prod = ?";
        Object[] par2 = { local.getDescricao(), empresa.getCodigo(), Integer.valueOf(rs.getInt("cd_local_prod")) };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else {
        SQL = "insert into local_prod(cd_empresa, descricao_local_prod) values(?,?)";

        Object[] par2 = { empresa.getCodigo(), local.getDescricao() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }

      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static void excluiCategoriaProd(Empresa empresa, CategoriaProd cat) {
    try {
      String SQL = "delete from categoria_prod where cd_empresa = ? and cd_categoria_prod = ?";
      Object[] par = { empresa.getCodigo(), cat.getCodCategoria() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Boolean salvaCategoriaProd(CategoriaProd cat, Empresa empresa) {
    try {
      if (cat.getCodCategoria() == null) {
        String SQL = "insert into categoria_prod(cd_empresa, descricao_categoria_prod, margem_categoria_prod) values(?,?,?)";

        Object[] par2 = { empresa.getCodigo(), cat.getDescCategoria(), cat.getMargemCategoria() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else {
        String SQL = "update categoria_prod set descricao_categoria_prod = ?, margem_categoria_prod = ? where cd_empresa = ? and cd_categoria_prod = ?";
        Object[] par2 = { cat.getDescCategoria(), cat.getMargemCategoria(), empresa.getCodigo(), cat.getCodCategoria() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }

      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static void buscaEansProduto(Produto produto, Empresa empresa) {
    produto.getEans().clear();
    try {
      String SQL = "select * from ean_prod where cd_empresa = ? and cd_produto = ? order by numero_ean_produto";
      Object[] par = { empresa.getCodigo(), produto.getCodProduto() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Ean e = new Ean();
        e.setEan(rs.getString("numero_ean_produto"));
        e.setDataCad(rs.getDate("data_cad_ean_produto"));
        e.setProduto(produto);
        produto.getEans().add(e);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  private static void salvaEan(Produto produto, Ean ean, Empresa empresa) {
    try {
      String SQL = "insert into ean_prod(cd_empresa, cd_produto, numero_ean_produto, data_cad_ean_produto) values(?,?,?,current_date)";

      Object[] par = { empresa.getCodigo(), produto.getCodProduto(), ean.getEan() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  private static void salvaEans(Produto produto, Empresa empresa) {
    for (Ean e : produto.getEans())
      salvaEan(produto, e, empresa);
  }

  public static void buscaFornecedoresProduto(Produto produto, Empresa empresa)
  {
    produto.getFornecedores().clear();
    try {
      String SQL = "select * from fornecedor_prod where cd_empresa = ? and cd_produto = ? order by cd_fornecedor";
      Object[] par = { empresa.getCodigo(), produto.getCodProduto() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Fornecedor f = new Fornecedor(Integer.valueOf(rs.getInt("cd_fornecedor")));
        PessoaDao.buscaFornecedor(f, empresa);
        produto.getFornecedores().add(f);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  private static void salvaFornecedor(Produto produto, Fornecedor fornecedor, Empresa empresa) {
    try {
      String SQL = "insert into fornecedor_prod(cd_empresa, cd_produto, cd_fornecedor, data_cadastro) values(?,?,?,current_date)";
      Object[] par = { empresa.getCodigo(), produto.getCodProduto(), fornecedor.getCdPessoa() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  private static void salvaFornecedores(Produto produto, Empresa empresa) {
    for (Fornecedor f : produto.getFornecedores())
      salvaFornecedor(produto, f, empresa);
  }

  public static Boolean isBusyEan(Ean ean, Empresa empresa)
  {
    try {
      String SQL = "select * from ean_prod where cd_empresa = ? and numero_ean_produto = ?";
      Object[] par = { empresa.getCodigo(), ean.getEan() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return Boolean.valueOf(true);
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean isBusyUnidade(Unidade unidade, Empresa empresa) {
    try {
      String SQL = "select * from produto where cd_empresa = ? and sigla_unidade_m = ?";
      Object[] par = { empresa.getCodigo(), unidade.getSigla() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return Boolean.valueOf(true);
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean isBusyCategoria(CategoriaProd cat, Empresa empresa) {
    try {
      String SQL = "select * from produto where cd_empresa = ? and cd_categoria_prod = ?";
      Object[] par = { empresa.getCodigo(), cat.getCodCategoria() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return Boolean.valueOf(true);
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean isBusyLocal(LocalProd local, Empresa empresa) {
    try {
      String SQL = "select * from produto where cd_empresa = ? and cd_local_prod = ?";
      Object[] par = { empresa.getCodigo(), local.getCodLocalProd() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return Boolean.valueOf(true);
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static ArrayList<CategoriaProd> getAllCategorias(Empresa empresa) {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from categoria_prod where cd_empresa = ?";
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        CategoriaProd cp = new CategoriaProd();
        cp.setCodCategoria(Integer.valueOf(rs.getInt("cd_categoria_prod")));
        cp.setDescCategoria(rs.getString("descricao_categoria_prod"));
        cp.setMargemCategoria(Double.valueOf(rs.getDouble("margem_categoria_prod")));
        lista.add(cp);
      }
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return lista;
  }

  public static ArrayList<Unidade> getAllUnidades(Empresa empresa) {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from unidade_medida where cd_empresa = ?";
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Unidade u = new Unidade();
        u.setNome(rs.getString("nome_unidade_m"));
        u.setSigla(rs.getString("sigla_unidade_m"));
        lista.add(u);
      }
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return lista;
  }

  public static ArrayList<LocalProd> getAllLocaisProdutos(Empresa empresa) {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from local_prod where cd_empresa = ?";
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        LocalProd lp = new LocalProd();
        lp.setCodLocalProd(Integer.valueOf(rs.getInt("cd_local_prod")));
        lp.setDescricao(rs.getString("descricao_local_prod"));
        lista.add(lp);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return lista;
  }

  public static Unidade getUnidade(String sigla_unidade_m, Empresa empresa) {
    Unidade u = new Unidade();
    u.setSigla(sigla_unidade_m);
    getUnidade(u, empresa);
    return u;
  }

  public static void getUnidade(Unidade u, Empresa empresa) {
    if (u != null)
      try {
        String SQL = "select * from unidade_medida where cd_empresa = ? and sigla_unidade_m = ?";
        Object[] par = { empresa.getCodigo(), u.getSigla() };
        ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        if (rs.next())
          u.setNome(rs.getString("nome_unidade_m"));
      }
      catch (Exception e) {
        Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
  }

  public static LocalProd getLocalProd(Integer cd_local_prod, Empresa empresa)
  {
    LocalProd l = new LocalProd(cd_local_prod);
    getLocalProd(l, empresa);
    return l;
  }

  public static void getLocalProd(LocalProd local, Empresa empresa) {
    if (local != null)
      try {
        String SQL = "select * from local_prod where cd_empresa = ? and cd_local_prod = ?";
        Object[] par1 = { empresa.getCodigo(), local.getCodLocalProd() };
        ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        if (rs.next())
          local.setDescricao(rs.getString("descricao_local_prod"));
      }
      catch (SQLException ex) {
        Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
  }

  public static CategoriaProd getCategoriaProd(Integer cd_categoria_prod, Empresa empresa)
  {
    CategoriaProd c = new CategoriaProd(cd_categoria_prod);
    getCategoriaProd(c, empresa);
    return c;
  }

  public static void getCategoriaProd(CategoriaProd categoria, Empresa empresa)
  {
    if (categoria != null)
      try {
        String SQL = "select * from categoria_prod where cd_empresa = ? and cd_categoria_prod = ?";
        Object[] par1 = { empresa.getCodigo(), categoria.getCodCategoria() };
        ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        if (rs.next()) {
          categoria.setDescCategoria(rs.getString("descricao_categoria_prod"));
          categoria.setMargemCategoria(Double.valueOf(rs.getDouble("margem_categoria_prod")));
        }
      } catch (SQLException ex) {
        Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
  }

  public static Boolean salvarProduto(Produto produto, Empresa empresa)
  {
    try {
      if (produto.getCodProduto() != null) {
        String SQL = "UPDATE produto SET cd_categoria_prod = ?, cd_imagem_prod = ?, sigla_unidade_m = ?, cd_local_prod = ?, descricao_produto = ?, vlr_venda_produto = ?, situacao_produto = ?, qtd_est_min = ? WHERE cd_empresa = ? and cd_produto = ?";

        Object[] par = { produto.getCategoriaProd().getCodCategoria(), produto.getImagemProduto().getCodImagem(), produto.getUnidade().getSigla(), produto.getLocalProd().getCodLocalProd(), produto.getDescricaoProduto(), produto.getValorVendaProduto(), produto.getSituacaoProduto(), produto.getEstMinimo(), empresa.getCodigo(), produto.getCodProduto() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

        excluiFornecedores(produto, empresa);
        salvaFornecedores(produto, empresa);
        buscaFornecedoresProduto(produto, empresa);
        excluiEans(produto, empresa);
        salvaEans(produto, empresa);
        buscaEansProduto(produto, empresa);
      } else {
        String SQL = "INSERT INTO produto (cd_empresa, cd_produto, cd_categoria_prod, cd_imagem_prod, sigla_unidade_m, cd_local_prod, descricao_produto, vlr_venda_produto, situacao_produto, qtd_est_min, data_cad_produto) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, current_date) RETURNING cd_produto";

        Object[] par = { empresa.getCodigo(), NextDaos.nextGenerico(empresa, "produto"), produto.getCategoriaProd().getCodCategoria(), produto.getImagemProduto().getCodImagem(), produto.getUnidade().getSigla(), produto.getLocalProd().getCodLocalProd(), produto.getDescricaoProduto(), produto.getValorVendaProduto(), produto.getSituacaoProduto(), produto.getEstMinimo() };

        ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        if (rs.next())
          produto.setCodProduto(Integer.valueOf(rs.getInt("cd_produto")));
        else {
          return Boolean.valueOf(false);
        }
      }
      if (!atuEstoque(produto, empresa)) {
        Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n Erro ao gravar quantidade em estoque do produto!", Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean excluirProduto(Produto produto, Empresa empresa) {
    try {
      String SQL = "delete from produto where cd_empresa = ? and cd_produto = ?";
      Object[] par = { empresa.getCodigo(), produto.getCodProduto() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static void populaProdutoResultSet(ResultSet rs, Produto p, Empresa empresa, Boolean getImage) {
    try {
      p.setCodProduto(Integer.valueOf(rs.getInt("cd_produto")));
      p.setCategoriaProd(getCategoriaProd(Integer.valueOf(rs.getInt("cd_categoria_prod")), empresa));
      p.setDataCadastroProduto(rs.getDate("data_cad_produto"));
      p.setDescricaoProduto(rs.getString("descricao_produto").trim());
      p.setSituacaoProduto(Boolean.valueOf(rs.getBoolean("situacao_produto")));
      p.setValorCompraProduto(Double.valueOf(rs.getDouble("vlr_compra_produto")));
      p.setValorVendaProduto(Double.valueOf(rs.getDouble("vlr_venda_produto")));
      p.setEstMinimo(Double.valueOf(rs.getDouble("qtd_est_min")));
      p.setLocalProd(getLocalProd(Integer.valueOf(rs.getInt("cd_local_prod")), empresa));
      p.setUnidade(getUnidade(rs.getString("sigla_unidade_m"), empresa));
      if (getImage.booleanValue()) {
        p.setImagemProduto(getImagemProduto(Variaveis.getEmpresa(), new Imagem(Integer.valueOf(rs.getInt("cd_imagem_prod")))));
      }
      getEstoque(p, empresa);
      buscaEansProduto(p, empresa);
      buscaFornecedoresProduto(p, empresa);
      p.setFlagPronto(Boolean.valueOf(true));
    } catch (IOException|SQLException ex) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Imagem getImagemProduto(Empresa empresa, Imagem p) throws IOException, SQLException {
    return ImagemDao.getImagemBanco("select cd_imagem_prod as cod, blob_imagem_prod as blob, md5_imagem_prod as md5, nome_imagem_prod as nome from imagem_prod where cd_empresa = " + empresa.getCodigo() + " and cd_imagem_prod = " + p.getCodImagem());
  }

  public static Integer imagemProdInDB(Empresa empresa, Imagem imagem) {
    try {
      String sql = "select * from imagem_prod where cd_empresa = ? and md5_imagem_prod like(?)";
      Object[] par = { empresa.getCodigo(), imagem.getMd5Imagem() };
      ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (res.next())
        return Integer.valueOf(res.getInt("cd_imagem_prod"));
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }

  public static void getEstoque(Produto p, Empresa empresa) {
    if (p != null)
      try {
        String SQL = "select quantidade_estoque from estoque_prod where cd_empresa = ? and cd_produto = ?";
        Object[] par = { empresa.getCodigo(), p.getCodProduto() };
        ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        if (rs.next()) {
          p.setQtdEstoque(Double.valueOf(rs.getDouble("quantidade_estoque")));
        } else {
          SQL = "INSERT INTO estoque_prod (cd_empresa, cd_produto, quantidade_estoque, dt_alt_estoque) VALUES (?, ?, 0, CURRENT_DATE)";
          DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        }
      }
      catch (Exception e) {
        Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
  }

  public static boolean atuEstoque(Produto p, Empresa empresa)
  {
    if (p != null) {
      try {
        String SQL = "update estoque_prod set quantidade_estoque = ? where cd_empresa = ? and cd_produto = ?";
        Object[] par = { p.getQtdEstoque(), empresa.getCodigo(), p.getCodProduto() };
        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

        return true;
      } catch (Exception e) {
        Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
    }
    return false;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.ProdutoDao
 * JD-Core Version:    0.6.2
 */