package dao;

import beans.Empresa;
import beans.Transportadora;
import beans.Variaveis;
import java.sql.ResultSet;
import java.util.ArrayList;
import utilitarios.DataBase;
import utilitarios.Utils;

public class TransportadoraDao
{
  public static Transportadora buscaTransportadora(Integer codTransportadora, Empresa empresa)
  {
    Transportadora o = new Transportadora(codTransportadora);
    buscaTransportadora(o, empresa);
    return o;
  }

  public static void buscaTransportadora(Transportadora transp, Empresa empresa) {
    try {
      String SQL = "select * from transportadora where cd_empresa = ? and cd_transportadora = ?";
      Object[] par = { empresa.getCodigo(), transp.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        transp.setNome(rs.getString("nome_transportadora"));
        transp.setTelefone(rs.getString("telefone_transportadora"));
        transp.setDescricao(rs.getString("descr_transportadora"));
      }
    } catch (Exception e) {
      Variaveis.addNewLog(TransportadoraDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static boolean isBusyTransportadora(Transportadora transp, Empresa empresa) {
    try {
      String SQL = "select * from os_cab where cd_empresa = ? and cd_transportadora = ?";
      Object[] par = { empresa.getCodigo(), transp.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return true;
    }
    catch (Exception e) {
      Variaveis.addNewLog(TransportadoraDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }

  public static void excluiTransportadora(Empresa empresa, Transportadora transp) {
    try {
      String SQL = "delete from transportadora where cd_empresa = ? and cd_transportadora = ?";
      Object[] par = { empresa.getCodigo(), transp.getCodigo() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(TransportadoraDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static ArrayList<Transportadora> getAllTransportadoras(Empresa empresa) {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from transportadora where cd_empresa = ? order by nome_transportadora";
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Transportadora t = new Transportadora();
        t.setCodigo(Integer.valueOf(rs.getInt("cd_transportadora")));
        t.setNome(rs.getString("nome_transportadora"));
        t.setTelefone(rs.getString("telefone_transportadora"));
        t.setDescricao(rs.getString("descr_transportadora"));
        lista.add(t);
      }
    }
    catch (Exception e) {
      Variaveis.addNewLog(TransportadoraDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return lista;
  }

  public static boolean salvaTransportadora(Transportadora transp, Empresa empresa) {
    try {
      if (transp.getCodigo() == null) {
        String SQL = "insert into transportadora(cd_empresa, cd_transportadora, nome_transportadora, telefone_transportadora, descr_transportadora, data_cad_transportadora) values(?, " + NextDaos.nextGenerico(empresa, "transportadora") + ", ?, ?, ?, current_date)";
        Object[] par2 = { empresa.getCodigo(), transp.getNome().trim(), transp.getTelefone(), transp.getDescricao() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else {
        String SQL = "update transportadora set nome_transportadora= ?, telefone_transportadora = ?, descr_transportadora = ? where cd_empresa = ? and cd_transportadora = ?";
        Object[] par2 = { transp.getNome().trim(), transp.getTelefone(), transp.getDescricao(), empresa.getCodigo(), transp.getCodigo() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }

      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(TransportadoraDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.TransportadoraDao
 * JD-Core Version:    0.6.2
 */