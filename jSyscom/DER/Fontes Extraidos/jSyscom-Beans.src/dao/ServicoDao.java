package dao;

import beans.Empresa;
import beans.Servico;
import beans.Variaveis;
import java.sql.ResultSet;
import java.util.ArrayList;
import utilitarios.DataBase;
import utilitarios.Utils;

public abstract class ServicoDao
{
  public static Servico buscaServico(Integer codServico, Empresa empresa)
  {
    Servico o = new Servico(codServico);
    buscaServico(o, empresa);
    return o;
  }

  public static void buscaServico(Servico serv, Empresa empresa) {
    try {
      String SQL = "select * from servico where cd_empresa = ? and cd_servico = ?";
      Object[] par = { empresa.getCodigo(), serv.getCodServico() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        serv.setDescServico(rs.getString("descricao_servico"));
        serv.setValorServico(Double.valueOf(rs.getDouble("vlr_servico")));
        serv.setDataCadastro(rs.getDate("data_cad_servico"));
      }
    } catch (Exception e) {
      Variaveis.addNewLog(ServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static boolean isBusyServico(Servico servico, Empresa empresa) {
    try {
      String SQL = "select * from os_servico_det where cd_empresa = ? and cd_servico = ?";
      Object[] par = { empresa.getCodigo(), servico.getCodServico() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return true;
    }
    catch (Exception e) {
      Variaveis.addNewLog(ServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }

  public static void excluiServico(Empresa empresa, Servico servico) {
    try {
      String SQL = "delete from servico where cd_empresa = ? and cd_servico = ?";
      Object[] par = { empresa.getCodigo(), servico.getCodServico() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(ServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static ArrayList<Servico> getAllServicos(Empresa empresa) {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from servico where cd_empresa = ? order by descricao_servico";
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Servico s = new Servico();
        s.setCodServico(Integer.valueOf(rs.getInt("cd_servico")));
        s.setDescServico(rs.getString("descricao_servico"));
        s.setValorServico(Double.valueOf(rs.getDouble("vlr_servico")));
        lista.add(s);
      }
    }
    catch (Exception e) {
      Variaveis.addNewLog(ServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return lista;
  }

  public static boolean salvaServico(Servico servico, Empresa empresa) {
    try {
      if (servico.getCodServico() == null) {
        String SQL = "insert into servico(cd_empresa, cd_servico, descricao_servico, vlr_servico, data_cad_servico) values(?, " + NextDaos.nextGenerico(empresa, "servico") + ", ?, ?, current_date)";
        Object[] par2 = { empresa.getCodigo(), servico.getDescServico().trim(), servico.getValorServico() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else {
        String SQL = "update servico set descricao_servico = ?, vlr_servico = ? where cd_empresa = ? and cd_servico = ?";
        Object[] par2 = { servico.getDescServico().trim(), servico.getValorServico(), empresa.getCodigo(), servico.getCodServico() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }

      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(TransportadoraDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.ServicoDao
 * JD-Core Version:    0.6.2
 */