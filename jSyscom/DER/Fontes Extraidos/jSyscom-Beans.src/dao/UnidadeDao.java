package dao;

import beans.Empresa;
import beans.Unidade;
import beans.Variaveis;
import java.sql.ResultSet;
import java.util.ArrayList;
import utilitarios.DataBase;
import utilitarios.Utils;

public class UnidadeDao
{
  public static ArrayList<Unidade> listarUnidades(Empresa empresa)
  {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from unidade_medida where cd_empresa = ? order by sigla_unidade_m";
      ResultSet rs = DataBase.executeQuery(SQL, new Object[] { empresa.getCodigo() }, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      while (rs.next()) {
        Unidade u = new Unidade();
        u.setNome(rs.getString("nome_unidade_m"));
        u.setSigla(rs.getString("sigla_unidade_m"));
        lista.add(u);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(NextDaos.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return lista;
  }

  public static Boolean salvarUnidade(Empresa empresa, Unidade unidade) {
    try {
      String SQL = "select * from unidade_medida where cd_empresa = ? and UPPER(sigla_unidade_m) like(?)";
      Object[] par1 = { empresa.getCodigo(), unidade.getSigla() };
      ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        SQL = "update unidade_medida set nome_unidade_m = ? where cd_empresa = ? and sigla_unidade_m like(?)";
        Object[] par2 = { unidade.getNome(), empresa.getCodigo(), rs.getString("sigla_unidade_m") };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else {
        SQL = "INSERT INTO unidade_medida (cd_empresa, sigla_unidade_m, nome_unidade_m) values (?,?,?)";
        Object[] par2 = { empresa.getCodigo(), unidade.getSigla(), unidade.getNome() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }

      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(NextDaos.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean excluirUnidade(Empresa empresa, Unidade unidade) {
    try {
      String SQL = "DELETE FROM unidade_medida WHERE cd_empresa = ? and sigla_unidade_m = ?";
      Object[] par1 = { empresa.getCodigo(), unidade.getSigla() };
      DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(NextDaos.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.UnidadeDao
 * JD-Core Version:    0.6.2
 */