package dao;

import beans.Cliente;
import beans.Contato;
import beans.Empresa;
import beans.Endereco;
import beans.Fornecedor;
import beans.Funcionario;
import beans.Pessoa;
import beans.Variaveis;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utilitarios.DataBase;
import utilitarios.Utils;
import utilitarios.Validador;

public abstract class PessoaDao
{
  public static final String CLIENTE = "cliente";
  public static final String FORNECEDOR = "fornecedor";
  public static final String FUNCIONARIO = "funcionario";

  public static void buscaPessoa(Pessoa p, Empresa empresa)
  {
    try
    {
      String SQL = "select * from pessoa where cd_empresa = ? and cd_pessoa = ?";
      Object[] par = { empresa.getCodigo(), p.getCdPessoa() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        populaPessoaResultSet(rs, p, empresa);
    }
    catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void populaPessoaResultSet(ResultSet rs, Pessoa p, Empresa empresa) {
    try {
      p.setCdPessoa(Integer.valueOf(rs.getInt("cd_pessoa")));
      p.setCpfCnpjPessoa(rs.getString("cpf_cnpj_pessoa"));
      p.setDataCadPessoa(rs.getDate("data_cad_pessoa"));
      p.setDataExpRgPessoa(rs.getDate("data_exp_rg_pessoa"));
      p.setDataNascimento(rs.getDate("data_nascimento"));
      p.setDescricaoPessoa(rs.getString("descricao_pessoa"));
      p.setEmailPessoa(rs.getString("email_pessoa"));
      p.setEmpresaPessoa(rs.getString("empresa_pessoa"));
      p.setFoneEmpresaPessoa(rs.getString("fone_empresa_pessoa"));
      p.setRgIePessoa(rs.getString("rg_ie_pessoa"));
      p.setNomePessoa(rs.getString("nome_pessoa"));
      p.setOrgaoRgPessoa(rs.getString("orgao_rg_pessoa"));
      p.setProfissaoPessoa(rs.getString("profissao_pessoa"));
      p.setTipoPessoa(rs.getString("tipo_pessoa").charAt(0));
      if (p.getTipoPessoa() == 'F') {
        p.setSexoPessoa(rs.getString("sexo_pessoa").charAt(0));
      }
      p.setSitePessoa(rs.getString("site_pessoa"));
      p.setTelefoneCellPessoa(rs.getString("telefone_cell_pessoa"));
      p.setTelefoneFixoPessoa(rs.getString("telefone_fixo_pessoa"));
      p.setCliente(buscaAssoc(p, Variaveis.getEmpresa(), "cliente"));
      p.setFuncionario(buscaAssoc(p, Variaveis.getEmpresa(), "funcionario"));
      p.setFornecedor(buscaAssoc(p, Variaveis.getEmpresa(), "fornecedor"));
      buscaEnderecosPessoa(p, empresa);
      buscaContatosPessoa(p, empresa);
      p.setFlagPronto(Boolean.valueOf(true));
    } catch (SQLException ex) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Pessoa buscaPessoa(Integer cd_pessoa, Empresa empresa) {
    Pessoa p = new Pessoa(cd_pessoa);
    buscaPessoa(p, empresa);
    return p;
  }

  public static Funcionario buscaFuncionario(Integer cd_funcionario, Empresa empresa) {
    Funcionario f = new Funcionario(cd_funcionario);
    buscaFuncionario(f, empresa);
    return f;
  }

  public static void buscaFuncionario(Funcionario funcionario, Empresa empresa) {
    try {
      String SQL = "select * from funcionario where cd_empresa = ? and cd_funcionario = ?";
      Object[] par = { empresa.getCodigo(), funcionario.getCdPessoa() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        buscaPessoa(funcionario, empresa);
    }
    catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Fornecedor buscaFornecedor(Integer cd_fornecedor, Empresa empresa) {
    Fornecedor f = new Fornecedor(cd_fornecedor);
    buscaFornecedor(f, empresa);
    return f;
  }

  public static void buscaFornecedor(Fornecedor fornecedor, Empresa empresa) {
    try {
      String SQL = "select * from fornecedor where cd_empresa = ? and cd_fornecedor = ?";
      Object[] par = { empresa.getCodigo(), fornecedor.getCdPessoa() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        buscaPessoa(fornecedor, empresa);
    }
    catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Cliente buscaCliente(Integer cd_cliente, Empresa empresa) {
    Cliente c = new Cliente(cd_cliente);
    buscaCliente(c, empresa);
    return c;
  }

  public static void buscaCliente(Cliente cliente, Empresa empresa) {
    try {
      String SQL = "select * from cliente where cd_empresa = ? and cd_cliente = ?";
      Object[] par = { empresa.getCodigo(), cliente.getCdPessoa() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        buscaPessoa(cliente, empresa);
    }
    catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void buscaEnderecosPessoa(Pessoa pessoa, Empresa empresa) {
    pessoa.getEnderecos().clear();
    try {
      String SQL = "select * from endereco_pessoa where cd_empresa = ? and cd_pessoa = ?";
      Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Endereco e = EnderecoDao.buscaCEP(rs.getString("cep_logradouro"));
        e.setNumero(Integer.valueOf(rs.getInt("numero_endereco_pessoa")));
        e.setComplemento(rs.getString("complemento_endereco_pessoa"));
        e.setPrincipal(Boolean.valueOf(rs.getBoolean("principal_endereco_pessoa")));
        pessoa.getEnderecos().add(e);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Endereco getEnderecoPessoa(Pessoa pessoa, String cep_logradouro, Integer nro, Empresa empresa) {
    try {
      String SQL = "select * from endereco_pessoa where cd_empresa = ? and cd_pessoa = ? and cep_logradouro = ? and numero_endereco_pessoa = ?";
      Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa(), cep_logradouro, nro };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        Endereco e = EnderecoDao.buscaCEP(rs.getString("cep_logradouro"));
        e.setNumero(Integer.valueOf(rs.getInt("numero_endereco_pessoa")));
        e.setComplemento(rs.getString("complemento_endereco_pessoa"));
        e.setPrincipal(Boolean.valueOf(rs.getBoolean("principal_endereco_pessoa")));
        return e;
      }
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }

  public static Boolean contemEnderecoPrincipal(Pessoa p) {
    for (Endereco e : p.getEnderecos()) {
      if (e.getPrincipal().booleanValue()) {
        return Boolean.valueOf(true);
      }
    }
    return Boolean.valueOf(false);
  }

  public static void buscaContatosPessoa(Pessoa pessoa, Empresa empresa) {
    pessoa.getContatos().clear();
    try {
      String SQL = "select * from contato where cd_empresa = ? and cd_pessoa = ? order by nome_contato";
      Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        Contato c = new Contato();
        c.setCodContato(Integer.valueOf(rs.getInt("cd_contato")));
        c.setCargoContato(rs.getString("cargo_contato"));
        c.setNomeContato(rs.getString("nome_contato"));
        c.setObservacao(rs.getString("obs_contato"));
        c.setTelefoneContato(rs.getString("telefone_contato"));
        c.setPessoaContato(pessoa);
        pessoa.getContatos().add(c);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiContato(Contato contato, Empresa empresa) {
    if (contato.getCodContato() == null)
      Variaveis.addNewLog("Falha ao excluir contato!", Boolean.valueOf(false), Boolean.valueOf(true), null);
    else
      try {
        String SQL = "delete from contato where cd_empresa = ? and cd_pessoa = ? and cd_contato = ?";
        Object[] par = { empresa.getCodigo(), contato.getPessoaContato().getCdPessoa(), contato.getCodContato() };
        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      catch (Exception e) {
        Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(false), null);
      }
  }

  public static Contato getContato(Pessoa pessoa, Integer cd_contato, Empresa empresa)
  {
    Contato c = new Contato(cd_contato);
    c.setPessoaContato(pessoa);
    getContato(c, empresa);
    return c;
  }

  public static void getContato(Contato contato, Empresa empresa) {
    try {
      String SQL = "select * from contato where cd_empresa = ? and cd_pessoa = ? and cd_contato = ?";
      Object[] par = { empresa.getCodigo(), contato.getPessoaContato().getCdPessoa(), contato.getCodContato() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        contato.setCargoContato(rs.getString("cargo_contato"));
        contato.setNomeContato(rs.getString("nome_contato"));
        contato.setObservacao(rs.getString("obs_contato"));
        contato.setTelefoneContato(rs.getString("telefone_contato"));
      }
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiEndereco(Pessoa pessoa, Endereco endereco, Empresa empresa) {
    try {
      String SQL = "delete from endereco_pessoa where cd_empresa = ? and cd_pessoa = ? and cep_logradouro = ? and numero_endereco_pessoa = ?";

      Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa(), endereco.getCepLogradouro(), endereco.getNumero() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog("Falha ao excluir endereco: " + endereco.toString(), Boolean.valueOf(false), Boolean.valueOf(true), null);
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(false), null);
    }
  }

  public static Boolean buscaAssoc(Pessoa pessoa, Empresa empresa, String tipo) {
    String sql = "select * from " + tipo + " where cd_empresa = ? and cd_" + tipo + " = ?";
    Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa() };
    try {
      ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        if (tipo.toLowerCase().trim().equals("cliente")) {
          pessoa.setDescontoCliente(Double.valueOf(rs.getDouble("desconto_cliente")));
        }
        if (tipo.toLowerCase().trim().equals("funcionario")) {
          pessoa.setValorHoraFuncionario(Double.valueOf(rs.getDouble("custo_hora_funcionario")));
        }
        return Boolean.valueOf(true);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean associarPessoa(Pessoa pessoa, Empresa empresa, String tipoPessoa) {
    String SQL = "insert into " + tipoPessoa + " (cd_empresa, cd_" + tipoPessoa + ") values (?,?) RETURNING cd_" + tipoPessoa;
    try {
      Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        if (tipoPessoa.toLowerCase().trim().equals("cliente")) {
          SQL = "UPDATE cliente SET desconto_cliente = ? WHERE cd_empresa = ? and cd_" + tipoPessoa + " = ?";
          Object[] par2 = { pessoa.getDescontoCliente(), empresa.getCodigo(), pessoa.getCdPessoa() };
          DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        }

        if (tipoPessoa.toLowerCase().trim().equals("funcionario")) {
          SQL = "UPDATE funcionario SET custo_hora_funcionario = ? WHERE cd_empresa = ? and cd_" + tipoPessoa + " = ?";
          Object[] par2 = { pessoa.getValorHoraFuncionario(), empresa.getCodigo(), pessoa.getCdPessoa() };
          DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        }

        return Boolean.valueOf(true);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static boolean desassociarPessoa(Pessoa pessoa, Empresa empresa, String tipoPessoa) {
    String SQL = "delete from " + tipoPessoa + " where cd_empresa = ? and cd_" + tipoPessoa + " = ?";
    try {
      Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }

  public static boolean excluirPessoa(Pessoa pessoa, Empresa empresa) {
    try {
      String SQL = "delete from pessoa where cd_empresa = ? and cd_pessoa = ?";
      Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }

  public static boolean salvarPessoa(Pessoa pessoa, Empresa empresa) {
    try {
      if (pessoa.getCdPessoa() != null) {
        String SQL = "Update pessoa set tipo_pessoa = ?, nome_pessoa = ?, data_nascimento = ?, descricao_pessoa = ?, email_pessoa = ?, site_pessoa = ?, telefone_fixo_pessoa = ?, telefone_cell_pessoa = ?, cpf_cnpj_pessoa = ?, rg_ie_pessoa = ?, orgao_rg_pessoa = ?, data_exp_rg_pessoa = ?, profissao_pessoa = ?, empresa_pessoa = ?, fone_empresa_pessoa = ?, sexo_pessoa = ? where cd_empresa = ? and cd_pessoa = ?";

        Object[] par = { (pessoa.getTipoPessoa() + "").trim(), pessoa.getNomePessoa(), pessoa.getDataNascimento(), pessoa.getDescricaoPessoa(), pessoa.getEmailPessoa(), pessoa.getSitePessoa(), Validador.soNumeros(pessoa.getTelefoneFixoPessoa()), Validador.soNumeros(pessoa.getTelefoneCellPessoa()), Validador.soNumeros(pessoa.getCpfCnpjPessoa()), pessoa.getRgIePessoa(), pessoa.getOrgaoRgPessoa(), pessoa.getDataExpRgPessoa(), pessoa.getProfissaoPessoa().trim(), pessoa.getEmpresaPessoa(), Validador.soNumeros(pessoa.getFoneEmpresaPessoa()), (pessoa.getSexoPessoa() + "").trim().toUpperCase(), empresa.getCodigo(), pessoa.getCdPessoa() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

        if (buscaAssoc(new Pessoa(pessoa.getCdPessoa()), empresa, "cliente").booleanValue()) {
          if (!pessoa.getCliente().booleanValue()) {
            desassociarPessoa(pessoa, Variaveis.getEmpresa(), "cliente");
          } else {
            SQL = "UPDATE cliente SET desconto_cliente = ? WHERE cd_empresa = ? and cd_cliente = ?";
            Object[] par2 = { pessoa.getDescontoCliente(), empresa.getCodigo(), pessoa.getCdPessoa() };
            DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
          }

        }
        else if (pessoa.getCliente().booleanValue()) {
          associarPessoa(pessoa, Variaveis.getEmpresa(), "cliente");
        }

        if (buscaAssoc(new Pessoa(pessoa.getCdPessoa()), empresa, "fornecedor").booleanValue()) {
          if (!pessoa.getFornecedor().booleanValue()) {
            desassociarPessoa(pessoa, Variaveis.getEmpresa(), "fornecedor");
          }
        }
        else if (pessoa.getFornecedor().booleanValue()) {
          associarPessoa(pessoa, Variaveis.getEmpresa(), "fornecedor");
        }

        if (buscaAssoc(new Pessoa(pessoa.getCdPessoa()), empresa, "funcionario").booleanValue()) {
          if (!pessoa.getFuncionario().booleanValue()) {
            desassociarPessoa(pessoa, Variaveis.getEmpresa(), "funcionario");
          } else {
            SQL = "UPDATE funcionario SET custo_hora_funcionario = ? WHERE cd_empresa = ? and cd_funcionario = ?";
            Object[] par2 = { pessoa.getValorHoraFuncionario(), empresa.getCodigo(), pessoa.getCdPessoa() };
            DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
          }

        }
        else if (pessoa.getFuncionario().booleanValue())
          associarPessoa(pessoa, Variaveis.getEmpresa(), "funcionario");
      }
      else
      {
        String SQL = "Insert into pessoa (cd_empresa, cd_pessoa, tipo_pessoa, nome_pessoa, data_nascimento, data_cad_pessoa, descricao_pessoa, email_pessoa, site_pessoa, telefone_fixo_pessoa, telefone_cell_pessoa, cpf_cnpj_pessoa, rg_ie_pessoa, orgao_rg_pessoa, data_exp_rg_pessoa, profissao_pessoa, empresa_pessoa, fone_empresa_pessoa, sexo_pessoa) values (?,?,?,?,?,current_date,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING cd_pessoa, data_cad_pessoa";

        Object[] par = { empresa.getCodigo(), NextDaos.nextGenerico(empresa, "pessoa"), (pessoa.getTipoPessoa() + "").trim(), pessoa.getNomePessoa(), pessoa.getDataNascimento(), pessoa.getDescricaoPessoa(), pessoa.getEmailPessoa(), pessoa.getSitePessoa(), Validador.soNumeros(pessoa.getTelefoneFixoPessoa()), Validador.soNumeros(pessoa.getTelefoneCellPessoa()), Validador.soNumeros(pessoa.getCpfCnpjPessoa()), pessoa.getRgIePessoa(), pessoa.getOrgaoRgPessoa(), pessoa.getDataExpRgPessoa(), pessoa.getProfissaoPessoa().trim(), pessoa.getEmpresaPessoa(), Validador.soNumeros(pessoa.getFoneEmpresaPessoa()), (pessoa.getSexoPessoa() + "").trim().toUpperCase() };

        ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        if (rs.next()) {
          pessoa.setCdPessoa(Integer.valueOf(rs.getInt("cd_pessoa")));
          pessoa.setDataCadPessoa(rs.getDate("data_cad_pessoa"));
          if (pessoa.getCliente().booleanValue()) {
            associarPessoa(pessoa, Variaveis.getEmpresa(), "cliente");
          }
          if (pessoa.getFornecedor().booleanValue()) {
            associarPessoa(pessoa, Variaveis.getEmpresa(), "fornecedor");
          }
          if (pessoa.getFuncionario().booleanValue()) {
            associarPessoa(pessoa, Variaveis.getEmpresa(), "funcionario");
          }
        }
      }
      salvaDifListaContatos(pessoa, empresa);
      buscaContatosPessoa(pessoa, empresa);
      salvaDifListaEnderecos(pessoa, empresa);
      buscaEnderecosPessoa(pessoa, empresa);
      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return false;
  }

  public static void salvaEndereco(Pessoa pessoa, Endereco endereco, Empresa empresa) {
    try {
      String SQL = "insert into endereco_pessoa(cd_empresa, cd_pessoa, cep_logradouro, numero_endereco_pessoa, complemento_endereco_pessoa, principal_endereco_pessoa) values(?,?,?,?,?,?)";

      Object[] par = { empresa.getCodigo(), pessoa.getCdPessoa(), endereco.getCepLogradouro(), endereco.getNumero(), endereco.getComplemento(), endereco.getPrincipal() };

      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void salvaContato(Contato contato, Empresa empresa) {
    try {
      if (contato.getCodContato() != null) {
        String SQL = "update contato set nome_contato = ?, cargo_contato = ?, telefone_contato = ?, obs_contato = ? where cd_empresa = ? and cd_pessoa =? and cd_contato = ?";

        Object[] par = { contato.getNomeContato(), contato.getCargoContato(), contato.getTelefoneContato(), contato.getObservacao(), empresa.getCodigo(), contato.getPessoaContato().getCdPessoa(), contato.getCodContato() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else {
        String SQL = "insert into contato(cd_empresa, cd_pessoa, cd_contato, nome_contato, cargo_contato, telefone_contato, obs_contato) values(?,?,?,?,?,?,?)";

        Integer a = NextDaos.nextContato(contato.getPessoaContato(), empresa);
        Object[] par = { empresa.getCodigo(), contato.getPessoaContato().getCdPessoa(), a, contato.getNomeContato(), contato.getCargoContato(), contato.getTelefoneContato(), contato.getObservacao() };

        DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
    }
    catch (Exception e) {
      Variaveis.addNewLog(PessoaDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(false), null);
    }
  }

  public static void salvaEnderecos(Pessoa pessoa, Empresa empresa) {
    for (Endereco e : pessoa.getEnderecos())
      salvaEndereco(pessoa, e, empresa);
  }

  public static void salvaContatos(Pessoa pessoa, Empresa empresa)
  {
    for (Contato c : pessoa.getContatos())
      salvaContato(c, empresa);
  }

  public static void salvaDifListaContatos(Pessoa pessoa, Empresa empresa)
  {
    Pessoa p = new Pessoa(pessoa.getCdPessoa());
    buscaContatosPessoa(p, empresa);
    for (Contato c : p.getContatos()) {
      if (!pessoa.getContatos().contains(c)) {
        excluiContato(c, empresa);
      }
    }
    salvaContatos(pessoa, empresa);
  }

  public static void salvaDifListaEnderecos(Pessoa pessoa, Empresa empresa) {
    Pessoa p = new Pessoa(pessoa.getCdPessoa());
    buscaEnderecosPessoa(p, empresa);
    for (Endereco e : p.getEnderecos()) {
      if (!pessoa.getEnderecos().contains(e))
        excluiEndereco(pessoa, e, empresa);
      else {
        pessoa.getEnderecos().remove(e);
      }
    }
    salvaEnderecos(pessoa, empresa);
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.PessoaDao
 * JD-Core Version:    0.6.2
 */