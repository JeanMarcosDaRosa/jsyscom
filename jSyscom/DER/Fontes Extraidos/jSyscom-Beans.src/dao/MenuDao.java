package dao;

import beans.Empresa;
import beans.Menu;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import utilitarios.DataBase;
import utilitarios.Utils;

public abstract class MenuDao
{
  public static boolean setGrupos(DefaultMutableTreeNode node, Integer codigo_nodo_sup, Usuario user)
    throws SQLException
  {
    Boolean ok = Boolean.valueOf(false);
    String query;
    String query;
    if (codigo_nodo_sup == null) {
      query = "SELECT * FROM menu WHERE id_nodo_sup IS NULL order by nodo_menu";
    }
    else
    {
      String query;
      if (user == null) {
        query = "select distinct(m.*) from menu as m where id_nodo_sup = " + String.valueOf(codigo_nodo_sup) + "order by nodo_menu ";
      }
      else
      {
        query = "select distinct(m.*) from usuario as u, restricoes as r, menu as m where r.cd_usuario = u.cd_usuario and r.cd_empresa = " + String.valueOf(Variaveis.getEmpresa().getCodigo()) + " and " + "u.cd_usuario = " + String.valueOf(user.getCodigoUsuario()) + " and " + "(((r.cd_acao = m.cd_acao) or (m.cd_acao is null)) and id_nodo_sup = " + String.valueOf(codigo_nodo_sup) + " ) " + "order by nodo_menu ";
      }

    }

    try
    {
      ResultSet rs = DataBase.executeQuery(query, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      while (rs.next()) {
        Integer id = Integer.valueOf(rs.getInt("cd_menu"));
        String nome = rs.getString("nodo_menu");
        Integer id_super = Integer.valueOf(rs.getInt("id_nodo_sup"));
        Integer oper = Integer.valueOf(rs.getInt("cd_acao"));
        Menu m = new Menu(id, nome, id_super, oper);
        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(m);
        if (rs.getInt("cd_acao") > 0) {
          node.add(newNode);
          ok = Boolean.valueOf(true);
        } else if ((setGrupos(newNode, id, user)) || (user == null)) {
          node.add(newNode);
          ok = Boolean.valueOf(true);
        }
      }
    } catch (SQLException ex) {
      Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return ok.booleanValue();
  }

  public static void cadOpcoesTela(OpcoesTela opcoes, Integer classe)
  {
    if (classe != null)
      try {
        String SQL = "select * from opcoes_tela where cd_classe = ?";
        Object[] par1 = { classe };
        Object[] par2 = { classe, opcoes.getIconifiable(), opcoes.getMaximizable(), opcoes.getResizable(), opcoes.getClosable(), opcoes.getVisible(), opcoes.getShow_version() };

        ResultSet res = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
        if (!res.next()) {
          SQL = "insert into opcoes_tela (cd_classe, iconifiable,maximizable,resizable,closable,visible,show_version) values(?,?,?,?,?,?,?)";

          DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        } else {
          SQL = "update opcoes_tela set cd_classe = ?, iconifiable = ?,maximizable = ?,resizable = ?,closable = ?,visible = ?,show_version = ? where cd_classe = " + classe;

          DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        }
      } catch (Exception e) {
        Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
  }

  public static Integer cadClasse(String nomeModulo, String nomeClasse, String descricao, OpcoesTela opcoes)
  {
    if ((!"".equals(nomeClasse)) && (!"".equals(nomeModulo))) {
      Integer cdAcao = null;
      try {
        Integer cd_modulos = Integer.valueOf(0);
        String SQL = "select cd_modulos from modulos where nome_modulos = ?";
        Object[] a = { nomeModulo };
        ResultSet x = DataBase.executeQuery(SQL, a, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
        if (x.next()) {
          cd_modulos = Integer.valueOf(x.getInt("cd_modulos"));
        }
        SQL = "select * from classe where nome_classe like (?)";
        Object[] par = { nomeClasse };
        ResultSet res = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
        if (!res.next()) {
          Object[] par1 = { cadAcao(), NextDaos.nextGenericoSemEmpresa("classe"), nomeClasse, cd_modulos, descricao.trim() };
          SQL = "insert into classe (cd_acao, cd_classe, nome_classe, cd_modulos, descricao_classe) values(?,?,?,?,?) RETURNING cd_acao, cd_classe";
          ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
          if (rs.next()) {
            cdAcao = Integer.valueOf(rs.getInt("cd_acao"));
            cadOpcoesTela(opcoes, Integer.valueOf(res.getInt("cd_classe")));
          }
        } else {
          Object[] par1 = { descricao, Integer.valueOf(res.getInt("cd_classe")) };
          SQL = "update classe set descricao_classe = ? where cd_classe = ?";
          DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
          cdAcao = Integer.valueOf(res.getInt("cd_acao"));
          cadOpcoesTela(opcoes, Integer.valueOf(res.getInt("cd_classe")));
        }
        return cdAcao;
      } catch (Exception e) {
        Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
    }
    return null;
  }

  public static Integer cadRelatorio(String nome) {
    if (!"".equals(nome)) {
      Integer cdAcao = null;
      try {
        String SQL = "select * from relatorio where nome_relatorio like (?)";
        Object[] par = { nome };
        ResultSet res = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
        if (!res.next()) {
          Object[] par1 = { cadAcao(), NextDaos.nextGenericoSemEmpresa("relatorio"), nome };
          SQL = "insert into relatorio (cd_acao, cd_relatorio, nome_relatorio) values(?,?,?) RETURNING cd_acao";
          ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
          if (rs.next()) {
            cdAcao = Integer.valueOf(rs.getInt("cd_acao"));
          }
        }
        return Integer.valueOf(res.getInt("cd_acao"));
      }
      catch (Exception e)
      {
        Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
    }
    return null;
  }

  public static ArrayList<String> getModulos() {
    ArrayList mods = new ArrayList();
    try {
      String SQL = "select * from modulos where ativo_modulos = true";
      ResultSet rs = DataBase.executeQuery(SQL, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      while (rs.next())
        mods.add(rs.getString("nome_modulos"));
    }
    catch (Exception e) {
      Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return mods;
  }

  public static Integer cadAcao() {
    try {
      String SQL = "select max(cd_acao) as prox from acao";
      ResultSet res = DataBase.executeQuery(SQL, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (res.next()) {
        SQL = "insert into acao (cd_acao) values(?)";
        ResultSet rs = DataBase.executeQuery(SQL, new Object[] { Integer.valueOf(res.getInt("prox") + 1) }, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
        if (rs.next())
          return Integer.valueOf(res.getInt("prox") + 1);
      }
    }
    catch (Exception e) {
      Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }

  public static ArrayList<String> getListClasses() {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from classe where nome_classe is not null";
      ResultSet res = DataBase.executeQuery(SQL, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      while (res.next())
        lista.add(res.getString("nome_classe"));
    }
    catch (Exception e) {
      Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }

    return lista;
  }

  public static Boolean alreadyClass(String classe) {
    String sql = "select * from classe where nome_classe like(?)";
    try {
      Object[] par = { classe };
      ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (res.next()) {
        return Boolean.valueOf(true);
      }
      return Boolean.valueOf(false);
    }
    catch (Exception e) {
      Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.MenuDao
 * JD-Core Version:    0.6.2
 */