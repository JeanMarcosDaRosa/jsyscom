package dao;

import beans.Empresa;
import beans.Equipamento;
import beans.OrdemServico;
import beans.Pessoa;
import beans.Variaveis;
import java.sql.ResultSet;
import utilitarios.DataBase;
import utilitarios.Utils;

public class NextDaos
{
  public static Integer nextGenerico(Empresa empresa, String tabela)
  {
    try
    {
      String SQL = "select max(cd_" + tabela + ") as prox from " + tabela + " where cd_empresa = ?";
      ResultSet rs = DataBase.executeQuery(SQL, new Object[] { empresa.getCodigo() }, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next())
        return Integer.valueOf(rs.getInt("prox") + 1);
    }
    catch (Exception e) {
      Variaveis.addNewLog(NextDaos.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Integer.valueOf(1);
  }

  public static Integer nextGenericoSemEmpresa(String tabela) {
    try {
      String SQL = "select max(cd_" + tabela + ") as prox from " + tabela;
      ResultSet rs = DataBase.executeQuery(SQL, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next())
        return Integer.valueOf(rs.getInt("prox") + 1);
    }
    catch (Exception e) {
      Variaveis.addNewLog(NextDaos.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Integer.valueOf(1);
  }

  public static Integer nextContato(Pessoa pessoa, Empresa empresa) {
    try {
      String SQL = "select max(cd_contato) as prox from contato where cd_empresa = ?";
      ResultSet rs = DataBase.executeQuery(SQL, new Object[] { empresa.getCodigo() }, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next())
        return Integer.valueOf(rs.getInt("prox") + 1);
    }
    catch (Exception e) {
      Variaveis.addNewLog(NextDaos.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Integer.valueOf(1);
  }

  public static Integer nextOrcamento(OrdemServico ordem, Empresa empresa) {
    try {
      String SQL = "select max(nro_revisao_orcamento) as prox from orcamento where cd_empresa = ? and cd_os_cab = ?";
      ResultSet rs = DataBase.executeQuery(SQL, new Object[] { empresa.getCodigo(), ordem.getCodOS() }, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next())
        return Integer.valueOf(rs.getInt("prox") + 1);
    }
    catch (Exception e) {
      Variaveis.addNewLog(NextDaos.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Integer.valueOf(1);
  }

  public static Integer nextTesteEquip(OrdemServico ordem, Empresa empresa)
  {
    try {
      String SQL = "select max(cd_os_tst_equip) as prox from os_tst_equip where cd_empresa = ? and cd_os_cab = ? and cd_equip = ?";
      ResultSet rs = DataBase.executeQuery(SQL, new Object[] { empresa.getCodigo(), ordem.getCodOS(), ordem.getEquipamento().getCodEquipamento() }, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (rs.next())
        return Integer.valueOf(rs.getInt("prox") + 1);
    }
    catch (Exception e) {
      Variaveis.addNewLog(NextDaos.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Integer.valueOf(1);
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.NextDaos
 * JD-Core Version:    0.6.2
 */