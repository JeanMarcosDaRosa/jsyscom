package dao;

import beans.Cliente;
import beans.Empresa;
import beans.Equipamento;
import beans.Imagem;
import beans.OrdemServico;
import beans.PropriedadeTipoEquip;
import beans.TipoEquipamento;
import beans.Variaveis;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utilitarios.DataBase;
import utilitarios.Utils;

public class EquipamentoDao
{
  public static Equipamento buscaEquipamento(Cliente cliente, Integer codEquipamento, Empresa empresa, Boolean getImage)
  {
    Equipamento o = new Equipamento(codEquipamento);
    o.setCliente(cliente);
    buscaEquipamento(o, empresa, getImage);
    return o;
  }

  public static void buscaEquipamento(Equipamento equipamento, Empresa empresa, Boolean getImage) {
    try {
      String SQL = "select * from equip where cd_empresa = ? and cd_equip = ?";
      Object[] par = { empresa.getCodigo(), equipamento.getCodEquipamento() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        populaEQResultSet(rs, equipamento, empresa, getImage);
    }
    catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Equipamento buscaEquipamentoSerie(Cliente cliente, String nroSerie, Empresa empresa, Boolean getImage) {
    Equipamento o = new Equipamento(nroSerie);
    o.setCliente(cliente);
    buscaEquipamento(o, empresa, getImage);
    return o;
  }

  public static void buscaEquipamentoSerie(Equipamento equipamento, Empresa empresa, Boolean getImage) {
    try {
      String SQL = "select * from equip where cd_empresa = ? and UPPER(nro_serie_equip) like ?";
      Object[] par = { empresa.getCodigo(), equipamento.getNroSerie() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        populaEQResultSet(rs, equipamento, empresa, getImage);
    }
    catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Boolean alreadyExistsEquip(Equipamento equipamento, Empresa empresa) {
    try {
      String SQL = "select * from equip where cd_empresa = ? and UPPER(nro_serie_equip) like ?";
      Object[] par = { empresa.getCodigo(), equipamento.getNroSerie() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return Boolean.valueOf(true);
    }
    catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean emGarantia(Equipamento equipamento, Empresa empresa) {
    try {
      String SQL = "select os.* from os_cab as os, equip where os.cd_empresa = equip.cd_empresa and os.cd_equip = equip.cd_equip and os.cd_empresa = ? and equip.nro_serie_equip like(?) and meses_garantia_os_cab > 0 and  data_final_garantia_os_cab >= current_date";

      Object[] par = { empresa.getCodigo(), equipamento.getNroSerie() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return Boolean.valueOf(true);
    }
    catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static void populaEQResultSet(ResultSet rs, Equipamento equip, Empresa empresa, Boolean getImage) {
    try {
      equip.setCodEquipamento(Integer.valueOf(rs.getInt("cd_equip")));
      equip.setTipoEquipamento(getTipoEquipamento(Integer.valueOf(rs.getInt("cd_tipo_equip")), empresa));
      equip.setDataFabricacao(rs.getDate("data_fabricacao_equip"));
      equip.setDescricao(rs.getString("descricao_equip"));
      equip.setMarca(rs.getString("marca_equip"));
      equip.setModelo(rs.getString("modelo_equip"));
      equip.setNroRastreio(Integer.valueOf(rs.getInt("nro_rastreio_equip")));
      equip.setNroSerie(rs.getString("nro_serie_equip"));
      Imagem imagem = OrdemServicoDao.getImagemConexao(Variaveis.getEmpresa(), new Imagem(Integer.valueOf(rs.getInt("cd_conexao_equip"))));
      equip.setConexao(imagem);
    } catch (SQLException|IOException ex) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void excluiTipoEquipamento(Empresa empresa, TipoEquipamento tipo) {
    try {
      String SQL = "delete from tipo_equip where cd_empresa = ? and cd_tipo_equip = ?";
      Object[] par = { empresa.getCodigo(), tipo.getCodTipo() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Boolean salvaTipoEquipamento(TipoEquipamento tipo, Empresa empresa) {
    try {
      String SQL = "select * from tipo_equip where cd_empresa = ? and cd_tipo_equip = ?";
      Object[] par1 = { empresa.getCodigo(), tipo.getCodTipo() };
      ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next()) {
        SQL = "update tipo_equip set descricao_tipo_equip = ? where cd_empresa = ? and cd_tipo_equip = ?";
        Object[] par2 = { tipo.getDescricaoEquipamento(), empresa.getCodigo(), Integer.valueOf(rs.getInt("cd_tipo_equip")) };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else {
        SQL = "insert into tipo_equip(cd_empresa, descricao_tipo_equip) values(?,?)";

        Object[] par2 = { empresa.getCodigo(), tipo.getDescricaoEquipamento() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }

      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static void excluiPropTipoEquipamento(Empresa empresa, PropriedadeTipoEquip prop) {
    try {
      String SQL = "delete from prop_tipo_equip where cd_empresa = ? and cd_tipo_equip = ? and cd_prop_tipo_equip = ?";
      Object[] par = { empresa.getCodigo(), prop.getTipoEquip().getCodTipo(), prop.getCodPropTipoEquip() };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
    }
    catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Boolean salvaPropTipoEquipamento(PropriedadeTipoEquip prop, Empresa empresa) {
    try {
      if (prop.getCodPropTipoEquip() != null) {
        String SQL = "update prop_tipo_equip set descr_prop_tipo_equip = ? where cd_empresa = ? and cd_tipo_equip = ? and cd_prop_tipo_equip = ?";
        Object[] par2 = { prop.getDescricao(), empresa.getCodigo(), prop.getTipoEquip().getCodTipo(), prop.getCodPropTipoEquip() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }
      else {
        String SQL = "insert into prop_tipo_equip(cd_empresa, cd_tipo_equip, descr_prop_tipo_equip) values(?,?,?)";

        Object[] par2 = { empresa.getCodigo(), prop.getTipoEquip().getCodTipo(), prop.getDescricao() };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }

      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static Boolean isBusyTipoEquipamento(TipoEquipamento tipo, Empresa empresa) {
    try {
      String SQL = "select * from equip where cd_empresa = ? and cd_tipo_equip = ?";
      Object[] par = { empresa.getCodigo(), tipo.getCodTipo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        return Boolean.valueOf(true);
    }
    catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }

  public static ArrayList<PropriedadeTipoEquip> getAllPropTipoEquipamento(TipoEquipamento tipo, Empresa empresa) {
    ArrayList lista = new ArrayList();
    if (tipo != null) {
      try {
        String SQL = "select * from prop_tipo_equip where cd_empresa = ? and cd_tipo_equip = ?";
        Object[] par = { empresa.getCodigo(), tipo.getCodTipo() };
        ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

        while (rs.next()) {
          PropriedadeTipoEquip lp = new PropriedadeTipoEquip(Integer.valueOf(rs.getInt("cd_prop_tipo_equip")), rs.getString("descr_prop_tipo_equip"), tipo);
          lp.setTipoEquip(tipo);
          lista.add(lp);
        }
      } catch (Exception e) {
        Variaveis.addNewLog(TipoEquipamento.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
      }
    }
    return lista;
  }

  public static ArrayList<TipoEquipamento> getAllTipoEquipamento(Empresa empresa)
  {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from tipo_equip where cd_empresa = ?";
      Object[] par = { empresa.getCodigo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        TipoEquipamento lp = new TipoEquipamento();
        lp.setCodTipo(Integer.valueOf(rs.getInt("cd_tipo_equip")));
        lp.setDescricaoEquipamento(rs.getString("descricao_tipo_equip"));
        lp.getPropriedades().addAll(getAllPropTipoEquipamento(lp, empresa));
        lista.add(lp);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(TipoEquipamento.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return lista;
  }

  public static TipoEquipamento getTipoEquipamento(Integer codTipoEquip, Empresa empresa) {
    TipoEquipamento t = new TipoEquipamento(codTipoEquip);
    getTipoEquipamento(t, empresa);
    return t;
  }

  public static void getTipoEquipamento(TipoEquipamento tipoEquip, Empresa empresa) {
    try {
      String SQL = "select * from tipo_equip where cd_empresa = ? and cd_tipo_equip = ?";
      Object[] par = { empresa.getCodigo(), tipoEquip.getCodTipo() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        tipoEquip.setDescricaoEquipamento(rs.getString("descricao_tipo_equip"));
    }
    catch (Exception e) {
      Variaveis.addNewLog(TipoEquipamento.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static ArrayList<OrdemServico> getHistEntrada(Equipamento equip, Empresa empresa) {
    ArrayList lista = new ArrayList();
    try {
      String SQL = "select * from os_cab where cd_empresa = ? and cd_equip = ? ";
      Object[] par = { empresa.getCodigo(), equip.getCodEquipamento() };
      ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      while (rs.next()) {
        OrdemServico o = new OrdemServico();
        o.setCodOS(Integer.valueOf(rs.getInt("cd_os_cab")));
        o.setDataEntrada(rs.getDate("data_ent_os_cab"));
        o.setDataSaida(rs.getDate("data_saida_os_cab"));
        o.setMesesGarantia(Integer.valueOf(rs.getInt("meses_garantia_os_cab")));
        o.setSobGarantia(Boolean.valueOf(rs.getBoolean("sob_garantia")));
        lista.add(o);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(TipoEquipamento.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return lista;
  }

  public static Boolean salvaEquipamento(Equipamento equip, Empresa empresa) {
    try {
      if (equip != null) {
        if (equip.getCodEquipamento() != null) {
          String SQL = "update equip set                  cd_cliente = ?,                 cd_tipo_equip = ?,                 cd_conexao_equip = ?,                 marca_equip = ?,                 modelo_equip = ?,                 descricao_equip = ?,                 nro_rastreio_equip = ?,                 nro_serie_equip = ?,                 data_fabricacao_equip = ? where cd_empresa = ? and cd_equip = ?";

          Object[] par2 = { equip.getCliente().getCdPessoa(), equip.getTipoEquipamento().getCodTipo(), equip.getConexao().getCodImagem(), equip.getMarca(), equip.getModelo(), equip.getDescricao(), equip.getNroRastreio(), equip.getNroSerie(), equip.getDataFabricacao(), empresa.getCodigo(), equip.getCodEquipamento() };

          DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
        }
        else {
          String SQL = "insert into equip(cd_empresa, cd_cliente, cd_equip,                 cd_tipo_equip,                 cd_conexao_equip,                 marca_equip,                 modelo_equip,                 descricao_equip,                 nro_rastreio_equip,                 nro_serie_equip,                 data_fabricacao_equip) values(?,?,?,?,?,?,?,?,?,?,?) RETURNING cd_equip";

          Object[] par2 = { empresa.getCodigo(), equip.getCliente().getCdPessoa(), NextDaos.nextGenerico(empresa, "equip"), equip.getTipoEquipamento().getCodTipo(), equip.getConexao().getCodImagem(), equip.getMarca(), equip.getModelo(), equip.getDescricao(), equip.getNroRastreio(), equip.getNroSerie(), equip.getDataFabricacao() };

          ResultSet rs = DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

          if (rs.next()) {
            equip.setCodEquipamento(Integer.valueOf(rs.getInt("cd_equip")));
          }
        }
        return Boolean.valueOf(true);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(EquipamentoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.EquipamentoDao
 * JD-Core Version:    0.6.2
 */