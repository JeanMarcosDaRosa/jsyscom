package dao;

import base.FrmPrincipal;
import beans.Conexao;
import beans.Empresa;
import beans.Imagem;
import beans.Variaveis;
import funcoes.OperacoesDB;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.postgresql.PGConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;
import utilitarios.DataBase;
import utilitarios.Utils;

public class ImagemDao
{
  public static Imagem getImagemBanco(String sql)
    throws IOException, SQLException
  {
    Connection conexao = Conexao.getInstance().getCon(Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), Variaveis.getDbType());
    Boolean autoCommit = Boolean.valueOf(false);
    try {
      autoCommit = Boolean.valueOf(conexao.getAutoCommit());
      conexao.setAutoCommit(false);
    } catch (SQLException ex) {
      Logger.getLogger(FrmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
    }
    try {
      BufferedImage i = null;
      Imagem img = null;

      LargeObjectManager lobj = ((PGConnection)conexao).getLargeObjectAPI();
      PreparedStatement ps = conexao.prepareStatement(sql);
      ResultSet rs = ps.executeQuery();
      int oid;
      while (rs.next())
      {
        oid = rs.getInt("blob");
        if (oid == 0) {
          break;
        }
        LargeObject obj = lobj.open(oid, 262144);

        byte[] buf = new byte[obj.size()];
        obj.read(buf, 0, obj.size());

        ByteArrayInputStream ms = new ByteArrayInputStream(buf);
        i = ImageIO.read(ms);
        ImageIcon image = new ImageIcon(i);
        img = new Imagem(image);
        img.setCodImagem(Integer.valueOf(rs.getInt("cod")));
        img.setMd5Imagem(rs.getString("md5"));
        img.setNomeImagem(rs.getString("nome"));

        obj.close();
      }
      rs.close();
      ps.close();

      conexao.commit();
      return img;
    } catch (SQLException ex) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    } finally {
      try {
        conexao.setAutoCommit(autoCommit.booleanValue());
      } catch (SQLException ex) {
        Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(false), null);
      }
    }
    return null;
  }

  public static ArrayList<Imagem> getListaImagemBanco(String sql, Integer posicao, Integer qtde) throws IOException, SQLException {
    Connection conexao = Conexao.getInstance().getCon(Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), Variaveis.getDbType());
    Boolean autoCommit = Boolean.valueOf(false);
    try {
      autoCommit = Boolean.valueOf(conexao.getAutoCommit());
      conexao.setAutoCommit(false);
    } catch (SQLException ex) {
      Logger.getLogger(FrmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
    }
    try {
      ArrayList lista = new ArrayList();

      LargeObjectManager lobj = ((PGConnection)conexao).getLargeObjectAPI();
      PreparedStatement ps = conexao.prepareStatement(sql);
      ResultSet rs = ps.executeQuery();
      int count = 0;
      int seq = 0;
      BufferedImage i;
      while ((rs.next()) && (count < qtde.intValue())) {
        if (seq >= posicao.intValue()) {
          i = null;

          int oid = rs.getInt("blob");
          if (oid == 0) {
            break;
          }
          LargeObject obj = lobj.open(oid, 262144);

          byte[] buf = new byte[obj.size()];
          obj.read(buf, 0, obj.size());

          ByteArrayInputStream ms = new ByteArrayInputStream(buf);
          i = ImageIO.read(ms);
          ImageIcon image = new ImageIcon(i);
          Imagem img = new Imagem(image);
          img.setCodImagem(Integer.valueOf(rs.getInt("cod")));
          img.setMd5Imagem(rs.getString("md5"));
          img.setNomeImagem(rs.getString("nome"));
          obj.close();
          count++;
          lista.add(img);
        }
        seq++;
      }
      rs.close();
      ps.close();

      conexao.commit();
      return lista;
    } catch (SQLException ex) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    } finally {
      try {
        conexao.setAutoCommit(autoCommit.booleanValue());
      } catch (SQLException ex) {
        Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(false), null);
      }
    }
    return null;
  }

  public static Integer setImagemBanco(String sql, File file) throws IOException, SQLException
  {
    Connection conexao = Conexao.getInstance().getCon(Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), Variaveis.getDbType());
    Boolean autoCommit = Boolean.valueOf(false);
    try {
      autoCommit = Boolean.valueOf(conexao.getAutoCommit());
      conexao.setAutoCommit(false);
    } catch (SQLException ex) {
      Logger.getLogger(FrmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
    }
    try {
      LargeObjectManager lobj = ((PGConnection)conexao).getLargeObjectAPI();
      long oid = lobj.createLO(393216);
      LargeObject obj = lobj.open(oid, 131072);
      FileInputStream fis = new FileInputStream(file);
      byte[] buf = new byte[2048];
      int tl = 0;
      int s;
      while ((s = fis.read(buf, 0, 2048)) > 0) {
        obj.write(buf, 0, s);
        tl += s;
      }
      obj.close();
      PreparedStatement ps = conexao.prepareStatement(sql);
      ps.setLong(1, oid);
      ResultSet rs = ps.executeQuery();
      Integer ret = null;
      if (rs.next()) {
        ret = Integer.valueOf(rs.getInt(1));
      }
      ps.close();
      fis.close();
      ps.close();
      conexao.commit();
      return ret;
    } catch (SQLException ex) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    } finally {
      try {
        conexao.setAutoCommit(autoCommit.booleanValue());
      } catch (SQLException ex) {
        Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(false), null);
      }
    }
    return null;
  }

  public static Imagem getWallpaper(Integer cd_imagem) throws IOException, SQLException {
    return getImagemBanco("SELECT cd_wallpaper as cod, blob_wallpaper as blob, md5_wallpaper as md5, nome_wallpaper as nome FROM wallpaper WHERE cd_empresa = " + Variaveis.getEmpresa().getCodigo() + " and cd_wallpaper = " + cd_imagem);
  }

  public static Integer wallpaperInDB(Empresa empresa, Imagem imagem) {
    try {
      String sql = "select * from wallpaper where cd_empresa = ? and md5_wallpaper like(?)";
      Object[] par = { empresa.getCodigo(), imagem.getMd5Imagem() };
      ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());
      if (res.next())
        return Integer.valueOf(res.getInt("cd_wallpaper"));
    }
    catch (Exception e) {
      Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return null;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     dao.ImagemDao
 * JD-Core Version:    0.6.2
 */