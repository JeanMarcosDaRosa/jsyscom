package beans;

import java.util.Date;

public class Usuario extends Funcionario
  implements Cloneable
{
  private Integer codigoUsuario;
  private String nomeUsuario;
  private String senhaUsuario;
  private String descricaoUsuario;
  private Date dataCadastroUsuario;
  private Empresa empresa;

  public Empresa getEmpresa()
  {
    return this.empresa;
  }

  public void setEmpresa(Empresa empresa) {
    this.empresa = empresa;
  }

  public Date getDataCadastro() {
    return this.dataCadastroUsuario;
  }

  public void setDataCadastro(Date dataCadastro) {
    this.dataCadastroUsuario = dataCadastro;
  }

  public String getDescricao() {
    return this.descricaoUsuario;
  }

  public void setDescricao(String descricao) {
    this.descricaoUsuario = (descricao != null ? descricao.toUpperCase().trim() : null);
  }

  public String getNomeUsuario() {
    return this.nomeUsuario;
  }

  public void setNomeUsuario(String nomeUsuario) {
    this.nomeUsuario = (nomeUsuario != null ? nomeUsuario.toUpperCase().trim() : null);
  }

  public String getSenhaUsuario() {
    return this.senhaUsuario;
  }

  public void setSenhaUsuario(String senhaUsuario) {
    this.senhaUsuario = senhaUsuario;
  }

  public Usuario(Integer codigoUsuario, String nomeUsuario, String senhaUsuario, String descricao, Date dataCadastro, Empresa empresa) {
    this.codigoUsuario = codigoUsuario;
    this.nomeUsuario = (nomeUsuario != null ? nomeUsuario.toUpperCase().trim() : null);
    this.senhaUsuario = senhaUsuario;
    this.descricaoUsuario = (descricao != null ? descricao.toUpperCase().trim() : null);
    this.dataCadastroUsuario = dataCadastro;
    this.empresa = empresa;
  }

  public Usuario(Empresa empresa) {
    this.codigoUsuario = null;
    this.nomeUsuario = "";
    this.senhaUsuario = "";
    this.descricaoUsuario = "";
    this.dataCadastroUsuario = null;
    this.empresa = empresa;
  }

  public Usuario(Integer codigoUsuario, Empresa empresa) {
    this.codigoUsuario = codigoUsuario;
    this.empresa = empresa;
    this.nomeUsuario = "";
    this.senhaUsuario = "";
    this.descricaoUsuario = "";
    this.dataCadastroUsuario = null;
  }

  public void clearUsuario() {
    clearFuncionario();
    this.codigoUsuario = Integer.valueOf(0);
    this.nomeUsuario = "";
    this.senhaUsuario = "";
    this.descricaoUsuario = "";
    this.dataCadastroUsuario = null;
  }

  public Integer getCodigoUsuario()
  {
    return this.codigoUsuario;
  }

  public void setCodigoUsuario(Integer codigoUsuario)
  {
    this.codigoUsuario = codigoUsuario;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Usuario other = (Usuario)obj;
    if ((this.codigoUsuario != other.codigoUsuario) && ((this.codigoUsuario == null) || (!this.codigoUsuario.equals(other.codigoUsuario)))) {
      return false;
    }
    if (this.nomeUsuario == null ? other.nomeUsuario != null : !this.nomeUsuario.equals(other.nomeUsuario)) {
      return false;
    }
    if ((this.dataCadastroUsuario != other.dataCadastroUsuario) && ((this.dataCadastroUsuario == null) || (!this.dataCadastroUsuario.equals(other.dataCadastroUsuario)))) {
      return false;
    }
    return true;
  }

  public int hashCode()
  {
    int hash = 7;
    return hash;
  }

  public String toString()
  {
    return this.nomeUsuario;
  }

  public Object clone() throws CloneNotSupportedException
  {
    return super.clone();
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Usuario
 * JD-Core Version:    0.6.2
 */