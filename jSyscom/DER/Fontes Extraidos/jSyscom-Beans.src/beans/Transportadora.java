package beans;

import java.util.Objects;
import java.util.Observable;

public class Transportadora extends Observable
{
  private Integer codigo;
  private String nome;
  private String descricao;
  private String telefone;
  private Boolean flagPronto;

  public Transportadora(Integer codigo)
  {
    clearTransportadora();
    this.codigo = codigo;
  }

  public Transportadora(Integer codigo, String nome, String telefone, String descricao) {
    this.codigo = codigo;
    this.nome = (nome != null ? nome.toUpperCase().trim() : null);
    this.telefone = telefone;
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
  }

  public Transportadora(Integer codigo, String nome) {
    clearTransportadora();
    this.codigo = codigo;
    this.nome = (nome != null ? nome.toUpperCase().trim() : null);
  }

  public Transportadora(String nome, String descricao, String telefone) {
    clearTransportadora();
    this.nome = nome;
    this.descricao = descricao;
    this.telefone = telefone;
  }

  public Transportadora()
  {
  }

  public Integer getCodigo()
  {
    return this.codigo;
  }

  public void setCodigo(Integer codigo)
  {
    this.codigo = codigo;
  }

  public String getNome()
  {
    return this.nome;
  }

  public void setNome(String nome)
  {
    this.nome = (nome != null ? nome.toUpperCase().trim() : null);
  }

  public String getDescricao()
  {
    return this.descricao;
  }

  public void setDescricao(String descricao)
  {
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
  }

  public String getTelefone()
  {
    return this.telefone;
  }

  public void setTelefone(String telefone)
  {
    this.telefone = telefone;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public final void clearTransportadora() {
    this.codigo = null;
    this.nome = "";
    this.telefone = "";
    this.descricao = "";
  }

  public String toString()
  {
    return this.nome;
  }

  public int hashCode()
  {
    int hash = 7;
    return hash;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Transportadora other = (Transportadora)obj;
    if (!Objects.equals(this.nome, other.nome)) {
      return false;
    }
    if (!Objects.equals(this.descricao, other.descricao)) {
      return false;
    }
    if (!Objects.equals(this.telefone, other.telefone)) {
      return false;
    }
    return true;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Transportadora
 * JD-Core Version:    0.6.2
 */