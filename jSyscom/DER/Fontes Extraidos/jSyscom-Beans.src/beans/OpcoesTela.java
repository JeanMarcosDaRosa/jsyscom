package beans;

import java.io.Serializable;

public class OpcoesTela
  implements Serializable
{
  private Boolean iconifiable;
  private Boolean maximizable;
  private Boolean resizable;
  private Boolean closable;
  private Boolean visible;
  private Boolean show_version;

  public OpcoesTela()
  {
    this.iconifiable = Boolean.valueOf(true);
    this.maximizable = Boolean.valueOf(true);
    this.resizable = Boolean.valueOf(true);
    this.closable = Boolean.valueOf(true);
    this.visible = Boolean.valueOf(true);
    this.show_version = Boolean.valueOf(true);
  }

  public OpcoesTela(Boolean iconifiable, Boolean maximizable, Boolean resizable, Boolean closable, Boolean visible, Boolean show_version) {
    this.iconifiable = iconifiable;
    this.maximizable = maximizable;
    this.resizable = resizable;
    this.closable = closable;
    this.visible = visible;
    this.show_version = show_version;
  }

  public Boolean getIconifiable()
  {
    return this.iconifiable;
  }

  public void setIconifiable(Boolean iconifiable)
  {
    this.iconifiable = iconifiable;
  }

  public Boolean getMaximizable()
  {
    return this.maximizable;
  }

  public void setMaximizable(Boolean maximizable)
  {
    this.maximizable = maximizable;
  }

  public Boolean getResizable()
  {
    return this.resizable;
  }

  public void setResizable(Boolean resizable)
  {
    this.resizable = resizable;
  }

  public Boolean getClosable()
  {
    return this.closable;
  }

  public void setClosable(Boolean closable)
  {
    this.closable = closable;
  }

  public Boolean getVisible()
  {
    return this.visible;
  }

  public void setVisible(Boolean visible)
  {
    this.visible = visible;
  }

  public String getAllOpcoesTela() {
    return "visible = " + this.visible.toString() + "\n" + "closable = " + this.closable.toString() + "\n" + "iconifiable = " + this.iconifiable.toString() + "\n" + "maximisable = " + this.maximizable.toString() + "\n" + "resisable = " + this.resizable.toString() + "\n" + "show_version = " + this.show_version.toString();
  }

  public Boolean getShow_version()
  {
    return this.show_version;
  }

  public void setShow_version(Boolean show_version)
  {
    this.show_version = show_version;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.OpcoesTela
 * JD-Core Version:    0.6.2
 */