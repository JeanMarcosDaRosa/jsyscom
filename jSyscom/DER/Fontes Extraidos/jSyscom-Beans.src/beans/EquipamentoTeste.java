package beans;

public class EquipamentoTeste
{
  private Integer codTeste;
  private Equipamento equipamento;
  private String descricao;
  private String defeito;

  public EquipamentoTeste()
  {
  }

  public EquipamentoTeste(Integer codTeste, Equipamento equipamento)
  {
    this.codTeste = codTeste;
    this.equipamento = equipamento;
  }

  public EquipamentoTeste(Integer codTeste, Equipamento equipamento, String descricao, String defeito) {
    this.codTeste = codTeste;
    this.equipamento = equipamento;
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
    this.defeito = (defeito != null ? defeito.toUpperCase().trim() : null);
  }

  public Integer getCodTeste()
  {
    return this.codTeste;
  }

  public void setCodTeste(Integer codTeste)
  {
    this.codTeste = codTeste;
  }

  public Equipamento getEquipamento()
  {
    return this.equipamento;
  }

  public void setEquipamento(Equipamento equipamento)
  {
    this.equipamento = equipamento;
  }

  public String getDescricao()
  {
    return this.descricao;
  }

  public void setDescricao(String descricao)
  {
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
  }

  public String getDefeito()
  {
    return this.defeito;
  }

  public void setDefeito(String defeito)
  {
    this.defeito = (defeito != null ? defeito.toUpperCase().trim() : null);
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.EquipamentoTeste
 * JD-Core Version:    0.6.2
 */