package beans;

public class Funcionario extends Pessoa
{
  public Funcionario()
  {
  }

  public Funcionario(Integer cd_funcionario)
  {
    setCdPessoa(cd_funcionario);
  }

  public void clearFuncionario() {
    clearPessoa();
  }

  public String toString()
  {
    return getNomePessoa();
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Funcionario
 * JD-Core Version:    0.6.2
 */