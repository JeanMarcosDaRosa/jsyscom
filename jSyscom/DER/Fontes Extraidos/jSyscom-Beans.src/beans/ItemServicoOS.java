package beans;

import java.util.Objects;

public class ItemServicoOS
{
  private Servico servico;
  private String observacoes;
  private Double quantidade;
  private Boolean mostrarQtde;
  private Double valorUnitario;
  private Double valorBruto;
  private OrdemServico ordemServico;

  public ItemServicoOS()
  {
  }

  public ItemServicoOS(OrdemServico ordemServico, Servico servico, String observacoes, Double quantidade, Boolean mostrarQtde, Double valorUnitario, Double valorBruto)
  {
    this.ordemServico = ordemServico;
    this.servico = servico;
    this.observacoes = observacoes;
    this.quantidade = quantidade;
    this.mostrarQtde = mostrarQtde;
    this.valorUnitario = valorUnitario;
    this.valorBruto = valorBruto;
  }

  public Servico getServico()
  {
    return this.servico;
  }

  public void setServico(Servico servico)
  {
    this.servico = servico;
  }

  public String getObservacoes()
  {
    return this.observacoes;
  }

  public void setObservacoes(String observacoes)
  {
    this.observacoes = observacoes;
  }

  public Double getValorBruto()
  {
    return this.valorBruto;
  }

  public void setValorBruto(Double valorBruto)
  {
    this.valorBruto = valorBruto;
  }

  public Double getQuantidade()
  {
    return this.quantidade;
  }

  public void setQuantidade(Double quantidade)
  {
    this.quantidade = quantidade;
  }

  public Boolean getMostrarQtde()
  {
    return this.mostrarQtde;
  }

  public void setMostrarQtde(Boolean mostrarQtde)
  {
    this.mostrarQtde = mostrarQtde;
  }

  public Double getValorUnitario()
  {
    return this.valorUnitario;
  }

  public void setValorUnitario(Double valorUnitario)
  {
    this.valorUnitario = valorUnitario;
  }

  public int hashCode()
  {
    int hash = 5;
    return hash;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ItemServicoOS other = (ItemServicoOS)obj;
    if (!Objects.equals(this.servico, other.servico)) {
      return false;
    }
    if (!Objects.equals(this.valorUnitario, other.valorUnitario)) {
      return false;
    }
    return true;
  }

  public OrdemServico getOrdemServico()
  {
    return this.ordemServico;
  }

  public void setOrdemServico(OrdemServico ordemServico)
  {
    this.ordemServico = ordemServico;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.ItemServicoOS
 * JD-Core Version:    0.6.2
 */