package beans;

import java.util.Observable;

public class LocalProd extends Observable
{
  private Integer codLocalProd;
  private String descricao;
  private Boolean flagPronto;

  public LocalProd(Integer codLocalProd, String descricao)
  {
    this.codLocalProd = codLocalProd;
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
  }

  public LocalProd(String descricao) {
    this.codLocalProd = null;
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
  }

  public LocalProd(Integer codLocalProd) {
    this.codLocalProd = codLocalProd;
    this.descricao = "";
  }

  public LocalProd() {
    this.codLocalProd = null;
    this.descricao = "";
  }

  public Integer getCodLocalProd()
  {
    return this.codLocalProd;
  }

  public void setCodLocalProd(Integer codLocalProd)
  {
    this.codLocalProd = codLocalProd;
  }

  public String getDescricao()
  {
    return this.descricao;
  }

  public void setDescricao(String descricao)
  {
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    LocalProd other = (LocalProd)obj;
    if (this.descricao == null ? other.descricao != null : !this.descricao.equals(other.descricao)) {
      return false;
    }
    return true;
  }

  public String toString()
  {
    return this.descricao;
  }

  public int hashCode()
  {
    int hash = 5;
    return hash;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public final void clearLocalProd() {
    this.codLocalProd = null;
    this.descricao = "";
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.LocalProd
 * JD-Core Version:    0.6.2
 */