package beans;

public class Menu
{
  private Integer id;
  private String Nome;
  private Integer id_super;
  private Integer Operacao;

  public Menu()
  {
    this.id = null;
    this.Nome = "";
    this.id_super = null;
    this.Operacao = null;
  }

  public Menu(Integer id, String Nome, Integer id_super, Integer Operacao) {
    this.id = id;
    this.Nome = Nome;
    this.id_super = id_super;
    this.Operacao = Operacao;
  }

  public Integer getId()
  {
    return this.id;
  }

  public void setId(Integer id)
  {
    this.id = id;
  }

  public String getNome()
  {
    return this.Nome;
  }

  public void setNome(String Nome)
  {
    this.Nome = Nome;
  }

  public Integer getId_super()
  {
    return this.id_super;
  }

  public void setId_super(Integer id_super)
  {
    this.id_super = id_super;
  }

  public String toString()
  {
    return this.Nome;
  }

  public Integer getOperacao()
  {
    return this.Operacao;
  }

  public void setOperacao(Integer Operacao)
  {
    this.Operacao = Operacao;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Menu
 * JD-Core Version:    0.6.2
 */