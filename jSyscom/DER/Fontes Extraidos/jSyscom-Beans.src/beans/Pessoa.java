package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

public class Pessoa extends Observable
{
  private Integer cdPessoa;
  private char tipoPessoa;
  private String nomePessoa;
  private Date dataNascimento;
  private Date dataCadPessoa;
  private String descricaoPessoa;
  private String emailPessoa;
  private String sitePessoa;
  private String telefoneFixoPessoa;
  private String telefoneCellPessoa;
  private String cpfCnpjPessoa;
  private String rgIePessoa;
  private String orgaoRgPessoa;
  private Date dataExpRgPessoa;
  private String profissaoPessoa;
  private String empresaPessoa;
  private String foneEmpresaPessoa;
  private char sexoPessoa;
  private Boolean cliente;
  private Boolean funcionario;
  private Boolean fornecedor;
  private Double descontoCliente;
  private Double valorHoraFuncionario;
  private ArrayList<Endereco> enderecos;
  private ArrayList<Contato> contatos;
  private Boolean flagPronto;

  public Boolean isCliente()
  {
    return getCliente();
  }

  public Boolean isFornecedor() {
    return getFornecedor();
  }

  public Boolean isFuncionario() {
    return getFuncionario();
  }

  public Boolean getCliente() {
    return this.cliente;
  }

  public void setCliente(Boolean cliente) {
    this.cliente = cliente;
  }

  public Boolean getFornecedor() {
    return this.fornecedor;
  }

  public void setFornecedor(Boolean fornecedor) {
    this.fornecedor = fornecedor;
  }

  public Boolean getFuncionario() {
    return this.funcionario;
  }

  public void setFuncionario(Boolean funcionario) {
    this.funcionario = funcionario;
  }

  public Pessoa() {
    this.enderecos = new ArrayList();
    this.contatos = new ArrayList();
    clearPessoa();
  }

  public Pessoa(Integer cdPessoa) {
    this.enderecos = new ArrayList();
    this.contatos = new ArrayList();
    clearPessoa();
    this.cdPessoa = cdPessoa;
  }

  public final void clearPessoa() {
    this.cdPessoa = null;
    this.tipoPessoa = 'F';
    this.nomePessoa = "";
    this.dataNascimento = null;
    this.dataCadPessoa = null;
    this.descricaoPessoa = "";
    this.emailPessoa = "";
    this.sitePessoa = "";
    this.telefoneFixoPessoa = "";
    this.telefoneCellPessoa = "";
    this.cpfCnpjPessoa = "";
    this.rgIePessoa = "";
    this.orgaoRgPessoa = "";
    this.dataExpRgPessoa = null;
    this.profissaoPessoa = "";
    this.empresaPessoa = "";
    this.foneEmpresaPessoa = "";
    this.sexoPessoa = 'M';
    this.cliente = Boolean.valueOf(false);
    this.funcionario = Boolean.valueOf(false);
    this.fornecedor = Boolean.valueOf(false);
    this.descontoCliente = Double.valueOf(0.0D);
    setValorHoraFuncionario(Double.valueOf(0.0D));
    this.enderecos.clear();
    this.contatos.clear();
  }

  public Integer getCdPessoa()
  {
    return this.cdPessoa;
  }

  public void setCdPessoa(Integer cdPessoa)
  {
    this.cdPessoa = cdPessoa;
  }

  public char getTipoPessoa()
  {
    return this.tipoPessoa;
  }

  public void setTipoPessoa(char tipoPessoa)
  {
    this.tipoPessoa = tipoPessoa;
  }

  public String getNomePessoa()
  {
    return this.nomePessoa;
  }

  public void setNomePessoa(String nomePessoa)
  {
    this.nomePessoa = (nomePessoa != null ? nomePessoa.toUpperCase().trim() : null);
  }

  public Date getDataNascimento()
  {
    return this.dataNascimento;
  }

  public void setDataNascimento(Date dataNascimento)
  {
    this.dataNascimento = dataNascimento;
  }

  public Date getDataCadPessoa()
  {
    return this.dataCadPessoa;
  }

  public void setDataCadPessoa(Date dataCadPessoa)
  {
    this.dataCadPessoa = dataCadPessoa;
  }

  public String getDescricaoPessoa()
  {
    return this.descricaoPessoa;
  }

  public void setDescricaoPessoa(String descricaoPessoa)
  {
    this.descricaoPessoa = (descricaoPessoa != null ? descricaoPessoa.toUpperCase().trim() : null);
  }

  public String getEmailPessoa()
  {
    return this.emailPessoa;
  }

  public void setEmailPessoa(String emailPessoa)
  {
    this.emailPessoa = emailPessoa;
  }

  public String getSitePessoa()
  {
    return this.sitePessoa;
  }

  public void setSitePessoa(String sitePessoa)
  {
    this.sitePessoa = sitePessoa;
  }

  public String getTelefoneFixoPessoa()
  {
    return this.telefoneFixoPessoa;
  }

  public void setTelefoneFixoPessoa(String telefoneFixoPessoa)
  {
    this.telefoneFixoPessoa = telefoneFixoPessoa;
  }

  public String getTelefoneCellPessoa()
  {
    return this.telefoneCellPessoa;
  }

  public void setTelefoneCellPessoa(String telefoneCellPessoa)
  {
    this.telefoneCellPessoa = telefoneCellPessoa;
  }

  public String getCpfCnpjPessoa()
  {
    return this.cpfCnpjPessoa;
  }

  public void setCpfCnpjPessoa(String cpfCnpjPessoa)
  {
    this.cpfCnpjPessoa = cpfCnpjPessoa;
  }

  public String getRgIePessoa()
  {
    return this.rgIePessoa;
  }

  public void setRgIePessoa(String rgIePessoa)
  {
    this.rgIePessoa = rgIePessoa;
  }

  public String getOrgaoRgPessoa()
  {
    return this.orgaoRgPessoa;
  }

  public void setOrgaoRgPessoa(String orgaoRgPessoa)
  {
    this.orgaoRgPessoa = (orgaoRgPessoa != null ? orgaoRgPessoa.toUpperCase().trim() : null);
  }

  public Date getDataExpRgPessoa()
  {
    return this.dataExpRgPessoa;
  }

  public void setDataExpRgPessoa(Date dataExpRgPessoa)
  {
    this.dataExpRgPessoa = dataExpRgPessoa;
  }

  public String getProfissaoPessoa()
  {
    return this.profissaoPessoa;
  }

  public void setProfissaoPessoa(String profissaoPessoa)
  {
    this.profissaoPessoa = (profissaoPessoa != null ? profissaoPessoa.toUpperCase().trim() : null);
  }

  public String getEmpresaPessoa()
  {
    return this.empresaPessoa;
  }

  public void setEmpresaPessoa(String empresaPessoa)
  {
    this.empresaPessoa = (empresaPessoa != null ? empresaPessoa.toUpperCase().trim() : null);
  }

  public String getFoneEmpresaPessoa()
  {
    return this.foneEmpresaPessoa;
  }

  public void setFoneEmpresaPessoa(String foneEmpresaPessoa)
  {
    this.foneEmpresaPessoa = foneEmpresaPessoa;
  }

  public char getSexoPessoa()
  {
    return this.sexoPessoa;
  }

  public void setSexoPessoa(char sexoPessoa)
  {
    this.sexoPessoa = sexoPessoa;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Pessoa other = (Pessoa)obj;
    if ((this.cdPessoa != other.cdPessoa) && ((this.cdPessoa == null) || (!this.cdPessoa.equals(other.cdPessoa)))) {
      return false;
    }
    if (this.tipoPessoa != other.tipoPessoa) {
      return false;
    }
    if (this.nomePessoa == null ? other.nomePessoa != null : !this.nomePessoa.equals(other.nomePessoa)) {
      return false;
    }
    if (this.cpfCnpjPessoa == null ? other.cpfCnpjPessoa != null : !this.cpfCnpjPessoa.equals(other.cpfCnpjPessoa)) {
      return false;
    }
    if (this.rgIePessoa == null ? other.rgIePessoa != null : !this.rgIePessoa.equals(other.rgIePessoa)) {
      return false;
    }
    return true;
  }

  public int hashCode()
  {
    int hash = 3;
    return hash;
  }

  public ArrayList<Endereco> getEnderecos()
  {
    return this.enderecos;
  }

  public void setEnderecos(ArrayList<Endereco> enderecos)
  {
    this.enderecos = enderecos;
  }

  public ArrayList<Contato> getContatos()
  {
    return this.contatos;
  }

  public void setContatos(ArrayList<Contato> contatos)
  {
    this.contatos = contatos;
  }

  public Double getDescontoCliente()
  {
    return this.descontoCliente;
  }

  public void setDescontoCliente(Double descontoCliente)
  {
    this.descontoCliente = descontoCliente;
  }

  public Double getValorHoraFuncionario()
  {
    return this.valorHoraFuncionario;
  }

  public void setValorHoraFuncionario(Double valorHoraFuncionario)
  {
    this.valorHoraFuncionario = valorHoraFuncionario;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Pessoa
 * JD-Core Version:    0.6.2
 */