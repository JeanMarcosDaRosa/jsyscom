package beans;

public class Cliente extends Pessoa
{
  public Cliente()
  {
  }

  public Cliente(Integer codCliente)
  {
    setCdPessoa(codCliente);
  }

  public void clearCliente() {
    clearPessoa();
  }

  public String toString()
  {
    return getNomePessoa();
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Cliente
 * JD-Core Version:    0.6.2
 */