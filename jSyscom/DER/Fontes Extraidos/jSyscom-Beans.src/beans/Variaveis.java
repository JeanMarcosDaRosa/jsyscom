package beans;

import base.FrmPrincipal;
import funcoes.OperacoesDB;
import java.awt.Component;
import java.net.URL;
import utilitarios.Arquivo;
import utilitarios.Extra;
import utilitarios.Format;

public abstract class Variaveis
{
  private static String versao = "0.05-jS";
  private static String ipServ = "localhost";
  private static String dbLocal = "jsyscom";
  private static String dbUser = "root";
  private static String dbPass = "";
  private static Integer dbPort = Integer.valueOf(5432);
  private static Integer dbType = Integer.valueOf(5432);
  private static Object retorno;
  private static String Log = "";
  private static String oldLog = "";
  private static Boolean net = Boolean.valueOf(false);
  private static String aviso = "";
  private static Integer cdAviso = Integer.valueOf(-1);
  private static String nomeMenu = "Menu Principal";
  private static Boolean newLog = Boolean.valueOf(false);
  private static Boolean gravaLog = Boolean.valueOf(true);
  private static Integer distanciaFrames = Integer.valueOf(20);
  private static Integer idImgFundo = Integer.valueOf(-1);
  private static Usuario userSys = null;
  private static Empresa empresa = null;
  private static String nomeLogo = "logo.png";
  private static Boolean bloqueado = Boolean.valueOf(false);
  private static int decimal = 2;
  private static FrmPrincipal formularioPrincipal = null;
  private static URL urlLogo = null;

  public static String getNomeLogo() {
    return nomeLogo;
  }

  public static void setNomeLogo(String nome_logo) {
    nomeLogo = nome_logo;
  }

  public static Usuario getUserSys() {
    return userSys;
  }

  public static void setUserSys(Usuario userSys) {
    userSys = userSys;
  }

  public static Integer getIdImgFundo() {
    return idImgFundo;
  }

  public static void setIdImgFundo(Integer id_imgFundo) {
    idImgFundo = id_imgFundo;
  }

  public static Boolean getNet() {
    return net;
  }

  public static void setNet(Boolean net) {
    net = net;
  }

  public static String getAllAtributs()
  {
    String ret = new StringBuilder().append("----Variaveis----\nversao = '").append(versao).append("'\n").append("path = '").append(Arquivo.getPathBase()).append("'\n").append("ipServ = '").append(ipServ).append("'\n").append("dbLocal = '").append(dbLocal).append("'\n").append("dbUser = '").append(dbUser).append("'\n").append("dbPass = size('").append(String.valueOf(dbPass.length())).append("')\n").append("nomeMenu = '").append(nomeMenu).append("'\n").append("Log = '").append(Log).append("'\n").append("decimal = '").append(String.valueOf(decimal)).append("'\n").append("userNome = '").append(userSys.getNomeUsuario()).append("'\n").append("userCodigo = '").append(String.valueOf(userSys.getCodigoUsuario())).append("'\n").append("empresaNome = '").append(getEmpresa().getFantasia()).append("'\n").append("empresaCodigo = '").append(String.valueOf(getEmpresa().getCodigo())).append("'\n").append("dbPort = '").append(String.valueOf(dbPort)).append("'\n").append("dbType = '").append(String.valueOf(dbType)).append("'\n").append("gravaLog = '").append(String.valueOf(gravaLog)).append("'\n").append("distanciaFrames = '").append(String.valueOf(distanciaFrames)).append("'\n").append("idImgFundo = '").append(String.valueOf(idImgFundo)).append("'\n").append("pathImage = '").append(String.valueOf(getIDImage())).append("'\n").append("net = '").append(getNet().booleanValue() ? "Sim" : "Não").append("'\n").append("newLog = '").append(newLog.toString()).toString();

    return ret;
  }

  public static String getIpServ()
  {
    return ipServ;
  }

  public static Boolean getNewLog() {
    return newLog;
  }

  public static void setNewLog(Boolean newLog) {
    newLog = newLog;
  }

  public static void addNewLog(String log, Boolean saveDB, Boolean showMessage, Component tela) {
    log = new StringBuilder().append("[").append(Format.getDate()).append("  -  ").append(Format.getTime()).append("]\n").append(log).toString();
    Log = new StringBuilder().append(Log).append(log).append("\n").toString();
    oldLog = new StringBuilder().append(oldLog).append("[").append(Format.getDate()).append(" - ").append(Format.getTime()).append("]: ").append(log).append("\n").toString();
    newLog = Boolean.valueOf(true);
    Arquivo.GravaLog("jSyscom.log", log);
    if (saveDB.booleanValue()) {
      OperacoesDB.gravaLogBanco("LOG TEMPO REAL", "INTERNA", log);
    }
    if (showMessage.booleanValue())
      Extra.Informacao(tela, log);
  }

  public static void setIpServ(String aIpServ)
  {
    ipServ = aIpServ;
  }

  public static String getDbLocal()
  {
    return dbLocal;
  }

  public static void setDbLocal(String aDbLocal)
  {
    dbLocal = aDbLocal;
  }

  public static String getDbUser()
  {
    return dbUser;
  }

  public static void setDbUser(String aDbUser)
  {
    dbUser = aDbUser;
  }

  public static String getDbPass()
  {
    return dbPass;
  }

  public static void setDbPass(String aDbPass)
  {
    dbPass = aDbPass;
  }

  public static String getVersao()
  {
    return versao;
  }

  public static void setVersao(String aVersao)
  {
    versao = aVersao;
  }

  public static String getNomeMenu()
  {
    return nomeMenu;
  }

  public static void setNomeMenu(String aNomeMenu)
  {
    nomeMenu = aNomeMenu;
  }

  public static Object getRetorno()
  {
    return retorno;
  }

  public static void setRetorno(Object aRetorno)
  {
    retorno = aRetorno;
  }

  public static String getLog()
  {
    if (newLog.booleanValue()) {
      newLog = Boolean.valueOf(false);
      String t = Log;
      Log = "";
      return t;
    }
    return "";
  }

  public static void setLog(String aLog)
  {
    Log = aLog;
  }

  public static String getOldLog()
  {
    return oldLog;
  }

  public static void setOldLog(String aOldLog)
  {
    oldLog = aOldLog;
  }

  public static Integer getDistanciaFrames()
  {
    return distanciaFrames;
  }

  public static void setDistanciaFrames(Integer aDistanciaFrames)
  {
    distanciaFrames = aDistanciaFrames;
  }

  public static Boolean getGravaLog()
  {
    return gravaLog;
  }

  public static void setGravaLog(Boolean aGravaLog)
  {
    gravaLog = aGravaLog;
  }

  public static Integer getIDImage()
  {
    return idImgFundo;
  }

  public static void setIDImage(Integer aPathImage)
  {
    idImgFundo = aPathImage;
  }

  public static Integer getDbPort()
  {
    return dbPort;
  }

  public static void setDbPort(Integer aDbPort)
  {
    dbPort = aDbPort;
  }

  public static Integer getDbType()
  {
    return dbType;
  }

  public static void setDbType(Integer aDbType)
  {
    dbType = aDbType;
  }

  public static String getAviso()
  {
    return aviso;
  }

  public static void setAviso(String aAviso)
  {
    aviso = aAviso;
  }

  public static Integer getCdAviso()
  {
    return cdAviso;
  }

  public static void setCdAviso(Integer aCd_aviso)
  {
    cdAviso = aCd_aviso;
  }

  public static Empresa getEmpresa()
  {
    return empresa;
  }

  public static void setEmpresa(Empresa aEmpresa)
  {
    empresa = aEmpresa;
  }

  public static Boolean getBloqueado()
  {
    return bloqueado;
  }

  public static void setBloqueado(Boolean aBloqueado)
  {
    bloqueado = aBloqueado;
  }

  public static FrmPrincipal getFormularioPrincipal()
  {
    return formularioPrincipal;
  }

  public static void setFormularioPrincipal(FrmPrincipal aFormularioPrincipal)
  {
    formularioPrincipal = aFormularioPrincipal;
  }

  public static int getDecimal()
  {
    return decimal;
  }

  public static void setDecimal(int aDecimal)
  {
    decimal = aDecimal;
  }

  public static URL getUrlLogo()
  {
    return urlLogo;
  }

  public static void setUrlLogo(URL aUrlLogo)
  {
    urlLogo = aUrlLogo;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Variaveis
 * JD-Core Version:    0.6.2
 */