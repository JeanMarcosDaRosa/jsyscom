package beans;

import java.util.Observable;

public class Unidade extends Observable
{
  private String nome;
  private String sigla;
  private Boolean flagPronto;

  public Unidade(String nome, String sigla)
  {
    this.nome = (nome != null ? nome.toUpperCase().trim() : null);
    this.sigla = (sigla != null ? sigla.toUpperCase().trim() : null);
  }

  public Unidade()
  {
  }

  public String getNome()
  {
    return this.nome;
  }

  public void setNome(String nome)
  {
    this.nome = (nome != null ? nome.toUpperCase().trim() : null);
  }

  public String getSigla()
  {
    return this.sigla;
  }

  public void setSigla(String sigla)
  {
    this.sigla = (sigla != null ? sigla.toUpperCase().trim() : null);
  }

  public String toString()
  {
    return this.sigla.trim();
  }

  public int hashCode()
  {
    int hash = 3;
    return hash;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Unidade other = (Unidade)obj;
    if (this.sigla == null ? other.sigla != null : !this.sigla.equals(other.sigla)) {
      return false;
    }
    return true;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Unidade
 * JD-Core Version:    0.6.2
 */