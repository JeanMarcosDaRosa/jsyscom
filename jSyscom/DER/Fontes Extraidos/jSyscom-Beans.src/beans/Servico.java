package beans;

import java.util.Date;
import java.util.Observable;

public class Servico extends Observable
  implements Cloneable
{
  private Integer codServico;
  private String descServico;
  private Double valorServico;
  private Date dataCadastro;
  private Boolean flagPronto;

  public Servico()
  {
  }

  public Servico(Integer codServico)
  {
    this.codServico = codServico;
  }

  public Servico(String descServico, Double valorServico) {
    this.descServico = (descServico != null ? descServico.toUpperCase().trim() : null);
    this.valorServico = valorServico;
  }

  public Servico(Integer codServico, String descServico, Double valorServico) {
    this.codServico = codServico;
    this.descServico = (descServico != null ? descServico.toUpperCase().trim() : null);
    this.valorServico = valorServico;
    this.dataCadastro = null;
  }

  public void clearServico() {
    this.codServico = null;
    this.descServico = "";
    this.valorServico = Double.valueOf(0.0D);
    this.dataCadastro = null;
  }

  public String toString()
  {
    return this.descServico;
  }

  public int hashCode()
  {
    int hash = 5;
    return hash;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Servico other = (Servico)obj;
    if (this.descServico == null ? other.descServico != null : !this.descServico.equals(other.descServico)) {
      return false;
    }
    return true;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public Integer getCodServico()
  {
    return this.codServico;
  }

  public void setCodServico(Integer codServico)
  {
    this.codServico = codServico;
  }

  public String getDescServico()
  {
    return this.descServico;
  }

  public void setDescServico(String descServico)
  {
    this.descServico = (descServico != null ? descServico.toUpperCase().trim() : null);
  }

  public Double getValorServico()
  {
    return this.valorServico;
  }

  public void setValorServico(Double valorServico)
  {
    this.valorServico = valorServico;
  }

  public Object clone() throws CloneNotSupportedException
  {
    return super.clone();
  }

  public Date getDataCadastro()
  {
    return this.dataCadastro;
  }

  public void setDataCadastro(Date dataCadastro)
  {
    this.dataCadastro = dataCadastro;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Servico
 * JD-Core Version:    0.6.2
 */