package beans;

import java.util.Date;
import java.util.Observable;
import utilitarios.Format;

public class Equipamento extends Observable
{
  private Integer codEquipamento;
  private String marca;
  private String modelo;
  private String descricao;
  private Integer nroRastreio;
  private String nroSerie;
  private Date dataFabricacao;
  private TipoEquipamento tipoEquipamento;
  private Imagem conexao;
  private Cliente cliente;
  private Boolean flagPronto;

  public Equipamento()
  {
  }

  public Equipamento(Integer codEquipamento)
  {
    this.codEquipamento = codEquipamento;
  }

  public Equipamento(String nroSerie) {
    this.nroSerie = nroSerie;
  }

  public Equipamento(Integer codEquipamento, String marca, String modelo, String descricao, Integer nroRastreio, String nroSerie, Date dataFabricacao, TipoEquipamento tipoEquipamento, Imagem conexao, Cliente cliente) {
    this.codEquipamento = codEquipamento;
    this.marca = (marca != null ? marca.toUpperCase().trim() : null);
    this.modelo = (modelo != null ? modelo.toUpperCase().trim() : null);
    this.descricao = descricao;
    this.nroRastreio = nroRastreio;
    this.nroSerie = (nroSerie != null ? Format.RemoveCaracteres(nroSerie.toUpperCase().trim(), "-/") : null);
    this.dataFabricacao = dataFabricacao;
    this.tipoEquipamento = tipoEquipamento;
    this.conexao = conexao;
    this.cliente = cliente;
  }

  public Integer getCodEquipamento()
  {
    return this.codEquipamento;
  }

  public void setCodEquipamento(Integer codEquipamento)
  {
    this.codEquipamento = codEquipamento;
  }

  public String getMarca()
  {
    return this.marca;
  }

  public void setMarca(String marca)
  {
    this.marca = (marca != null ? marca.toUpperCase().trim() : null);
  }

  public String getModelo()
  {
    return this.modelo;
  }

  public void setModelo(String modelo)
  {
    this.modelo = (modelo != null ? modelo.toUpperCase().trim() : null);
  }

  public String getDescricao()
  {
    return this.descricao;
  }

  public void setDescricao(String descricao)
  {
    this.descricao = descricao;
  }

  public Integer getNroRastreio()
  {
    return this.nroRastreio;
  }

  public void setNroRastreio(Integer nroRastreio)
  {
    this.nroRastreio = nroRastreio;
  }

  public String getNroSerie()
  {
    return this.nroSerie;
  }

  public void setNroSerie(String nroSerie)
  {
    this.nroSerie = (nroSerie != null ? Format.RemoveCaracteres(nroSerie.toUpperCase().trim(), "-/") : null);
  }

  public Date getDataFabricacao()
  {
    return this.dataFabricacao;
  }

  public void setDataFabricacao(Date dataFabricacao)
  {
    this.dataFabricacao = dataFabricacao;
  }

  public TipoEquipamento getTipoEquipamento()
  {
    return this.tipoEquipamento;
  }

  public void setTipoEquipamento(TipoEquipamento tipoEquipamento)
  {
    this.tipoEquipamento = tipoEquipamento;
  }

  public Imagem getConexao()
  {
    return this.conexao;
  }

  public void setConexao(Imagem conexao)
  {
    this.conexao = conexao;
  }

  public Cliente getCliente()
  {
    return this.cliente;
  }

  public void setCliente(Cliente cliente)
  {
    this.cliente = cliente;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public final void clearEquipamento() {
    this.codEquipamento = null;
    this.marca = "";
    this.modelo = "";
    this.descricao = "";
    this.nroRastreio = null;
    this.nroSerie = "";
    this.dataFabricacao = null;
    this.tipoEquipamento = null;
    this.conexao = null;
    this.cliente = null;
  }

  public String toString()
  {
    return this.marca + ", " + this.modelo;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Equipamento
 * JD-Core Version:    0.6.2
 */