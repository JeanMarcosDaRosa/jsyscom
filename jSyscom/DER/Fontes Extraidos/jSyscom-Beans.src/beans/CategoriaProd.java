package beans;

import java.util.Observable;

public class CategoriaProd extends Observable
{
  private Integer codCategoria;
  private String descCategoria;
  private Double margemCategoria;
  private Boolean flagPronto;

  public CategoriaProd()
  {
  }

  public CategoriaProd(Integer codCategoria)
  {
    this.codCategoria = codCategoria;
  }

  public CategoriaProd(Integer codCategoria, String descCategoria, Double margemCategoria) {
    this.codCategoria = codCategoria;
    this.descCategoria = (descCategoria != null ? descCategoria.toUpperCase().trim() : null);
    this.margemCategoria = margemCategoria;
  }

  public CategoriaProd(String descCategoria, Double margemCategoria) {
    this.codCategoria = Integer.valueOf(0);
    this.descCategoria = (descCategoria != null ? descCategoria.toUpperCase().trim() : null);
    this.margemCategoria = margemCategoria;
  }

  public Double getMargemCategoria()
  {
    return this.margemCategoria;
  }

  public void setMargemCategoria(Double margemCategoria)
  {
    this.margemCategoria = margemCategoria;
  }

  public Integer getCodCategoria()
  {
    return this.codCategoria;
  }

  public void setCodCategoria(Integer codCategoria)
  {
    this.codCategoria = codCategoria;
  }

  public String getDescCategoria()
  {
    return this.descCategoria;
  }

  public void setDescCategoria(String descCategoria)
  {
    this.descCategoria = (descCategoria != null ? descCategoria.toUpperCase().trim() : null);
  }

  public String toString()
  {
    return this.descCategoria;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    CategoriaProd other = (CategoriaProd)obj;
    if (this.descCategoria == null ? other.descCategoria != null : !this.descCategoria.equals(other.descCategoria)) {
      return false;
    }
    return true;
  }

  public int hashCode()
  {
    int hash = 3;
    return hash;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.CategoriaProd
 * JD-Core Version:    0.6.2
 */