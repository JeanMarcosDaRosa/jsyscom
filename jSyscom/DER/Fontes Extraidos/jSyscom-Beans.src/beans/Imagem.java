package beans;

import java.util.Observable;
import javax.swing.ImageIcon;

public class Imagem extends Observable
  implements Cloneable
{
  private Integer codImagem;
  private String nomeImagem;
  private String md5Imagem;
  private ImageIcon blobImagem;
  private Boolean flagPronto;

  public Imagem()
  {
  }

  public Imagem(Integer codImagem)
  {
    this.codImagem = codImagem;
  }

  public Imagem(ImageIcon blobImagem) {
    this.blobImagem = blobImagem;
  }

  public Imagem(Integer codImagem, String nomeImagem, String md5Imagem, ImageIcon blobImagem) {
    this.codImagem = codImagem;
    this.nomeImagem = nomeImagem;
    this.md5Imagem = md5Imagem;
    this.blobImagem = blobImagem;
  }

  public final void clearImagem() {
    this.codImagem = null;
    this.nomeImagem = "";
    this.md5Imagem = "";
    this.blobImagem = null;
  }

  public Integer getCodImagem()
  {
    return this.codImagem;
  }

  public void setCodImagem(Integer codImagem)
  {
    this.codImagem = codImagem;
  }

  public String getNomeImagem()
  {
    return this.nomeImagem;
  }

  public void setNomeImagem(String nomeImagem)
  {
    this.nomeImagem = nomeImagem;
  }

  public String getMd5Imagem()
  {
    return this.md5Imagem;
  }

  public void setMd5Imagem(String md5Imagem)
  {
    this.md5Imagem = md5Imagem;
  }

  public ImageIcon getBlobImagem()
  {
    return this.blobImagem;
  }

  public void setBlobImagem(ImageIcon blobImagem)
  {
    this.blobImagem = blobImagem;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public Object clone() throws CloneNotSupportedException
  {
    return super.clone();
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Imagem
 * JD-Core Version:    0.6.2
 */