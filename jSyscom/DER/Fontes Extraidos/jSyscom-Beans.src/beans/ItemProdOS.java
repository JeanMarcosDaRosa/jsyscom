package beans;

import java.util.Objects;

public class ItemProdOS
{
  private Produto produto;
  private Double quantidade;
  private Double valorUnitario;
  private Double valorBruto;
  private Boolean original;
  private OrdemServico ordemServico;

  public ItemProdOS()
  {
  }

  public ItemProdOS(OrdemServico ordemServico, Produto produto, Double quantidade, Double valorUnitario, Double valorBruto, Boolean original)
  {
    this.ordemServico = ordemServico;
    this.produto = produto;
    this.quantidade = quantidade;
    this.valorUnitario = valorUnitario;
    this.valorBruto = valorBruto;
    this.original = original;
  }

  public Produto getProduto()
  {
    return this.produto;
  }

  public void setProduto(Produto produto)
  {
    this.produto = produto;
  }

  public Double getQuantidade()
  {
    return this.quantidade;
  }

  public void setQuantidade(Double quantidade)
  {
    this.quantidade = quantidade;
  }

  public Double getValorBruto()
  {
    return this.valorBruto;
  }

  public void setValorBruto(Double valorBruto)
  {
    this.valorBruto = valorBruto;
  }

  public Double getValorUnitario()
  {
    return this.valorUnitario;
  }

  public void setValorUnitario(Double valorUnitario)
  {
    this.valorUnitario = valorUnitario;
  }

  public Boolean getOriginal()
  {
    return this.original;
  }

  public void setOriginal(Boolean original)
  {
    this.original = original;
  }

  public OrdemServico getOrdemServico()
  {
    return this.ordemServico;
  }

  public void setOrdemServico(OrdemServico ordemServico)
  {
    this.ordemServico = ordemServico;
  }

  public int hashCode()
  {
    int hash = 7;
    return hash;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ItemProdOS other = (ItemProdOS)obj;
    if (!Objects.equals(this.produto, other.produto)) {
      return false;
    }
    if (!Objects.equals(this.valorUnitario, other.valorUnitario)) {
      return false;
    }
    return true;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.ItemProdOS
 * JD-Core Version:    0.6.2
 */