package beans;

public class Fornecedor extends Pessoa
{
  public Fornecedor(Integer codFornecedor)
  {
    setCdPessoa(codFornecedor);
  }

  public Fornecedor() {
  }

  public void clearFornecedor() {
    clearPessoa();
  }

  public String toString()
  {
    return getNomePessoa();
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Fornecedor
 * JD-Core Version:    0.6.2
 */