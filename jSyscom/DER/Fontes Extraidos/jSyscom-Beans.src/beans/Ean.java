package beans;

import java.util.Date;
import utilitarios.Validador;

public class Ean
{
  private String ean;
  private Date dataCad;
  private Produto produto;

  public Ean(String ean, Produto produto)
  {
    this.ean = (ean != null ? Validador.soNumeros(ean) : null);
    this.produto = produto;
  }

  public Ean()
  {
  }

  public String getEan()
  {
    return this.ean;
  }

  public void setEan(String ean)
  {
    this.ean = (ean != null ? Validador.soNumeros(ean) : null);
  }

  public Produto getProduto()
  {
    return this.produto;
  }

  public void setProduto(Produto produto)
  {
    this.produto = produto;
  }

  public Date getDataCad()
  {
    return this.dataCad;
  }

  public void setDataCad(Date dataCad)
  {
    this.dataCad = dataCad;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Ean
 * JD-Core Version:    0.6.2
 */