package beans;

import java.util.Objects;

public class Contato
{
  private Integer codContato;
  private String nomeContato;
  private String cargoContato;
  private String telefoneContato;
  private String observacao;
  private Pessoa pessoaContato;

  public Contato()
  {
    clearContato();
  }

  public Contato(Integer codContato) {
    clearContato();
    this.codContato = codContato;
  }

  public Contato(Integer codContato, String nomeContato, String cargoContato, String telefoneContato, String observacao, Pessoa pessoaContato) {
    clearContato();
    this.codContato = codContato;
    this.nomeContato = (nomeContato != null ? nomeContato.toUpperCase().trim() : null);
    this.cargoContato = (cargoContato != null ? cargoContato.toUpperCase().trim() : null);
    this.telefoneContato = telefoneContato;
    this.observacao = observacao;
    this.pessoaContato = pessoaContato;
  }

  public Integer getCodContato()
  {
    return this.codContato;
  }

  public void setCodContato(Integer codContato)
  {
    this.codContato = codContato;
  }

  public String getNomeContato()
  {
    return this.nomeContato;
  }

  public void setNomeContato(String nomeContato)
  {
    this.nomeContato = (nomeContato != null ? nomeContato.toUpperCase().trim() : null);
  }

  public String getCargoContato()
  {
    return this.cargoContato;
  }

  public void setCargoContato(String cargoContato)
  {
    this.cargoContato = (cargoContato != null ? cargoContato.toUpperCase().trim() : null);
  }

  public String getTelefoneContato()
  {
    return this.telefoneContato;
  }

  public void setTelefoneContato(String telefoneContato)
  {
    this.telefoneContato = telefoneContato;
  }

  public Pessoa getPessoaContato()
  {
    return this.pessoaContato;
  }

  public void setPessoaContato(Pessoa pessoaContato)
  {
    this.pessoaContato = pessoaContato;
  }

  public String getObservacao()
  {
    return this.observacao;
  }

  public void setObservacao(String observacao)
  {
    this.observacao = observacao;
  }

  public String toString()
  {
    return this.nomeContato + ", " + this.telefoneContato;
  }

  public final void clearContato() {
    this.codContato = null;
    this.nomeContato = "";
    this.cargoContato = "";
    this.telefoneContato = null;
    this.observacao = "";
    if (this.pessoaContato != null)
      this.pessoaContato.clearPessoa();
  }

  public int hashCode()
  {
    int hash = 5;
    return hash;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Contato other = (Contato)obj;
    if (!Objects.equals(this.codContato, other.codContato)) {
      return false;
    }
    if (!Objects.equals(this.nomeContato, other.nomeContato)) {
      return false;
    }
    return true;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Contato
 * JD-Core Version:    0.6.2
 */