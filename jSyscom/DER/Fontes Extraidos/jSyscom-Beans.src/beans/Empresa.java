package beans;

import utilitarios.Validador;

public class Empresa
{
  private Integer codigo;
  private String rsocial;
  private String fantasia;
  private String cnpj;
  private String ie;
  private String fone;
  private String fax;
  private String responsavel;
  private String email;
  private Endereco endereco;

  public Empresa()
  {
  }

  public Empresa(Integer codigo)
  {
    this.codigo = codigo;
  }

  public Integer getCodigo()
  {
    return this.codigo;
  }

  public void setCodigo(Integer codigo)
  {
    this.codigo = codigo;
  }

  public String getRsocial()
  {
    return this.rsocial;
  }

  public void setRsocial(String rsocial)
  {
    this.rsocial = (rsocial != null ? rsocial.toUpperCase().trim() : null);
  }

  public String getFantasia()
  {
    return this.fantasia;
  }

  public void setFantasia(String fantasia)
  {
    this.fantasia = (fantasia != null ? fantasia.toUpperCase().trim() : null);
  }

  public String getCnpj()
  {
    return this.cnpj;
  }

  public void setCnpj(String cnpj)
  {
    this.cnpj = Validador.soNumeros(cnpj);
  }

  public String getIe()
  {
    return this.ie;
  }

  public void setIe(String ie)
  {
    this.ie = Validador.soNumeros(ie);
  }

  public String getFone()
  {
    return this.fone;
  }

  public void setFone(String fone)
  {
    this.fone = fone;
  }

  public String getFax()
  {
    return this.fax;
  }

  public void setFax(String fax)
  {
    this.fax = fax;
  }

  public String getResponsavel()
  {
    return this.responsavel;
  }

  public void setResponsavel(String responsavel)
  {
    this.responsavel = (responsavel != null ? responsavel.toUpperCase().trim() : null);
  }

  public String getEmail()
  {
    return this.email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public Endereco getEndereco()
  {
    return this.endereco;
  }

  public void setEndereco(Endereco endereco)
  {
    this.endereco = endereco;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Empresa
 * JD-Core Version:    0.6.2
 */