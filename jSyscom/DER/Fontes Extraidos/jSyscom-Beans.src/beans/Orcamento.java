package beans;

import java.util.Date;

public final class Orcamento
{
  private OrdemServico ordemServico;
  private Integer nroRevisao;
  private Boolean autorizado;
  private Date dataCadastro;
  private Date dataAutorizacao;
  private Date dataPrevisao;
  private String tipoFrete;
  private Double valorFrete;
  private Double valorBruto;
  private Double valorLiquido;
  private Double valorDesconto;
  private Boolean usoDescontoCliente;
  private Integer diasValidade;

  public Orcamento(OrdemServico ordemServico)
  {
    clearOrcamento();
    this.ordemServico = ordemServico;
  }

  public Orcamento(Integer nroRevisao, OrdemServico ordemServico) {
    clearOrcamento();
    this.nroRevisao = nroRevisao;
    this.ordemServico = ordemServico;
  }

  public Orcamento(OrdemServico ordemServico, Integer nroRevisao, Boolean autorizado, Date dataCadastro, Date dataAutorizacao, Date dataPrevisao, String tipoFrete, Double valorFrete, Double valorBruto, Double valorLiquido, Double valorDesconto, Boolean usoDescontoCliente, Integer diasValidade) {
    this.ordemServico = ordemServico;
    this.nroRevisao = nroRevisao;
    this.autorizado = autorizado;
    this.dataCadastro = dataCadastro;
    this.dataAutorizacao = dataAutorizacao;
    this.dataPrevisao = dataPrevisao;
    this.tipoFrete = (tipoFrete != null ? tipoFrete.toUpperCase().trim() : null);
    this.valorFrete = valorFrete;
    this.valorBruto = valorBruto;
    this.valorLiquido = valorLiquido;
    this.valorDesconto = valorDesconto;
    this.usoDescontoCliente = usoDescontoCliente;
    this.diasValidade = diasValidade;
  }

  public void clearOrcamento() {
    this.nroRevisao = null;
    this.autorizado = Boolean.valueOf(false);
    this.dataCadastro = null;
    this.dataAutorizacao = null;
    this.dataPrevisao = null;
    this.tipoFrete = "CIF";
    this.valorFrete = Double.valueOf(0.0D);
    this.valorBruto = Double.valueOf(0.0D);
    this.valorLiquido = Double.valueOf(0.0D);
    this.valorDesconto = Double.valueOf(0.0D);
    this.usoDescontoCliente = Boolean.valueOf(false);
    this.diasValidade = null;
  }

  public OrdemServico getOrdemServico()
  {
    return this.ordemServico;
  }

  public void setOrdemServico(OrdemServico ordemServico)
  {
    this.ordemServico = ordemServico;
  }

  public Integer getNroRevisao()
  {
    return this.nroRevisao;
  }

  public void setNroRevisao(Integer nroRevisao)
  {
    this.nroRevisao = nroRevisao;
  }

  public Boolean getAutorizado()
  {
    return this.autorizado;
  }

  public void setAutorizado(Boolean autorizado)
  {
    this.autorizado = autorizado;
  }

  public Date getDataCadastro()
  {
    return this.dataCadastro;
  }

  public void setDataCadastro(Date dataCadastro)
  {
    this.dataCadastro = dataCadastro;
  }

  public Date getDataAutorizacao()
  {
    return this.dataAutorizacao;
  }

  public void setDataAutorizacao(Date dataAutorizacao)
  {
    this.dataAutorizacao = dataAutorizacao;
  }

  public Date getDataPrevisao()
  {
    return this.dataPrevisao;
  }

  public void setDataPrevisao(Date dataPrevisao)
  {
    this.dataPrevisao = dataPrevisao;
  }

  public String getTipoFrete()
  {
    return this.tipoFrete;
  }

  public void setTipoFrete(String tipoFrete)
  {
    this.tipoFrete = (tipoFrete != null ? tipoFrete.toUpperCase().trim() : null);
  }

  public Double getValorFrete()
  {
    return this.valorFrete;
  }

  public void setValorFrete(Double valorFrete)
  {
    this.valorFrete = valorFrete;
  }

  public Double getValorBruto()
  {
    return this.valorBruto;
  }

  public void setValorBruto(Double valorBruto)
  {
    this.valorBruto = valorBruto;
  }

  public Double getValorLiquido()
  {
    return this.valorLiquido;
  }

  public void setValorLiquido(Double valorLiquido)
  {
    this.valorLiquido = valorLiquido;
  }

  public Double getValorDesconto()
  {
    return this.valorDesconto;
  }

  public void setValorDesconto(Double valorDesconto)
  {
    this.valorDesconto = valorDesconto;
  }

  public Boolean getUsoDescontoCliente()
  {
    return this.usoDescontoCliente;
  }

  public void setUsoDescontoCliente(Boolean usoDescontoCliente)
  {
    this.usoDescontoCliente = usoDescontoCliente;
  }

  public Integer getDiasValidade()
  {
    return this.diasValidade;
  }

  public void setDiasValidade(Integer diasValidade)
  {
    this.diasValidade = diasValidade;
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Beans.jar
 * Qualified Name:     beans.Orcamento
 * JD-Core Version:    0.6.2
 */