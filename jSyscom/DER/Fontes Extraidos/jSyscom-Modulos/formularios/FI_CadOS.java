package formularios;

import base.FrmPrincipal;
import beans.Cliente;
import beans.Contato;
import beans.Empresa;
import beans.Endereco;
import beans.Equipamento;
import beans.EquipamentoTeste;
import beans.Funcionario;
import beans.Imagem;
import beans.ItemProdOS;
import beans.ItemPropTipoEquip;
import beans.ItemServicoOS;
import beans.LocalProd;
import beans.OpcoesTela;
import beans.Orcamento;
import beans.OrdemServico;
import beans.Pessoa;
import beans.Produto;
import beans.PropriedadeTipoEquip;
import beans.Servico;
import beans.TipoEquipamento;
import beans.Transportadora;
import beans.Usuario;
import beans.Variaveis;
import dao.EquipamentoDao;
import dao.ImagemDao;
import dao.NextDaos;
import dao.OrdemServicoDao;
import dao.PessoaDao;
import dao.ProdutoDao;
import dao.ServicoDao;
import dao.TransportadoraDao;
import funcoes.Util;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.ELProperty;
import org.jdesktop.observablecollections.ObservableCollections;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.JTableBinding.ColumnBinding;
import org.jdesktop.swingbinding.SwingBindings;
import utilitarios.Arquivo;
import utilitarios.Extra;
import utilitarios.Format;
import utilitarios.ImageFileView;
import utilitarios.ImageFilter;
import utilitarios.ImagePreview;
import utilitarios.Utils;
import utilitarios.Validador;

public final class FI_CadOS extends JInternalFrame
  implements Observer, ListSelectionListener
{
  private static Integer numInstancias = Integer.valueOf(0);
  private Integer numMaxInstancias = Integer.valueOf(65535);
  private static final String versao = "v0.01";
  private FrmPrincipal princ = null;
  private static Integer seqInstancias = Integer.valueOf(0);
  boolean alterado = false;
  private Usuario usuario = null;
  private JFileChooser fc;
  private String nomeArq = null;
  private TipoEquipamento tipoEquipAnterior = null;
  boolean aa = true;
  private JButton btnAddEnvolvidos;
  private JButton btnAddItemProd;
  private JButton btnAddItemServ;
  private JButton btnAddProd;
  private JButton btnAddServ;
  private JButton btnAddTeste;
  private JButton btnAtualizarValoresProd;
  private JButton btnAtualizarValoresServ;
  private JButton btnAuthOrc;
  private JButton btnBuscaCliente;
  private JButton btnBuscarOS;
  private JButton btnCancOS;
  private JButton btnCancelarItemProd;
  private JButton btnCancelarItemServ;
  private JButton btnCarregaImagemBD;
  private JButton btnCarregaImagemPasta;
  private JButton btnEditOS;
  private JButton btnExcluirOS;
  private JButton btnFinalPgto;
  private JButton btnGerarOrc;
  private JButton btnImprConserto;
  private JButton btnImprFichaCli;
  private JButton btnImprOrc;
  private JButton btnLimparImagem;
  private JButton btnManutReal;
  private JButton btnNovaLocalizacao;
  private JButton btnNovaOS;
  private JButton btnNovaTrasp;
  private JButton btnNovoTipoEquip;
  private JButton btnRecalcularTotais;
  private JButton btnSair;
  private JButton btnSaveOS;
  private JButton btnVerificar;
  private ButtonGroup buttonGroup1;
  private JComboBox cboxContatoCliente;
  private JComboBox cboxEnderCliente;
  private JComboBox cboxGarantia;
  private JComboBox cboxLocal;
  private JComboBox cboxStatus;
  private JComboBox cboxTipoEquip;
  private JComboBox cboxTipoFrete;
  private JComboBox cboxTransp;
  private JCheckBox chkAplicarDesconto;
  private JCheckBox chkGarantia;
  private JCheckBox chkMostraQtdHoras;
  private JCheckBox chkProdOriginal;
  private Cliente cliente;
  private JTextField edtCodCliente;
  private JTextArea edtConserto;
  private JTextField edtCpfCliente;
  private JFormattedTextField edtDataAuth;
  private JFormattedTextField edtDataCad;
  private JFormattedTextField edtDataEntrada;
  private JFormattedTextField edtDataFbr;
  private JFormattedTextField edtDataPrev;
  private JFormattedTextField edtDataSaida;
  private JTextArea edtDefeito;
  private JTextField edtDescServ;
  private JTextField edtDesconto;
  private JTextField edtDescrProduto;
  private JTextField edtDescrTeste;
  private JTextArea edtDescricaoEquip;
  private JTextArea edtEstadoEquip;
  private JTextField edtHoras;
  private JTextField edtMarca;
  private JTextField edtMargemCliente;
  private JTextField edtModelo;
  private JTextField edtNomeCliente;
  private JTextField edtNroOCcliente;
  private JTextField edtNroRastreio;
  private JTextField edtNroSerie;
  private JTextArea edtObservServ;
  private JTextArea edtParecer;
  private JTextField edtPrecoProd;
  private JTextField edtQtdeProduto;
  private JTextField edtResultTest;
  private JTextField edtTelefoneCliente;
  private JTextField edtValorBrutoServ;
  private JTextField edtValorFrete;
  private JTextField edtValorUnitServ;
  private JTextField edtVlrBruto;
  private JTextField edtVlrTotal;
  private Equipamento equipamento;
  private JLabel iconConexao;
  private JLabel iconProd;
  private Imagem imagemConexao;
  private Imagem imagemProd;
  private JLabel jLabel1;
  private JLabel jLabel10;
  private JLabel jLabel11;
  private JLabel jLabel12;
  private JLabel jLabel13;
  private JLabel jLabel17;
  private JLabel jLabel18;
  private JLabel jLabel19;
  private JLabel jLabel2;
  private JLabel jLabel20;
  private JLabel jLabel21;
  private JLabel jLabel22;
  private JLabel jLabel23;
  private JLabel jLabel24;
  private JLabel jLabel25;
  private JLabel jLabel26;
  private JLabel jLabel27;
  private JLabel jLabel28;
  private JLabel jLabel29;
  private JLabel jLabel3;
  private JLabel jLabel30;
  private JLabel jLabel31;
  private JLabel jLabel32;
  private JLabel jLabel33;
  private JLabel jLabel34;
  private JLabel jLabel35;
  private JLabel jLabel36;
  private JLabel jLabel37;
  private JLabel jLabel38;
  private JLabel jLabel39;
  private JLabel jLabel4;
  private JLabel jLabel40;
  private JLabel jLabel41;
  private JLabel jLabel42;
  private JLabel jLabel43;
  private JLabel jLabel44;
  private JLabel jLabel45;
  private JLabel jLabel46;
  private JLabel jLabel47;
  private JLabel jLabel5;
  private JLabel jLabel6;
  private JLabel jLabel7;
  private JLabel jLabel8;
  private JLabel jLabel9;
  private JPanel jPanel1;
  private JScrollPane jScrollPane1;
  private JScrollPane jScrollPane10;
  private JScrollPane jScrollPane11;
  private JScrollPane jScrollPane12;
  private JScrollPane jScrollPane13;
  private JScrollPane jScrollPane14;
  private JScrollPane jScrollPane15;
  private JScrollPane jScrollPane2;
  private JScrollPane jScrollPane3;
  private JScrollPane jScrollPane4;
  private JScrollPane jScrollPane5;
  private JScrollPane jScrollPane6;
  private JScrollPane jScrollPane7;
  private JScrollPane jScrollPane8;
  private JScrollPane jScrollPane9;
  private JSeparator jSeparator1;
  private JSeparator jSeparator2;
  private JToggleButton jToggleButton1;
  private JLabel lblCodigo;
  private JLabel lblDataAuth;
  private JLabel lblDataCad;
  private JLabel lblDataNasc2;
  private JLabel lblDataNasc3;
  private JLabel lblPrecoProd;
  private JLabel lblSubTotalProd;
  private JLabel lblSubtotalServ;
  private List<Pessoa> listaEnvolvidos;
  private List<EquipamentoTeste> listaEquipamentoTestes;
  private List<ItemProdOS> listaItensProd;
  private List<ItemPropTipoEquip> listaItensPropriedades;
  private List<ItemServicoOS> listaItensServico;
  private List<Orcamento> listaOrcamentos;
  private LocalProd localProd;
  private JMenuItem mAnalizeRemover;
  private JMenuItem mEnvolvidosRemover;
  private JMenuItem mItemProdAlterar;
  private JMenuItem mItemProdRemover;
  private JMenuItem mItemServAlterar;
  private JMenuItem mItemServRemover;
  private JPopupMenu menuAnalize;
  private JPopupMenu menuEnvolvidos;
  private JPopupMenu menuItensProd;
  private JPopupMenu menuItensServ;
  public OrdemServico ordemServico;
  private JTabbedPane pnlAbasEquipamento;
  private JPanel pnlAjustItensProd;
  private JPanel pnlAnalize;
  private JPanel pnlBotoes;
  private JPanel pnlCodPessoa;
  private JPanel pnlComplementar;
  private JPanel pnlConserto;
  private JPanel pnlDCliente;
  private JPanel pnlDEquipamento;
  private JPanel pnlDetalhes;
  private JPanel pnlEnvolvidos;
  private JPanel pnlFinalizacao;
  private JPanel pnlHistEquipEntr;
  private JPanel pnlInfoBasicas;
  private JPanel pnlInformacoes;
  private JPanel pnlItensProd;
  private JPanel pnlItensServ;
  private JPanel pnlManutencao;
  private JPanel pnlOCPendentes;
  private JPanel pnlOrcamento;
  private JPanel pnlPrincipalOS;
  private JTabbedPane pnlTabManutencao;
  private JTabbedPane pnlTabulado;
  private JTabbedPane pnlTabuladoFinalizacao;
  private Produto produto;
  private Servico servico;
  private JTable tabelaEnvolvidos;
  private JTable tabelaHistEquipEntr;
  private JTable tabelaItensProd;
  private JTable tabelaItensServico;
  private JTable tabelaOCPendentes;
  private JTable tabelaOrcamentos;
  private JTable tabelaPropriedades;
  private JTable tabelaTestes;
  private TipoEquipamento tipoEquipamento;
  private Transportadora transportadora;
  private BindingGroup bindingGroup;

  public Usuario getUsuario()
  {
    return this.usuario;
  }

  public void setUsuario(Usuario usuario) {
    this.usuario = usuario;
  }

  public Integer getNumInstancias() {
    return numInstancias;
  }

  public void addNumInstancia(Integer i) {
    numInstancias = Integer.valueOf(numInstancias.intValue() + i.intValue());
    if (i.intValue() > -1)
      seqInstancias = Integer.valueOf(seqInstancias.intValue() + 1);
  }

  public FI_CadOS(String title, OpcoesTela parametros, Usuario user)
  {
    if (numInstancias.intValue() >= this.numMaxInstancias.intValue()) {
      String msg = new StringBuilder().append("Número máximo instâncias (").append(this.numMaxInstancias).append(") de ").append(title).append(" atingido!").toString();
      Variaveis.addNewLog(msg, Boolean.valueOf(false), Boolean.valueOf(true), this);
    } else {
      initComponents();
      this.usuario = user;
      if (parametros.getShow_version().booleanValue()) {
        title = new StringBuilder().append(title).append(" [v0.01]").toString();
      }
      setClosable(parametros.getClosable().booleanValue());
      setIconifiable(parametros.getIconifiable().booleanValue());
      setMaximizable(parametros.getMaximizable().booleanValue());
      setResizable(parametros.getResizable().booleanValue());
      setTitle(new StringBuilder().append(title).append(seqInstancias.intValue() > 0 ? new StringBuilder().append(" - [").append(String.valueOf(seqInstancias)).append("]").toString() : "").toString());
      this.princ = Variaveis.getFormularioPrincipal();
      this.princ.setLocalizacaoFrame(this);
      this.princ.Desktop.add(this, JLayeredPane.DEFAULT_LAYER);
      this.princ.Desktop.setComponentZOrder(this, 0);
      setVisible(parametros.getVisible().booleanValue());
      addNumInstancia(Integer.valueOf(1));
      ativaEdits(Boolean.valueOf(false));
      ativaButtons(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false));
      loadLstLocalProd(ProdutoDao.getAllLocaisProdutos(Variaveis.getEmpresa()));
      loadLstTipoEquipamento(EquipamentoDao.getAllTipoEquipamento(Variaveis.getEmpresa()));
      loadLstTransportadora(TransportadoraDao.getAllTransportadoras(Variaveis.getEmpresa()));
      this.pnlTabulado.setEnabledAt(1, false);
      this.pnlTabulado.setEnabledAt(2, false);
      this.pnlTabulado.setEnabledAt(3, false);
      this.pnlTabulado.setEnabledAt(4, false);
      this.imagemConexao.addObserver(this);
      this.ordemServico.addObserver(this);
      this.tipoEquipamento.addObserver(this);
      this.transportadora.addObserver(this);
      this.localProd.addObserver(this);
      this.cliente.addObserver(this);
      this.produto.addObserver(this);
      this.servico.addObserver(this);
      this.iconProd.setHorizontalAlignment(0);
      this.iconConexao.setHorizontalAlignment(0);
      this.tabelaItensProd.getSelectionModel().addListSelectionListener(this);
      this.tabelaItensProd.getColumnModel().getSelectionModel().addListSelectionListener(this);
      limpaCampos();
    }
  }

  public void valueChanged(ListSelectionEvent e)
  {
    if ((e.getSource() == this.tabelaItensProd.getSelectionModel()) && (this.tabelaItensProd.getRowSelectionAllowed()) && (this.pnlTabulado.getSelectedComponent() == this.pnlItensProd))
      try
      {
        if ((this.tabelaItensProd.getRowCount() > 0) && (this.tabelaItensProd.getSelectedRow() >= 0)) {
          this.lblPrecoProd.setText(Format.FormataValor(Format.doubleToString(((ItemProdOS)this.listaItensProd.get(this.tabelaItensProd.getSelectedRow())).getValorUnitario()), Variaveis.getDecimal()));
          setImagemProd(((ItemProdOS)this.listaItensProd.get(this.tabelaItensProd.getSelectedRow())).getProduto());
        }
      } catch (Exception ex) {
        Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
      }
  }

  public void loadLstLocalProd(List<LocalProd> lista)
  {
    String selected = "";
    if (this.cboxLocal.getSelectedItem() != null) {
      selected = this.cboxLocal.getSelectedItem().toString();
    }
    this.cboxLocal.removeAllItems();
    this.cboxLocal.addItem("");
    for (LocalProd cp : lista) {
      this.cboxLocal.addItem(cp);
    }
    this.cboxLocal.setSelectedItem(selected);
  }

  public void loadLstTipoEquipamento(List<TipoEquipamento> lista) {
    String selected = "";
    if (this.cboxTipoEquip.getSelectedItem() != null) {
      selected = this.cboxTipoEquip.getSelectedItem().toString();
    }
    this.cboxTipoEquip.removeAllItems();
    this.cboxTipoEquip.addItem("");
    for (TipoEquipamento cp : lista) {
      this.cboxTipoEquip.addItem(cp);
    }
    this.cboxTipoEquip.setSelectedItem(selected);
  }

  public void loadLstTransportadora(List<Transportadora> lista) {
    String selected = "";
    if (this.cboxTransp.getSelectedItem() != null) {
      selected = this.cboxTransp.getSelectedItem().toString();
    }
    this.cboxTransp.removeAllItems();
    this.cboxTransp.addItem("");
    for (Transportadora cp : lista) {
      this.cboxTransp.addItem(cp);
    }
    this.cboxTransp.setSelectedItem(selected);
  }

  public void setImagemConexao() {
    this.iconConexao.setText("");
    this.equipamento.setConexao(this.imagemConexao);
    this.iconConexao.setIcon(this.imagemConexao.getBlobImagem());
    if (this.imagemConexao.getBlobImagem() != null) {
      if (this.imagemConexao.getBlobImagem().getIconWidth() > this.iconConexao.getWidth())
        this.iconConexao.setIcon(new ImageIcon(this.imagemConexao.getBlobImagem().getImage().getScaledInstance(this.iconConexao.getWidth(), this.iconConexao.getHeight(), 1)));
    }
    else
      this.iconConexao.setText("Conexão Equipamento");
  }

  public void setImagemProd(Produto prod)
  {
    if ((prod != null) && 
      (prod.getImagemProduto().getBlobImagem() != null))
      if (prod.getImagemProduto().getBlobImagem().getIconWidth() > 256) {
        this.imagemProd.setBlobImagem(new ImageIcon(prod.getImagemProduto().getBlobImagem().getImage().getScaledInstance(256, -1, 1)));
        this.iconProd.setIcon(this.imagemProd.getBlobImagem());
      } else {
        this.iconProd.setIcon(prod.getImagemProduto().getBlobImagem());
      }
  }

  public void setLocalizacao()
  {
    loadLstLocalProd(ProdutoDao.getAllLocaisProdutos(Variaveis.getEmpresa()));
    this.cboxLocal.setSelectedItem(this.localProd);
  }

  public void setTipoEquip() {
    this.aa = false;
    loadLstTipoEquipamento(EquipamentoDao.getAllTipoEquipamento(Variaveis.getEmpresa()));
    this.cboxTipoEquip.setSelectedItem(this.tipoEquipamento);
    populaTabelaPropDef();
  }

  public void setTransportadora() {
    loadLstTransportadora(TransportadoraDao.getAllTransportadoras(Variaveis.getEmpresa()));
    this.cboxTransp.setSelectedItem(this.transportadora);
  }

  public void setCliente() {
    try {
      this.edtCodCliente.setText(String.valueOf(this.cliente.getCdPessoa()));
      this.edtNomeCliente.setText(this.cliente.getNomePessoa());
      this.edtCpfCliente.setText(this.cliente.getCpfCnpjPessoa());
      this.edtTelefoneCliente.setText(this.cliente.getTelefoneFixoPessoa());
      this.edtMargemCliente.setText(Format.FormataValor(Format.doubleToString(this.cliente.getDescontoCliente()), Variaveis.getDecimal()));
      this.cboxEnderCliente.removeAllItems();
      Endereco endPrinc = null;
      for (Endereco o : this.cliente.getEnderecos()) {
        this.cboxEnderCliente.addItem(o);
        if (o.getPrincipal().booleanValue()) {
          endPrinc = o;
        }
      }
      if (endPrinc != null) {
        this.cboxEnderCliente.setSelectedItem(endPrinc);
      }
      this.cboxContatoCliente.removeAllItems();
      for (Contato o : this.cliente.getContatos()) {
        this.cboxContatoCliente.addItem(o);
      }
      if (this.cliente.getEnderecos().size() <= 0) {
        Extra.Informacao(this, "Aviso!\n\nEste cliente não tem nenhum endereço associado!\nNão será possivel salvar a OS sem associar um endereço ao cliente!");

        this.cboxEnderCliente.addItem("");
        this.cboxEnderCliente.setSelectedIndex(0);
      }
      if (this.cliente.getContatos().size() <= 0) {
        this.cboxContatoCliente.addItem("");
        this.cboxContatoCliente.setSelectedIndex(0);
      }
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  public void setProduto() {
    try {
      this.edtQtdeProduto.setText("1,00");
      this.chkProdOriginal.setSelected(true);
      this.edtDescrProduto.setText(this.produto.getDescricaoProduto());
      this.edtPrecoProd.setText(Format.FormataValor(Format.doubleToString(this.produto.getValorVendaProduto()), Variaveis.getDecimal()));
      this.lblPrecoProd.setText(Format.FormataValor(Format.doubleToString(this.produto.getValorVendaProduto()), Variaveis.getDecimal()));
      this.btnAddItemProd.setEnabled(true);
      this.btnCancelarItemProd.setEnabled(true);
      this.edtQtdeProduto.setEnabled(true);
      this.chkProdOriginal.setEnabled(true);
      setImagemProd(this.produto);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  public void setServico() {
    try {
      this.edtHoras.setText("0,00");
      this.chkMostraQtdHoras.setSelected(true);
      this.edtDescServ.setText(this.servico.getDescServico());
      this.edtValorUnitServ.setText(Format.FormataValor(Format.doubleToString(this.servico.getValorServico()), Variaveis.getDecimal()));
      this.edtValorBrutoServ.setText(Format.FormataValor(Format.doubleToString(this.servico.getValorServico()), Variaveis.getDecimal()));
      this.edtObservServ.setText("");
      this.btnAddItemServ.setEnabled(true);
      this.btnCancelarItemServ.setEnabled(true);
      this.edtHoras.setEnabled(true);
      this.chkMostraQtdHoras.setEnabled(true);
      this.edtValorBrutoServ.setEnabled(true);
      this.edtObservServ.setEnabled(true);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  public void setOS() {
    try {
      limpaCampos();
      for (Endereco o : this.ordemServico.getCliente().getEnderecos()) {
        this.cboxEnderCliente.addItem(o);
      }
      for (Contato o : this.ordemServico.getCliente().getContatos()) {
        this.cboxContatoCliente.addItem(o);
      }
      this.cliente.setCdPessoa(this.ordemServico.getCliente().getCdPessoa());
      this.cliente.setDescontoCliente(this.ordemServico.getCliente().getDescontoCliente());
      this.cliente.setCpfCnpjPessoa(this.ordemServico.getCliente().getCpfCnpjPessoa());
      this.cliente.setNomePessoa(this.ordemServico.getCliente().getNomePessoa());
      this.cliente.setTelefoneFixoPessoa(this.ordemServico.getCliente().getTelefoneFixoPessoa());
      this.cliente.setEnderecos(this.ordemServico.getCliente().getEnderecos());
      this.cliente.setContatos(this.ordemServico.getCliente().getContatos());
      setCliente();
      this.lblCodigo.setText(String.valueOf(this.ordemServico.getCodOS()));
      this.edtConserto.setText(this.ordemServico.getConsertoDetalhado());
      if (this.ordemServico.getDataCadastro() != null) {
        this.edtDataCad.setText(Format.DateToStr(this.ordemServico.getDataCadastro()));
      }
      if (this.ordemServico.getDataEntrada() != null) {
        this.edtDataEntrada.setText(Format.DateToStr(this.ordemServico.getDataEntrada()));
      }
      if (this.ordemServico.getDataSaida() != null) {
        this.edtDataSaida.setText(Format.DateToStr(this.ordemServico.getDataSaida()));
      }
      this.equipamento = this.ordemServico.getEquipamento();
      setEquipamento();
      this.edtEstadoEquip.setText(this.ordemServico.getEstadoEquipamento());
      this.edtDefeito.setText(this.ordemServico.getDefeitoReclamado());
      this.edtNroOCcliente.setText(String.valueOf(this.ordemServico.getNroOcCliente() != null ? this.ordemServico.getNroOcCliente() : ""));
      this.edtParecer.setText(this.ordemServico.getParecer());
      this.chkGarantia.setSelected(this.ordemServico.getSobGarantia().booleanValue());


      this.listaItensProd.addAll(this.ordemServico.getItensProdutos());
      this.listaItensServico.addAll(this.ordemServico.getItensServicos());
      this.listaEnvolvidos.addAll(this.ordemServico.getEnvolvidos());
      this.listaEquipamentoTestes.addAll(this.ordemServico.getTestesEquipamentos());
      this.listaOrcamentos.addAll(this.ordemServico.getOrcamentos());
      this.listaItensPropriedades.clear();
      this.listaItensPropriedades.addAll(this.ordemServico.getItemsPropriedades());



      if (this.ordemServico.getOrcamentos().size() > 0) {
        this.btnImprOrc.setEnabled(true);
        this.btnAuthOrc.setEnabled(true);
      }
      for (Orcamento o : this.listaOrcamentos) {
        if (o.getAutorizado().booleanValue()) {
          this.btnManutReal.setEnabled(true);
        }
      }
      if (this.ordemServico.getContatoCliente() != null) {
        this.cboxContatoCliente.setSelectedItem(this.ordemServico.getContatoCliente());
      }
      if (this.ordemServico.getEnderecoCliente() != null) {
        this.cboxEnderCliente.setSelectedItem(this.ordemServico.getEnderecoCliente());
      }
      if (this.ordemServico.getLocalizacao() != null) {
        this.cboxLocal.setSelectedItem(this.ordemServico.getLocalizacao());
      }
      if (this.ordemServico.getMesesGarantia() != null) {
        this.cboxGarantia.setSelectedItem(String.valueOf(this.ordemServico.getMesesGarantia()));
      }
      if (this.ordemServico.getTransportadora() != null) {
        this.cboxTransp.setSelectedItem(this.ordemServico.getTransportadora());
      }
      if (this.ordemServico.getStatusOS() != null) {
        this.cboxStatus.setSelectedItem(this.ordemServico.getStatusOS());
        if (this.ordemServico.getStatusOS().equals("AGUARDANDO PGTO")) {
          this.btnFinalPgto.setEnabled(true);
        }
      }
      if (this.ordemServico.getUltimoOrcamento() != null) {
        if (this.ordemServico.getUltimoOrcamento().getTipoFrete() != null) {
          this.cboxTipoFrete.setSelectedItem(this.ordemServico.getUltimoOrcamento().getTipoFrete());
        }
        if (this.ordemServico.getUltimoOrcamento().getDataAutorizacao() != null) {
          this.edtDataAuth.setText(Format.DateToStr(this.ordemServico.getUltimoOrcamento().getDataAutorizacao()));
        }
        if (this.ordemServico.getUltimoOrcamento().getDataPrevisao() != null) {
          this.edtDataPrev.setText(Format.DateToStr(this.ordemServico.getUltimoOrcamento().getDataPrevisao()));
        }
        this.chkAplicarDesconto.setSelected(this.ordemServico.getUltimoOrcamento().getUsoDescontoCliente().booleanValue());
        this.edtDesconto.setText(Format.FormataValor(Format.doubleToString(this.ordemServico.getUltimoOrcamento().getValorDesconto()), Variaveis.getDecimal()));
        this.edtValorFrete.setText(Format.FormataValor(Format.doubleToString(this.ordemServico.getUltimoOrcamento().getValorFrete()), Variaveis.getDecimal()));
        this.edtVlrBruto.setText(Format.FormataValor(Format.doubleToString(this.ordemServico.getUltimoOrcamento().getValorBruto()), Variaveis.getDecimal()));
        this.edtVlrTotal.setText(Format.FormataValor(Format.doubleToString(this.ordemServico.getUltimoOrcamento().getValorLiquido()), Variaveis.getDecimal()));
      }
    } catch (Exception e) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(e)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  public void setEquipamento() {
    try {
      if (this.equipamento.getTipoEquipamento() != null) {
        this.cboxTipoEquip.setSelectedItem(this.equipamento.getTipoEquipamento());
      }
      this.edtDescricaoEquip.setText(this.equipamento.getDescricao());
      this.edtMarca.setText(this.equipamento.getMarca());
      this.edtModelo.setText(this.equipamento.getModelo());
      this.edtNroRastreio.setText(String.valueOf(this.equipamento.getNroRastreio() != null ? this.equipamento.getNroRastreio() : ""));
      this.edtNroSerie.setText(this.equipamento.getNroSerie());
      if (this.equipamento.getDataFabricacao() != null) {
        this.edtDataFbr.setText(Format.DateToStr(this.equipamento.getDataFabricacao()));
      }
      this.imagemConexao.clearImagem();
      if (this.equipamento.getConexao() != null) {
        this.imagemConexao.setCodImagem(this.equipamento.getConexao().getCodImagem());
        this.imagemConexao.setNomeImagem(this.equipamento.getConexao().getNomeImagem());
        this.imagemConexao.setMd5Imagem(this.equipamento.getConexao().getMd5Imagem());
        this.imagemConexao.setBlobImagem(this.equipamento.getConexao().getBlobImagem());
      }

      DefaultTableModel dm = (DefaultTableModel)this.tabelaHistEquipEntr.getModel();
      while (dm.getRowCount() > 0) {
        dm.removeRow(0);
      }
      for (OrdemServico o : EquipamentoDao.getHistEntrada(this.equipamento, Variaveis.getEmpresa())) {
        if (o.getCodOS() != this.ordemServico.getCodOS()) {
          dm.addRow(new Object[] { o.getCodOS(), o.getDataEntrada(), o.getDataSaida(), o.getSobGarantia(), o.getMesesGarantia() });
        }
      }
      setImagemConexao();
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  public void populaOS() {
    try {
      Integer cdOS = null;
      if (this.ordemServico.getCodOS() != null) {
        cdOS = this.ordemServico.getCodOS();
      }
      Integer cdEq = null;
      if (this.equipamento.getCodEquipamento() != null) {
        cdEq = this.equipamento.getCodEquipamento();
      }
      Usuario uT = (Usuario)this.ordemServico.getUsuario().clone();
      this.ordemServico.clearOrdemServico();
      this.ordemServico.setUsuario(uT);
      this.equipamento.clearEquipamento();
      this.equipamento.setCodEquipamento(cdEq);
      this.equipamento.setCliente(this.cliente);
      this.equipamento.setConexao(this.imagemConexao);
      if (!Validador.soNumeros(this.edtDataFbr.getText()).equals(""))
        this.equipamento.setDataFabricacao(Format.StrToDate(this.edtDataFbr.getText()));
      else {
        this.equipamento.setDataFabricacao(null);
      }
      this.equipamento.setDescricao(this.edtDescricaoEquip.getText());
      this.equipamento.setMarca(this.edtMarca.getText().trim());
      this.equipamento.setModelo(this.edtModelo.getText().trim());
      if (!Validador.soNumeros(this.edtNroRastreio.getText()).trim().equals("")) {
        this.equipamento.setNroRastreio(Integer.valueOf(Integer.parseInt(Validador.soNumeros(this.edtNroRastreio.getText().trim()))));
      }
      this.equipamento.setNroSerie(this.edtNroSerie.getText().trim());
      this.equipamento.setTipoEquipamento((TipoEquipamento)this.cboxTipoEquip.getSelectedItem());
      this.ordemServico.setCodOS(cdOS);
      this.ordemServico.setCliente(this.cliente);
      this.ordemServico.setLocalizacao(this.cboxLocal.getSelectedItem().toString().trim().equals("") ? new LocalProd() : (LocalProd)this.cboxLocal.getSelectedItem());
      this.ordemServico.setTransportadora(this.cboxTransp.getSelectedItem().toString().trim().equals("") ? new Transportadora() : (Transportadora)this.cboxTransp.getSelectedItem());
      this.ordemServico.setContatoCliente(this.cboxContatoCliente.getSelectedItem().toString().trim().equals("") ? new Contato() : (Contato)this.cboxContatoCliente.getSelectedItem());
      this.ordemServico.setEnderecoCliente((Endereco)this.cboxEnderCliente.getSelectedItem());
      this.ordemServico.setConsertoDetalhado(this.edtConserto.getText());
      this.ordemServico.setDefeitoReclamado(this.edtDefeito.getText());
      this.ordemServico.setEstadoEquipamento(this.edtEstadoEquip.getText());
      this.ordemServico.setEquipamento(this.equipamento);
      this.ordemServico.setMesesGarantia(Integer.valueOf(Integer.parseInt(this.cboxGarantia.getSelectedItem().toString())));
      this.ordemServico.setSobGarantia(Boolean.valueOf(this.chkGarantia.isSelected()));
      this.ordemServico.setStatusOS(this.cboxStatus.getSelectedItem().toString().trim());
      this.ordemServico.setParecer(this.edtParecer.getText());
      if (!Validador.soNumeros(this.edtNroOCcliente.getText()).trim().equals("")) {
        this.ordemServico.setNroOcCliente(Integer.valueOf(Integer.parseInt(Validador.soNumeros(this.edtNroOCcliente.getText().trim()))));
      }
      if (!Validador.soNumeros(this.edtDataEntrada.getText()).equals(""))
        this.ordemServico.setDataEntrada(Format.StrToDate(this.edtDataEntrada.getText()));
      else {
        this.ordemServico.setDataEntrada(null);
      }
      if (!Validador.soNumeros(this.edtDataCad.getText()).equals(""))
        this.ordemServico.setDataCadastro(Format.StrToDate(this.edtDataCad.getText()));
      else {
        this.ordemServico.setDataCadastro(null);
      }
      if (!Validador.soNumeros(this.edtDataSaida.getText()).equals(""))
        this.ordemServico.setDataSaida(Format.StrToDate(this.edtDataSaida.getText()));
      else {
        this.ordemServico.setDataSaida(null);
      }
      this.ordemServico.getEnvolvidos().clear();
      this.ordemServico.getItensProdutos().clear();
      this.ordemServico.getItensServicos().clear();
      this.ordemServico.getOrcamentos().clear();
      this.ordemServico.getTestesEquipamentos().clear();
      this.ordemServico.getItemsPropriedades().clear();
      this.ordemServico.getEnvolvidos().addAll(this.listaEnvolvidos);
      this.ordemServico.getItensProdutos().addAll(this.listaItensProd);
      this.ordemServico.getItensServicos().addAll(this.listaItensServico);
      this.ordemServico.getOrcamentos().addAll(this.listaOrcamentos);
      this.ordemServico.getTestesEquipamentos().addAll(this.listaEquipamentoTestes);
      this.ordemServico.getItemsPropriedades().addAll(this.listaItensPropriedades);
    } catch (Exception e) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(e)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void cancelar()
  {
    if (this.alterado) {
      if (Extra.Pergunta(this, "Descartar alterações não salvas?")) {
        limpaCampos();
        ativaEdits(Boolean.valueOf(false));
        ativaButtons(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false));
        this.alterado = false;
        this.ordemServico.clearOrdemServico();
        this.pnlTabulado.setSelectedComponent(this.pnlPrincipalOS);
        this.pnlAbasEquipamento.setSelectedComponent(this.pnlDEquipamento);
        this.pnlTabulado.setEnabledAt(1, false);
        this.pnlTabulado.setEnabledAt(2, false);
        this.pnlTabulado.setEnabledAt(3, false);
        this.pnlTabulado.setEnabledAt(4, false);
      }
    } else {
      limpaCampos();
      ativaEdits(Boolean.valueOf(false));
      ativaButtons(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false));
      this.alterado = false;
      this.ordemServico.clearOrdemServico();
      this.pnlTabulado.setSelectedComponent(this.pnlPrincipalOS);
      this.pnlAbasEquipamento.setSelectedComponent(this.pnlDEquipamento);
      this.pnlTabulado.setEnabledAt(1, false);
      this.pnlTabulado.setEnabledAt(2, false);
      this.pnlTabulado.setEnabledAt(3, false);
      this.pnlTabulado.setEnabledAt(4, false);
    }
  }

  private void ativaEdits(Boolean ativa) {
    this.edtCodCliente.setEnabled(ativa.booleanValue());
    this.btnBuscaCliente.setEnabled(ativa.booleanValue());
    this.cboxContatoCliente.setEnabled(ativa.booleanValue());
    this.cboxEnderCliente.setEnabled(ativa.booleanValue());
    this.pnlAbasEquipamento.setEnabled(ativa.booleanValue());
    this.edtDataPrev.setEnabled(ativa.booleanValue());
    this.cboxTipoFrete.setEnabled(ativa.booleanValue());
    this.chkAplicarDesconto.setEnabled(ativa.booleanValue());
    this.chkGarantia.setEnabled(ativa.booleanValue());
    this.btnGerarOrc.setEnabled(ativa.booleanValue());
    this.edtNroSerie.setEnabled(ativa.booleanValue());
    this.edtValorFrete.setEnabled(ativa.booleanValue());
    ativaEditsEquip(ativa);
    if (ativa.booleanValue())
      this.edtValorFrete.setEnabled(this.cboxTipoFrete.getSelectedItem().toString().toUpperCase().trim().equals("FOB"));
  }

  private void ativaEditsEquip(Boolean ativa)
  {
    this.edtMarca.setEnabled(ativa.booleanValue());
    this.edtModelo.setEnabled(ativa.booleanValue());
    this.cboxTipoEquip.setEnabled(ativa.booleanValue());
    this.edtNroRastreio.setEnabled(ativa.booleanValue());
    this.edtDataFbr.setEnabled(ativa.booleanValue());
    this.btnNovoTipoEquip.setEnabled(ativa.booleanValue());
    this.edtDescricaoEquip.setEnabled(ativa.booleanValue());
    this.tabelaPropriedades.setEnabled(ativa.booleanValue());
  }

  private void ativaButtons(Boolean buscar, Boolean novo, Boolean alterar, Boolean excluir, Boolean cancelar, Boolean salvar)
  {
    this.btnBuscarOS.setEnabled(buscar.booleanValue());
    this.btnCancOS.setEnabled(cancelar.booleanValue());
    this.btnExcluirOS.setEnabled(excluir.booleanValue());
    this.btnEditOS.setEnabled(alterar.booleanValue());
    this.btnNovaOS.setEnabled(novo.booleanValue());
    this.btnSaveOS.setEnabled(salvar.booleanValue());
  }

  private void limpaCampos() {
    this.lblCodigo.setText("0");
    this.edtCodCliente.setText("");
    this.edtCpfCliente.setText("");
    this.edtConserto.setText("");
    this.edtDataAuth.setText("");
    this.edtDataCad.setText("");
    this.edtDataEntrada.setText("");
    this.edtDataFbr.setText("");
    this.edtDataPrev.setText("");
    this.edtDataSaida.setText("");
    this.edtDefeito.setText("");
    this.edtDesconto.setText("0,00");
    this.edtDescrTeste.setText("");
    this.edtDescricaoEquip.setText("");
    this.edtEstadoEquip.setText("");
    this.edtMarca.setText("");
    this.edtMargemCliente.setText("0,00");
    this.edtModelo.setText("");
    this.edtNomeCliente.setText("");
    this.edtNroOCcliente.setText("");
    this.edtNroRastreio.setText("");
    this.edtNroSerie.setText("");
    this.edtParecer.setText("");
    this.edtResultTest.setText("");
    this.edtTelefoneCliente.setText("");
    this.edtValorFrete.setText("0,00");
    this.edtVlrBruto.setText("0,00");
    this.edtVlrTotal.setText("0,00");
    this.chkAplicarDesconto.setSelected(false);
    this.chkGarantia.setSelected(false);
    this.btnImprOrc.setEnabled(false);
    this.btnAuthOrc.setEnabled(false);
    this.btnManutReal.setEnabled(false);
    this.btnFinalPgto.setEnabled(false);
    this.cboxContatoCliente.removeAllItems();
    this.cboxEnderCliente.removeAllItems();
    this.tipoEquipAnterior = null;
    if (this.cboxLocal.getItemCount() > 0) {
      this.cboxLocal.setSelectedIndex(0);
    }
    if (this.cboxGarantia.getItemCount() > 0) {
      this.cboxGarantia.setSelectedIndex(0);
    }
    if (this.cboxStatus.getItemCount() > 0) {
      this.cboxStatus.setSelectedIndex(0);
    }
    if (this.cboxTipoEquip.getItemCount() > 0) {
      this.cboxTipoEquip.setSelectedIndex(0);
    }
    if (this.cboxTransp.getItemCount() > 0) {
      this.cboxTransp.setSelectedIndex(0);
    }
    if (this.cboxTipoFrete.getItemCount() > 0) {
      this.cboxTipoFrete.setSelectedIndex(0);
    }
    this.listaEnvolvidos.clear();
    this.listaEquipamentoTestes.clear();
    this.listaItensProd.clear();
    this.listaItensServico.clear();
    this.listaOrcamentos.clear();
    this.listaItensPropriedades.clear();
    limpaImagemConexao();
    limpaProd();
    limpaServico();
  }

  private void initComponents()
  {
    this.bindingGroup = new BindingGroup();

    this.buttonGroup1 = new ButtonGroup();
    this.listaItensProd = ObservableCollections.observableList(new ArrayList());
    this.listaItensServico = ObservableCollections.observableList(new ArrayList());
    this.listaEquipamentoTestes = ObservableCollections.observableList(new ArrayList());
    this.listaEnvolvidos = ObservableCollections.observableList(new ArrayList());
    this.localProd = new LocalProd();
    this.tipoEquipamento = new TipoEquipamento();
    this.ordemServico = new OrdemServico();
    this.imagemConexao = new Imagem();
    this.menuItensProd = new JPopupMenu();
    this.mItemProdRemover = new JMenuItem();
    this.mItemProdAlterar = new JMenuItem();
    this.menuItensServ = new JPopupMenu();
    this.mItemServRemover = new JMenuItem();
    this.mItemServAlterar = new JMenuItem();
    this.menuAnalize = new JPopupMenu();
    this.mAnalizeRemover = new JMenuItem();
    this.menuEnvolvidos = new JPopupMenu();
    this.mEnvolvidosRemover = new JMenuItem();
    this.cliente = new Cliente();
    this.produto = new Produto();
    this.servico = new Servico();
    this.listaOrcamentos = ObservableCollections.observableList(new ArrayList());
    this.equipamento = new Equipamento();
    this.jToggleButton1 = new JToggleButton();
    this.imagemProd = new Imagem();
    this.transportadora = new Transportadora();
    this.listaItensPropriedades = ObservableCollections.observableList(new ArrayList());
    this.jScrollPane14 = new JScrollPane();
    this.pnlTabulado = new JTabbedPane();
    this.pnlPrincipalOS = new JPanel();
    this.pnlInfoBasicas = new JPanel();
    this.lblDataCad = new JLabel();
    this.edtDataCad = new JFormattedTextField();
    this.pnlCodPessoa = new JPanel();
    this.lblCodigo = new JLabel();
    this.pnlDCliente = new JPanel();
    this.jLabel17 = new JLabel();
    this.edtCodCliente = new JTextField();
    this.btnBuscaCliente = new JButton();
    this.jLabel18 = new JLabel();
    this.edtNomeCliente = new JTextField();
    this.jLabel19 = new JLabel();
    this.edtCpfCliente = new JTextField();
    this.jLabel20 = new JLabel();
    this.edtTelefoneCliente = new JTextField();
    this.jLabel21 = new JLabel();
    this.cboxEnderCliente = new JComboBox();
    this.cboxContatoCliente = new JComboBox();
    this.jLabel22 = new JLabel();
    this.pnlAbasEquipamento = new JTabbedPane();
    this.pnlDEquipamento = new JPanel();
    this.jLabel7 = new JLabel();
    this.edtNroSerie = new JTextField();
    this.jLabel8 = new JLabel();
    this.cboxTipoEquip = new JComboBox();
    this.btnNovoTipoEquip = new JButton();
    this.jLabel9 = new JLabel();
    this.edtModelo = new JTextField();
    this.edtMarca = new JTextField();
    this.jLabel10 = new JLabel();
    this.jLabel11 = new JLabel();
    this.edtNroRastreio = new JTextField();
    this.jLabel12 = new JLabel();
    this.edtDataFbr = new JFormattedTextField();
    this.jLabel23 = new JLabel();
    this.jScrollPane1 = new JScrollPane();
    this.edtDescricaoEquip = new JTextArea();
    this.jLabel13 = new JLabel();
    this.jScrollPane15 = new JScrollPane();
    this.tabelaPropriedades = new JTable();
    this.pnlDetalhes = new JPanel();
    this.iconConexao = new JLabel();
    this.btnLimparImagem = new JButton();
    this.btnCarregaImagemBD = new JButton();
    this.btnCarregaImagemPasta = new JButton();
    this.jSeparator1 = new JSeparator();
    this.cboxLocal = new JComboBox();
    this.jLabel1 = new JLabel();
    this.btnNovaLocalizacao = new JButton();
    this.jLabel24 = new JLabel();
    this.jScrollPane5 = new JScrollPane();
    this.edtDefeito = new JTextArea();
    this.jLabel25 = new JLabel();
    this.jScrollPane6 = new JScrollPane();
    this.edtEstadoEquip = new JTextArea();
    this.pnlComplementar = new JPanel();
    this.edtNroOCcliente = new JTextField();
    this.jLabel34 = new JLabel();
    this.edtDataEntrada = new JFormattedTextField();
    this.lblDataNasc2 = new JLabel();
    this.lblDataNasc3 = new JLabel();
    this.edtDataSaida = new JFormattedTextField();
    this.jLabel45 = new JLabel();
    this.cboxGarantia = new JComboBox();
    this.jLabel46 = new JLabel();
    this.pnlHistEquipEntr = new JPanel();
    this.jScrollPane13 = new JScrollPane();
    this.tabelaHistEquipEntr = new JTable();
    this.cboxStatus = new JComboBox();
    this.jLabel26 = new JLabel();
    this.lblDataAuth = new JLabel();
    this.edtDataAuth = new JFormattedTextField();
    this.pnlBotoes = new JPanel();
    this.btnSair = new JButton();
    this.btnBuscarOS = new JButton();
    this.btnNovaOS = new JButton();
    this.btnEditOS = new JButton();
    this.btnExcluirOS = new JButton();
    this.btnCancOS = new JButton();
    this.btnSaveOS = new JButton();
    this.pnlItensProd = new JPanel();
    this.pnlAjustItensProd = new JPanel();
    this.jScrollPane2 = new JScrollPane();
    this.tabelaItensProd = new JTable();
    this.jLabel2 = new JLabel();
    this.btnAddProd = new JButton();
    this.lblPrecoProd = new JLabel();
    this.iconProd = new JLabel();
    this.jLabel27 = new JLabel();
    this.edtDescrProduto = new JTextField();
    this.jLabel28 = new JLabel();
    this.btnAddItemProd = new JButton();
    this.edtQtdeProduto = new JTextField();
    this.jLabel29 = new JLabel();
    this.lblSubTotalProd = new JLabel();
    this.jLabel44 = new JLabel();
    this.edtPrecoProd = new JTextField();
    this.chkProdOriginal = new JCheckBox();
    this.btnAtualizarValoresProd = new JButton();
    this.btnCancelarItemProd = new JButton();
    this.pnlItensServ = new JPanel();
    this.jPanel1 = new JPanel();
    this.chkMostraQtdHoras = new JCheckBox();
    this.btnAtualizarValoresServ = new JButton();
    this.jLabel30 = new JLabel();
    this.edtValorUnitServ = new JTextField();
    this.jLabel3 = new JLabel();
    this.jLabel5 = new JLabel();
    this.edtHoras = new JTextField();
    this.edtValorBrutoServ = new JTextField();
    this.jScrollPane4 = new JScrollPane();
    this.edtObservServ = new JTextArea();
    this.btnAddItemServ = new JButton();
    this.lblSubtotalServ = new JLabel();
    this.jLabel43 = new JLabel();
    this.jLabel40 = new JLabel();
    this.btnCancelarItemServ = new JButton();
    this.btnAddServ = new JButton();
    this.jLabel6 = new JLabel();
    this.jScrollPane3 = new JScrollPane();
    this.tabelaItensServico = new JTable();
    this.edtDescServ = new JTextField();
    this.pnlManutencao = new JPanel();
    this.pnlEnvolvidos = new JPanel();
    this.jScrollPane9 = new JScrollPane();
    this.tabelaEnvolvidos = new JTable();
    this.btnAddEnvolvidos = new JButton();
    this.pnlTabManutencao = new JTabbedPane();
    this.pnlAnalize = new JPanel();
    this.jLabel41 = new JLabel();
    this.edtDescrTeste = new JTextField();
    this.jLabel42 = new JLabel();
    this.edtResultTest = new JTextField();
    this.btnAddTeste = new JButton();
    this.jScrollPane12 = new JScrollPane();
    this.tabelaTestes = new JTable();
    this.pnlConserto = new JPanel();
    this.jLabel35 = new JLabel();
    this.jScrollPane7 = new JScrollPane();
    this.edtConserto = new JTextArea();
    this.jScrollPane8 = new JScrollPane();
    this.edtParecer = new JTextArea();
    this.jLabel36 = new JLabel();
    this.jSeparator2 = new JSeparator();
    this.pnlFinalizacao = new JPanel();
    this.pnlOCPendentes = new JPanel();
    this.jScrollPane11 = new JScrollPane();
    this.tabelaOCPendentes = new JTable();
    this.btnVerificar = new JButton();
    this.pnlTabuladoFinalizacao = new JTabbedPane();
    this.pnlInformacoes = new JPanel();
    this.jLabel4 = new JLabel();
    this.edtVlrBruto = new JTextField();
    this.jLabel31 = new JLabel();
    this.edtMargemCliente = new JTextField();
    this.jLabel32 = new JLabel();
    this.edtDesconto = new JTextField();
    this.chkAplicarDesconto = new JCheckBox();
    this.jLabel33 = new JLabel();
    this.edtVlrTotal = new JTextField();
    this.btnGerarOrc = new JButton();
    this.jLabel37 = new JLabel();
    this.cboxTipoFrete = new JComboBox();
    this.jLabel38 = new JLabel();
    this.edtValorFrete = new JTextField();
    this.jLabel39 = new JLabel();
    this.edtDataPrev = new JFormattedTextField();
    this.chkGarantia = new JCheckBox();
    this.btnRecalcularTotais = new JButton();
    this.btnImprConserto = new JButton();
    this.cboxTransp = new JComboBox();
    this.jLabel47 = new JLabel();
    this.btnNovaTrasp = new JButton();
    this.btnImprFichaCli = new JButton();
    this.pnlOrcamento = new JPanel();
    this.btnImprOrc = new JButton();
    this.btnAuthOrc = new JButton();
    this.btnFinalPgto = new JButton();
    this.jScrollPane10 = new JScrollPane();
    this.tabelaOrcamentos = new JTable();
    this.btnManutReal = new JButton();

    this.mItemProdRemover.setText("Remover Selecionado");
    this.mItemProdRemover.setToolTipText("");
    this.mItemProdRemover.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.mItemProdRemoverActionPerformed(evt);
      }
    });
    this.menuItensProd.add(this.mItemProdRemover);

    this.mItemProdAlterar.setText("Alterar");
    this.mItemProdAlterar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.mItemProdAlterarActionPerformed(evt);
      }
    });
    this.menuItensProd.add(this.mItemProdAlterar);

    this.mItemServRemover.setText("Remover Selecionado");
    this.mItemServRemover.setToolTipText("");
    this.mItemServRemover.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.mItemServRemoverActionPerformed(evt);
      }
    });
    this.menuItensServ.add(this.mItemServRemover);

    this.mItemServAlterar.setText("Alterar");
    this.mItemServAlterar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.mItemServAlterarActionPerformed(evt);
      }
    });
    this.menuItensServ.add(this.mItemServAlterar);

    this.mAnalizeRemover.setText("Remover Selecionado(s)");
    this.mAnalizeRemover.setToolTipText("");
    this.mAnalizeRemover.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.mAnalizeRemoverActionPerformed(evt);
      }
    });
    this.menuAnalize.add(this.mAnalizeRemover);

    this.mEnvolvidosRemover.setText("Remover Selecionado(s)");
    this.mEnvolvidosRemover.setToolTipText("");
    this.mEnvolvidosRemover.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.mEnvolvidosRemoverActionPerformed(evt);
      }
    });
    this.menuEnvolvidos.add(this.mEnvolvidosRemover);

    this.jToggleButton1.setText("jToggleButton1");

    setDefaultCloseOperation(0);
    addInternalFrameListener(new InternalFrameListener() {
      public void internalFrameOpened(InternalFrameEvent evt) {
      }
      public void internalFrameClosing(InternalFrameEvent evt) {
        FI_CadOS.this.formInternalFrameClosing(evt);
      }

      public void internalFrameClosed(InternalFrameEvent evt)
      {
      }

      public void internalFrameIconified(InternalFrameEvent evt)
      {
      }

      public void internalFrameDeiconified(InternalFrameEvent evt)
      {
      }

      public void internalFrameActivated(InternalFrameEvent evt)
      {
      }

      public void internalFrameDeactivated(InternalFrameEvent evt)
      {
      }
    });
    this.pnlTabulado.setAutoscrolls(true);

    this.pnlInfoBasicas.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));

    this.lblDataCad.setFont(new Font("Ubuntu", 1, 15));
    this.lblDataCad.setText("Data Cadastro:");
    try
    {
      this.edtDataCad.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####")));
    } catch (ParseException ex) {
      ex.printStackTrace();
    }
    this.edtDataCad.setEnabled(false);

    this.pnlCodPessoa.setBorder(BorderFactory.createTitledBorder("Nro. Doc"));
    this.pnlCodPessoa.setForeground(Color.black);

    this.lblCodigo.setFont(new Font("Tahoma", 1, 14));
    this.lblCodigo.setForeground(Color.blue);
    this.lblCodigo.setText("0");

    GroupLayout pnlCodPessoaLayout = new GroupLayout(this.pnlCodPessoa);
    this.pnlCodPessoa.setLayout(pnlCodPessoaLayout);
    pnlCodPessoaLayout.setHorizontalGroup(pnlCodPessoaLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlCodPessoaLayout.createSequentialGroup().addContainerGap().addComponent(this.lblCodigo).addContainerGap(69, 32767)));

    pnlCodPessoaLayout.setVerticalGroup(pnlCodPessoaLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlCodPessoaLayout.createSequentialGroup().addComponent(this.lblCodigo).addContainerGap(-1, 32767)));

    this.pnlDCliente.setBorder(BorderFactory.createTitledBorder("Dados do Cliente"));

    this.jLabel17.setText("Código:");

    this.edtCodCliente.setFont(new Font("Ubuntu", 1, 15));
    this.edtCodCliente.setForeground(Color.blue);
    this.edtCodCliente.addKeyListener(new KeyAdapter() {
      public void keyPressed(KeyEvent evt) {
        FI_CadOS.this.edtCodClienteKeyPressed(evt);
      }
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtCodClienteKeyReleased(evt);
      }
    });
    this.btnBuscaCliente.setText("...");
    this.btnBuscaCliente.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnBuscaClienteActionPerformed(evt);
      }
    });
    this.jLabel18.setText("Nome:");

    this.edtNomeCliente.setEnabled(false);

    this.jLabel19.setText("CPF/CNPJ:");

    this.edtCpfCliente.setEnabled(false);

    this.jLabel20.setText("Telefone:");

    this.edtTelefoneCliente.setEnabled(false);

    this.jLabel21.setText("Endereço:");

    this.jLabel22.setText("Contato:");

    GroupLayout pnlDClienteLayout = new GroupLayout(this.pnlDCliente);
    this.pnlDCliente.setLayout(pnlDClienteLayout);
    pnlDClienteLayout.setHorizontalGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDClienteLayout.createSequentialGroup().addContainerGap().addGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel21).addComponent(this.jLabel17)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(pnlDClienteLayout.createSequentialGroup().addComponent(this.edtCodCliente, -2, 75, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnBuscaCliente, -2, 37, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel18).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtNomeCliente, -2, 255, -2)).addComponent(this.cboxEnderCliente, 0, -1, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel22, GroupLayout.Alignment.TRAILING).addComponent(this.jLabel19, GroupLayout.Alignment.TRAILING)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(pnlDClienteLayout.createSequentialGroup().addComponent(this.edtCpfCliente, -2, 154, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel20).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtTelefoneCliente)).addComponent(this.cboxContatoCliente, -2, 356, -2)).addGap(14, 14, 14)));

    pnlDClienteLayout.setVerticalGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDClienteLayout.createSequentialGroup().addContainerGap().addGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel17).addComponent(this.edtCodCliente, -2, -1, -2).addComponent(this.btnBuscaCliente).addComponent(this.jLabel18).addComponent(this.edtNomeCliente, -2, -1, -2).addComponent(this.jLabel19).addComponent(this.edtCpfCliente, -2, -1, -2).addComponent(this.jLabel20).addComponent(this.edtTelefoneCliente, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel21).addComponent(this.cboxEnderCliente, -2, -1, -2)).addGroup(pnlDClienteLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel22).addComponent(this.cboxContatoCliente, -2, -1, -2))).addContainerGap(-1, 32767)));

    this.pnlDEquipamento.setBorder(BorderFactory.createTitledBorder(""));

    this.jLabel7.setText("Nro Série:");

    this.edtNroSerie.addFocusListener(new FocusAdapter() {
      public void focusLost(FocusEvent evt) {
        FI_CadOS.this.edtNroSerieFocusLost(evt);
      }
    });
    this.edtNroSerie.addKeyListener(new KeyAdapter() {
      public void keyPressed(KeyEvent evt) {
        FI_CadOS.this.edtNroSerieKeyPressed(evt);
      }
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtNroSerieKeyReleased(evt);
      }
    });
    this.jLabel8.setText("Tipo de Equip:");

    this.cboxTipoEquip.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        FI_CadOS.this.cboxTipoEquipItemStateChanged(evt);
      }
    });
    this.cboxTipoEquip.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.cboxTipoEquipActionPerformed(evt);
      }
    });
    this.btnNovoTipoEquip.setText("...");
    this.btnNovoTipoEquip.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnNovoTipoEquipActionPerformed(evt);
      }
    });
    this.jLabel9.setText("Modelo:");

    this.jLabel10.setText("Marca:");

    this.jLabel11.setText("Nro Rastreio:");

    this.jLabel12.setText("Data Fabricação:");
    try
    {
      this.edtDataFbr.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####")));
    } catch (ParseException ex) {
      ex.printStackTrace();
    }

    this.jLabel23.setText("Descrição:");

    this.edtDescricaoEquip.setColumns(20);
    this.edtDescricaoEquip.setLineWrap(true);
    this.edtDescricaoEquip.setRows(5);
    this.edtDescricaoEquip.setWrapStyleWord(true);
    this.edtDescricaoEquip.setDragEnabled(true);
    this.jScrollPane1.setViewportView(this.edtDescricaoEquip);

    this.jLabel13.setText("Propriedades:");

    this.tabelaPropriedades.setComponentPopupMenu(this.menuAnalize);
    this.tabelaPropriedades.setSelectionMode(0);

    JTableBinding jTableBinding = SwingBindings.createJTableBinding(AutoBinding.UpdateStrategy.READ_WRITE, this.listaItensPropriedades, this.tabelaPropriedades);
    JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${propTipoEquip}"));
    columnBinding.setColumnName("Descrição");
    columnBinding.setColumnClass(PropriedadeTipoEquip.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valor}"));
    columnBinding.setColumnName("Valor");
    columnBinding.setColumnClass(String.class);
    this.bindingGroup.addBinding(jTableBinding);
    jTableBinding.bind();
    this.jScrollPane15.setViewportView(this.tabelaPropriedades);

    GroupLayout pnlDEquipamentoLayout = new GroupLayout(this.pnlDEquipamento);
    this.pnlDEquipamento.setLayout(pnlDEquipamentoLayout);
    pnlDEquipamentoLayout.setHorizontalGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addGap(41, 41, 41).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel13).addComponent(this.jLabel8).addComponent(this.jLabel7).addComponent(this.jLabel23)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addComponent(this.cboxTipoEquip, -2, 237, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.btnNovoTipoEquip, -2, 34, -2)).addComponent(this.edtNroSerie)).addGap(18, 18, 18).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel11).addComponent(this.jLabel9, GroupLayout.Alignment.TRAILING)).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addGap(173, 173, 173).addComponent(this.jLabel12)).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addGap(9, 9, 9).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addGap(189, 189, 189).addComponent(this.jLabel10)).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.edtNroRastreio, -2, 159, -2).addComponent(this.edtModelo, GroupLayout.Alignment.TRAILING, -2, 159, -2))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.edtMarca, GroupLayout.Alignment.TRAILING, -2, 174, -2).addComponent(this.edtDataFbr, GroupLayout.Alignment.TRAILING, -2, 127, -2))))).addComponent(this.jScrollPane1).addComponent(this.jScrollPane15)).addContainerGap(172, 32767)));

    pnlDEquipamentoLayout.setVerticalGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addContainerGap().addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel7).addComponent(this.edtNroSerie, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnNovoTipoEquip).addComponent(this.cboxTipoEquip, -2, -1, -2).addComponent(this.jLabel8).addComponent(this.jLabel11))).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.edtModelo, -2, -1, -2).addComponent(this.jLabel10).addComponent(this.edtMarca, -2, -1, -2).addComponent(this.jLabel9)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.edtNroRastreio, -2, -1, -2).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel12).addComponent(this.edtDataFbr, -2, -1, -2))))).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel23).addComponent(this.jScrollPane1, -2, -1, -2)).addGap(16, 16, 16).addGroup(pnlDEquipamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDEquipamentoLayout.createSequentialGroup().addComponent(this.jLabel13).addGap(0, 231, 32767)).addComponent(this.jScrollPane15, -2, 0, 32767)).addContainerGap()));

    this.pnlAbasEquipamento.addTab("Dados do Equipamento", this.pnlDEquipamento);

    this.iconConexao.setBackground(new Color(153, 204, 255));
    this.iconConexao.setForeground(new Color(0, 102, 102));
    this.iconConexao.setHorizontalAlignment(0);
    this.iconConexao.setText("Conexão Equipamento");
    this.iconConexao.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(0, 51, 51)));

    this.btnLimparImagem.setText("Limpar");
    this.btnLimparImagem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnLimparImagemActionPerformed(evt);
      }
    });
    this.btnCarregaImagemBD.setText("Imagens BD");
    this.btnCarregaImagemBD.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnCarregaImagemBDActionPerformed(evt);
      }
    });
    this.btnCarregaImagemPasta.setText("Imagens Pasta");
    this.btnCarregaImagemPasta.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnCarregaImagemPastaActionPerformed(evt);
      }
    });
    this.jSeparator1.setOrientation(1);

    this.jLabel1.setText("Localização:");

    this.btnNovaLocalizacao.setText("...");
    this.btnNovaLocalizacao.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnNovaLocalizacaoActionPerformed(evt);
      }
    });
    this.jLabel24.setText("Defeito Reclamado:");

    this.edtDefeito.setColumns(20);
    this.edtDefeito.setRows(5);
    this.jScrollPane5.setViewportView(this.edtDefeito);

    this.jLabel25.setText("Estado Equipamento:");

    this.edtEstadoEquip.setColumns(20);
    this.edtEstadoEquip.setRows(5);
    this.jScrollPane6.setViewportView(this.edtEstadoEquip);

    GroupLayout pnlDetalhesLayout = new GroupLayout(this.pnlDetalhes);
    this.pnlDetalhes.setLayout(pnlDetalhesLayout);
    pnlDetalhesLayout.setHorizontalGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDetalhesLayout.createSequentialGroup().addContainerGap().addGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(pnlDetalhesLayout.createSequentialGroup().addComponent(this.btnLimparImagem).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.btnCarregaImagemBD).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnCarregaImagemPasta)).addComponent(this.iconConexao, -2, 403, -2)).addGap(18, 18, 18).addComponent(this.jSeparator1, -2, 20, -2).addGap(83, 83, 83).addGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel24).addComponent(this.jLabel25).addComponent(this.jLabel1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane5, GroupLayout.Alignment.TRAILING).addGroup(pnlDetalhesLayout.createSequentialGroup().addComponent(this.cboxLocal, 0, 400, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnNovaLocalizacao, -2, 34, -2)).addComponent(this.jScrollPane6)).addContainerGap()));

    pnlDetalhesLayout.setVerticalGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, pnlDetalhesLayout.createSequentialGroup().addContainerGap().addGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlDetalhesLayout.createSequentialGroup().addGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cboxLocal, -2, -1, -2).addComponent(this.btnNovaLocalizacao).addComponent(this.jLabel1)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel24).addComponent(this.jScrollPane5, -1, 129, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel25).addComponent(this.jScrollPane6, -1, 145, 32767)).addGap(133, 133, 133)).addGroup(pnlDetalhesLayout.createSequentialGroup().addComponent(this.iconConexao, -2, 332, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlDetalhesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnCarregaImagemPasta).addComponent(this.btnLimparImagem).addComponent(this.btnCarregaImagemBD)).addContainerGap()))).addComponent(this.jSeparator1));

    this.pnlAbasEquipamento.addTab("Detalhes", this.pnlDetalhes);

    this.jLabel34.setText("Nro OC Cliente:");
    try
    {
      this.edtDataEntrada.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####")));
    } catch (ParseException ex) {
      ex.printStackTrace();
    }

    this.lblDataNasc2.setFont(new Font("Ubuntu", 0, 15));
    this.lblDataNasc2.setText("Data Entrada:");

    this.lblDataNasc3.setFont(new Font("Ubuntu", 0, 15));
    this.lblDataNasc3.setText("Data Saida:");
    try
    {
      this.edtDataSaida.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####")));
    } catch (ParseException ex) {
      ex.printStackTrace();
    }

    this.jLabel45.setText("Prazo de Garantia:");

    this.cboxGarantia.setModel(new DefaultComboBoxModel(new String[] { "0", "3", "6", "12" }));

    this.jLabel46.setText("meses");

    GroupLayout pnlComplementarLayout = new GroupLayout(this.pnlComplementar);
    this.pnlComplementar.setLayout(pnlComplementarLayout);
    pnlComplementarLayout.setHorizontalGroup(pnlComplementarLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlComplementarLayout.createSequentialGroup().addContainerGap().addGroup(pnlComplementarLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel34, GroupLayout.Alignment.TRAILING).addComponent(this.jLabel45, GroupLayout.Alignment.TRAILING).addComponent(this.lblDataNasc3, GroupLayout.Alignment.TRAILING).addComponent(this.lblDataNasc2, GroupLayout.Alignment.TRAILING)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlComplementarLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.edtNroOCcliente, -2, 118, -2).addComponent(this.edtDataEntrada, -2, 118, -2).addComponent(this.edtDataSaida, -2, 118, -2).addGroup(pnlComplementarLayout.createSequentialGroup().addComponent(this.cboxGarantia, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel46))).addContainerGap(877, 32767)));

    pnlComplementarLayout.setVerticalGroup(pnlComplementarLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlComplementarLayout.createSequentialGroup().addContainerGap().addGroup(pnlComplementarLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel34).addComponent(this.edtNroOCcliente, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlComplementarLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblDataNasc2).addComponent(this.edtDataEntrada, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlComplementarLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblDataNasc3).addComponent(this.edtDataSaida, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlComplementarLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cboxGarantia, -2, -1, -2).addComponent(this.jLabel45).addComponent(this.jLabel46)).addContainerGap(307, 32767)));

    this.pnlAbasEquipamento.addTab("Complementar", this.pnlComplementar);

    this.tabelaHistEquipEntr.setModel(new DefaultTableModel(new Object[0][], new String[] { "Nro Doc", "Data Entrada", "Data Saída", "Fez uso da Garantia", "Prazo da Garantia (Mês)" })
    {
      Class[] types = { Integer.class, Object.class, Object.class, Boolean.class, Integer.class };

      boolean[] canEdit = { false, false, false, false, false };

      public Class getColumnClass(int columnIndex)
      {
        return this.types[columnIndex];
      }

      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return this.canEdit[columnIndex];
      }
    });
    this.tabelaHistEquipEntr.setSelectionMode(0);
    this.jScrollPane13.setViewportView(this.tabelaHistEquipEntr);

    GroupLayout pnlHistEquipEntrLayout = new GroupLayout(this.pnlHistEquipEntr);
    this.pnlHistEquipEntr.setLayout(pnlHistEquipEntrLayout);
    pnlHistEquipEntrLayout.setHorizontalGroup(pnlHistEquipEntrLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlHistEquipEntrLayout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane13, -1, 1126, 32767).addGap(14, 14, 14)));

    pnlHistEquipEntrLayout.setVerticalGroup(pnlHistEquipEntrLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlHistEquipEntrLayout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane13, -1, 443, 32767).addContainerGap()));

    this.pnlAbasEquipamento.addTab("Hitórico de Entradas", this.pnlHistEquipEntr);

    this.cboxStatus.setModel(new DefaultComboBoxModel(new String[] { "ABERTO", "AGUARDANDO OC", "AGUARDANDO AUTH", "EM ANDAMENTO", "AGUARDANDO PGTO", "FINALIZADO" }));
    this.cboxStatus.setEnabled(false);

    this.jLabel26.setFont(new Font("Ubuntu", 1, 15));
    this.jLabel26.setText("Status OS:");

    this.lblDataAuth.setFont(new Font("Ubuntu", 1, 15));
    this.lblDataAuth.setText("Data Autorização:");
    try
    {
      this.edtDataAuth.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####")));
    } catch (ParseException ex) {
      ex.printStackTrace();
    }
    this.edtDataAuth.setEnabled(false);

    GroupLayout pnlInfoBasicasLayout = new GroupLayout(this.pnlInfoBasicas);
    this.pnlInfoBasicas.setLayout(pnlInfoBasicasLayout);
    pnlInfoBasicasLayout.setHorizontalGroup(pnlInfoBasicasLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, pnlInfoBasicasLayout.createSequentialGroup().addContainerGap().addGroup(pnlInfoBasicasLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.pnlAbasEquipamento, GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.LEADING, pnlInfoBasicasLayout.createSequentialGroup().addComponent(this.pnlCodPessoa, -2, -1, -2).addGap(18, 18, 18).addComponent(this.jLabel26).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cboxStatus, -2, 235, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.lblDataCad).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtDataCad, -2, 118, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblDataAuth).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtDataAuth, -2, 118, -2)).addComponent(this.pnlDCliente, GroupLayout.Alignment.LEADING, -1, -1, 32767)).addContainerGap()));

    pnlInfoBasicasLayout.setVerticalGroup(pnlInfoBasicasLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInfoBasicasLayout.createSequentialGroup().addContainerGap().addGroup(pnlInfoBasicasLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInfoBasicasLayout.createSequentialGroup().addComponent(this.pnlCodPessoa, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)).addGroup(GroupLayout.Alignment.TRAILING, pnlInfoBasicasLayout.createSequentialGroup().addGroup(pnlInfoBasicasLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInfoBasicasLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblDataAuth).addComponent(this.edtDataAuth, -2, -1, -2)).addGroup(pnlInfoBasicasLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblDataCad).addComponent(this.edtDataCad, -2, -1, -2)).addGroup(pnlInfoBasicasLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel26).addComponent(this.cboxStatus, -2, -1, -2))).addGap(16, 16, 16))).addComponent(this.pnlDCliente, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.pnlAbasEquipamento).addContainerGap()));

    this.btnSair.setText("Sair");
    this.btnSair.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnSairActionPerformed(evt);
      }
    });
    this.btnBuscarOS.setText("Buscar");
    this.btnBuscarOS.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnBuscarOSActionPerformed(evt);
      }
    });
    this.btnNovaOS.setText("Novo");
    this.btnNovaOS.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnNovaOSActionPerformed(evt);
      }
    });
    this.btnEditOS.setText("Alterar");
    this.btnEditOS.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnEditOSActionPerformed(evt);
      }
    });
    this.btnExcluirOS.setText("Excluir");
    this.btnExcluirOS.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnExcluirOSActionPerformed(evt);
      }
    });
    this.btnCancOS.setText("Cancelar");
    this.btnCancOS.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnCancOSActionPerformed(evt);
      }
    });
    this.btnSaveOS.setText("Confirmar");
    this.btnSaveOS.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnSaveOSActionPerformed(evt);
      }
    });
    GroupLayout pnlBotoesLayout = new GroupLayout(this.pnlBotoes);
    this.pnlBotoes.setLayout(pnlBotoesLayout);
    pnlBotoesLayout.setHorizontalGroup(pnlBotoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlBotoesLayout.createSequentialGroup().addContainerGap().addComponent(this.btnBuscarOS).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnNovaOS).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnEditOS).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnExcluirOS).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnCancOS).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnSaveOS).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.btnSair, -2, 61, -2).addContainerGap()));

    pnlBotoesLayout.linkSize(0, new Component[] { this.btnBuscarOS, this.btnCancOS, this.btnEditOS, this.btnExcluirOS, this.btnNovaOS, this.btnSair, this.btnSaveOS });

    pnlBotoesLayout.setVerticalGroup(pnlBotoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlBotoesLayout.createSequentialGroup().addGroup(pnlBotoesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnBuscarOS).addComponent(this.btnNovaOS).addComponent(this.btnEditOS).addComponent(this.btnExcluirOS).addComponent(this.btnCancOS).addComponent(this.btnSaveOS).addComponent(this.btnSair)).addContainerGap(-1, 32767)));

    GroupLayout pnlPrincipalOSLayout = new GroupLayout(this.pnlPrincipalOS);
    this.pnlPrincipalOS.setLayout(pnlPrincipalOSLayout);
    pnlPrincipalOSLayout.setHorizontalGroup(pnlPrincipalOSLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.pnlBotoes, -1, -1, 32767).addComponent(this.pnlInfoBasicas, -1, -1, 32767));

    pnlPrincipalOSLayout.setVerticalGroup(pnlPrincipalOSLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlPrincipalOSLayout.createSequentialGroup().addComponent(this.pnlInfoBasicas, -1, -1, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.pnlBotoes, -2, -1, -2)));

    this.pnlTabulado.addTab("Ordem de Serviço", this.pnlPrincipalOS);

    this.tabelaItensProd.setComponentPopupMenu(this.menuItensProd);
    this.tabelaItensProd.setSelectionMode(0);

    jTableBinding = SwingBindings.createJTableBinding(AutoBinding.UpdateStrategy.READ_WRITE, this.listaItensProd, this.tabelaItensProd);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${produto}"));
    columnBinding.setColumnName("Produto");
    columnBinding.setColumnClass(Produto.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${quantidade}"));
    columnBinding.setColumnName("Quantidade");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorUnitario}"));
    columnBinding.setColumnName("Valor Unitario");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorBruto}"));
    columnBinding.setColumnName("Valor Bruto");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${original}"));
    columnBinding.setColumnName("Original");
    columnBinding.setColumnClass(Boolean.class);
    columnBinding.setEditable(false);
    this.bindingGroup.addBinding(jTableBinding);
    jTableBinding.bind();
    this.tabelaItensProd.addPropertyChangeListener(new PropertyChangeListener() {
      public void propertyChange(PropertyChangeEvent evt) {
        FI_CadOS.this.tabelaItensProdPropertyChange(evt);
      }
    });
    this.jScrollPane2.setViewportView(this.tabelaItensProd);

    this.jLabel2.setText("Preço Unitario:");

    this.btnAddProd.setText("...");
    this.btnAddProd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAddProdActionPerformed(evt);
      }
    });
    this.lblPrecoProd.setFont(new Font("Dialog", 1, 12));
    this.lblPrecoProd.setForeground(Color.blue);
    this.lblPrecoProd.setText("0,00");

    this.iconProd.setBackground(new Color(153, 204, 255));
    this.iconProd.setForeground(new Color(0, 102, 102));
    this.iconProd.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(0, 51, 51)));

    this.jLabel27.setText("Produto:");

    this.edtDescrProduto.setEnabled(false);

    this.jLabel28.setText("Qtde:");

    this.btnAddItemProd.setText("Adicionar à Lista");
    this.btnAddItemProd.setEnabled(false);
    this.btnAddItemProd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAddItemProdActionPerformed(evt);
      }
    });
    this.edtQtdeProduto.setFont(new Font("Dialog", 1, 12));
    this.edtQtdeProduto.setForeground(new Color(0, 0, 255));
    this.edtQtdeProduto.setHorizontalAlignment(11);
    this.edtQtdeProduto.setText("0,00");
    this.edtQtdeProduto.setEnabled(false);
    this.edtQtdeProduto.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtQtdeProdutoKeyReleased(evt);
      }
    });
    this.jLabel29.setText("Subtotal:");

    this.lblSubTotalProd.setFont(new Font("Dialog", 1, 12));
    this.lblSubTotalProd.setForeground(Color.blue);
    this.lblSubTotalProd.setText("0,00");

    this.jLabel44.setText("Valor Unitário:");

    this.edtPrecoProd.setFont(new Font("Dialog", 1, 12));
    this.edtPrecoProd.setForeground(new Color(0, 0, 255));
    this.edtPrecoProd.setHorizontalAlignment(11);
    this.edtPrecoProd.setText("0,00");
    this.edtPrecoProd.setEnabled(false);
    this.edtPrecoProd.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtPrecoProdKeyReleased(evt);
      }
    });
    this.chkProdOriginal.setSelected(true);
    this.chkProdOriginal.setText("Produto Original");
    this.chkProdOriginal.setEnabled(false);

    this.btnAtualizarValoresProd.setText("Atualizar Valores");
    this.btnAtualizarValoresProd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAtualizarValoresProdActionPerformed(evt);
      }
    });
    this.btnCancelarItemProd.setText("Cancelar");
    this.btnCancelarItemProd.setEnabled(false);
    this.btnCancelarItemProd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnCancelarItemProdActionPerformed(evt);
      }
    });
    GroupLayout pnlAjustItensProdLayout = new GroupLayout(this.pnlAjustItensProd);
    this.pnlAjustItensProd.setLayout(pnlAjustItensProdLayout);
    pnlAjustItensProdLayout.setHorizontalGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlAjustItensProdLayout.createSequentialGroup().addContainerGap().addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel44).addComponent(this.jLabel27)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.edtDescrProduto).addGroup(GroupLayout.Alignment.TRAILING, pnlAjustItensProdLayout.createSequentialGroup().addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlAjustItensProdLayout.createSequentialGroup().addComponent(this.edtPrecoProd, -2, 132, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel28).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtQtdeProduto, -2, 125, -2)).addComponent(this.chkProdOriginal)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.btnCancelarItemProd, -1, -1, 32767).addComponent(this.btnAddItemProd, -1, -1, 32767)))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnAddProd, -2, 41, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.iconProd, -2, 294, -2).addGroup(pnlAjustItensProdLayout.createSequentialGroup().addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel2, GroupLayout.Alignment.TRAILING).addComponent(this.jLabel29, GroupLayout.Alignment.TRAILING)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lblSubTotalProd).addComponent(this.lblPrecoProd)))).addContainerGap()).addGroup(GroupLayout.Alignment.TRAILING, pnlAjustItensProdLayout.createSequentialGroup().addGap(12, 12, 12).addComponent(this.jScrollPane2, -1, 856, 32767).addGap(318, 318, 318)).addGroup(pnlAjustItensProdLayout.createSequentialGroup().addContainerGap().addComponent(this.btnAtualizarValoresProd, -2, 151, -2).addContainerGap(-1, 32767)));

    pnlAjustItensProdLayout.setVerticalGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlAjustItensProdLayout.createSequentialGroup().addContainerGap().addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlAjustItensProdLayout.createSequentialGroup().addComponent(this.iconProd, -2, 244, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.lblPrecoProd)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel29).addComponent(this.lblSubTotalProd))).addGroup(pnlAjustItensProdLayout.createSequentialGroup().addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel27).addComponent(this.edtDescrProduto, -2, -1, -2).addComponent(this.btnAddProd)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnAddItemProd).addComponent(this.jLabel28).addComponent(this.edtQtdeProduto, -2, -1, -2).addComponent(this.edtPrecoProd, -2, -1, -2).addComponent(this.jLabel44)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlAjustItensProdLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnCancelarItemProd).addComponent(this.chkProdOriginal)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane2, -1, 586, 32767))).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.btnAtualizarValoresProd).addContainerGap()));

    GroupLayout pnlItensProdLayout = new GroupLayout(this.pnlItensProd);
    this.pnlItensProd.setLayout(pnlItensProdLayout);
    pnlItensProdLayout.setHorizontalGroup(pnlItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.pnlAjustItensProd, -1, -1, 32767));

    pnlItensProdLayout.setVerticalGroup(pnlItensProdLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.pnlAjustItensProd, -1, -1, 32767));

    this.pnlTabulado.addTab("Itens de Produto", this.pnlItensProd);

    this.chkMostraQtdHoras.setSelected(true);
    this.chkMostraQtdHoras.setText("Mostrar Qtde Hr");
    this.chkMostraQtdHoras.setEnabled(false);

    this.btnAtualizarValoresServ.setText("Atualizar Valores");
    this.btnAtualizarValoresServ.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAtualizarValoresServActionPerformed(evt);
      }
    });
    this.jLabel30.setText("Subtotal:");

    this.edtValorUnitServ.setFont(new Font("Dialog", 1, 12));
    this.edtValorUnitServ.setForeground(Color.blue);
    this.edtValorUnitServ.setHorizontalAlignment(11);
    this.edtValorUnitServ.setText("0,00");
    this.edtValorUnitServ.setEnabled(false);
    this.edtValorUnitServ.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtValorUnitServKeyReleased(evt);
      }
    });
    this.jLabel3.setText("Descrição do Serviço:");

    this.jLabel5.setText("Valor Bruto:");

    this.edtHoras.setFont(new Font("Dialog", 1, 12));
    this.edtHoras.setForeground(Color.blue);
    this.edtHoras.setHorizontalAlignment(11);
    this.edtHoras.setText("0,00");
    this.edtHoras.setEnabled(false);
    this.edtHoras.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtHorasKeyReleased(evt);
      }
    });
    this.edtValorBrutoServ.setFont(new Font("Dialog", 1, 12));
    this.edtValorBrutoServ.setForeground(Color.blue);
    this.edtValorBrutoServ.setHorizontalAlignment(11);
    this.edtValorBrutoServ.setText("0,00");
    this.edtValorBrutoServ.setEnabled(false);
    this.edtValorBrutoServ.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtValorBrutoServKeyReleased(evt);
      }
    });
    this.edtObservServ.setColumns(20);
    this.edtObservServ.setRows(5);
    this.edtObservServ.setEnabled(false);
    this.jScrollPane4.setViewportView(this.edtObservServ);

    this.btnAddItemServ.setText("Adicionar à lista");
    this.btnAddItemServ.setEnabled(false);
    this.btnAddItemServ.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAddItemServActionPerformed(evt);
      }
    });
    this.lblSubtotalServ.setFont(new Font("Dialog", 1, 12));
    this.lblSubtotalServ.setForeground(Color.blue);
    this.lblSubtotalServ.setText("0,00");

    this.jLabel43.setText("Valor Unitário:");

    this.jLabel40.setText("Horas:");

    this.btnCancelarItemServ.setText("Cancelar");
    this.btnCancelarItemServ.setEnabled(false);
    this.btnCancelarItemServ.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnCancelarItemServActionPerformed(evt);
      }
    });
    this.btnAddServ.setText("...");
    this.btnAddServ.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAddServActionPerformed(evt);
      }
    });
    this.jLabel6.setText("Observações:");

    this.tabelaItensServico.setComponentPopupMenu(this.menuItensServ);
    this.tabelaItensServico.setSelectionMode(0);

    jTableBinding = SwingBindings.createJTableBinding(AutoBinding.UpdateStrategy.READ_WRITE, this.listaItensServico, this.tabelaItensServico);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${servico}"));
    columnBinding.setColumnName("Servico");
    columnBinding.setColumnClass(Servico.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${quantidade}"));
    columnBinding.setColumnName("Qtde Horas");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorUnitario}"));
    columnBinding.setColumnName("Valor Unitario");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorBruto}"));
    columnBinding.setColumnName("Valor Bruto");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${observacoes}"));
    columnBinding.setColumnName("Observacoes");
    columnBinding.setColumnClass(String.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${mostrarQtde}"));
    columnBinding.setColumnName("Mostrar Qtde");
    columnBinding.setColumnClass(Boolean.class);
    columnBinding.setEditable(false);
    this.bindingGroup.addBinding(jTableBinding);
    jTableBinding.bind();
    this.jScrollPane3.setViewportView(this.tabelaItensServico);

    this.edtDescServ.setEnabled(false);

    GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
    this.jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane3, GroupLayout.Alignment.TRAILING).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addGap(167, 167, 167).addComponent(this.jScrollPane4, -1, 854, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.btnAtualizarValoresServ, -1, -1, 32767).addComponent(this.btnAddItemServ, -1, -1, 32767).addComponent(this.btnCancelarItemServ, -2, 135, -2))).addGroup(jPanel1Layout.createSequentialGroup().addGap(6, 6, 6).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel3).addComponent(this.jLabel43).addComponent(this.jLabel6)).addGap(6, 6, 6).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.edtValorUnitServ, -2, 107, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel5).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtValorBrutoServ, -2, 107, -2)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.edtDescServ, -2, 499, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnAddServ, -2, 45, -2))).addGap(12, 12, 12).addComponent(this.jLabel40).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtHoras, -2, 101, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.chkMostraQtdHoras).addGap(69, 69, 69)).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 946, -2).addComponent(this.jLabel30).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblSubtotalServ).addGap(59, 59, 59))).addContainerGap()));

    jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.edtDescServ, -2, -1, -2).addComponent(this.btnAddServ).addComponent(this.jLabel40).addComponent(this.edtHoras, -2, -1, -2).addComponent(this.chkMostraQtdHoras)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.edtValorBrutoServ, -2, -1, -2).addComponent(this.jLabel5).addComponent(this.edtValorUnitServ, -2, -1, -2).addComponent(this.jLabel43)).addGap(16, 16, 16).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel6).addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.btnAddItemServ).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnCancelarItemServ).addGap(8, 8, 8).addComponent(this.btnAtualizarValoresServ)).addComponent(this.jScrollPane4, -2, 104, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane3, -1, 499, 32767))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel30).addComponent(this.lblSubtotalServ)).addGap(32, 32, 32)));

    GroupLayout pnlItensServLayout = new GroupLayout(this.pnlItensServ);
    this.pnlItensServ.setLayout(pnlItensServLayout);
    pnlItensServLayout.setHorizontalGroup(pnlItensServLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767));

    pnlItensServLayout.setVerticalGroup(pnlItensServLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767));

    this.pnlTabulado.addTab("Itens de Serviços", this.pnlItensServ);

    this.pnlEnvolvidos.setBorder(BorderFactory.createTitledBorder("Envolvidos"));

    this.tabelaEnvolvidos.setComponentPopupMenu(this.menuEnvolvidos);
    this.tabelaEnvolvidos.setSelectionMode(2);

    jTableBinding = SwingBindings.createJTableBinding(AutoBinding.UpdateStrategy.READ_WRITE, this.listaEnvolvidos, this.tabelaEnvolvidos);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${cdPessoa}"));
    columnBinding.setColumnName("Código");
    columnBinding.setColumnClass(Integer.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${nomePessoa}"));
    columnBinding.setColumnName("Nome");
    columnBinding.setColumnClass(String.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${profissaoPessoa}"));
    columnBinding.setColumnName("Cargo");
    columnBinding.setColumnClass(String.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorHoraFuncionario}"));
    columnBinding.setColumnName("Valor Hora");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    this.bindingGroup.addBinding(jTableBinding);
    jTableBinding.bind();
    this.jScrollPane9.setViewportView(this.tabelaEnvolvidos);

    this.btnAddEnvolvidos.setText("Adicionar Envolvido(s)");
    this.btnAddEnvolvidos.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAddEnvolvidosActionPerformed(evt);
      }
    });
    GroupLayout pnlEnvolvidosLayout = new GroupLayout(this.pnlEnvolvidos);
    this.pnlEnvolvidos.setLayout(pnlEnvolvidosLayout);
    pnlEnvolvidosLayout.setHorizontalGroup(pnlEnvolvidosLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlEnvolvidosLayout.createSequentialGroup().addContainerGap().addGroup(pnlEnvolvidosLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane9).addGroup(pnlEnvolvidosLayout.createSequentialGroup().addComponent(this.btnAddEnvolvidos).addGap(0, 0, 32767))).addContainerGap()));

    pnlEnvolvidosLayout.setVerticalGroup(pnlEnvolvidosLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlEnvolvidosLayout.createSequentialGroup().addGap(27, 27, 27).addComponent(this.jScrollPane9, -1, 245, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.btnAddEnvolvidos).addGap(21, 21, 21)));

    this.jLabel41.setText("Descrição do Teste:");

    this.edtDescrTeste.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtDescrTesteKeyReleased(evt);
      }
    });
    this.jLabel42.setText("Defeito/Resultado:");

    this.edtResultTest.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtResultTestKeyReleased(evt);
      }
    });
    this.btnAddTeste.setText("Incluir");
    this.btnAddTeste.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAddTesteActionPerformed(evt);
      }
    });
    this.tabelaTestes.setComponentPopupMenu(this.menuAnalize);
    this.tabelaTestes.setSelectionMode(2);

    jTableBinding = SwingBindings.createJTableBinding(AutoBinding.UpdateStrategy.READ_WRITE, this.listaEquipamentoTestes, this.tabelaTestes);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${descricao}"));
    columnBinding.setColumnName("Descricao");
    columnBinding.setColumnClass(String.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${defeito}"));
    columnBinding.setColumnName("Resultado");
    columnBinding.setColumnClass(String.class);
    columnBinding.setEditable(false);
    this.bindingGroup.addBinding(jTableBinding);
    jTableBinding.bind();
    this.jScrollPane12.setViewportView(this.tabelaTestes);

    GroupLayout pnlAnalizeLayout = new GroupLayout(this.pnlAnalize);
    this.pnlAnalize.setLayout(pnlAnalizeLayout);
    pnlAnalizeLayout.setHorizontalGroup(pnlAnalizeLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlAnalizeLayout.createSequentialGroup().addContainerGap().addGroup(pnlAnalizeLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlAnalizeLayout.createSequentialGroup().addComponent(this.jLabel41).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtDescrTeste, -2, 301, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel42).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtResultTest, -1, 401, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.btnAddTeste, -2, 100, -2)).addComponent(this.jScrollPane12, -1, 1130, 32767)).addContainerGap()));

    pnlAnalizeLayout.setVerticalGroup(pnlAnalizeLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlAnalizeLayout.createSequentialGroup().addContainerGap().addGroup(pnlAnalizeLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.edtDescrTeste, -2, -1, -2).addComponent(this.jLabel41).addComponent(this.jLabel42).addComponent(this.edtResultTest, -2, -1, -2).addComponent(this.btnAddTeste)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jScrollPane12, -1, 259, 32767).addContainerGap()));

    this.pnlTabManutencao.addTab("Análize/Testes", this.pnlAnalize);

    this.jLabel35.setText("Conserto Detalhado:");

    this.edtConserto.setColumns(20);
    this.edtConserto.setRows(5);
    this.jScrollPane7.setViewportView(this.edtConserto);

    this.edtParecer.setColumns(20);
    this.edtParecer.setRows(5);
    this.jScrollPane8.setViewportView(this.edtParecer);

    this.jLabel36.setText("Parecer:");

    this.jSeparator2.setOrientation(1);

    GroupLayout pnlConsertoLayout = new GroupLayout(this.pnlConserto);
    this.pnlConserto.setLayout(pnlConsertoLayout);
    pnlConsertoLayout.setHorizontalGroup(pnlConsertoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlConsertoLayout.createSequentialGroup().addContainerGap().addGroup(pnlConsertoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane7, -2, 478, -2).addComponent(this.jLabel35)).addGap(45, 45, 45).addComponent(this.jSeparator2, -2, -1, -2).addGap(37, 37, 37).addGroup(pnlConsertoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane8, -2, 478, -2).addComponent(this.jLabel36)).addContainerGap()));

    pnlConsertoLayout.setVerticalGroup(pnlConsertoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlConsertoLayout.createSequentialGroup().addContainerGap().addGroup(pnlConsertoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jSeparator2).addGroup(pnlConsertoLayout.createSequentialGroup().addComponent(this.jLabel36).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane8, -1, 277, 32767)).addGroup(pnlConsertoLayout.createSequentialGroup().addComponent(this.jLabel35).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane7))).addContainerGap()));

    this.pnlTabManutencao.addTab("Conserto", this.pnlConserto);

    GroupLayout pnlManutencaoLayout = new GroupLayout(this.pnlManutencao);
    this.pnlManutencao.setLayout(pnlManutencaoLayout);
    pnlManutencaoLayout.setHorizontalGroup(pnlManutencaoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, pnlManutencaoLayout.createSequentialGroup().addContainerGap().addGroup(pnlManutencaoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.pnlTabManutencao).addComponent(this.pnlEnvolvidos, -1, -1, 32767)).addContainerGap()));

    pnlManutencaoLayout.setVerticalGroup(pnlManutencaoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, pnlManutencaoLayout.createSequentialGroup().addContainerGap().addComponent(this.pnlTabManutencao, -2, 363, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.pnlEnvolvidos, -1, -1, 32767).addContainerGap()));

    this.pnlTabulado.addTab("Manutenção", this.pnlManutencao);

    this.pnlOCPendentes.setBorder(BorderFactory.createTitledBorder("Ordens de Compra Pendentes"));

    this.tabelaOCPendentes.setModel(new DefaultTableModel(new Object[][] { new Object[0], new Object[0], new Object[0], new Object[0] }, new String[0]));

    this.jScrollPane11.setViewportView(this.tabelaOCPendentes);

    this.btnVerificar.setText("Verificar OCs");

    GroupLayout pnlOCPendentesLayout = new GroupLayout(this.pnlOCPendentes);
    this.pnlOCPendentes.setLayout(pnlOCPendentesLayout);
    pnlOCPendentesLayout.setHorizontalGroup(pnlOCPendentesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlOCPendentesLayout.createSequentialGroup().addContainerGap().addGroup(pnlOCPendentesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane11).addGroup(pnlOCPendentesLayout.createSequentialGroup().addComponent(this.btnVerificar).addGap(0, 0, 32767))).addContainerGap()));

    pnlOCPendentesLayout.setVerticalGroup(pnlOCPendentesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlOCPendentesLayout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane11, -1, 243, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnVerificar).addContainerGap()));

    this.pnlInformacoes.setBorder(null);

    this.jLabel4.setText("Valor Bruto:");

    this.edtVlrBruto.setForeground(Color.blue);
    this.edtVlrBruto.setHorizontalAlignment(11);
    this.edtVlrBruto.setText("0,00");
    this.edtVlrBruto.setEnabled(false);
    this.edtVlrBruto.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtVlrBrutoKeyReleased(evt);
      }
    });
    this.jLabel31.setText("Margem do Cliente:");

    this.edtMargemCliente.setForeground(Color.blue);
    this.edtMargemCliente.setHorizontalAlignment(11);
    this.edtMargemCliente.setText("0,00");
    this.edtMargemCliente.setEnabled(false);
    this.edtMargemCliente.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtMargemClienteKeyReleased(evt);
      }
    });
    this.jLabel32.setText("Desconto:");

    this.edtDesconto.setForeground(Color.blue);
    this.edtDesconto.setHorizontalAlignment(11);
    this.edtDesconto.setText("0,00");
    this.edtDesconto.setEnabled(false);
    this.edtDesconto.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtDescontoKeyReleased(evt);
      }
    });
    this.chkAplicarDesconto.setText("Desconto do cliente");
    this.chkAplicarDesconto.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        FI_CadOS.this.chkAplicarDescontoItemStateChanged(evt);
      }
    });
    this.jLabel33.setText("Valor Total:");

    this.edtVlrTotal.setForeground(Color.blue);
    this.edtVlrTotal.setHorizontalAlignment(11);
    this.edtVlrTotal.setText("0,00");
    this.edtVlrTotal.setEnabled(false);
    this.edtVlrTotal.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtVlrTotalKeyReleased(evt);
      }
    });
    this.btnGerarOrc.setText("Gerar Orçamento");
    this.btnGerarOrc.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnGerarOrcActionPerformed(evt);
      }
    });
    this.jLabel37.setText("Tipo de Frete:");

    this.cboxTipoFrete.setModel(new DefaultComboBoxModel(new String[] { "FOB", "CIF" }));
    this.cboxTipoFrete.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        FI_CadOS.this.cboxTipoFreteItemStateChanged(evt);
      }
    });
    this.jLabel38.setText("Valor Frete:");

    this.edtValorFrete.setForeground(Color.blue);
    this.edtValorFrete.setHorizontalAlignment(11);
    this.edtValorFrete.setText("0,00");
    this.edtValorFrete.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent evt) {
        FI_CadOS.this.edtValorFreteKeyReleased(evt);
      }
    });
    this.jLabel39.setText("Previsão de Conclusão:");
    try
    {
      this.edtDataPrev.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####")));
    } catch (ParseException ex) {
      ex.printStackTrace();
    }

    this.chkGarantia.setText("Uso da Garantia");
    this.chkGarantia.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.chkGarantiaActionPerformed(evt);
      }
    });
    this.btnRecalcularTotais.setText("Recalcular Totais");
    this.btnRecalcularTotais.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnRecalcularTotaisActionPerformed(evt);
      }
    });
    this.btnImprConserto.setText("Ficha de Conserto");
    this.btnImprConserto.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnImprConsertoActionPerformed(evt);
      }
    });
    this.cboxTransp.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        FI_CadOS.this.cboxTranspItemStateChanged(evt);
      }
    });
    this.jLabel47.setText("Transportadora:");

    this.btnNovaTrasp.setText("...");
    this.btnNovaTrasp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnNovaTraspActionPerformed(evt);
      }
    });
    this.btnImprFichaCli.setText("Ficha para Cliente");
    this.btnImprFichaCli.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnImprFichaCliActionPerformed(evt);
      }
    });
    GroupLayout pnlInformacoesLayout = new GroupLayout(this.pnlInformacoes);
    this.pnlInformacoes.setLayout(pnlInformacoesLayout);
    pnlInformacoesLayout.setHorizontalGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInformacoesLayout.createSequentialGroup().addContainerGap().addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInformacoesLayout.createSequentialGroup().addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel31).addComponent(this.jLabel4).addComponent(this.jLabel37).addComponent(this.jLabel47)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInformacoesLayout.createSequentialGroup().addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(pnlInformacoesLayout.createSequentialGroup().addComponent(this.edtVlrBruto, -2, 104, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.jLabel33)).addGroup(pnlInformacoesLayout.createSequentialGroup().addComponent(this.edtMargemCliente, -2, 104, -2).addGap(32, 32, 32).addComponent(this.jLabel32)).addGroup(pnlInformacoesLayout.createSequentialGroup().addComponent(this.cboxTipoFrete, -2, 104, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.jLabel38))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInformacoesLayout.createSequentialGroup().addComponent(this.edtDesconto, -2, 104, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 347, 32767).addComponent(this.jLabel39).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.edtDataPrev, -2, 127, -2)).addGroup(pnlInformacoesLayout.createSequentialGroup().addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.edtVlrTotal, -2, 104, -2).addComponent(this.edtValorFrete, -2, 104, -2)).addGap(0, 652, 32767)))).addGroup(pnlInformacoesLayout.createSequentialGroup().addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInformacoesLayout.createSequentialGroup().addComponent(this.cboxTransp, -2, 278, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnNovaTrasp, -2, 42, -2)).addGroup(pnlInformacoesLayout.createSequentialGroup().addComponent(this.chkAplicarDesconto).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.chkGarantia))).addGap(0, 0, 32767)))).addGroup(pnlInformacoesLayout.createSequentialGroup().addComponent(this.btnRecalcularTotais).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnGerarOrc).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnImprConserto).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnImprFichaCli).addGap(0, 0, 32767))).addContainerGap()));

    pnlInformacoesLayout.linkSize(0, new Component[] { this.btnGerarOrc, this.btnImprConserto, this.btnRecalcularTotais });

    pnlInformacoesLayout.setVerticalGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlInformacoesLayout.createSequentialGroup().addContainerGap().addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel31).addComponent(this.edtMargemCliente, -2, -1, -2).addComponent(this.jLabel32).addComponent(this.edtDesconto, -2, -1, -2).addComponent(this.jLabel39).addComponent(this.edtDataPrev, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.edtVlrTotal, -2, -1, -2).addComponent(this.jLabel33).addComponent(this.edtVlrBruto, -2, -1, -2).addComponent(this.jLabel4)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cboxTipoFrete, -2, -1, -2).addComponent(this.jLabel37).addComponent(this.edtValorFrete, -2, -1, -2).addComponent(this.jLabel38)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnNovaTrasp).addComponent(this.cboxTransp, -2, -1, -2).addComponent(this.jLabel47)).addGap(18, 18, 18).addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.chkAplicarDesconto).addComponent(this.chkGarantia)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 117, 32767).addGroup(pnlInformacoesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnGerarOrc).addComponent(this.btnRecalcularTotais).addComponent(this.btnImprConserto).addComponent(this.btnImprFichaCli)).addContainerGap()));

    this.pnlTabuladoFinalizacao.addTab("Finalização", this.pnlInformacoes);

    this.pnlOrcamento.setBorder(null);

    this.btnImprOrc.setText("Imprimir último orçamento");
    this.btnImprOrc.setEnabled(false);
    this.btnImprOrc.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnImprOrcActionPerformed(evt);
      }
    });
    this.btnAuthOrc.setText("Autorizar último orçamento");
    this.btnAuthOrc.setEnabled(false);
    this.btnAuthOrc.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnAuthOrcActionPerformed(evt);
      }
    });
    this.btnFinalPgto.setText("Finalizar Pagamento");
    this.btnFinalPgto.setEnabled(false);
    this.btnFinalPgto.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnFinalPgtoActionPerformed(evt);
      }
    });
    jTableBinding = SwingBindings.createJTableBinding(AutoBinding.UpdateStrategy.READ_WRITE, this.listaOrcamentos, this.tabelaOrcamentos);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${nroRevisao}"));
    columnBinding.setColumnName("Nro Revisão");
    columnBinding.setColumnClass(Integer.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${dataCadastro}"));
    columnBinding.setColumnName("Cadastro");
    columnBinding.setColumnClass(Date.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorBruto}"));
    columnBinding.setColumnName("Valor Bruto");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorDesconto}"));
    columnBinding.setColumnName("Desconto");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorLiquido}"));
    columnBinding.setColumnName("Valor Final");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${valorFrete}"));
    columnBinding.setColumnName("Valor Frete");
    columnBinding.setColumnClass(Double.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${tipoFrete}"));
    columnBinding.setColumnName("Frete");
    columnBinding.setColumnClass(String.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${dataPrevisao}"));
    columnBinding.setColumnName("Previsão");
    columnBinding.setColumnClass(Date.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${diasValidade}"));
    columnBinding.setColumnName("Dias Validade");
    columnBinding.setColumnClass(Integer.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${dataAutorizacao}"));
    columnBinding.setColumnName("Data Autorização");
    columnBinding.setColumnClass(Date.class);
    columnBinding.setEditable(false);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${autorizado}"));
    columnBinding.setColumnName("Autorizado");
    columnBinding.setColumnClass(Boolean.class);
    columnBinding.setEditable(false);
    this.bindingGroup.addBinding(jTableBinding);
    jTableBinding.bind();
    this.jScrollPane10.setViewportView(this.tabelaOrcamentos);

    this.btnManutReal.setText("Manutenção Realizada");
    this.btnManutReal.setEnabled(false);
    this.btnManutReal.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_CadOS.this.btnManutRealActionPerformed(evt);
      }
    });
    GroupLayout pnlOrcamentoLayout = new GroupLayout(this.pnlOrcamento);
    this.pnlOrcamento.setLayout(pnlOrcamentoLayout);
    pnlOrcamentoLayout.setHorizontalGroup(pnlOrcamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlOrcamentoLayout.createSequentialGroup().addContainerGap().addGroup(pnlOrcamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlOrcamentoLayout.createSequentialGroup().addComponent(this.btnImprOrc).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.btnAuthOrc).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnManutReal).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 325, 32767).addComponent(this.btnFinalPgto)).addComponent(this.jScrollPane10)).addContainerGap()));

    pnlOrcamentoLayout.linkSize(0, new Component[] { this.btnAuthOrc, this.btnImprOrc, this.btnManutReal });

    pnlOrcamentoLayout.setVerticalGroup(pnlOrcamentoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, pnlOrcamentoLayout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane10, -1, 309, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlOrcamentoLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnImprOrc).addComponent(this.btnAuthOrc).addComponent(this.btnFinalPgto).addComponent(this.btnManutReal)).addGap(6, 6, 6)));

    this.pnlTabuladoFinalizacao.addTab("Orçamentos", this.pnlOrcamento);

    GroupLayout pnlFinalizacaoLayout = new GroupLayout(this.pnlFinalizacao);
    this.pnlFinalizacao.setLayout(pnlFinalizacaoLayout);
    pnlFinalizacaoLayout.setHorizontalGroup(pnlFinalizacaoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, pnlFinalizacaoLayout.createSequentialGroup().addContainerGap().addGroup(pnlFinalizacaoLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.pnlTabuladoFinalizacao).addComponent(this.pnlOCPendentes, GroupLayout.Alignment.LEADING, -1, -1, 32767)).addContainerGap()));

    pnlFinalizacaoLayout.setVerticalGroup(pnlFinalizacaoLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlFinalizacaoLayout.createSequentialGroup().addContainerGap().addComponent(this.pnlTabuladoFinalizacao, -2, 401, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.pnlOCPendentes, -1, -1, 32767).addContainerGap()));

    this.pnlTabulado.addTab("Finalização", this.pnlFinalizacao);

    this.jScrollPane14.setViewportView(this.pnlTabulado);
    this.pnlTabulado.getAccessibleContext().setAccessibleName("Ordem de Serviço");

    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jScrollPane14).addGap(0, 0, 0)));

    layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jScrollPane14).addGap(0, 0, 0)));

    this.bindingGroup.bind();

    pack();
  }

  private void formInternalFrameClosing(InternalFrameEvent evt) {
    if ((this.alterado) && 
      (!Extra.Pergunta(this, "Sair sem salvar?"))) {
      return;
    }

    dispose();
    addNumInstancia(Integer.valueOf(-1));
  }

  private void btnSairActionPerformed(ActionEvent evt) {
    try {
      setClosed(true);
    } catch (PropertyVetoException e) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(e)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void btnCancOSActionPerformed(ActionEvent evt) {
    cancelar();
  }

  public void setAtivoButtonBuscar(boolean ativar) {
    this.btnBuscarOS.setEnabled(ativar);
    this.btnBuscarOS.setVisible(ativar);
  }

  public void executaButtonEditar() {
    btnEditOSActionPerformed(null);
  }

  private void btnNovaOSActionPerformed(ActionEvent evt) {
    cancelar();
    ativaEdits(Boolean.valueOf(true));
    ativaButtons(Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(true), Boolean.valueOf(true));
    this.alterado = true;
    this.ordemServico.clearOrdemServico();
    this.ordemServico.setUsuario(this.usuario);
    this.lblCodigo.setText(String.valueOf(NextDaos.nextGenerico(Variaveis.getEmpresa(), "os_cab")));
    this.edtCodCliente.requestFocus();
  }

  private void btnBuscarOSActionPerformed(ActionEvent evt) {
    FI_CadFindOS frmOS = (FI_CadFindOS)Util.execucaoDiretaClass("formularios.FI_CadFindOS", "Modulos-jSyscom.jar", new StringBuilder().append("Localiza Ordem de Serviço - ").append(Util.getSource(seqInstancias, FI_CadFindOS.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);

    if (frmOS != null) {
      frmOS.setAtivoButtonNovo(false);
      frmOS.ordemServico = this.ordemServico;
    }
  }

  private void btnEditOSActionPerformed(ActionEvent evt) {
    ativaEdits(Boolean.valueOf(true));
    ativaButtons(Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(true), Boolean.valueOf(true));
    this.alterado = true;
    this.pnlTabulado.setEnabledAt(1, true);
    this.pnlTabulado.setEnabledAt(2, true);
    this.pnlTabulado.setEnabledAt(3, true);
    this.pnlTabulado.setEnabledAt(4, true);
  }

  private Boolean salvarOS() {
    if (this.cliente.getCdPessoa() == null) {
      Extra.Informacao(this, "Cliente é obrigatório!");
      return Boolean.valueOf(false);
    }
    if (this.cboxTipoEquip.getSelectedItem().toString().trim().equals("")) {
      Extra.Informacao(this, "Tipo de Equipamento é obrigatório!");
      return Boolean.valueOf(false);
    }
    if (this.edtNroSerie.getText().trim().equals("")) {
      Extra.Informacao(this, "Número de Série do Equipamento é obrigatório!");
      return Boolean.valueOf(false);
    }
    if (this.edtMarca.getText().trim().equals("")) {
      Extra.Informacao(this, "Marca do Equipamento é obrigatório!");
      return Boolean.valueOf(false);
    }
    if (this.edtModelo.getText().trim().equals("")) {
      Extra.Informacao(this, "Modelo do Equipamento é obrigatório!");
      return Boolean.valueOf(false);
    }
    if (this.cboxEnderCliente.getSelectedItem().toString().trim().equals("")) {
      Extra.Informacao(this, "Endereço do Cliente é obrigatório!");
      return Boolean.valueOf(false);
    }
    if (this.edtDefeito.getText().trim().equals("")) {
      Extra.Informacao(this, "Descrição do defeito reclamado é obrigatório!");
      return Boolean.valueOf(false);
    }
    try {
      populaOS();
      if (OrdemServicoDao.salvarOS(this.ordemServico, Variaveis.getEmpresa())) {
        Imagem i = (Imagem)this.imagemConexao.clone();
        setOS();
        this.imagemConexao = i;
        setImagemConexao();
        return Boolean.valueOf(true);
      }
    } catch (Exception e) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(e)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
    return Boolean.valueOf(false);
  }

  private void salvarOrdemServico() {
    if ((this.imagemConexao.getBlobImagem() != null) && 
      (this.imagemConexao.getCodImagem() == null)) {
      try {
        Integer a = OrdemServicoDao.imagemConexaoInDB(Variaveis.getEmpresa(), this.imagemConexao);
        if ((a != null) && 
          (!Extra.Pergunta(this, "A imagem utilizada como conexão parece já existir no Banco de Dados, gostaria de salva-la mesmo assim?\nPara procura-la no banco de dados basta clicar em 'Imagem DB'")))
        {
          return;
        }

        String qry = new StringBuilder().append("INSERT INTO conexao_equip (cd_empresa, nome_conexao_equip, md5_conexao_equip, blob_conexao_equip) VALUES (").append(Variaveis.getEmpresa().getCodigo()).append(", '").append(this.imagemConexao.getNomeImagem()).append("', '").append(this.imagemConexao.getMd5Imagem()).append("', ?) RETURNING cd_conexao_equip").toString();

        this.imagemConexao.setCodImagem(ImagemDao.setImagemBanco(qry, new File(this.nomeArq)));
      } catch (SQLException|IOException ex) {
        Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
      }
    }

    if (salvarOS().booleanValue()) {
      ativaEdits(Boolean.valueOf(false));
      ativaButtons(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false));
      this.alterado = false;
      this.pnlTabulado.setSelectedComponent(this.pnlPrincipalOS);
      this.pnlAbasEquipamento.setSelectedComponent(this.pnlDEquipamento);
      this.pnlTabulado.setEnabledAt(1, false);
      this.pnlTabulado.setEnabledAt(2, false);
      this.pnlTabulado.setEnabledAt(3, false);
      this.pnlTabulado.setEnabledAt(4, false);
      Extra.Informacao(this, "Salvo com sucesso!");
    } else {
      Extra.Informacao(this, "Falha ao salvar Ordem de Serviço!");
    }
  }

  private void btnSaveOSActionPerformed(ActionEvent evt) {
    salvarOrdemServico();
  }

  private void btnExcluirOSActionPerformed(ActionEvent evt) {
    if (this.usuario.getCodigoUsuario().equals(this.ordemServico.getUsuario().getCodigoUsuario())) {
      if (this.ordemServico.getCodOS() != null) {
        if (Extra.Pergunta(this, "Excluir registro atual?"))
          if (OrdemServicoDao.excluirOS(this.ordemServico, Variaveis.getEmpresa())) {
            this.alterado = false;
            cancelar();
            Extra.Informacao(this, "Registro apagado com sucesso!");
          } else {
            Extra.Informacao(this, "Falha ao apagar registro!");
          }
      }
      else {
        this.alterado = false;
        cancelar();
      }
    }
    else Extra.Informacao(this, "Este usuário não tem permissão para excluir esta OS!");
  }

  private void limpaProd()
  {
    this.produto = new Produto();
    this.produto.addObserver(this);
    this.edtDescrProduto.setText("");
    this.edtPrecoProd.setText("0,00");
    this.edtQtdeProduto.setText("0,00");
    this.chkProdOriginal.setSelected(true);
    this.lblPrecoProd.setText("0,00");
    this.lblSubTotalProd.setText("0,00");
    this.btnAddItemProd.setEnabled(false);
    this.btnCancelarItemProd.setEnabled(false);
    this.edtQtdeProduto.setEnabled(false);
    this.chkProdOriginal.setEnabled(false);
    this.tabelaItensProd.setEnabled(true);
    limpaImagemProd();
  }

  private void limpaServico() {
    this.servico = new Servico();
    this.servico.addObserver(this);
    this.edtValorBrutoServ.setText("0,00");
    this.edtValorUnitServ.setText("0,00");
    this.lblSubtotalServ.setText("0,00");
    this.edtHoras.setText("0,00");
    this.edtObservServ.setText("");
    this.edtDescServ.setText("");
    this.chkMostraQtdHoras.setSelected(true);
    this.btnAddItemServ.setEnabled(false);
    this.btnCancelarItemServ.setEnabled(false);
    this.edtHoras.setEnabled(false);
    this.chkMostraQtdHoras.setEnabled(false);
    this.edtValorBrutoServ.setEnabled(false);
    this.edtObservServ.setEnabled(false);
    this.tabelaItensServico.setEnabled(true);
  }

  private void limpaImagemProd() {
    this.iconProd.setIcon(null);
  }

  private void limpaImagemConexao() {
    this.iconConexao.setIcon(null);
    this.iconConexao.setText("Conexão Equipamento");
    this.nomeArq = null;
    if (this.fc != null) {
      this.fc.setSelectedFile(null);
    }
    if (this.imagemConexao != null)
      this.imagemConexao.clearImagem();
  }

  private void recalculaTotais()
  {
    try
    {
      Double valorItensProd = Double.valueOf(0.0D);
      Double valorItensServ = Double.valueOf(0.0D);
      Double valorFrete = Format.stringToDouble(this.edtValorFrete.getText().trim());
      for (ItemProdOS o : this.listaItensProd) {
        valorItensProd = Double.valueOf(valorItensProd.doubleValue() + o.getValorBruto().doubleValue());
      }
      for (ItemServicoOS o : this.listaItensServico) {
        valorItensServ = Double.valueOf(valorItensServ.doubleValue() + o.getValorBruto().doubleValue());
      }
      Double valorBrutoTotal = Double.valueOf(valorItensProd.doubleValue() + valorItensServ.doubleValue());
      Double percDescontoCliente = Format.stringToDouble(this.edtMargemCliente.getText().trim());
      Double totalDesconto = Double.valueOf(0.0D);
      if (this.chkAplicarDesconto.isSelected()) {
        totalDesconto = Double.valueOf(valorBrutoTotal.doubleValue() * (percDescontoCliente.doubleValue() / 100.0D));
      }
      this.lblSubTotalProd.setText(Format.FormataValor(Format.doubleToString(valorItensProd), Variaveis.getDecimal()));
      this.lblSubtotalServ.setText(Format.FormataValor(Format.doubleToString(valorItensServ), Variaveis.getDecimal()));
      this.edtVlrBruto.setText(Format.FormataValor(Format.doubleToString(Double.valueOf(valorBrutoTotal.doubleValue() + valorFrete.doubleValue())), Variaveis.getDecimal()));
      this.edtDesconto.setText(Format.FormataValor(Format.doubleToString(totalDesconto), Variaveis.getDecimal()));
      this.edtVlrTotal.setText(Format.FormataValor(Format.doubleToString(Double.valueOf(valorBrutoTotal.doubleValue() - totalDesconto.doubleValue() + valorFrete.doubleValue())), Variaveis.getDecimal()));
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void btnNovaLocalizacaoActionPerformed(ActionEvent evt) {
    FI_CadLocalizacao o = (FI_CadLocalizacao)Util.execucaoDiretaClass("formularios.FI_CadLocalizacao", "Modulos-jSyscom.jar", new StringBuilder().append("Cadastro de Locais/Gôndolas - ").append(Util.getSource(seqInstancias, FI_CadLocalizacao.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);

    if (o != null)
      o.localProd = this.localProd;
  }

  private void btnNovoTipoEquipActionPerformed(ActionEvent evt)
  {
    FI_CadTipoEquip o = (FI_CadTipoEquip)Util.execucaoDiretaClass("formularios.FI_CadTipoEquip", "Modulos-jSyscom.jar", new StringBuilder().append("Cadastro de Tipos de Equipamento - ").append(Util.getSource(seqInstancias, FI_CadTipoEquip.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);
    if (o != null)
      o.tipoEquipamento = this.tipoEquipamento;
  }

  private void btnLimparImagemActionPerformed(ActionEvent evt)
  {
    limpaImagemConexao();
  }

  private void btnCarregaImagemBDActionPerformed(ActionEvent evt) {
    FI_SysGaleryImages galery = (FI_SysGaleryImages)Util.execucaoDiretaClass("formularios.FI_SysGaleryImages", "Modulos-jSyscom.jar", new StringBuilder().append("Localiza Imagens - ").append(Util.getSource(seqInstancias, FI_CadFindProduto.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);
    if (galery != null) {
      galery.setWidthMaxExport(0);
      galery.setSqlDefault(new StringBuilder().append("select cd_conexao_equip as cod, blob_conexao_equip as blob, md5_conexao_equip as md5, nome_conexao_equip as nome from conexao_equip where cd_empresa = ").append(Variaveis.getEmpresa().getCodigo()).toString());
      galery.exportImagem = this.imagemConexao;
    }
  }

  private void btnCarregaImagemPastaActionPerformed(ActionEvent evt) {
    if (this.fc == null) {
      this.fc = new JFileChooser();
      this.fc.addChoosableFileFilter(new ImageFilter());
      this.fc.setAcceptAllFileFilterUsed(false);
      this.fc.setFileView(new ImageFileView());
      this.fc.setAccessory(new ImagePreview(this.fc));
    }
    int returnVal = this.fc.showDialog(this, "Abrir");
    if (returnVal == 0)
      try {
        this.imagemConexao.clearImagem();
        this.imagemConexao.setBlobImagem(new ImageIcon(this.fc.getSelectedFile().toString()));
        this.imagemConexao.setMd5Imagem(Arquivo.getMD5Checksum(this.fc.getSelectedFile().toString()));
        this.imagemConexao.setNomeImagem(this.fc.getSelectedFile().getName());
        this.nomeArq = this.fc.getSelectedFile().toString();
        Integer a = OrdemServicoDao.imagemConexaoInDB(Variaveis.getEmpresa(), this.imagemConexao);
        if ((a != null) && 
          (Extra.Pergunta(this, "Esta imagem parece já existir no Banco de Dados, gostaria de usar a que esta no banco?"))) {
          this.nomeArq = null;
          Imagem i = OrdemServicoDao.getImagemConexao(Variaveis.getEmpresa(), new Imagem(a));
          this.imagemConexao.clearImagem();
          this.imagemConexao.setCodImagem(i.getCodImagem());
          this.imagemConexao.setBlobImagem(i.getBlobImagem());
          this.imagemConexao.setMd5Imagem(i.getMd5Imagem());
          this.imagemConexao.setNomeImagem(i.getNomeImagem());
        }

        this.imagemConexao.setFlagPronto(Boolean.valueOf(true));
      } catch (Exception ex) {
        Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
      }
  }

  private void edtQtdeProdutoKeyReleased(KeyEvent evt)
  {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void edtValorFreteKeyReleased(KeyEvent evt) {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
    if (evt.getKeyCode() == 10)
      recalculaTotais();
  }

  private void edtVlrBrutoKeyReleased(KeyEvent evt)
  {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void edtMargemClienteKeyReleased(KeyEvent evt) {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void edtDescontoKeyReleased(KeyEvent evt) {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void edtVlrTotalKeyReleased(KeyEvent evt) {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void edtValorBrutoServKeyReleased(KeyEvent evt) {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void edtHorasKeyReleased(KeyEvent evt) {
    if (evt.getKeyCode() == 10)
      try {
        if (Format.stringToDouble(this.edtHoras.getText().trim()).doubleValue() > 0.0D)
          this.edtValorBrutoServ.setText(Format.FormataValor(Format.doubleToString(Double.valueOf(this.servico.getValorServico().doubleValue() * Format.stringToDouble(this.edtHoras.getText().trim()).doubleValue())), Variaveis.getDecimal()));
      }
      catch (Exception ex) {
        Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
      }
    else
      try {
        Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
      } catch (Exception ex) {
        Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
      }
  }

  private void edtValorUnitServKeyReleased(KeyEvent evt)
  {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void edtPrecoProdKeyReleased(KeyEvent evt) {
    try {
      Format.formatTextField((JTextField)evt.getComponent(), Variaveis.getDecimal(), false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void btnGerarOrcActionPerformed(ActionEvent evt) {
    if (!Validador.soNumeros(this.edtDataPrev.getText()).equals("")) {
      Boolean auth = Boolean.valueOf(false);
      for (Orcamento o : this.listaOrcamentos) {
        if (o.getAutorizado().booleanValue()) {
          auth = Boolean.valueOf(true);
        }
      }
      if (auth.booleanValue()) {
        if (!Extra.Pergunta(this, "Já existe uma revisão de orçamento autorizada pelo cliente!\nGostaria de cancelar a revisão anterior para gerar um novo orçamento?"))
        {
          return;
        }
        for (Orcamento o : this.listaOrcamentos) {
          o.setAutorizado(Boolean.valueOf(false));
        }
      }

      String dias = JOptionPane.showInputDialog("Dias de validade do orçamento:");
      try {
        Integer rdias = Integer.valueOf(Integer.parseInt(Validador.soNumeros(dias)));
        Orcamento o = new Orcamento(this.ordemServico);
        recalculaTotais();
        o.setDiasValidade(rdias);
        o.setAutorizado(Boolean.valueOf(false));
        o.setDataPrevisao(Format.StrToDate(this.edtDataPrev.getText()));
        o.setTipoFrete(this.cboxTipoFrete.getSelectedItem().toString());
        o.setUsoDescontoCliente(Boolean.valueOf(this.chkAplicarDesconto.isSelected()));
        o.setValorBruto(Format.stringToDouble(this.edtVlrBruto.getText().trim()));
        o.setValorDesconto(Format.stringToDouble(this.edtDesconto.getText().trim()));
        o.setValorLiquido(Format.stringToDouble(this.edtVlrTotal.getText().trim()));
        o.setValorFrete(Format.stringToDouble(this.edtValorFrete.getText().trim()));
        o.setOrdemServico(this.ordemServico);
        this.listaOrcamentos.add(o);
        this.ordemServico.getOrcamentos().clear();
        this.ordemServico.getOrcamentos().addAll(this.listaOrcamentos);
        this.ordemServico.setUltimoOrcamento(o);
        this.cboxStatus.setSelectedItem("AGUARDANDO AUTH");
        this.btnImprOrc.setEnabled(true);
        this.btnAuthOrc.setEnabled(true);
        if (Extra.Pergunta(this, "Salvar ordem de serviço agora?"))
          salvarOrdemServico();
      }
      catch (Exception e) {
        Extra.Informacao(this, "Nro de dias inválido!");
      }
    }
    else {
      Extra.Informacao(this, "Data de previsão inválida!");
    }
  }

  private void chkGarantiaActionPerformed(ActionEvent evt) {
  }

  private void cboxTipoFreteItemStateChanged(ItemEvent evt) {
    if (this.cboxTipoFrete.getSelectedItem().toString().toUpperCase().trim().equals("FOB")) {
      this.edtValorFrete.setEnabled(true);
    } else {
      this.edtValorFrete.setText("0,00");
      this.edtValorFrete.setEnabled(false);
    }
  }

  private void btnBuscaClienteActionPerformed(ActionEvent evt) {
    FI_CadFindPessoa frmFC = (FI_CadFindPessoa)Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", new StringBuilder().append("Localiza Cliente - ").append(Util.getSource(seqInstancias, FI_CadFindPessoa.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);
    if (frmFC != null) {
      frmFC.setTipoObrigatorioBusca(this.cliente);
      frmFC.pessoa = this.cliente;
    }
  }

  private void edtCodClienteKeyReleased(KeyEvent evt) {
  }

  private void edtCodClienteKeyPressed(KeyEvent evt) {
    if (evt.getKeyCode() == 10)
      if (!PessoaDao.buscaCliente(Integer.valueOf(Integer.parseInt(Validador.soNumeros(this.edtCodCliente.getText().trim()))), Variaveis.getEmpresa()).getNomePessoa().trim().equals("")) {
        this.cliente.setCdPessoa(Integer.valueOf(Integer.parseInt(Validador.soNumeros(this.edtCodCliente.getText().trim()))));
        PessoaDao.buscaCliente(this.cliente, Variaveis.getEmpresa());
      } else {
        Extra.Informacao(this, "Cliente não encontrado!");
      }
  }

  private void btnAddProdActionPerformed(ActionEvent evt)
  {
    this.edtQtdeProduto.requestFocus();
    FI_CadFindProduto o = (FI_CadFindProduto)Util.execucaoDiretaClass("formularios.FI_CadFindProduto", "Modulos-jSyscom.jar", new StringBuilder().append("Localiza Produtos - ").append(Util.getSource(seqInstancias, FI_CadFindProduto.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);
    if (o != null)
      o.produto = this.produto;
  }

  private void btnAddServActionPerformed(ActionEvent evt)
  {
    this.edtHoras.requestFocus();
    FI_CadServico o = (FI_CadServico)Util.execucaoDiretaClass("formularios.FI_CadServico", "Modulos-jSyscom.jar", new StringBuilder().append("Localiza Serviços - ").append(Util.getSource(seqInstancias, FI_CadFindProduto.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);
    if (o != null)
      o.servico = this.servico;
  }

  private void btnAddEnvolvidosActionPerformed(ActionEvent evt)
  {
    FI_CadFindPessoa o = (FI_CadFindPessoa)Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", new StringBuilder().append("Localiza Funcionarios - ").append(Util.getSource(seqInstancias, FI_CadFindProduto.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);
    if (o != null) {
      o.setTipoObrigatorioBusca(new Funcionario());
      o.listaExport = this.listaEnvolvidos;
    }
  }

  private void btnAddItemProdActionPerformed(ActionEvent evt) {
    try {
      if (this.produto.getCodProduto() != null) {
        ItemProdOS item = new ItemProdOS();
        if (!this.tabelaItensProd.isEnabled()) {
          item = (ItemProdOS)this.listaItensProd.get(this.tabelaItensProd.getSelectedRow());
          this.listaItensProd.remove(item);
        }
        item.setOrdemServico(this.ordemServico);
        item.setOriginal(Boolean.valueOf(this.chkProdOriginal.isSelected()));
        item.setProduto(this.produto);
        item.setQuantidade(Format.stringToDouble(this.edtQtdeProduto.getText().trim()));
        item.setValorUnitario(this.produto.getValorVendaProduto());
        item.setValorBruto(Double.valueOf(this.produto.getValorVendaProduto().doubleValue() * Format.stringToDouble(this.edtQtdeProduto.getText().trim()).doubleValue()));
        for (ItemProdOS i : this.listaItensProd) {
          if (item.equals(i)) {
            item.setQuantidade(Double.valueOf(item.getQuantidade().doubleValue() + i.getQuantidade().doubleValue()));
            item.setValorBruto(Double.valueOf(item.getValorBruto().doubleValue() + i.getValorBruto().doubleValue()));
            this.listaItensProd.remove(i);
            break;
          }
        }
        this.listaItensProd.add(item);
        limpaProd();
        recalculaTotais();
      } else {
        Extra.Informacao(this, "Nenhum produto selecionado!");
      }
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void tabelaItensProdPropertyChange(PropertyChangeEvent evt) {
  }

  private void btnAddItemServActionPerformed(ActionEvent evt) {
    try {
      if (this.servico.getCodServico() != null) {
        ItemServicoOS item = new ItemServicoOS();
        if (!this.tabelaItensServico.isEnabled()) {
          item = (ItemServicoOS)this.listaItensServico.get(this.tabelaItensServico.getSelectedRow());
          this.listaItensServico.remove(item);
        }
        item.setOrdemServico(this.ordemServico);
        item.setServico((Servico)this.servico.clone());
        item.setMostrarQtde(Boolean.valueOf(this.chkMostraQtdHoras.isSelected()));
        item.setObservacoes(this.edtObservServ.getText());
        item.setQuantidade(Format.stringToDouble(this.edtHoras.getText().trim()));
        item.setValorUnitario(this.servico.getValorServico());
        item.setValorBruto(Format.stringToDouble(this.edtValorBrutoServ.getText().trim()));
        for (ItemServicoOS i : this.listaItensServico) {
          if (item.equals(i)) {
            item.setQuantidade(Double.valueOf(item.getQuantidade().doubleValue() + i.getQuantidade().doubleValue()));
            item.setValorBruto(Double.valueOf(item.getValorBruto().doubleValue() + i.getValorBruto().doubleValue()));
            this.listaItensServico.remove(i);
            break;
          }
        }
        this.listaItensServico.add(item);
        limpaServico();
        recalculaTotais();
      }
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void btnAddTesteActionPerformed(ActionEvent evt) {
    if ((!this.edtDescrTeste.getText().trim().equals("")) && (!this.edtResultTest.getText().trim().equals(""))) {
      EquipamentoTeste teste = new EquipamentoTeste();
      teste.setDescricao(this.edtDescrTeste.getText().trim());
      teste.setDefeito(this.edtResultTest.getText().trim());
      teste.setEquipamento(this.equipamento);
      this.listaEquipamentoTestes.add(teste);
      this.edtDescrTeste.setText("");
      this.edtResultTest.setText("");
    }
    this.edtDescrTeste.requestFocus();
  }

  private void edtResultTestKeyReleased(KeyEvent evt) {
    if (evt.getKeyCode() == 10)
      btnAddTesteActionPerformed(null);
  }

  private void edtDescrTesteKeyReleased(KeyEvent evt)
  {
    if (evt.getKeyCode() == 10)
      this.edtResultTest.requestFocus();
  }

  private void edtNroSerieKeyReleased(KeyEvent evt)
  {
  }

  private void edtNroSerieKeyPressed(KeyEvent evt) {
    if ((evt.getKeyCode() == 10) || (evt.getKeyCode() == 9))
      if (this.cliente.getCdPessoa() != null) {
        this.equipamento.setCliente(this.cliente);
        this.equipamento.setNroSerie(this.edtNroSerie.getText().toUpperCase().trim());
        if (EquipamentoDao.alreadyExistsEquip(this.equipamento, Variaveis.getEmpresa()).booleanValue()) {
          Extra.Informacao(this, "Equipamento encontrado!");
          if (EquipamentoDao.emGarantia(this.equipamento, Variaveis.getEmpresa()).booleanValue()) {
            Extra.Informacao(this, "Este equipamento parece estar na garantia!\nFavor verificar seu histórico!");
          }

          EquipamentoDao.buscaEquipamentoSerie(this.equipamento, Variaveis.getEmpresa(), Boolean.valueOf(true));
          setEquipamento();
        }
        ativaEditsEquip(Boolean.valueOf(true));
      } else {
        Extra.Informacao(this, "Primeiro defina o cliente!");
      }
  }

  private void btnAtualizarValoresProdActionPerformed(ActionEvent evt)
  {
    if (Extra.Pergunta(this, "Esta opção ira recalcular todos os itens de produtos com os seus respectivos valores atualizados!\nApós a execução desta opção será necessaria a geração de uma nova revisão de orçamento!\nRecalcular todos os itens de produtos ?"))
    {
      for (ItemProdOS o : this.listaItensProd) {
        ProdutoDao.buscaProduto(o.getProduto(), Variaveis.getEmpresa(), Boolean.valueOf(true));
        o.setValorUnitario(o.getProduto().getValorVendaProduto());
        o.setValorBruto(Double.valueOf(o.getValorUnitario().doubleValue() * o.getQuantidade().doubleValue()));
      }
      recalculaTotais();
    }
  }

  private void btnAtualizarValoresServActionPerformed(ActionEvent evt) {
    if (Extra.Pergunta(this, "Esta opção ira recalcular todos os itens de serviço com os seus respectivos valores atualizados!\nApós a execução desta opção será necessaria a geração de uma nova revisão de orçamento!\nRecalcular todos os itens de serviço ?"))
    {
      for (ItemServicoOS o : this.listaItensServico) {
        ServicoDao.buscaServico(o.getServico(), Variaveis.getEmpresa());
        o.setValorUnitario(o.getServico().getValorServico());
        o.setValorBruto(Double.valueOf(o.getValorUnitario().doubleValue() * o.getQuantidade().doubleValue()));
      }
      recalculaTotais();
    }
  }

  private void mItemProdRemoverActionPerformed(ActionEvent evt) {
    if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
      this.listaItensProd.remove(this.tabelaItensProd.getSelectedRow());
      recalculaTotais();
    }
  }

  private void mItemServRemoverActionPerformed(ActionEvent evt) {
    if (Extra.Pergunta(this, "Confirma exclusão deste item?")) {
      this.listaItensServico.remove(this.tabelaItensServico.getSelectedRow());
      recalculaTotais();
    }
  }

  private void mEnvolvidosRemoverActionPerformed(ActionEvent evt) {
    if (Extra.Pergunta(this, "Confirma exclusão deste item?"))
      this.listaEnvolvidos.remove(this.tabelaEnvolvidos.getSelectedRow());
  }

  private void mAnalizeRemoverActionPerformed(ActionEvent evt)
  {
    if (Extra.Pergunta(this, "Confirma exclusão deste item?"))
      this.listaEquipamentoTestes.remove(this.tabelaTestes.getSelectedRow());
  }

  private void mItemProdAlterarActionPerformed(ActionEvent evt)
  {
    try {
      limpaProd();
      ItemProdOS item = (ItemProdOS)this.listaItensProd.get(this.tabelaItensProd.getSelectedRow());
      this.produto = item.getProduto();
      setProduto();
      this.edtQtdeProduto.setText(Format.FormataValor(Format.doubleToString(item.getQuantidade()), Variaveis.getDecimal()));
      this.chkProdOriginal.setSelected(item.getOriginal().booleanValue());
      this.tabelaItensProd.setEnabled(false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void mItemServAlterarActionPerformed(ActionEvent evt) {
    try {
      limpaServico();
      ItemServicoOS item = (ItemServicoOS)this.listaItensServico.get(this.tabelaItensServico.getSelectedRow());
      this.servico = item.getServico();
      setServico();
      this.edtObservServ.setText(item.getObservacoes());
      this.edtValorBrutoServ.setText(Format.FormataValor(Format.doubleToString(item.getValorBruto()), Variaveis.getDecimal()));
      this.edtHoras.setText(Format.FormataValor(Format.doubleToString(item.getQuantidade()), Variaveis.getDecimal()));
      this.chkMostraQtdHoras.setSelected(item.getMostrarQtde().booleanValue());
      this.tabelaItensServico.setEnabled(false);
    } catch (Exception ex) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(ex)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void btnCancelarItemServActionPerformed(ActionEvent evt) {
    limpaServico();
  }

  private void btnCancelarItemProdActionPerformed(ActionEvent evt) {
    limpaProd();
  }

  private void chkAplicarDescontoItemStateChanged(ItemEvent evt) {
    recalculaTotais();
  }

  private void btnRecalcularTotaisActionPerformed(ActionEvent evt) {
    recalculaTotais();
    Extra.Informacao(this, "Os totais estão atualizados!");
  }

  private void btnImprConsertoActionPerformed(ActionEvent evt) {
    HashMap h = new HashMap();
    List ordems = new ArrayList();
    ordems.add(this.ordemServico);
    h.put("USUARIO", this.usuario);
    Util.imprimeRelatorioJarFile(ordems, h, "rel/fichaconserto/relFichaConc.jasper", Variaveis.getEmpresa());
  }

  private void btnAuthOrcActionPerformed(ActionEvent evt) {
    if (Extra.Pergunta(this, new StringBuilder().append("Confirma autorização do cliente para execução da Ordem de Serviço por meio do orçamento [ Revisão: ").append(String.valueOf(this.ordemServico.getUltimoOrcamento().getNroRevisao())).append(" ] ?").toString())) {
      if (this.ordemServico.getUltimoOrcamento().getDataCadastro() == null) {
        this.ordemServico.getUltimoOrcamento().setDataCadastro(new Date());
      }
      if (Format.getDiffDay(this.ordemServico.getUltimoOrcamento().getDataCadastro(), new Date()) <= this.ordemServico.getUltimoOrcamento().getDiasValidade().intValue()) {
        if (!this.ordemServico.getUltimoOrcamento().getAutorizado().booleanValue()) {
          Orcamento item = this.ordemServico.getUltimoOrcamento();
          this.listaOrcamentos.remove(item);
          item.setAutorizado(Boolean.valueOf(true));
          item.setDataAutorizacao(new Date());
          this.listaOrcamentos.add(item);
        }
        this.cboxStatus.setSelectedItem("EM ANDAMENTO");
        this.btnManutReal.setEnabled(true);
        Extra.Informacao(this, "Orçamento Autorizado!, Aguardando retorno da manuteção!");
      } else {
        Extra.Informacao(this, "Praso de validade do último orçamento venceu! Favor gerar um novo orçamento!");
      }
    }
  }

  private void btnImprOrcActionPerformed(ActionEvent evt) {
    if (this.listaOrcamentos.size() > 0) {
      if (this.ordemServico.getUltimoOrcamento() != null) {
        HashMap h = new HashMap();
        List ordems = new ArrayList();
        ordems.add(this.ordemServico);
        h.put("USUARIO", this.usuario);
        Util.imprimeRelatorioJarFile(ordems, h, "rel/orcamento/relOrcCab.jasper", Variaveis.getEmpresa());
      }
    }
    else Extra.Informacao(this, "Não há orçamentos gerados para esta Ordem de Serviço!");
  }

  private void btnManutRealActionPerformed(ActionEvent evt)
  {
    if (Extra.Pergunta(this, "Confirma realização da manutenção ?")) {
      this.cboxStatus.setSelectedItem("AGUARDANDO PGTO");
      this.btnFinalPgto.setEnabled(true);
      this.btnManutReal.setEnabled(false);
      Extra.Informacao(this, "Manutenção realizada!, Aguardando Pagamento!");
    }
  }

  private void btnFinalPgtoActionPerformed(ActionEvent evt) {
    if (Extra.Pergunta(this, "Finalizar pagamanto da ordem de serviço?")) {
      this.cboxStatus.setSelectedItem("FINALIZADO");
      this.btnFinalPgto.setEnabled(false);
      Extra.Informacao(this, "Ordem de Serviço finalizada!");
    }
  }

  private void btnNovaTraspActionPerformed(ActionEvent evt) {
    FI_CadTransp o = (FI_CadTransp)Util.execucaoDiretaClass("formularios.FI_CadTransp", "Modulos-jSyscom.jar", new StringBuilder().append("Cadastro de Transportadora - ").append(Util.getSource(seqInstancias, FI_CadTransp.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);
    if (o != null)
      o.transportadora = this.transportadora;
  }

  private void cboxTranspItemStateChanged(ItemEvent evt)
  {
  }

  private void cboxTipoEquipItemStateChanged(ItemEvent evt) {
  }

  private void cboxTipoEquipActionPerformed(ActionEvent evt) {
    if (this.aa) {
      if ((!this.cboxTipoEquip.getSelectedItem().equals(this.tipoEquipAnterior)) && (this.tipoEquipAnterior != null)) {
        if (Extra.Pergunta(this, "Descartar Propriedades?")) {
          if (this.cboxTipoEquip.getSelectedIndex() == 0) {
            this.tipoEquipAnterior = null;
            this.listaItensPropriedades.clear();
          } else {
            populaTabelaPropDef();
          }
        }
        else this.cboxTipoEquip.setSelectedItem(this.tipoEquipAnterior);
      }
      else if ((this.tipoEquipAnterior == null) && (this.cboxTipoEquip.getSelectedIndex() != 0))
        populaTabelaPropDef();
    }
    else
      this.aa = true;
  }

  private void edtNroSerieFocusLost(FocusEvent evt)
  {
  }

  private void btnImprFichaCliActionPerformed(ActionEvent evt)
  {
    HashMap h = new HashMap();
    List ordems = new ArrayList();
    ordems.add(this.ordemServico);
    h.put("USUARIO", this.usuario);
    Util.imprimeRelatorioJarFile(ordems, h, "rel/fichacliente/relFichaCliConc.jasper", Variaveis.getEmpresa());
  }

  private void populaTabelaPropDef() {
    this.tipoEquipAnterior = ((TipoEquipamento)this.cboxTipoEquip.getSelectedItem());
    this.listaItensPropriedades.clear();
    this.tipoEquipAnterior.getPropriedades().clear();
    this.tipoEquipAnterior.getPropriedades().addAll(EquipamentoDao.getAllPropTipoEquipamento(this.tipoEquipAnterior, Variaveis.getEmpresa()));
    for (PropriedadeTipoEquip p : this.tipoEquipAnterior.getPropriedades()) {
      ItemPropTipoEquip a = new ItemPropTipoEquip(p, "");
      this.listaItensPropriedades.add(a);
    }
  }

  public void update(Observable o, Object arg)
  {
    if ((o instanceof OrdemServico)) {
      limpaCampos();
      ativaEdits(Boolean.valueOf(false));
      this.pnlTabulado.setEnabledAt(1, false);
      this.pnlTabulado.setEnabledAt(2, false);
      this.pnlTabulado.setEnabledAt(3, false);
      ativaButtons(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false));
      this.alterado = false;
      setOS();
    } else if ((o instanceof Imagem)) {
      setImagemConexao();
    } else if ((o instanceof LocalProd)) {
      setLocalizacao();
    } else if ((o instanceof TipoEquipamento)) {
      setTipoEquip();
    } else if ((o instanceof Transportadora)) {
      setTransportadora();
    } else if ((o instanceof Cliente)) {
      setCliente();
    } else if ((o instanceof Produto)) {
      setProduto();
    } else if ((o instanceof Servico)) {
      setServico();
    }
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Modulos.jar
 * Qualified Name:     formularios.FI_CadOS
 * JD-Core Version:    0.6.2
 */
