package formularios;

import base.FrmPrincipal;
import beans.Empresa;
import beans.Menu;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import dao.MenuDao;
import dao.UsuarioDao;
import funcoes.Util;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.ELProperty;
import org.jdesktop.observablecollections.ObservableCollections;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.JTableBinding.ColumnBinding;
import org.jdesktop.swingbinding.SwingBindings;
import utilitarios.DataBase;
import utilitarios.Extra;
import utilitarios.Utils;

public final class FI_SysPermissoes extends JInternalFrame
{
  private static Integer numInstancias = Integer.valueOf(0);
  private Integer numMaxInstancias = Integer.valueOf(1);
  private static final String versao = "v0.01";
  private FrmPrincipal princ = null;
  private static Integer seqInstancias = Integer.valueOf(0);
  private Boolean exp = Boolean.valueOf(false);
  Boolean alteraTabela = Boolean.valueOf(false);
  private Usuario usuario = null;
  public static DefaultTreeModel model;
  public static DefaultMutableTreeNode root;
  private JMenuItem addUsersMenu;
  private JTree arvoreMenu;
  private JButton btnAddUser;
  private JButton btnAplicar;
  private JButton btnCancelar;
  private JButton btnSair;
  private JButton btnSalvar;
  private JComboBox cboxClasse;
  private JComboBox cboxModulos;
  private JComboBox cboxOperacoes;
  private JComboBox cboxUsuario;
  private JButton jBnAtualiza;
  private JButton jBnExpande;
  private JLabel jLabel1;
  private JLabel jLabel2;
  private JLabel jLabel3;
  private JScrollPane jScrollPane1;
  private JScrollPane jScrollPane2;
  private JLabel lblClasse;
  private JLabel lblModulo;
  private List<Usuario> listaUsuarios;
  private List<Usuario> listaUsuariosSistema;
  private JMenuItem menuAtualizar;
  private JPanel pnlBotoes;
  private JPanel pnlMenu;
  private JPanel pnlOperacoes;
  private JTabbedPane pnlTabulacoes;
  private JPopupMenu popupMenuTabela;
  private JPopupMenu popupMenuUserSys;
  private JMenuItem removeAllUserMenu;
  private JMenuItem removeUserMenu;
  private JTable tabelaPermitidos;
  private BindingGroup bindingGroup;

  public Usuario getUsuario()
  {
    return this.usuario;
  }

  public void setUsuario(Usuario usuario) {
    this.usuario = usuario;
  }

  public Integer getNumInstancias() {
    return numInstancias;
  }

  public void addNumInstancia(Integer i) {
    numInstancias = Integer.valueOf(numInstancias.intValue() + i.intValue());
    if (i.intValue() > -1)
      seqInstancias = Integer.valueOf(seqInstancias.intValue() + 1);
  }

  public FI_SysPermissoes(String title, OpcoesTela parametros, Usuario user)
  {
    if (numInstancias.intValue() >= this.numMaxInstancias.intValue()) {
      String msg = new StringBuilder().append("Número máximo instâncias (").append(this.numMaxInstancias).append(") de ").append(title).append(" atingido!").toString();
      Variaveis.addNewLog(msg, Boolean.valueOf(false), Boolean.valueOf(true), this);
    } else {
      initComponents();
      this.usuario = user;
      if (parametros.getShow_version().booleanValue()) {
        title = new StringBuilder().append(title).append(" [v0.01]").toString();
      }
      setClosable(parametros.getClosable().booleanValue());
      setIconifiable(parametros.getIconifiable().booleanValue());
      setMaximizable(parametros.getMaximizable().booleanValue());
      setResizable(parametros.getResizable().booleanValue());
      setTitle(new StringBuilder().append(title).append(seqInstancias.intValue() > 0 ? new StringBuilder().append(" - [").append(String.valueOf(seqInstancias)).append("]").toString() : "").toString());
      this.princ = Variaveis.getFormularioPrincipal();
      this.princ.setLocalizacaoFrame(this);
      this.princ.Desktop.add(this, JLayeredPane.DEFAULT_LAYER);
      this.princ.Desktop.setComponentZOrder(this, 0);
      setVisible(parametros.getVisible().booleanValue());
      addNumInstancia(Integer.valueOf(1));
      this.arvoreMenu.setModel(getTreeModel());
      updateTreeModel();
      this.btnCancelar.setEnabled(false);
      this.btnSalvar.setEnabled(false);
      setUsersCombo();
      this.cboxClasse.setEnabled(false);
      this.cboxClasse.setVisible(false);
      this.lblClasse.setVisible(false);
      this.cboxModulos.setEnabled(false);
      this.cboxModulos.setVisible(false);
      this.lblModulo.setVisible(false);
    }
  }

  public static DefaultTreeModel getTreeModel() {
    root = new DefaultMutableTreeNode(Variaveis.getNomeMenu());
    model = new DefaultTreeModel(root);
    return model;
  }

  public static void updateTreeModel()
  {
    root.removeAllChildren();
    model.reload();
    try {
      MenuDao.setGrupos(root, null, null);
    } catch (SQLException ex) {
      Variaveis.addNewLog(ex.getMessage(), Boolean.valueOf(true), Boolean.valueOf(true), null);
    }
    model.reload();
  }

  private void initComponents()
  {
    this.bindingGroup = new BindingGroup();

    this.listaUsuarios = ObservableCollections.observableList(new ArrayList());
    this.popupMenuTabela = new JPopupMenu();
    this.addUsersMenu = new JMenuItem();
    this.removeUserMenu = new JMenuItem();
    this.removeAllUserMenu = new JMenuItem();
    this.listaUsuariosSistema = ObservableCollections.observableList(new ArrayList());
    this.popupMenuUserSys = new JPopupMenu();
    this.menuAtualizar = new JMenuItem();
    this.pnlTabulacoes = new JTabbedPane();
    this.pnlMenu = new JPanel();
    this.jScrollPane2 = new JScrollPane();
    this.arvoreMenu = new JTree();
    this.jBnExpande = new JButton();
    this.jBnAtualiza = new JButton();
    this.pnlOperacoes = new JPanel();
    this.btnAplicar = new JButton();
    this.jLabel3 = new JLabel();
    this.cboxUsuario = new JComboBox();
    this.jLabel2 = new JLabel();
    this.cboxOperacoes = new JComboBox();
    this.lblClasse = new JLabel();
    this.cboxClasse = new JComboBox();
    this.lblModulo = new JLabel();
    this.cboxModulos = new JComboBox();
    this.jScrollPane1 = new JScrollPane();
    this.tabelaPermitidos = new JTable();
    this.jLabel1 = new JLabel();
    this.pnlBotoes = new JPanel();
    this.btnSalvar = new JButton();
    this.btnSair = new JButton();
    this.btnCancelar = new JButton();
    this.btnAddUser = new JButton();

    this.addUsersMenu.setText("Adicionar Usuário(s)");
    this.addUsersMenu.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.addUsersMenuActionPerformed(evt);
      }
    });
    this.popupMenuTabela.add(this.addUsersMenu);

    this.removeUserMenu.setText("Remover usuário(s) selecionado(s)");
    this.removeUserMenu.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.removeUserMenuActionPerformed(evt);
      }
    });
    this.popupMenuTabela.add(this.removeUserMenu);

    this.removeAllUserMenu.setText("Remover Todos");
    this.removeAllUserMenu.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.removeAllUserMenuActionPerformed(evt);
      }
    });
    this.popupMenuTabela.add(this.removeAllUserMenu);

    this.menuAtualizar.setText("Atualizar");
    this.menuAtualizar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.menuAtualizarActionPerformed(evt);
      }
    });
    this.popupMenuUserSys.add(this.menuAtualizar);

    setDefaultCloseOperation(0);
    addInternalFrameListener(new InternalFrameListener() {
      public void internalFrameOpened(InternalFrameEvent evt) {
      }
      public void internalFrameClosing(InternalFrameEvent evt) {
        FI_SysPermissoes.this.formInternalFrameClosing(evt);
      }

      public void internalFrameClosed(InternalFrameEvent evt)
      {
      }

      public void internalFrameIconified(InternalFrameEvent evt)
      {
      }

      public void internalFrameDeiconified(InternalFrameEvent evt)
      {
      }

      public void internalFrameActivated(InternalFrameEvent evt)
      {
      }

      public void internalFrameDeactivated(InternalFrameEvent evt)
      {
      }
    });
    this.pnlTabulacoes.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent evt) {
        FI_SysPermissoes.this.pnlTabulacoesStateChanged(evt);
      }
    });
    DefaultMutableTreeNode treeNode1 = new DefaultMutableTreeNode("root");
    this.arvoreMenu.setModel(new DefaultTreeModel(treeNode1));
    this.arvoreMenu.addTreeSelectionListener(new TreeSelectionListener() {
      public void valueChanged(TreeSelectionEvent evt) {
        FI_SysPermissoes.this.arvoreMenuValueChanged(evt);
      }
    });
    this.jScrollPane2.setViewportView(this.arvoreMenu);

    this.jBnExpande.setIcon(new ImageIcon(getClass().getResource("/imagens/expand.png")));
    this.jBnExpande.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.jBnExpandeActionPerformed(evt);
      }
    });
    this.jBnAtualiza.setIcon(new ImageIcon(getClass().getResource("/imagens/reload.png")));
    this.jBnAtualiza.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.jBnAtualizaActionPerformed(evt);
      }
    });
    GroupLayout pnlMenuLayout = new GroupLayout(this.pnlMenu);
    this.pnlMenu.setLayout(pnlMenuLayout);
    pnlMenuLayout.setHorizontalGroup(pnlMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlMenuLayout.createSequentialGroup().addContainerGap().addGroup(pnlMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane2, -1, 274, 32767).addGroup(pnlMenuLayout.createSequentialGroup().addComponent(this.jBnExpande, -2, 35, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jBnAtualiza, -2, 35, -2).addGap(0, 191, 32767))).addContainerGap()));

    pnlMenuLayout.setVerticalGroup(pnlMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlMenuLayout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane2, -1, 462, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(pnlMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jBnAtualiza).addComponent(this.jBnExpande))));

    this.pnlTabulacoes.addTab("Menus", this.pnlMenu);

    this.btnAplicar.setText("Aplicar");
    this.btnAplicar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.btnAplicarActionPerformed(evt);
      }
    });
    this.jLabel3.setText("Selecione o usuário destino:");

    this.jLabel2.setText("Operações:");

    this.cboxOperacoes.setModel(new DefaultComboBoxModel(new String[] { "Copiar de outro usuário ->", "Aplicar todas as permissões", "Aplicar permissões para classe" }));
    this.cboxOperacoes.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        FI_SysPermissoes.this.cboxOperacoesItemStateChanged(evt);
      }
    });
    this.lblClasse.setText("Chamada de Tela:");

    this.cboxClasse.setEditable(true);
    this.cboxClasse.setEnabled(false);

    this.lblModulo.setText("Módulo:");

    this.cboxModulos.setEnabled(false);
    this.cboxModulos.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.cboxModulosActionPerformed(evt);
      }
    });
    GroupLayout pnlOperacoesLayout = new GroupLayout(this.pnlOperacoes);
    this.pnlOperacoes.setLayout(pnlOperacoesLayout);
    pnlOperacoesLayout.setHorizontalGroup(pnlOperacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlOperacoesLayout.createSequentialGroup().addContainerGap().addGroup(pnlOperacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.cboxClasse, 0, -1, 32767).addGroup(pnlOperacoesLayout.createSequentialGroup().addGroup(pnlOperacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lblClasse).addGroup(pnlOperacoesLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.cboxOperacoes, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.jLabel3, GroupLayout.Alignment.LEADING).addComponent(this.cboxUsuario, GroupLayout.Alignment.LEADING, 0, 269, 32767).addComponent(this.jLabel2, GroupLayout.Alignment.LEADING)).addComponent(this.lblModulo)).addGap(0, 0, 32767)).addComponent(this.cboxModulos, 0, 270, 32767).addGroup(GroupLayout.Alignment.TRAILING, pnlOperacoesLayout.createSequentialGroup().addGap(0, 0, 32767).addComponent(this.btnAplicar))).addContainerGap()));

    pnlOperacoesLayout.setVerticalGroup(pnlOperacoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlOperacoesLayout.createSequentialGroup().addContainerGap().addComponent(this.jLabel3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cboxUsuario, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jLabel2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cboxOperacoes, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.lblModulo).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cboxModulos, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.lblClasse).addGap(2, 2, 2).addComponent(this.cboxClasse, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnAplicar).addContainerGap(228, 32767)));

    this.pnlTabulacoes.addTab("Operações com Permissões", this.pnlOperacoes);

    this.tabelaPermitidos.setComponentPopupMenu(this.popupMenuTabela);
    this.tabelaPermitidos.setInheritsPopupMenu(true);

    JTableBinding jTableBinding = SwingBindings.createJTableBinding(AutoBinding.UpdateStrategy.READ_WRITE, this.listaUsuarios, this.tabelaPermitidos);
    JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${codigoUsuario}"));
    columnBinding.setColumnName("Codigo Usuario");
    columnBinding.setColumnClass(Integer.class);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${nomeUsuario}"));
    columnBinding.setColumnName("Nome Usuario");
    columnBinding.setColumnClass(String.class);
    columnBinding = jTableBinding.addColumnBinding(ELProperty.create("${cargo}"));
    columnBinding.setColumnName("Cargo");
    columnBinding.setColumnClass(String.class);
    this.bindingGroup.addBinding(jTableBinding);
    jTableBinding.bind();
    this.jScrollPane1.setViewportView(this.tabelaPermitidos);

    this.jLabel1.setFont(new Font("Tahoma", 1, 11));
    this.jLabel1.setText("Usuários com Permissões:");

    this.btnSalvar.setText("Salvar");
    this.btnSalvar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.btnSalvarActionPerformed(evt);
      }
    });
    this.btnSair.setText("Sair");
    this.btnSair.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.btnSairActionPerformed(evt);
      }
    });
    this.btnCancelar.setText("Cancelar");
    this.btnCancelar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.btnCancelarActionPerformed(evt);
      }
    });
    this.btnAddUser.setText("Adicionar Usuário(s)");
    this.btnAddUser.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        FI_SysPermissoes.this.btnAddUserActionPerformed(evt);
      }
    });
    GroupLayout pnlBotoesLayout = new GroupLayout(this.pnlBotoes);
    this.pnlBotoes.setLayout(pnlBotoesLayout);
    pnlBotoesLayout.setHorizontalGroup(pnlBotoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(pnlBotoesLayout.createSequentialGroup().addComponent(this.btnAddUser).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.btnSalvar).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnCancelar).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnSair)));

    pnlBotoesLayout.linkSize(0, new Component[] { this.btnCancelar, this.btnSair, this.btnSalvar });

    pnlBotoesLayout.setVerticalGroup(pnlBotoesLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, pnlBotoesLayout.createSequentialGroup().addGap(0, 0, 32767).addGroup(pnlBotoesLayout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.btnSalvar).addComponent(this.btnSair).addComponent(this.btnCancelar).addComponent(this.btnAddUser))));

    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.pnlTabulacoes, -2, 299, -2).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(8, 8, 8).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1).addComponent(this.jScrollPane1))).addGroup(layout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.pnlBotoes, -1, -1, 32767))).addContainerGap()));

    layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.pnlTabulacoes).addGroup(layout.createSequentialGroup().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.pnlBotoes, -2, -1, -2))).addContainerGap()));

    this.bindingGroup.bind();

    pack();
  }

  private void formInternalFrameClosing(InternalFrameEvent evt) {
    if ((this.alteraTabela.booleanValue()) && 
      (!Extra.Pergunta(this, "Deseja descartar alterações não salvas?"))) {
      return;
    }

    dispose();
    addNumInstancia(Integer.valueOf(-1));
  }

  private void setUsersCombo() {
    this.cboxUsuario.removeAllItems();
    for (Usuario u : this.listaUsuariosSistema)
      this.cboxUsuario.addItem(u);
  }

  private void atualizaListaUserSys(Empresa empresa)
  {
    this.listaUsuariosSistema.clear();
    UsuarioDao.listarUsuarios(empresa, this.listaUsuariosSistema);
    setUsersCombo();
  }

  private void arvoreMenuValueChanged(TreeSelectionEvent evt) {
    if (this.arvoreMenu.getSelectionPath() != null) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode)this.arvoreMenu.getLastSelectedPathComponent();
      if ((node.getUserObject() instanceof Menu)) {
        Menu item = (Menu)node.getUserObject();
        this.listaUsuarios.clear();
        this.btnSalvar.setEnabled(false);
        this.btnCancelar.setEnabled(false);
        if (item.getOperacao() != null)
          UsuarioDao.buscaPermitidos(item.getOperacao(), Variaveis.getEmpresa(), this.listaUsuarios);
      }
    }
  }

  private void jBnExpandeActionPerformed(ActionEvent evt)
  {
    this.exp = Boolean.valueOf(!this.exp.booleanValue());
    Util.expandAll(this.arvoreMenu, this.exp.booleanValue());
  }

  private void jBnAtualizaActionPerformed(ActionEvent evt) {
    this.arvoreMenu.setModel(getTreeModel());
    updateTreeModel();
    this.listaUsuarios.clear();
  }

  private void btnSairActionPerformed(ActionEvent evt) {
    try {
      setClosed(true);
    } catch (PropertyVetoException e) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(e)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
  }

  private void btnCancelarActionPerformed(ActionEvent evt) {
    if (this.alteraTabela.booleanValue()) {
      if (Extra.Pergunta(this, "Descartar alterações não salvas?")) {
        this.arvoreMenu.setEnabled(true);
        arvoreMenuValueChanged(null);
        this.alteraTabela = Boolean.valueOf(false);
        this.arvoreMenu.setEnabled(true);
      }
    } else {
      this.arvoreMenu.setEnabled(true);
      arvoreMenuValueChanged(null);
      this.alteraTabela = Boolean.valueOf(false);
      this.arvoreMenu.setEnabled(true);
    }
  }

  private void addUsersMenuActionPerformed(ActionEvent evt) {
    this.alteraTabela = Boolean.valueOf(true);
    this.arvoreMenu.setEnabled(false);
    this.btnSalvar.setEnabled(this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu));
    this.btnCancelar.setEnabled(this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu));
    FI_CadFindUsuario user = (FI_CadFindUsuario)Util.execucaoDiretaClass("formularios.FI_CadFindUsuario", "Modulos-jSyscom.jar", new StringBuilder().append("Localiza Usuario(s) - ").append(Util.getSource(seqInstancias, FI_SysPermissoes.class)).toString(), new OpcoesTela(Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)), this.usuario);
    user.listaExport = this.listaUsuarios;
  }

  private void removeUserMenuActionPerformed(ActionEvent evt) {
    if (this.tabelaPermitidos.getSelectedRowCount() == 1) {
      if (Extra.Pergunta(this, "Confirma exclusão da permissão do usuário?")) {
        this.listaUsuarios.remove(this.tabelaPermitidos.getSelectedRow());
        this.alteraTabela = Boolean.valueOf(true);
        this.arvoreMenu.setEnabled(false);
        this.btnSalvar.setEnabled(this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu));
        this.btnCancelar.setEnabled(this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu));
      }
    } else if ((this.tabelaPermitidos.getSelectedRowCount() > 1) && 
      (Extra.Pergunta(this, "Confirma exclusão das permissões selecionadas?"))) {
      int[] array = this.tabelaPermitidos.getSelectedRows();
      ArrayList v = new ArrayList();
      for (int i : array) {
        v.add(this.listaUsuarios.get(i));
      }
      for (Usuario veiculo : v) {
        this.listaUsuarios.remove(veiculo);
      }
      this.alteraTabela = Boolean.valueOf(true);
      this.arvoreMenu.setEnabled(false);
      this.btnSalvar.setEnabled(this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu));
      this.btnCancelar.setEnabled(this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu));
    }
  }

  private void removeAllUserMenuActionPerformed(ActionEvent evt)
  {
    if (Extra.Pergunta(this, "Confirma exclusão de todas as permissões deste menu?")) {
      this.listaUsuarios.clear();
      this.alteraTabela = Boolean.valueOf(true);
      this.arvoreMenu.setEnabled(false);
      this.btnSalvar.setEnabled(this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu));
      this.btnCancelar.setEnabled(this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu));
    }
  }

  private boolean salvar(Integer cd_acao, Empresa empresa) {
    try {
      String SQL = "delete from restricoes where cd_empresa = ? and cd_acao = ?";
      Object[] par = { empresa.getCodigo(), cd_acao };
      DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());

      SQL = "insert into restricoes(cd_empresa, cd_usuario, cd_acao) values(?,?,?)";
      for (Usuario user : this.listaUsuarios) {
        Object[] par2 = { empresa.getCodigo(), user.getCodigoUsuario(), cd_acao };
        DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 1, Variaveis.getDbType());
      }

      return true;
    } catch (Exception e) {
      Variaveis.addNewLog(new StringBuilder().append(getClass().getName()).append(":\n").append(Utils.getStackTrace(e)).toString(), Boolean.valueOf(false), Boolean.valueOf(true), this);
    }
    return false;
  }

  private void btnSalvarActionPerformed(ActionEvent evt) {
    DefaultMutableTreeNode node = (DefaultMutableTreeNode)this.arvoreMenu.getLastSelectedPathComponent();
    Menu item = (Menu)node.getUserObject();
    if (item.getOperacao() != null)
      if (salvar(item.getOperacao(), Variaveis.getEmpresa())) {
        this.btnCancelar.setEnabled(false);
        this.btnSalvar.setEnabled(false);
        this.alteraTabela = Boolean.valueOf(false);
        this.arvoreMenu.setEnabled(true);
        Extra.Informacao(this, "Alterações aplicadas com sucesso!");
      } else {
        Extra.Informacao(this, "Erro ao aplicar alterações!");
      }
  }

  private void btnAddUserActionPerformed(ActionEvent evt)
  {
    addUsersMenuActionPerformed(evt);
  }

  private void pnlTabulacoesStateChanged(ChangeEvent evt) {
    this.listaUsuarios.clear();
    this.btnSalvar.setEnabled(false);
    this.btnCancelar.setEnabled(false);
    this.arvoreMenu.setEnabled(true);
    if (this.pnlTabulacoes.getSelectedComponent().equals(this.pnlMenu)) {
      this.tabelaPermitidos.setRowSelectionAllowed(true);
    } else if (this.pnlTabulacoes.getSelectedComponent().equals(this.pnlOperacoes)) {
      this.tabelaPermitidos.setRowSelectionAllowed(false);
      atualizaListaUserSys(Variaveis.getEmpresa());
      this.cboxOperacoes.setSelectedIndex(0);
    }
  }

  private void menuAtualizarActionPerformed(ActionEvent evt) {
    atualizaListaUserSys(Variaveis.getEmpresa());
  }

  private void btnAplicarActionPerformed(ActionEvent evt) {
    if (this.cboxOperacoes.getSelectedIndex() == 0) {
      if (this.cboxUsuario.getSelectedItem() != null)
      {
        Usuario destino;
        if (this.tabelaPermitidos.getRowCount() > 0) {
          destino = (Usuario)this.cboxUsuario.getSelectedItem();
          if (Extra.Pergunta(this, new StringBuilder().append("Confirma cópia das permissões para '").append(destino.getNomeUsuario()).append("' ?").toString())) {
            for (Usuario origem : this.listaUsuarios)
              if (UsuarioDao.copiarPermissoes(origem, destino))
                Extra.Informacao(this, "Permissões copiadas com sucesso!");
              else
                Extra.Informacao(this, new StringBuilder().append("Falha ao copiar algumas das permissões de '").append(origem.getNomeUsuario()).append("'!").toString());
          }
        }
        else
        {
          Extra.Informacao(this, "Selecione um usuário com as permissões à serem copiadas!");
        }
      } else {
        Extra.Informacao(this, "Selecione um usuário destino para as permissões!");
      }
    } else if (this.cboxOperacoes.getSelectedIndex() == 1) {
      Usuario destino = (Usuario)this.cboxUsuario.getSelectedItem();
      if (Extra.Pergunta(this, new StringBuilder().append("Confirma aplicação de todas as permissões do sistema para '").append(destino.getNomeUsuario()).append("' ?").toString())) {
        if (UsuarioDao.allRestrictionForUser(Variaveis.getEmpresa(), destino).booleanValue())
          Extra.Informacao(this, "Permissões aplicadas com sucesso!");
        else {
          Extra.Informacao(this, new StringBuilder().append("Falha ao aplicar permissões para '").append(destino.getNomeUsuario()).append("'!").toString());
        }
      }
    }
    else if ((this.cboxOperacoes.getSelectedIndex() == 2) && (!this.cboxClasse.getSelectedItem().toString().trim().equals(""))) {
      if (!this.cboxClasse.getSelectedItem().toString().trim().equals("")) {
        Usuario destino = (Usuario)this.cboxUsuario.getSelectedItem();
        if (MenuDao.alreadyClass(this.cboxClasse.getSelectedItem().toString().trim()).booleanValue()) {
          if (UsuarioDao.setPermition(destino, this.cboxClasse.getSelectedItem().toString().trim()).booleanValue())
            Extra.Informacao(this, "Permissão aplicada com sucesso!");
          else
            Extra.Informacao(this, new StringBuilder().append("Falha ao aplicar permissão para '").append(destino.getNomeUsuario()).append("'!").toString());
        }
        else {
          Integer indexAcao = MenuDao.cadClasse(this.cboxModulos.getSelectedItem().toString().trim(), this.cboxClasse.getSelectedItem().toString().trim(), "", new OpcoesTela());
          if (indexAcao != null) {
            if (UsuarioDao.setPermition(this.usuario, indexAcao).booleanValue())
              Extra.Informacao(this, "Permissão aplicada com sucesso!");
            else
              Extra.Informacao(this, new StringBuilder().append("Falha ao aplicar permissão para '").append(destino.getNomeUsuario()).append("'!").toString());
          }
          else
            Extra.Informacao(this, new StringBuilder().append("Falha ao aplicar permissão para '").append(destino.getNomeUsuario()).append("'!").toString());
        }
      }
      else {
        Extra.Informacao(this, "Selecione ou digite o endereço da tela");
      }
    }
  }

  private void cboxOperacoesItemStateChanged(ItemEvent evt) {
    if (this.cboxOperacoes.getSelectedIndex() == 2) {
      this.cboxClasse.removeAllItems();
      this.cboxClasse.addItem("");
      for (String item : MenuDao.getListClasses()) {
        this.cboxClasse.addItem(item);
      }
      this.cboxClasse.setSelectedIndex(0);
      this.cboxModulos.removeAllItems();
      for (String s : MenuDao.getModulos()) {
        this.cboxModulos.addItem(s);
      }

      this.cboxClasse.setEnabled(true);
      this.cboxClasse.setVisible(true);
      this.lblClasse.setVisible(true);
      this.cboxModulos.setEnabled(true);
      this.cboxModulos.setVisible(true);
      this.lblModulo.setVisible(true);
      this.cboxClasse.requestFocus();
    } else {
      this.cboxClasse.setEnabled(false);
      this.cboxClasse.setVisible(false);
      this.cboxModulos.setEnabled(false);
      this.cboxModulos.setVisible(false);
      this.lblClasse.setVisible(false);
      this.lblModulo.setVisible(false);
    }
  }

  private void cboxModulosActionPerformed(ActionEvent evt)
  {
  }
}

/* Location:           F:\Programacao\Java\jSyscom\DER\jSyscom\lib\jSyscom-Modulos.jar
 * Qualified Name:     formularios.FI_SysPermissoes
 * JD-Core Version:    0.6.2
 */