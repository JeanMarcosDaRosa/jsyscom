/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package incompletos;

import base.FrmPrincipal;
import beans.*;
import dao.ImagemDao;
import dao.ProdutoDao;
import formularios.*;
import funcoes.Util;
import java.awt.Image;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import utilitarios.*;

/**
 *
 * @author Jean
 */
public final class FI_CadOS extends javax.swing.JInternalFrame implements Observer {

    static Integer numInstancias = 0;
    private Integer numMaxInstancias = 65535;
    private final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private Integer seqInstancias = 0;
    boolean alterado = false;
    private Usuario usuario = null;
    private JFileChooser fc;
    private String nomeArq = null;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }

    public FI_CadOS(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title + (seqInstancias > 0 ? (" - [" + String.valueOf(seqInstancias) + "]") : ""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            ativaEdits(false);
            ativaButtons(true, true, false, false, false, false);
            imagemProd.addObserver(this);
            //Populo o combo de Locais dos Produtos
            loadLstLocalProd(ProdutoDao.getAllLocaisProdutos(Variaveis.getEmpresa()));
            //pnlTabulado.setEnabledAt(1, false);
            //pnlTabulado.setEnabledAt(2, false);
            //pnlTabulado.setEnabledAt(3, false);
            ordemServico.addObserver(this);
            tipoEquipamento.addObserver(this);
            localProd.addObserver(this);
            iconProd.setHorizontalAlignment(JLabel.CENTER);
            iconConexao.setHorizontalAlignment(JLabel.CENTER);
            limpaCampos();
        }
    }

    public void loadLstLocalProd(List<LocalProd> lista) {
        String selected = "";
        if (cboxLocal.getSelectedItem() != null) {
            selected = cboxLocal.getSelectedItem().toString();
        }
        cboxLocal.removeAllItems();
        cboxLocal.addItem("");
        for (LocalProd cp : lista) {
            cboxLocal.addItem(cp);
        }
        cboxLocal.setSelectedItem(selected);
    }

    public void setImagemProd() {
        if (imagemProd.getBlobImagem() != null) {
            if (imagemProd.getBlobImagem().getIconWidth() > 256) {
                imagemProd.setBlobImagem(new ImageIcon(imagemProd.getBlobImagem().getImage().getScaledInstance(256, -1, Image.SCALE_DEFAULT)));
            }
        }
        //produto.setImagemProduto(imagemProd);
        iconProd.setIcon(imagemProd.getBlobImagem());
    }

    public void setLocalizacao() {
        loadLstLocalProd(ProdutoDao.getAllLocaisProdutos(Variaveis.getEmpresa()));
        cboxLocal.setSelectedItem(localProd);
    }

    public void setProduto() {
//        try {
//            limpaCampos();
//            lblCodigo.setText(String.valueOf(produto.getCodProduto()));
//            edtDescricao.setText(produto.getDescricaoProduto());
//            if (produto.getDataCadastroProduto() != null) {
//                edtDataCad.setText(Format.DateToStr(produto.getDataCadastroProduto()));
//            }
//            edtPreco.setText(Format.FormataValor(Format.doubleToString(produto.getValorVendaProduto()), Variaveis.getDecimal()));
//            if (produto.getLocalProd() != null) {
//                cboxLocal.setSelectedItem(produto.getLocalProd());
//            }
//            cboxSituacao.setSelectedIndex(produto.getSituacaoProduto() ? 0 : 1);
//            listaItensProd.clear();
//            listaItensProd.addAll(produto.getEans());
//            listaItensServico.clear();
//            listaItensServico.addAll(produto.getFornecedores());
//            imagemProd.clearImagem();
//            imagemProd.setCodImagem(produto.getImagemProduto().getCodImagem());
//            imagemProd.setNomeImagem(produto.getImagemProduto().getNomeImagem());
//            imagemProd.setMd5Imagem(produto.getImagemProduto().getMd5Imagem());
//            imagemProd.setBlobImagem(produto.getImagemProduto().getBlobImagem());
//            setImagemProd();
//        } catch (Exception e) {
//            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
//        }
    }

    public void populaProduto() {
//        try {
//            Integer cd = null;
//            if (produto.getCodProduto() != null) {
//                cd = produto.getCodProduto();
//            }
//            produto.clearProduto();
//            produto.setCodProduto(cd);
//            produto.setCategoriaProd((CategoriaProd) cboxCategoria.getSelectedItem());
//            if (!Validador.soNumeros(edtDataCad.getText()).equals("")) {
//                produto.setDataCadastroProduto(Format.StrToDate(edtDataCad.getText()));
//            } else {
//                produto.setDataCadastroProduto(null);
//            }
//            produto.setDescricaoProduto(edtDescricao.getText().trim());
//            produto.setImagemProduto(imagemProd);
//            produto.setLocalProd((LocalProd) cboxLocal.getSelectedItem());
//            produto.setSituacaoProduto(cboxSituacao.getSelectedIndex() == 0);
//            produto.setValorVendaProduto(Format.stringToDouble(edtPreco.getText().trim()));
//        } catch (Exception e) {
//            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
//        }
    }

    private void cancelar() {
        if (alterado) {
            if (Extra.Pergunta(this, "Descartar alterações não salvas?")) {
                limpaCampos();
                ativaEdits(false);
                ativaButtons(true, true, false, false, false, false);
                alterado = false;
                ordemServico.clearOrdemServico();
                pnlTabulado.setEnabledAt(1, false);
                pnlTabulado.setEnabledAt(2, false);
                pnlTabulado.setEnabledAt(3, false);
            }
        } else {
            limpaCampos();
            ativaEdits(false);
            ativaButtons(true, true, false, false, false, false);
            alterado = false;
            ordemServico.clearOrdemServico();;
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
        }
    }

    private void ativaEdits(Boolean ativa) {
//        edtDescricao.setEnabled(ativa);
//        edtPreco.setEnabled(ativa);
//        cboxCategoria.setEnabled(ativa);
//        cboxLocal.setEnabled(ativa);
//        cboxSituacao.setEnabled(ativa);
//        btnCarregaImagemBD.setEnabled(ativa);
//        btnCarregaImagemPasta.setEnabled(ativa);
//        btnLimparImagem.setEnabled(ativa);
//        btnNovaCategoria.setEnabled(ativa);
//        btnNovaLocalizacao.setEnabled(ativa);
    }

    private void ativaButtons(Boolean buscar, Boolean novo, Boolean alterar,
            Boolean excluir, Boolean cancelar, Boolean salvar) {
        btnBuscarOS.setEnabled(buscar);
        btnCancOS.setEnabled(cancelar);
        btnExcluirOS.setEnabled(excluir);
        btnEditOS.setEnabled(alterar);
        btnNovaOS.setEnabled(novo);
        btnSaveOS.setEnabled(salvar);
    }

    private void limpaCampos() {
//        lblCodigo.setText("0");
//        edtDescricao.setText("");
//        edtPreco.setText("0,00");
//        edtDataCad.setText("");
//        if (cboxCategoria.getItemCount() > 0) {
//            cboxCategoria.setSelectedIndex(0);
//        }
//        if (cboxLocal.getItemCount() > 0) {
//            cboxLocal.setSelectedIndex(0);
//        }
//        if (cboxSituacao.getItemCount() > 0) {
//            cboxSituacao.setSelectedIndex(0);
//        }
//        limpaImagem();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        listaItensProd = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Produto>());
        listaItensServico = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Servico>());
        imagemProd = new beans.Imagem();
        localProd = new beans.LocalProd();
        tipoEquipamento = new beans.TipoEquipamento();
        ordemServico = new beans.OrdemServico();
        imagemConexao = new beans.Imagem();
        pnlTabulado = new javax.swing.JTabbedPane();
        pnlPrincipalOS = new javax.swing.JPanel();
        pnlInfoBasicas = new javax.swing.JPanel();
        lblDataNasc = new javax.swing.JLabel();
        edtDataCad = new javax.swing.JFormattedTextField();
        pnlCodPessoa = new javax.swing.JPanel();
        lblCodigo = new javax.swing.JLabel();
        pnlDCliente = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        edtCodCliente = new javax.swing.JTextField();
        btnBuscaCliente = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel22 = new javax.swing.JLabel();
        pnlAbasEquipamento = new javax.swing.JTabbedPane();
        pnlDEquipamento = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        edtNroSerie = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cboxTipoEquip = new javax.swing.JComboBox();
        btnNovoTipoEquip = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        edtModelo = new javax.swing.JTextField();
        edtMarca = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        edtNroRastreio = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        edtDataFbr = new javax.swing.JFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        edtEntrada = new javax.swing.JTextField();
        edtSaida = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        edtPotencia = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        edtTensao = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        iconConexao = new javax.swing.JLabel();
        btnLimparImagem = new javax.swing.JButton();
        btnCarregaImagemBD = new javax.swing.JButton();
        btnCarregaImagemPasta = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        cboxLocal = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        btnNovaLocalizacao = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jLabel25 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextArea4 = new javax.swing.JTextArea();
        jPanel6 = new javax.swing.JPanel();
        jTextField9 = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        edtDataCad2 = new javax.swing.JFormattedTextField();
        lblDataNasc2 = new javax.swing.JLabel();
        lblDataNasc3 = new javax.swing.JLabel();
        edtDataCad3 = new javax.swing.JFormattedTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jCheckBox2 = new javax.swing.JCheckBox();
        jLabel33 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jLabel37 = new javax.swing.JLabel();
        edtDataFbr1 = new javax.swing.JFormattedTextField();
        jComboBox3 = new javax.swing.JComboBox();
        jLabel26 = new javax.swing.JLabel();
        lblDataNasc1 = new javax.swing.JLabel();
        edtDataCad1 = new javax.swing.JFormattedTextField();
        pnlBotoes = new javax.swing.JPanel();
        btnSair = new javax.swing.JButton();
        btnBuscarOS = new javax.swing.JButton();
        btnNovaOS = new javax.swing.JButton();
        btnEditOS = new javax.swing.JButton();
        btnExcluirOS = new javax.swing.JButton();
        btnCancOS = new javax.swing.JButton();
        btnSaveOS = new javax.swing.JButton();
        pnlItensProd = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnLimpaListaItemProd = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelaItensProd = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        btnAddItemProd = new javax.swing.JButton();
        lblPrecoProd = new javax.swing.JLabel();
        iconProd = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        edtPotencia1 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        lblPrecoProd1 = new javax.swing.JLabel();
        pnlItensServ = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelaitensServico = new javax.swing.JTable();
        btnAddItemServ = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        edtDescServ = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        edtValorServ = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel30 = new javax.swing.JLabel();
        lblPrecoProd2 = new javax.swing.JLabel();
        pnlManutencao = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton9 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTextArea5 = new javax.swing.JTextArea();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel36 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTextArea6 = new javax.swing.JTextArea();
        pnlOrcamento = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        pnlPrincipalFornecedor = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        pnlInfoBasicas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblDataNasc.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lblDataNasc.setText("Data Cadastro:");

        try {
            edtDataCad.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        pnlCodPessoa.setBorder(javax.swing.BorderFactory.createTitledBorder("Nro. Doc"));
        pnlCodPessoa.setForeground(java.awt.Color.black);

        lblCodigo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblCodigo.setForeground(java.awt.Color.blue);
        lblCodigo.setText("0");

        javax.swing.GroupLayout pnlCodPessoaLayout = new javax.swing.GroupLayout(pnlCodPessoa);
        pnlCodPessoa.setLayout(pnlCodPessoaLayout);
        pnlCodPessoaLayout.setHorizontalGroup(
            pnlCodPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCodPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCodigo)
                .addContainerGap(69, Short.MAX_VALUE))
        );
        pnlCodPessoaLayout.setVerticalGroup(
            pnlCodPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCodPessoaLayout.createSequentialGroup()
                .addComponent(lblCodigo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlDCliente.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados do Cliente"));

        jLabel17.setText("Código:");

        edtCodCliente.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        edtCodCliente.setForeground(java.awt.Color.blue);

        btnBuscaCliente.setText("...");

        jLabel18.setText("Nome:");

        jLabel19.setText("CPF/CNPJ:");

        jLabel20.setText("Telefone:");

        jLabel21.setText("Endereço:");

        jLabel22.setText("Contato:");

        javax.swing.GroupLayout pnlDClienteLayout = new javax.swing.GroupLayout(pnlDCliente);
        pnlDCliente.setLayout(pnlDClienteLayout);
        pnlDClienteLayout.setHorizontalGroup(
            pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel21)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlDClienteLayout.createSequentialGroup()
                        .addComponent(edtCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscaCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel19, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlDClienteLayout.createSequentialGroup()
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField3))
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
        );
        pnlDClienteLayout.setVerticalGroup(
            pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(edtCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscaCliente)
                    .addComponent(jLabel18)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlDClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel22)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlDEquipamento.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel7.setText("Nro Série:");

        jLabel8.setText("Tipo de Equip:");

        btnNovoTipoEquip.setText("...");
        btnNovoTipoEquip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoTipoEquipActionPerformed(evt);
            }
        });

        jLabel9.setText("Modelo:");

        jLabel10.setText("Marca:");

        jLabel11.setText("Nro Rastreio:");

        jLabel12.setText("Data Fabricação:");

        try {
            edtDataFbr.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel13.setText("Entrada:");

        edtEntrada.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtEntrada.setForeground(new java.awt.Color(0, 0, 255));
        edtEntrada.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtEntrada.setText("0,00");
        edtEntrada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtEntradaKeyReleased(evt);
            }
        });

        edtSaida.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtSaida.setForeground(new java.awt.Color(0, 0, 255));
        edtSaida.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtSaida.setText("0,00");
        edtSaida.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtSaidaKeyReleased(evt);
            }
        });

        jLabel14.setText("Saída:");

        jLabel15.setText("Potencia Nominal:");

        edtPotencia.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtPotencia.setForeground(new java.awt.Color(0, 0, 255));
        edtPotencia.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtPotencia.setText("0,00");
        edtPotencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtPotenciaKeyReleased(evt);
            }
        });

        jLabel16.setText("Tensão Nominal:");

        edtTensao.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtTensao.setForeground(new java.awt.Color(0, 0, 255));
        edtTensao.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtTensao.setText("0,00");
        edtTensao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtTensaoKeyReleased(evt);
            }
        });

        jLabel23.setText("Descrição:");

        jTextArea2.setColumns(20);
        jTextArea2.setLineWrap(true);
        jTextArea2.setRows(5);
        jTextArea2.setWrapStyleWord(true);
        jTextArea2.setDragEnabled(true);
        jScrollPane1.setViewportView(jTextArea2);

        javax.swing.GroupLayout pnlDEquipamentoLayout = new javax.swing.GroupLayout(pnlDEquipamento);
        pnlDEquipamento.setLayout(pnlDEquipamentoLayout);
        pnlDEquipamentoLayout.setHorizontalGroup(
            pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel15)
                    .addComponent(jLabel8)
                    .addComponent(jLabel7)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                .addComponent(cboxTipoEquip, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnNovoTipoEquip, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                .addComponent(edtPotencia, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtTensao))
                            .addComponent(edtNroSerie))
                        .addGap(18, 18, 18)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                .addGap(173, 173, 173)
                                .addComponent(jLabel12))
                            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                        .addComponent(edtEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(30, 30, 30)
                                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)))
                                    .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(edtNroRastreio, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(edtModelo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(edtMarca, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(edtSaida, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(edtDataFbr, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(jScrollPane1))
                .addContainerGap(81, Short.MAX_VALUE))
        );

        pnlDEquipamentoLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {edtMarca, edtSaida});

        pnlDEquipamentoLayout.setVerticalGroup(
            pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7)
                                .addComponent(edtNroSerie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnNovoTipoEquip)
                            .addComponent(cboxTipoEquip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(edtPotencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16)
                            .addComponent(edtTensao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)
                            .addComponent(edtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                                .addComponent(edtNroRastreio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(edtSaida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(edtEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel14)
                                        .addComponent(jLabel13))))
                            .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel12)
                                .addComponent(edtDataFbr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDEquipamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDEquipamentoLayout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                .addContainerGap())
        );

        pnlAbasEquipamento.addTab("Dados do Equipamento", pnlDEquipamento);

        iconConexao.setBackground(new java.awt.Color(153, 204, 255));
        iconConexao.setForeground(new java.awt.Color(0, 102, 102));
        iconConexao.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        iconConexao.setText("Conexão Equipamento");
        iconConexao.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 51, 51)));

        btnLimparImagem.setText("Limpar");
        btnLimparImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparImagemActionPerformed(evt);
            }
        });

        btnCarregaImagemBD.setText("Imagens BD");
        btnCarregaImagemBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaImagemBDActionPerformed(evt);
            }
        });

        btnCarregaImagemPasta.setText("Imagens Pasta");
        btnCarregaImagemPasta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaImagemPastaActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel1.setText("Localização:");

        btnNovaLocalizacao.setText("...");
        btnNovaLocalizacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaLocalizacaoActionPerformed(evt);
            }
        });

        jLabel24.setText("Defeito Reclamado:");

        jTextArea3.setColumns(20);
        jTextArea3.setRows(5);
        jScrollPane5.setViewportView(jTextArea3);

        jLabel25.setText("Estado Equipamento:");

        jTextArea4.setColumns(20);
        jTextArea4.setRows(5);
        jScrollPane6.setViewportView(jTextArea4);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnLimparImagem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCarregaImagemBD)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCarregaImagemPasta))
                    .addComponent(iconConexao, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel24)
                    .addComponent(jLabel25)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cboxLocal, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNovaLocalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE)
                    .addComponent(jScrollPane6))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(iconConexao, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCarregaImagemPasta)
                            .addComponent(btnLimparImagem)
                            .addComponent(btnCarregaImagemBD)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cboxLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnNovaLocalizacao))
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel24)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pnlAbasEquipamento.addTab("Detalhes", jPanel1);

        jLabel34.setText("Nro OC Cliente:");

        try {
            edtDataCad2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        lblDataNasc2.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lblDataNasc2.setText("Data Entrada:");

        lblDataNasc3.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lblDataNasc3.setText("Data Saida:");

        try {
            edtDataCad3.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel34)
                    .addComponent(lblDataNasc2)
                    .addComponent(lblDataNasc3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtDataCad3, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtDataCad2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(746, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDataNasc2)
                    .addComponent(edtDataCad2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDataNasc3)
                    .addComponent(edtDataCad3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(135, Short.MAX_VALUE))
        );

        pnlAbasEquipamento.addTab("Extra", jPanel6);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações"));

        jLabel4.setText("Valor Bruto:");

        jTextField5.setForeground(java.awt.Color.blue);
        jTextField5.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTextField5.setText("0,00");

        jLabel31.setText("Margem do Cliente:");

        jTextField6.setForeground(java.awt.Color.blue);
        jTextField6.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTextField6.setText("0,00");

        jLabel32.setText("Desconto:");

        jTextField7.setForeground(java.awt.Color.blue);
        jTextField7.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTextField7.setText("0,00");

        jCheckBox2.setText("Aplicar desconto do cliente");

        jLabel33.setText("Valor Total:");

        jTextField8.setForeground(java.awt.Color.blue);
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTextField8.setText("0,00");

        jButton3.setText("Gerar Orçamento");

        jButton4.setText("Imprimir Ficha de Conserto");

        jButton5.setText("Finalizar Pagamento");

        jLabel37.setText("Data Autorização:");

        try {
            edtDataFbr1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel33)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField5)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel31)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel32)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jCheckBox2))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel37)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtDataFbr1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox2)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(jLabel37)
                    .addComponent(edtDataFbr1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton4)
                    .addComponent(jButton5))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ABERTO", "AGUARDANDO OC", "AGUARDANDO AUTH", "AGUARDANDO PGTO", "EM ANDAMENTO", "FINALIZADO" }));

        jLabel26.setText("Status OS:");

        lblDataNasc1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lblDataNasc1.setText("Data Autorização:");

        try {
            edtDataCad1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout pnlInfoBasicasLayout = new javax.swing.GroupLayout(pnlInfoBasicas);
        pnlInfoBasicas.setLayout(pnlInfoBasicasLayout);
        pnlInfoBasicasLayout.setHorizontalGroup(
            pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInfoBasicasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlAbasEquipamento, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlInfoBasicasLayout.createSequentialGroup()
                        .addComponent(pnlCodPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblDataNasc)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtDataCad, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblDataNasc1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtDataCad1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnlDCliente, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlInfoBasicasLayout.setVerticalGroup(
            pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                        .addComponent(pnlCodPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInfoBasicasLayout.createSequentialGroup()
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblDataNasc1)
                                .addComponent(edtDataCad1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblDataNasc)
                                .addComponent(edtDataCad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel26)
                                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16)))
                .addComponent(pnlDCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlAbasEquipamento, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnBuscarOS.setText("Buscar");
        btnBuscarOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarOSActionPerformed(evt);
            }
        });

        btnNovaOS.setText("Novo");
        btnNovaOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaOSActionPerformed(evt);
            }
        });

        btnEditOS.setText("Alterar");
        btnEditOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditOSActionPerformed(evt);
            }
        });

        btnExcluirOS.setText("Excluir");
        btnExcluirOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirOSActionPerformed(evt);
            }
        });

        btnCancOS.setText("Cancelar");
        btnCancOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancOSActionPerformed(evt);
            }
        });

        btnSaveOS.setText("Confirmar");
        btnSaveOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveOSActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBotoesLayout = new javax.swing.GroupLayout(pnlBotoes);
        pnlBotoes.setLayout(pnlBotoesLayout);
        pnlBotoesLayout.setHorizontalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBuscarOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNovaOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExcluirOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSaveOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlBotoesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBuscarOS, btnCancOS, btnEditOS, btnExcluirOS, btnNovaOS, btnSair, btnSaveOS});

        pnlBotoesLayout.setVerticalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addGroup(pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarOS)
                    .addComponent(btnNovaOS)
                    .addComponent(btnEditOS)
                    .addComponent(btnExcluirOS)
                    .addComponent(btnCancOS)
                    .addComponent(btnSaveOS)
                    .addComponent(btnSair))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlPrincipalOSLayout = new javax.swing.GroupLayout(pnlPrincipalOS);
        pnlPrincipalOS.setLayout(pnlPrincipalOSLayout);
        pnlPrincipalOSLayout.setHorizontalGroup(
            pnlPrincipalOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlInfoBasicas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlPrincipalOSLayout.setVerticalGroup(
            pnlPrincipalOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalOSLayout.createSequentialGroup()
                .addComponent(pnlInfoBasicas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlTabulado.addTab("Ordem de Serviço", pnlPrincipalOS);

        btnLimpaListaItemProd.setText("Limpar Lista");

        tabelaItensProd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tabelaItensProd);

        jLabel2.setText("Preço Unitario:");

        btnAddItemProd.setText("Adicionar Produto(s)");

        lblPrecoProd.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblPrecoProd.setForeground(java.awt.Color.blue);
        lblPrecoProd.setText("0,00");

        iconProd.setBackground(new java.awt.Color(153, 204, 255));
        iconProd.setForeground(new java.awt.Color(0, 102, 102));
        iconProd.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 51, 51)));

        jLabel27.setText("Produto:");

        jLabel28.setText("Qtde:");

        jButton2.setText("Adicionar à Lista");

        edtPotencia1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtPotencia1.setForeground(new java.awt.Color(0, 0, 255));
        edtPotencia1.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtPotencia1.setText("0,00");
        edtPotencia1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtPotencia1KeyReleased(evt);
            }
        });

        jLabel29.setText("Subtotal:");

        lblPrecoProd1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblPrecoProd1.setForeground(java.awt.Color.blue);
        lblPrecoProd1.setText("0,00");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnAddItemProd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimpaListaItemProd)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 765, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel28)
                                    .addComponent(jLabel27))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField4)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(edtPotencia1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton2)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel29)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(lblPrecoProd1))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(lblPrecoProd))))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(lblPrecoProd))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel29)
                            .addComponent(lblPrecoProd1))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel27)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel28)
                            .addComponent(jButton2)
                            .addComponent(edtPotencia1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 582, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddItemProd)
                    .addComponent(btnLimpaListaItemProd))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlItensProdLayout = new javax.swing.GroupLayout(pnlItensProd);
        pnlItensProd.setLayout(pnlItensProdLayout);
        pnlItensProdLayout.setHorizontalGroup(
            pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlItensProdLayout.setVerticalGroup(
            pnlItensProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pnlTabulado.addTab("Itens de Produto", pnlItensProd);

        tabelaitensServico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tabelaitensServico);

        btnAddItemServ.setText("...");

        jLabel3.setText("Descrição do Serviço:");

        edtDescServ.setEnabled(false);

        jLabel5.setText("Valor:");

        edtValorServ.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtValorServ.setForeground(java.awt.Color.blue);
        edtValorServ.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorServ.setText("0,00");

        jButton1.setText("Adicionar à lista");

        jLabel6.setText("Observações:");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane4.setViewportView(jTextArea1);

        jLabel30.setText("Subtotal:");

        lblPrecoProd2.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        lblPrecoProd2.setForeground(java.awt.Color.blue);
        lblPrecoProd2.setText("0,00");

        javax.swing.GroupLayout pnlItensServLayout = new javax.swing.GroupLayout(pnlItensServ);
        pnlItensServ.setLayout(pnlItensServLayout);
        pnlItensServLayout.setHorizontalGroup(
            pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlItensServLayout.createSequentialGroup()
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlItensServLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3))
                    .addGroup(pnlItensServLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel6))
                        .addGap(6, 6, 6)
                        .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlItensServLayout.createSequentialGroup()
                                .addComponent(edtDescServ)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAddItemServ, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtValorServ, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlItensServLayout.createSequentialGroup()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 772, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlItensServLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPrecoProd2)
                .addGap(85, 85, 85))
        );
        pnlItensServLayout.setVerticalGroup(
            pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlItensServLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(edtDescServ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(edtValorServ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddItemServ))
                .addGap(18, 18, 18)
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jButton1)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlItensServLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(lblPrecoProd2))
                .addGap(13, 13, 13))
        );

        pnlTabulado.addTab("Itens de Serviços", pnlItensServ);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Envolvidos"));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane9.setViewportView(jTable1);

        jButton9.setText("Adicionar Envolvido(s)");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jButton9)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton9)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Manutanção"));

        jLabel35.setText("Conserto Detalhado:");

        jTextArea5.setColumns(20);
        jTextArea5.setRows(5);
        jScrollPane7.setViewportView(jTextArea5);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel36.setText("Parecer:");

        jTextArea6.setColumns(20);
        jTextArea6.setRows(5);
        jScrollPane8.setViewportView(jTextArea6);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addComponent(jSeparator2))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlManutencaoLayout = new javax.swing.GroupLayout(pnlManutencao);
        pnlManutencao.setLayout(pnlManutencaoLayout);
        pnlManutencaoLayout.setHorizontalGroup(
            pnlManutencaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlManutencaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlManutencaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlManutencaoLayout.setVerticalGroup(
            pnlManutencaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlManutencaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlTabulado.addTab("Manutenção", pnlManutencao);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane10.setViewportView(jTable2);

        jButton6.setText("Imprimir Orçamento");

        jButton7.setText("Autorizar Orçamento");

        jButton8.setText("Finalizar Pagamento");

        javax.swing.GroupLayout pnlOrcamentoLayout = new javax.swing.GroupLayout(pnlOrcamento);
        pnlOrcamento.setLayout(pnlOrcamentoLayout);
        pnlOrcamentoLayout.setHorizontalGroup(
            pnlOrcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrcamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOrcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1071, Short.MAX_VALUE)
                    .addGroup(pnlOrcamentoLayout.createSequentialGroup()
                        .addComponent(jButton6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton8)))
                .addContainerGap())
        );
        pnlOrcamentoLayout.setVerticalGroup(
            pnlOrcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlOrcamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 660, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton6)
                    .addComponent(jButton7)
                    .addComponent(jButton8))
                .addContainerGap())
        );

        pnlTabulado.addTab("Orçamentos/PGTO", pnlOrcamento);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane11.setViewportView(jTable3);

        javax.swing.GroupLayout pnlPrincipalFornecedorLayout = new javax.swing.GroupLayout(pnlPrincipalFornecedor);
        pnlPrincipalFornecedor.setLayout(pnlPrincipalFornecedorLayout);
        pnlPrincipalFornecedorLayout.setHorizontalGroup(
            pnlPrincipalFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalFornecedorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 1071, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlPrincipalFornecedorLayout.setVerticalGroup(
            pnlPrincipalFornecedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPrincipalFornecedorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 696, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlTabulado.addTab("Ordems de Compra Pendentes", pnlPrincipalFornecedor);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlTabulado)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlTabulado)
        );

        pnlTabulado.getAccessibleContext().setAccessibleName("Ordem de Serviço");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        try {
            setClosed(true);
        } catch (PropertyVetoException e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
	}//GEN-LAST:event_btnSairActionPerformed

    private void btnCancOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancOSActionPerformed
        cancelar();
    }//GEN-LAST:event_btnCancOSActionPerformed

    public void setAtivoButtonBuscar(boolean ativar) {
        this.btnBuscarOS.setEnabled(ativar);
        this.btnBuscarOS.setVisible(ativar);
    }

    public void executaButtonEditar() {
        btnEditOSActionPerformed(null);
    }

    private void btnNovaOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaOSActionPerformed
//        cancelar();
//        ativaEdits(true);
//        ativaButtons(false, false, false, false, true, true);
//        alterado = true;
//        produto.clearProduto();
//        lblCodigo.setText(String.valueOf(NextDaos.nextGenerico(Variaveis.getEmpresa(), "produto")));
//        edtDescricao.requestFocus();
    }//GEN-LAST:event_btnNovaOSActionPerformed

    private void btnBuscarOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarOSActionPerformed
//        FI_CadFindProduto frmProduto = (FI_CadFindProduto) Util.execucaoDiretaClass("formularios.FI_CadFindProduto", "Modulos-jSyscom.jar", "Localiza Produto - " + Util.getSource(seqInstancias, FI_CadFindProduto.class), new OpcoesTela(true, true, true, true, true, false), usuario);
//        if (frmProduto != null) {
//            frmProduto.setAtivoButtonNovo(false);
//            frmProduto.produto = produto;
//        }
    }//GEN-LAST:event_btnBuscarOSActionPerformed

    private void btnEditOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditOSActionPerformed
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true);
        alterado = true;
        pnlTabulado.setEnabledAt(1, true);
        pnlTabulado.setEnabledAt(2, true);
        pnlTabulado.setEnabledAt(3, true);
    }//GEN-LAST:event_btnEditOSActionPerformed

    private Boolean salvar() {
//        if (edtDescricao.getText().trim().equals("")) {
//            Extra.Informacao(this, "Campo descrição é obrigatório!");
//            return false;
//        }
//        if (cboxCategoria.getSelectedItem().toString().trim().equals("")) {
//            Extra.Informacao(this, "Categoria é obrigatória!");
//            return false;
//        }
//        if (cboxLocal.getSelectedItem().toString().trim().equals("")) {
//            Extra.Informacao(this, "Local é obrigatório!");
//            return false;
//        }
//        if (cboxLocal.getSelectedItem().toString().trim().equals("")) {
//            Extra.Informacao(this, "Local é obrigatório!");
//            return false;
//        }
//        if (iconProd.getIcon() == null) {
//            Extra.Informacao(this, "Imagem é obrigatório!");
//            return false;
//        }
//        try {
//            populaProduto();
//            if (ProdutoDao.salvarProduto(produto, Variaveis.getEmpresa())) {
//                Imagem i = (Imagem)imagemProd.clone();
//                setProduto();
//                imagemProd = i;
//                setImagemProd();
//                return true;
//            }
//        } catch (Exception e) {
//            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
//        }
        return false;
    }

    private void btnSaveOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveOSActionPerformed
        if (imagemProd.getBlobImagem() != null) {
            if (imagemProd.getCodImagem() == null) {
                try {
                    Integer a = ProdutoDao.imagemProdInDB(Variaveis.getEmpresa(), imagemProd);
                    if (a != null) {
                        if (!Extra.Pergunta(this, "A imagem utilizada parece já existir no Banco de Dados, gostaria de salva-la mesmo assim?\n"
                                + "Para procura-la no banco de dados basta clicar em 'Imagem DB'")) {
                            return;
                        }
                    }
                    String qry = "INSERT INTO imagem_prod (cd_empresa, nome_imagem_prod, md5_imagem_prod, blob_imagem_prod) "
                            + "VALUES (" + Variaveis.getEmpresa().getCodigo() + ", '" + imagemProd.getNomeImagem() + "', '" + imagemProd.getMd5Imagem() + "', ?) RETURNING cd_imagem_prod";
                    imagemProd.setCodImagem(ImagemDao.setImagemBanco(qry, new File(nomeArq)));
                    //produto.setImagemProduto(imagemProd);
                } catch (IOException ex) {
                    Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
                }
            }
        }
        if (salvar()) {
            ativaEdits(false);
            ativaButtons(true, true, true, true, true, false);
            alterado = false;
            pnlTabulado.setEnabledAt(1, true);
            pnlTabulado.setEnabledAt(2, true);
            pnlTabulado.setEnabledAt(3, true);
            Extra.Informacao(this, "Salvo com sucesso! \n"
                    + "Agora pode adicionar o(s) EAN(s) e Fornecedor(es) do produto!");
        } else {
            Extra.Informacao(this, "Falha ao salvar produto!");
        }
    }//GEN-LAST:event_btnSaveOSActionPerformed

    private void btnExcluirOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirOSActionPerformed
//        if (produto.getCodProduto() != null) {
//            if (Extra.Pergunta(this, "Excluir registro atual?")) {
//                if (ProdutoDao.excluirProduto(produto, Variaveis.getEmpresa())) {
//                    alterado = false;
//                    cancelar();
//                    Extra.Informacao(this, "Registro apagado com sucesso!");
//                } else {
//                    Extra.Informacao(this, "Falha ao apagar registro!");
//                }
//            }
//        } else {
//            alterado = false;
//            cancelar();
//        }
    }//GEN-LAST:event_btnExcluirOSActionPerformed

    private void limpaImagemProd() {
        iconProd.setIcon(null);
    }

    private void limpaImagemConexao() {
        iconConexao.setIcon(null);
        nomeArq = null;
        if (fc != null) {
            fc.setSelectedFile(null);
        }
        if (imagemProd != null) {
            imagemProd.clearImagem();
        }
    }

    private void btnNovaLocalizacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaLocalizacaoActionPerformed
        FI_CadLocalizacao o = (FI_CadLocalizacao) Util.execucaoDiretaClass("formularios.FI_CadLocalizacao", "Modulos-jSyscom.jar", "Cadastro de Locais/Gôndolas - " + Util.getSource(seqInstancias, FI_CadLocalizacao.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.localProd = localProd;
        }
    }//GEN-LAST:event_btnNovaLocalizacaoActionPerformed

    private void btnNovoTipoEquipActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoTipoEquipActionPerformed
        FI_CadTipoEquip o = (FI_CadTipoEquip) Util.execucaoDiretaClass("formularios.FI_CadTipoEquip", "Modulos-jSyscom.jar", "Cadastro de Tipos de Equipamento - " + Util.getSource(seqInstancias, FI_CadTipoEquip.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (o != null) {
            o.tipoEquipamento = tipoEquipamento;
        }
    }//GEN-LAST:event_btnNovoTipoEquipActionPerformed

    private void btnLimparImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparImagemActionPerformed
        limpaImagemConexao();
    }//GEN-LAST:event_btnLimparImagemActionPerformed

    private void btnCarregaImagemBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaImagemBDActionPerformed
        FI_SysGaleryImages galery = (FI_SysGaleryImages) Util.execucaoDiretaClass("formularios.FI_SysGaleryImages", "Modulos-jSyscom.jar", "Localiza Imagens - " + Util.getSource(seqInstancias, FI_CadFindProduto.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (galery != null) {
            galery.setWidthMaxExport(256);
            galery.setSqlDefault("select cd_conexao_equip as cod, blob_conexao_equip as blob, md5_conexao_equip as md5, nome_conexao_equip as nome from conexao_equip where cd_empresa = " + Variaveis.getEmpresa().getCodigo());
            galery.exportImagem = imagemConexao;
        }
    }//GEN-LAST:event_btnCarregaImagemBDActionPerformed

    private void btnCarregaImagemPastaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaImagemPastaActionPerformed
        if (fc == null) {
            fc = new JFileChooser();
            fc.addChoosableFileFilter(new ImageFilter());
            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileView(new ImageFileView());
            fc.setAccessory(new ImagePreview(fc));
        }
        int returnVal = fc.showDialog(this, "Abrir");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                imagemConexao.clearImagem();
                imagemConexao.setBlobImagem((ImageIcon) Util.setImagen(fc.getSelectedFile().toString(), iconProd.getWidth(), -1));
                imagemConexao.setMd5Imagem(Arquivo.getMD5Checksum(fc.getSelectedFile().toString())); //Unable to load library 'BEMAFI32': libBEMAFI32.so: cannot open shared object file
                imagemConexao.setNomeImagem(fc.getSelectedFile().getName());
                nomeArq = fc.getSelectedFile().toString();
                Integer a = ProdutoDao.imagemProdInDB(Variaveis.getEmpresa(), imagemConexao);
                if (a != null) {
                    if (Extra.Pergunta(this, "Esta imagem parece já existir no Banco de Dados, gostaria de usar a que esta no banco?")) {
                        nomeArq = null;
                        Imagem i = ProdutoDao.getImagemProduto(Variaveis.getEmpresa(), new Imagem(a));
                        imagemConexao.clearImagem();
                        imagemConexao.setCodImagem(i.getCodImagem());
                        imagemConexao.setBlobImagem(i.getBlobImagem());
                        imagemConexao.setMd5Imagem(i.getMd5Imagem());
                        imagemConexao.setNomeImagem(i.getNomeImagem());
                    }
                }
                imagemConexao.setFlagPronto(true);
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        }
    }//GEN-LAST:event_btnCarregaImagemPastaActionPerformed

    private void edtEntradaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtEntradaKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtEntradaKeyReleased

    private void edtSaidaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtSaidaKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtSaidaKeyReleased

    private void edtPotenciaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtPotenciaKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtPotenciaKeyReleased

    private void edtTensaoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtTensaoKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtTensaoKeyReleased

    private void edtPotencia1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtPotencia1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_edtPotencia1KeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddItemProd;
    private javax.swing.JButton btnAddItemServ;
    private javax.swing.JButton btnBuscaCliente;
    private javax.swing.JButton btnBuscarOS;
    private javax.swing.JButton btnCancOS;
    private javax.swing.JButton btnCarregaImagemBD;
    private javax.swing.JButton btnCarregaImagemPasta;
    private javax.swing.JButton btnEditOS;
    private javax.swing.JButton btnExcluirOS;
    private javax.swing.JButton btnLimpaListaItemProd;
    private javax.swing.JButton btnLimparImagem;
    private javax.swing.JButton btnNovaLocalizacao;
    private javax.swing.JButton btnNovaOS;
    private javax.swing.JButton btnNovoTipoEquip;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSaveOS;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cboxLocal;
    private javax.swing.JComboBox cboxTipoEquip;
    private javax.swing.JTextField edtCodCliente;
    private javax.swing.JFormattedTextField edtDataCad;
    private javax.swing.JFormattedTextField edtDataCad1;
    private javax.swing.JFormattedTextField edtDataCad2;
    private javax.swing.JFormattedTextField edtDataCad3;
    private javax.swing.JFormattedTextField edtDataFbr;
    private javax.swing.JFormattedTextField edtDataFbr1;
    private javax.swing.JTextField edtDescServ;
    private javax.swing.JTextField edtEntrada;
    private javax.swing.JTextField edtMarca;
    private javax.swing.JTextField edtModelo;
    private javax.swing.JTextField edtNroRastreio;
    private javax.swing.JTextField edtNroSerie;
    private javax.swing.JTextField edtPotencia;
    private javax.swing.JTextField edtPotencia1;
    private javax.swing.JTextField edtSaida;
    private javax.swing.JTextField edtTensao;
    private javax.swing.JTextField edtValorServ;
    private javax.swing.JLabel iconConexao;
    private javax.swing.JLabel iconProd;
    private beans.Imagem imagemConexao;
    private beans.Imagem imagemProd;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextArea jTextArea4;
    private javax.swing.JTextArea jTextArea5;
    private javax.swing.JTextArea jTextArea6;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblDataNasc;
    private javax.swing.JLabel lblDataNasc1;
    private javax.swing.JLabel lblDataNasc2;
    private javax.swing.JLabel lblDataNasc3;
    private javax.swing.JLabel lblPrecoProd;
    private javax.swing.JLabel lblPrecoProd1;
    private javax.swing.JLabel lblPrecoProd2;
    private java.util.List<Produto> listaItensProd;
    private java.util.List<Servico> listaItensServico;
    private beans.LocalProd localProd;
    private beans.OrdemServico ordemServico;
    private javax.swing.JTabbedPane pnlAbasEquipamento;
    private javax.swing.JPanel pnlBotoes;
    private javax.swing.JPanel pnlCodPessoa;
    private javax.swing.JPanel pnlDCliente;
    private javax.swing.JPanel pnlDEquipamento;
    private javax.swing.JPanel pnlInfoBasicas;
    private javax.swing.JPanel pnlItensProd;
    private javax.swing.JPanel pnlItensServ;
    private javax.swing.JPanel pnlManutencao;
    private javax.swing.JPanel pnlOrcamento;
    private javax.swing.JPanel pnlPrincipalFornecedor;
    private javax.swing.JPanel pnlPrincipalOS;
    private javax.swing.JTabbedPane pnlTabulado;
    private javax.swing.JTable tabelaItensProd;
    private javax.swing.JTable tabelaitensServico;
    private beans.TipoEquipamento tipoEquipamento;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Produto) {
            limpaCampos();
            ativaEdits(false);
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pnlTabulado.setEnabledAt(3, false);
            ativaButtons(true, true, true, true, true, false);
            alterado = false;
            setProduto();
        } else if (o instanceof Imagem) {
            setImagemProd();
        } else if (o instanceof LocalProd) {
            setLocalizacao();
        }
    }
}
