/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import base.FrmPrincipal;
import beans.Empresa;
import beans.Imagem;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import dao.ImagemDao;
import funcoes.Util;
import java.awt.Image;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import utilitarios.Arquivo;
import utilitarios.DataBase;
import utilitarios.Extra;
import utilitarios.ImageFileView;
import utilitarios.ImageFilter;
import utilitarios.ImagePreview;
import utilitarios.SaveAsPng;
import utilitarios.Utils;

/**
 *
 * @author Jean
 */
public final class FI_SysConfig extends javax.swing.JInternalFrame implements Observer {//

    static Integer numInstancias = 0;
    private Integer numMaxInstancias = 1;
    private final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private Integer seqInstancias = 0;
    private Usuario usuario = null;
    private JFileChooser fc;
    private String nomeArq = null;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }

    /**
     * Creates new form FI_Config
     */
    public FI_SysConfig(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title + (seqInstancias > 0 ? (" - [" + String.valueOf(seqInstancias) + "]") : ""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            loadConfig(usuario);
            imagemProd.addObserver(this);
            iconProd.setHorizontalAlignment(JLabel.CENTER);
        }
    }

    public void loadConfig(Usuario usuario) {
        try {
            String SQL = "select * from lookandfeels order by cd_lookandfeels";
            ResultSet rs = DataBase.executeQuery(SQL, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                cbFeel.addItem(rs.getString("nome_lookandfeels").trim());
            }
            if (comConfig(Variaveis.getEmpresa(), usuario)) {
                Object[] par = {Variaveis.getEmpresa().getCodigo(), usuario.getCodigoUsuario()};
                SQL = "select l.nome_lookandfeels as LOOK_NOME, c.* from lookandfeels as l, config_usuario as c where "
                        + "l.cd_lookandfeels = c.cd_lookandfeels and "
                        + "c.cd_empresa = ? and c.cd_usuario = ?";
                ResultSet res = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                if (res.next()) {
                    cbFeel.setSelectedItem(res.getString("LOOK_NOME").trim());
                    spinDistancia.setValue(res.getInt("distancia_frame_config"));
                    //edtArqFundo.setText(Variaveis.getPathImage());
                    edtMP.setText(res.getString("nome_menu_config"));
                    if (res.getInt("cd_wallpaper") > 0) {
                        Imagem i = ImagemDao.getWallpaper(res.getInt("cd_wallpaper"));
                        imagemProd.clearImagem();
                        imagemProd.setCodImagem(i.getCodImagem());
                        imagemProd.setBlobImagem(i.getBlobImagem());
                        imagemProd.setMd5Imagem(i.getMd5Imagem());
                        imagemProd.setNomeImagem(i.getNomeImagem());

                    }
                    setImagemProd();
                }
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }

    public boolean comConfig(Empresa empresa, Usuario usuario) {
        try {
            String SQL = "select * from config_usuario where cd_empresa = ? and cd_usuario = ?";
            Object[] par = {empresa.getCodigo(), usuario.getCodigoUsuario()};
            ResultSet res = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (res.next()) {
                return true;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
        return false;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        imagemProd = new beans.Imagem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cbFeel = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        spinDistancia = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        edtMP = new javax.swing.JTextField();
        btnOK = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnCarregaImagemPasta = new javax.swing.JButton();
        btnCarregaImagemBD = new javax.swing.JButton();
        btnLimparImagem = new javax.swing.JButton();
        iconProd = new javax.swing.JLabel();

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jLabel1.setText("Look and feel:");

        cbFeel.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbFeelItemStateChanged(evt);
            }
        });
        cbFeel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbFeelActionPerformed(evt);
            }
        });

        jLabel2.setText("Distancia Frames:");

        spinDistancia.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));

        jLabel3.setText("Raiz Menu Principal:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(spinDistancia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtMP))
                    .addComponent(cbFeel, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cbFeel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(edtMP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinDistancia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnOK.setText("Confirmar");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnCarregaImagemPasta.setText("Imagens Pasta");
        btnCarregaImagemPasta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaImagemPastaActionPerformed(evt);
            }
        });

        btnCarregaImagemBD.setText("Imagens BD");
        btnCarregaImagemBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCarregaImagemBDActionPerformed(evt);
            }
        });

        btnLimparImagem.setText("Limpar");
        btnLimparImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparImagemActionPerformed(evt);
            }
        });

        iconProd.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        iconProd.setMaximumSize(new java.awt.Dimension(800, 600));
        iconProd.setMinimumSize(new java.awt.Dimension(800, 600));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancelar))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnLimparImagem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCarregaImagemBD)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCarregaImagemPasta))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 496, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(iconProd, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCarregaImagemPasta)
                    .addComponent(btnLimparImagem)
                    .addComponent(btnCarregaImagemBD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar)
                    .addComponent(btnOK))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        //loadConfig(usuario);
        //applyConfig();
        try {
            setClosed(true);
        } catch (PropertyVetoException e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void applyConfig() {
        try {
            Variaveis.setDistanciaFrames((Integer) spinDistancia.getValue());
            Variaveis.setNomeMenu(edtMP.getText());
            try {
                String SQL = "SELECT * FROM lookandfeels FULL OUTER JOIN modulos USING(cd_modulos) WHERE lookandfeels.nome_lookandfeels = ?";
                Object[] par = {cbFeel.getSelectedItem().toString().trim()};
                ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                if (rs.next()) {
                    if (rs.getString("nome_modulos") != null && !"".equals(rs.getString("nome_modulos").trim())) {
                        Util.lookandfeel(cbFeel.getSelectedItem().toString(), rs.getString("nome_modulos").trim(), princ);
                    } else {
                        Util.lookandfeel(cbFeel.getSelectedItem().toString(), princ);
                    }
                }
            } catch (Exception e) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
            }
            Variaveis.getFormularioPrincipal().atualizaMenu();
            Variaveis.setIDImage(imagemProd.getCodImagem());
            if (imagemProd.getBlobImagem() != null) {
                Variaveis.getFormularioPrincipal().setWallpaper(SaveAsPng.getBufferedImageFromImage(imagemProd.getBlobImagem().getImage()));
            } else {
                Variaveis.getFormularioPrincipal().setWallpaper(null);
            }

        } catch (IOException ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }

    private void saveConfig(Usuario usuario) {
        try {
            Integer codWall = null;
            if (imagemProd.getBlobImagem() != null) {
                if (imagemProd.getCodImagem() != null) {
                    String qry = "UPDATE config_usuario SET cd_wallpaper = ? where cd_empresa = ? and cd_usuario = ?";
                    Object[] par = {imagemProd.getCodImagem(), Variaveis.getEmpresa().getCodigo(), usuario.getCodigoUsuario()};
                    DataBase.executeQuery(qry, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                    codWall = imagemProd.getCodImagem();
                } else {
                    if (ImagemDao.wallpaperInDB(Variaveis.getEmpresa(), imagemProd)!=null) {
                        if (!Extra.Pergunta(this, "Este Wallpaper parece já existir no Banco de Dados, gostaria de salva-lo mesmo assim?\n"
                                + "Para procura-lo no banco de dados basta clicar em 'Imagem DB'")) {
                            return;
                        }
                    }
                    String qry = "INSERT INTO wallpaper (cd_empresa, nome_wallpaper, md5_wallpaper, blob_wallpaper) "
                            + "VALUES (" + Variaveis.getEmpresa().getCodigo() + ", '" + imagemProd.getNomeImagem() + "', '" + imagemProd.getMd5Imagem() + "', ?) RETURNING cd_wallpaper";
                    codWall = ImagemDao.setImagemBanco(qry, new File(nomeArq));
                }
            }
            String gambi = "null";
            if (codWall != null) {
                gambi = String.valueOf(codWall);
            }
            if (comConfig(Variaveis.getEmpresa(), usuario)) {
                String SQL = "UPDATE config_usuario SET cd_lookandfeels = (select cd_lookandfeels from lookandfeels where "
                        + "nome_lookandfeels like (?)), cd_wallpaper = " + gambi + ", nome_menu_config = ?, distancia_frame_config = ? "
                        + "where cd_empresa = ? and cd_usuario = ?";
                Object[] par = {cbFeel.getSelectedItem().toString().trim(), edtMP.getText(), (Integer) spinDistancia.getValue(), Variaveis.getEmpresa().getCodigo(), usuario.getCodigoUsuario()};
                DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
            } else {
                String SQL = "INSERT INTO config (cd_empresa, cd_usuario, cd_lookandfeels, nome_menu_config, distancia_frame_config, cd_wallpaper) "
                        + "values(?, ?, (select cd_lookandfeels from lookandfeels where nome_lookandfeels = ?), ?, ?, " + gambi + ")";
                Object[] par = {Variaveis.getEmpresa().getCodigo(), usuario.getCodigoUsuario(), cbFeel.getSelectedItem().toString().trim(), edtMP.getText(), (Integer) spinDistancia.getValue(), codWall};
                DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
            }
            Extra.Informacao(this, "Configurações salvas com Sucesso!");
            applyConfig();
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
            Extra.Informacao(this, "Erro ao salvar Configurações!");
        }
    }

    private void cbFeelItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbFeelItemStateChanged
    }//GEN-LAST:event_cbFeelItemStateChanged

    private void cbFeelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbFeelActionPerformed
    }//GEN-LAST:event_cbFeelActionPerformed

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        if (Extra.Pergunta(this, "Salvar novas configurações?\n"
                + "Recomendado reiniciar o jSyscom em caso de alterações do Look And Feel!")) {
            saveConfig(usuario);
        }
    }//GEN-LAST:event_btnOKActionPerformed

    private void btnCarregaImagemPastaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaImagemPastaActionPerformed
        if (fc == null) {
            fc = new JFileChooser();
            fc.addChoosableFileFilter(new ImageFilter());
            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileView(new ImageFileView());
            fc.setAccessory(new ImagePreview(fc));
        }
        int returnVal = fc.showDialog(this, "Abrir");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                imagemProd.clearImagem();
                imagemProd.setBlobImagem((ImageIcon) Util.setImagen(fc.getSelectedFile().toString(), -1, -1));
                imagemProd.setMd5Imagem(Arquivo.getMD5Checksum(fc.getSelectedFile().toString())); //Unable to load library 'BEMAFI32': libBEMAFI32.so: cannot open shared object file
                imagemProd.setNomeImagem(fc.getSelectedFile().getName());
                nomeArq = fc.getSelectedFile().toString();
                Integer a = ImagemDao.wallpaperInDB(Variaveis.getEmpresa(), imagemProd);
                if (a!=null) {
                    if (Extra.Pergunta(this, "Este Wallpaper parece já existir no Banco de Dados, gostaria de usar o que esta no banco?")) {
                        nomeArq = null;
                        Imagem i = ImagemDao.getWallpaper(a);
                        imagemProd.clearImagem();
                        imagemProd.setCodImagem(i.getCodImagem());
                        imagemProd.setBlobImagem(i.getBlobImagem());
                        imagemProd.setMd5Imagem(i.getMd5Imagem());
                        imagemProd.setNomeImagem(i.getNomeImagem());
                    }
                }
                setImagemProd();
                //imagemProd.setFlagPronto(true);
                //iconProd.setIcon();
            } catch (Exception ex) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
            }
        }
    }//GEN-LAST:event_btnCarregaImagemPastaActionPerformed

    private void btnCarregaImagemBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCarregaImagemBDActionPerformed
        FI_SysGaleryImages galery = (FI_SysGaleryImages) Util.execucaoDiretaClass("formularios.FI_SysGaleryImages", "Modulos-jSyscom.jar", "Localiza Imagens - " + Util.getSource(seqInstancias, FI_SysConfig.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (galery != null) {
            galery.setWidthMaxExport(0);
            galery.setSqlDefault("select cd_wallpaper as cod, blob_wallpaper as blob, md5_wallpaper as md5, nome_wallpaper as nome from wallpaper where cd_empresa = " + Variaveis.getEmpresa().getCodigo());
            galery.exportImagem = imagemProd;
        }
    }//GEN-LAST:event_btnCarregaImagemBDActionPerformed

    private void limpaImagem() {
        iconProd.setIcon(null);
        nomeArq = null;
        if (fc != null) {
            fc.setSelectedFile(null);
        }
        if (imagemProd != null) {
            imagemProd.clearImagem();
        }
    }

    public void setImagemProd() {
        if (imagemProd.getBlobImagem() != null) {
            iconProd.setIcon(new ImageIcon(imagemProd.getBlobImagem().getImage().getScaledInstance(iconProd.getWidth(), -1, Image.SCALE_DEFAULT)));
        } else {
            iconProd.setIcon(null);
        }
    }

    private void btnLimparImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparImagemActionPerformed
        limpaImagem();
    }//GEN-LAST:event_btnLimparImagemActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCarregaImagemBD;
    private javax.swing.JButton btnCarregaImagemPasta;
    private javax.swing.JButton btnLimparImagem;
    private javax.swing.JButton btnOK;
    private javax.swing.JComboBox cbFeel;
    private javax.swing.JTextField edtMP;
    private javax.swing.JLabel iconProd;
    private beans.Imagem imagemProd;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSpinner spinDistancia;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Imagem) {
            setImagemProd();
        }
    }
}
