/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package formularios;

import base.FrmPrincipal;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import funcoes.OperacoesDB;
import java.beans.PropertyVetoException;
import utilitarios.DataBase;
import utilitarios.Format;
import utilitarios.Mem;
import utilitarios.Utils;

/**
 *
 * @author Jean
 */
public final class FI_SysAbout extends javax.swing.JInternalFrame {

    static Integer numInstancias = 0;
    private Integer numMaxInstancias = 1;
    private final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private Integer seqInstancias = 0;
    private Usuario usuario = null;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if(i>-1){
            seqInstancias += 1;
        }
    }

    /** Creates new form FI_SysAbout */
    public FI_SysAbout(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title+(seqInstancias>0?(" - ["+String.valueOf(seqInstancias)+"]"):""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            setSobre();
        }
    }

    private void setSobre() {
        String db = "";
        if (Variaveis.getDbType().equals(DataBase.FIREBIRD)) {
            db = "Firebird";
        } else if (Variaveis.getDbType().equals(DataBase.MYSQL)) {
            db = "MySQL";
        } else if (Variaveis.getDbType().equals(DataBase.POSTGRES)) {
            db = "PostgreSQL";
        }
        edtSobre.setText(
                "\tjSyscom " + "\n\n"
                + "Versão Atual: " + Variaveis.getVersao() + "\n"
                + "Programador: Jean Marcos da Rosa\n"
                + "Telefone: (54)9105-3771\n"
                + "E-mail: jeancaraca@hotmail.com\n"
                + "\n"
                + "\tDesenvolvimento:\n\n"
                + "Linguagem: Java (OpenJDK 7u5)\n"
                + "IDE: Netbeans 7.2\n"
                + "Banco de Dados: " + db );
    
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jFundo = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        edtSobre = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        cboxInfoType = new javax.swing.JComboBox();

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jFundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/tela.png"))); // NOI18N
        jFundo.setMaximumSize(new java.awt.Dimension(800, 600));
        jFundo.setMinimumSize(new java.awt.Dimension(800, 600));

        edtSobre.setBackground(new java.awt.Color(204, 204, 255));
        edtSobre.setColumns(20);
        edtSobre.setEditable(false);
        edtSobre.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        edtSobre.setForeground(new java.awt.Color(0, 0, 51));
        edtSobre.setRows(5);
        jScrollPane1.setViewportView(edtSobre);

        jButton1.setText("Sair");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        cboxInfoType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "jSyscom", "Status" }));
        cboxInfoType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboxInfoTypeItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jFundo, javax.swing.GroupLayout.PREFERRED_SIZE, 92, Short.MAX_VALUE)
                    .addComponent(cboxInfoType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jFundo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboxInfoType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 259, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        try {
            setClosed(true);
        } catch (PropertyVetoException e) {
            Variaveis.addNewLog(this.getClass().getName()+":\n"+Utils.getStackTrace(e), false, true, this);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cboxInfoTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboxInfoTypeItemStateChanged
        if(cboxInfoType.getSelectedItem().equals("jSyscom")){
            setSobre();
        }else if(cboxInfoType.getSelectedItem().equals("Status")){
            setInfoSO();
        }
    }//GEN-LAST:event_cboxInfoTypeItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cboxInfoType;
    private javax.swing.JTextArea edtSobre;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jFundo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    private void setInfoSO() {
        edtSobre.setText("\tStatus:\n\n"
                + "Ip do Servidor: "+Variaveis.getIpServ()+"\n"
                + "Nome Banco de Dados: "+Variaveis.getDbLocal()+"\n"
                + "\n"
                + "\tLicença de Uso:\n\n"
                + "Data de expiração: "+Format.DateToStr(OperacoesDB.getDataExpLic(Variaveis.getEmpresa()))+"\n"
                + "Dias Restantes: "+String.valueOf(OperacoesDB.getDiasRestantesLic(Variaveis.getEmpresa()))
                +"\n\n"
                +"\tMemória Alocada:\n"+Mem.getReport());
    }
}
