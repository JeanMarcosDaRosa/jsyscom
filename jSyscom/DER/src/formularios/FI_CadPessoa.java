/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import base.FrmPrincipal;
import beans.*;
import dao.PessoaDao;
import funcoes.Util;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTextField;
import utilitarios.DataBase;
import utilitarios.Extra;
import utilitarios.Format;
import utilitarios.Utils;
import utilitarios.Validador;

/**
 *
 * @author Jean
 */
public final class FI_CadPessoa extends javax.swing.JInternalFrame implements Observer {

    static Integer numInstancias = 0;
    private Integer numMaxInstancias = 65535;
    private final String versao = "v0.02";
    private FrmPrincipal princ = null;
    private Integer seqInstancias = 0;
    int cont = 0;
    boolean alterado = false;
    private Usuario usuario = null;
    private Integer tmpCdLogradouro = 0;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }

    public FI_CadPessoa(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title + (seqInstancias > 0 ? (" - [" + String.valueOf(seqInstancias) + "]") : ""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            limpaCampos();
            ativaEdits(false);
            edtComplemento.setEnabled(false);
            edtNumCasa.setEnabled(false);
            ativaButtons(true, true, false, false, false, false, false);
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            pessoa.addObserver(this);
            endereco.addObserver(this);
            lblMargemCliente.setVisible(false);
            edtDescontoCliente.setVisible(false);
            lblValorHoraFunc.setVisible(false);
            edtValorHoraFunc.setVisible(false);
        }
    }

    public void setPessoa() {
        try {
            buscaCargos();
            edtNome.setText(pessoa.getNomePessoa());
            edtDataNasc.setText(Format.DateToStr(pessoa.getDataNascimento()));
            edtDataCad.setText(Format.DateToStr(pessoa.getDataCadPessoa()));
            edtDescricao.setText(pessoa.getDescricaoPessoa());
            edtEmail.setText(pessoa.getEmailPessoa());
            edtSite.setText(pessoa.getSitePessoa());
            edtTF.setText(pessoa.getTelefoneFixoPessoa());
            edtTCell.setText(pessoa.getTelefoneCellPessoa());
            edtCPFCNPJ.setText(pessoa.getCpfCnpjPessoa());
            edtRGIE.setText(pessoa.getRgIePessoa());
            if (pessoa.getTipoPessoa() == 'F') {
                rPF.setSelected(true);
                edtDataExp.setText(Format.DateToStr(pessoa.getDataExpRgPessoa()));
                edtOrgao.setText(pessoa.getOrgaoRgPessoa());
                cbProfissao.setSelectedItem(pessoa.getProfissaoPessoa().trim());
                edtEmpresa.setText(pessoa.getEmpresaPessoa());
                edtFoneEmpresa.setText(pessoa.getFoneEmpresaPessoa());
                if (pessoa.getSexoPessoa() == 'M') {
                    cbSexo.setSelectedIndex(0);
                } else if (pessoa.getSexoPessoa() == 'F') {
                    cbSexo.setSelectedIndex(1);
                }
            } else if (pessoa.getTipoPessoa() == 'J') {
                rPJ.setSelected(true);
            }
            cbCliente.setSelected(pessoa.isCliente());
            edtDescontoCliente.setText(Format.FormataValor(Format.doubleToString(pessoa.getDescontoCliente()), Variaveis.getDecimal()));
            cbFornecedor.setSelected(pessoa.isFornecedor());
            cbFuncionario.setSelected(pessoa.isFuncionario());
            if (pessoa.getFuncionario() != null) {
                cbProfissao.setSelectedItem(pessoa.getProfissaoPessoa());
            }
            btnEditPessoa.requestFocus();
            listaEnderecos.clear();
            listaContatos.clear();
            listaEnderecos.addAll(pessoa.getEnderecos());
            listaContatos.addAll(pessoa.getContatos());
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }

    public void populaPessoa() {
        try {
            Integer cd = null;
            if (pessoa.getCdPessoa() != null) {
                cd = pessoa.getCdPessoa();
            }
            pessoa.clearPessoa();
            pessoa.setCdPessoa(cd);
            pessoa.setNomePessoa(edtNome.getText());
            pessoa.setDataNascimento(Format.StrToDate(edtDataNasc.getText()));
            pessoa.setDataCadPessoa(Format.StrToDate(edtDataCad.getText()));
            pessoa.setDescricaoPessoa(edtDescricao.getText());
            pessoa.setEmailPessoa(edtEmail.getText());
            pessoa.setSitePessoa(edtSite.getText());
            pessoa.setTelefoneFixoPessoa(edtTF.getText());
            pessoa.setTelefoneCellPessoa(edtTCell.getText());
            pessoa.setCpfCnpjPessoa(edtCPFCNPJ.getText());
            pessoa.setRgIePessoa(edtRGIE.getText());
            if (cbCliente.isSelected()) {
                pessoa.setCliente(0);
                pessoa.setDescontoCliente(Format.stringToDouble(edtDescontoCliente.getText().trim()));
            } else {
                pessoa.setCliente(null);
            }
            if (cbFornecedor.isSelected()) {
                pessoa.setFornecedor(0);
            } else {
                pessoa.setFornecedor(null);
            }
            if (cbFuncionario.isSelected()) {
                pessoa.setFuncionario(0);
                pessoa.setValorHoraFuncionario(Format.stringToDouble(edtValorHoraFunc.getText().trim()));
            } else {
                pessoa.setFuncionario(null);
            }
            if (rPF.isSelected()) {
                pessoa.setTipoPessoa('F');
                pessoa.setDataExpRgPessoa(Format.StrToDate(edtDataExp.getText()));
                pessoa.setOrgaoRgPessoa(edtOrgao.getText());
                pessoa.setProfissaoPessoa(cbProfissao.getSelectedItem().toString());
                pessoa.setEmpresaPessoa(edtEmpresa.getText());
                pessoa.setFoneEmpresaPessoa(edtFoneEmpresa.getText());
                if (cbSexo.getSelectedIndex() == 0) {
                    pessoa.setSexoPessoa('M');
                } else if (cbSexo.getSelectedIndex() == 1) {
                    pessoa.setSexoPessoa('F');
                }
            } else if (rPJ.isSelected()) {
                pessoa.setTipoPessoa('J');
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }

    public void limpaEdtsEndereco() {
        edtPais.setText("");
        edtUF.setText("");
        edtCidade.setText("");
        edtBairro.setText("");
        edtLogradouro.setText("");
        edtComplemento.setText("");
        edtNumCasa.setText("");
        edtCEP.setText("");
        cboxPrincipal.setSelected(false);
    }

    public void setEndereco(Endereco e) {
        //Limpa os campos
        limpaEdtsEndereco();
        //Seta os valores
        edtPais.setText(e.getNomePais());
        edtUF.setText(e.getSiglaUf());
        edtCidade.setText(e.getNomeCidade());
        edtBairro.setText(e.getNomeBairro());
        edtLogradouro.setText(e.getNomeLogradouro());
        edtCEP.setText(e.getCepLogradouro());
        tmpCdLogradouro = e.getCodLogradouro();
        cboxPrincipal.setSelected(e.getPrincipal());
        edtNumCasa.setText(String.valueOf(e.getNumero()));
        edtComplemento.setText(e.getComplemento());
        btnAssociar.setEnabled(true);
    }

    private void cancelar() {
        if (alterado) {
            if (Extra.Pergunta(this, "Descartar alterações não salvas?")) {
                limpaCampos();
                ativaEdits(false);
                edtComplemento.setEnabled(false);
                edtNumCasa.setEnabled(false);
                ativaButtons(true, true, false, false, false, false, false);
                alterado = false;
                pessoa.clearPessoa();
                endereco.clearEndereco();
                pnlTabulado.setEnabledAt(1, false);
                pnlTabulado.setEnabledAt(2, false);
            }
        } else {
            limpaCampos();
            ativaEdits(false);
            edtComplemento.setEnabled(false);
            edtNumCasa.setEnabled(false);
            ativaButtons(true, true, false, false, false, false, false);
            alterado = false;
            pessoa.clearPessoa();
            endereco.clearEndereco();
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
        }
    }

    private void ativaEdits(Boolean ativa) {
        edtNome.setEnabled(ativa);
        edtEmail.setEnabled(ativa);
        edtSite.setEnabled(ativa);
        edtTF.setEnabled(ativa);
        edtTCell.setEnabled(ativa);
        edtDescricao.setEnabled(ativa);
        edtDataNasc.setEnabled(ativa);
        edtCPFCNPJ.setEnabled(ativa);
        edtRGIE.setEnabled(ativa);
        cbProfissao.setEnabled(ativa);
        edtOrgao.setEnabled(ativa);
        edtEmpresa.setEnabled(ativa);
        edtDataExp.setEnabled(ativa);
        edtFoneEmpresa.setEnabled(ativa);
        cbSexo.setEnabled(ativa);
        rPF.setEnabled(ativa);
        rPJ.setEnabled(ativa);
        cbCliente.setEnabled(ativa);
        cbFornecedor.setEnabled(ativa);
        cbFuncionario.setEnabled(ativa);
    }

    private void ativaButtons(Boolean buscar, Boolean novo, Boolean alterar,
            Boolean excluir, Boolean cancelar, Boolean salvar, Boolean buscarCep) {
        btnBuscarPessoa.setEnabled(buscar);
        btnCancPessoa.setEnabled(cancelar);
        btnDelPessoa.setEnabled(excluir);
        btnEditPessoa.setEnabled(alterar);
        btnNovoPessoa.setEnabled(novo);
        btnSavePessoa.setEnabled(salvar);
        btnBuscaCep.setEnabled(buscarCep);
    }

    private void limpaCampos() {
        rPF.setSelected(true);
        //lblCodigo.setText("");
        edtNome.setText("");
        edtEmail.setText("");
        edtSite.setText("");
        edtTF.setText("");
        edtTCell.setText("");
        edtDescricao.setText("");
        edtDataCad.setText("");
        edtDataNasc.setText("");
        edtCPFCNPJ.setText("");
        edtRGIE.setText("");
        cbProfissao.removeAllItems();
        cbProfissao.addItem("");
        edtOrgao.setText("");
        edtEmpresa.setText("");
        edtDataExp.setText("");
        edtFoneEmpresa.setText("");
        edtPais.setText("");
        edtUF.setText("");
        edtCidade.setText("");
        edtBairro.setText("");
        edtLogradouro.setText("");
        edtComplemento.setText("");
        edtNumCasa.setText("");
        edtCEP.setText("");
        cboxPrincipal.setSelected(false);
        cbSexo.setSelectedIndex(0);
        cbCliente.setSelected(false);
        cbFornecedor.setSelected(false);
        cbFuncionario.setSelected(false);
        tmpCdLogradouro = 0;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        buttonGroup1 = new javax.swing.ButtonGroup();
        pessoa = new beans.Pessoa();
        popopMenuContato = new javax.swing.JPopupMenu();
        menuRemoverSel = new javax.swing.JMenuItem();
        menuRemoverAll = new javax.swing.JMenuItem();
        endereco = new beans.Endereco();
        listaEnderecos = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Endereco>());
        listaContatos = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Contato>());
        popupMenuEnderecos = new javax.swing.JPopupMenu();
        menuRemSelEnder = new javax.swing.JMenuItem();
        menuRemAllEnder = new javax.swing.JMenuItem();
        pnlTabulado = new javax.swing.JTabbedPane();
        pnlPessoa = new javax.swing.JPanel();
        pnlInfoBasicas = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        edtNome = new javax.swing.JTextField();
        lblDataNasc = new javax.swing.JLabel();
        edtEmail = new javax.swing.JTextField();
        lblEmail = new javax.swing.JLabel();
        edtSite = new javax.swing.JTextField();
        lblSite = new javax.swing.JLabel();
        lblDataCad = new javax.swing.JLabel();
        edtDataCad = new javax.swing.JTextField();
        lblTF = new javax.swing.JLabel();
        edtTCell = new javax.swing.JTextField();
        lblTCell = new javax.swing.JLabel();
        edtTF = new javax.swing.JTextField();
        edtDescricao = new javax.swing.JTextField();
        lblDescricao = new javax.swing.JLabel();
        edtDataNasc = new javax.swing.JFormattedTextField();
        pnlInfoDetalhada = new javax.swing.JPanel();
        edtCPFCNPJ = new javax.swing.JTextField();
        lblCPFCNPJ = new javax.swing.JLabel();
        edtRGIE = new javax.swing.JTextField();
        lblRGIE = new javax.swing.JLabel();
        edtOrgao = new javax.swing.JTextField();
        lblOrgao = new javax.swing.JLabel();
        lblDataExp = new javax.swing.JLabel();
        cbSexo = new javax.swing.JComboBox();
        lblSexo = new javax.swing.JLabel();
        lblProfissao = new javax.swing.JLabel();
        lblEmpresa = new javax.swing.JLabel();
        edtEmpresa = new javax.swing.JTextField();
        edtFoneEmpresa = new javax.swing.JTextField();
        lblFoneEmpresa = new javax.swing.JLabel();
        edtDataExp = new javax.swing.JFormattedTextField();
        cbProfissao = new javax.swing.JComboBox();
        pnlBotoes = new javax.swing.JPanel();
        btnSair = new javax.swing.JButton();
        btnBuscarPessoa = new javax.swing.JButton();
        btnNovoPessoa = new javax.swing.JButton();
        btnEditPessoa = new javax.swing.JButton();
        btnDelPessoa = new javax.swing.JButton();
        btnCancPessoa = new javax.swing.JButton();
        btnSavePessoa = new javax.swing.JButton();
        pnlTipoPessoa = new javax.swing.JPanel();
        rPF = new javax.swing.JRadioButton();
        rPJ = new javax.swing.JRadioButton();
        pnlAssocia = new javax.swing.JPanel();
        cbCliente = new javax.swing.JCheckBox();
        cbFornecedor = new javax.swing.JCheckBox();
        cbFuncionario = new javax.swing.JCheckBox();
        pnlExtra = new javax.swing.JPanel();
        lblMargemCliente = new javax.swing.JLabel();
        edtDescontoCliente = new javax.swing.JTextField();
        lblValorHoraFunc = new javax.swing.JLabel();
        edtValorHoraFunc = new javax.swing.JTextField();
        pnlEnderecos = new javax.swing.JPanel();
        pnlEdits = new javax.swing.JPanel();
        btnBuscaCep = new javax.swing.JButton();
        lblPais = new javax.swing.JLabel();
        edtPais = new javax.swing.JTextField();
        lblUF = new javax.swing.JLabel();
        edtUF = new javax.swing.JTextField();
        lblCidade = new javax.swing.JLabel();
        edtCidade = new javax.swing.JTextField();
        lblLogradouro = new javax.swing.JLabel();
        edtLogradouro = new javax.swing.JTextField();
        edtBairro = new javax.swing.JTextField();
        lblBairro = new javax.swing.JLabel();
        lblNumero = new javax.swing.JLabel();
        edtNumCasa = new javax.swing.JTextField();
        edtComplemento = new javax.swing.JTextField();
        lblComplemento = new javax.swing.JLabel();
        edtCEP = new javax.swing.JTextField();
        lblCEP = new javax.swing.JLabel();
        btnNovoEndereco = new javax.swing.JButton();
        btnAssociar = new javax.swing.JButton();
        cboxPrincipal = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelaEnderecos = new javax.swing.JTable();
        pnlContato = new javax.swing.JPanel();
        pnlCabContato = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        edtNomeContato = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        edtCargoContato = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        edtTelefoneContato = new javax.swing.JTextField();
        btnInserirContato = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        edtObservacoes = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaContatos = new javax.swing.JTable();

        menuRemoverSel.setText("Remover Selecionado(s)");
        menuRemoverSel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverSelActionPerformed(evt);
            }
        });
        popopMenuContato.add(menuRemoverSel);

        menuRemoverAll.setText("Remover Todos");
        menuRemoverAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemoverAllActionPerformed(evt);
            }
        });
        popopMenuContato.add(menuRemoverAll);

        menuRemSelEnder.setText("Remover Selecionado(s)");
        menuRemSelEnder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemSelEnderActionPerformed(evt);
            }
        });
        popupMenuEnderecos.add(menuRemSelEnder);

        menuRemAllEnder.setText("Remover Todos");
        menuRemAllEnder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRemAllEnderActionPerformed(evt);
            }
        });
        popupMenuEnderecos.add(menuRemAllEnder);

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        pnlInfoBasicas.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações Basicas"));

        lblNome.setText("Nome:");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${nomePessoa}"), edtNome, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblDataNasc.setText("Data Nasc/Fund:");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${emailPessoa}"), edtEmail, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        edtEmail.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                edtEmailFocusLost(evt);
            }
        });

        lblEmail.setText("Email:");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${sitePessoa}"), edtSite, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblSite.setText("Site:");

        lblDataCad.setText("Data Cadastro:");

        edtDataCad.setEditable(false);
        edtDataCad.setEnabled(false);

        lblTF.setText("Tel. Fixo:");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${telefoneCellPessoa}"), edtTCell, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblTCell.setText("Tel. Celular:");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${telefoneFixoPessoa}"), edtTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${descricaoPessoa}"), edtDescricao, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblDescricao.setText("Descrição:");

        try {
            edtDataNasc.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout pnlInfoBasicasLayout = new javax.swing.GroupLayout(pnlInfoBasicas);
        pnlInfoBasicas.setLayout(pnlInfoBasicasLayout);
        pnlInfoBasicasLayout.setHorizontalGroup(
            pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNome, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblEmail, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblSite, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(8, 8, 8)
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtEmail)
                    .addComponent(edtNome)
                    .addComponent(edtSite))
                .addGap(4, 4, 4)
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblTF)
                    .addComponent(lblTCell)
                    .addComponent(lblDescricao))
                .addGap(10, 10, 10)
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtTCell, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edtTF, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDataCad, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblDataNasc, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtDataCad)
                            .addComponent(edtDataNasc)))
                    .addComponent(edtDescricao))
                .addContainerGap())
        );

        pnlInfoBasicasLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {edtTCell, edtTF});

        pnlInfoBasicasLayout.setVerticalGroup(
            pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(edtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDataNasc)
                    .addComponent(lblTF)
                    .addComponent(edtTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtDataNasc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEmail)
                    .addComponent(edtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtDataCad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDataCad)
                    .addComponent(edtTCell, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTCell))
                .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblSite))
                    .addGroup(pnlInfoBasicasLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoBasicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtSite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDescricao))))
                .addContainerGap())
        );

        pnlInfoDetalhada.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações Pessoa Física"));

        lblCPFCNPJ.setText("CPF:");

        lblRGIE.setText("RG:");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${orgaoRgPessoa}"), edtOrgao, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblOrgao.setText("Orgão:");

        lblDataExp.setText("Data Expedição:");

        cbSexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Feminino" }));

        lblSexo.setText("Sexo:");

        lblProfissao.setText("Profissão:");

        lblEmpresa.setText("Empresa:");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${empresaPessoa}"), edtEmpresa, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${foneEmpresaPessoa}"), edtFoneEmpresa, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblFoneEmpresa.setText("Fone Empresa:");

        try {
            edtDataExp.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        cbProfissao.setEditable(true);

        javax.swing.GroupLayout pnlInfoDetalhadaLayout = new javax.swing.GroupLayout(pnlInfoDetalhada);
        pnlInfoDetalhada.setLayout(pnlInfoDetalhadaLayout);
        pnlInfoDetalhadaLayout.setHorizontalGroup(
            pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoDetalhadaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblProfissao, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblRGIE, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblCPFCNPJ, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(4, 4, 4)
                .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtCPFCNPJ)
                    .addComponent(edtRGIE)
                    .addComponent(cbProfissao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblOrgao)
                    .addComponent(lblSexo)
                    .addComponent(lblEmpresa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInfoDetalhadaLayout.createSequentialGroup()
                        .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtEmpresa)
                            .addGroup(pnlInfoDetalhadaLayout.createSequentialGroup()
                                .addComponent(edtOrgao, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDataExp, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblFoneEmpresa, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtFoneEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edtDataExp, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlInfoDetalhadaLayout.createSequentialGroup()
                        .addComponent(cbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlInfoDetalhadaLayout.setVerticalGroup(
            pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInfoDetalhadaLayout.createSequentialGroup()
                .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCPFCNPJ)
                        .addComponent(edtCPFCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblSexo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInfoDetalhadaLayout.createSequentialGroup()
                        .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDataExp)
                            .addComponent(edtDataExp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtFoneEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblFoneEmpresa)))
                    .addGroup(pnlInfoDetalhadaLayout.createSequentialGroup()
                        .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtOrgao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOrgao))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblProfissao)
                            .addComponent(lblEmpresa)
                            .addComponent(edtEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlInfoDetalhadaLayout.createSequentialGroup()
                        .addGroup(pnlInfoDetalhadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblRGIE)
                            .addComponent(edtRGIE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbProfissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnBuscarPessoa.setText("Buscar");
        btnBuscarPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPessoaActionPerformed(evt);
            }
        });

        btnNovoPessoa.setText("Novo");
        btnNovoPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoPessoaActionPerformed(evt);
            }
        });

        btnEditPessoa.setText("Alterar");
        btnEditPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditPessoaActionPerformed(evt);
            }
        });

        btnDelPessoa.setText("Excluir");
        btnDelPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelPessoaActionPerformed(evt);
            }
        });

        btnCancPessoa.setText("Cancelar");
        btnCancPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancPessoaActionPerformed(evt);
            }
        });

        btnSavePessoa.setText("Confirmar");
        btnSavePessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSavePessoaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBotoesLayout = new javax.swing.GroupLayout(pnlBotoes);
        pnlBotoes.setLayout(pnlBotoesLayout);
        pnlBotoesLayout.setHorizontalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBuscarPessoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNovoPessoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditPessoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelPessoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancPessoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSavePessoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlBotoesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBuscarPessoa, btnCancPessoa, btnDelPessoa, btnEditPessoa, btnNovoPessoa, btnSair, btnSavePessoa});

        pnlBotoesLayout.setVerticalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addGroup(pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarPessoa)
                    .addComponent(btnNovoPessoa)
                    .addComponent(btnEditPessoa)
                    .addComponent(btnDelPessoa)
                    .addComponent(btnCancPessoa)
                    .addComponent(btnSavePessoa)
                    .addComponent(btnSair))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlTipoPessoa.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de Pessoa"));

        buttonGroup1.add(rPF);
        rPF.setSelected(true);
        rPF.setText("Pessoa Física");
        rPF.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rPFItemStateChanged(evt);
            }
        });

        buttonGroup1.add(rPJ);
        rPJ.setText("Pessoa Jurídica");
        rPJ.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rPJItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pnlTipoPessoaLayout = new javax.swing.GroupLayout(pnlTipoPessoa);
        pnlTipoPessoa.setLayout(pnlTipoPessoaLayout);
        pnlTipoPessoaLayout.setHorizontalGroup(
            pnlTipoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTipoPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rPF)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rPJ)
                .addContainerGap(70, Short.MAX_VALUE))
        );
        pnlTipoPessoaLayout.setVerticalGroup(
            pnlTipoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTipoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(rPF)
                .addComponent(rPJ))
        );

        pnlAssocia.setBorder(javax.swing.BorderFactory.createTitledBorder("Associação"));

        cbCliente.setText("Cliente");
        cbCliente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbClienteItemStateChanged(evt);
            }
        });

        cbFornecedor.setText("Fornecedor");
        cbFornecedor.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbFornecedorItemStateChanged(evt);
            }
        });

        cbFuncionario.setText("Funcionario");
        cbFuncionario.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cbFuncionarioStateChanged(evt);
            }
        });
        cbFuncionario.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbFuncionarioItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pnlAssociaLayout = new javax.swing.GroupLayout(pnlAssocia);
        pnlAssocia.setLayout(pnlAssociaLayout);
        pnlAssociaLayout.setHorizontalGroup(
            pnlAssociaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAssociaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbCliente)
                .addGap(64, 64, 64)
                .addComponent(cbFornecedor)
                .addGap(45, 45, 45)
                .addComponent(cbFuncionario)
                .addContainerGap(129, Short.MAX_VALUE))
        );
        pnlAssociaLayout.setVerticalGroup(
            pnlAssociaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAssociaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbCliente)
                .addComponent(cbFornecedor)
                .addComponent(cbFuncionario))
        );

        pnlExtra.setBorder(javax.swing.BorderFactory.createTitledBorder("Extra"));

        lblMargemCliente.setText("Margem de descontos do cliente (%):");

        edtDescontoCliente.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtDescontoCliente.setForeground(new java.awt.Color(0, 0, 255));
        edtDescontoCliente.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtDescontoCliente.setText("0,00");
        edtDescontoCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtDescontoClienteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtDescontoClienteKeyReleased(evt);
            }
        });

        lblValorHoraFunc.setText("Valor HORA Funcionário (R$):");

        edtValorHoraFunc.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        edtValorHoraFunc.setForeground(new java.awt.Color(0, 0, 255));
        edtValorHoraFunc.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        edtValorHoraFunc.setText("0,00");
        edtValorHoraFunc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtValorHoraFuncKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout pnlExtraLayout = new javax.swing.GroupLayout(pnlExtra);
        pnlExtra.setLayout(pnlExtraLayout);
        pnlExtraLayout.setHorizontalGroup(
            pnlExtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlExtraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlExtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblMargemCliente)
                    .addComponent(lblValorHoraFunc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlExtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtValorHoraFunc, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtDescontoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlExtraLayout.setVerticalGroup(
            pnlExtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlExtraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlExtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMargemCliente)
                    .addComponent(edtDescontoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlExtraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblValorHoraFunc)
                    .addComponent(edtValorHoraFunc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlPessoaLayout = new javax.swing.GroupLayout(pnlPessoa);
        pnlPessoa.setLayout(pnlPessoaLayout);
        pnlPessoaLayout.setHorizontalGroup(
            pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlInfoDetalhada, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlPessoaLayout.createSequentialGroup()
                        .addComponent(pnlTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlAssocia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(pnlInfoBasicas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlExtra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlPessoaLayout.setVerticalGroup(
            pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnlAssocia, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTipoPessoa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlInfoBasicas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlInfoDetalhada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlExtra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addComponent(pnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlTabulado.addTab("Pessoa", pnlPessoa);

        pnlEdits.setBorder(javax.swing.BorderFactory.createTitledBorder("Endereço"));

        btnBuscaCep.setText("Buscar por CEP");
        btnBuscaCep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaCepActionPerformed(evt);
            }
        });

        lblPais.setText("Pais:");

        edtPais.setEnabled(false);

        lblUF.setText("UF:");

        edtUF.setEnabled(false);

        lblCidade.setText("Cidade:");

        edtCidade.setEnabled(false);

        lblLogradouro.setText("Logradouro:");

        edtLogradouro.setEnabled(false);
        edtLogradouro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtLogradouroActionPerformed(evt);
            }
        });
        edtLogradouro.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                edtLogradouroInputMethodTextChanged(evt);
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        edtLogradouro.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                edtLogradouroPropertyChange(evt);
            }
        });

        edtBairro.setEnabled(false);

        lblBairro.setText("Bairro:");

        lblNumero.setText("Numero:");

        edtNumCasa.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${numeroEnderecoPessoa}"), edtNumCasa, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        edtComplemento.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pessoa, org.jdesktop.beansbinding.ELProperty.create("${complementoEnderecoPessoa}"), edtComplemento, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblComplemento.setText("Complemento:");

        edtCEP.setEditable(false);
        edtCEP.setEnabled(false);

        lblCEP.setText("CEP:");

        btnNovoEndereco.setText("Cadastrar Novo");
        btnNovoEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoEnderecoActionPerformed(evt);
            }
        });

        btnAssociar.setText("Associar Endereço");
        btnAssociar.setEnabled(false);
        btnAssociar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssociarActionPerformed(evt);
            }
        });

        cboxPrincipal.setText("End. Principal");

        javax.swing.GroupLayout pnlEditsLayout = new javax.swing.GroupLayout(pnlEdits);
        pnlEdits.setLayout(pnlEditsLayout);
        pnlEditsLayout.setHorizontalGroup(
            pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEditsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlEditsLayout.createSequentialGroup()
                        .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPais, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblBairro, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblNumero, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtBairro)
                            .addComponent(edtNumCasa)
                            .addGroup(pnlEditsLayout.createSequentialGroup()
                                .addComponent(edtPais, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblUF)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtUF, javax.swing.GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCidade, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblLogradouro, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblComplemento, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtLogradouro)
                            .addComponent(edtCidade)
                            .addGroup(pnlEditsLayout.createSequentialGroup()
                                .addComponent(edtComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblCEP)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtCEP, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                                .addComponent(cboxPrincipal))))
                    .addGroup(pnlEditsLayout.createSequentialGroup()
                        .addComponent(btnBuscaCep)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAssociar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnNovoEndereco)))
                .addContainerGap())
        );

        pnlEditsLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAssociar, btnBuscaCep, btnNovoEndereco});

        pnlEditsLayout.setVerticalGroup(
            pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEditsLayout.createSequentialGroup()
                .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPais)
                    .addComponent(edtPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUF)
                    .addComponent(edtUF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCidade)
                    .addComponent(edtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBairro)
                    .addComponent(edtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLogradouro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNumero)
                    .addComponent(edtNumCasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblComplemento)
                    .addComponent(edtComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCEP)
                    .addComponent(edtCEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboxPrincipal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlEditsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscaCep)
                    .addComponent(btnNovoEndereco)
                    .addComponent(btnAssociar)))
        );

        tabelaEnderecos.setComponentPopupMenu(popupMenuEnderecos);

        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaEnderecos, tabelaEnderecos, "");
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nomeLogradouro}"));
        columnBinding.setColumnName("Nome Logradouro");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${numero}"));
        columnBinding.setColumnName("Numero");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nomeCidade}"));
        columnBinding.setColumnName("Nome Cidade");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${cepLogradouro}"));
        columnBinding.setColumnName("Cep Logradouro");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${principal}"));
        columnBinding.setColumnName("Principal");
        columnBinding.setColumnClass(Boolean.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane2.setViewportView(tabelaEnderecos);

        javax.swing.GroupLayout pnlEnderecosLayout = new javax.swing.GroupLayout(pnlEnderecos);
        pnlEnderecos.setLayout(pnlEnderecosLayout);
        pnlEnderecosLayout.setHorizontalGroup(
            pnlEnderecosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlEdits, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 913, Short.MAX_VALUE)
        );
        pnlEnderecosLayout.setVerticalGroup(
            pnlEnderecosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEnderecosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlEdits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE))
        );

        pnlTabulado.addTab("Endereços", pnlEnderecos);

        pnlCabContato.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 51)));

        jLabel1.setText("Nome Contato:");

        edtNomeContato.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtNomeContatoKeyPressed(evt);
            }
        });

        jLabel2.setText("Cargo:");

        edtCargoContato.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtCargoContatoKeyPressed(evt);
            }
        });

        jLabel3.setText("Telefone:");

        edtTelefoneContato.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtTelefoneContatoKeyPressed(evt);
            }
        });

        btnInserirContato.setText("Inserir");
        btnInserirContato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInserirContatoActionPerformed(evt);
            }
        });

        jLabel4.setText("Observações:");

        edtObservacoes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtObservacoesKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnlCabContatoLayout = new javax.swing.GroupLayout(pnlCabContato);
        pnlCabContato.setLayout(pnlCabContatoLayout);
        pnlCabContatoLayout.setHorizontalGroup(
            pnlCabContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCabContatoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCabContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(pnlCabContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlCabContatoLayout.createSequentialGroup()
                        .addComponent(edtNomeContato, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtCargoContato, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtTelefoneContato, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(edtObservacoes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnInserirContato, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        pnlCabContatoLayout.setVerticalGroup(
            pnlCabContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCabContatoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCabContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(edtNomeContato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(edtCargoContato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(edtTelefoneContato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnInserirContato))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlCabContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(edtObservacoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        tabelaContatos.setComponentPopupMenu(popopMenuContato);

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaContatos, tabelaContatos);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nomeContato}"));
        columnBinding.setColumnName("Nome Contato");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${cargoContato}"));
        columnBinding.setColumnName("Cargo Contato");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${telefoneContato}"));
        columnBinding.setColumnName("Telefone Contato");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${observacao}"));
        columnBinding.setColumnName("Observacao");
        columnBinding.setColumnClass(String.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane1.setViewportView(tabelaContatos);

        javax.swing.GroupLayout pnlContatoLayout = new javax.swing.GroupLayout(pnlContato);
        pnlContato.setLayout(pnlContatoLayout);
        pnlContatoLayout.setHorizontalGroup(
            pnlContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlCabContato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1)
        );
        pnlContatoLayout.setVerticalGroup(
            pnlContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContatoLayout.createSequentialGroup()
                .addComponent(pnlCabContato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE))
        );

        pnlTabulado.addTab("Contatos", pnlContato);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlTabulado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlTabulado)
                .addGap(3, 3, 3))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void defineTipoPessoa() {
        if (rPJ.isSelected()) {
            lblSexo.setVisible(false);
            cbSexo.setVisible(false);
            lblOrgao.setVisible(false);
            edtOrgao.setVisible(false);
            lblDataExp.setVisible(false);
            edtDataExp.setVisible(false);
            lblProfissao.setVisible(false);
            cbProfissao.setVisible(false);
            lblEmpresa.setVisible(false);
            edtEmpresa.setVisible(false);
            lblFoneEmpresa.setVisible(false);
            edtFoneEmpresa.setVisible(false);
            cbFuncionario.setVisible(false);
            cbFuncionario.setSelected(false);
            lblCPFCNPJ.setText("CNPJ:");
            lblRGIE.setText("IE:");
        } else if (rPF.isSelected()) {
            lblSexo.setVisible(true);
            cbSexo.setVisible(true);
            lblOrgao.setVisible(true);
            edtOrgao.setVisible(true);
            lblDataExp.setVisible(true);
            edtDataExp.setVisible(true);
            lblProfissao.setVisible(true);
            cbProfissao.setVisible(true);
            lblEmpresa.setVisible(true);
            edtEmpresa.setVisible(true);
            lblFoneEmpresa.setVisible(true);
            edtFoneEmpresa.setVisible(true);
            cbFuncionario.setVisible(true);
            lblCPFCNPJ.setText("CPF:");
            lblRGIE.setText("RG:");
        }
    }

    public void setAtivoButtonBuscar(boolean ativar) {
        this.btnBuscarPessoa.setEnabled(ativar);
        this.btnBuscarPessoa.setVisible(ativar);
    }

    public void executaButtonEditar() {
        btnEditPessoaActionPerformed(null);
    }

    private Boolean salvar() {
        //:p
        if (Validador.soNumeros(edtDataNasc.getText()).trim().equals("")) {
            Extra.Informacao(this, "Campo data nascimento/fundação é obrigatório!");
            return false;
        }
        if (edtNome.getText().trim().equals("")) {
            Extra.Informacao(this, "Campo nome é obrigatório!");
            return false;
        }
        if (edtCPFCNPJ.getText().trim().equals("")) {
            Extra.Informacao(this, "Campo CPF/CNPJ é obrigatório!");
            return false;
        }
        if (edtRGIE.getText().trim().equals("")) {
            Extra.Informacao(this, "Campo RG/IE é obrigatório!");
            return false;
        }
        if (edtTF.getText().trim().equals("")) {
            Extra.Informacao(this, "Campo telefone fixo é obrigatório!");
            return false;
        }
        if (rPF.isSelected()) {
            if ((Validador.soNumeros(edtDataExp.getText()).trim().equals(""))) {
                Extra.Informacao(this, "Campo data expedição do RG é obrigatório!");
                return false;
            }
        }
        try {
            populaPessoa();
            if (PessoaDao.salvarPessoa(pessoa, Variaveis.getEmpresa())) {
                limpaCampos();
                setPessoa();
                return true;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
        return false;
    }

    private void setEmpresaProfissao() {
        edtEmpresa.setEnabled(false);
        edtEmpresa.setText(Variaveis.getEmpresa().getFantasia());
        edtFoneEmpresa.setEnabled(false);
        String SQL = "select fone_empresa from empresa where cd_empresa = ?";
        Object[] par = {Variaveis.getEmpresa().getCodigo()};
        try {
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT,
                    Variaveis.getDbType());
            if (rs.next()) {
                edtFoneEmpresa.setText(rs.getString("fone_empresa"));
            }
            if (cbProfissao.getItemCount() >= 0) {
                cbProfissao.setSelectedIndex(0);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }

    private void buscaCargos() {
        try {
            Object[] par = {Variaveis.getEmpresa().getCodigo()};
            String SQL = "select distinct(profissao_pessoa) from pessoa where cd_empresa = ? and profissao_pessoa is not null";
            ResultSet rs2 = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT,
                    Variaveis.getDbType());
            cbProfissao.removeAllItems();
            cbProfissao.addItem("");
            while (rs2.next()) {
                String a = rs2.getString("profissao_pessoa");
                if (!a.trim().equals("")) {
                    cbProfissao.addItem(a);
                }
            }
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }

    private void menuRemoverSelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverSelActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todos os items selecionados?")) {
            int[] array = tabelaContatos.getSelectedRows();
            ArrayList<Contato> v = new ArrayList<Contato>();
            for (int i : array) {
                v.add(pessoa.getContatos().get(i));
            }
            for (Contato contact : v) {
                PessoaDao.excluiContato(pessoa, contact, Variaveis.getEmpresa());
            }
        }
    }//GEN-LAST:event_menuRemoverSelActionPerformed

    private void menuRemoverAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemoverAllActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todos os items?")) {
            PessoaDao.excluiAllContatos(pessoa, Variaveis.getEmpresa());
        }
    }//GEN-LAST:event_menuRemoverAllActionPerformed

    private void btnInserirContatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInserirContatoActionPerformed
        if (!edtNomeContato.getText().equals("")) {
            Contato c = new Contato();
            c.setNomeContato(edtNomeContato.getText().trim());
            c.setCargoContato(edtCargoContato.getText().trim());
            c.setTelefoneContato(edtTelefoneContato.getText().trim());
            c.setObservacao(edtObservacoes.getText().trim());
            c.setPessoaContato(pessoa);
            PessoaDao.salvaContato(pessoa, c, Variaveis.getEmpresa());
            edtNomeContato.setText("");
            edtCargoContato.setText("");
            edtTelefoneContato.setText("");
            edtObservacoes.setText("");
        }
        edtNomeContato.requestFocus();
    }//GEN-LAST:event_btnInserirContatoActionPerformed

    private void edtTelefoneContatoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtTelefoneContatoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            edtObservacoes.requestFocus();
        }
    }//GEN-LAST:event_edtTelefoneContatoKeyPressed

    private void edtCargoContatoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtCargoContatoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            edtTelefoneContato.requestFocus();
        }
    }//GEN-LAST:event_edtCargoContatoKeyPressed

    private void edtNomeContatoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtNomeContatoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            edtCargoContato.requestFocus();
        }
    }//GEN-LAST:event_edtNomeContatoKeyPressed

    private void btnAssociarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssociarActionPerformed
        if (!edtNumCasa.getText().trim().equals("")) {
            if (PessoaDao.contemEnderecoPrincipal(pessoa) && cboxPrincipal.isSelected()) {
                Extra.Informacao(this, "Já existe um endereço principal para esta pessoa!");
                return;
            }
            Endereco e = new Endereco(tmpCdLogradouro, edtPais.getText(), edtUF.getText(), edtCidade.getText(), edtBairro.getText(), edtLogradouro.getText(), edtCEP.getText(), Integer.parseInt(edtNumCasa.getText()), edtComplemento.getText(), cboxPrincipal.isSelected());
            if (pessoa.getEnderecos().contains(e)) {
                if (Extra.Pergunta(this, "Este endereço já esta associado a pessoa! \n"
                        + "Gostaria de atualiza-lo?")) {
                    PessoaDao.salvaEndereco(pessoa, e, Variaveis.getEmpresa());
                    btnAssociar.setEnabled(false);
                    limpaEdtsEndereco();
                }
            } else {
                PessoaDao.salvaEndereco(pessoa, e, Variaveis.getEmpresa());
                btnAssociar.setEnabled(false);
                limpaEdtsEndereco();
            }
        } else {
            Extra.Informacao(this, "Campo Numero é OBRIGATÓRIO!");
        }
    }//GEN-LAST:event_btnAssociarActionPerformed

    private void btnNovoEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoEnderecoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNovoEnderecoActionPerformed

    private void edtLogradouroPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_edtLogradouroPropertyChange
   }//GEN-LAST:event_edtLogradouroPropertyChange

    private void edtLogradouroInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_edtLogradouroInputMethodTextChanged
   }//GEN-LAST:event_edtLogradouroInputMethodTextChanged

    private void edtLogradouroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtLogradouroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtLogradouroActionPerformed

    private void btnBuscaCepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaCepActionPerformed
        FI_CadFindCEP cep = (FI_CadFindCEP) Util.execucaoDiretaClass("formularios.FI_CadFindCEP", "Modulos-jSyscom.jar", "Localiza Endereço - " + Util.getSource(seqInstancias, FI_CadPessoa.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        cep.endereco = endereco;
    }//GEN-LAST:event_btnBuscaCepActionPerformed

    private void cbFuncionarioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbFuncionarioItemStateChanged
        if (cbFuncionario.isSelected()) {
            lblValorHoraFunc.setVisible(true);
            edtValorHoraFunc.setVisible(true);
            if ((((cbProfissao.getSelectedIndex() >= 0) ? (cbProfissao.getSelectedItem().toString().equals("")) : true)) && edtEmpresa.getText().trim().equals("")
                    && edtFoneEmpresa.getText().trim().equals("")) {
                setEmpresaProfissao();
            }
        } else {
            lblValorHoraFunc.setVisible(false);
            edtValorHoraFunc.setVisible(false);
        }
    }//GEN-LAST:event_cbFuncionarioItemStateChanged

    private void cbFornecedorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbFornecedorItemStateChanged
   }//GEN-LAST:event_cbFornecedorItemStateChanged

    private void cbClienteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbClienteItemStateChanged
        if (cbCliente.isSelected()) {
            lblMargemCliente.setVisible(true);
            edtDescontoCliente.setVisible(true);
        } else {
            lblMargemCliente.setVisible(false);
            edtDescontoCliente.setVisible(false);
        }
   }//GEN-LAST:event_cbClienteItemStateChanged

    private void rPJItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rPJItemStateChanged
        defineTipoPessoa();
    }//GEN-LAST:event_rPJItemStateChanged

    private void rPFItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rPFItemStateChanged
        defineTipoPessoa();
    }//GEN-LAST:event_rPFItemStateChanged

    private void btnSavePessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSavePessoaActionPerformed
        if (salvar()) {
            ativaEdits(false);
            edtComplemento.setEnabled(false);
            edtNumCasa.setEnabled(false);
            ativaButtons(true, true, true, true, true, false, true);
            alterado = false;
            pnlTabulado.setEnabledAt(1, true);
            pnlTabulado.setEnabledAt(2, true);
            Extra.Informacao(this, "Salvo com sucesso! \n"
                    + "Agora pode adicionar o(s) endereço(s) e contato(s) da pessoa!");
        } else {
            Extra.Informacao(this, "Falha ao salvar pessoa!");
        }
    }//GEN-LAST:event_btnSavePessoaActionPerformed

    private void btnCancPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancPessoaActionPerformed
        cancelar();
    }//GEN-LAST:event_btnCancPessoaActionPerformed

    private void btnDelPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelPessoaActionPerformed
        if (pessoa.getCdPessoa() != null) {
            if (Extra.Pergunta(this, "Excluir registro atual?")) {
                if (PessoaDao.excluirPessoa(pessoa, Variaveis.getEmpresa())) {
                    alterado = false;
                    cancelar();
                    Extra.Informacao(this, "Registro apagado com sucesso!");
                } else {
                    Extra.Informacao(this, "Falha ao apagar registro!");
                }
            }
        } else {
            alterado = false;
            cancelar();
        }
    }//GEN-LAST:event_btnDelPessoaActionPerformed

    private void btnEditPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditPessoaActionPerformed
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true, true);
        alterado = true;
        pnlTabulado.setEnabledAt(1, true);
        pnlTabulado.setEnabledAt(2, true);
    }//GEN-LAST:event_btnEditPessoaActionPerformed

    private void btnNovoPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoPessoaActionPerformed
        cancelar();
        ativaEdits(true);
        ativaButtons(false, false, false, false, true, true, true);
        alterado = true;
        pessoa.clearPessoa();
        buscaCargos();
        edtNome.requestFocus();
        //endereco.clearEndereco();
    }//GEN-LAST:event_btnNovoPessoaActionPerformed

    private void btnBuscarPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarPessoaActionPerformed
        FI_CadFindPessoa frmPessoa = (FI_CadFindPessoa) Util.execucaoDiretaClass("formularios.FI_CadFindPessoa", "Modulos-jSyscom.jar", "Localiza Pessoa - " + Util.getSource(seqInstancias, FI_CadPessoa.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        if (frmPessoa != null) {
            frmPessoa.setAtivoButtonNovo(false);
            frmPessoa.pessoa = pessoa;
        }
    }//GEN-LAST:event_btnBuscarPessoaActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        try {
            setClosed(true);
        } catch (PropertyVetoException e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
    }//GEN-LAST:event_btnSairActionPerformed

    private void edtEmailFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_edtEmailFocusLost
        if (!edtEmail.getText().trim().equals("")) {
            if (!Validador.isEmail(edtEmail.getText())) {
                Extra.Informacao(this, "Email inválido!");
                edtEmail.setText("");
                edtEmail.requestFocus();
            }
        }
    }//GEN-LAST:event_edtEmailFocusLost

    private void edtObservacoesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtObservacoesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnInserirContatoActionPerformed(null);
        }
    }//GEN-LAST:event_edtObservacoesKeyPressed

    private void menuRemSelEnderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemSelEnderActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todos os items selecionados?")) {
            int[] array = tabelaEnderecos.getSelectedRows();
            ArrayList<Endereco> v = new ArrayList<Endereco>();
            for (int i : array) {
                v.add(pessoa.getEnderecos().get(i));
            }
            for (Endereco ender : v) {
                PessoaDao.excluiEndereco(pessoa, ender, Variaveis.getEmpresa());
            }
        }
    }//GEN-LAST:event_menuRemSelEnderActionPerformed

    private void menuRemAllEnderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRemAllEnderActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todos os items?")) {
            PessoaDao.excluiAllEnderecos(pessoa, Variaveis.getEmpresa());
        }
    }//GEN-LAST:event_menuRemAllEnderActionPerformed

    private void cbFuncionarioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_cbFuncionarioStateChanged
    }//GEN-LAST:event_cbFuncionarioStateChanged

    private void edtDescontoClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtDescontoClienteKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtDescontoClienteKeyReleased

    private void edtValorHoraFuncKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtValorHoraFuncKeyReleased
        try {
            Format.formatTextField(((JTextField) evt.getComponent()), Variaveis.getDecimal(), false);
        } catch (Exception ex) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(ex), false, true, this);
        }
    }//GEN-LAST:event_edtValorHoraFuncKeyReleased

    private void edtDescontoClienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtDescontoClienteKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtDescontoClienteKeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAssociar;
    private javax.swing.JButton btnBuscaCep;
    private javax.swing.JButton btnBuscarPessoa;
    private javax.swing.JButton btnCancPessoa;
    private javax.swing.JButton btnDelPessoa;
    private javax.swing.JButton btnEditPessoa;
    private javax.swing.JButton btnInserirContato;
    private javax.swing.JButton btnNovoEndereco;
    private javax.swing.JButton btnNovoPessoa;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSavePessoa;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox cbCliente;
    private javax.swing.JCheckBox cbFornecedor;
    private javax.swing.JCheckBox cbFuncionario;
    private javax.swing.JComboBox cbProfissao;
    private javax.swing.JComboBox cbSexo;
    private javax.swing.JCheckBox cboxPrincipal;
    private javax.swing.JTextField edtBairro;
    private javax.swing.JTextField edtCEP;
    private javax.swing.JTextField edtCPFCNPJ;
    private javax.swing.JTextField edtCargoContato;
    private javax.swing.JTextField edtCidade;
    private javax.swing.JTextField edtComplemento;
    private javax.swing.JTextField edtDataCad;
    private javax.swing.JFormattedTextField edtDataExp;
    private javax.swing.JFormattedTextField edtDataNasc;
    private javax.swing.JTextField edtDescontoCliente;
    private javax.swing.JTextField edtDescricao;
    private javax.swing.JTextField edtEmail;
    private javax.swing.JTextField edtEmpresa;
    private javax.swing.JTextField edtFoneEmpresa;
    private javax.swing.JTextField edtLogradouro;
    private javax.swing.JTextField edtNome;
    private javax.swing.JTextField edtNomeContato;
    private javax.swing.JTextField edtNumCasa;
    private javax.swing.JTextField edtObservacoes;
    private javax.swing.JTextField edtOrgao;
    private javax.swing.JTextField edtPais;
    private javax.swing.JTextField edtRGIE;
    private javax.swing.JTextField edtSite;
    private javax.swing.JTextField edtTCell;
    private javax.swing.JTextField edtTF;
    private javax.swing.JTextField edtTelefoneContato;
    private javax.swing.JTextField edtUF;
    private javax.swing.JTextField edtValorHoraFunc;
    private beans.Endereco endereco;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblBairro;
    private javax.swing.JLabel lblCEP;
    private javax.swing.JLabel lblCPFCNPJ;
    private javax.swing.JLabel lblCidade;
    private javax.swing.JLabel lblComplemento;
    private javax.swing.JLabel lblDataCad;
    private javax.swing.JLabel lblDataExp;
    private javax.swing.JLabel lblDataNasc;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblEmpresa;
    private javax.swing.JLabel lblFoneEmpresa;
    private javax.swing.JLabel lblLogradouro;
    private javax.swing.JLabel lblMargemCliente;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNumero;
    private javax.swing.JLabel lblOrgao;
    private javax.swing.JLabel lblPais;
    private javax.swing.JLabel lblProfissao;
    private javax.swing.JLabel lblRGIE;
    private javax.swing.JLabel lblSexo;
    private javax.swing.JLabel lblSite;
    private javax.swing.JLabel lblTCell;
    private javax.swing.JLabel lblTF;
    private javax.swing.JLabel lblUF;
    private javax.swing.JLabel lblValorHoraFunc;
    private java.util.List<Contato> listaContatos;
    private java.util.List<Endereco> listaEnderecos;
    private javax.swing.JMenuItem menuRemAllEnder;
    private javax.swing.JMenuItem menuRemSelEnder;
    private javax.swing.JMenuItem menuRemoverAll;
    private javax.swing.JMenuItem menuRemoverSel;
    public beans.Pessoa pessoa;
    private javax.swing.JPanel pnlAssocia;
    private javax.swing.JPanel pnlBotoes;
    private javax.swing.JPanel pnlCabContato;
    private javax.swing.JPanel pnlContato;
    private javax.swing.JPanel pnlEdits;
    private javax.swing.JPanel pnlEnderecos;
    private javax.swing.JPanel pnlExtra;
    private javax.swing.JPanel pnlInfoBasicas;
    private javax.swing.JPanel pnlInfoDetalhada;
    private javax.swing.JPanel pnlPessoa;
    private javax.swing.JTabbedPane pnlTabulado;
    private javax.swing.JPanel pnlTipoPessoa;
    private javax.swing.JPopupMenu popopMenuContato;
    private javax.swing.JPopupMenu popupMenuEnderecos;
    private javax.swing.JRadioButton rPF;
    private javax.swing.JRadioButton rPJ;
    private javax.swing.JTable tabelaContatos;
    private javax.swing.JTable tabelaEnderecos;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Pessoa) {
            limpaCampos();
            ativaEdits(false);
            pnlTabulado.setEnabledAt(1, false);
            pnlTabulado.setEnabledAt(2, false);
            edtComplemento.setEnabled(false);
            edtNumCasa.setEnabled(false);
            ativaButtons(true, true, true, true, true, false, false);
            alterado = false;
            setPessoa();
        } else if (o instanceof Endereco) {
            edtNumCasa.requestFocus();
            edtNumCasa.setEnabled(true);
            edtComplemento.setEnabled(true);
            setEndereco((Endereco) o);
        }
    }
}
