/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package formularios;

import base.FrmPrincipal;
import beans.OpcoesTela;
import beans.Usuario;
import beans.Variaveis;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.apache.commons.mail.EmailException;
import utilitarios.*;

/**
 *
 * @author Jean
 */
public final class FI_EnviaXML extends javax.swing.JInternalFrame {

    static Integer numInstancias = 0;
    private Integer numMaxInstancias = 65535;
    private final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private Integer seqInstancias = 0;
    private Usuario usuario = null;
    private int tempoTimer = 60000;
    private String path = "";
    private String path_backup = "";
    private String eMensagem = "";
    private String eSubject = "";
    private String eRemetente = "";
    private String eSMTP_Server = "";
    private Integer eSMTP_Port = 587;
    private String eSMTP_User = "";
    private String eSMTP_Pass = "";

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public void addMemo(String text) {
        memo1.setText(memo1.getText() + "\n" + text);
    }
    
    
    public FI_EnviaXML(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title+(seqInstancias>0?(" - ["+String.valueOf(seqInstancias)+"]"):""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            t.start();
            tControle.start();
            tempoFaltante.start();
            inicia();
        }

    }
    

    public String montaMSG(String numnfe, String protocolo) {
        String tmp = eMensagem.replaceFirst("@chave_acesso", numnfe);
        return tmp.replaceFirst("@protocolo", protocolo);
    }

    public void inicia() {
        carregaConfiguracoes();
        t.stop();
        tempoFaltante.stop();
        lblTempo.setText("0");
        addMemo("Iniciando envios...");
        String[] arquivos = Arquivo.listaArqsMaskDir(path, "", ".xml");
        addMemo("Quantidade de arquivos encontrados: " + arquivos.length);
        for (String arq : arquivos) {
            addMemo("-------------------------------------");
            addMemo("Arquivo: " + path + arq);
            String pdf = Format.RemoveUltimosChar(arq, 4) + ".pdf";
            if (Arquivo.FileExists(path + pdf)) {
                LeitorXML l = new LeitorXML(path + arq);
                String email = l.getInfo("//k:nfeProc/k:NFe/k:infNFe/k:dest/k:email");
                String NumNFe = l.getInfo("//k:nfeProc/k:protNFe/k:infProt/k:chNFe");
                String NumProt = l.getInfo("//k:nfeProc/k:protNFe/k:infProt/k:nProt");
                if (email == null) {
                    String xml = Arquivo.LeArqTXT(path + arq);
                    String xtmp = xml.substring(xml.indexOf("<email>") + 7, xml.indexOf("</email>"));
                    email = xtmp;
                }
                if (NumNFe == null) {
                    NumNFe = Validador.soNumeros(Format.RemoveUltimosChar(arq, 4));
                }
                addMemo("E-mail: " + email);
                addMemo("NFe: " + NumNFe);
                if (NumProt != null) {
                    addMemo("Protocolo: " + NumProt);
                } else {
                    NumProt = "";
                }
                try {
                    String[] anexo = {path + arq, path + pdf};
                    addMemo("Enviando email para: " + email);
                    String[] destinatarios = {email, eRemetente};
                    Email.enviaEmailComAnexo(eSMTP_Server, eSMTP_Port, eSMTP_User, eSMTP_Pass, destinatarios,
                            eRemetente, eSubject, montaMSG(NumNFe, NumProt), anexo);
                    Arquivo.MoveFile(path + arq, path_backup + arq);
                    Arquivo.MoveFile(path + pdf, path_backup + pdf);
                } catch (EmailException e) {
                    addMemo(e.getMessage());
                    addMemo("Falha no envio!");
                    Variaveis.addNewLog(this.getClass().getName()+":\n\"Falha no envio!\"\n"+Utils.getStackTrace(e), false, false, this);
                }
            } else {
                addMemo("PDF não encontrado!");
            }
            addMemo("-------------------------------------");
        }
        addMemo("Fim de envios!");
        lblTempo.setText(String.valueOf(t.getDelay() / 1000));
        t.start();
        tempoFaltante.start();
    }

    public void carregaConfiguracoes() {
        if (Arquivo.FileExists(Arquivo.getPathBase() + "Config.ini")) {
            tempoTimer = Integer.parseInt(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "ENVIA_XML", "TEMPO_ENTRE_ENVIOS").trim()) * 1000;
            path = Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "ENVIA_XML", "PATH").trim();
            path_backup = Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "ENVIA_XML", "PATH_BACKUP").trim();
            eRemetente = Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "EMAIL", "REMETENTE").trim();
            eSubject = Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "EMAIL", "SUBJECT").trim();
            eSMTP_Server = Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "EMAIL", "SMTP_SERVER").trim();
            eSMTP_Port = Integer.parseInt(Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "EMAIL", "SMTP_PORT").trim());
            eSMTP_User = Arquivo.LeINI(Arquivo.getPathBase() + "Config.ini", "EMAIL", "SMTP_USER").trim();
        }
        if (Arquivo.FileExists(Arquivo.getPathBase() + "EmailPass.pw")) {
            eSMTP_Pass = Extra.CriptoDecripto(Extra.DECRIPTOGRAFA, Arquivo.LePrimeiraLinhaArqTXT(Arquivo.getPathBase() + "EmailPass.pw").trim());
        }
        if (Arquivo.FileExists(Arquivo.getPathBase() + "MSG.txt")) {
            eMensagem = Arquivo.LeArqTXT(Arquivo.getPathBase() + "MSG.txt");
        }
        t.stop();
        atuTimerSync();
        t.start();
    }
    javax.swing.Timer t = new javax.swing.Timer(60000, new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            inicia();
        }
    });
    javax.swing.Timer tControle = new javax.swing.Timer(1500000, new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            inicia();
        }
    });
    javax.swing.Timer tempoFaltante = new javax.swing.Timer(1000, new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            int i = Integer.parseInt(lblTempo.getText());
            lblTempo.setText(String.valueOf(i - 1));
            if (Arquivo.FileExists(path + "SyncNow.flg")) {
                inicia();
                Arquivo.DeleteFile(path + "SyncNow.flg");
            }
        }
    });

    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }


    public void atuTimerSync() {
        t.setDelay(tempoTimer);
        lblTempo.setText(String.valueOf(t.getDelay() / 1000));
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        memo1 = new javax.swing.JTextArea();
        btnStart = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnHide = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        lblTempo = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblResgistrosTotais = new javax.swing.JLabel();

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        memo1.setColumns(20);
        memo1.setEditable(false);
        memo1.setRows(5);
        memo1.setFocusable(false);
        jScrollPane1.setViewportView(memo1);

        btnStart.setText("Iniciar envios agora");
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });

        jLabel1.setText("Logs de Envio");

        btnHide.setText("Ocultar Tela");
        btnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHideActionPerformed(evt);
            }
        });

        jLabel2.setText("Próximo envio em: ");

        lblTempo.setText("0");

        jLabel4.setText("Total Enviados:");

        lblResgistrosTotais.setText("0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(125, 125, 125)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(lblTempo))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnHide, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblResgistrosTotais, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(lblTempo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(lblResgistrosTotais, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnStart)
                    .addComponent(btnHide))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        if (Extra.Pergunta(this, "Deseja parar o envio de NFe e Sair do Programa?")) {
            addNumInstancia(-1);
            Arquivo.GravaLog(path + "envio.log", memo1.getText());
            this.dispose();
        }else{
            evt = null;
        }
    }//GEN-LAST:event_formInternalFrameClosing

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartActionPerformed
        inicia();
    }//GEN-LAST:event_btnStartActionPerformed

    private void btnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHideActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_btnHideActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnHide;
    private javax.swing.JButton btnStart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblResgistrosTotais;
    private javax.swing.JLabel lblTempo;
    private javax.swing.JTextArea memo1;
    // End of variables declaration//GEN-END:variables
}
