/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import base.FrmPrincipal;
import beans.*;
import dao.MenuDao;
import dao.UsuarioDao;
import funcoes.Util;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import utilitarios.DataBase;
import utilitarios.Extra;
import utilitarios.Utils;

/**
 *
 * @author Jean
 */
public final class FI_SysPermissoes extends javax.swing.JInternalFrame {

    static Integer numInstancias = 0;
    private Integer numMaxInstancias = 1;
    private final String versao = "v0.01";
    private FrmPrincipal princ = null;
    private Integer seqInstancias = 0;
    private Boolean exp = false;
    Boolean alteraTabela = false;
    private Usuario usuario = null;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getNumInstancias() {
        return numInstancias;
    }

    public void addNumInstancia(Integer i) {
        numInstancias += i;
        if (i > -1) {
            seqInstancias += 1;
        }
    }

    public FI_SysPermissoes(String title, OpcoesTela parametros, Usuario user) {
        if (numInstancias >= numMaxInstancias) {
            String msg = "Número máximo instâncias (" + numMaxInstancias + ") de " + title + " atingido!";
            Variaveis.addNewLog(msg, false, true, this);
        } else {
            initComponents();
            this.usuario = user;
            if (parametros.getShow_version()) {
                title += " [" + versao + "]";
            }
            this.setClosable(parametros.getClosable());
            this.setIconifiable(parametros.getIconifiable());
            this.setMaximizable(parametros.getMaximizable());
            this.setResizable(parametros.getResizable());
            this.setTitle(title + (seqInstancias > 0 ? (" - [" + String.valueOf(seqInstancias) + "]") : ""));
            this.princ = Variaveis.getFormularioPrincipal();
            princ.setLocalizacaoFrame(this);
            princ.Desktop.add(this, javax.swing.JLayeredPane.DEFAULT_LAYER);
            princ.Desktop.setComponentZOrder(this, 0);
            this.setVisible(parametros.getVisible());
            addNumInstancia(1);
            arvoreMenu.setModel(FrmPrincipal.getTreeModel());
            FrmPrincipal.updateTreeModel();
            btnCancelar.setEnabled(false);
            btnSalvar.setEnabled(false);
            setUsersCombo();
            cboxClasse.setEnabled(false);
            cboxClasse.setVisible(false);
            lblClasse.setVisible(false);
            cboxModulos.setEnabled(false);
            cboxModulos.setVisible(false);
            lblModulo.setVisible(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        listaUsuarios = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Usuario>());
        popupMenuTabela = new javax.swing.JPopupMenu();
        addUsersMenu = new javax.swing.JMenuItem();
        removeUserMenu = new javax.swing.JMenuItem();
        removeAllUserMenu = new javax.swing.JMenuItem();
        listaUsuariosSistema = org.jdesktop.observablecollections.ObservableCollections.observableList(new ArrayList<Usuario>());
        popupMenuUserSys = new javax.swing.JPopupMenu();
        menuAtualizar = new javax.swing.JMenuItem();
        pnlTabulacoes = new javax.swing.JTabbedPane();
        pnlMenu = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        arvoreMenu = new javax.swing.JTree();
        jBnExpande = new javax.swing.JButton();
        jBnAtualiza = new javax.swing.JButton();
        pnlOperacoes = new javax.swing.JPanel();
        btnAplicar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        cboxUsuario = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        cboxOperacoes = new javax.swing.JComboBox();
        lblClasse = new javax.swing.JLabel();
        cboxClasse = new javax.swing.JComboBox();
        lblModulo = new javax.swing.JLabel();
        cboxModulos = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPermitidos = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        pnlBotoes = new javax.swing.JPanel();
        btnSalvar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnAddUser = new javax.swing.JButton();

        addUsersMenu.setText("Adicionar Usuário(s)");
        addUsersMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addUsersMenuActionPerformed(evt);
            }
        });
        popupMenuTabela.add(addUsersMenu);

        removeUserMenu.setText("Remover usuário(s) selecionado(s)");
        removeUserMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeUserMenuActionPerformed(evt);
            }
        });
        popupMenuTabela.add(removeUserMenu);

        removeAllUserMenu.setText("Remover Todos");
        removeAllUserMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAllUserMenuActionPerformed(evt);
            }
        });
        popupMenuTabela.add(removeAllUserMenu);

        menuAtualizar.setText("Atualizar");
        menuAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAtualizarActionPerformed(evt);
            }
        });
        popupMenuUserSys.add(menuAtualizar);

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        pnlTabulacoes.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                pnlTabulacoesStateChanged(evt);
            }
        });

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        arvoreMenu.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        arvoreMenu.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                arvoreMenuValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(arvoreMenu);

        jBnExpande.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/expand.png"))); // NOI18N
        jBnExpande.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBnExpandeActionPerformed(evt);
            }
        });

        jBnAtualiza.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/reload.png"))); // NOI18N
        jBnAtualiza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBnAtualizaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlMenuLayout = new javax.swing.GroupLayout(pnlMenu);
        pnlMenu.setLayout(pnlMenuLayout);
        pnlMenuLayout.setHorizontalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                    .addGroup(pnlMenuLayout.createSequentialGroup()
                        .addComponent(jBnExpande, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBnAtualiza, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 194, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlMenuLayout.setVerticalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBnAtualiza)
                    .addComponent(jBnExpande)))
        );

        pnlTabulacoes.addTab("Menus", pnlMenu);

        btnAplicar.setText("Aplicar");
        btnAplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAplicarActionPerformed(evt);
            }
        });

        jLabel3.setText("Selecione o usuário destino:");

        jLabel2.setText("Operações:");

        cboxOperacoes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Copiar de outro usuário ->", "Aplicar todas as permissões", "Aplicar permissões para classe" }));
        cboxOperacoes.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboxOperacoesItemStateChanged(evt);
            }
        });

        lblClasse.setText("Chamada de Tela:");

        cboxClasse.setEditable(true);
        cboxClasse.setEnabled(false);

        lblModulo.setText("Módulo:");

        cboxModulos.setEnabled(false);
        cboxModulos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxModulosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlOperacoesLayout = new javax.swing.GroupLayout(pnlOperacoes);
        pnlOperacoes.setLayout(pnlOperacoesLayout);
        pnlOperacoesLayout.setHorizontalGroup(
            pnlOperacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOperacoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOperacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboxClasse, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlOperacoesLayout.createSequentialGroup()
                        .addGroup(pnlOperacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblClasse)
                            .addGroup(pnlOperacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(cboxOperacoes, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cboxUsuario, javax.swing.GroupLayout.Alignment.LEADING, 0, 269, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING))
                            .addComponent(lblModulo))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(cboxModulos, 0, 270, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlOperacoesLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAplicar)))
                .addContainerGap())
        );
        pnlOperacoesLayout.setVerticalGroup(
            pnlOperacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOperacoesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboxUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboxOperacoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblModulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboxModulos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblClasse)
                .addGap(2, 2, 2)
                .addComponent(cboxClasse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAplicar)
                .addContainerGap(222, Short.MAX_VALUE))
        );

        pnlTabulacoes.addTab("Operações com Permissões", pnlOperacoes);

        tabelaPermitidos.setComponentPopupMenu(popupMenuTabela);
        tabelaPermitidos.setInheritsPopupMenu(true);

        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, listaUsuarios, tabelaPermitidos);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${codigoUsuario}"));
        columnBinding.setColumnName("Codigo Usuario");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nomeUsuario}"));
        columnBinding.setColumnName("Nome Usuario");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${cargo}"));
        columnBinding.setColumnName("Cargo");
        columnBinding.setColumnClass(String.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane1.setViewportView(tabelaPermitidos);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Usuários com Permissões:");

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnAddUser.setText("Adicionar Usuário(s)");
        btnAddUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddUserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBotoesLayout = new javax.swing.GroupLayout(pnlBotoes);
        pnlBotoes.setLayout(pnlBotoesLayout);
        pnlBotoesLayout.setHorizontalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBotoesLayout.createSequentialGroup()
                .addComponent(btnAddUser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalvar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSair))
        );

        pnlBotoesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnCancelar, btnSair, btnSalvar});

        pnlBotoesLayout.setVerticalGroup(
            pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBotoesLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(pnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(btnSalvar)
                    .addComponent(btnSair)
                    .addComponent(btnCancelar)
                    .addComponent(btnAddUser)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlTabulacoes, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane1)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTabulacoes)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        addNumInstancia(-1);
    }//GEN-LAST:event_formInternalFrameClosing

    private void setUsersCombo() {
        cboxUsuario.removeAllItems();
        for (Usuario u : listaUsuariosSistema) {
            cboxUsuario.addItem(u);
        }
    }

    private void atualizaListaUserSys(Empresa empresa) {
        listaUsuariosSistema.removeAll(listaUsuariosSistema);
        UsuarioDao.listarUsuarios(empresa, listaUsuariosSistema);
        setUsersCombo();
    }

    private void arvoreMenuValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_arvoreMenuValueChanged
        if (arvoreMenu.getSelectionPath() != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) arvoreMenu.getLastSelectedPathComponent();
            if (node.getUserObject() instanceof Menu) {
                Menu item = (Menu) node.getUserObject();
                listaUsuarios.removeAll(listaUsuarios);
                btnSalvar.setEnabled(false);
                btnCancelar.setEnabled(false);
                if (item.getOperacao() != 0) {
                    UsuarioDao.buscaPermitidos(item.getOperacao(), Variaveis.getEmpresa(), listaUsuarios);
                }
            }
        }
    }//GEN-LAST:event_arvoreMenuValueChanged

    private void jBnExpandeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBnExpandeActionPerformed
        exp = !exp;
        Util.expandAll(arvoreMenu, exp);
    }//GEN-LAST:event_jBnExpandeActionPerformed

    private void jBnAtualizaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBnAtualizaActionPerformed
        arvoreMenu.setModel(FrmPrincipal.getTreeModel());
        FrmPrincipal.updateTreeModel();
        listaUsuarios.removeAll(listaUsuarios);
    }//GEN-LAST:event_jBnAtualizaActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        if (alteraTabela) {
            if (Extra.Pergunta(this, "Deseja descartar alterações não salvas?")) {
                try {
                    setClosed(true);
                } catch (PropertyVetoException e) {
                    Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
                }
            }
        } else {
            try {
                setClosed(true);
            } catch (PropertyVetoException e) {
                Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
            }
        }
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        if (alteraTabela) {
            if (Extra.Pergunta(this, "Descartar alterações não salvas?")) {
                arvoreMenu.setEnabled(true);
                arvoreMenuValueChanged(null);
                alteraTabela = false;
                arvoreMenu.setEnabled(true);
            }
        } else {
            arvoreMenu.setEnabled(true);
            arvoreMenuValueChanged(null);
            alteraTabela = false;
            arvoreMenu.setEnabled(true);
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void addUsersMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addUsersMenuActionPerformed
        alteraTabela = true;
        arvoreMenu.setEnabled(false);
        btnSalvar.setEnabled(true && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
        btnCancelar.setEnabled(true && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
        FI_CadFindUsuario user = (FI_CadFindUsuario) Util.execucaoDiretaClass("formularios.FI_CadFindUsuario", "Modulos-jSyscom.jar", "Localiza Usuario(s) - " + Util.getSource(seqInstancias, FI_SysPermissoes.class), new OpcoesTela(true, true, true, true, true, false), usuario);
        user.listaExport = listaUsuarios;
    }//GEN-LAST:event_addUsersMenuActionPerformed

    private void removeUserMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeUserMenuActionPerformed
        if (tabelaPermitidos.getSelectedRowCount() == 1) {
            if (Extra.Pergunta(this, "Confirma exclusão da permissão do usuário?")) {
                listaUsuarios.remove(tabelaPermitidos.getSelectedRow());
                alteraTabela = true;
                arvoreMenu.setEnabled(false);
                btnSalvar.setEnabled(true && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
                btnCancelar.setEnabled(true && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
            }
        } else if (tabelaPermitidos.getSelectedRowCount() > 1) {
            if (Extra.Pergunta(this, "Confirma exclusão das permissões selecionadas?")) {
                int[] array = tabelaPermitidos.getSelectedRows();
                ArrayList<Usuario> v = new ArrayList<Usuario>();
                for (int i : array) {
                    v.add(listaUsuarios.get(i));
                }
                for (Usuario veiculo : v) {
                    listaUsuarios.remove(veiculo);
                }
                alteraTabela = true;
                arvoreMenu.setEnabled(false);
                btnSalvar.setEnabled(true && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
                btnCancelar.setEnabled(true && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
            }
        }
    }//GEN-LAST:event_removeUserMenuActionPerformed

    private void removeAllUserMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeAllUserMenuActionPerformed
        if (Extra.Pergunta(this, "Confirma exclusão de todas as permissões deste menu?")) {
            listaUsuarios.removeAll(listaUsuarios);
            alteraTabela = true;
            arvoreMenu.setEnabled(false);
            btnSalvar.setEnabled(true && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
            btnCancelar.setEnabled(true && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
        }
    }//GEN-LAST:event_removeAllUserMenuActionPerformed

    private boolean salvar(Integer cd_acao, Empresa empresa) {
        try {
            String SQL = "delete from restricoes where cd_empresa = ? and cd_acao = ?";
            Object[] par = {empresa.getCodigo(), cd_acao};
            DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(),
                    Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
            SQL = "insert into restricoes(cd_acao, cd_usuario, cd_empresa) values(?,?,?)";
            for (Usuario user : listaUsuarios) {
                Object[] par2 = {cd_acao, user.getCodigoUsuario(), empresa.getCodigo()};
                DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(),
                        Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
            }
            return true;
        } catch (Exception e) {
            Variaveis.addNewLog(this.getClass().getName() + ":\n" + Utils.getStackTrace(e), false, true, this);
        }
        return false;
    }

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) arvoreMenu.getLastSelectedPathComponent();
        Menu item = (Menu) node.getUserObject();
        if (salvar(item.getOperacao(), Variaveis.getEmpresa())) {
            btnCancelar.setEnabled(false && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
            btnSalvar.setEnabled(false && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
            alteraTabela = false;
            arvoreMenu.setEnabled(true);
            Extra.Informacao(this, "Alterações aplicadas com sucesso!");
        } else {
            Extra.Informacao(this, "Erro ao aplicar alterações!");
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnAddUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddUserActionPerformed
        addUsersMenuActionPerformed(evt);
    }//GEN-LAST:event_btnAddUserActionPerformed

    private void pnlTabulacoesStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_pnlTabulacoesStateChanged
        listaUsuarios.removeAll(listaUsuarios);
        btnSalvar.setEnabled(false && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
        btnCancelar.setEnabled(false && (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)));
        arvoreMenu.setEnabled(true);
        if (pnlTabulacoes.getSelectedComponent().equals(pnlMenu)) {
            tabelaPermitidos.setRowSelectionAllowed(true);
        } else if (pnlTabulacoes.getSelectedComponent().equals(pnlOperacoes)) {
            tabelaPermitidos.setRowSelectionAllowed(false);
            atualizaListaUserSys(Variaveis.getEmpresa());
            cboxOperacoes.setSelectedIndex(0);
        }
    }//GEN-LAST:event_pnlTabulacoesStateChanged

    private void menuAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAtualizarActionPerformed
        atualizaListaUserSys(Variaveis.getEmpresa());
    }//GEN-LAST:event_menuAtualizarActionPerformed

    private void btnAplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAplicarActionPerformed
        if (cboxOperacoes.getSelectedIndex() == 0) {
            if (cboxUsuario.getSelectedItem() != null) {
                if (tabelaPermitidos.getRowCount() > 0) {
                    Usuario destino = ((Usuario) cboxUsuario.getSelectedItem());
                    if (Extra.Pergunta(this, "Confirma cópia das permissões para '" + destino.getNomeUsuario() + "' ?")) {
                        for (Usuario origem : listaUsuarios) {
                            if (UsuarioDao.copiarPermissoes(origem, destino)) {
                                Extra.Informacao(this, "Permissões copiadas com sucesso!");
                            } else {
                                Extra.Informacao(this, "Falha ao copiar algumas das permissões de '" + origem.getNomeUsuario() + "'!");
                            }
                        }
                    }
                } else {
                    Extra.Informacao(this, "Selecione um usuário com as permissões à serem copiadas!");
                }
            } else {
                Extra.Informacao(this, "Selecione um usuário destino para as permissões!");
            }
        } else if (cboxOperacoes.getSelectedIndex() == 1) {
            Usuario destino = ((Usuario) cboxUsuario.getSelectedItem());
            if (Extra.Pergunta(this, "Confirma aplicação de todas as permissões do sistema para '" + destino.getNomeUsuario() + "' ?")) {
                if (UsuarioDao.allRestrictionForUser(Variaveis.getEmpresa(), destino)) {
                    Extra.Informacao(this, "Permissões aplicadas com sucesso!");
                } else {
                    Extra.Informacao(this, "Falha ao aplicar permissões para '" + destino.getNomeUsuario() + "'!");
                }

            }
        } else if ((cboxOperacoes.getSelectedIndex() == 2) && (!cboxClasse.getSelectedItem().toString().trim().equals(""))) {
            if (!cboxClasse.getSelectedItem().toString().trim().equals("")) {
                Usuario destino = ((Usuario) cboxUsuario.getSelectedItem());
                if (MenuDao.classeExistente(cboxClasse.getSelectedItem().toString().trim())) {
                    if (UsuarioDao.setPermition(destino, cboxClasse.getSelectedItem().toString().trim(), true)) {
                        Extra.Informacao(this, "Permissão aplicada com sucesso!");
                    } else {
                        Extra.Informacao(this, "Falha ao aplicar permissão para '" + destino.getNomeUsuario() + "'!");
                    }
                } else {
                    Integer indexAcao = MenuDao.cadAcao(MenuDao.cadClasse(cboxModulos.getSelectedItem().toString().trim(), cboxClasse.getSelectedItem().toString().trim(), true, ""), "cd_classe", true);
                    if (indexAcao != -1) {
                        if (UsuarioDao.setPermition(usuario, indexAcao, true)) {
                            Extra.Informacao(this, "Permissão aplicada com sucesso!");
                        } else {
                            Extra.Informacao(this, "Falha ao aplicar permissão para '" + destino.getNomeUsuario() + "'!");
                        }
                    } else {
                        Extra.Informacao(this, "Falha ao aplicar permissão para '" + destino.getNomeUsuario() + "'!");
                    }
                }
            } else {
                Extra.Informacao(this, "Selecione ou digite o endereço da tela");
            }
        }
    }//GEN-LAST:event_btnAplicarActionPerformed

    private void cboxOperacoesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboxOperacoesItemStateChanged
        if (cboxOperacoes.getSelectedIndex() == 2) {
            cboxClasse.removeAllItems();
            cboxClasse.addItem("");
            for (String item : MenuDao.getListClasses()) {
                cboxClasse.addItem(item);
            }
            cboxClasse.setSelectedIndex(0);
            cboxModulos.removeAllItems();
            for (String s : MenuDao.getModulos()) {
                cboxModulos.addItem(s);
            }

            cboxClasse.setEnabled(true);
            cboxClasse.setVisible(true);
            lblClasse.setVisible(true);
            cboxModulos.setEnabled(true);
            cboxModulos.setVisible(true);
            lblModulo.setVisible(true);
            cboxClasse.requestFocus();
        } else {
            cboxClasse.setEnabled(false);
            cboxClasse.setVisible(false);
            cboxModulos.setEnabled(false);
            cboxModulos.setVisible(false);
            lblClasse.setVisible(false);
            lblModulo.setVisible(false);
        }
    }//GEN-LAST:event_cboxOperacoesItemStateChanged

    private void cboxModulosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxModulosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboxModulosActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addUsersMenu;
    private javax.swing.JTree arvoreMenu;
    private javax.swing.JButton btnAddUser;
    private javax.swing.JButton btnAplicar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox cboxClasse;
    private javax.swing.JComboBox cboxModulos;
    private javax.swing.JComboBox cboxOperacoes;
    private javax.swing.JComboBox cboxUsuario;
    private javax.swing.JButton jBnAtualiza;
    private javax.swing.JButton jBnExpande;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblClasse;
    private javax.swing.JLabel lblModulo;
    private java.util.List<Usuario> listaUsuarios;
    private java.util.List<Usuario> listaUsuariosSistema;
    private javax.swing.JMenuItem menuAtualizar;
    private javax.swing.JPanel pnlBotoes;
    private javax.swing.JPanel pnlMenu;
    private javax.swing.JPanel pnlOperacoes;
    private javax.swing.JTabbedPane pnlTabulacoes;
    private javax.swing.JPopupMenu popupMenuTabela;
    private javax.swing.JPopupMenu popupMenuUserSys;
    private javax.swing.JMenuItem removeAllUserMenu;
    private javax.swing.JMenuItem removeUserMenu;
    private javax.swing.JTable tabelaPermitidos;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
