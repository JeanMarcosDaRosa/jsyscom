/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.3                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          jsyscom.dez                                     */
/* Project name:          jSyscom - Sistema de Automacao Comercial        */
/* Author:                Jean Marcos da Rosa                             */
/* Script type:           Database drop script                            */
/* Created on:            2012-10-03 13:14                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop procedures                                                        */
/* ---------------------------------------------------------------------- */

DROP FUNCTION public.valida_licenca (cd_empresa Integer);

DROP FUNCTION public.aplica_licenca (cd_empresa Integer, data_exp date);

DROP FUNCTION public.set_all_restrictions_for_user (EMPRESA_CD integer, USUARIO_CD integer);

DROP FUNCTION public.copy_restrictions_user_for_user (EMPRESA_CD integer, USUARIO_O_CD integer, USUARIO_D_CD integer);

DROP FUNCTION public.delete_user (EMPRESA_CD integer, USUARIO_CD integer);

DROP FUNCTION public.delete_not_using_imagem_prod (empresa_cd integer);

DROP FUNCTION public.delete_not_using_wallpaper (empresa_cd integer);

DROP FUNCTION public.atualiza_data_garantia_os ();

/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */

ALTER TABLE public.lookandfeels DROP CONSTRAINT fk_lookandfeels_reference_modulos;

ALTER TABLE public.wallpaper DROP CONSTRAINT fk_wallpaper_reference_empresa;

ALTER TABLE public.config_empresa DROP CONSTRAINT fk_config_empresa_reference_empresa;

ALTER TABLE public.opcoes_tela DROP CONSTRAINT fk_opcoes_tela_reference_classe;

ALTER TABLE public.config_usuario DROP CONSTRAINT fk_config_usuario_reference_wallpaper;

ALTER TABLE public.config_usuario DROP CONSTRAINT fk_config_usuario_reference_lookandfeels;

ALTER TABLE public.config_usuario DROP CONSTRAINT fk_config_usuario_reference_empresa;

ALTER TABLE public.config_usuario DROP CONSTRAINT fk_config_usuario_reference_usuario;

ALTER TABLE public.menu DROP CONSTRAINT fk_menu_reference_acao;

ALTER TABLE public.empresa DROP CONSTRAINT fk_empresa_reference_logradouro;

ALTER TABLE public.classe DROP CONSTRAINT fk_classe_reference_modulos;

ALTER TABLE public.classe DROP CONSTRAINT fk_acao_classe;

ALTER TABLE public.operacao DROP CONSTRAINT fk_acao_operacao;

ALTER TABLE public.usuario DROP CONSTRAINT fk_usuario_reference_funcionario;

ALTER TABLE public.usuario DROP CONSTRAINT fk_usuario_reference_empresa;

ALTER TABLE public.restricoes DROP CONSTRAINT fk_restricoes_reference_acao;

ALTER TABLE public.restricoes DROP CONSTRAINT fk_restricoes_reference_usuario;

ALTER TABLE public.restricoes DROP CONSTRAINT fk_restricoes_reference_empresa;

ALTER TABLE public.logs DROP CONSTRAINT fk_logs_reference_empresa;

ALTER TABLE public.logs DROP CONSTRAINT fk_logs_reference_usuario;

ALTER TABLE public.conta_email DROP CONSTRAINT fk_conta_email_reference_empresa;

ALTER TABLE public.uf DROP CONSTRAINT fk_uf_reference_pais;

ALTER TABLE public.cidade DROP CONSTRAINT fk_cidade_reference_uf;

ALTER TABLE public.bairro DROP CONSTRAINT fk_bairro_reference_cidade;

ALTER TABLE public.logradouro DROP CONSTRAINT fk_logradouro_reference_bairro;

ALTER TABLE public.pessoa DROP CONSTRAINT fk_pessoa_reference_empresa;

ALTER TABLE public.cliente DROP CONSTRAINT fk_cliente_reference_pessoa;

ALTER TABLE public.cliente DROP CONSTRAINT fk_cliente_reference_empresa;

ALTER TABLE public.fornecedor DROP CONSTRAINT fk_fornecedor_reference_pessoa;

ALTER TABLE public.fornecedor DROP CONSTRAINT fk_fornecedor_reference_empresa;

ALTER TABLE public.unidade_medida DROP CONSTRAINT fk_empresa_unidade_medida;

ALTER TABLE public.forma_pagamento DROP CONSTRAINT fk_forma_pagamento_reference_empresa;

ALTER TABLE public.produto DROP CONSTRAINT fk_produto_reference_unidade_medida;

ALTER TABLE public.produto DROP CONSTRAINT fk_produto_reference_empresa;

ALTER TABLE public.produto DROP CONSTRAINT fk_produto_reference_categoria_prod;

ALTER TABLE public.produto DROP CONSTRAINT fk_produto_reference_imagem_prod;

ALTER TABLE public.produto DROP CONSTRAINT fk_produto_reference_local_prod;

ALTER TABLE public.compra_cab DROP CONSTRAINT fk_compra_cab_reference_fornecedor;

ALTER TABLE public.compra_cab DROP CONSTRAINT fk_compra_cab_reference_empresa;

ALTER TABLE public.compra_cab DROP CONSTRAINT fk_usuario_compra_cab;

ALTER TABLE public.compra_det DROP CONSTRAINT fk_compra_det_reference_produto;

ALTER TABLE public.compra_det DROP CONSTRAINT fk_compra_det_reference_empresa;

ALTER TABLE public.compra_det DROP CONSTRAINT fk_compra_det_reference_compra_cab;

ALTER TABLE public.ean_prod DROP CONSTRAINT fk_ean_prod_reference_produto;

ALTER TABLE public.ean_prod DROP CONSTRAINT fk_ean_prod_reference_empresa;

ALTER TABLE public.funcionario DROP CONSTRAINT fk_funcionario_reference_pessoa;

ALTER TABLE public.funcionario DROP CONSTRAINT fk_funcionario_reference_empresa;

ALTER TABLE public.venda_cab DROP CONSTRAINT fk_venda_cab_reference_empresa;

ALTER TABLE public.venda_cab DROP CONSTRAINT fk_venda_cab_reference_cliente;

ALTER TABLE public.venda_cab DROP CONSTRAINT fk_usuario_venda_cab;

ALTER TABLE public.parcelamento DROP CONSTRAINT fk_parcelamento_reference_empresa;

ALTER TABLE public.venda_det DROP CONSTRAINT fk_venda_det_reference_venda_cab;

ALTER TABLE public.venda_det DROP CONSTRAINT fk_venda_det_reference_empresa;

ALTER TABLE public.venda_det DROP CONSTRAINT fk_venda_det_reference_produto;

ALTER TABLE public.autorun DROP CONSTRAINT fk_autorun_reference_acao;

ALTER TABLE public.autorun DROP CONSTRAINT fk_autorun_reference_usuario;

ALTER TABLE public.autorun DROP CONSTRAINT fk_autorun_reference_empresa;

ALTER TABLE public.licenca DROP CONSTRAINT fk_licenca_reference_empresa;

ALTER TABLE public.aviso DROP CONSTRAINT fk_aviso_reference_empresa;

ALTER TABLE public.aviso DROP CONSTRAINT fk_aviso_reference_usuario;

ALTER TABLE public.categoria_prod DROP CONSTRAINT fk_categoria_prod_reference_empresa;

ALTER TABLE public.imagem_prod DROP CONSTRAINT fk_imagem_prod_reference_empresa;

ALTER TABLE public.estoque_prod DROP CONSTRAINT fk_estoque_prod_reference_produto;

ALTER TABLE public.fornecedor_prod DROP CONSTRAINT fk_fornecedor_prod_reference_fornecedor;

ALTER TABLE public.fornecedor_prod DROP CONSTRAINT fk_fornecedor_prod_reference_produto;

ALTER TABLE public.contato DROP CONSTRAINT fk_contato_reference_pessoa;

ALTER TABLE public.endereco_pessoa DROP CONSTRAINT fk_endereco_pessoa_reference_pessoa;

ALTER TABLE public.endereco_pessoa DROP CONSTRAINT fk_endereco_pessoa_reference_logradouro;

ALTER TABLE public.endereco_pessoa DROP CONSTRAINT fk_endereco_pessoa_reference_empresa;

ALTER TABLE public.parcela DROP CONSTRAINT fk_parcela_reference_parcelamento;

ALTER TABLE public.local_prod DROP CONSTRAINT fk_local_prod_reference_empresa;

ALTER TABLE public.os_cab DROP CONSTRAINT fk_cliente_os_cab;

ALTER TABLE public.os_cab DROP CONSTRAINT fk_usuario_os_cab;

ALTER TABLE public.os_cab DROP CONSTRAINT fk_contato_os_cab;

ALTER TABLE public.os_cab DROP CONSTRAINT fk_endereco_pessoa_os_cab;

ALTER TABLE public.os_cab DROP CONSTRAINT fk_equip_os_cab;

ALTER TABLE public.os_cab DROP CONSTRAINT fk_local_prod_os_cab;

ALTER TABLE public.os_cab DROP CONSTRAINT fk_movimento_os_cab;

ALTER TABLE public.os_prod_det DROP CONSTRAINT fk_os_cab_os_prod_det;

ALTER TABLE public.os_prod_det DROP CONSTRAINT fk_produto_os_prod_det;

ALTER TABLE public.os_servico_det DROP CONSTRAINT fk_os_cab_os_servico_det;

ALTER TABLE public.os_servico_det DROP CONSTRAINT fk_servico_os_servico_det;

ALTER TABLE public.servico DROP CONSTRAINT fk_empresa_servico;

ALTER TABLE public.equip DROP CONSTRAINT fk_empresa_equip;

ALTER TABLE public.equip DROP CONSTRAINT fk_tipo_equip_equip;

ALTER TABLE public.equip DROP CONSTRAINT fk_conexao_equip_equip;

ALTER TABLE public.equip DROP CONSTRAINT fk_cliente_equip;

ALTER TABLE public.tipo_equip DROP CONSTRAINT fk_empresa_tipo_equip;

ALTER TABLE public.os_tst_equip DROP CONSTRAINT fk_os_cab_os_tst_equip;

ALTER TABLE public.os_tst_equip DROP CONSTRAINT fk_equip_os_tst_equip;

ALTER TABLE public.os_envolvidos DROP CONSTRAINT fk_os_cab_os_envolvidos;

ALTER TABLE public.os_envolvidos DROP CONSTRAINT fk_funcionario_os_envolvidos;

ALTER TABLE public.orcamento DROP CONSTRAINT fk_os_cab_orcamento;

ALTER TABLE public.relatorio DROP CONSTRAINT fk_acao_relatorio;

/* ---------------------------------------------------------------------- */
/* Drop table "orcamento"                                                 */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.orcamento DROP CONSTRAINT PK_orcamento;

/* Drop table */

DROP TABLE public.orcamento;

/* ---------------------------------------------------------------------- */
/* Drop table "os_envolvidos"                                             */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.os_envolvidos DROP CONSTRAINT PK_os_envolvidos;

/* Drop table */

DROP TABLE public.os_envolvidos;

/* ---------------------------------------------------------------------- */
/* Drop table "os_tst_equip"                                              */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.os_tst_equip DROP CONSTRAINT PK_os_tst_equip;

/* Drop table */

DROP TABLE public.os_tst_equip;

/* ---------------------------------------------------------------------- */
/* Drop table "os_servico_det"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.os_servico_det DROP CONSTRAINT PK_os_servico_det;

/* Drop table */

DROP TABLE public.os_servico_det;

/* ---------------------------------------------------------------------- */
/* Drop table "os_prod_det"                                               */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.os_prod_det DROP CONSTRAINT PK_os_prod_det;

/* Drop table */

DROP TABLE public.os_prod_det;

/* ---------------------------------------------------------------------- */
/* Drop table "os_cab"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.os_cab DROP CONSTRAINT PK_os_cab;

/* Drop table */

DROP TABLE public.os_cab;

/* ---------------------------------------------------------------------- */
/* Drop table "config_usuario"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.config_usuario DROP CONSTRAINT pk_config;

/* Drop table */

DROP TABLE public.config_usuario;

/* ---------------------------------------------------------------------- */
/* Drop table "equip"                                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.equip DROP CONSTRAINT PK_equip;

/* Drop table */

DROP TABLE public.equip;

/* ---------------------------------------------------------------------- */
/* Drop table "fornecedor_prod"                                           */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.fornecedor_prod DROP CONSTRAINT PK_fornecedor_prod;

/* Drop table */

DROP TABLE public.fornecedor_prod;

/* ---------------------------------------------------------------------- */
/* Drop table "estoque_prod"                                              */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.estoque_prod DROP CONSTRAINT PK_estoque_prod;

/* Drop table */

DROP TABLE public.estoque_prod;

/* ---------------------------------------------------------------------- */
/* Drop table "aviso"                                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.aviso DROP CONSTRAINT PK_aviso;

/* Drop table */

DROP TABLE public.aviso;

/* ---------------------------------------------------------------------- */
/* Drop table "autorun"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.autorun DROP CONSTRAINT PK_autorun;

/* Drop table */

DROP TABLE public.autorun;

/* ---------------------------------------------------------------------- */
/* Drop table "venda_det"                                                 */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.venda_det DROP CONSTRAINT PK_venda_det;

/* Drop table */

DROP TABLE public.venda_det;

/* ---------------------------------------------------------------------- */
/* Drop table "venda_cab"                                                 */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.venda_cab DROP CONSTRAINT PK_venda_cab;

/* Drop table */

DROP TABLE public.venda_cab;

/* ---------------------------------------------------------------------- */
/* Drop table "ean_prod"                                                  */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.ean_prod DROP CONSTRAINT PK_ean_prod;

/* Drop table */

DROP TABLE public.ean_prod;

/* ---------------------------------------------------------------------- */
/* Drop table "compra_det"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.compra_det DROP CONSTRAINT PK_compra_det;

/* Drop table */

DROP TABLE public.compra_det;

/* ---------------------------------------------------------------------- */
/* Drop table "compra_cab"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.compra_cab DROP CONSTRAINT PK_compra_cab;

/* Drop table */

DROP TABLE public.compra_cab;

/* ---------------------------------------------------------------------- */
/* Drop table "produto"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.produto DROP CONSTRAINT PK_produto;

/* Drop table */

DROP TABLE public.produto;

/* ---------------------------------------------------------------------- */
/* Drop table "logs"                                                      */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.logs DROP CONSTRAINT pk_logs;

/* Drop table */

DROP TABLE public.logs;

/* ---------------------------------------------------------------------- */
/* Drop table "restricoes"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.restricoes DROP CONSTRAINT pk_restricoes;

/* Drop table */

DROP TABLE public.restricoes;

/* ---------------------------------------------------------------------- */
/* Drop table "usuario"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.usuario DROP CONSTRAINT pk_usuario;

/* Drop table */

DROP TABLE public.usuario;

/* ---------------------------------------------------------------------- */
/* Drop table "config_empresa"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.config_empresa DROP CONSTRAINT pk_config_geral;

/* Drop table */

DROP TABLE public.config_empresa;

/* ---------------------------------------------------------------------- */
/* Drop table "wallpaper"                                                 */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.wallpaper DROP CONSTRAINT imagem_pkey;

/* Drop table */

DROP TABLE public.wallpaper;

/* ---------------------------------------------------------------------- */
/* Drop table "tipo_equip"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.tipo_equip DROP CONSTRAINT PK_tipo_equip;

/* Drop table */

DROP TABLE public.tipo_equip;

/* ---------------------------------------------------------------------- */
/* Drop table "servico"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.servico DROP CONSTRAINT PK_servico;

/* Drop table */

DROP TABLE public.servico;

/* ---------------------------------------------------------------------- */
/* Drop table "local_prod"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.local_prod DROP CONSTRAINT PK_local_prod;

/* Drop table */

DROP TABLE public.local_prod;

/* ---------------------------------------------------------------------- */
/* Drop table "parcela"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.parcela DROP CONSTRAINT PK_parcela;

/* Drop table */

DROP TABLE public.parcela;

/* ---------------------------------------------------------------------- */
/* Drop table "endereco_pessoa"                                           */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.endereco_pessoa DROP CONSTRAINT PK_endereco_pessoa;

/* Drop table */

DROP TABLE public.endereco_pessoa;

/* ---------------------------------------------------------------------- */
/* Drop table "contato"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.contato DROP CONSTRAINT PK_contato;

/* Drop table */

DROP TABLE public.contato;

/* ---------------------------------------------------------------------- */
/* Drop table "imagem_prod"                                               */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.imagem_prod DROP CONSTRAINT PK_imagem_prod;

/* Drop table */

DROP TABLE public.imagem_prod;

/* ---------------------------------------------------------------------- */
/* Drop table "categoria_prod"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.categoria_prod DROP CONSTRAINT PK_categoria_prod;

/* Drop table */

DROP TABLE public.categoria_prod;

/* ---------------------------------------------------------------------- */
/* Drop table "licenca"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.licenca DROP CONSTRAINT PK_licenca;

/* Drop table */

DROP TABLE public.licenca;

/* ---------------------------------------------------------------------- */
/* Drop table "parcelamento"                                              */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.parcelamento DROP CONSTRAINT PK_parcelamento;

/* Drop table */

DROP TABLE public.parcelamento;

/* ---------------------------------------------------------------------- */
/* Drop table "funcionario"                                               */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.funcionario DROP CONSTRAINT PK_funcionario;

/* Drop table */

DROP TABLE public.funcionario;

/* ---------------------------------------------------------------------- */
/* Drop table "forma_pagamento"                                           */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.forma_pagamento DROP CONSTRAINT PK_forma_pagamento;

/* Drop table */

DROP TABLE public.forma_pagamento;

/* ---------------------------------------------------------------------- */
/* Drop table "unidade_medida"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.unidade_medida DROP CONSTRAINT PK_unidade_medida;

/* Drop table */

DROP TABLE public.unidade_medida;

/* ---------------------------------------------------------------------- */
/* Drop table "fornecedor"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.fornecedor DROP CONSTRAINT PK_fornecedor;

/* Drop table */

DROP TABLE public.fornecedor;

/* ---------------------------------------------------------------------- */
/* Drop table "cliente"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.cliente DROP CONSTRAINT PK_cliente;

/* Drop table */

DROP TABLE public.cliente;

/* ---------------------------------------------------------------------- */
/* Drop table "pessoa"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.pessoa DROP CONSTRAINT PK_pessoa;

/* Drop table */

DROP TABLE public.pessoa;

/* ---------------------------------------------------------------------- */
/* Drop table "conta_email"                                               */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.conta_email DROP CONSTRAINT PK_CONTA_EMAIL;

/* Drop table */

DROP TABLE public.conta_email;

/* ---------------------------------------------------------------------- */
/* Drop table "empresa"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.empresa DROP CONSTRAINT pk_empresa;

/* Drop table */

DROP TABLE public.empresa;

/* ---------------------------------------------------------------------- */
/* Drop table "opcoes_tela"                                               */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.opcoes_tela DROP CONSTRAINT pk_opcoes_tela;

/* Drop table */

DROP TABLE public.opcoes_tela;

/* ---------------------------------------------------------------------- */
/* Drop table "logradouro"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.logradouro DROP CONSTRAINT PK_logradouro;

/* Drop table */

DROP TABLE public.logradouro;

/* ---------------------------------------------------------------------- */
/* Drop table "bairro"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.bairro DROP CONSTRAINT PK_bairro;

/* Drop table */

DROP TABLE public.bairro;

/* ---------------------------------------------------------------------- */
/* Drop table "cidade"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.cidade DROP CONSTRAINT PK_cidade;

/* Drop table */

DROP TABLE public.cidade;

/* ---------------------------------------------------------------------- */
/* Drop table "uf"                                                        */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.uf DROP CONSTRAINT PK_uf;

/* Drop table */

DROP TABLE public.uf;

/* ---------------------------------------------------------------------- */
/* Drop table "operacao"                                                  */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.operacao DROP CONSTRAINT pk_operacao;

/* Drop table */

DROP TABLE public.operacao;

/* ---------------------------------------------------------------------- */
/* Drop table "classe"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.classe DROP CONSTRAINT pk_classe;

/* Drop table */

DROP TABLE public.classe;

/* ---------------------------------------------------------------------- */
/* Drop table "menu"                                                      */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.menu DROP CONSTRAINT pk_menu;

/* Drop table */

DROP TABLE public.menu;

/* ---------------------------------------------------------------------- */
/* Drop table "lookandfeels"                                              */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.lookandfeels DROP CONSTRAINT pk_lookandfeels;

/* Drop table */

DROP TABLE public.lookandfeels;

/* ---------------------------------------------------------------------- */
/* Drop table "relatorio"                                                 */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.relatorio DROP CONSTRAINT PK_relatorio;

/* Drop table */

DROP TABLE public.relatorio;

/* ---------------------------------------------------------------------- */
/* Drop table "movimento"                                                 */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.movimento DROP CONSTRAINT PK_movimento;

/* Drop table */

DROP TABLE public.movimento;

/* ---------------------------------------------------------------------- */
/* Drop table "conexao_equip"                                             */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.conexao_equip DROP CONSTRAINT PK_conexao_equip;

/* Drop table */

DROP TABLE public.conexao_equip;

/* ---------------------------------------------------------------------- */
/* Drop table "pais"                                                      */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.pais DROP CONSTRAINT PK_pais;

/* Drop table */

DROP TABLE public.pais;

/* ---------------------------------------------------------------------- */
/* Drop table "acao"                                                      */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.acao DROP CONSTRAINT pk_acao;

/* Drop table */

DROP TABLE public.acao;

/* ---------------------------------------------------------------------- */
/* Drop table "modulos"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE public.modulos DROP CONSTRAINT pk_binarys;

/* Drop table */

DROP TABLE public.modulos;
