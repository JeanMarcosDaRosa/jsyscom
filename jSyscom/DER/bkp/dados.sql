--
-- Data for table public.empresa (OID = 25337) (LIMIT 0,2)
--

INSERT INTO empresa (cd_empresa, cd_logradouro, razao_social_empresa, fantasia_empresa, ie_empresa, cnpj_empresa, fone_empresa, fax_empresa, responsavel_empresa, numero_endereco_empresa, complemento_endereco_empresa, email_empresa)
VALUES (0, NULL, 'Desenvolvimento', 'Desenvolvimento', '', '', '05433116064', '05433116460', 'Jean Marcos da Rosa', 587, NULL, 'jeancaraca@hotmail.com');

INSERT INTO empresa (cd_empresa, cd_logradouro, razao_social_empresa, fantasia_empresa, ie_empresa, cnpj_empresa, fone_empresa, fax_empresa, responsavel_empresa, numero_endereco_empresa, complemento_endereco_empresa, email_empresa)
VALUES (1, NULL, 'Saicon', 'Roberto Chiesa Bartelmebs & CIA LTDA', '0910310947', '12039502000105', '05433273535', '', 'Roberto Chiesa Bartelmebs', 230, NULL, 'saicon@saiconrs.com.br');


--
-- Data for table public.forma_pagamento (OID = 19462) (LIMIT 0,2)
--
INSERT INTO forma_pagamento(cd_empresa, cd_forma_pagamento, nome_forma_pagamento, descricao_forma_pagamento)
VALUES (0, 1, 'DINHEIRO', '');

INSERT INTO forma_pagamento(cd_empresa, cd_forma_pagamento, nome_forma_pagamento, descricao_forma_pagamento)
VALUES (0, 2, 'BOLETO', '');

INSERT INTO forma_pagamento(cd_empresa, cd_forma_pagamento, nome_forma_pagamento, descricao_forma_pagamento)
VALUES (0, 3, 'CHEQUE', '');

INSERT INTO forma_pagamento(cd_empresa, cd_forma_pagamento, nome_forma_pagamento, descricao_forma_pagamento)
VALUES (0, 4, 'PARCELAMENTO', '');


--
-- Data for table public.unidade_medida (OID = 19462) (LIMIT 0,2)
--
INSERT INTO unidade_medida(cd_empresa, sigla_unidade_m, nome_unidade_m)
VALUES (0, 'UN', 'UNIDADE');

INSERT INTO unidade_medida(cd_empresa, sigla_unidade_m, nome_unidade_m)
VALUES (0, 'M', 'METRO');

INSERT INTO unidade_medida(cd_empresa, sigla_unidade_m, nome_unidade_m)
VALUES (0, 'CM', 'CENT�METRO');

INSERT INTO unidade_medida(cd_empresa, sigla_unidade_m, nome_unidade_m)
VALUES (0, 'KG', 'QUILOGRAMA');

INSERT INTO unidade_medida(cd_empresa, sigla_unidade_m, nome_unidade_m)
VALUES (0, 'LT', 'LITRO');

INSERT INTO unidade_medida(cd_empresa, sigla_unidade_m, nome_unidade_m)
VALUES (0, 'CX', 'CAIXA');

INSERT INTO unidade_medida(cd_empresa, sigla_unidade_m, nome_unidade_m)
VALUES (0, 'H', 'HORA');


--
-- Data for table public.tipo_equip (OID = 19462) (LIMIT 0,2)
--
INSERT INTO tipo_equip(cd_empresa, cd_tipo_equip, descricao_tipo_equip)
VALUES (0, 1, 'MOTOR');

INSERT INTO tipo_equip(cd_empresa, cd_tipo_equip, descricao_tipo_equip)
VALUES (0, 2, 'INVERSOR');


--
-- Data for table public.local_prod (OID = 19462) (LIMIT 0,2)
--
INSERT INTO local_prod(cd_empresa, cd_local_prod, descricao_local_prod)
VALUES (0, 1, 'G�NDOLA A');

INSERT INTO local_prod(cd_empresa, cd_local_prod, descricao_local_prod)
VALUES (0, 2, 'BOX 1');


--
-- Data for table public.categoria_prod (OID = 19462) (LIMIT 0,2)
--
INSERT INTO categoria_prod(cd_empresa, cd_categoria_prod, descricao_categoria_prod, margem_categoria_prod)
VALUES (0, 1, 'INFORMATICA', 20);

INSERT INTO categoria_prod(cd_empresa, cd_categoria_prod, descricao_categoria_prod, margem_categoria_prod)
VALUES (0, 2, 'RESISTORES', 30);


--
-- Data for table public.wallpaper (OID = 19462) (LIMIT 0,2)
--
INSERT INTO wallpaper(cd_empresa, cd_wallpaper, nome_wallpaper, blob_wallpaper, md5_wallpaper)
VALUES (0, 1, 'fundo_prata', lo_import('/cep/imagens/fundo_prata.jpg'), '93312aac6a8a56438ea47aca15b172a2');

INSERT INTO wallpaper(cd_empresa, cd_wallpaper, nome_wallpaper, blob_wallpaper, md5_wallpaper)
VALUES (0, 2, 'saicon_logo', lo_import('/cep/imagens/saicon.png'), 'f851f93671f95e03fa986abd8c9b89ab');

INSERT INTO wallpaper(cd_empresa, cd_wallpaper, nome_wallpaper, blob_wallpaper, md5_wallpaper)
VALUES (0, 3, 'motorcycle', lo_import('/cep/imagens/motorcycle.jpg'), 'f851f9367af55e03fa986abd8c9b89ab');


--
-- Data for table public.imagem_prod (OID = 19462) (LIMIT 0,2)
--
INSERT INTO imagem_prod(cd_empresa, cd_imagem_prod, nome_imagem_prod, blob_imagem_prod, md5_imagem_prod)
VALUES (0, 1, 'sem_imagem', lo_import('/cep/imagens/sem_imagem.gif'), '93312aac6a8a56438ea47aca15b172a2');


--
-- Data for table public.modulos (OID = 17262) (LIMIT 0,4)
--
INSERT INTO modulos (cd_modulos, nome_modulos, bind_modulos, md5_modulos, descricao_modulos, ativo_modulos)
VALUES (2, 'JTattoo.jar', NULL, NULL, NULL, true);

INSERT INTO modulos (cd_modulos, nome_modulos, bind_modulos, md5_modulos, descricao_modulos, ativo_modulos)
VALUES (3, 'quaqua.jar', NULL, NULL, NULL, true);

INSERT INTO modulos (cd_modulos, nome_modulos, bind_modulos, md5_modulos, descricao_modulos, ativo_modulos)
VALUES (4, 'ilf-gpl.jar', NULL, NULL, NULL, true);

INSERT INTO modulos (cd_modulos, nome_modulos, bind_modulos, md5_modulos, descricao_modulos, ativo_modulos)
VALUES (1, 'jSyscom-Modulos.jar', NULL, NULL, NULL, true);

INSERT INTO modulos (cd_modulos, nome_modulos, bind_modulos, md5_modulos, descricao_modulos, ativo_modulos)
VALUES (5, 'jSyscom-Beans.jar', NULL, NULL, NULL, true);

--
-- Data for table public.lookandfeels (OID = 19248) (LIMIT 0,18)
--
INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (1, 'com.sun.java.swing.plaf.motif.MotifLookAndFeel', NULL);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (2, 'com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel', NULL);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (3, 'javax.swing.plaf.metal.MetalLookAndFeel', NULL);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (4, 'com.jtattoo.plaf.acryl.AcrylLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (5, 'ch.randelshofer.quaqua.leopard.Quaqua15LeopardCrossPlatformLookAndFeel', 3);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (6, 'com.jtattoo.plaf.aluminium.AluminiumLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (7, 'com.jtattoo.plaf.bernstein.BernsteinLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (8, 'com.jtattoo.plaf.fast.FastLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (9, 'com.jtattoo.plaf.graphite.GraphiteLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (10, 'com.jtattoo.plaf.hifi.HiFiLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (11, 'com.jtattoo.plaf.luna.LunaLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (12, 'com.jtattoo.plaf.mcwin.McWinLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (13, 'com.jtattoo.plaf.mint.MintLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (14, 'com.jtattoo.plaf.noire.NoireLookAndFeel', 2);

INSERT INTO lookandfeels (cd_lookandfeels, nome_lookandfeels, cd_modulos)
VALUES (15, 'com.jtattoo.plaf.smart.SmartLookAndFeel', 2);

--
-- Data for table public.usuario (OID = 25375) (LIMIT 0,2)
--
INSERT INTO usuario (cd_usuario, cd_empresa, cd_funcionario, nome_usuario, senha_usuario, descricao_usuario, data_cadastro_usuario)
VALUES (1, 0, NULL, 'ROOT', 'c0170645287a2a770177a5afb706c4e2', NULL, current_date);

--
-- Data for table public.config_usuario (OID = 17251) (LIMIT 0,2)
--
INSERT INTO config_usuario (cd_empresa, cd_usuario, cd_wallpaper, cd_lookandfeels, nome_menu_config, grava_logs_config, distancia_frame_config)
VALUES (0, 1, 2, 2, 'Menu Principal', true, 20);

--
-- Data for table public.licenca (OID = 25661) (LIMIT 0,5)
--
INSERT INTO licenca (cd_empresa, data_expira)
VALUES (0, '2013-07-10');

--
-- Data for table public.config_empresa (OID = 19206) (LIMIT 0,1)
--
INSERT INTO config_empresa (cd_empresa, titulo_software, atu_mod)
VALUES (0, '@title [@company] -> Vers�o: @version', false);

--
-- Data for table public.acao (OID = 25386) (LIMIT 0,19)
--
INSERT INTO acao (cd_acao)
VALUES (1);

INSERT INTO acao (cd_acao)
VALUES (2);

INSERT INTO acao (cd_acao)
VALUES (3);

INSERT INTO acao (cd_acao)
VALUES (4);

INSERT INTO acao (cd_acao)
VALUES (5);

INSERT INTO acao (cd_acao)
VALUES (6);

INSERT INTO acao (cd_acao)
VALUES (7);

INSERT INTO acao (cd_acao)
VALUES (8);

INSERT INTO acao (cd_acao)
VALUES (9);

INSERT INTO acao (cd_acao)
VALUES (10);

INSERT INTO acao (cd_acao)
VALUES (11);

INSERT INTO acao (cd_acao)
VALUES (12);

INSERT INTO acao (cd_acao)
VALUES (13);

INSERT INTO acao (cd_acao)
VALUES (14);

INSERT INTO acao (cd_acao)
VALUES (15);

INSERT INTO acao (cd_acao)
VALUES (16);

INSERT INTO acao (cd_acao)
VALUES (17);

INSERT INTO acao (cd_acao)
VALUES (18);

INSERT INTO acao (cd_acao)
VALUES (19);

INSERT INTO acao (cd_acao)
VALUES (20);

INSERT INTO acao (cd_acao)
VALUES (21);

INSERT INTO acao (cd_acao)
VALUES (22);

INSERT INTO acao (cd_acao)
VALUES (23);

INSERT INTO acao (cd_acao)
VALUES (24);

INSERT INTO acao (cd_acao)
VALUES (25);

INSERT INTO acao (cd_acao)
VALUES (26);

INSERT INTO acao (cd_acao)
VALUES (27);


--
-- Data for table public.relatorio (OID = 25355) (LIMIT 0,21)
--
INSERT INTO relatorio (cd_acao, cd_relatorio, nome_relatorio)
VALUES (27, 1, 'relatorioCliente.jasper');


--
-- Data for table public.classe (OID = 25355) (LIMIT 0,21)
--
INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (2, 1, 1, 'formularios.FI_CadFindCEP', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (3, 2, 1, 'formularios.FI_CadFindPessoa', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (4, 3, 1, 'formularios.FI_CadFindUsuario', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (5, 4, 1, 'formularios.FI_CadMenu', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (6, 5, 1, 'formularios.FI_CadPessoa', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (7, 6, 1, 'formularios.FI_CadUsuario', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (8, 7, 1, 'formularios.FI_EnviaXML', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (9, 8, 1, 'formularios.FI_SysAbout', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (10, 9, 1, 'formularios.FI_SysAvisos', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (11, 10, 1, 'formularios.FI_SysConfig', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (12, 11, 1, 'formularios.FI_SysConfigGeral', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (13, 12, 1, 'formularios.FI_SysExecutaClasse', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (14, 13, 1, 'formularios.FI_SysLogs', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (15, 14, 1, 'formularios.FI_SysPermissoes', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (16, 15, 1, 'formularios.FI_SysSQL', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (17, 16, 1, 'formularios.FI_SysSaida', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (18, 17, 1, 'formularios.FI_CadFindProduto', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (19, 18, 1, 'formularios.FI_CadUnidade', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (20, 19, 1, 'formularios.FI_CadProduto', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (1, 20, 1, 'incompletos._ExemploBase', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (21, 21, 1, 'formularios.FI_CadLocalizacao', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (22, 22, 1, 'formularios.FI_CadServico', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (23, 23, 1, 'formularios.FI_CadOS', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (24, 24, 1, 'formularios.FI_CadCategoriaProd', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (25, 25, 1, 'formularios.FI_CadTipoEquip', '');

INSERT INTO classe (cd_acao, cd_classe, cd_modulos, nome_classe, descricao_classe)
VALUES (26, 26, 1, 'formularios.FI_CadFindOS', '');



--
-- Data for table public.opcoes_tela (OID = 25291) (LIMIT 0,21)
--
INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (1, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (2, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (3, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (4, true, true, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (5, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (6, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (7, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (8, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (9, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (10, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (11, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (12, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (13, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (14, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (16, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (15, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (17, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (18, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (19, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (20, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (21, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (22, true, false, false, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (23, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (24, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (25, true, true, true, true, true, true);

INSERT INTO opcoes_tela (cd_classe, iconifiable, maximizable, resizable, closable, visible, show_version)
VALUES (26, true, true, true, true, true, true);


--
-- Data for table public.menu (OID = 25326) (LIMIT 0,97)
--
INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (1, NULL, 'Comercial', NULL);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (2, NULL, 'Cadastros', 1);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (3, 1, 'Entrada de Estoque', 2);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (4, 1, 'Nota Fiscal de Entrada e Saida', 2);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (5, 1, 'Saida de Estoque', 2);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (6, 1, 'Lista de Faltas', 2);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (7, 1, 'Ordem de Compra', 2);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (8, NULL, 'Relat�rios', 1);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (9, 1, 'Rel. Altera��o do Pre�o de Venda', 8);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (10, 1, 'Rel. Compra e Venda por Departamento', 8);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (11, 1, 'Rel. Fornecedores do Produto', 8);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (12, 1, 'Rel. Giro de Estoque', 8);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (13, 1, 'Rel. Lista de Pre�o', 8);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (14, 1, 'Rel. Produto do Fornecedor', 8);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (15, NULL, 'Manuten��o', NULL);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (16, NULL, 'Cadastros', 15);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (17, 1, 'Acerto de Estoque', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (18, 1, 'Categorias', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (19, 1, 'Formas de Pagamento', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (21, 19, 'Unidades', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (22, 6, 'Pessoas', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (23, 20, 'Produtos', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (24, NULL, 'Localizar', 15);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (25, 1, 'Ordems de Servi�o', 2);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (26, 3, 'Busca Pessoas', 24);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (27, 2, 'Busca Endere�os', 24);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (28, 18, 'Busca Produtos', 24);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (29, NULL, 'Sistema', NULL);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (30, NULL, 'Cadastros', 29);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (31, 1, 'Empresa', 30);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (32, 5, 'Itens do Menu', 30);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (33, NULL, 'Usu�rios', 30);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (34, 4, 'Localizar', 33);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (35, 7, 'Gerenciamento', 33);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (36, 15, 'Permiss�es', 33);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (37, NULL, 'Administra��o', 29);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (38, NULL, 'Utilit�rios', 37);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (39, 16, 'Executa SQL', 38);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (40, 17, 'Saida em tempo real', 38);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (41, 14, 'Visualizador de Logs', 38);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (42, 8, 'Envia NFe', 38);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (43, NULL, 'Configura��es', 37);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (44, 12, 'Op��es Gerais', 43);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (45, 11, 'Personaliza��o (Por Usu�rio)', 43);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (46, 21, 'Localiza��es/G�ndolas', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (47, 22, 'Servi�os', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (48, 23, 'Ordem de Servi�o', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (49, 24, 'Categorias', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (50, 25, 'Tipo de Equipamento', 16);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (51, 26, 'Busca Ordem de Servi�o', 24);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (52, NULL, 'Relat�rios', 15);

INSERT INTO menu (cd_menu, cd_acao, nodo_menu, id_nodo_sup)
VALUES (53, 27, 'Rel. Clientes', 52);


--
-- Data for table public.restricoes (OID = 26205) (LIMIT 0,42)
--
--INSERT INTO restricoes (cd_empresa, cd_usuario, cd_acao)
--VALUES (0, 1, 1);

SELECT set_all_restrictions_for_user(0, 1);

DELETE FROM restricoes WHERE cd_empresa = 0 AND cd_usuario = 1 and cd_acao = 1;


--
-- Data for table public.pessoa (OID = 17417) (LIMIT 0,3)
--
INSERT INTO pessoa (cd_pessoa, cd_empresa, tipo_pessoa, nome_pessoa, data_nascimento, data_cad_pessoa, descricao_pessoa, email_pessoa, site_pessoa, telefone_fixo_pessoa, telefone_cell_pessoa, cpf_cnpj_pessoa, rg_ie_pessoa, orgao_rg_pessoa, data_exp_rg_pessoa, profissao_pessoa, empresa_pessoa, fone_empresa_pessoa, sexo_pessoa)
VALUES (2, 0, 'F', 'Luana Boeira Peres', '1988-04-19', '2012-04-17', '', 'lub.peres@hotmail.com', '', '5433111205', '81411782', '01687495050', '3102061722', 'SJS', '2004-11-22', 'auxiliar administrativo', 'Roberto Chiesa Bartelmebs & CIA LTDA', '05433273535', 'F');

INSERT INTO pessoa (cd_pessoa, cd_empresa, tipo_pessoa, nome_pessoa, data_nascimento, data_cad_pessoa, descricao_pessoa, email_pessoa, site_pessoa, telefone_fixo_pessoa, telefone_cell_pessoa, cpf_cnpj_pessoa, rg_ie_pessoa, orgao_rg_pessoa, data_exp_rg_pessoa, profissao_pessoa, empresa_pessoa, fone_empresa_pessoa, sexo_pessoa)
VALUES (3, 0, 'J', 'KUHN DO BRASIL S/A', '1901-01-01', '2012-04-18', '', '', '', '33166200', '', '06216625000171', '0910261296', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO pessoa (cd_pessoa, cd_empresa, tipo_pessoa, nome_pessoa, data_nascimento, data_cad_pessoa, descricao_pessoa, email_pessoa, site_pessoa, telefone_fixo_pessoa, telefone_cell_pessoa, cpf_cnpj_pessoa, rg_ie_pessoa, orgao_rg_pessoa, data_exp_rg_pessoa, profissao_pessoa, empresa_pessoa, fone_empresa_pessoa, sexo_pessoa)
VALUES (1, 0, 'F', 'Roberto Chiesa Bartelmebs', '1988-05-31', '2012-04-14', '', 'roberto@saiconrs.com', 'www.saiconrs.com', '33112144', '81411825', '00899498035', '6060497507', 'sjs', '0002-11-30 BC', 'Gerente', 'Roberto Chiesa Bartelmebs & CIA LTDA', '05433273535', 'M');

--
-- Data for table public.endereco_pessoa (OID = 17417) (LIMIT 0,3)
--
INSERT INTO endereco_pessoa (cd_pessoa, cd_logradouro, cd_empresa, principal_endereco_pessoa, complemento_endereco_pessoa, numero_endereco_pessoa)
VALUES (1, 400460, 0, true, '',  36);

INSERT INTO endereco_pessoa (cd_pessoa, cd_logradouro, cd_empresa, principal_endereco_pessoa, complemento_endereco_pessoa, numero_endereco_pessoa)
VALUES (2, 400333, 0, true, '',  195);

INSERT INTO endereco_pessoa (cd_pessoa, cd_logradouro, cd_empresa, principal_endereco_pessoa, complemento_endereco_pessoa, numero_endereco_pessoa)
VALUES (3, 400245, 0, true, '',  1380);

--
-- Data for table public.cliente (OID = 17427) (LIMIT 0,2)
--
INSERT INTO cliente (cd_empresa, cd_cliente)
VALUES (0, 1);

INSERT INTO cliente (cd_empresa, cd_cliente)
VALUES (0, 3);

--
-- Data for table public.fornecedor (OID = 17436) (LIMIT 0,1)
--
INSERT INTO fornecedor (cd_empresa, cd_fornecedor)
VALUES (0, 3);

--
-- Data for table public.funcionario (OID = 17505) (LIMIT 0,2)
--
INSERT INTO funcionario (cd_empresa, cd_funcionario)
VALUES (0, 1);

INSERT INTO funcionario (cd_empresa, cd_funcionario)
VALUES (0, 2);


--
-- Data for sequence public.lookandfeels_cd_lookandfeels_seq (OID = 17212)
--
SELECT pg_catalog.setval('lookandfeels_cd_lookandfeels_seq', 16, false);
--
-- Data for sequence public.imagem_prod_cd_imagem_prod_seq (OID = 17221)
--
SELECT pg_catalog.setval('imagem_prod_cd_imagem_prod_seq', 1, false);
--
-- Data for sequence public.operacao_cd_operacao_seq (OID = 17312)
--
SELECT pg_catalog.setval('operacao_cd_operacao_seq', 1, false);
--
-- Data for sequence public.autorun_cd_autorun_seq (OID = 17560)
--
SELECT pg_catalog.setval('autorun_cd_autorun_seq', 1, false);
--
-- Data for sequence public.categoria_prod_cd_categoria_prod_seq (OID = 17240)
--
SELECT pg_catalog.setval('categoria_prod_cd_categoria_prod_seq', 1, true);
--
-- Data for sequence public.classe_cd_classe_seq (OID = 17301)
--
SELECT pg_catalog.setval('classe_cd_classe_seq', 25, true);
--
-- Data for sequence public.conta_email_cd_conta_email_seq (OID = 17361)
--
SELECT pg_catalog.setval('conta_email_cd_conta_email_seq', 1, false);
--
-- Data for sequence public.modulos_cd_modulos_seq (OID = 17260)
--
SELECT pg_catalog.setval('modulos_cd_modulos_seq', 4, false);
--
-- Data for sequence public.ean_produto_cd_ean_produto_seq (OID = 17249)
--
SELECT pg_catalog.setval('ean_prod_cd_ean_prod_seq', 1, false);
--
-- Data for sequence public.menu_cd_menu_seq (OID = 17275)
--
SELECT pg_catalog.setval('menu_cd_menu_seq', 50, true);
--
-- Data for sequence public.wallpaper_cd_wallpaper_seq (OID = 17937)
--
SELECT pg_catalog.setval('wallpaper_cd_wallpaper_seq', 3, true);
