/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.3                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          jsyscom.dez                                     */
/* Project name:          jSyscom - Sistema de Automacao Comercial        */
/* Author:                Jean Marcos da Rosa                             */
/* Script type:           Database creation script                        */
/* Created on:            2013-06-19 11:18                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Sequences                                                              */
/* ---------------------------------------------------------------------- */

CREATE SEQUENCE public.autorun_cd_autorun_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE SEQUENCE public.aviso_cd_aviso_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE SEQUENCE public.categoria_prod_cd_categoria_prod_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 2 CACHE 1;

CREATE SEQUENCE public.conexao_equip_cd_conexao_equip_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 2 CACHE 1;

CREATE SEQUENCE public.conta_email_cd_conta_email_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE SEQUENCE public.forma_pagamento_cd_forma_pagamento_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE SEQUENCE public.imagem_prod_cd_imagem_prod_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 10 CACHE 1;

CREATE SEQUENCE public.local_prod_cd_local_prod_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 13 CACHE 1;

CREATE SEQUENCE public.logs_cd_logs_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 146 CACHE 1;

CREATE SEQUENCE public.lookandfeels_cd_lookandfeels_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 16 CACHE 1;

CREATE SEQUENCE public.os_tst_equip_cd_os_tst_equip_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE SEQUENCE public.prop_tipo_equip_cd_prop_tipo_equip_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 4 CACHE 1;

CREATE SEQUENCE public.tipo_equip_cd_tipo_equip_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE SEQUENCE public.wallpaper_cd_wallpaper_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 3 CACHE 1;

/* ---------------------------------------------------------------------- */
/* Tables                                                                 */
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */
/* Add table "lookandfeels"                                               */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.lookandfeels (
    cd_modulos INTEGER,
    cd_lookandfeels INTEGER  NOT NULL,
    nome_lookandfeels CHARACTER VARYING(200)  NOT NULL,
    CONSTRAINT pk_lookandfeels PRIMARY KEY (cd_lookandfeels)
);

CREATE UNIQUE INDEX index_lookandfeels_pk ON public.lookandfeels (cd_lookandfeels);

CREATE INDEX index_lookandfeels_1_fk ON public.lookandfeels (cd_modulos);

/* ---------------------------------------------------------------------- */
/* Add table "wallpaper"                                                  */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.wallpaper (
    cd_empresa INTEGER  NOT NULL,
    cd_wallpaper INTEGER  NOT NULL,
    nome_wallpaper CHARACTER VARYING(50)  NOT NULL,
    md5_wallpaper CHARACTER VARYING(32)  NOT NULL,
    blob_wallpaper OID,
    data DATE DEFAULT 'current_date',
    hora TIME DEFAULT 'current_time',
    CONSTRAINT imagem_pkey PRIMARY KEY (cd_empresa, cd_wallpaper)
);

CREATE UNIQUE INDEX index_wallpaper_pk ON public.wallpaper (cd_empresa,cd_wallpaper);

CREATE UNIQUE INDEX index_wallpaper_2_ak ON public.wallpaper (cd_wallpaper,cd_empresa);

CREATE INDEX index_wallpaper_1_fk ON public.wallpaper (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "config_empresa"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.config_empresa (
    cd_empresa INTEGER  NOT NULL,
    titulo_software CHARACTER VARYING(300) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    atu_mod BOOLEAN  NOT NULL,
    nro_decimal INTEGER DEFAULT 2  NOT NULL,
    CONSTRAINT pk_config_geral PRIMARY KEY (cd_empresa)
);

CREATE UNIQUE INDEX index_config_empresa_pk ON public.config_empresa (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "opcoes_tela"                                                */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.opcoes_tela (
    cd_classe INTEGER  NOT NULL,
    iconifiable BOOLEAN  NOT NULL,
    maximizable BOOLEAN  NOT NULL,
    resizable BOOLEAN  NOT NULL,
    closable BOOLEAN  NOT NULL,
    visible BOOLEAN  NOT NULL,
    show_version BOOLEAN  NOT NULL,
    CONSTRAINT pk_opcoes_tela PRIMARY KEY (cd_classe)
);

CREATE UNIQUE INDEX index_opcoes_tela_pk ON public.opcoes_tela (cd_classe);

/* ---------------------------------------------------------------------- */
/* Add table "config_usuario"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.config_usuario (
    cd_empresa INTEGER  NOT NULL,
    cd_usuario INTEGER  NOT NULL,
    cd_wallpaper INTEGER,
    cd_lookandfeels INTEGER,
    nome_menu_config CHARACTER VARYING(50) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    grava_logs_config BOOLEAN  NOT NULL,
    distancia_frame_config INTEGER DEFAULT NULL::numeric  NOT NULL,
    CONSTRAINT pk_config PRIMARY KEY (cd_empresa, cd_usuario)
);

CREATE UNIQUE INDEX index_config_usuario_pk ON public.config_usuario (cd_empresa,cd_usuario);

CREATE INDEX index_config_usuario_4_fk ON public.config_usuario (cd_usuario,cd_empresa);

CREATE INDEX index_config_usuario_3_fk ON public.config_usuario (cd_empresa);

CREATE INDEX index_config_usuario_2_fk ON public.config_usuario (cd_lookandfeels);

CREATE INDEX index_config_usuario_1_fk ON public.config_usuario (cd_wallpaper,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "modulos"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.modulos (
    cd_modulos INTEGER  NOT NULL,
    nome_modulos CHARACTER VARYING(256) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    bind_modulos BYTEA,
    md5_modulos CHARACTER VARYING(32) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying',
    descricao_modulos CHARACTER VARYING(50) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying',
    ativo_modulos BOOLEAN  NOT NULL,
    CONSTRAINT pk_binarys PRIMARY KEY (cd_modulos)
);

CREATE UNIQUE INDEX index_modulos_pk ON public.modulos (cd_modulos);

/* ---------------------------------------------------------------------- */
/* Add table "menu"                                                       */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.menu (
    cd_acao INTEGER,
    cd_menu INTEGER  NOT NULL,
    nodo_menu CHARACTER VARYING(200) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying',
    id_nodo_sup NUMERIC(11) DEFAULT NULL::numeric,
    CONSTRAINT pk_menu PRIMARY KEY (cd_menu)
);

CREATE UNIQUE INDEX index_menu_pk ON public.menu (cd_menu);

CREATE INDEX index_menu_1_fk ON public.menu (cd_acao);

/* ---------------------------------------------------------------------- */
/* Add table "empresa"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.empresa (
    cep_logradouro CHARACTER VARYING(8),
    cd_empresa INTEGER  NOT NULL,
    razao_social_empresa CHARACTER VARYING(200) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    fantasia_empresa CHARACTER VARYING(200) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    ie_empresa CHARACTER VARYING(18) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    cnpj_empresa CHARACTER VARYING(18) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    fone_empresa CHARACTER VARYING(15) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    fax_empresa CHARACTER VARYING(15) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying',
    responsavel_empresa CHARACTER VARYING(50),
    numero_endereco_empresa INTEGER,
    complemento_endereco_empresa CHARACTER VARYING(50),
    email_empresa CHARACTER VARYING(100),
    CONSTRAINT pk_empresa PRIMARY KEY (cd_empresa)
);

CREATE UNIQUE INDEX index_empresa_pk ON public.empresa (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "classe"                                                     */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.classe (
    cd_acao INTEGER  NOT NULL,
    cd_modulos INTEGER  NOT NULL,
    cd_classe INTEGER  NOT NULL,
    nome_classe CHARACTER VARYING(100) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    debug_classe BOOLEAN,
    descricao_classe CHARACTER VARYING(100) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying',
    CONSTRAINT pk_classe PRIMARY KEY (cd_modulos, cd_classe)
);

CREATE UNIQUE INDEX index_classe_pk ON public.classe (cd_classe,cd_modulos);

CREATE UNIQUE INDEX index_classe_3_ak ON public.classe (cd_classe);

CREATE INDEX index_classe_2_fk ON public.classe (cd_modulos);

/* ---------------------------------------------------------------------- */
/* Add table "operacao"                                                   */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.operacao (
    cd_acao INTEGER  NOT NULL,
    cd_operacao INTEGER  NOT NULL,
    descricao_operacao CHARACTER VARYING(50),
    CONSTRAINT pk_operacao PRIMARY KEY (cd_operacao)
);

CREATE UNIQUE INDEX index_operacao_pk ON public.operacao (cd_operacao);

/* ---------------------------------------------------------------------- */
/* Add table "usuario"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.usuario (
    cd_empresa INTEGER  NOT NULL,
    cd_funcionario INTEGER,
    cd_usuario INTEGER  NOT NULL,
    nome_usuario CHARACTER VARYING(50) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    senha_usuario CHARACTER VARYING(32) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying'  NOT NULL,
    descricao_usuario CHARACTER VARYING(40),
    data_cadastro_usuario DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    CONSTRAINT pk_usuario PRIMARY KEY (cd_empresa, cd_usuario)
);

CREATE UNIQUE INDEX index_usuario_pk ON public.usuario (cd_empresa,cd_usuario);

CREATE UNIQUE INDEX index_usuario_3_ak ON public.usuario (cd_usuario,cd_empresa);

CREATE INDEX index_usuario_2_fk ON public.usuario (cd_empresa);

CREATE INDEX index_usuario_1_fk ON public.usuario (cd_empresa,cd_funcionario);

/* ---------------------------------------------------------------------- */
/* Add table "acao"                                                       */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.acao (
    cd_acao INTEGER  NOT NULL,
    CONSTRAINT pk_acao PRIMARY KEY (cd_acao)
);

CREATE UNIQUE INDEX index_acao_pk ON public.acao (cd_acao);

/* ---------------------------------------------------------------------- */
/* Add table "restricoes"                                                 */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.restricoes (
    cd_empresa INTEGER  NOT NULL,
    cd_usuario INTEGER  NOT NULL,
    cd_acao INTEGER  NOT NULL,
    CONSTRAINT pk_restricoes PRIMARY KEY (cd_empresa, cd_usuario, cd_acao)
);

CREATE INDEX index_restricoes_1_fk ON public.restricoes (cd_acao);

CREATE INDEX index_restricoes_2_fk ON public.restricoes (cd_usuario,cd_empresa);

CREATE INDEX index_restricoes_3_fk ON public.restricoes (cd_empresa);

CREATE UNIQUE INDEX index_restricoes_pk ON public.restricoes (cd_empresa,cd_usuario,cd_acao);

/* ---------------------------------------------------------------------- */
/* Add table "logs"                                                       */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.logs (
    cd_empresa INTEGER,
    cd_usuario INTEGER,
    cd_logs INTEGER  NOT NULL,
    data_logs DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    hora_logs TIME DEFAULT '(''''''''''''''''now'''''''''''''''')::time with time zone'  NOT NULL,
    titulo_logs CHARACTER VARYING(50)  NOT NULL,
    operacao_logs CHARACTER VARYING(50)  NOT NULL,
    descricao_logs CHARACTER VARYING(200) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying',
    versao_js_logs CHARACTER VARYING(10) DEFAULT '''''''''''''''''NULL::character varying''''''''''''''''::character varying',
    CONSTRAINT pk_logs PRIMARY KEY (cd_logs)
);

CREATE UNIQUE INDEX index_logs_pk ON public.logs (cd_logs);

CREATE INDEX index_logs_2_fk ON public.logs (cd_usuario,cd_empresa);

CREATE INDEX index_logs_1_fk ON public.logs (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "conta_email"                                                */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.conta_email (
    cd_empresa INTEGER  NOT NULL,
    cd_conta_email INTEGER  NOT NULL,
    endereco_conta_email CHARACTER VARYING(100)  NOT NULL,
    proprietario_conta_email CHARACTER VARYING(100)  NOT NULL,
    host_conta_email CHARACTER(100)  NOT NULL,
    port_conta_email INTEGER  NOT NULL,
    user_conta_email CHARACTER VARYING(100)  NOT NULL,
    pass_conta_email CHARACTER VARYING(100)  NOT NULL,
    CONSTRAINT pk_conta_email PRIMARY KEY (cd_empresa, cd_conta_email)
);

CREATE UNIQUE INDEX index_conta_email_pk ON public.conta_email (cd_empresa,cd_conta_email);

CREATE INDEX index_conta_email_1_fk ON public.conta_email (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "uf"                                                         */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.uf (
    cd_pais INTEGER  NOT NULL,
    sigla_uf CHARACTER(2)  NOT NULL,
    nome_uf CHARACTER VARYING(40)  NOT NULL,
    CONSTRAINT pk_uf PRIMARY KEY (sigla_uf)
);

CREATE UNIQUE INDEX index_uf_pk ON public.uf (sigla_uf);

CREATE INDEX index_uf_1_fk ON public.uf (cd_pais);

/* ---------------------------------------------------------------------- */
/* Add table "cidade"                                                     */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.cidade (
    cd_cidade INTEGER  NOT NULL,
    sigla_uf CHARACTER(2)  NOT NULL,
    nome_cidade CHARACTER VARYING(80)  NOT NULL,
    CONSTRAINT pk_cidade PRIMARY KEY (cd_cidade)
);

CREATE UNIQUE INDEX index_cidade_pk ON public.cidade (cd_cidade);

/* ---------------------------------------------------------------------- */
/* Add table "pais"                                                       */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.pais (
    cd_pais INTEGER  NOT NULL,
    sigla_pais CHARACTER(2)  NOT NULL,
    nome_pais CHARACTER VARYING(40)  NOT NULL,
    zip_code_pais CHARACTER VARYING(20)  NOT NULL,
    CONSTRAINT pk_pais PRIMARY KEY (cd_pais)
);

CREATE UNIQUE INDEX index_pais_pk ON public.pais (cd_pais);

/* ---------------------------------------------------------------------- */
/* Add table "bairro"                                                     */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.bairro (
    cd_bairro INTEGER  NOT NULL,
    cd_cidade INTEGER  NOT NULL,
    nome_bairro CHARACTER VARYING(80)  NOT NULL,
    CONSTRAINT pk_bairro PRIMARY KEY (cd_bairro)
);

CREATE UNIQUE INDEX index_bairro_pk ON public.bairro (cd_bairro);

CREATE INDEX index_bairro_1_fk ON public.bairro (cd_cidade);

/* ---------------------------------------------------------------------- */
/* Add table "logradouro"                                                 */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.logradouro (
    cd_cidade INTEGER  NOT NULL,
    cd_bairro INTEGER  NOT NULL,
    cep_logradouro CHARACTER VARYING(8)  NOT NULL,
    nome_logradouro CHARACTER VARYING(150)  NOT NULL,
    CONSTRAINT pk_logradouro PRIMARY KEY (cep_logradouro)
);

CREATE UNIQUE INDEX index_logradouro_pk ON public.logradouro (cep_logradouro);

CREATE INDEX index_logradouro_1_fk ON public.logradouro (cd_bairro);

/* ---------------------------------------------------------------------- */
/* Add table "pessoa"                                                     */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.pessoa (
    cd_empresa INTEGER  NOT NULL,
    cd_pessoa INTEGER  NOT NULL,
    nome_pessoa CHARACTER VARYING(120)  NOT NULL,
    tipo_pessoa CHARACTER(1)  NOT NULL,
    data_nascimento DATE,
    data_cad_pessoa DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    descricao_pessoa CHARACTER VARYING(150),
    email_pessoa CHARACTER VARYING(120),
    site_pessoa CHARACTER VARYING(120),
    telefone_fixo_pessoa CHARACTER VARYING(11)  NOT NULL,
    telefone_cell_pessoa CHARACTER VARYING(11),
    cpf_cnpj_pessoa CHARACTER VARYING(20)  NOT NULL,
    rg_ie_pessoa CHARACTER VARYING(20)  NOT NULL,
    orgao_rg_pessoa CHARACTER VARYING(10),
    data_exp_rg_pessoa DATE,
    profissao_pessoa CHARACTER VARYING(120),
    empresa_pessoa CHARACTER VARYING(120),
    fone_empresa_pessoa CHARACTER VARYING(11),
    sexo_pessoa CHARACTER(1),
    CONSTRAINT pk_pessoa PRIMARY KEY (cd_empresa, cd_pessoa)
);

CREATE UNIQUE INDEX index_pessoa_4_pk ON public.pessoa (cd_empresa,cd_pessoa);

CREATE UNIQUE INDEX index_pessoa_3_ak ON public.pessoa (cd_pessoa,cd_empresa);

CREATE INDEX index_pessoa_2_fk ON public.pessoa (cd_empresa);

CREATE UNIQUE INDEX index_pessoa_1_pk ON public.pessoa (cd_empresa,cpf_cnpj_pessoa);

/* ---------------------------------------------------------------------- */
/* Add table "cliente"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.cliente (
    cd_empresa INTEGER  NOT NULL,
    cd_cliente INTEGER  NOT NULL,
    desconto_cliente DOUBLE PRECISION DEFAULT 0  NOT NULL,
    CONSTRAINT pk_cliente PRIMARY KEY (cd_empresa, cd_cliente)
);

CREATE UNIQUE INDEX index_cliente_pk ON public.cliente (cd_empresa,cd_cliente);

CREATE INDEX index_cliente_3_fk ON public.cliente (cd_empresa);

CREATE INDEX index_cliente_2_fk ON public.cliente (cd_cliente,cd_empresa);

CREATE UNIQUE INDEX index_cliente_1_fk ON public.cliente (cd_cliente,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "fornecedor"                                                 */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.fornecedor (
    cd_empresa INTEGER  NOT NULL,
    cd_fornecedor INTEGER  NOT NULL,
    CONSTRAINT pk_fornecedor PRIMARY KEY (cd_empresa, cd_fornecedor)
);

CREATE UNIQUE INDEX index_fornecedor_pk ON public.fornecedor (cd_empresa,cd_fornecedor);

CREATE INDEX index_fornecedor_2_fk ON public.fornecedor (cd_empresa);

CREATE INDEX index_fornecedor_1_fk ON public.fornecedor (cd_fornecedor,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "unidade_medida"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.unidade_medida (
    cd_empresa INTEGER  NOT NULL,
    sigla_unidade_m CHARACTER(5)  NOT NULL,
    nome_unidade_m CHARACTER VARYING(40)  NOT NULL,
    CONSTRAINT pk_unidade_medida PRIMARY KEY (cd_empresa, sigla_unidade_m)
);

CREATE UNIQUE INDEX index_unidade_medida_3_pk ON public.unidade_medida (cd_empresa,sigla_unidade_m);

CREATE UNIQUE INDEX index_unidade_medida_2_ak ON public.unidade_medida (sigla_unidade_m,cd_empresa);

CREATE INDEX index_unidade_medida_1_fk ON public.unidade_medida (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "forma_pagamento"                                            */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.forma_pagamento (
    cd_empresa INTEGER  NOT NULL,
    cd_forma_pagamento INTEGER  NOT NULL,
    nome_forma_pagamento CHARACTER VARYING(40)  NOT NULL,
    descricao_forma_pagamento CHARACTER VARYING(40),
    CONSTRAINT pk_forma_pagamento PRIMARY KEY (cd_empresa, cd_forma_pagamento)
);

CREATE UNIQUE INDEX index_forma_pagamento_pk ON public.forma_pagamento (cd_empresa,cd_forma_pagamento);

CREATE INDEX index_forma_pagamento_2_fk ON public.forma_pagamento (cd_empresa);

CREATE UNIQUE INDEX index_forma_pagamento_1_ak ON public.forma_pagamento (cd_forma_pagamento,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "produto"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.produto (
    cd_empresa INTEGER  NOT NULL,
    cd_categoria_prod INTEGER  NOT NULL,
    cd_imagem_prod INTEGER  NOT NULL,
    sigla_unidade_m CHARACTER(5)  NOT NULL,
    cd_local_prod INTEGER  NOT NULL,
    cd_produto INTEGER  NOT NULL,
    descricao_produto CHARACTER VARYING(200)  NOT NULL,
    detalhe_produto CHARACTER VARYING(400),
    vlr_venda_produto DOUBLE PRECISION  NOT NULL,
    vlr_compra_produto DOUBLE PRECISION DEFAULT 0  NOT NULL,
    qtd_est_min DOUBLE PRECISION DEFAULT 0  NOT NULL,
    data_cad_produto DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    situacao_produto BOOLEAN  NOT NULL,
    CONSTRAINT pk_produto PRIMARY KEY (cd_empresa, cd_produto)
);

CREATE UNIQUE INDEX index_produto_pk ON public.produto (cd_empresa,cd_produto);

CREATE UNIQUE INDEX index_produto_7_ak ON public.produto (cd_produto,cd_empresa);

CREATE INDEX index_produto_6_fk ON public.produto (cd_empresa,cd_local_prod);

CREATE INDEX index_produto_5_fk ON public.produto (cd_imagem_prod,cd_empresa);

CREATE INDEX index_produto_4_fk ON public.produto (cd_categoria_prod,cd_empresa);

CREATE INDEX index_produto_3_fk ON public.produto (cd_empresa);

CREATE INDEX index_produto_2_fk ON public.produto (sigla_unidade_m,cd_empresa);

CREATE INDEX index_produto_1_fk ON public.produto (sigla_unidade_m);

/* ---------------------------------------------------------------------- */
/* Add table "compra_cab"                                                 */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.compra_cab (
    cd_empresa INTEGER  NOT NULL,
    cd_usuario INTEGER  NOT NULL,
    cd_fornecedor INTEGER  NOT NULL,
    cd_compra_cab INTEGER  NOT NULL,
    data_compra_cab DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    hora_compra_cab TIME DEFAULT '(''''''''''''''''now'''''''''''''''')::time with time zone'  NOT NULL,
    vlr_bruto_compra_cab DOUBLE PRECISION DEFAULT 0,
    vlr_liquido_compra_cab DOUBLE PRECISION DEFAULT 0,
    vlr_desconto_compra_cab DOUBLE PRECISION DEFAULT 0,
    CONSTRAINT pk_compra_cab PRIMARY KEY (cd_empresa, cd_compra_cab)
);

CREATE UNIQUE INDEX index_compra_cab_pk ON public.compra_cab (cd_empresa,cd_compra_cab);

CREATE INDEX index_compra_cab_3_fk ON public.compra_cab (cd_empresa,cd_usuario);

CREATE INDEX index_compra_cab_2_fk ON public.compra_cab (cd_empresa);

CREATE INDEX index_compra_cab_1_fk ON public.compra_cab (cd_empresa,cd_fornecedor);

/* ---------------------------------------------------------------------- */
/* Add table "compra_det"                                                 */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.compra_det (
    cd_empresa INTEGER  NOT NULL,
    cd_compra_cab INTEGER  NOT NULL,
    cd_produto INTEGER  NOT NULL,
    cd_compra_det INTEGER  NOT NULL,
    qtd_compra_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_unitario_compra_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_desconto_total_compra_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    CONSTRAINT pk_compra_det PRIMARY KEY (cd_empresa, cd_compra_cab, cd_compra_det)
);

CREATE UNIQUE INDEX index_compra_det_pk ON public.compra_det (cd_empresa,cd_compra_cab,cd_compra_det);

CREATE INDEX index_compra_det_3_fk ON public.compra_det (cd_empresa,cd_compra_cab);

CREATE INDEX index_compra_det_2_fk ON public.compra_det (cd_empresa);

CREATE INDEX index_compra_det_1_fk ON public.compra_det (cd_produto,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "ean_prod"                                                   */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.ean_prod (
    cd_empresa INTEGER  NOT NULL,
    cd_produto INTEGER  NOT NULL,
    numero_ean_produto CHARACTER VARYING(14)  NOT NULL,
    data_cad_ean_produto DATE  NOT NULL,
    CONSTRAINT pk_ean_prod PRIMARY KEY (cd_empresa, cd_produto, numero_ean_produto)
);

CREATE UNIQUE INDEX index_ean_prod_pk ON public.ean_prod (cd_empresa,cd_produto);

CREATE INDEX index_ean_prod_2_fk ON public.ean_prod (cd_empresa);

CREATE INDEX index_ean_prod_1_fk ON public.ean_prod (cd_produto,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "funcionario"                                                */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.funcionario (
    cd_empresa INTEGER  NOT NULL,
    cd_funcionario INTEGER  NOT NULL,
    custo_hora_funcionario DOUBLE PRECISION DEFAULT 0  NOT NULL,
    CONSTRAINT pk_funcionario PRIMARY KEY (cd_empresa, cd_funcionario)
);

CREATE UNIQUE INDEX index_funcionario_pk ON public.funcionario (cd_empresa,cd_funcionario);

CREATE INDEX index_funcionario_2_fk ON public.funcionario (cd_empresa);

CREATE INDEX index_funcionario_1_fk ON public.funcionario (cd_funcionario,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "venda_cab"                                                  */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.venda_cab (
    cd_empresa INTEGER  NOT NULL,
    cd_usuario INTEGER  NOT NULL,
    cd_cliente INTEGER  NOT NULL,
    cd_venda_cab INTEGER  NOT NULL,
    vlr_bruto_venda_cab DOUBLE PRECISION,
    vlr_liquido_venda_cab DOUBLE PRECISION,
    vlr_desconto_venda_cab DOUBLE PRECISION,
    data_venda_cab DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    hora_venda_cab TIME DEFAULT '(''''''''''''''''now'''''''''''''''')::time with time zone'  NOT NULL,
    CONSTRAINT pk_venda_cab PRIMARY KEY (cd_empresa, cd_venda_cab)
);

CREATE UNIQUE INDEX index_venda_cab_pk ON public.venda_cab (cd_empresa,cd_venda_cab);

CREATE UNIQUE INDEX index_venda_cab_4_ak ON public.venda_cab (cd_venda_cab,cd_empresa);

CREATE INDEX index_venda_cab_3_fk ON public.venda_cab (cd_empresa,cd_usuario);

CREATE INDEX index_venda_cab_2_fk ON public.venda_cab (cd_empresa,cd_cliente);

CREATE INDEX index_venda_cab_1_fk ON public.venda_cab (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "parcelamento"                                               */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.parcelamento (
    cd_empresa INTEGER  NOT NULL,
    cd_parcelamento INTEGER  NOT NULL,
    numero_parcelas_parcelamento INTEGER  NOT NULL,
    intervalo_data_parcelamento INTEGER  NOT NULL,
    vlr_total_parcelamento DOUBLE PRECISION  NOT NULL,
    vlr_entrada_parcelamento DOUBLE PRECISION  NOT NULL,
    perc_desc_adiant_parcelamento DOUBLE PRECISION  NOT NULL,
    perc_juro_parcelamento DOUBLE PRECISION  NOT NULL,
    multa_atrazo_parcelamento DOUBLE PRECISION  NOT NULL,
    venc_primeira_parcela_parcelamento DATE  NOT NULL,
    CONSTRAINT pk_parcelamento PRIMARY KEY (cd_empresa, cd_parcelamento)
);

CREATE UNIQUE INDEX index_parcelamento_pk ON public.parcelamento (cd_empresa,cd_parcelamento);

CREATE INDEX index_parcelamento_1_fk ON public.parcelamento (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "venda_det"                                                  */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.venda_det (
    cd_empresa INTEGER  NOT NULL,
    cd_venda_cab INTEGER  NOT NULL,
    cd_produto INTEGER  NOT NULL,
    cd_venda_det INTEGER  NOT NULL,
    qtd_venda_det DOUBLE PRECISION  NOT NULL,
    vlr_unitario_venda_det DOUBLE PRECISION  NOT NULL,
    vlt_desconto_total_venda_det DOUBLE PRECISION  NOT NULL,
    CONSTRAINT pk_venda_det PRIMARY KEY (cd_empresa, cd_venda_cab, cd_venda_det)
);

CREATE UNIQUE INDEX index_venda_det_pk ON public.venda_det (cd_empresa,cd_venda_cab,cd_venda_det);

CREATE INDEX index_venda_det_3_fk ON public.venda_det (cd_empresa,cd_produto);

CREATE INDEX index_venda_det_2_fk ON public.venda_det (cd_empresa);

CREATE INDEX index_venda_det_1_fk ON public.venda_det (cd_venda_cab,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "autorun"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.autorun (
    cd_empresa INTEGER  NOT NULL,
    cd_usuario INTEGER  NOT NULL,
    cd_acao INTEGER,
    cd_autorun INTEGER  NOT NULL,
    nro_operacao INTEGER,
    descricao_autorun CHARACTER VARYING(50),
    somente_inicio BOOLEAN  NOT NULL,
    ativo_autorun BOOLEAN  NOT NULL,
    CONSTRAINT pk_autorun PRIMARY KEY (cd_empresa, cd_usuario, cd_autorun)
);

CREATE UNIQUE INDEX index_autorun_pk ON public.autorun (cd_empresa,cd_usuario,cd_autorun);

CREATE INDEX index_autorun_3_fk ON public.autorun (cd_empresa);

CREATE INDEX index_autorun_2_fk ON public.autorun (cd_usuario,cd_empresa);

CREATE INDEX index_autorun_1_fk ON public.autorun (cd_acao);

/* ---------------------------------------------------------------------- */
/* Add table "licenca"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.licenca (
    cd_empresa INTEGER  NOT NULL,
    data_expira DATE  NOT NULL,
    CONSTRAINT pk_licenca PRIMARY KEY (cd_empresa, data_expira)
);

CREATE UNIQUE INDEX index_licenca_pk ON public.licenca (cd_empresa,data_expira);

CREATE INDEX index_licenca_1_fk ON public.licenca (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "aviso"                                                      */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.aviso (
    cd_empresa INTEGER  NOT NULL,
    cd_usuario INTEGER  NOT NULL,
    cd_aviso INTEGER  NOT NULL,
    dt_ini DATE,
    dt_fim DATE,
    descricao_aviso CHARACTER VARYING(500)  NOT NULL,
    insistente_aviso BOOLEAN  NOT NULL,
    visualizado_aviso BOOLEAN  NOT NULL,
    CONSTRAINT pk_aviso PRIMARY KEY (cd_empresa, cd_usuario, cd_aviso)
);

CREATE UNIQUE INDEX index_aviso_pk ON public.aviso (cd_empresa,cd_usuario,cd_aviso);

CREATE INDEX index_aviso_2_fk ON public.aviso (cd_usuario,cd_empresa);

CREATE INDEX index_aviso_1_fk ON public.aviso (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "categoria_prod"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.categoria_prod (
    cd_empresa INTEGER  NOT NULL,
    cd_categoria_prod INTEGER  NOT NULL,
    descricao_categoria_prod CHARACTER VARYING(50)  NOT NULL,
    margem_categoria_prod DOUBLE PRECISION  NOT NULL,
    CONSTRAINT pk_categoria_prod PRIMARY KEY (cd_empresa, cd_categoria_prod)
);

CREATE UNIQUE INDEX index_categoria_prod_pk ON public.categoria_prod (cd_empresa,cd_categoria_prod);

CREATE INDEX index_categoria_prod_2_fk ON public.categoria_prod (cd_empresa);

CREATE UNIQUE INDEX index_categoria_prod_1 ON public.categoria_prod (descricao_categoria_prod);

/* ---------------------------------------------------------------------- */
/* Add table "imagem_prod"                                                */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.imagem_prod (
    cd_empresa INTEGER  NOT NULL,
    cd_imagem_prod INTEGER  NOT NULL,
    nome_imagem_prod CHARACTER VARYING(80)  NOT NULL,
    md5_imagem_prod CHARACTER VARYING(32)  NOT NULL,
    blob_imagem_prod OID,
    data DATE DEFAULT 'current_date',
    hora TIME DEFAULT 'current_time',
    CONSTRAINT pk_imagem_prod PRIMARY KEY (cd_empresa, cd_imagem_prod)
);

CREATE UNIQUE INDEX index_imagem_prod_pk ON public.imagem_prod (cd_empresa,cd_imagem_prod);

CREATE UNIQUE INDEX index_imagem_prod_2_ak ON public.imagem_prod (cd_imagem_prod,cd_empresa);

CREATE INDEX index_imagem_prod_1_fk ON public.imagem_prod (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "estoque_prod"                                               */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.estoque_prod (
    cd_empresa INTEGER  NOT NULL,
    cd_produto INTEGER  NOT NULL,
    quantidade_estoque DOUBLE PRECISION  NOT NULL,
    dt_alt_estoque DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    CONSTRAINT pk_estoque_prod PRIMARY KEY (cd_empresa, cd_produto)
);

CREATE UNIQUE INDEX index_estoque_prod_pk ON public.estoque_prod (cd_empresa,cd_produto);

CREATE INDEX index_estoque_prod_1_fk ON public.estoque_prod (cd_produto,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "fornecedor_prod"                                            */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.fornecedor_prod (
    cd_empresa INTEGER  NOT NULL,
    cd_produto INTEGER  NOT NULL,
    cd_fornecedor INTEGER  NOT NULL,
    data_cadastro DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    CONSTRAINT pk_fornecedor_prod PRIMARY KEY (cd_empresa, cd_produto, cd_fornecedor)
);

CREATE UNIQUE INDEX index_fornecedor_prod_pk ON public.fornecedor_prod (cd_empresa,cd_produto,cd_fornecedor);

CREATE INDEX index_fornecedor_prod_3_fk ON public.fornecedor_prod (cd_produto,cd_empresa);

CREATE INDEX index_fornecedor_prod_2_fk ON public.fornecedor_prod (cd_empresa,cd_fornecedor);

CREATE INDEX index_fornecedor_prod_1_fk ON public.fornecedor_prod (cd_fornecedor,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "contato"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.contato (
    cd_empresa INTEGER  NOT NULL,
    cd_pessoa INTEGER  NOT NULL,
    cd_contato INTEGER  NOT NULL,
    nome_contato CHARACTER VARYING(50)  NOT NULL,
    cargo_contato CHARACTER VARYING(50)  NOT NULL,
    telefone_contato CHARACTER VARYING(20),
    obs_contato CHARACTER VARYING(80),
    CONSTRAINT pk_contato PRIMARY KEY (cd_empresa, cd_contato)
);

CREATE UNIQUE INDEX index_contato_pk ON public.contato (cd_empresa,cd_pessoa,cd_contato);

CREATE INDEX index_contato_2_fk ON public.contato (cd_pessoa,cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "endereco_pessoa"                                            */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.endereco_pessoa (
    cd_empresa INTEGER  NOT NULL,
    cd_pessoa INTEGER  NOT NULL,
    cep_logradouro CHARACTER VARYING(8)  NOT NULL,
    numero_endereco_pessoa INTEGER  NOT NULL,
    complemento_endereco_pessoa CHARACTER VARYING(40),
    principal_endereco_pessoa BOOLEAN  NOT NULL,
    CONSTRAINT pk_endereco_pessoa PRIMARY KEY (cd_empresa, cep_logradouro, numero_endereco_pessoa)
);

CREATE UNIQUE INDEX index_endereco_pessoa_pk ON public.endereco_pessoa (cd_empresa,cd_pessoa,numero_endereco_pessoa);

CREATE INDEX index_endereco_pessoa_3_fk ON public.endereco_pessoa (cd_empresa);

CREATE INDEX index_endereco_pessoa_2_fk ON public.endereco_pessoa (cd_pessoa);

/* ---------------------------------------------------------------------- */
/* Add table "parcela"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.parcela (
    cd_empresa INTEGER  NOT NULL,
    cd_parcelamento INTEGER  NOT NULL,
    cd_parcela INTEGER  NOT NULL,
    vlr_parcela DOUBLE PRECISION  NOT NULL,
    data_venc_parcela DATE  NOT NULL,
    pago_parcela BOOLEAN  NOT NULL,
    CONSTRAINT pk_parcela PRIMARY KEY (cd_empresa, cd_parcela)
);

CREATE UNIQUE INDEX index_parcela_pk ON public.parcela (cd_empresa,cd_parcela);

CREATE INDEX index_parcela_1_fk ON public.parcela (cd_empresa,cd_parcelamento);

/* ---------------------------------------------------------------------- */
/* Add table "local_prod"                                                 */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.local_prod (
    cd_empresa INTEGER  NOT NULL,
    cd_local_prod INTEGER  NOT NULL,
    descricao_local_prod CHARACTER VARYING(50)  NOT NULL,
    CONSTRAINT pk_local_prod PRIMARY KEY (cd_empresa, cd_local_prod)
);

CREATE UNIQUE INDEX index_local_prod_pk ON public.local_prod (cd_empresa,cd_local_prod);

CREATE INDEX index_local_prod_2_fk ON public.local_prod (cd_empresa);

CREATE UNIQUE INDEX idx_local_prod_1 ON public.local_prod (cd_empresa,descricao_local_prod);

/* ---------------------------------------------------------------------- */
/* Add table "os_cab"                                                     */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.os_cab (
    cd_empresa INTEGER  NOT NULL,
    cd_usuario INTEGER  NOT NULL,
    cd_cliente INTEGER  NOT NULL,
    cd_contato_cliente INTEGER,
    cep_logradouro_cliente CHARACTER VARYING(8),
    numero_endereco_cliente INTEGER,
    cd_equip INTEGER  NOT NULL,
    cd_local_prod INTEGER,
    cd_transportadora INTEGER,
    cd_os_cab INTEGER  NOT NULL,
    cep_logradouro_transp CHARACTER VARYING(8),
    numero_endereco_transp INTEGER,
    nro_oc_cliente_os_cab INTEGER,
    defeito_reclamado_equip_os_cab CHARACTER VARYING(500),
    estado_equip_os_cab CHARACTER VARYING(200),
    conserto_detalhado_os_cab CHARACTER VARYING(500),
    parecer_os_cab CHARACTER VARYING(500),
    meses_garantia_os_cab INTEGER DEFAULT 0  NOT NULL,
    data_cad_os_cab DATE DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    data_ent_os_cab DATE DEFAULT '(''''''''''''''''now'''''''''''''''')',
    data_saida_os_cab DATE,
    data_final_garantia_os_cab DATE,
    sob_garantia BOOLEAN DEFAULT false  NOT NULL,
    status_os_cab CHARACTER(1) DEFAULT '''''''''''''''''A''''''''''''''''::bpchar'  NOT NULL,
    CONSTRAINT pk_os_cab PRIMARY KEY (cd_empresa, cd_os_cab)
);

CREATE UNIQUE INDEX index_os_cab_pk ON public.os_cab (cd_empresa,cd_os_cab);

CREATE INDEX index_os_cab_7_fk ON public.os_cab (cd_empresa,cd_local_prod);

CREATE INDEX index_os_cab_6_fk ON public.os_cab (cd_equip,cd_empresa);

CREATE INDEX index_os_cab_5_fk ON public.os_cab (cd_empresa,numero_endereco_cliente);

CREATE INDEX index_os_cab_4_fk ON public.os_cab (cd_empresa,cd_contato_cliente);

CREATE INDEX index_os_cab_3_fk ON public.os_cab (cd_empresa,cd_usuario);

CREATE INDEX index_os_cab_2_fk ON public.os_cab (cd_empresa,cd_cliente);

CREATE INDEX index_os_cab_1_fk ON public.os_cab (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "os_prod_det"                                                */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.os_prod_det (
    cd_empresa INTEGER  NOT NULL,
    cd_os_cab INTEGER  NOT NULL,
    cd_produto INTEGER  NOT NULL,
    qtd_os_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_unitario_os_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_bruto_os_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    original_os_det BOOLEAN DEFAULT true  NOT NULL,
    CONSTRAINT pk_os_prod_det PRIMARY KEY (cd_empresa, cd_os_cab, cd_produto)
);

CREATE UNIQUE INDEX index_os_prod_det_pk ON public.os_prod_det (cd_empresa,cd_os_cab,cd_produto);

CREATE INDEX index_os_prod_det_2_fk ON public.os_prod_det (cd_empresa,cd_produto);

CREATE INDEX index_os_prod_det_1_fk ON public.os_prod_det (cd_empresa,cd_os_cab);

/* ---------------------------------------------------------------------- */
/* Add table "os_servico_det"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.os_servico_det (
    cd_empresa INTEGER  NOT NULL,
    cd_os_cab INTEGER  NOT NULL,
    cd_servico INTEGER  NOT NULL,
    qtd_os_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_unitario_os_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_bruto_os_det DOUBLE PRECISION DEFAULT 0  NOT NULL,
    mostrar_qtd_os_det BOOLEAN DEFAULT true  NOT NULL,
    obs_os_det CHARACTER VARYING(100),
    CONSTRAINT pk_os_servico_det PRIMARY KEY (cd_empresa, cd_os_cab)
);

CREATE UNIQUE INDEX index_os_servico_det_pk ON public.os_servico_det (cd_empresa,cd_os_cab);

CREATE INDEX index_os_servico_det_2_fk ON public.os_servico_det (cd_empresa,cd_servico);

CREATE INDEX index_os_servico_det_1_fk ON public.os_servico_det (cd_empresa,cd_os_cab);

/* ---------------------------------------------------------------------- */
/* Add table "servico"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.servico (
    cd_empresa INTEGER  NOT NULL,
    cd_servico INTEGER  NOT NULL,
    descricao_servico CHARACTER VARYING(80)  NOT NULL,
    vlr_servico CHARACTER VARYING(40) DEFAULT '''''''''''''''''0''''''''''''''''::character varying'  NOT NULL,
    data_cad_servico CHARACTER(40) DEFAULT '(''''''''''''''''now'''''''''''''''')',
    CONSTRAINT pk_servico PRIMARY KEY (cd_empresa, cd_servico)
);

CREATE UNIQUE INDEX index_servico_pk ON public.servico (cd_empresa,cd_servico);

CREATE INDEX index_servico_1_fk ON public.servico (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "equip"                                                      */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.equip (
    cd_empresa INTEGER  NOT NULL,
    cd_cliente INTEGER  NOT NULL,
    cd_tipo_equip INTEGER  NOT NULL,
    cd_equip INTEGER  NOT NULL,
    marca_equip CHARACTER VARYING(60),
    modelo_equip CHARACTER VARYING(60),
    descricao_equip CHARACTER VARYING(200),
    nro_rastreio_int_equip INTEGER,
    nro_rastreio_cli_equip INTEGER,
    nro_serie_equip CHARACTER VARYING(60),
    data_fabricacao_equip DATE,
    CONSTRAINT pk_equip PRIMARY KEY (cd_empresa, cd_equip)
);

CREATE UNIQUE INDEX index_equip_pk ON public.equip (cd_empresa,cd_equip);

CREATE INDEX index_equip_4_fk ON public.equip (cd_empresa,cd_cliente);

CREATE INDEX index_equip_3_fk ON public.equip (cd_empresa);

CREATE INDEX index_equip_2_fk ON public.equip (cd_empresa,cd_tipo_equip);

CREATE INDEX index_equip_1_fk ON public.equip (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "tipo_equip"                                                 */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.tipo_equip (
    cd_empresa INTEGER  NOT NULL,
    cd_tipo_equip INTEGER  NOT NULL,
    descricao_tipo_equip CHARACTER VARYING(80)  NOT NULL,
    CONSTRAINT pk_tipo_equip PRIMARY KEY (cd_empresa, cd_tipo_equip)
);

CREATE UNIQUE INDEX index_tipo_equip_pk ON public.tipo_equip (cd_empresa,cd_tipo_equip);

CREATE INDEX index_tipo_equip_1_fk ON public.tipo_equip (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "conexao_equip"                                              */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.conexao_equip (
    cd_empresa INTEGER  NOT NULL,
    cd_conexao_equip INTEGER  NOT NULL,
    nome_conexao_equip CHARACTER VARYING(80)  NOT NULL,
    md5_conexao_equip CHARACTER VARYING(32)  NOT NULL,
    blob_conexao_equip OID,
    data DATE DEFAULT 'currente_date',
    hora TIME DEFAULT 'current_time',
    CONSTRAINT pk_conexao_equip PRIMARY KEY (cd_empresa, cd_conexao_equip)
);

CREATE UNIQUE INDEX index_conexao_equip_pk ON public.conexao_equip (cd_empresa,cd_conexao_equip);

CREATE UNIQUE INDEX index_conexao_equip_2_ak ON public.conexao_equip (cd_conexao_equip,cd_empresa);

CREATE INDEX index_conexao_equip_1_fk ON public.conexao_equip (cd_empresa);

/* ---------------------------------------------------------------------- */
/* Add table "os_tst_equip"                                               */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.os_tst_equip (
    cd_empresa INTEGER  NOT NULL,
    cd_os_cab INTEGER  NOT NULL,
    cd_equip INTEGER  NOT NULL,
    cd_os_tst_equip INTEGER  NOT NULL,
    desc_os_tst_equip CHARACTER VARYING(100)  NOT NULL,
    def_enc_os_tst_equip CHARACTER VARYING(100)  NOT NULL,
    CONSTRAINT pk_os_tst_equip PRIMARY KEY (cd_empresa, cd_os_cab, cd_equip, cd_os_tst_equip)
);

CREATE UNIQUE INDEX index_os_tst_equip_pk ON public.os_tst_equip (cd_empresa,cd_os_cab,cd_equip,cd_os_tst_equip);

CREATE INDEX index_os_tst_equip_2_fk ON public.os_tst_equip (cd_equip,cd_empresa);

CREATE INDEX index_os_tst_equip_1_fk ON public.os_tst_equip (cd_empresa,cd_os_cab);

/* ---------------------------------------------------------------------- */
/* Add table "os_envolvidos"                                              */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.os_envolvidos (
    cd_empresa INTEGER  NOT NULL,
    cd_os_cab INTEGER  NOT NULL,
    cd_funcionario INTEGER  NOT NULL,
    CONSTRAINT pk_os_envolvidos PRIMARY KEY (cd_empresa, cd_os_cab, cd_funcionario)
);

CREATE UNIQUE INDEX index_os_envolvidos_pk ON public.os_envolvidos (cd_empresa,cd_os_cab,cd_funcionario);

CREATE INDEX index_os_envolvidos_2_fk ON public.os_envolvidos (cd_empresa,cd_funcionario);

CREATE INDEX index_os_envolvidos_1_fk ON public.os_envolvidos (cd_empresa,cd_os_cab);

/* ---------------------------------------------------------------------- */
/* Add table "orcamento"                                                  */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.orcamento (
    cd_empresa INTEGER  NOT NULL,
    cd_os_cab INTEGER  NOT NULL,
    nro_revisao_orcamento INTEGER DEFAULT 0  NOT NULL,
    autorizado_orcamento BOOLEAN DEFAULT false  NOT NULL,
    data_cad_orcamento TIMESTAMP DEFAULT '(''''''''''''''''now'''''''''''''''')'  NOT NULL,
    data_auth_orcamento TIMESTAMP,
    data_prev_orcamento DATE,
    vlr_bruto_orcamento DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_liquido_orcamento DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_desconto_orcamento DOUBLE PRECISION DEFAULT 0  NOT NULL,
    vlr_frete_orcamento DOUBLE PRECISION DEFAULT 0  NOT NULL,
    tipo_frete_orcamento CHARACTER(3) DEFAULT '''''''''''''''''FOB''''''''''''''''::bpchar'  NOT NULL,
    usa_desc_cli_orcamento BOOLEAN DEFAULT false  NOT NULL,
    dias_validade_orcamento INTEGER DEFAULT 7  NOT NULL,
    CONSTRAINT pk_orcamento PRIMARY KEY (cd_empresa, cd_os_cab, nro_revisao_orcamento)
);

CREATE UNIQUE INDEX index_orcamento_pk ON public.orcamento (cd_empresa,cd_os_cab,nro_revisao_orcamento);

CREATE INDEX index_orcamento_1_fk ON public.orcamento (cd_empresa,cd_os_cab,nro_revisao_orcamento);

/* ---------------------------------------------------------------------- */
/* Add table "relatorio"                                                  */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.relatorio (
    cd_acao INTEGER  NOT NULL,
    cd_relatorio INTEGER  NOT NULL,
    nome_relatorio CHARACTER VARYING(256)  NOT NULL,
    CONSTRAINT pk_relatorio PRIMARY KEY (cd_relatorio)
);

/* ---------------------------------------------------------------------- */
/* Add table "transportadora"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.transportadora (
    cd_empresa INTEGER  NOT NULL,
    cd_transportadora INTEGER  NOT NULL,
    CONSTRAINT pk_transportadora PRIMARY KEY (cd_empresa, cd_transportadora)
);

/* ---------------------------------------------------------------------- */
/* Add table "os_prop_tipo_equip"                                         */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.os_prop_tipo_equip (
    cd_empresa INTEGER  NOT NULL,
    cd_tipo_equip INTEGER  NOT NULL,
    cd_prop_tipo_equip INTEGER  NOT NULL,
    cd_os_cab INTEGER  NOT NULL,
    valor_os_prop_tipo_equip CHARACTER VARYING(80),
    CONSTRAINT pk_os_prop_tipo_equip PRIMARY KEY (cd_empresa, cd_tipo_equip, cd_prop_tipo_equip, cd_os_cab)
);

/* ---------------------------------------------------------------------- */
/* Add table "prop_tipo_equip"                                            */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.prop_tipo_equip (
    cd_empresa INTEGER  NOT NULL,
    cd_tipo_equip INTEGER  NOT NULL,
    cd_prop_tipo_equip INTEGER  NOT NULL,
    descr_prop_tipo_equip CHARACTER VARYING(80)  NOT NULL,
    CONSTRAINT pk_prop_tipo_equip PRIMARY KEY (cd_empresa, cd_tipo_equip, cd_prop_tipo_equip)
);

/* ---------------------------------------------------------------------- */
/* Add table "imagem_equip"                                               */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.imagem_equip (
    cd_empresa INTEGER  NOT NULL,
    cd_equip INTEGER  NOT NULL,
    cd_conexao_equip INTEGER  NOT NULL,
    ordem INTEGER DEFAULT 0  NOT NULL,
    CONSTRAINT PK_imagem_equip PRIMARY KEY (cd_empresa, cd_equip, cd_conexao_equip)
);

/* ---------------------------------------------------------------------- */
/* Add table "mov_nf"                                                     */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.mov_nf (
    cd_empresa INTEGER  NOT NULL,
    cd_fornecedor INTEGER,
    cep_logradouro_fornecedor CHARACTER VARYING(8),
    numero_endereco_fornecedor INTEGER,
    cd_transportadora INTEGER,
    cep_logradouro_transp CHARACTER VARYING(8),
    numero_endereco_transp INTEGER,
    tipo_mov_nf INTEGER  NOT NULL,
    nro_mov_nf INTEGER  NOT NULL,
    serie_mov_nf INTEGER,
    nat_op_mov_nf CHARACTER VARYING(200),
    chave_acesso_mov_nf CHARACTER VARYING(50),
    prot_auth_mov_nf CHARACTER VARYING(100),
    dt_emi_mov_nf DATE,
    dt_entrada_mov_nf DATE,
    tipo_frete_mov_nf CHARACTER VARYING(5),
    base_icms_mov_nf DOUBLE PRECISION,
    valor_icms_mov_nf DOUBLE PRECISION,
    base_icms_subs_mov_nf DOUBLE PRECISION,
    valor_icms_subs_mov_nf DOUBLE PRECISION,
    valor_tot_prod_mov_nf DOUBLE PRECISION,
    valor_frete_mov_nf DOUBLE PRECISION,
    valor_seguro_frete_mov_nf DOUBLE PRECISION,
    desconto_mov_nf DOUBLE PRECISION,
    demais_despesas_mov_nf DOUBLE PRECISION,
    valor_ipi_mov_nf DOUBLE PRECISION,
    valor_tot_mov_nf DOUBLE PRECISION,
    issqn_vlr_tot_mov_nf DOUBLE PRECISION,
    issqn_base_mov_nf DOUBLE PRECISION,
    issqn_vlr_mov_nf DOUBLE PRECISION,
    issqn_insc_mun_mov_nf CHARACTER VARYING(40),
    conta_antt_mov_nf CHARACTER VARYING(40),
    peso_bruto_carga_mov_nf DOUBLE PRECISION,
    peso_liq_carga_mov_nf DOUBLE PRECISION,
    qtd_carga_mov_nf DOUBLE PRECISION,
    placa_mov_nf CHARACTER VARYING(10),
    uf_placa_mov_nf CHARACTER VARYING(2),
    especie_carga_mov_nf CHARACTER VARYING(50),
    marca_carga_mov_nf CHARACTER VARYING(100),
    nro_carga_mov_nf CHARACTER VARYING(40),
    dados_adicionais_mov_nf CHARACTER VARYING(200),
    dt_cad DATE DEFAULT 'current_date'  NOT NULL,
    CONSTRAINT PK_mov_nf PRIMARY KEY (cd_empresa, tipo_mov_nf, nro_mov_nf)
);

/* ---------------------------------------------------------------------- */
/* Add table "mov_nf_fatura"                                              */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.mov_nf_fatura (
    cd_empresa INTEGER  NOT NULL,
    tipo_mov_nf INTEGER  NOT NULL,
    nro_mov_nf INTEGER  NOT NULL,
    numero_mov_nf_fatura CHARACTER VARYING(40)  NOT NULL,
    dt_venc_mov_nf_fatura DATE  NOT NULL,
    valor_mov_nf_fatura DOUBLE PRECISION  NOT NULL,
    CONSTRAINT PK_mov_nf_fatura PRIMARY KEY (cd_empresa, tipo_mov_nf, nro_mov_nf, numero_mov_nf_fatura)
);

/* ---------------------------------------------------------------------- */
/* Add table "mov_nf_items"                                               */
/* ---------------------------------------------------------------------- */

CREATE TABLE public.mov_nf_items (
    cd_empresa INTEGER  NOT NULL,
    tipo_mov_nf INTEGER  NOT NULL,
    nro_mov_nf INTEGER  NOT NULL,
    cd_produto INTEGER  NOT NULL,
    cd_mov_nf_items SERIAL  NOT NULL,
    qtd_mov_nf_items DOUBLE PRECISION  NOT NULL,
    vlr_un_mov_nf_items DOUBLE PRECISION  NOT NULL,
    vlr_tot_mov_nf_items DOUBLE PRECISION  NOT NULL,
    vlr_icms_mov_nf_items DOUBLE PRECISION  NOT NULL,
    aliq_mov_nf_items DOUBLE PRECISION  NOT NULL,
    CONSTRAINT PK_mov_nf_items PRIMARY KEY (cd_empresa, tipo_mov_nf, nro_mov_nf, cd_produto)
);

/* ---------------------------------------------------------------------- */
/* Foreign key constraints                                                */
/* ---------------------------------------------------------------------- */

ALTER TABLE public.lookandfeels ADD CONSTRAINT fk_lookandfeels_reference_modulos 
    FOREIGN KEY (cd_modulos) REFERENCES public.modulos (cd_modulos) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.wallpaper ADD CONSTRAINT fk_wallpaper_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.config_empresa ADD CONSTRAINT fk_config_empresa_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.opcoes_tela ADD CONSTRAINT fk_opcoes_tela_reference_classe 
    FOREIGN KEY (cd_classe) REFERENCES public.classe (cd_classe) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.config_usuario ADD CONSTRAINT fk_config_usuario_reference_wallpaper 
    FOREIGN KEY (cd_wallpaper, cd_empresa) REFERENCES public.wallpaper (cd_wallpaper,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.config_usuario ADD CONSTRAINT fk_config_usuario_reference_lookandfeels 
    FOREIGN KEY (cd_lookandfeels) REFERENCES public.lookandfeels (cd_lookandfeels) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.config_usuario ADD CONSTRAINT fk_config_usuario_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.config_usuario ADD CONSTRAINT fk_config_usuario_reference_usuario 
    FOREIGN KEY (cd_usuario, cd_empresa) REFERENCES public.usuario (cd_usuario,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.menu ADD CONSTRAINT fk_menu_reference_acao 
    FOREIGN KEY (cd_acao) REFERENCES public.acao (cd_acao) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.empresa ADD CONSTRAINT fk_logradouro_empresa 
    FOREIGN KEY (cep_logradouro) REFERENCES public.logradouro (cep_logradouro) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE public.classe ADD CONSTRAINT fk_classe_reference_modulos 
    FOREIGN KEY (cd_modulos) REFERENCES public.modulos (cd_modulos) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.classe ADD CONSTRAINT fk_acao_classe 
    FOREIGN KEY (cd_acao) REFERENCES public.acao (cd_acao) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.operacao ADD CONSTRAINT fk_acao_operacao 
    FOREIGN KEY (cd_acao) REFERENCES public.acao (cd_acao) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.usuario ADD CONSTRAINT fk_usuario_reference_funcionario 
    FOREIGN KEY (cd_empresa, cd_funcionario) REFERENCES public.funcionario (cd_empresa,cd_funcionario) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.usuario ADD CONSTRAINT fk_usuario_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.restricoes ADD CONSTRAINT fk_restricoes_reference_acao 
    FOREIGN KEY (cd_acao) REFERENCES public.acao (cd_acao) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.restricoes ADD CONSTRAINT fk_restricoes_reference_usuario 
    FOREIGN KEY (cd_usuario, cd_empresa) REFERENCES public.usuario (cd_usuario,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.restricoes ADD CONSTRAINT fk_restricoes_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.logs ADD CONSTRAINT fk_logs_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.logs ADD CONSTRAINT fk_logs_reference_usuario 
    FOREIGN KEY (cd_usuario, cd_empresa) REFERENCES public.usuario (cd_usuario,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.conta_email ADD CONSTRAINT fk_conta_email_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.uf ADD CONSTRAINT fk_uf_reference_pais 
    FOREIGN KEY (cd_pais) REFERENCES public.pais (cd_pais) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.cidade ADD CONSTRAINT fk_uf_cidade 
    FOREIGN KEY (sigla_uf) REFERENCES public.uf (sigla_uf) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.bairro ADD CONSTRAINT fk_bairro_reference_cidade 
    FOREIGN KEY (cd_cidade) REFERENCES public.cidade (cd_cidade) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.logradouro ADD CONSTRAINT fk_logradouro_reference_bairro 
    FOREIGN KEY (cd_bairro) REFERENCES public.bairro (cd_bairro) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.logradouro ADD CONSTRAINT fk_cidade_logradouro 
    FOREIGN KEY (cd_cidade) REFERENCES public.cidade (cd_cidade) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.pessoa ADD CONSTRAINT fk_pessoa_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.cliente ADD CONSTRAINT fk_cliente_reference_pessoa 
    FOREIGN KEY (cd_cliente, cd_empresa) REFERENCES public.pessoa (cd_pessoa,cd_empresa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.cliente ADD CONSTRAINT fk_cliente_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.fornecedor ADD CONSTRAINT fk_fornecedor_reference_pessoa 
    FOREIGN KEY (cd_fornecedor, cd_empresa) REFERENCES public.pessoa (cd_pessoa,cd_empresa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.fornecedor ADD CONSTRAINT fk_fornecedor_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.unidade_medida ADD CONSTRAINT fk_empresa_unidade_medida 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.forma_pagamento ADD CONSTRAINT fk_forma_pagamento_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.produto ADD CONSTRAINT fk_produto_reference_unidade_medida 
    FOREIGN KEY (sigla_unidade_m, cd_empresa) REFERENCES public.unidade_medida (sigla_unidade_m,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.produto ADD CONSTRAINT fk_produto_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.produto ADD CONSTRAINT fk_produto_reference_categoria_prod 
    FOREIGN KEY (cd_categoria_prod, cd_empresa) REFERENCES public.categoria_prod (cd_categoria_prod,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.produto ADD CONSTRAINT fk_produto_reference_imagem_prod 
    FOREIGN KEY (cd_imagem_prod, cd_empresa) REFERENCES public.imagem_prod (cd_imagem_prod,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.produto ADD CONSTRAINT fk_produto_reference_local_prod 
    FOREIGN KEY (cd_empresa, cd_local_prod) REFERENCES public.local_prod (cd_empresa,cd_local_prod) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.compra_cab ADD CONSTRAINT fk_compra_cab_reference_fornecedor 
    FOREIGN KEY (cd_empresa, cd_fornecedor) REFERENCES public.fornecedor (cd_empresa,cd_fornecedor) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.compra_cab ADD CONSTRAINT fk_compra_cab_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.compra_cab ADD CONSTRAINT fk_usuario_compra_cab 
    FOREIGN KEY (cd_empresa, cd_usuario) REFERENCES public.usuario (cd_empresa,cd_usuario) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.compra_det ADD CONSTRAINT fk_compra_det_reference_produto 
    FOREIGN KEY (cd_produto, cd_empresa) REFERENCES public.produto (cd_produto,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.compra_det ADD CONSTRAINT fk_compra_det_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.compra_det ADD CONSTRAINT fk_compra_det_reference_compra_cab 
    FOREIGN KEY (cd_empresa, cd_compra_cab) REFERENCES public.compra_cab (cd_empresa,cd_compra_cab) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.ean_prod ADD CONSTRAINT fk_ean_prod_reference_produto 
    FOREIGN KEY (cd_produto, cd_empresa) REFERENCES public.produto (cd_produto,cd_empresa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.ean_prod ADD CONSTRAINT fk_ean_prod_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.funcionario ADD CONSTRAINT fk_funcionario_reference_pessoa 
    FOREIGN KEY (cd_funcionario, cd_empresa) REFERENCES public.pessoa (cd_pessoa,cd_empresa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.funcionario ADD CONSTRAINT fk_funcionario_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.venda_cab ADD CONSTRAINT fk_venda_cab_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.venda_cab ADD CONSTRAINT fk_venda_cab_reference_cliente 
    FOREIGN KEY (cd_empresa, cd_cliente) REFERENCES public.cliente (cd_empresa,cd_cliente) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.venda_cab ADD CONSTRAINT fk_usuario_venda_cab 
    FOREIGN KEY (cd_empresa, cd_usuario) REFERENCES public.usuario (cd_empresa,cd_usuario) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.parcelamento ADD CONSTRAINT fk_parcelamento_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.venda_det ADD CONSTRAINT fk_venda_det_reference_venda_cab 
    FOREIGN KEY (cd_venda_cab, cd_empresa) REFERENCES public.venda_cab (cd_venda_cab,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.venda_det ADD CONSTRAINT fk_venda_det_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.venda_det ADD CONSTRAINT fk_venda_det_reference_produto 
    FOREIGN KEY (cd_empresa, cd_produto) REFERENCES public.produto (cd_empresa,cd_produto) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.autorun ADD CONSTRAINT fk_autorun_reference_acao 
    FOREIGN KEY (cd_acao) REFERENCES public.acao (cd_acao) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.autorun ADD CONSTRAINT fk_autorun_reference_usuario 
    FOREIGN KEY (cd_usuario, cd_empresa) REFERENCES public.usuario (cd_usuario,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.autorun ADD CONSTRAINT fk_autorun_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.licenca ADD CONSTRAINT fk_licenca_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.aviso ADD CONSTRAINT fk_aviso_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.aviso ADD CONSTRAINT fk_aviso_reference_usuario 
    FOREIGN KEY (cd_usuario, cd_empresa) REFERENCES public.usuario (cd_usuario,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.categoria_prod ADD CONSTRAINT fk_categoria_prod_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.imagem_prod ADD CONSTRAINT fk_imagem_prod_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.estoque_prod ADD CONSTRAINT fk_estoque_prod_reference_produto 
    FOREIGN KEY (cd_produto, cd_empresa) REFERENCES public.produto (cd_produto,cd_empresa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.fornecedor_prod ADD CONSTRAINT fk_fornecedor_prod_reference_fornecedor 
    FOREIGN KEY (cd_empresa, cd_fornecedor) REFERENCES public.fornecedor (cd_empresa,cd_fornecedor) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.fornecedor_prod ADD CONSTRAINT fk_fornecedor_prod_reference_produto 
    FOREIGN KEY (cd_produto, cd_empresa) REFERENCES public.produto (cd_produto,cd_empresa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.contato ADD CONSTRAINT fk_contato_reference_pessoa 
    FOREIGN KEY (cd_pessoa, cd_empresa) REFERENCES public.pessoa (cd_pessoa,cd_empresa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.endereco_pessoa ADD CONSTRAINT fk_endereco_pessoa_reference_pessoa 
    FOREIGN KEY (cd_empresa, cd_pessoa) REFERENCES public.pessoa (cd_empresa,cd_pessoa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.endereco_pessoa ADD CONSTRAINT fk_endereco_pessoa_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.endereco_pessoa ADD CONSTRAINT fk_logradouro_endereco_pessoa 
    FOREIGN KEY (cep_logradouro) REFERENCES public.logradouro (cep_logradouro) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.parcela ADD CONSTRAINT fk_parcela_reference_parcelamento 
    FOREIGN KEY (cd_empresa, cd_parcelamento) REFERENCES public.parcelamento (cd_empresa,cd_parcelamento) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.local_prod ADD CONSTRAINT fk_local_prod_reference_empresa 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_cab ADD CONSTRAINT fk_cliente_os_cab 
    FOREIGN KEY (cd_empresa, cd_cliente) REFERENCES public.cliente (cd_empresa,cd_cliente) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_cab ADD CONSTRAINT fk_usuario_os_cab 
    FOREIGN KEY (cd_empresa, cd_usuario) REFERENCES public.usuario (cd_empresa,cd_usuario) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_cab ADD CONSTRAINT fk_contato_os_cab 
    FOREIGN KEY (cd_empresa, cd_contato_cliente) REFERENCES public.contato (cd_empresa,cd_contato) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_cab ADD CONSTRAINT fk_endereco_pessoa_os_cab 
    FOREIGN KEY (cd_empresa, numero_endereco_cliente, cep_logradouro_cliente) REFERENCES public.endereco_pessoa (cd_empresa,numero_endereco_pessoa,cep_logradouro) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_cab ADD CONSTRAINT fk_equip_os_cab 
    FOREIGN KEY (cd_equip, cd_empresa) REFERENCES public.equip (cd_equip,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_cab ADD CONSTRAINT fk_local_prod_os_cab 
    FOREIGN KEY (cd_empresa, cd_local_prod) REFERENCES public.local_prod (cd_empresa,cd_local_prod) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_cab ADD CONSTRAINT fk_transportadora_os_cab 
    FOREIGN KEY (cd_empresa, cd_transportadora) REFERENCES public.transportadora (cd_empresa,cd_transportadora) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_prod_det ADD CONSTRAINT fk_os_cab_os_prod_det 
    FOREIGN KEY (cd_empresa, cd_os_cab) REFERENCES public.os_cab (cd_empresa,cd_os_cab) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE public.os_prod_det ADD CONSTRAINT fk_produto_os_prod_det 
    FOREIGN KEY (cd_empresa, cd_produto) REFERENCES public.produto (cd_empresa,cd_produto) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.os_servico_det ADD CONSTRAINT fk_os_cab_os_servico_det 
    FOREIGN KEY (cd_empresa, cd_os_cab) REFERENCES public.os_cab (cd_empresa,cd_os_cab) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE public.os_servico_det ADD CONSTRAINT fk_servico_os_servico_det 
    FOREIGN KEY (cd_empresa, cd_servico) REFERENCES public.servico (cd_empresa,cd_servico) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.servico ADD CONSTRAINT fk_empresa_servico 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.equip ADD CONSTRAINT fk_empresa_equip 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.equip ADD CONSTRAINT fk_tipo_equip_equip 
    FOREIGN KEY (cd_empresa, cd_tipo_equip) REFERENCES public.tipo_equip (cd_empresa,cd_tipo_equip) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.equip ADD CONSTRAINT fk_conexao_equip_equip 
    FOREIGN KEY (cd_empresa) REFERENCES public.conexao_equip (cd_empresa) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE public.equip ADD CONSTRAINT fk_cliente_equip 
    FOREIGN KEY (cd_empresa, cd_cliente) REFERENCES public.cliente (cd_empresa,cd_cliente) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE public.tipo_equip ADD CONSTRAINT fk_empresa_tipo_equip 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_tst_equip ADD CONSTRAINT fk_os_cab_os_tst_equip 
    FOREIGN KEY (cd_empresa, cd_os_cab) REFERENCES public.os_cab (cd_empresa,cd_os_cab) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE public.os_tst_equip ADD CONSTRAINT fk_equip_os_tst_equip 
    FOREIGN KEY (cd_equip, cd_empresa) REFERENCES public.equip (cd_equip,cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.os_envolvidos ADD CONSTRAINT fk_os_cab_os_envolvidos 
    FOREIGN KEY (cd_empresa, cd_os_cab) REFERENCES public.os_cab (cd_empresa,cd_os_cab) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.os_envolvidos ADD CONSTRAINT fk_funcionario_os_envolvidos 
    FOREIGN KEY (cd_empresa, cd_funcionario) REFERENCES public.funcionario (cd_empresa,cd_funcionario) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.orcamento ADD CONSTRAINT fk_os_cab_orcamento 
    FOREIGN KEY (cd_empresa, cd_os_cab) REFERENCES public.os_cab (cd_empresa,cd_os_cab) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE public.relatorio ADD CONSTRAINT fk_acao_relatorio 
    FOREIGN KEY (cd_acao) REFERENCES public.acao (cd_acao) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.transportadora ADD CONSTRAINT fk_empresa_transportadora 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.transportadora ADD CONSTRAINT fk_pessoa_transportadora 
    FOREIGN KEY (cd_empresa, cd_transportadora) REFERENCES public.pessoa (cd_empresa,cd_pessoa) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.os_prop_tipo_equip ADD CONSTRAINT fk_os_cab_os_prop_tipo_equip 
    FOREIGN KEY (cd_empresa, cd_os_cab) REFERENCES public.os_cab (cd_empresa,cd_os_cab) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.os_prop_tipo_equip ADD CONSTRAINT fk_prop_tipo_equip_os_prop_tipo_equip 
    FOREIGN KEY (cd_empresa, cd_tipo_equip, cd_prop_tipo_equip) REFERENCES public.prop_tipo_equip (cd_empresa,cd_tipo_equip,cd_prop_tipo_equip) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.prop_tipo_equip ADD CONSTRAINT fk_tipo_equip_prop_tipo_equip 
    FOREIGN KEY (cd_empresa, cd_tipo_equip) REFERENCES public.tipo_equip (cd_empresa,cd_tipo_equip) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.imagem_equip ADD CONSTRAINT fk_empresa_imagem_equip 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.imagem_equip ADD CONSTRAINT fk_equip_imagem_equip 
    FOREIGN KEY (cd_empresa, cd_equip) REFERENCES public.equip (cd_empresa,cd_equip) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.imagem_equip ADD CONSTRAINT fk_conexao_equip_imagem_equip 
    FOREIGN KEY (cd_empresa, cd_conexao_equip) REFERENCES public.conexao_equip (cd_empresa,cd_conexao_equip) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.mov_nf ADD CONSTRAINT fk_empresa_mov_nf 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.mov_nf ADD CONSTRAINT fk_transportadora_mov_nf 
    FOREIGN KEY (cd_empresa, cd_transportadora) REFERENCES public.transportadora (cd_empresa,cd_transportadora) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.mov_nf ADD CONSTRAINT fk_fornecedor_mov_nf 
    FOREIGN KEY (cd_empresa, cd_fornecedor) REFERENCES public.fornecedor (cd_empresa,cd_fornecedor) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.mov_nf ADD CONSTRAINT fk_endereco_pessoa_mov_nf 
    FOREIGN KEY (cd_empresa, cep_logradouro_fornecedor, numero_endereco_fornecedor, cd_fornecedor) REFERENCES public.endereco_pessoa (cd_empresa,cep_logradouro,numero_endereco_pessoa,cd_pessoa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.mov_nf ADD CONSTRAINT fk_endereco_pessoa_mov_nf_2 
    FOREIGN KEY (cd_empresa, cep_logradouro_transp, numero_endereco_transp, cd_transportadora) REFERENCES public.endereco_pessoa (cd_empresa,cep_logradouro,numero_endereco_pessoa,cd_pessoa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.mov_nf_fatura ADD CONSTRAINT fk_empresa_mov_nf_fatura 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.mov_nf_fatura ADD CONSTRAINT fk_mov_nf_mov_nf_fatura 
    FOREIGN KEY (cd_empresa, tipo_mov_nf, nro_mov_nf) REFERENCES public.mov_nf (cd_empresa,tipo_mov_nf,nro_mov_nf) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.mov_nf_items ADD CONSTRAINT fk_empresa_mov_nf_items 
    FOREIGN KEY (cd_empresa) REFERENCES public.empresa (cd_empresa) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE public.mov_nf_items ADD CONSTRAINT fk_mov_nf_mov_nf_items 
    FOREIGN KEY (cd_empresa, tipo_mov_nf, nro_mov_nf) REFERENCES public.mov_nf (cd_empresa,tipo_mov_nf,nro_mov_nf) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE public.mov_nf_items ADD CONSTRAINT fk_produto_mov_nf_items 
    FOREIGN KEY (cd_empresa, cd_produto) REFERENCES public.produto (cd_empresa,cd_produto) ON DELETE RESTRICT ON UPDATE RESTRICT;

/* ---------------------------------------------------------------------- */
/* Procedures                                                             */
/* ---------------------------------------------------------------------- */

CREATE OR REPLACE FUNCTION valida_licenca(cd_empresa Integer)
RETURNS Boolean AS $body$
DECLARE
	query TEXT;
	row RECORD;
BEGIN
	query := 'SELECT data_expira FROM licenca WHERE cd_empresa = '|| cd_empresa 
		|| ' and data_expira >= current_date;';
	FOR row IN EXECUTE query LOOP
		RETURN true;
	END LOOP;
	RETURN false;
END
$body$ LANGUAGE plpgsql;;

CREATE OR REPLACE FUNCTION aplica_licenca(cd_empresa Integer, data_exp date)
RETURNS Boolean AS $body$
DECLARE
	query TEXT;
	row RECORD;
BEGIN
	query := 'INSERT INTO licenca (cd_empresa, data_expira) values('|| cd_empresa 
		|| ', $$' || data_exp || '$$) RETURNING cd_empresa;';
	FOR row IN EXECUTE query LOOP
		RETURN true;
	END LOOP;
	RETURN false;
END
$body$ LANGUAGE plpgsql;;

CREATE OR REPLACE FUNCTION set_all_restrictions_for_user(EMPRESA_CD integer, USUARIO_CD integer) RETURNS boolean AS
$BODY$
DECLARE
	query1 TEXT;
	row1 RECORD;
	query2 TEXT;
	row2 RECORD;
BEGIN
	query1 := 'SELECT cd_usuario FROM usuario WHERE cd_empresa = '|| EMPRESA_CD || ' and cd_usuario = ' || USUARIO_CD ||';';
	FOR row1 IN EXECUTE query1 LOOP
	  DELETE FROM restricoes WHERE cd_empresa = EMPRESA_CD and cd_usuario = USUARIO_CD ;
	  query2 := 'SELECT cd_acao FROM acao;';
	  FOR row2 IN EXECUTE query2 LOOP
	    INSERT INTO restricoes (cd_empresa, cd_usuario, cd_acao) values (EMPRESA_CD, USUARIO_CD, row2.cd_acao);
	  END LOOP;
	  RETURN true;
	END LOOP;
	RETURN false;
END
$BODY$
LANGUAGE plpgsql VOLATILE;;

CREATE OR REPLACE FUNCTION copy_restrictions_user_for_user(EMPRESA_CD integer, USUARIO_O_CD integer, USUARIO_D_CD integer) RETURNS boolean AS
$BODY$	
DECLARE
	query TEXT;
	row RECORD;
BEGIN
	query := 'SELECT cd_acao FROM restricoes WHERE cd_empresa = '|| EMPRESA_CD || ' and cd_usuario = ' || USUARIO_O_CD ||';';
	FOR row IN EXECUTE query LOOP
	  DELETE FROM restricoes WHERE cd_empresa = EMPRESA_CD and cd_usuario = USUARIO_D_CD and cd_acao = row.cd_acao;
	  INSERT INTO restricoes (cd_empresa, cd_usuario, cd_acao) values (EMPRESA_CD, USUARIO_D_CD, row.cd_acao);
	END LOOP;
	RETURN true;
END
$BODY$
LANGUAGE plpgsql VOLATILE;;

CREATE OR REPLACE FUNCTION delete_user(EMPRESA_CD integer, USUARIO_CD integer) RETURNS boolean AS
$BODY$	
DECLARE
	query TEXT;
	row RECORD;
BEGIN
	query := 'SELECT cd_usuario FROM usuario WHERE cd_empresa = '|| EMPRESA_CD || ' and cd_usuario = ' || USUARIO_CD ||';';
	FOR row IN EXECUTE query LOOP
	  DELETE FROM restricoes WHERE cd_empresa = EMPRESA_CD and cd_usuario = USUARIO_CD ;
	  DELETE FROM autorun WHERE cd_empresa = EMPRESA_CD and cd_usuario = USUARIO_CD ;
	  DELETE FROM aviso WHERE cd_empresa = EMPRESA_CD and cd_usuario = USUARIO_CD ;
	  DELETE FROM logs WHERE cd_empresa = EMPRESA_CD and cd_usuario = USUARIO_CD ;
	  DELETE FROM config_usuario WHERE cd_empresa = EMPRESA_CD and cd_usuario = USUARIO_CD ;
	  DELETE FROM usuario WHERE cd_empresa = EMPRESA_CD and cd_usuario = USUARIO_CD ;
	  RETURN true;
	END LOOP;
	RETURN false;
END
$BODY$
LANGUAGE plpgsql VOLATILE;;

CREATE OR REPLACE FUNCTION delete_not_using_imagem_prod(empresa_cd integer)
  RETURNS boolean AS
$BODY$	
DECLARE
	query TEXT;
	row RECORD;
BEGIN
	query := 'SELECT cd_imagem_prod FROM imagem_prod WHERE cd_empresa = '|| EMPRESA_CD || ' and cd_imagem_prod not in (SELECT cd_imagem_prod FROM produto WHERE cd_empresa = '|| EMPRESA_CD || ');';
	FOR row IN EXECUTE query LOOP
	  DELETE FROM imagem_prod WHERE cd_empresa = EMPRESA_CD and cd_imagem_prod = row.cd_imagem_prod;
	END LOOP;
	RETURN true;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION delete_not_using_imagem_prod(integer) OWNER TO postgres;;;

CREATE OR REPLACE FUNCTION delete_not_using_wallpaper(empresa_cd integer)
  RETURNS boolean AS
$BODY$	
DECLARE
	query TEXT;
	row RECORD;
BEGIN
	query := 'SELECT cd_wallpaper FROM wallpaper WHERE cd_empresa = '|| EMPRESA_CD || ' and cd_wallpaper not in (SELECT cd_wallpaper FROM config_usuario WHERE cd_empresa = '|| EMPRESA_CD || ');';
	FOR row IN EXECUTE query LOOP
	  DELETE FROM wallpaper WHERE cd_empresa = EMPRESA_CD and cd_wallpaper = row.cd_wallpaper;
	END LOOP;
	RETURN true;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION delete_not_using_wallpaper(integer) OWNER TO postgres;;;

/* ---------------------------------------------------------------------- */
/* Triggers                                                               */
/* ---------------------------------------------------------------------- */

REATE OR REPLACE FUNCTION fc_cria_estoque()
  RETURNS trigger AS
$BODY$
  DECLARE
    	contador integer;
	item RECORD;
 BEGIN
    contador = 0;
    FOR item IN (SELECT quantidade_estoque FROM estoque_prod WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto)
    LOOP
        contador = contador + 1;
    END LOOP;
    IF NOT (contador > 0) THEN
      INSERT INTO estoque_prod (cd_empresa, cd_produto, quantidade_estoque, dt_alt_estoque) VALUES (NEW.cd_empresa, NEW.cd_produto, 0, CURRENT_DATE);
    END IF;
    RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fc_cria_estoque()
  OWNER TO postgres;

-- Trigger: atu_cria_estoque on produto

-- DROP TRIGGER atu_cria_estoque ON produto;

CREATE TRIGGER atu_cria_estoque
  AFTER INCREATE OR REPLACE FUNCTION fc_cria_estoque()
  RETURNS trigger AS
$BODY$
  DECLARE
    	contador integer;
	item RECORD;
 BEGIN
    contador = 0;
    FOR item IN (SELECT quantidade_estoque FROM estoque_prod WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto)
    LOOP
        contador = contador + 1;
    END LOOP;
    IF NOT (contador > 0) THEN
      INSERT INTO estoque_prod (cd_empresa, cd_produto, quantidade_estoque, dt_alt_estoque) VALUES (NEW.cd_empresa, NEW.cd_produto, 0, CURRENT_DATE);
    END IF;
    RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fc_cria_estoque()
  OWNER TO postgres;

CREATE TRIGGER atu_cria_estoque
  AFTER INSERT OR UPDATE
  ON produto
  FOR EACH ROW
  EXECUTE PROCEDURE fc_cria_estoque();;

CREATE OR REPLACE FUNCTION atualiza_data_garantia_os()
  RETURNS trigger AS
$BODY$
DECLARE meses INTEGER;
BEGIN
	meses := NEW.meses_garantia_os_cab;
	NEW.data_final_garantia_os_cab := CURRENT_DATE + (meses || ' month')::INTERVAL;
  return new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION atualiza_data_garantia_os()
  OWNER TO postgres;

CREATE TRIGGER tg_atualiza_data_garantia_os
BEFORE INSERT OR UPDATE ON os_cab
    FOR EACH ROW EXECUTE PROCEDURE atualiza_data_garantia_os();;

CREATE OR REPLACE FUNCTION estoque_mov_nv()
  RETURNS trigger AS
$BODY$
  DECLARE
    qtdeproduto double precision;

 BEGIN
  
  IF (TG_OP = 'DELETE') THEN 
    SELECT 
      INTO qtdeproduto 
           quantidade_estoque 
      FROM estoque_prod 
     WHERE cd_empresa = OLD.cd_empresa AND cd_produto = OLD.cd_produto;

    IF (OLD.tipo_mov_nf = 1) THEN
      UPDATE estoque_prod 
         SET quantidade_estoque = qtdeproduto + OLD.qtd_mov_nf_items, dt_alt_estoque = current_date
       WHERE cd_empresa = OLD.cd_empresa AND cd_produto = OLD.cd_produto;
    ELSIF (OLD.tipo_mov_nf = 0) THEN 
      UPDATE estoque_prod 
         SET quantidade_estoque = qtdeproduto - OLD.qtd_mov_nf_items, dt_alt_estoque = current_date
       WHERE cd_empresa = OLD.cd_empresa AND cd_produto = OLD.cd_produto;
    END IF;

  
  ELSIF (TG_OP = 'UPDATE') THEN 
    SELECT 
      INTO qtdeproduto 
           quantidade_estoque 
      FROM estoque_prod 
     WHERE cd_empresa = OLD.cd_empresa AND cd_produto = OLD.cd_produto;

    IF (OLD.tipo_mov_nf = 1) THEN
      UPDATE estoque_prod 
         SET quantidade_estoque = qtdeproduto + OLD.qtd_mov_nf_items, dt_alt_estoque = current_date
       WHERE cd_empresa = OLD.cd_empresa AND cd_produto = OLD.cd_produto;
    ELSIF (OLD.tipo_mov_nf = 0) THEN 
      UPDATE estoque_prod 
         SET quantidade_estoque = qtdeproduto - OLD.qtd_mov_nf_items, dt_alt_estoque = current_date
       WHERE cd_empresa = OLD.cd_empresa AND cd_produto = OLD.cd_produto;
    END IF;

    
    SELECT 
      INTO qtdeproduto 
           quantidade_estoque 
      FROM estoque_prod 
     WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto;

    IF (NEW.tipo_mov_nf = 1) THEN
      UPDATE estoque_prod 
         SET quantidade_estoque = qtdeproduto - NEW.qtd_mov_nf_items
       WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto;
    ELSIF (NEW.tipo_mov_nf = 0) THEN 
      UPDATE estoque_prod 
         SET quantidade_estoque = qtdeproduto + NEW.qtd_mov_nf_items
       WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto;
      UPDATE produto 
         SET vlr_compra_produto = NEW.vlr_un_mov_nf_items
       WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto;
    END IF;


  ELSIF (TG_OP = 'INSERT') THEN 
    SELECT 
      INTO qtdeproduto 
           quantidade_estoque 
      FROM estoque_prod 
     WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto;

    IF (NEW.tipo_mov_nf = 1) THEN
      UPDATE estoque_prod 
         SET quantidade_estoque = qtdeproduto - NEW.qtd_mov_nf_items
       WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto;
    ELSIF (NEW.tipo_mov_nf = 0) THEN
      UPDATE estoque_prod 
         SET quantidade_estoque = qtdeproduto + NEW.qtd_mov_nf_items
       WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto;
      UPDATE produto 
         SET vlr_compra_produto = NEW.vlr_un_mov_nf_items
       WHERE cd_empresa = NEW.cd_empresa AND cd_produto = NEW.cd_produto;
    END IF;
  END IF;

  return new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION estoque_mov_nv()
  OWNER TO postgres;


CREATE TRIGGER atualiza_estoque_mov_nf
  AFTER INSERT OR UPDATE OR DELETE
  ON mov_nf_items
  FOR EACH ROW
  EXECUTE PROCEDURE estoque_mov_nv();;
