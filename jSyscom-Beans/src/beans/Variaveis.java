/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import base.FrmPrincipal;
import funcoes.OperacoesDB;
import java.awt.Component;
import java.net.URL;
import utilitarios.Arquivo;
import utilitarios.DataBase;
import utilitarios.Extra;
import utilitarios.Format;

/**
 *
 * @author Jean
 */
public abstract class Variaveis {

    private static String   versao      = "0.05-jS";
    private static String   ipServ      = "localhost";
    private static String   dbLocal     = "jsyscom";
    private static String   dbUser      = "root";
    private static String   dbPass      = "";
    private static Integer  dbPort      = 5432;
    private static Integer  dbType      = DataBase.POSTGRES;
    private static Object   retorno;
    private static String   Log         = "";
    private static String   oldLog      = "";
    private static Boolean  net         = false;
    private static String   aviso       = "";
    private static Integer  cdAviso     = -1;
    private static String   nomeMenu    = "Menu Principal";
    private static Boolean  newLog      = false;
    private static Boolean  gravaLog    = true;
    private static Integer  distanciaFrames = 20;
    private static Integer  idImgFundo  = -1;
    private static Usuario  userSys     = null;
    private static Empresa  empresa     = null;
    private static String   nomeLogo    = "logo.png";
    private static Boolean  bloqueado   = false;
    private static int      decimal     = 2;
    private static FrmPrincipal formularioPrincipal = null;
    private static URL      urlLogo = null;

    public static String getNomeLogo() {
        return nomeLogo;
    }

    public static void setNomeLogo(String nome_logo) {
        Variaveis.nomeLogo = nome_logo;
    }

    public static Usuario getUserSys() {
        return userSys;
    }

    public static void setUserSys(Usuario userSys) {
        Variaveis.userSys = userSys;
    }

    public static Integer getIdImgFundo() {
        return idImgFundo;
    }

    public static void setIdImgFundo(Integer id_imgFundo) {
        Variaveis.idImgFundo = id_imgFundo;
    }

    public static Boolean getNet() {
        return net;
    }

    public static void setNet(Boolean net) {
        Variaveis.net = net;
    }
    //     DADOS DO USUARIO

    public static String getAllAtributs() {
        String ret = "----Variaveis----\n"
                + "versao = '" + versao + "'\n"
                + "path = '" + Arquivo.getPathBase() + "'\n"
                + "ipServ = '" + ipServ + "'\n"
                + "dbLocal = '" + dbLocal + "'\n"
                + "dbUser = '" + dbUser + "'\n"
                + "dbPass = size('" + String.valueOf(dbPass.length()) + "')\n"
                + "nomeMenu = '" + nomeMenu + "'\n"
                + "Log = '" + Log + "'\n"
                + "decimal = '"+ String.valueOf(decimal) + "'\n"
                + "userNome = '" + userSys.getNomeUsuario() + "'\n"
                + "userCodigo = '" + String.valueOf(userSys.getCodigoUsuario()) + "'\n"
                + "empresaNome = '" + getEmpresa().getFantasia() + "'\n"
                + "empresaCodigo = '" + String.valueOf(getEmpresa().getCodigo()) + "'\n"
                + "dbPort = '" + String.valueOf(dbPort) + "'\n"
                + "dbType = '" + String.valueOf(dbType) + "'\n"
                + "gravaLog = '" + String.valueOf(gravaLog) + "'\n"
                + "distanciaFrames = '" + String.valueOf(distanciaFrames) + "'\n"
                + "idImgFundo = '" + String.valueOf(idImgFundo) + "'\n"
                + "pathImage = '" + String.valueOf(getIDImage()) + "'\n"
                + "net = '" + (getNet()?"Sim":"Não") + "'\n"
                + "newLog = '" + newLog.toString();
        return ret;
    }

    /**
     * @return the ipServ
     */
    public static String getIpServ() {
        return ipServ;
    }

    public static Boolean getNewLog() {
        return newLog;
    }

    public static void setNewLog(Boolean newLog) {
        Variaveis.newLog = newLog;
    }

    public static void addNewLog(String log, Boolean saveDB, Boolean showMessage, Component tela) {
        log = "["+Format.getDate()+"  -  "+Format.getTime()+"]\n"+log;
        Log += log + "\n";
        oldLog += "[" + Format.getDate() + " - " + Format.getTime() + "]: " + log + "\n";
        newLog = true;
        Arquivo.GravaLog("jSyscom.log", log);
        if (saveDB) {
            OperacoesDB.gravaLogBanco("LOG TEMPO REAL", "INTERNA", log);
        }
        if(showMessage){
            Extra.Informacao(tela, log);
        }
    }

    /**
     * @param aIpServ the ipServ to set
     */
    public static void setIpServ(String aIpServ) {
        ipServ = aIpServ;
    }

    /**
     * @return the dbLocal
     */
    public static String getDbLocal() {
        return dbLocal;
    }

    /**
     * @param aDbLocal the dbLocal to set
     */
    public static void setDbLocal(String aDbLocal) {
        dbLocal = aDbLocal;
    }

    /**
     * @return the dbUser
     */
    public static String getDbUser() {
        return dbUser;
    }

    /**
     * @param aDbUser the dbUser to set
     */
    public static void setDbUser(String aDbUser) {
        dbUser = aDbUser;
    }

    /**
     * @return the dbPass
     */
    public static String getDbPass() {
        return dbPass;
    }

    /**
     * @param aDbPass the dbPass to set
     */
    public static void setDbPass(String aDbPass) {
        dbPass = aDbPass;
    }

    /**
     * @return the versao
     */
    public static String getVersao() {
        return versao;
    }

    /**
     * @param aVersao the versao to set
     */
    public static void setVersao(String aVersao) {
        versao = aVersao;
    }

    /**
     * @return the nomeMenu
     */
    public static String getNomeMenu() {
        return nomeMenu;
    }

    /**
     * @param aNomeMenu the nomeMenu to set
     */
    public static void setNomeMenu(String aNomeMenu) {
        nomeMenu = aNomeMenu;
    }

    /**
     * @return the retorno
     */
    public static Object getRetorno() {
        return retorno;
    }

    /**
     * @param aRetorno the retorno to set
     */
    public static void setRetorno(Object aRetorno) {
        retorno = aRetorno;
    }

    /**
     * @return the Log
     */
    public static String getLog() {
        if (newLog) {
            newLog = false;
            String t = Log;
            Log = "";
            return t;
        }
        return "";
    }

    /**
     * @param aLog the Log to set
     */
    public static void setLog(String aLog) {
        Log = aLog;
    }

    /**
     * @return the oldLog
     */
    public static String getOldLog() {
        return oldLog;
    }

    /**
     * @param aOldLog the oldLog to set
     */
    public static void setOldLog(String aOldLog) {
        oldLog = aOldLog;
    }

    /**
     * @return the distanciaFrames
     */
    public static Integer getDistanciaFrames() {
        return distanciaFrames;
    }

    /**
     * @param aDistanciaFrames the distanciaFrames to set
     */
    public static void setDistanciaFrames(Integer aDistanciaFrames) {
        distanciaFrames = aDistanciaFrames;
    }

    /**
     * @return the gravaLog
     */
    public static Boolean getGravaLog() {
        return gravaLog;
    }

    /**
     * @param aGravaLog the gravaLog to set
     */
    public static void setGravaLog(Boolean aGravaLog) {
        gravaLog = aGravaLog;
    }

    /**
     * @return the id_imgFundo
     */
    public static Integer getIDImage() {
        return idImgFundo;
    }

    /**
     * @param aPathImage the id_imgFundo to set
     */
    public static void setIDImage(Integer aPathImage) {
        idImgFundo = aPathImage;
    }

    /**
     * @return the dbPort
     */
    public static Integer getDbPort() {
        return dbPort;
    }

    /**
     * @param aDbPort the dbPort to set
     */
    public static void setDbPort(Integer aDbPort) {
        dbPort = aDbPort;
    }

    /**
     * @return the dbType
     */
    public static Integer getDbType() {
        return dbType;
    }

    /**
     * @param aDbType the dbType to set
     */
    public static void setDbType(Integer aDbType) {
        dbType = aDbType;
    }

    /**
     * @return the aviso
     */
    public static String getAviso() {
        return aviso;
    }

    /**
     * @param aAviso the aviso to set
     */
    public static void setAviso(String aAviso) {
        aviso = aAviso;
    }

    /**
     * @return the cd_aviso
     */
    public static Integer getCdAviso() {
        return cdAviso;
    }

    /**
     * @param aCd_aviso the cd_aviso to set
     */
    public static void setCdAviso(Integer aCd_aviso) {
        cdAviso = aCd_aviso;
    }

    /**
     * @return the empresa
     */
    public static Empresa getEmpresa() {
        return empresa;
    }

    /**
     * @param aEmpresa the empresa to set
     */
    public static void setEmpresa(Empresa aEmpresa) {
        empresa = aEmpresa;
    }

    /**
     * @return the bloqueado
     */
    public static Boolean getBloqueado() {
        return bloqueado;
    }

    /**
     * @param aBloqueado the bloqueado to set
     */
    public static void setBloqueado(Boolean aBloqueado) {
        bloqueado = aBloqueado;
    }

    /**
     * @return the formularioPrincipal
     */
    public static FrmPrincipal getFormularioPrincipal() {
        return formularioPrincipal;
    }

    /**
     * @param aFormularioPrincipal the formularioPrincipal to set
     */
    public static void setFormularioPrincipal(FrmPrincipal aFormularioPrincipal) {
        formularioPrincipal = aFormularioPrincipal;
    }

    /**
     * @return the decimal
     */
    public static int getDecimal() {
        return decimal;
    }

    /**
     * @param aDecimal the decimal to set
     */
    public static void setDecimal(int aDecimal) {
        decimal = aDecimal;
    }

    /**
     * @return the urlLogo
     */
    public static URL getUrlLogo() {
        return urlLogo;
    }

    /**
     * @param aUrlLogo the urlLogo to set
     */
    public static void setUrlLogo(URL aUrlLogo) {
        urlLogo = aUrlLogo;
    }
}
