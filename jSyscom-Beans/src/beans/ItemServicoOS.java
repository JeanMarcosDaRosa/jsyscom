/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Objects;

/**
 *
 * @author jean-marcos
 */
public class ItemServicoOS {

    private Servico servico;
    private String observacoes;
    private Double quantidade;
    private Boolean mostrarQtde;
    private Double valorUnitario;
    private Double valorBruto;
    private OrdemServico ordemServico;

    public ItemServicoOS() {
    }

    public ItemServicoOS(OrdemServico ordemServico, Servico servico, String observacoes, Double quantidade, Boolean mostrarQtde, Double valorUnitario, Double valorBruto) {
        this.ordemServico = ordemServico;
        this.servico = servico;
        this.observacoes = observacoes;
        this.quantidade = quantidade;
        this.mostrarQtde = mostrarQtde;
        this.valorUnitario = valorUnitario;
        this.valorBruto = valorBruto;
    }


    /**
     * @return the servico
     */
    public Servico getServico() {
        return servico;
    }

    /**
     * @param servico the servico to set
     */
    public void setServico(Servico servico) {
        this.servico = servico;
    }

    /**
     * @return the observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * @param observacoes the observacoes to set
     */
    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    /**
     * @return the valorBruto
     */
    public Double getValorBruto() {
        return valorBruto;
    }

    /**
     * @param valorBruto the valorBruto to set
     */
    public void setValorBruto(Double valorBruto) {
        this.valorBruto = valorBruto;
    }

    /**
     * @return the quantidade
     */
    public Double getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * @return the mostrarQtde
     */
    public Boolean getMostrarQtde() {
        return mostrarQtde;
    }

    /**
     * @param mostrarQtde the mostrarQtde to set
     */
    public void setMostrarQtde(Boolean mostrarQtde) {
        this.mostrarQtde = mostrarQtde;
    }

    /**
     * @return the valorUnitario
     */
    public Double getValorUnitario() {
        return valorUnitario;
    }

    /**
     * @param valorUnitario the valorUnitario to set
     */
    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemServicoOS other = (ItemServicoOS) obj;
        if (!Objects.equals(this.servico, other.servico)) {
            return false;
        }
        if (!Objects.equals(this.valorUnitario, other.valorUnitario)) {
            return false;
        }
        return true;
    }

    /**
     * @return the ordemServico
     */
    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    /**
     * @param ordemServico the ordemServico to set
     */
    public void setOrdemServico(OrdemServico ordemServico) {
        this.ordemServico = ordemServico;
    }
}
