/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

/**
 *
 * @author Jean
 */
public class Produto extends Observable implements Cloneable {

    private Integer codProduto;
    private String descricaoProduto;
    private String detalhesProduto;
    private Date dataCadastroProduto;
    private Double valorCompraProduto;
    private Double valorVendaProduto;
    private Double estMinimo;
    private Double qtdEstoque;
    private Boolean situacaoProduto;
    private Imagem imagemProduto;
    private CategoriaProd categoriaProd;
    private LocalProd localProd;
    private Unidade unidade;
    private ArrayList<Ean> eans;
    private ArrayList<Fornecedor> fornecedores;
    private Boolean flagPronto;

    public Produto() {
        this.eans = new ArrayList();
        this.fornecedores = new ArrayList();
        clearProduto();
    }

    public Produto(Integer codProduto) {
        this.eans = new ArrayList();
        this.fornecedores = new ArrayList();
        clearProduto();
        this.codProduto = codProduto;
    }

    public final void clearProduto() {
        this.codProduto = null;
        this.descricaoProduto = null;
        this.detalhesProduto = null;
        this.dataCadastroProduto = new Date();
        this.valorCompraProduto = Double.valueOf(0.0D);
        this.valorVendaProduto = Double.valueOf(0.0D);
        this.estMinimo = Double.valueOf(0.0D);
        this.qtdEstoque = Double.valueOf(0.0D);
        this.situacaoProduto = Boolean.valueOf(true);
        this.imagemProduto = null;
        this.categoriaProd = null;
        this.localProd = null;
        this.unidade = null;
        this.eans.clear();
        this.fornecedores.clear();
    }

    public Integer getCodProduto() {
        return this.codProduto;
    }

    public void setCodProduto(Integer codProduto) {
        this.codProduto = codProduto;
    }

    public ArrayList<Ean> getEans() {
        return this.eans;
    }

    public void setEans(ArrayList<Ean> eans) {
        this.eans = eans;
    }

    public ArrayList<Fornecedor> getFornecedores() {
        return this.fornecedores;
    }

    public void setFornecedores(ArrayList<Fornecedor> fornecedores) {
        this.fornecedores = fornecedores;
    }

    public String getDescricaoProduto() {
        return this.descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = (descricaoProduto != null ? descricaoProduto.toUpperCase().trim() : null);
    }

    public Date getDataCadastroProduto() {
        return this.dataCadastroProduto;
    }

    public void setDataCadastroProduto(Date dataCadastroProduto) {
        this.dataCadastroProduto = dataCadastroProduto;
    }

    public Double getValorCompraProduto() {
        return this.valorCompraProduto;
    }

    public void setValorCompraProduto(Double valorCompraProduto) {
        this.valorCompraProduto = valorCompraProduto;
    }

    public Double getValorVendaProduto() {
        return this.valorVendaProduto;
    }

    public void setValorVendaProduto(Double valorVendaProduto) {
        this.valorVendaProduto = valorVendaProduto;
    }

    public Boolean getSituacaoProduto() {
        return this.situacaoProduto;
    }

    public void setSituacaoProduto(Boolean situacaoProduto) {
        this.situacaoProduto = situacaoProduto;
    }

    public Imagem getImagemProduto() {
        return this.imagemProduto;
    }

    public void setImagemProduto(Imagem imagemProduto) {
        this.imagemProduto = imagemProduto;
    }

    public CategoriaProd getCategoriaProd() {
        return this.categoriaProd;
    }

    public void setCategoriaProd(CategoriaProd categoriaProd) {
        this.categoriaProd = categoriaProd;
    }

    public Unidade getUnidade() {
        return this.unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public LocalProd getLocalProd() {
        return this.localProd;
    }

    public void setLocalProd(LocalProd localProd) {
        this.localProd = localProd;
    }

    public Boolean getFlagPronto() {
        return this.flagPronto;
    }

    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();
        notifyObservers();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Produto other = (Produto) obj;
        if ((this.codProduto != other.codProduto) && ((this.codProduto == null) || (!this.codProduto.equals(other.codProduto)))) {
            return false;
        }
        if (this.descricaoProduto == null ? other.descricaoProduto != null : !this.descricaoProduto.equals(other.descricaoProduto)) {
            return false;
        }
        if ((this.situacaoProduto != other.situacaoProduto) && ((this.situacaoProduto == null) || (!this.situacaoProduto.equals(other.situacaoProduto)))) {
            return false;
        }
        if ((this.categoriaProd != other.categoriaProd) && ((this.categoriaProd == null) || (!this.categoriaProd.equals(other.categoriaProd)))) {
            return false;
        }
        if ((this.unidade != other.unidade) && ((this.unidade == null) || (!this.unidade.equals(other.unidade)))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    public Double getEstMinimo() {
        return this.estMinimo;
    }

    public void setEstMinimo(Double estMinimo) {
        this.estMinimo = estMinimo;
    }

    @Override
    public String toString() {
        return this.descricaoProduto;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Double getQtdEstoque() {
        return this.qtdEstoque;
    }

    public void setQtdEstoque(Double qtdEstoque) {
        this.qtdEstoque = qtdEstoque;
    }

    /**
     * @return the detalhesProduto
     */
    public String getDetalhesProduto() {
        return detalhesProduto;
    }

    /**
     * @param detalhesProduto the detalhesProduto to set
     */
    public void setDetalhesProduto(String detalhesProduto) {
        this.detalhesProduto = detalhesProduto;
    }
}