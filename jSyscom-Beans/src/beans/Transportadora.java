/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;


/**
 *
 * @author jean-marcos
 */
public class Transportadora extends Pessoa {

    public Transportadora(Integer codTransportadora) {
        setCdPessoa(codTransportadora);
    }

    public Transportadora() {
    }

    public void clearTransportadora() {
        clearPessoa();
    }

    @Override
    public String toString() {
        return getNomePessoa();
    }
    
}
