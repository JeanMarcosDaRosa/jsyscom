/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Date;

/**
 *
 * @author jean-marcos
 */
public final class Orcamento {
    private OrdemServico ordemServico;
    private Integer nroRevisao;
    private Boolean autorizado;
    private Date dataCadastro;
    private Date dataAutorizacao;
    private Date dataPrevisao;
    private String tipoFrete;
    private Double valorFrete;
    private Double valorBruto;
    private Double valorLiquido;
    private Double valorDesconto;
    private Boolean usoDescontoCliente;
    private Integer diasValidade;


    public Orcamento(OrdemServico ordemServico) {
        clearOrcamento();
        this.ordemServico = ordemServico;
    }
    
    public Orcamento(Integer nroRevisao, OrdemServico ordemServico) {
        clearOrcamento();
        this.nroRevisao = nroRevisao;
        this.ordemServico = ordemServico;
    }

    public Orcamento(OrdemServico ordemServico, Integer nroRevisao, Boolean autorizado, Date dataCadastro, Date dataAutorizacao, Date dataPrevisao, String tipoFrete, Double valorFrete, Double valorBruto, Double valorLiquido, Double valorDesconto, Boolean usoDescontoCliente, Integer diasValidade) {
        this.ordemServico = ordemServico;
        this.nroRevisao = nroRevisao;
        this.autorizado = autorizado;
        this.dataCadastro = dataCadastro;
        this.dataAutorizacao = dataAutorizacao;
        this.dataPrevisao = dataPrevisao;
        this.tipoFrete = tipoFrete != null ? tipoFrete.toUpperCase().trim() : null;
        this.valorFrete = valorFrete;
        this.valorBruto = valorBruto;
        this.valorLiquido = valorLiquido;
        this.valorDesconto = valorDesconto;
        this.usoDescontoCliente = usoDescontoCliente; 
        this.diasValidade = diasValidade;
    }

    public void clearOrcamento() {
        this.nroRevisao = null;
        this.autorizado = false;
        this.dataCadastro = null;
        this.dataAutorizacao = null;
        this.dataPrevisao = null;
        this.tipoFrete = "CIF";
        this.valorFrete = 0.00;
        this.valorBruto = 0.00;
        this.valorLiquido = 0.00;
        this.valorDesconto = 0.00;
        this.usoDescontoCliente = false;
        this.diasValidade = null;
    }

    /**
     * @return the ordemServico
     */
    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    /**
     * @param ordemServico the ordemServico to set
     */
    public void setOrdemServico(OrdemServico ordemServico) {
        this.ordemServico = ordemServico;
    }

    /**
     * @return the nroRevisao
     */
    public Integer getNroRevisao() {
        return nroRevisao;
    }

    /**
     * @param nroRevisao the nroRevisao to set
     */
    public void setNroRevisao(Integer nroRevisao) {
        this.nroRevisao = nroRevisao;
    }

    /**
     * @return the autorizado
     */
    public Boolean getAutorizado() {
        return autorizado;
    }

    /**
     * @param autorizado the autorizado to set
     */
    public void setAutorizado(Boolean autorizado) {
        this.autorizado = autorizado;
    }

    /**
     * @return the dataCadastro
     */
    public Date getDataCadastro() {
        return dataCadastro;
    }

    /**
     * @param dataCadastro the dataCadastro to set
     */
    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    /**
     * @return the dataAutorizacao
     */
    public Date getDataAutorizacao() {
        return dataAutorizacao;
    }

    /**
     * @param dataAutorizacao the dataAutorizacao to set
     */
    public void setDataAutorizacao(Date dataAutorizacao) {
        this.dataAutorizacao = dataAutorizacao;
    }

    /**
     * @return the dataPrevisao
     */
    public Date getDataPrevisao() {
        return dataPrevisao;
    }

    /**
     * @param dataPrevisao the dataPrevisao to set
     */
    public void setDataPrevisao(Date dataPrevisao) {
        this.dataPrevisao = dataPrevisao;
    }

    /**
     * @return the tipoFrete
     */
    public String getTipoFrete() {
        return tipoFrete;
    }

    /**
     * @param tipoFrete the tipoFrete to set
     */
    public void setTipoFrete(String tipoFrete) {
        this.tipoFrete = tipoFrete != null ? tipoFrete.toUpperCase().trim() : null;
    }

    /**
     * @return the valorFrete
     */
    public Double getValorFrete() {
        return valorFrete;
    }

    /**
     * @param valorFrete the valorFrete to set
     */
    public void setValorFrete(Double valorFrete) {
        this.valorFrete = valorFrete;
    }

    /**
     * @return the valorBruto
     */
    public Double getValorBruto() {
        return valorBruto;
    }

    /**
     * @param valorBruto the valorBruto to set
     */
    public void setValorBruto(Double valorBruto) {
        this.valorBruto = valorBruto;
    }

    /**
     * @return the valorLiquido
     */
    public Double getValorLiquido() {
        return valorLiquido;
    }

    /**
     * @param valorLiquido the valorLiquido to set
     */
    public void setValorLiquido(Double valorLiquido) {
        this.valorLiquido = valorLiquido;
    }

    /**
     * @return the valorDesconto
     */
    public Double getValorDesconto() {
        return valorDesconto;
    }

    /**
     * @param valorDesconto the valorDesconto to set
     */
    public void setValorDesconto(Double valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    /**
     * @return the usoDescontoCliente
     */
    public Boolean getUsoDescontoCliente() {
        return usoDescontoCliente;
    }

    /**
     * @param usoDescontoCliente the usoDescontoCliente to set
     */
    public void setUsoDescontoCliente(Boolean usoDescontoCliente) {
        this.usoDescontoCliente = usoDescontoCliente;
    }

    /**
     * @return the diasValidade
     */
    public Integer getDiasValidade() {
        return diasValidade;
    }

    /**
     * @param diasValidade the diasValidade to set
     */
    public void setDiasValidade(Integer diasValidade) {
        this.diasValidade = diasValidade;
    }
}
