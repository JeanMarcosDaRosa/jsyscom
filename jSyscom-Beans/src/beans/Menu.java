/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author Jean
 */
public class Menu {

    private Integer id;
    private String Nome;
    private Integer id_super;
    private Integer Operacao;

    public Menu() {
        this.id = null;
        this.Nome = "";
        this.id_super = null;
        this.Operacao = null;
    }

    public Menu(Integer id, String Nome, Integer id_super, Integer Operacao) {
        this.id = id;
        this.Nome = Nome;
        this.id_super = id_super;
        this.Operacao = Operacao;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the Nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    /**
     * @return the id_super
     */
    public Integer getId_super() {
        return id_super;
    }

    /**
     * @param id_super the id_super to set
     */
    public void setId_super(Integer id_super) {
        this.id_super = id_super;
    }

    @Override
    public String toString() {
        return this.Nome;
    }

    /**
     * @return the Operacao
     */
    public Integer getOperacao() {
        return Operacao;
    }

    /**
     * @param Operacao the Operacao to set
     */
    public void setOperacao(Integer Operacao) {
        this.Operacao = Operacao;
    }
}
