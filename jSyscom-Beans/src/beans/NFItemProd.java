/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Objects;

/**
 *
 * @author jean-marcos
 */
public class NFItemProd {
    private Produto produto;
    private Double quantidade;
    private Double valorUnitario;
    private Double valorTotal;
    private Double valorICMS;
    private Double aliquota;
    private NFEntrada nfEntrada;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.produto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NFItemProd other = (NFItemProd) obj;
        if (!Objects.equals(this.produto, other.produto)) {
            return false;
        }
        return true;
    }
    
//    private String ncm;
//    private String cst;
//    private Integer cfop;
//    private Double baseIcms;
//    private Double valorIcms;
//    private Double valorIpi;
//    private Double aliqIcms;
//    private Double aliqIpi;

    public NFItemProd() {
    }

    /**
     * @return the produto
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    
    /**
     * @return the quantidade
     */
    public Double getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * @return the valorUnitario
     */
    public Double getValorUnitario() {
        return valorUnitario;
    }

    /**
     * @param valorUnitario the valorUnitario to set
     */
    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    /**
     * @return the valorTotal
     */
    public Double getValorTotal() {
        return valorTotal;
    }

    /**
     * @param valorTotal the valorTotal to set
     */
    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    /**
     * @return the valorICMS
     */
    public Double getValorICMS() {
        return valorICMS;
    }

    /**
     * @param valorICMS the valorICMS to set
     */
    public void setValorICMS(Double valorICMS) {
        this.valorICMS = valorICMS;
    }

    /**
     * @return the aliquota
     */
    public Double getAliquota() {
        return aliquota;
    }

    /**
     * @param aliquota the aliquota to set
     */
    public void setAliquota(Double aliquota) {
        this.aliquota = aliquota;
    }

    /**
     * @return the nfEntrada
     */
    public NFEntrada getNfEntrada() {
        return nfEntrada;
    }

    /**
     * @param nfEntrada the nfEntrada to set
     */
    public void setNfEntrada(NFEntrada nfEntrada) {
        this.nfEntrada = nfEntrada;
    }

    
}
