/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author Jean
 */
public class Fornecedor extends Pessoa {

    public Fornecedor(Integer codFornecedor) {
        setCdPessoa(codFornecedor);
    }

    public Fornecedor() {
    }

    public void clearFornecedor() {
        clearPessoa();
    }

    @Override
    public String toString() {
        return getNomePessoa();
    }
}
