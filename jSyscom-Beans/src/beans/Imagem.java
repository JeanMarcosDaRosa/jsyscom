/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Objects;
import java.util.Observable;
import javax.swing.ImageIcon;

/**
 *
 * @author Jean
 */
public class Imagem extends Observable implements Cloneable{

    private Integer codImagem;
    private String nomeImagem;
    private String md5Imagem;
    private ImageIcon blobImagem;
    private Boolean flagPronto;

    public Imagem() {
    }

    public Imagem(Integer codImagem) {
        this.codImagem = codImagem;
    }

    public Imagem(ImageIcon blobImagem) {
        this.blobImagem = blobImagem;
    }

    public Imagem(Integer codImagem, String nomeImagem, String md5Imagem, ImageIcon blobImagem) {
        this.codImagem = codImagem;
        this.nomeImagem = nomeImagem;
        this.md5Imagem = md5Imagem;
        this.blobImagem = blobImagem;
    }

    public final void clearImagem() {
        this.codImagem = null;
        this.nomeImagem = "";
        this.md5Imagem = "";
        this.blobImagem = null;
    }

    /**
     * @return the codImagem
     */
    public Integer getCodImagem() {
        return codImagem;
    }

    /**
     * @param codImagem the codImagem to set
     */
    public void setCodImagem(Integer codImagem) {
        this.codImagem = codImagem;
    }

    /**
     * @return the nomeImagem
     */
    public String getNomeImagem() {
        return nomeImagem;
    }

    /**
     * @param nomeImagem the nomeImagem to set
     */
    public void setNomeImagem(String nomeImagem) {
        this.nomeImagem = nomeImagem;
    }

    /**
     * @return the md5Imagem
     */
    public String getMd5Imagem() {
        return md5Imagem;
    }

    /**
     * @param md5Imagem the md5Imagem to set
     */
    public void setMd5Imagem(String md5Imagem) {
        this.md5Imagem = md5Imagem;
    }

    /**
     * @return the blobImagem
     */
    public ImageIcon getBlobImagem() {
        return blobImagem;
    }

    /**
     * @param blobImagem the blobImagem to set
     */
    public void setBlobImagem(ImageIcon blobImagem) {
        this.blobImagem = blobImagem;
    }

    /**
     * @return the flagPronto
     */
    public Boolean getFlagPronto() {
        return flagPronto;
    }

    /**
     * @param flagPronto the flagPronto to set
     */
    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();// ? Registro de modificação do modelo
        notifyObservers(); // ? Notificação de modificação nos valores do modelo
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.codImagem);
        hash = 53 * hash + Objects.hashCode(this.md5Imagem);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Imagem other = (Imagem) obj;
        if (!Objects.equals(this.codImagem, other.codImagem)) {
            return false;
        }
        if (!Objects.equals(this.md5Imagem, other.md5Imagem)) {
            return false;
        }
        return true;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }
}
