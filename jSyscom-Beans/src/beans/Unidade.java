/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Observable;

/**
 *
 * @author Jean
 */
public class Unidade extends Observable {

    private String nome;
    private String sigla;
    private Boolean flagPronto;

    public Unidade(String nome, String sigla) {
        this.nome = nome != null ? nome.toUpperCase().trim() : null;
        this.sigla = sigla != null ? sigla.toUpperCase().trim() : null;
    }

    public Unidade() {
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome != null ? nome.toUpperCase().trim() : null;
    }

    /**
     * @return the sigla
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * @param sigla the sigla to set
     */
    public void setSigla(String sigla) {
        this.sigla = sigla != null ? sigla.toUpperCase().trim() : null;
    }

    @Override
    public String toString() {
        return sigla.trim();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Unidade other = (Unidade) obj;
        if ((this.sigla == null) ? (other.sigla != null) : !this.sigla.equals(other.sigla)) {
            return false;
        }
        return true;
    }

    /**
     * @return the flagPronto
     */
    public Boolean getFlagPronto() {
        return flagPronto;
    }

    /**
     * @param flagPronto the flagPronto to set
     */
    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();// ? Registro de modificação do modelo
        notifyObservers(); // ? Notificação de modificação nos valores do modeloF
    }
}
