/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Observable;

/**
 *
 * @author Jean
 */
public class CategoriaProd extends Observable {

    private Integer codCategoria;
    private String descCategoria;
    private Double margemCategoria;
    private Boolean flagPronto;

    public CategoriaProd() {
    }

    public CategoriaProd(Integer codCategoria) {
        this.codCategoria = codCategoria;
    }

    public CategoriaProd(Integer codCategoria, String descCategoria, Double margemCategoria) {
        this.codCategoria = codCategoria;
        this.descCategoria = descCategoria != null ? descCategoria.toUpperCase().trim() : null;
        this.margemCategoria = margemCategoria;
    }

    public CategoriaProd(String descCategoria, Double margemCategoria) {
        this.codCategoria = 0;
        this.descCategoria = descCategoria != null ? descCategoria.toUpperCase().trim() : null;
        this.margemCategoria = margemCategoria;
    }

    /**
     * @return the margemCategoria
     */
    public Double getMargemCategoria() {
        return margemCategoria;
    }

    /**
     * @param margemCategoria the margemCategoria to set
     */
    public void setMargemCategoria(Double margemCategoria) {
        this.margemCategoria = margemCategoria;
    }

    /**
     * @return the codCategoria
     */
    public Integer getCodCategoria() {
        return codCategoria;
    }

    /**
     * @param codCategoria the codCategoria to set
     */
    public void setCodCategoria(Integer codCategoria) {
        this.codCategoria = codCategoria;
    }

    /**
     * @return the descCategoria
     */
    public String getDescCategoria() {
        return descCategoria;
    }

    /**
     * @param descCategoria the descCategoria to set
     */
    public void setDescCategoria(String descCategoria) {
        this.descCategoria = descCategoria != null ? descCategoria.toUpperCase().trim() : null;
    }

    @Override
    public String toString() {
        return descCategoria;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CategoriaProd other = (CategoriaProd) obj;
        if ((this.descCategoria == null) ? (other.descCategoria != null) : !this.descCategoria.equals(other.descCategoria)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    /**
     * @return the flagPronto
     */
    public Boolean getFlagPronto() {
        return flagPronto;
    }

    /**
     * @param flagPronto the flagPronto to set
     */
    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();// ? Registro de modificação do modelo
        notifyObservers(); // ? Notificação de modificação nos valores do modelo
    }
}
