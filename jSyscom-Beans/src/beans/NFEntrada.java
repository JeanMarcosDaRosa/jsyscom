/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.Observable;

/**
 *
 * @author jean-marcos
 */
public final class NFEntrada extends Observable implements Cloneable {

    private static int fEmitente = 1;
    private static int fDestinatario = 2;
    private Integer numeroNf;
    private Integer serieNf;
    private String naturezaOperacao;
    private String chaveAcesso;
    private String protocoloAutorizacao;
    private Date dataEmissao;
    private Date dataEntrada;
    private Fornecedor fornecedor;
    private Endereco enderecoFornecedor;
    private ArrayList<NFFatura> faturas = new ArrayList<>();
    private ArrayList<NFItemProd> itemsProd = new ArrayList<>();
    private Transportadora transportadora;
    private Endereco enderecoTransp;
    private String tipoFrete;
    private Double baseIcms;
    private Double valorIcms;
    private Double baseIcmsSubs;
    private Double valorIcmsSubs;
    private Double valorTotProd;
    private Double valorFrete;
    private Double valorSeguroFrete;
    private Double desconto;
    private Double demaisDespesas;
    private Double valorIpi;
    private Double valorTotNota;
    private Double quantidadeCarga;
    private Double pesoBrutoCarga;
    private Double pesoLiqCarga;
    private Double issqnvlrTotServicos;
    private Double issqnBaseCalc;
    private Double issqnValor;
    private String contaAntt;
    private String placaVeiculo;
    private String ufPlacaVeiculo;
    private String especieCarga;
    private String marcaCarga;
    private String numeracaoCarga;
    private String dadosAdicionaisNf;
    private String issqnInscMun;
    private boolean flagPronto;

    public void clearNFEntrada() {
        this.numeroNf = null;
        this.serieNf = null;
        this.naturezaOperacao = "";
        this.chaveAcesso = "";
        this.protocoloAutorizacao = "";
        this.dataEmissao = null;
        this.dataEntrada = null;
        this.fornecedor = null;
        this.enderecoFornecedor = null;
        this.faturas.clear();
        this.itemsProd.clear();
        this.transportadora = null;
        this.enderecoTransp = null;
        this.tipoFrete = null;
        this.baseIcms = 0.00;
        this.valorIcms = 0.00;
        this.baseIcmsSubs = 0.00;
        this.valorIcmsSubs = 0.00;
        this.valorTotProd = 0.00;
        this.valorFrete = 0.00;
        this.valorSeguroFrete = 0.00;
        this.desconto = 0.00;
        this.demaisDespesas = 0.00;
        this.valorIpi = 0.00;
        this.valorTotNota = 0.00;
        this.quantidadeCarga = 0.00;
        this.pesoBrutoCarga = 0.00;
        this.pesoLiqCarga = 0.00;
        this.issqnvlrTotServicos = 0.00;
        this.issqnBaseCalc = 0.00;
        this.issqnValor = 0.00;
        this.contaAntt = "";
        this.placaVeiculo = "";
        this.ufPlacaVeiculo = "";
        this.especieCarga = "";
        this.marcaCarga = "";
        this.numeracaoCarga = "";
        this.dadosAdicionaisNf = "";
        this.issqnInscMun = "";
    }

    public NFEntrada() {
        this.faturas = new ArrayList();
        this.itemsProd = new ArrayList();
        clearNFEntrada();
    }

    public NFEntrada(Integer numero) {
        this.faturas = new ArrayList();
        this.itemsProd = new ArrayList();
        clearNFEntrada();
        this.numeroNf = numero;
    }

    /**
     * @return the flagPronto
     */
    public Boolean getFlagPronto() {
        return flagPronto;
    }

    /**
     * @param flagPronto the flagPronto to set
     */
    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();// ? Registro de modificação do modelo
        notifyObservers(); // ? Notificação de modificação nos valores do modelo
    }

    /**
     * @return the fEmitente
     */
    public static int getfEmitente() {
        return fEmitente;
    }

    /**
     * @param afEmitente the fEmitente to set
     */
    public static void setfEmitente(int afEmitente) {
        fEmitente = afEmitente;
    }

    /**
     * @return the fDestinatario
     */
    public static int getfDestinatario() {
        return fDestinatario;
    }

    /**
     * @param afDestinatario the fDestinatario to set
     */
    public static void setfDestinatario(int afDestinatario) {
        fDestinatario = afDestinatario;
    }

    /**
     * @return the numeroNf
     */
    public Integer getNumeroNf() {
        return numeroNf;
    }

    /**
     * @param numeroNf the numeroNf to set
     */
    public void setNumeroNf(Integer numeroNf) {
        this.numeroNf = numeroNf;
    }

    /**
     * @return the serieNf
     */
    public Integer getSerieNf() {
        return serieNf;
    }

    /**
     * @param serieNf the serieNf to set
     */
    public void setSerieNf(Integer serieNf) {
        this.serieNf = serieNf;
    }

    /**
     * @return the chaveAcesso
     */
    public String getChaveAcesso() {
        return chaveAcesso;
    }

    /**
     * @param chaveAcesso the chaveAcesso to set
     */
    public void setChaveAcesso(String chaveAcesso) {
        this.chaveAcesso = chaveAcesso;
    }

    /**
     * @return the protocoloAutorizacao
     */
    public String getProtocoloAutorizacao() {
        return protocoloAutorizacao;
    }

    /**
     * @param protocoloAutorizacao the protocoloAutorizacao to set
     */
    public void setProtocoloAutorizacao(String protocoloAutorizacao) {
        this.protocoloAutorizacao = protocoloAutorizacao;
    }

    /**
     * @return the dataEmissao
     */
    public Date getDataEmissao() {
        return dataEmissao;
    }

    /**
     * @param dataEmissao the dataEmissao to set
     */
    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    /**
     * @return the dataEntrada
     */
    public Date getDataEntrada() {
        return dataEntrada;
    }

    /**
     * @param dataEntrada the dataEntrada to set
     */
    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    /**
     * @return the fornecedor
     */
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    /**
     * @param fornecedor the fornecedor to set
     */
    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    /**
     * @return the faturas
     */
    public ArrayList<NFFatura> getFaturas() {
        return faturas;
    }

    /**
     * @param faturas the faturas to set
     */
    public void setFaturas(ArrayList<NFFatura> faturas) {
        this.faturas = faturas;
    }

    /**
     * @return the transportadora
     */
    public Transportadora getTransportadora() {
        return transportadora;
    }

    /**
     * @param transportadora the transportadora to set
     */
    public void setTransportadora(Transportadora transportadora) {
        this.transportadora = transportadora;
    }

    /**
     * @return the enderecoTransp
     */
    public Endereco getEnderecoTransp() {
        return enderecoTransp;
    }

    /**
     * @param enderecoTransp the enderecoTransp to set
     */
    public void setEnderecoTransp(Endereco enderecoTransp) {
        this.enderecoTransp = enderecoTransp;
    }

    /**
     * @return the baseIcms
     */
    public Double getBaseIcms() {
        return baseIcms;
    }

    /**
     * @param baseIcms the baseIcms to set
     */
    public void setBaseIcms(Double baseIcms) {
        this.baseIcms = baseIcms;
    }

    /**
     * @return the valorIcms
     */
    public Double getValorIcms() {
        return valorIcms;
    }

    /**
     * @param valorIcms the valorIcms to set
     */
    public void setValorIcms(Double valorIcms) {
        this.valorIcms = valorIcms;
    }

    /**
     * @return the baseIcmsSubs
     */
    public Double getBaseIcmsSubs() {
        return baseIcmsSubs;
    }

    /**
     * @param baseIcmsSubs the baseIcmsSubs to set
     */
    public void setBaseIcmsSubs(Double baseIcmsSubs) {
        this.baseIcmsSubs = baseIcmsSubs;
    }

    /**
     * @return the valorIcmsSubs
     */
    public Double getValorIcmsSubs() {
        return valorIcmsSubs;
    }

    /**
     * @param valorIcmsSubs the valorIcmsSubs to set
     */
    public void setValorIcmsSubs(Double valorIcmsSubs) {
        this.valorIcmsSubs = valorIcmsSubs;
    }

    /**
     * @return the valorTotProd
     */
    public Double getValorTotProd() {
        return valorTotProd;
    }

    /**
     * @param valorTotProd the valorTotProd to set
     */
    public void setValorTotProd(Double valorTotProd) {
        this.valorTotProd = valorTotProd;
    }

    /**
     * @return the valorFrete
     */
    public Double getValorFrete() {
        return valorFrete;
    }

    /**
     * @param valorFrete the valorFrete to set
     */
    public void setValorFrete(Double valorFrete) {
        this.valorFrete = valorFrete;
    }

    /**
     * @return the valorSeguroFrete
     */
    public Double getValorSeguroFrete() {
        return valorSeguroFrete;
    }

    /**
     * @param valorSeguroFrete the valorSeguroFrete to set
     */
    public void setValorSeguroFrete(Double valorSeguroFrete) {
        this.valorSeguroFrete = valorSeguroFrete;
    }

    /**
     * @return the desconto
     */
    public Double getDesconto() {
        return desconto;
    }

    /**
     * @param desconto the desconto to set
     */
    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    /**
     * @return the demaisDespesas
     */
    public Double getDemaisDespesas() {
        return demaisDespesas;
    }

    /**
     * @param demaisDespesas the demaisDespesas to set
     */
    public void setDemaisDespesas(Double demaisDespesas) {
        this.demaisDespesas = demaisDespesas;
    }

    /**
     * @return the valorIpi
     */
    public Double getValorIpi() {
        return valorIpi;
    }

    /**
     * @param valorIpi the valorIpi to set
     */
    public void setValorIpi(Double valorIpi) {
        this.valorIpi = valorIpi;
    }

    /**
     * @return the valorTotNota
     */
    public Double getValorTotNota() {
        return valorTotNota;
    }

    /**
     * @param valorTotNota the valorTotNota to set
     */
    public void setValorTotNota(Double valorTotNota) {
        this.valorTotNota = valorTotNota;
    }

    /**
     * @return the tipoFrete
     */
    public String getTipoFrete() {
        switch (tipoFrete.toUpperCase().trim()) {
            case "F":
                return "FOB";
            case "C":
                return "CIF";
            default:
                return this.tipoFrete;
        }
    }

    /**
     * @param tipoFrete the tipoFrete to set
     */
    public void setTipoFrete(String tipoFrete) {
        switch (tipoFrete.toUpperCase().trim()) {
            case "FOB":
                this.tipoFrete = "F";
                break;
            case "CIF":
                this.tipoFrete = "C";
                break;
            default:
                this.tipoFrete = tipoFrete != null ? tipoFrete.toUpperCase().trim() : null;
                break;
        }
    }

    /**
     * @return the contaAntt
     */
    public String getContaAntt() {
        return contaAntt;
    }

    /**
     * @param contaAntt the contaAntt to set
     */
    public void setContaAntt(String contaAntt) {
        this.contaAntt = contaAntt;
    }

    /**
     * @return the placaVeiculo
     */
    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    /**
     * @param placaVeiculo the placaVeiculo to set
     */
    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    /**
     * @return the quantidadeCarga
     */
    public Double getQuantidadeCarga() {
        return quantidadeCarga;
    }

    /**
     * @param quantidadeCarga the quantidadeCarga to set
     */
    public void setQuantidadeCarga(Double quantidadeCarga) {
        this.quantidadeCarga = quantidadeCarga;
    }

    /**
     * @return the especieCarga
     */
    public String getEspecieCarga() {
        return especieCarga;
    }

    /**
     * @param especieCarga the especieCarga to set
     */
    public void setEspecieCarga(String especieCarga) {
        this.especieCarga = especieCarga;
    }

    /**
     * @return the marcaCarga
     */
    public String getMarcaCarga() {
        return marcaCarga;
    }

    /**
     * @param marcaCarga the marcaCarga to set
     */
    public void setMarcaCarga(String marcaCarga) {
        this.marcaCarga = marcaCarga;
    }

    /**
     * @return the numeracaoCarga
     */
    public String getNumeracaoCarga() {
        return numeracaoCarga;
    }

    /**
     * @param numeracaoCarga the numeracaoCarga to set
     */
    public void setNumeracaoCarga(String numeracaoCarga) {
        this.numeracaoCarga = numeracaoCarga;
    }

    /**
     * @return the pesoBrutoCarga
     */
    public Double getPesoBrutoCarga() {
        return pesoBrutoCarga;
    }

    /**
     * @param pesoBrutoCarga the pesoBrutoCarga to set
     */
    public void setPesoBrutoCarga(Double pesoBrutoCarga) {
        this.pesoBrutoCarga = pesoBrutoCarga;
    }

    /**
     * @return the pesoLiqCarga
     */
    public Double getPesoLiqCarga() {
        return pesoLiqCarga;
    }

    /**
     * @param pesoLiqCarga the pesoLiqCarga to set
     */
    public void setPesoLiqCarga(Double pesoLiqCarga) {
        this.pesoLiqCarga = pesoLiqCarga;
    }

    /**
     * @return the issqnInscMun
     */
    public String getIssqnInscMun() {
        return issqnInscMun;
    }

    /**
     * @param issqnInscMun the issqnInscMun to set
     */
    public void setIssqnInscMun(String issqnInscMun) {
        this.issqnInscMun = issqnInscMun;
    }

    /**
     * @return the issqnvlrTotServicos
     */
    public Double getIssqnvlrTotServicos() {
        return issqnvlrTotServicos;
    }

    /**
     * @param issqnvlrTotServicos the issqnvlrTotServicos to set
     */
    public void setIssqnvlrTotServicos(Double issqnvlrTotServicos) {
        this.issqnvlrTotServicos = issqnvlrTotServicos;
    }

    /**
     * @return the issqnBaseCalc
     */
    public Double getIssqnBaseCalc() {
        return issqnBaseCalc;
    }

    /**
     * @param issqnBaseCalc the issqnBaseCalc to set
     */
    public void setIssqnBaseCalc(Double issqnBaseCalc) {
        this.issqnBaseCalc = issqnBaseCalc;
    }

    /**
     * @return the issqnValor
     */
    public Double getIssqnValor() {
        return issqnValor;
    }

    /**
     * @param issqnValor the issqnValor to set
     */
    public void setIssqnValor(Double issqnValor) {
        this.issqnValor = issqnValor;
    }

    /**
     * @return the dadosAdicionaisNf
     */
    public String getDadosAdicionaisNf() {
        return dadosAdicionaisNf;
    }

    /**
     * @param dadosAdicionaisNf the dadosAdicionaisNf to set
     */
    public void setDadosAdicionaisNf(String dadosAdicionaisNf) {
        this.dadosAdicionaisNf = dadosAdicionaisNf;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.numeroNf);
        hash = 23 * hash + Objects.hashCode(this.serieNf);
        hash = 23 * hash + Objects.hashCode(this.fornecedor);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NFEntrada other = (NFEntrada) obj;
        if (!Objects.equals(this.numeroNf, other.numeroNf)) {
            return false;
        }
        if (!Objects.equals(this.serieNf, other.serieNf)) {
            return false;
        }
        if (!Objects.equals(this.fornecedor, other.fornecedor)) {
            return false;
        }
        return true;
    }

    /**
     * @return the itemsProd
     */
    public ArrayList<NFItemProd> getItemsProd() {
        return itemsProd;
    }

    /**
     * @param itemsProd the itemsProd to set
     */
    public void setItemsProd(ArrayList<NFItemProd> itemsProd) {
        this.itemsProd = itemsProd;
    }

    /**
     * @return the naturezaOperacao
     */
    public String getNaturezaOperacao() {
        return naturezaOperacao;
    }

    /**
     * @param naturezaOperacao the naturezaOperacao to set
     */
    public void setNaturezaOperacao(String naturezaOperacao) {
        this.naturezaOperacao = naturezaOperacao;
    }

    /**
     * @return the ufPlacaVeiculo
     */
    public String getUfPlacaVeiculo() {
        return ufPlacaVeiculo;
    }

    /**
     * @param ufPlacaVeiculo the ufPlacaVeiculo to set
     */
    public void setUfPlacaVeiculo(String ufPlacaVeiculo) {
        this.ufPlacaVeiculo = ufPlacaVeiculo != null ? ufPlacaVeiculo.toUpperCase().trim() : null;
    }

    /**
     * @return the enderecoFrornecedor
     */
    public Endereco getEnderecoFornecedor() {
        return enderecoFornecedor;
    }

    /**
     * @param enderecoFrornecedor the enderecoFrornecedor to set
     */
    public void setEnderecoFornecedor(Endereco enderecoFornecedor) {
        this.enderecoFornecedor = enderecoFornecedor;
    }
}
