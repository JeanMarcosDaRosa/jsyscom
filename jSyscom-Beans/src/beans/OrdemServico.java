/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

/**
 *
 * @author jean-marcos
 */
public class OrdemServico extends Observable
{
  private Integer codOS;
  private Integer nroOcCliente;
  private String defeitoReclamado;
  private String estadoEquipamento;
  private String consertoDetalhado;
  private String parecer;
  private Date dataCadastro;
  private Date dataEntrada;
  private Date dataSaida;
  private String statusOS;
  private Usuario usuario;
  private Cliente cliente = new Cliente();
  private Contato contatoCliente;
  private Endereco enderecoCliente;
  private Equipamento equipamento;
  private LocalProd localizacao;
  private Integer mesesGarantia;
  private Boolean sobGarantia;
  private Orcamento ultimoOrcamento;
  private Transportadora transportadora;
  private Endereco enderecoTransp;
  private ArrayList<EquipamentoTeste> testesEquipamentos = new ArrayList();
  private ArrayList<ItemProdOS> itensProdutos = new ArrayList();
  private ArrayList<ItemServicoOS> itensServicos = new ArrayList();
  private ArrayList<Orcamento> orcamentos = new ArrayList();
  private ArrayList<Pessoa> envolvidos = new ArrayList();
  private ArrayList<OrdemServico> ordems = new ArrayList();
  private ArrayList<ItemPropTipoEquip> itemsPropriedades = new ArrayList();
  private Boolean flagPronto;

  public OrdemServico()
  {
    clearOrdemServico();
  }

  public OrdemServico(Integer codOS) {
    clearOrdemServico();
    this.codOS = codOS;
  }

  public OrdemServico(Integer codOS, Integer nroOcCliente, String defeitoReclamado, String estadoEquipamento, String consertoDetalhado, String parecer, Date dataCadastro, Date dataEntrada, Date dataSaida, String statusOS, Usuario usuario, Cliente cliente, Contato contatoCliente, Endereco enderecoCliente, Equipamento equipamento, LocalProd localizacao, Transportadora transportadora,Endereco enderecoTransp) {
    clearOrdemServico();
    this.codOS = codOS;
    this.nroOcCliente = nroOcCliente;
    this.defeitoReclamado = defeitoReclamado;
    this.estadoEquipamento = estadoEquipamento;
    this.consertoDetalhado = consertoDetalhado;
    this.parecer = parecer;
    this.dataCadastro = dataCadastro;
    this.dataEntrada = dataEntrada;
    this.dataSaida = dataSaida;
    this.statusOS = (statusOS != null ? statusOS.toUpperCase().trim() : null);
    this.usuario = usuario;
    this.cliente = cliente;
    this.contatoCliente = contatoCliente;
    this.enderecoCliente = enderecoCliente;
    this.equipamento = equipamento;
    this.localizacao = localizacao;
    this.transportadora = transportadora;
    this.enderecoTransp = enderecoTransp;
  }

  public Integer getCodOS()
  {
    return this.codOS;
  }

  public void setCodOS(Integer codOS)
  {
    this.codOS = codOS;
  }

  public Integer getNroOcCliente()
  {
    return this.nroOcCliente;
  }

  public void setNroOcCliente(Integer nroOcCliente)
  {
    this.nroOcCliente = nroOcCliente;
  }

  public String getDefeitoReclamado()
  {
    return this.defeitoReclamado;
  }

  public void setDefeitoReclamado(String defeitoReclamado)
  {
    this.defeitoReclamado = defeitoReclamado;
  }

  public String getEstadoEquipamento()
  {
    return this.estadoEquipamento;
  }

  public void setEstadoEquipamento(String estadoEquipamento)
  {
    this.estadoEquipamento = estadoEquipamento;
  }

  public String getConsertoDetalhado()
  {
    return this.consertoDetalhado;
  }

  public void setConsertoDetalhado(String consertoDetalhado)
  {
    this.consertoDetalhado = consertoDetalhado;
  }

  public String getParecer()
  {
    return this.parecer;
  }

  public void setParecer(String parecer)
  {
    this.parecer = parecer;
  }

  public Date getDataCadastro()
  {
    return this.dataCadastro;
  }

  public void setDataCadastro(Date dataCadastro)
  {
    this.dataCadastro = dataCadastro;
  }

  public Date getDataEntrada()
  {
    return this.dataEntrada;
  }

  public void setDataEntrada(Date dataEntrada)
  {
    this.dataEntrada = dataEntrada;
  }

  public Date getDataSaida()
  {
    return this.dataSaida;
  }

  public void setDataSaida(Date dataSaida)
  {
    this.dataSaida = dataSaida;
  }

  public String getStatusOS()
  {
    return this.statusOS;
  }

  public void setStatusOS(String statusOS)
  {
    this.statusOS = (statusOS != null ? statusOS.toUpperCase().trim() : null);
  }

  public Usuario getUsuario()
  {
    return this.usuario;
  }

  public void setUsuario(Usuario usuario)
  {
    this.usuario = usuario;
  }

  public Cliente getCliente()
  {
    return this.cliente;
  }

  public void setCliente(Cliente cliente)
  {
    this.cliente = cliente;
  }

  public Contato getContatoCliente()
  {
    return this.contatoCliente;
  }

  public void setContatoCliente(Contato contatoCliente)
  {
    this.contatoCliente = contatoCliente;
  }

  public Endereco getEnderecoCliente()
  {
    return this.enderecoCliente;
  }

  public void setEnderecoCliente(Endereco enderecoCliente)
  {
    this.enderecoCliente = enderecoCliente;
  }

  public Equipamento getEquipamento()
  {
    return this.equipamento;
  }

  public void setEquipamento(Equipamento equipamento)
  {
    this.equipamento = equipamento;
  }

  public LocalProd getLocalizacao()
  {
    return this.localizacao;
  }

  public void setLocalizacao(LocalProd localizacao)
  {
    this.localizacao = localizacao;
  }

  public ArrayList<EquipamentoTeste> getTestesEquipamentos()
  {
    return this.testesEquipamentos;
  }

  public void setTestesEquipamentos(ArrayList<EquipamentoTeste> testesEquipamentos)
  {
    this.testesEquipamentos = testesEquipamentos;
  }

  public ArrayList<ItemProdOS> getItensProdutos()
  {
    return this.itensProdutos;
  }

  public void setItensProdutos(ArrayList<ItemProdOS> itensProdutos)
  {
    this.itensProdutos = itensProdutos;
  }

  public ArrayList<ItemServicoOS> getItensServicos()
  {
    return this.itensServicos;
  }

  public void setItensServicos(ArrayList<ItemServicoOS> itensServicos)
  {
    this.itensServicos = itensServicos;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public final void clearOrdemServico() {
    getOrdems().clear();
    getOrdems().add(this);
    this.codOS = null;
    this.nroOcCliente = null;
    this.defeitoReclamado = "";
    this.estadoEquipamento = "";
    this.consertoDetalhado = "";
    this.parecer = "";
    this.dataCadastro = null;
    this.dataEntrada = null;
    this.dataSaida = null;
    this.statusOS = "";
    this.usuario = null;
    this.equipamento = null;
    this.localizacao = null;
    this.contatoCliente = null;
    this.enderecoCliente = null;
    this.ultimoOrcamento = null;
    this.transportadora = null;
    this.enderecoTransp = null;
    this.cliente = null;
    this.orcamentos.clear();
    this.itensServicos.clear();
    this.itensProdutos.clear();
    this.testesEquipamentos.clear();
    this.itemsPropriedades.clear();
  }

  public ArrayList<Orcamento> getOrcamentos()
  {
    return this.orcamentos;
  }

  public void setOrcamentos(ArrayList<Orcamento> orcamentos)
  {
    this.orcamentos = orcamentos;
  }

  public ArrayList<Pessoa> getEnvolvidos()
  {
    return this.envolvidos;
  }

  public Integer getMesesGarantia()
  {
    return this.mesesGarantia;
  }

  public void setMesesGarantia(Integer mesesGarantia)
  {
    this.mesesGarantia = mesesGarantia;
  }

  public Boolean getSobGarantia()
  {
    return this.sobGarantia;
  }

  public void setSobGarantia(Boolean sobGarantia)
  {
    this.sobGarantia = sobGarantia;
  }

  public Orcamento getUltimoOrcamento()
  {
    return this.ultimoOrcamento;
  }

  public void setUltimoOrcamento(Orcamento ultimoOrcamento)
  {
    this.ultimoOrcamento = ultimoOrcamento;
  }

  public Transportadora getTransportadora()
  {
    return this.transportadora;
  }

  public void setTransportadora(Transportadora transportadora)
  {
    this.transportadora = transportadora;
  }

  public ArrayList<OrdemServico> getOrdems()
  {
    return this.ordems;
  }

  public void setOrdems(ArrayList<OrdemServico> ordems)
  {
    this.ordems = ordems;
  }

  public ArrayList<ItemPropTipoEquip> getItemsPropriedades()
  {
    return this.itemsPropriedades;
  }

  public void setItemsPropriedades(ArrayList<ItemPropTipoEquip> itemsPropriedades)
  {
    this.itemsPropriedades = itemsPropriedades;
  }

    /**
     * @return the enderecoTransp
     */
    public Endereco getEnderecoTransp() {
        return enderecoTransp;
    }

    /**
     * @param enderecoTransp the enderecoTransp to set
     */
    public void setEnderecoTransp(Endereco enderecoTransp) {
        this.enderecoTransp = enderecoTransp;
    }
}