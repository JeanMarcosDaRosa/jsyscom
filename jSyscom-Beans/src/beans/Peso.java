/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Date;

/**
 *
 * @author Jean
 */
public class Peso{


    private Double valorPeso;
    private Date dataPeso;

    public Peso() {
    }

    /**
     * @return the valorPeso
     */
    public Double getValorPeso() {
        return valorPeso;
    }

    /**
     * @param valorPeso the valorPeso to set
     */
    public void setValorPeso(Double valorPeso) {
        this.valorPeso = valorPeso;
    }

    /**
     * @return the dataPeso
     */
    public Date getDataPeso() {
        return dataPeso;
    }

    /**
     * @param dataPeso the dataPeso to set
     */
    public void setDataPeso(Date dataPeso) {
        this.dataPeso = dataPeso;
    }

    
}
