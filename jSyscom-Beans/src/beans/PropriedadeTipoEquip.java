package beans;

import java.util.Objects;

public class PropriedadeTipoEquip
{
  private Integer codPropTipoEquip;
  private String descricao;
  private TipoEquipamento tipoEquip;

  public PropriedadeTipoEquip(Integer codPropTipoEquip, String descricao, TipoEquipamento tipoEquip)
  {
    this.codPropTipoEquip = codPropTipoEquip;
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
    this.tipoEquip = tipoEquip;
  }

  public String toString()
  {
    return this.descricao;
  }

  public PropriedadeTipoEquip(String descricao, TipoEquipamento tipoEquip) {
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
    this.tipoEquip = tipoEquip;
  }

  public int hashCode()
  {
    int hash = 7;
    return hash;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    PropriedadeTipoEquip other = (PropriedadeTipoEquip)obj;
    if (!Objects.equals(this.descricao, other.descricao)) {
      return false;
    }
    return true;
  }

  public Integer getCodPropTipoEquip()
  {
    return this.codPropTipoEquip;
  }

  public void setCodPropTipoEquip(Integer codPropTipoEquip)
  {
    this.codPropTipoEquip = codPropTipoEquip;
  }

  public String getDescricao()
  {
    return this.descricao;
  }

  public void setDescricao(String descricao)
  {
    this.descricao = (descricao != null ? descricao.toUpperCase().trim() : null);
  }

  public TipoEquipamento getTipoEquip()
  {
    return this.tipoEquip;
  }

  public void setTipoEquip(TipoEquipamento tipoEquip)
  {
    this.tipoEquip = tipoEquip;
  }
}