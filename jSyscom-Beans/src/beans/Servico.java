/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Date;
import java.util.Observable;

/**
 *
 * @author Jean
 */
public class Servico extends Observable implements Cloneable {

    private Integer codServico;
    private String descServico;
    private Double valorServico;
    private Date dataCadastro;
    private Boolean flagPronto;

    public Servico() {
    }

    public Servico(Integer codServico) {
        this.codServico = codServico;
    }

    public Servico(String descServico, Double valorServico) {
        this.descServico = descServico != null ? descServico.toUpperCase().trim() : null;
        this.valorServico = valorServico;
    }

    public Servico(Integer codServico, String descServico, Double valorServico) {
        this.codServico = codServico;
        this.descServico = descServico != null ? descServico.toUpperCase().trim() : null;
        this.valorServico = valorServico;
        this.dataCadastro = null;
    }

    public void clearServico() {
        this.codServico = null;
        this.descServico = "";
        this.valorServico = 0.00;
        this.dataCadastro = null;
    }

    @Override
    public String toString() {
        return descServico;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Servico other = (Servico) obj;
        if ((this.descServico == null) ? (other.descServico != null) : !this.descServico.equals(other.descServico)) {
            return false;
        }
        return true;
    }

    /**
     * @return the flagPronto
     */
    public Boolean getFlagPronto() {
        return flagPronto;
    }

    /**
     * @param flagPronto the flagPronto to set
     */
    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();// ? Registro de modificação do modelo
        notifyObservers(); // ? Notificação de modificação nos valores do modelo
    }

    /**
     * @return the codServico
     */
    public Integer getCodServico() {
        return codServico;
    }

    /**
     * @param codServico the codServico to set
     */
    public void setCodServico(Integer codServico) {
        this.codServico = codServico;
    }

    /**
     * @return the descServico
     */
    public String getDescServico() {
        return descServico;
    }

    /**
     * @param descServico the descServico to set
     */
    public void setDescServico(String descServico) {
        this.descServico = descServico != null ? descServico.toUpperCase().trim() : null;
    }

    /**
     * @return the valorServico
     */
    public Double getValorServico() {
        return valorServico;
    }

    /**
     * @param valorServico the valorServico to set
     */
    public void setValorServico(Double valorServico) {
        this.valorServico = valorServico;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * @return the dataCadastro
     */
    public Date getDataCadastro() {
        return dataCadastro;
    }

    /**
     * @param dataCadastro the dataCadastro to set
     */
    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
