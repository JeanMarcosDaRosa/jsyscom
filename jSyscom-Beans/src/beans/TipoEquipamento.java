/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Observable;

/**
 *
 * @author jean-marcos
 */
public class TipoEquipamento extends Observable {

    private Integer codTipo;
    private String descricaoEquipamento;
    private Boolean flagPronto;
    private ArrayList<PropriedadeTipoEquip> propriedades = new ArrayList();

    @Override
    public String toString() {
        return this.descricaoEquipamento;
    }

    public TipoEquipamento(String descricaoEquipamento) {
        this.codTipo = null;
        this.descricaoEquipamento = descricaoEquipamento;
    }

    public TipoEquipamento() {
    }

    public TipoEquipamento(Integer codTipo, String descricaoEquipamento) {
        this.codTipo = codTipo;
        this.descricaoEquipamento = (descricaoEquipamento != null ? descricaoEquipamento.toUpperCase().trim() : null);
    }

    public TipoEquipamento(Integer codTipo) {
        this.codTipo = codTipo;
    }

    public ArrayList<PropriedadeTipoEquip> getPropriedades() {
        return this.propriedades;
    }

    public void setPropriedades(ArrayList<PropriedadeTipoEquip> propriedades) {
        this.propriedades = propriedades;
    }

    public Integer getCodTipo() {
        return this.codTipo;
    }

    public void setCodTipo(Integer codTipo) {
        this.codTipo = codTipo;
    }

    public String getDescricaoEquipamento() {
        return this.descricaoEquipamento;
    }

    public void setDescricaoEquipamento(String descricaoEquipamento) {
        this.descricaoEquipamento = (descricaoEquipamento != null ? descricaoEquipamento.toUpperCase().trim() : null);
    }

    public Boolean getFlagPronto() {
        return this.flagPronto;
    }

    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();
        notifyObservers();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TipoEquipamento other = (TipoEquipamento) obj;
        if (!Objects.equals(this.codTipo, other.codTipo)) {
            return false;
        }
        if (!Objects.equals(this.descricaoEquipamento, other.descricaoEquipamento)) {
            return false;
        }
        return true;
    }
}