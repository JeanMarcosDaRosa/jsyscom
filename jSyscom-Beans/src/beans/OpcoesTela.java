/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;

/**
 *
 * @author Jean
 */
public class OpcoesTela implements Serializable {

    private Boolean iconifiable;
    private Boolean maximizable;
    private Boolean resizable;
    private Boolean closable;
    private Boolean visible;
    private Boolean show_version;

    public OpcoesTela() {
        this.iconifiable = true;
        this.maximizable = true;
        this.resizable = true;
        this.closable = true;
        this.visible = true;
        this.show_version = true;
    }

    public OpcoesTela(Boolean iconifiable, Boolean maximizable, Boolean resizable, Boolean closable, Boolean visible, Boolean show_version) {
        this.iconifiable = iconifiable;
        this.maximizable = maximizable;
        this.resizable = resizable;
        this.closable = closable;
        this.visible = visible;
        this.show_version = show_version;
    }
    
    /**
     * @return the iconifiable
     */
    public Boolean getIconifiable() {
        return iconifiable;
    }

    /**
     * @param iconifiable the iconifiable to set
     */
    public void setIconifiable(Boolean iconifiable) {
        this.iconifiable = iconifiable;
    }

    /**
     * @return the maximizable
     */
    public Boolean getMaximizable() {
        return maximizable;
    }

    /**
     * @param maximizable the maximizable to set
     */
    public void setMaximizable(Boolean maximizable) {
        this.maximizable = maximizable;
    }

    /**
     * @return the resizable
     */
    public Boolean getResizable() {
        return resizable;
    }

    /**
     * @param resizable the resizable to set
     */
    public void setResizable(Boolean resizable) {
        this.resizable = resizable;
    }

    /**
     * @return the closable
     */
    public Boolean getClosable() {
        return closable;
    }

    /**
     * @param closable the closable to set
     */
    public void setClosable(Boolean closable) {
        this.closable = closable;
    }

    /**
     * @return the visible
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getAllOpcoesTela() {
        return "visible = " + this.visible.toString() + "\n"
                + "closable = " + this.closable.toString() + "\n"
                + "iconifiable = " + this.iconifiable.toString() + "\n"
                + "maximisable = " + this.maximizable.toString() + "\n"
                + "resisable = " + this.resizable.toString() + "\n"
                + "show_version = " + this.show_version.toString();
    }

    /**
     * @return the show_version
     */
    public Boolean getShow_version() {
        return show_version;
    }

    /**
     * @param show_version the show_version to set
     */
    public void setShow_version(Boolean show_version) {
        this.show_version = show_version;
    }
}
