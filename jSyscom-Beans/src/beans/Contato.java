/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Objects;

/**
 *
 * @author Jean
 */
public class Contato {
    private Integer codContato;
    private String nomeContato;
    private String cargoContato;
    private String telefoneContato;
    private String observacao;
    private Pessoa pessoaContato;
    public Contato(){
    }

    public Contato(Integer codContato) {
        this.codContato = codContato;
    }

    public Contato(Integer codContato, String nomeContato, String cargoContato, String telefoneContato, String observacao, Pessoa pessoaContato) {
        this.codContato = codContato;
        this.nomeContato = nomeContato!=null?nomeContato.toUpperCase().trim():null;
        this.cargoContato = cargoContato!=null?cargoContato.toUpperCase().trim():null;;
        this.telefoneContato = telefoneContato;
        this.observacao = observacao;
        this.pessoaContato = pessoaContato;
    }

    /**
     * @return the codContato
     */
    public Integer getCodContato() {
        return codContato;
    }

    /**
     * @param codContato the codContato to set
     */
    public void setCodContato(Integer codContato) {
        this.codContato = codContato;
    }

    /**
     * @return the nomeContato
     */
    public String getNomeContato() {
        return nomeContato;
    }

    /**
     * @param nomeContato the nomeContato to set
     */
    public void setNomeContato(String nomeContato) {
        this.nomeContato = nomeContato!=null?nomeContato.toUpperCase().trim():null;
    }

    /**
     * @return the cargoContato
     */
    public String getCargoContato() {
        return cargoContato;
    }

    /**
     * @param cargoContato the cargoContato to set
     */
    public void setCargoContato(String cargoContato) {
        this.cargoContato = cargoContato!=null?cargoContato.toUpperCase().trim():null;
    }

    /**
     * @return the telefoneContato
     */
    public String getTelefoneContato() {
        return telefoneContato;
    }

    /**
     * @param telefoneContato the telefoneContato to set
     */
    public void setTelefoneContato(String telefoneContato) {
        this.telefoneContato = telefoneContato;
    }

    /**
     * @return the pessoaContato
     */
    public Pessoa getPessoaContato() {
        return pessoaContato;
    }

    /**
     * @param pessoaContato the pessoaContato to set
     */
    public void setPessoaContato(Pessoa pessoaContato) {
        this.pessoaContato = pessoaContato;
    }

    /**
     * @return the observacao
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * @param observacao the observacao to set
     */
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public String toString() {
        return nomeContato + ", " + telefoneContato;
    }
    
    public final void clearContato(){
        this.codContato = null;
        this.nomeContato = "";
        this.cargoContato = "";
        this.telefoneContato = null;
        this.observacao = "";
        if(this.pessoaContato!=null) {
            this.pessoaContato.clearPessoa();
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contato other = (Contato) obj;
        if (!Objects.equals(this.codContato, other.codContato)) {
            return false;
        }
        if (!Objects.equals(this.nomeContato, other.nomeContato)) {
            return false;
        }
        return true;
    }
    
}
