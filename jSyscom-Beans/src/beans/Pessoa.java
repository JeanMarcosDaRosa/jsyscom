/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

/**
 *
 * @author Jean
 */
public class Pessoa extends Observable {

    private Integer cdPessoa;
    private char tipoPessoa;
    private String nomePessoa;
    private Date dataNascimento;
    private Date dataCadPessoa;
    private String descricaoPessoa;
    private String emailPessoa;
    private String sitePessoa;
    private String telefoneFixoPessoa;
    private String telefoneCellPessoa;
    private String cpfCnpjPessoa;
    private String rgIePessoa;
    private String orgaoRgPessoa;
    private Date dataExpRgPessoa;
    private String profissaoPessoa;
    private String empresaPessoa;
    private String foneEmpresaPessoa;
    private char sexoPessoa;
    private Boolean cliente;
    private Boolean funcionario;
    private Boolean fornecedor;
    private Boolean transportadora;
    private Double descontoCliente;
    private Double valorHoraFuncionario;
    private ArrayList<Endereco> enderecos;
    private ArrayList<Contato> contatos;
    private Boolean flagPronto;

    public Boolean isCliente() {
        return getCliente();
    }

    public Boolean isFornecedor() {
        return getFornecedor();
    }

    public Boolean isFuncionario() {
        return getFuncionario();
    }
    
    public Boolean isTransportadora() {
        return getTransportadora();
    }

    public Boolean getCliente() {
        return cliente;
    }

    public void setCliente(Boolean cliente) {
        this.cliente = cliente;
    }

    public Boolean getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Boolean fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Boolean getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Boolean funcionario) {
        this.funcionario = funcionario;
    }

    public Pessoa() {
        enderecos = new ArrayList<>();
        contatos = new ArrayList<>();
        clearPessoa();
    }
    
    public Pessoa(Integer cdPessoa) {
        enderecos = new ArrayList<>();
        contatos = new ArrayList<>();
        clearPessoa();
        this.cdPessoa = cdPessoa;
    }

    public final void clearPessoa() {
        cdPessoa = null;
        tipoPessoa = 'F';
        nomePessoa = "";
        dataNascimento = null;
        dataCadPessoa = null;
        descricaoPessoa = "";
        emailPessoa = "";
        sitePessoa = "";
        telefoneFixoPessoa = "";
        telefoneCellPessoa = "";
        cpfCnpjPessoa = "";
        rgIePessoa = "";
        orgaoRgPessoa = "";
        dataExpRgPessoa = null;
        profissaoPessoa = "";
        empresaPessoa = "";
        foneEmpresaPessoa = "";
        sexoPessoa = 'M';
        cliente = false;
        funcionario = false;
        fornecedor = false;
        transportadora = false;
        descontoCliente = 0.00;
        setValorHoraFuncionario((Double) 0.00);
        enderecos.clear();
        contatos.clear();
    }

    /**
     * @return the cdPessoa
     */
    public Integer getCdPessoa() {
        return cdPessoa;
    }

    /**
     * @param cdPessoa the cdPessoa to set
     */
    public void setCdPessoa(Integer cdPessoa) {
        this.cdPessoa = cdPessoa;
    }

    /**
     * @return the tipoPessoa
     */
    public char getTipoPessoa() {
        return tipoPessoa;
    }

    /**
     * @param tipoPessoa the tipoPessoa to set
     */
    public void setTipoPessoa(char tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    /**
     * @return the nomePessoa
     */
    public String getNomePessoa() {
        return nomePessoa;
    }

    /**
     * @param nomePessoa the nomePessoa to set
     */
    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa!=null?nomePessoa.toUpperCase().trim():null;
    }

    /**
     * @return the dataNascimento
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * @param dataNascimento the dataNascimento to set
     */
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    /**
     * @return the dataCadPessoa
     */
    public Date getDataCadPessoa() {
        return dataCadPessoa;
    }

    /**
     * @param dataCadPessoa the dataCadPessoa to set
     */
    public void setDataCadPessoa(Date dataCadPessoa) {
        this.dataCadPessoa = dataCadPessoa;
    }

    /**
     * @return the descricaoPessoa
     */
    public String getDescricaoPessoa() {
        return descricaoPessoa;
    }

    /**
     * @param descricaoPessoa the descricaoPessoa to set
     */
    public void setDescricaoPessoa(String descricaoPessoa) {
        this.descricaoPessoa = descricaoPessoa!=null?descricaoPessoa.toUpperCase().trim():null;
    }

    /**
     * @return the emailPessoa
     */
    public String getEmailPessoa() {
        return emailPessoa;
    }

    /**
     * @param emailPessoa the emailPessoa to set
     */
    public void setEmailPessoa(String emailPessoa) {
        this.emailPessoa = emailPessoa;
    }

    /**
     * @return the sitePessoa
     */
    public String getSitePessoa() {
        return sitePessoa;
    }

    /**
     * @param sitePessoa the sitePessoa to set
     */
    public void setSitePessoa(String sitePessoa) {
        this.sitePessoa = sitePessoa;
    }

    /**
     * @return the telefoneFixoPessoa
     */
    public String getTelefoneFixoPessoa() {
        return telefoneFixoPessoa;
    }

    /**
     * @param telefoneFixoPessoa the telefoneFixoPessoa to set
     */
    public void setTelefoneFixoPessoa(String telefoneFixoPessoa) {
        this.telefoneFixoPessoa = telefoneFixoPessoa;
    }

    /**
     * @return the telefoneCellPessoa
     */
    public String getTelefoneCellPessoa() {
        return telefoneCellPessoa;
    }

    /**
     * @param telefoneCellPessoa the telefoneCellPessoa to set
     */
    public void setTelefoneCellPessoa(String telefoneCellPessoa) {
        this.telefoneCellPessoa = telefoneCellPessoa;
    }

    /**
     * @return the cnpjPessoa
     */
    public String getCpfCnpjPessoa() {
        return cpfCnpjPessoa;
    }

    /**
     * @param cnpjPessoa the cnpjPessoa to set
     */
    public void setCpfCnpjPessoa(String cpfCnpjPessoa) {
        this.cpfCnpjPessoa = cpfCnpjPessoa;
    }

    /**
     * @return the iePessoa
     */
    public String getRgIePessoa() {
        return rgIePessoa;
    }

    /**
     * @param iePessoa the iePessoa to set
     */
    public void setRgIePessoa(String rgIePessoa) {
        this.rgIePessoa = rgIePessoa;
    }

    
    /**
     * @return the orgaoRgPessoa
     */
    public String getOrgaoRgPessoa() {
        return orgaoRgPessoa;
    }

    /**
     * @param orgaoRgPessoa the orgaoRgPessoa to set
     */
    public void setOrgaoRgPessoa(String orgaoRgPessoa) {
        this.orgaoRgPessoa = orgaoRgPessoa!=null?orgaoRgPessoa.toUpperCase().trim():null;
    }

    /**
     * @return the dataExpRgPessoa
     */
    public Date getDataExpRgPessoa() {
        return dataExpRgPessoa;
    }

    /**
     * @param dataExpRgPessoa the dataExpRgPessoa to set
     */
    public void setDataExpRgPessoa(Date dataExpRgPessoa) {
        this.dataExpRgPessoa = dataExpRgPessoa;
    }

    /**
     * @return the profissaoPessoa
     */
    public String getProfissaoPessoa() {
        return profissaoPessoa;
    }

    /**
     * @param profissaoPessoa the profissaoPessoa to set
     */
    public void setProfissaoPessoa(String profissaoPessoa) {
        this.profissaoPessoa = profissaoPessoa!=null?profissaoPessoa.toUpperCase().trim():null;
    }

    /**
     * @return the empresaPessoa
     */
    public String getEmpresaPessoa() {
        return empresaPessoa;
    }

    /**
     * @param empresaPessoa the empresaPessoa to set
     */
    public void setEmpresaPessoa(String empresaPessoa) {
        this.empresaPessoa = empresaPessoa!=null?empresaPessoa.toUpperCase().trim():null;
    }

    /**
     * @return the foneEmpresaPessoa
     */
    public String getFoneEmpresaPessoa() {
        return foneEmpresaPessoa;
    }

    /**
     * @param foneEmpresaPessoa the foneEmpresaPessoa to set
     */
    public void setFoneEmpresaPessoa(String foneEmpresaPessoa) {
        this.foneEmpresaPessoa = foneEmpresaPessoa;
    }

    /**
     * @return the sexoPessoa
     */
    public char getSexoPessoa() {
        return sexoPessoa;
    }

    /**
     * @param sexoPessoa the sexoPessoa to set
     */
    public void setSexoPessoa(char sexoPessoa) {
        this.sexoPessoa = sexoPessoa;
    }

    /**
     * @return the flagPronto
     */
    public Boolean getFlagPronto() {
        return flagPronto;
    }

    /**
     * @param flagPronto the flagPronto to set
     */
    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();// ? Registro de modificação do modelo
        notifyObservers(); // ? Notificação de modificação nos valores do modelo
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pessoa other = (Pessoa) obj;
        if (this.cdPessoa != other.cdPessoa && (this.cdPessoa == null || !this.cdPessoa.equals(other.cdPessoa))) {
            return false;
        }
        if (this.tipoPessoa != other.tipoPessoa) {
            return false;
        }
        if ((this.nomePessoa == null) ? (other.nomePessoa != null) : !this.nomePessoa.equals(other.nomePessoa)) {
            return false;
        }
        if ((this.cpfCnpjPessoa == null) ? (other.cpfCnpjPessoa != null) : !this.cpfCnpjPessoa.equals(other.cpfCnpjPessoa)) {
            return false;
        }
        if ((this.rgIePessoa == null) ? (other.rgIePessoa != null) : !this.rgIePessoa.equals(other.rgIePessoa)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    /**
     * @return the enderecos
     */
    public ArrayList<Endereco> getEnderecos() {
        return enderecos;
    }

    /**
     * @param enderecos the enderecos to set
     */
    public void setEnderecos(ArrayList<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    /**
     * @return the contatos
     */
    public ArrayList<Contato> getContatos() {
        return contatos;
    }

    /**
     * @param contatos the contatos to set
     */
    public void setContatos(ArrayList<Contato> contatos) {
        this.contatos = contatos;
    }

    /**
     * @return the descontoCliente
     */
    public Double getDescontoCliente() {
        return descontoCliente;
    }

    /**
     * @param descontoCliente the descontoCliente to set
     */
    public void setDescontoCliente(Double descontoCliente) {
        this.descontoCliente = descontoCliente;
    }

    /**
     * @return the valorHoraFuncionario
     */
    public Double getValorHoraFuncionario() {
        return valorHoraFuncionario;
    }

    /**
     * @param valorHoraFuncionario the valorHoraFuncionario to set
     */
    public void setValorHoraFuncionario(Double valorHoraFuncionario) {
        this.valorHoraFuncionario = valorHoraFuncionario;
    }

    /**
     * @return the tranportadora
     */
    public Boolean getTransportadora() {
        return transportadora;
    }

    /**
     * @param tranportadora the tranportadora to set
     */
    public void setTransportadora(Boolean tranportadora) {
        this.transportadora = tranportadora;
    }
}
