/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Date;

/**
 *
 * @author Jean
 */
public class Usuario extends Funcionario implements Cloneable {

    private Integer codigoUsuario;
    private String nomeUsuario;
    private String senhaUsuario;
    private String descricaoUsuario;
    private Date dataCadastroUsuario;
    private Empresa empresa;

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Date getDataCadastro() {
        return dataCadastroUsuario;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastroUsuario = dataCadastro;
    }

    public String getDescricao() {
        return descricaoUsuario;
    }

    public void setDescricao(String descricao) {
        this.descricaoUsuario = descricao != null ? descricao.toUpperCase().trim() : null;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario != null ? nomeUsuario.toUpperCase().trim() : null;
    }

    public String getSenhaUsuario() {
        return senhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }

    public Usuario(Integer codigoUsuario, String nomeUsuario, String senhaUsuario, String descricao, Date dataCadastro, Empresa empresa) {
        this.codigoUsuario = codigoUsuario;
        this.nomeUsuario = nomeUsuario != null ? nomeUsuario.toUpperCase().trim() : null;
        this.senhaUsuario = senhaUsuario;
        this.descricaoUsuario = descricao != null ? descricao.toUpperCase().trim() : null;
        this.dataCadastroUsuario = dataCadastro;
        this.empresa = empresa;
    }

    public Usuario(Empresa empresa) {
        this.codigoUsuario = null;
        this.nomeUsuario = "";
        this.senhaUsuario = "";
        this.descricaoUsuario = "";
        this.dataCadastroUsuario = null;
        this.empresa = empresa;
    }

    public Usuario(Integer codigoUsuario, Empresa empresa) {
        this.codigoUsuario = codigoUsuario;
        this.empresa = empresa;
        this.nomeUsuario = "";
        this.senhaUsuario = "";
        this.descricaoUsuario = "";
        this.dataCadastroUsuario = null;
    }

    public void clearUsuario() {
        clearFuncionario();
        this.codigoUsuario = null;
        this.nomeUsuario = "";
        this.senhaUsuario = "";
        this.descricaoUsuario = "";
        this.dataCadastroUsuario = null;
    }

    /**
     * @return the codigoUsuario
     */
    public Integer getCodigoUsuario() {
        return codigoUsuario;
    }

    /**
     * @param codigoUsuario the codigoUsuario to set
     */
    public void setCodigoUsuario(Integer codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.codigoUsuario != other.codigoUsuario && (this.codigoUsuario == null || !this.codigoUsuario.equals(other.codigoUsuario))) {
            return false;
        }
        if ((this.nomeUsuario == null) ? (other.nomeUsuario != null) : !this.nomeUsuario.equals(other.nomeUsuario)) {
            return false;
        }
        if (this.dataCadastroUsuario != other.dataCadastroUsuario && (this.dataCadastroUsuario == null || !this.dataCadastroUsuario.equals(other.dataCadastroUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public String toString() {
        return nomeUsuario;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
