/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import utilitarios.Format;

/**
 *
 * @author jean-marcos
 */
public class Equipamento extends Observable
{
  private Integer codEquipamento;
  private String marca;
  private String modelo;
  private String descricao;
  private Integer nroRastreioInt;
  private Integer nroRastreioCli;
  private String nroSerie;
  private Date dataFabricacao;
  private TipoEquipamento tipoEquipamento;
  private ArrayList<Imagem> imagens = new ArrayList<>();
  private Cliente cliente;
  private Boolean flagPronto;

  public Equipamento()
  {
  }

  public Equipamento(Integer codEquipamento)
  {
    this.codEquipamento = codEquipamento;
  }

  public Equipamento(String nroSerie) {
    this.nroSerie = nroSerie;
  }

  public Equipamento(Integer codEquipamento, String marca, String modelo, String descricao, Integer nroRastreioInt, Integer nroRastreioCli, String nroSerie, Date dataFabricacao, TipoEquipamento tipoEquipamento, ArrayList<Imagem> imagens, Cliente cliente) {
    this.codEquipamento = codEquipamento;
    this.marca = (marca != null ? marca.toUpperCase().trim() : null);
    this.modelo = (modelo != null ? modelo.toUpperCase().trim() : null);
    this.descricao = descricao;
    this.nroRastreioInt = nroRastreioInt;
    this.nroRastreioCli = nroRastreioCli;
    this.nroSerie = (nroSerie != null ? Format.RemoveCaracteres(nroSerie.toUpperCase().trim(), "-/") : null);
    this.dataFabricacao = dataFabricacao;
    this.tipoEquipamento = tipoEquipamento;
    this.imagens = imagens;
    this.cliente = cliente;
  }

  public Integer getCodEquipamento()
  {
    return this.codEquipamento;
  }

  public void setCodEquipamento(Integer codEquipamento)
  {
    this.codEquipamento = codEquipamento;
  }

  public String getMarca()
  {
    return this.marca;
  }

  public void setMarca(String marca)
  {
    this.marca = (marca != null ? marca.toUpperCase().trim() : null);
  }

  public String getModelo()
  {
    return this.modelo;
  }

  public void setModelo(String modelo)
  {
    this.modelo = (modelo != null ? modelo.toUpperCase().trim() : null);
  }

  public String getDescricao()
  {
    return this.descricao;
  }

  public void setDescricao(String descricao)
  {
    this.descricao = descricao;
  }

  public Integer getNroRastreioInt()
  {
    return this.nroRastreioInt;
  }

  public void setNroRastreioInt(Integer nroRastreioInt)
  {
    this.nroRastreioInt = nroRastreioInt;
  }

  public String getNroSerie()
  {
    return this.nroSerie;
  }

  public void setNroSerie(String nroSerie)
  {
    this.nroSerie = (nroSerie != null ? Format.RemoveCaracteres(nroSerie.toUpperCase().trim(), "-/") : null);
  }

  public Date getDataFabricacao()
  {
    return this.dataFabricacao;
  }

  public void setDataFabricacao(Date dataFabricacao)
  {
    this.dataFabricacao = dataFabricacao;
  }

  public TipoEquipamento getTipoEquipamento()
  {
    return this.tipoEquipamento;
  }

  public void setTipoEquipamento(TipoEquipamento tipoEquipamento)
  {
    this.tipoEquipamento = tipoEquipamento;
  }


  public Cliente getCliente()
  {
    return this.cliente;
  }

  public void setCliente(Cliente cliente)
  {
    this.cliente = cliente;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public final void clearEquipamento() {
    this.codEquipamento = null;
    this.marca = "";
    this.modelo = "";
    this.descricao = "";
    this.nroRastreioInt = null;
    this.nroRastreioCli = null;
    this.nroSerie = "";
    this.dataFabricacao = null;
    this.tipoEquipamento = null;
        this.getImagens().clear();
    this.cliente = null;
  }

    @Override
  public String toString()
  {
    return this.marca + ", " + this.modelo;
  }

    /**
     * @return the nroRastreioCli
     */
    public Integer getNroRastreioCli() {
        return nroRastreioCli;
    }

    /**
     * @param nroRastreioCli the nroRastreioCli to set
     */
    public void setNroRastreioCli(Integer nroRastreioCli) {
        this.nroRastreioCli = nroRastreioCli;
    }

    /**
     * @return the imagens
     */
    public ArrayList<Imagem> getImagens() {
        return imagens;
    }

    /**
     * @param imagens the imagens to set
     */
    public void setImagens(ArrayList<Imagem> imagens) {
        this.imagens = imagens;
    }
}