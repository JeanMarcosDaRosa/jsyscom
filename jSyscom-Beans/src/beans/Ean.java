/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Date;
import utilitarios.Validador;

/**
 *
 * @author Jean
 */
public class Ean {
    private String ean;
    private Date dataCad;
    private Produto produto;

    public Ean(String ean, Produto produto) {
        this.ean = ean!=null?Validador.soNumeros(ean):null;
        this.produto = produto;
    }

    public Ean() {
    }

    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }

    /**
     * @param ean the ean to set
     */
    public void setEan(String ean) {
        this.ean = ean!=null?Validador.soNumeros(ean):null;
    }

    /**
     * @return the produto
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    /**
     * @return the dataCad
     */
    public Date getDataCad() {
        return dataCad;
    }

    /**
     * @param dataCad the dataCad to set
     */
    public void setDataCad(Date dataCad) {
        this.dataCad = dataCad;
    }
}
