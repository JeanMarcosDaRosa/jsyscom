/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author Jean
 */
public class Cliente extends Pessoa {

    public Cliente() {
    }

    public Cliente(Integer codCliente) {
        setCdPessoa(codCliente);
    }

    public void clearCliente() {
        clearPessoa();
    }

    @Override
    public String toString() {
        return getNomePessoa();
    }
    
}
