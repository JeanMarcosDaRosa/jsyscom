/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author jean-marcos
 */
public class NFFatura {
    private String numero;
    private Date vencimento;
    private Double valor;
    private NFEntrada nfEntrada;

    public NFFatura() {
    }

    public NFFatura(String numero, Date vencimento, Double valor) {
        this.numero = numero;
        this.vencimento = vencimento;
        this.valor = valor;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the vencimento
     */
    public Date getVencimento() {
        return vencimento;
    }

    /**
     * @param vencimento the vencimento to set
     */
    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    /**
     * @return the valor
     */
    public Double getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(Double valor) {
        this.valor = valor;
    }
    
        @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.numero);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NFFatura other = (NFFatura) obj;
        if (!Objects.equals(this.numero, other.numero)) {
            return false;
        }
        return true;
    }

    /**
     * @return the nfEntrada
     */
    public NFEntrada getNfEntrada() {
        return nfEntrada;
    }

    /**
     * @param nfEntrada the nfEntrada to set
     */
    public void setNfEntrada(NFEntrada nfEntrada) {
        this.nfEntrada = nfEntrada;
    }

}
