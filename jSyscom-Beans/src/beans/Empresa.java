/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import utilitarios.Validador;

/**
 *
 * @author Jean
 */
public class Empresa {

    private Integer codigo;
    private String  rsocial;
    private String  fantasia;
    private String  cnpj;
    private String  ie;
    private String  fone;
    private String  fax;
    private String  responsavel;
    private String  email;
    private Endereco endereco;

    public Empresa() {
    }

    public Empresa(Integer codigo) {
        this.codigo = codigo;
    }
    
    /**
     * @return the codigo
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the rsocial
     */
    public String getRsocial() {
        return rsocial;
    }

    /**
     * @param rsocial the rsocial to set
     */
    public void setRsocial(String rsocial) {
        this.rsocial = rsocial!=null?rsocial.toUpperCase().trim():null;
    }

    /**
     * @return the fantasia
     */
    public String getFantasia() {
        return fantasia;
    }

    /**
     * @param fantasia the fantasia to set
     */
    public void setFantasia(String fantasia) {
        this.fantasia = fantasia!=null?fantasia.toUpperCase().trim():null;
    }

    /**
     * @return the cnpj
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * @param cnpj the cnpj to set
     */
    public void setCnpj(String cnpj) {
        this.cnpj = Validador.soNumeros(cnpj);
    }

    /**
     * @return the ie
     */
    public String getIe() {
        return ie;
    }

    /**
     * @param ie the ie to set
     */
    public void setIe(String ie) {
        this.ie = Validador.soNumeros(ie);
    }

    /**
     * @return the fone
     */
    public String getFone() {
        return fone;
    }

    /**
     * @param fone the fone to set
     */
    public void setFone(String fone) {
        this.fone = fone;
    }

    /**
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * @param fax the fax to set
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * @return the responsavel
     */
    public String getResponsavel() {
        return responsavel;
    }

    /**
     * @param responsavel the responsavel to set
     */
    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel!=null?responsavel.toUpperCase().trim():null;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the endereco
     */
    public Endereco getEndereco() {
        return endereco;
    }

    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
