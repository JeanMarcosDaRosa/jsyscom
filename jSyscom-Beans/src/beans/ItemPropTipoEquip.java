package beans;

public class ItemPropTipoEquip
{
  private PropriedadeTipoEquip propTipoEquip;
  private String valor;

  public ItemPropTipoEquip(PropriedadeTipoEquip propTipoEquip, String valor)
  {
    this.propTipoEquip = propTipoEquip;
    this.valor = valor;
  }

  public PropriedadeTipoEquip getPropTipoEquip()
  {
    return this.propTipoEquip;
  }

  public void setPropTipoEquip(PropriedadeTipoEquip propTipoEquip)
  {
    this.propTipoEquip = propTipoEquip;
  }

  public String getValor()
  {
    return this.valor;
  }

  public void setValor(String valor)
  {
    this.valor = valor;
  }
}