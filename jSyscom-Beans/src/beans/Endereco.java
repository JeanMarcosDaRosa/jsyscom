/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Objects;
import java.util.Observable;

/**
 *
 * @author Jean
 */
public class Endereco extends Observable
{
  private String nomePais;
  private String siglaUf;
  private String nomeCidade;
  private String nomeBairro;
  private String nomeLogradouro;
  private String cepLogradouro;
  private String complemento;
  private Integer numero;
  private Boolean principal;
  private Boolean flagPronto;

  public Endereco(String nomePais, String siglaUf, String nomeCidade, String nomeBairro, String nomeLogradouro, String cepLogradouro, Integer numero, String complemento, Boolean principal)
  {
    this.nomePais = nomePais;
    this.siglaUf = siglaUf;
    this.nomeCidade = nomeCidade;
    this.nomeBairro = nomeBairro;
    this.nomeLogradouro = nomeLogradouro;
    this.cepLogradouro = cepLogradouro;
    this.numero = numero;
    this.complemento = complemento;
    this.principal = principal;
  }

  public Endereco(String nomePais, String siglaUf, String nomeCidade, String nomeBairro, String nomeLogradouro, String cepLogradouro) {
    this.nomePais = nomePais;
    this.siglaUf = siglaUf;
    this.nomeCidade = nomeCidade;
    this.nomeBairro = nomeBairro;
    this.nomeLogradouro = nomeLogradouro;
    this.cepLogradouro = cepLogradouro;
    this.numero = Integer.valueOf(0);
    this.complemento = "";
    this.principal = Boolean.valueOf(false);
  }

  public Endereco() {
    this.nomePais = "";
    this.siglaUf = "";
    this.nomeCidade = "";
    this.nomeBairro = "";
    this.nomeLogradouro = "";
    this.cepLogradouro = "";
    this.numero = Integer.valueOf(0);
    this.complemento = "";
    this.principal = Boolean.valueOf(false);
  }

  public Endereco(String cep) {
    this.cepLogradouro = cep;
    this.nomePais = "";
    this.siglaUf = "";
    this.nomeCidade = "";
    this.nomeBairro = "";
    this.nomeLogradouro = "";
    this.numero = Integer.valueOf(0);
    this.complemento = "";
    this.principal = Boolean.valueOf(false);
  }

  public void clearEndereco() {
    this.nomePais = "";
    this.siglaUf = "";
    this.nomeCidade = "";
    this.nomeBairro = "";
    this.nomeLogradouro = "";
    this.cepLogradouro = "";
  }

  public String getNomePais()
  {
    return this.nomePais;
  }

  public void setNomePais(String nomePais)
  {
    this.nomePais = nomePais;
  }

  public String getSiglaUf()
  {
    return this.siglaUf;
  }

  public void setSiglaUf(String siglaUf)
  {
    this.siglaUf = siglaUf;
  }

  public String getNomeCidade()
  {
    return this.nomeCidade;
  }

  public void setNomeCidade(String nomeCidade)
  {
    this.nomeCidade = nomeCidade;
  }

  public String getNomeBairro()
  {
    return this.nomeBairro;
  }

  public void setNomeBairro(String nomeBairro)
  {
    this.nomeBairro = nomeBairro;
  }

  public String getNomeLogradouro()
  {
    return this.nomeLogradouro;
  }

  public void setNomeLogradouro(String nomeLogradouro)
  {
    this.nomeLogradouro = nomeLogradouro;
  }

  public String getCepLogradouro()
  {
    return this.cepLogradouro;
  }

  public void setCepLogradouro(String cepLogradouro)
  {
    this.cepLogradouro = cepLogradouro;
  }

  public Boolean getFlagPronto()
  {
    return this.flagPronto;
  }

  public void setFlagPronto(Boolean flagPronto)
  {
    this.flagPronto = flagPronto;
    setChanged();
    notifyObservers();
  }

  public String toString()
  {
    return this.nomeLogradouro + " - " + this.numero + ", " + this.nomeBairro + " - " + this.nomeCidade;
  }

  public int hashCode()
  {
    int hash = 7;
    return hash;
  }

  public boolean equals(Object obj)
  {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Endereco other = (Endereco)obj;
    if (!Objects.equals(this.cepLogradouro, other.cepLogradouro)) {
      return false;
    }
    if (!Objects.equals(this.numero, other.numero)) {
      return false;
    }
    return true;
  }

  public String getComplemento()
  {
    return this.complemento;
  }

  public void setComplemento(String complemento)
  {
    this.complemento = complemento;
  }

  public Integer getNumero()
  {
    return this.numero;
  }

  public void setNumero(Integer numero)
  {
    this.numero = numero;
  }

  public Boolean getPrincipal()
  {
    return this.principal;
  }

  public void setPrincipal(Boolean principal)
  {
    this.principal = principal;
  }
}