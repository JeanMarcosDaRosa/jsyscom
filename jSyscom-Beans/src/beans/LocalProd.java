/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Observable;

/**
 *
 * @author jean
 */
public class LocalProd extends Observable {

    private Integer codLocalProd;
    private String descricao;
    private Boolean flagPronto;

    public LocalProd(Integer codLocalProd, String descricao) {
        this.codLocalProd = codLocalProd;
        this.descricao = descricao != null ? descricao.toUpperCase().trim() : null;
    }

    public LocalProd(String descricao) {
        this.codLocalProd = null;
        this.descricao = descricao != null ? descricao.toUpperCase().trim() : null;
    }
    
    public LocalProd(Integer  codLocalProd) {
        this.codLocalProd = codLocalProd;
        this.descricao = "";
    }

    public LocalProd() {
        this.codLocalProd = null;
        this.descricao = "";
    }

    /**
     * @return the codLocalProd
     */
    public Integer getCodLocalProd() {
        return codLocalProd;
    }

    /**
     * @param codLocalProd the codLocalProd to set
     */
    public void setCodLocalProd(Integer codLocalProd) {
        this.codLocalProd = codLocalProd;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao != null ? descricao.toUpperCase().trim() : null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LocalProd other = (LocalProd) obj;
        if ((this.descricao == null) ? (other.descricao != null) : !this.descricao.equals(other.descricao)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descricao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    /**
     * @return the flagPronto
     */
    public Boolean getFlagPronto() {
        return flagPronto;
    }

    /**
     * @param flagPronto the flagPronto to set
     */
    public void setFlagPronto(Boolean flagPronto) {
        this.flagPronto = flagPronto;
        setChanged();// ? Registro de modificação do modelo
        notifyObservers(); // ? Notificação de modificação nos valores do modelo
    }

    public final void clearLocalProd() {
        this.codLocalProd = null;
        this.descricao = "";
    }
}
