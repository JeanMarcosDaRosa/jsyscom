/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author jean-marcos
 */
public class EquipamentoTeste {
    private Integer codTeste;
    private Equipamento equipamento;
    private String descricao;
    private String defeito;

    public EquipamentoTeste() {
    }

    public EquipamentoTeste(Integer codTeste, Equipamento equipamento) {
        this.codTeste = codTeste;
        this.equipamento = equipamento;
    }

    public EquipamentoTeste(Integer codTeste, Equipamento equipamento, String descricao, String defeito) {
        this.codTeste = codTeste;
        this.equipamento = equipamento;
        this.descricao = descricao!=null?descricao.toUpperCase().trim():null;
        this.defeito = defeito!=null?defeito.toUpperCase().trim():null;
    }

    /**
     * @return the codTeste
     */
    public Integer getCodTeste() {
        return codTeste;
    }

    /**
     * @param codTeste the codTeste to set
     */
    public void setCodTeste(Integer codTeste) {
        this.codTeste = codTeste;
    }

    /**
     * @return the equipamento
     */
    public Equipamento getEquipamento() {
        return equipamento;
    }

    /**
     * @param equipamento the equipamento to set
     */
    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao!=null?descricao.toUpperCase().trim():null;
    }

    /**
     * @return the defeito
     */
    public String getDefeito() {
        return defeito;
    }

    /**
     * @param defeito the defeito to set
     */
    public void setDefeito(String defeito) {
        this.defeito =  defeito!=null?defeito.toUpperCase().trim():null;
    }
    
    
}
