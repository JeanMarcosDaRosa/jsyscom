/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author Jean
 */
public class Funcionario extends Pessoa {

    public Funcionario() {
    }

    public Funcionario(Integer cd_funcionario) {
        setCdPessoa(cd_funcionario);
    }

    public void clearFuncionario() {
        clearPessoa();
    }

    @Override
    public String toString() {
        return getNomePessoa();
    }
}
