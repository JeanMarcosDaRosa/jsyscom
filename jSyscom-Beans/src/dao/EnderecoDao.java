/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Endereco;
import beans.Variaveis;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilitarios.DataBase;
import utilitarios.Utils;

/**
 *
 * @author Jean
 */
public abstract class EnderecoDao
{
  public static Endereco buscaCEP(String cep)
  {
    Endereco e = new Endereco();
    buscaCEP(cep, e);
    return e;
  }

  public static void buscaCEP(String cep, Endereco e) {
    String sql = "select \tlogradouro.cep_logradouro, \tpais.nome_pais, uf.sigla_uf, cidade.nome_cidade, \tbairro.nome_bairro, logradouro.nome_logradouro from \tpais, uf, cidade, bairro, logradouro where \tpais.cd_pais = uf.cd_pais and \tuf.sigla_uf = cidade.sigla_uf and \tcidade.cd_cidade = bairro.cd_cidade and \tbairro.cd_bairro = logradouro.cd_bairro and \tlogradouro.cd_cidade = cidade.cd_cidade and      logradouro.cep_logradouro = ? ";

    Object[] par = { cep };
    try {
      ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), 2, Variaveis.getDbType());

      if (rs.next())
        populaEnderecoResultSet(rs, e);
    }
    catch (Exception ex) {
      Variaveis.addNewLog(EnderecoDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static void populaEnderecoResultSet(ResultSet rs, Endereco e) {
    try {
      e.setCepLogradouro(rs.getString("cep_logradouro"));
      e.setNomePais(rs.getString("nome_pais"));
      e.setSiglaUf(rs.getString("sigla_uf"));
      e.setNomeCidade(rs.getString("nome_cidade"));
      e.setNomeBairro(rs.getString("nome_bairro"));
      e.setNomeLogradouro(rs.getString("nome_logradouro"));
      e.setFlagPronto(Boolean.valueOf(true));
    } catch (SQLException ex) {
      Variaveis.addNewLog(EnderecoDao.class.getName() + ":\n" + Utils.getStackTrace(ex), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
  }

  public static Boolean salvaEndereco(Endereco endereco)
  {
    try
    {
      return Boolean.valueOf(true);
    } catch (Exception e) {
      Variaveis.addNewLog(ProdutoDao.class.getName() + ":\n" + Utils.getStackTrace(e), Boolean.valueOf(false), Boolean.valueOf(true), null);
    }
    return Boolean.valueOf(false);
  }
}