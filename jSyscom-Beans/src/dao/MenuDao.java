/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Menu;
import beans.OpcoesTela;
import beans.Variaveis;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import utilitarios.DataBase;
import utilitarios.Utils;

/**
 *
 * @author Jean
 */
public abstract class MenuDao {

    //Essa é a função recursiva que vai montar os grupos e subgrupos  
    //primeiro ele seleciona todos os que não tem super grupo  
    //depois os subgrupos deles, depois os subgrupos deles....  
    public static boolean setGrupos(DefaultMutableTreeNode node, Integer codigo_nodo_sup, Integer cd_user) throws SQLException {
        String query;
        Boolean ok = false;
        if (codigo_nodo_sup == null) {
            query = "SELECT * FROM menu WHERE id_nodo_sup IS NULL order by nodo_menu";
        } else {
            if (cd_user == null) {
                query = "select distinct(m.*) from "
                        + "menu as m where "
                        + "id_nodo_sup = " + String.valueOf(codigo_nodo_sup)
                        + "order by nodo_menu ";
            } else {
                query = "select distinct(m.*) from "
                        + "usuario as u, restricoes as r, menu as m where "
                        + "r.cd_usuario = u.cd_usuario and "
                        + "r.cd_empresa = " + String.valueOf(Variaveis.getEmpresa().getCodigo()) + " and "
                        + "u.cd_usuario = " + String.valueOf(cd_user) + " and "
                        + "(((r.cd_acao = m.cd_acao) or (m.cd_acao is null)) and id_nodo_sup = " + String.valueOf(codigo_nodo_sup) + " ) "
                        + "order by nodo_menu ";
            }
        }
        try {
            ResultSet rs = DataBase.executeQuery(query, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                Integer id = rs.getInt("cd_menu");
                String nome = rs.getString("nodo_menu");
                Integer id_super = rs.getInt("id_nodo_sup");
                Integer oper = rs.getInt("cd_acao");
                Menu m = new Menu(id, nome, id_super, oper);
                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(m);
                if (rs.getInt("cd_acao") > 0) {
                    node.add(newNode);
                    ok = true;
                } else if (setGrupos(newNode, id, cd_user) || (cd_user == null)) {
                    node.add(newNode);
                    ok = true;
                }
            }
        } catch (SQLException ex) {
            Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
        return ok;
    }

    //CADASTROS DE MENUS--------------------------------------------------------
    public static void cadOpcoesTela(OpcoesTela opcoes, Integer classe) {
        if (classe != null) {
            try {
                String SQL = "select * from opcoes_tela where cd_classe = ?";
                Object[] par1 = {classe};
                Object[] par2 = {classe, opcoes.getIconifiable(), opcoes.getMaximizable(), opcoes.getResizable(),
                    opcoes.getClosable(), opcoes.getVisible(), opcoes.getShow_version()};
                ResultSet res = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                if (!res.next()) {
                    SQL = "insert into opcoes_tela (cd_classe, iconifiable,maximizable,"
                            + "resizable,closable,visible,show_version) values(?,?,?,?,?,?,?)";
                    DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                } else {
                    SQL = "update opcoes_tela set cd_classe = ?, iconifiable = ?,maximizable = ?,"
                            + "resizable = ?,closable = ?,visible = ?,show_version = ? where cd_classe = " + classe;
                    DataBase.executeQuery(SQL, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                }
            } catch (Exception e) {
                Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
            }
        }
    }

    public static Integer cadClasse(String nomeModulo, String nomeClasse, String descricao, OpcoesTela opcoes) {
        if (!"".equals(nomeClasse) && !"".equals(nomeModulo)) {
            Integer cdAcao = null;
            try {
                Integer cd_modulos = 0;
                String SQL = "select cd_modulos from modulos where nome_modulos = ?";
                Object[] a = {nomeModulo};
                ResultSet x = DataBase.executeQuery(SQL, a, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                if (x.next()) {
                    cd_modulos = x.getInt("cd_modulos");
                }
                SQL = "select * from classe where nome_classe like (?)";
                Object[] par = {nomeClasse};
                ResultSet res = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                if (!res.next()) {
                    Object[] par1 = {MenuDao.cadAcao(), NextDaos.nextGenericoSemEmpresa("classe"), nomeClasse, cd_modulos, descricao.trim()};
                    SQL = "insert into classe (cd_acao, cd_classe, nome_classe, cd_modulos, descricao_classe) values(?,?,?,?,?) RETURNING cd_acao, cd_classe";
                    ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                    if (rs.next()) {
                        cdAcao = rs.getInt("cd_acao");
                        cadOpcoesTela(opcoes, rs.getInt("cd_classe"));
                    }
                } else {
                    Object[] par1 = {descricao, res.getInt("cd_classe")};
                    SQL = "update classe set descricao_classe = ? where cd_classe = ?";
                    DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                    cdAcao = res.getInt("cd_acao");
                    cadOpcoesTela(opcoes, res.getInt("cd_classe"));
                }
                return cdAcao;
            } catch (Exception e) {
                Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
            }
        }
        return null;
    }

    public static Integer cadRelatorio(String nome) {
        if (!"".equals(nome)) {
            Integer cdAcao = null;
            try {
                String SQL = "select * from relatorio where nome_relatorio like (?)";
                Object[] par = {nome};
                ResultSet res = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                if (!res.next()) {
                    Object[] par1 = {MenuDao.cadAcao(), NextDaos.nextGenericoSemEmpresa("relatorio"), nome};
                    SQL = "insert into relatorio (cd_acao, cd_relatorio, nome_relatorio) values(?,?,?) RETURNING cd_acao";
                    ResultSet rs = DataBase.executeQuery(SQL, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                    if (rs.next()) {
                        cdAcao = rs.getInt("cd_acao");
                    }
                } else {
                    cdAcao = res.getInt("cd_acao");
                }
                return cdAcao;
            } catch (Exception e) {
                Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
            }
        }
        return null;
    }

    public static ArrayList<String> getModulos() {
        ArrayList<String> mods = new ArrayList<>();
        try {
            String SQL = "select * from modulos where ativo_modulos = true";
            ResultSet rs = DataBase.executeQuery(SQL, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                mods.add(rs.getString("nome_modulos"));
            }
        } catch (Exception e) {
            Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return mods;
    }

    public static Integer cadAcao() {
        try {
            String SQL = "select max(cd_acao) as prox from acao";
            ResultSet res = DataBase.executeQuery(SQL, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (res.next()) {
                SQL = "insert into acao (cd_acao) values(?)";
                DataBase.executeQuery(SQL, new Object[]{res.getInt("prox") + 1}, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                return res.getInt("prox") + 1;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return null;
    }

    public static ArrayList<String> getListClasses() {
        ArrayList<String> lista = new ArrayList<>();
        try {
            String SQL = "select * from classe where nome_classe is not null";
            ResultSet res = DataBase.executeQuery(SQL, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (res.next()) {
                lista.add(res.getString("nome_classe"));
            }
        } catch (Exception e) {
            Variaveis.addNewLog(MenuDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }

        return lista;
    }

    public static Boolean alreadyClass(String classe) {
        String sql = "select * from classe where nome_classe like(?)";
        try {
            Object[] par = {classe};
            ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (res.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }
}
