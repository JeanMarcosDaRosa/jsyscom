/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Empresa;
import beans.Usuario;
import beans.Variaveis;
import funcoes.OperacoesDB;
import funcoes.Util;
import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilitarios.DataBase;
import utilitarios.Extra;
import utilitarios.Utils;

/**
 *
 * @author Jean
 */
public abstract class UsuarioDao {

    public static Boolean getPermition(Usuario user, String classe) {
        String sql = "select "
                + "	r.cd_acao "
                + "from "
                + "	empresa as e, "
                + "	usuario as u, "
                + "	acao as a, "
                + "	classe as c, "
                + "	restricoes as r "
                + "where "
                + "	r.cd_empresa = e.cd_empresa and "
                + "	r.cd_usuario = u.cd_usuario and "
                + "	r.cd_acao = a.cd_acao and "
                + "     a.cd_acao = c.cd_acao and "
                + "	e.cd_empresa = ? and "
                + "	u.cd_usuario = ? and "
                + "	c.nome_classe like(?)";
        try {
            Object[] par = {user.getEmpresa().getCodigo(), user.getCodigoUsuario(), classe};
            ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (res.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return true;
    }

    public static Boolean getPermition(Usuario user, Integer cd_acao) {
        String sql = "select r.cd_acao from empresa as e, "
                + "usuario as u, acao as a, restricoes as r where "
                + "r.cd_empresa = e.cd_empresa and "
                + "r.cd_usuario = u.cd_usuario and "
                + "r.cd_acao = a.cd_acao and "
                + "e.cd_empresa = ? and "
                + "u.cd_usuario = ? and "
                + "a.cd_acao = ?";
        try {
            Object[] par = {user.getEmpresa().getCodigo(), user.getCodigoUsuario(), cd_acao};
            ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (res.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static Integer getAcaoForClass(String classe) {
        if (classe != null) {
            try {
                String sql = "select c.cd_acao from classe as c where c.nome_classe like(?)";
                Object[] par = {classe};
                ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                if (rs.next()) {
                    return rs.getInt("cd_acao");
                }
            } catch (Exception e) {
                Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
            }
        }
        return null;
    }

    public static Boolean setPermition(Usuario user, String classe) {
        if (classe != null && user != null) {
            if (!getPermition(user, classe)) {
                Integer a = getAcaoForClass(classe);
                if (a != null) {
                    String sql = "insert into restricoes(cd_acao, cd_usuario, cd_empresa) values(?,?,?) RETURNING cd_usuario";
                    try {
                        Object[] par = {a, user.getCodigoUsuario(), user.getEmpresa().getCodigo()};
                        ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                        if (rs.next()) {
                            Variaveis.addNewLog("Adicionada Permissão para Usuário: '" + user.getNomeUsuario() + "' da empresa: '" + user.getEmpresa().getFantasia() + "' classe: '" + classe + "'", true, false, null);
                            return true;
                        }
                    } catch (Exception e) {
                        Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
                    }
                }
            } else {
                return true;
            }
        }
        return false;
    }

    public static Boolean setPermition(Usuario user, Integer cd_acao) {
        if (cd_acao != -1 && user != null) {
            if (!getPermition(user, cd_acao)) {
                String sql = "insert into restricoes(cd_acao, cd_usuario, cd_empresa) values(?,?,?) RETURNING cd_usuario";
                try {
                    Object[] par = {cd_acao, user.getCodigoUsuario(), user.getEmpresa().getCodigo()};
                    ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                    if (rs.next()) {
                        Variaveis.addNewLog("Adicionada Permissão para Usuário: '" + user.getNomeUsuario() + "' da empresa: '" + user.getEmpresa().getFantasia() + "' Ação: '" + cd_acao + "'", true, false, null);
                        return true;
                    }
                } catch (Exception e) {
                    Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
                }
            } else {
                return true;
            }
        }
        return false;
    }

    public static boolean copiarPermissoes(Usuario usuarioOrigem, Usuario usuarioDestino) {
        try {
            String SQL = "select copy_restrictions_user_for_user(?,?,?)";
            Object[] par = {usuarioOrigem.getEmpresa().getCodigo(), usuarioOrigem.getCodigoUsuario(), usuarioDestino.getCodigoUsuario()};
            DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            return true;
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static Boolean allRestrictionForUser(Empresa e, Usuario u) {
        try {
            String sql = "select set_all_restrictions_for_user(?, ?)";
            Object[] par = {e.getCodigo(), u.getCodigoUsuario()};
            ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                return (rs.getBoolean(1));
            }
        } catch (Exception a) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(a), false, true, null);
        }
        return false;
    }

    public static boolean limparLogsUsuario(Usuario usuario) {
        try {
            String SQL = "delete from logs where cd_empresa = ? and cd_usuario = ?";
            Object[] par = {usuario.getEmpresa().getCodigo(), usuario.getCodigoUsuario()};
            DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE,
                    Variaveis.getDbType());
            return true;
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static boolean excluirUsuario(Usuario usuario) {
        try {
            String SQL = "SELECT delete_user(?,?)";
            Object[] par = {usuario.getEmpresa().getCodigo(), usuario.getCodigoUsuario()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT,
                    Variaveis.getDbType());
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static void buscaUsuario(Usuario u) {
        try {
            String SQL = "select u.* from usuario as u where u.cd_empresa = ? and u.cd_usuario = ?";
            Object[] par = {u.getEmpresa().getCodigo(), u.getCodigoUsuario()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                populaUsuarioResultSet(rs, u);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static Usuario buscaUsuario(Integer cdUsuario, Empresa empresa) {
        Usuario u = new Usuario(cdUsuario, empresa);
        buscaUsuario(u);
        return u;
    }

    public static void buscaPermitidos(Integer cd_acao, Empresa empresa, List<Usuario> lista) {
        try {
            String SQL = "select u.* from usuario as u where u.cd_empresa = ? and u.cd_usuario "
                    + "in(select cd_usuario from restricoes where cd_empresa = ? and cd_acao = ?) order by u.nome_usuario";
            Object[] par = {empresa.getCodigo(), empresa.getCodigo(), cd_acao};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                Usuario u = new Usuario(empresa);
                populaUsuarioResultSet(rs, u);
                lista.add(u);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static void populaUsuarioResultSet(ResultSet rs, Usuario u) {
        try {
            u.setCodigoUsuario(rs.getInt("cd_usuario"));
            u.setNomeUsuario(rs.getString("nome_usuario"));
            u.setDataCadastro(rs.getDate("data_cadastro_usuario"));
            u.setDescricao(rs.getString("descricao_usuario"));
            u.setCdPessoa((Integer)rs.getObject("cd_funcionario"));
            if (u.getCdPessoa() != null) {
                PessoaDao.buscaFuncionario(u, u.getEmpresa());
            }
            u.setFlagPronto(true);
        } catch (SQLException ex) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
    }

    public static List<Usuario> buscaPermitidos(Integer cd_acao, Empresa empresa) {
        List<Usuario> lista = new ArrayList<>();
        buscaPermitidos(cd_acao, empresa, lista);
        return lista;
    }

    public static void listarUsuarios(Empresa empresa, List<Usuario> lista) {
        try {
            String SQL = "select * from usuario where cd_empresa = ? order by nome_usuario";
            Object[] par = {empresa.getCodigo()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                Usuario u = new Usuario(rs.getInt("cd_usuario"), empresa);
                populaUsuarioResultSet(rs, u);
                lista.add(u);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static List<Usuario> listarUsuarios(Empresa empresa) {
        List<Usuario> lista = new ArrayList<>();
        listarUsuarios(empresa, lista);
        return lista;
    }

    public static Boolean salvarUsuario(Usuario usuarioCad) {
        try {
            if (usuarioCad.getCodigoUsuario() == null) {
                Integer proxUser = NextDaos.nextGenerico(usuarioCad.getEmpresa(), "usuario");
                String SQL = "Insert into usuario (cd_empresa, cd_usuario, cd_funcionario, nome_usuario, senha_usuario, descricao_usuario, "
                        + "data_cadastro_usuario) values(?,?,?,?,?,?,current_date) RETURNING cd_usuario";
                Object[] par = {Variaveis.getEmpresa().getCodigo(), proxUser, usuarioCad.getCdPessoa(), usuarioCad.getNomeUsuario(), usuarioCad.getSenhaUsuario(), usuarioCad.getDescricao().trim()};
                ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                        Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT,
                        Variaveis.getDbType());
                if (rs.next()) {
                    usuarioCad.setCodigoUsuario(rs.getInt("cd_usuario"));
                    return true;
                }
            } else {
                String SQL = "update usuario set cd_empresa = ?, cd_funcionario = ?, nome_usuario = ?, senha_usuario = ?, descricao_usuario = ? "
                        + "where cd_empresa = ? and cd_usuario = ? RETURNING cd_usuario";
                Object[] par = {usuarioCad.getEmpresa().getCodigo(), usuarioCad.getCdPessoa(), usuarioCad.getNomeUsuario(),
                    usuarioCad.getSenhaUsuario(), usuarioCad.getDescricao().trim(), usuarioCad.getEmpresa().getCodigo(), usuarioCad.getCodigoUsuario()};
                ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                        Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT,
                        Variaveis.getDbType());
                if (rs.next()) {
                    usuarioCad.setCodigoUsuario(rs.getInt("cd_usuario"));
                    return true;
                }
            }
        } catch (Exception e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static Usuario login(String nome, String senha, Empresa empresa) {
        try {
            String sql = "select u.cd_usuario from "
                    + "usuario as u where "
                    + "u.cd_empresa = ? and "
                    + "u.nome_usuario = ? and "
                    + "u.senha_usuario = ?";
            Object[] par1 = {empresa.getCodigo(), nome, Extra.getMD5(senha)};
            ResultSet rs1 = DataBase.executeQuery(sql, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs1.next()) {
                Variaveis.setEmpresa(empresa);
                Usuario u = UsuarioDao.buscaUsuario(rs1.getInt("cd_usuario"), Variaveis.getEmpresa());
                Variaveis.setUserSys(u);
                OperacoesDB.gravaLogBanco("SUCESSO", "LOGIN", "Usuario: [" + nome + "], Empresa: [" + Variaveis.getEmpresa().getFantasia() + "]");
                return u;
            } else {
                OperacoesDB.gravaLogBanco("FALHA", "LOGIN", "Usuario: [" + nome + "], Empresa: [" + empresa.getFantasia() + "]");
                return null;
            }
        } catch (SQLException e) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return null;
    }

    public static Boolean setConfiguracoes(Usuario u, Empresa e, Component c) {
        try {
            String sql = "SELECT * FROM config_usuario FULL OUTER JOIN lookandfeels USING(cd_lookandfeels) WHERE config_usuario.cd_empresa = ? and config_usuario.cd_usuario = ?";
            Object[] par = {e.getCodigo(), u.getCodigoUsuario()};
            ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                try {
                    if (rs.getString("NOME_LOOKANDFEELS") != null) {
                        Integer i = rs.getInt("CD_MODULOS");
                        if ((i != null) && (i != 0)) {
                            sql = "SELECT nome_modulos FROM modulos WHERE cd_modulos = ? and ativo_modulos = true";
                            Object[] par2 = {i};
                            ResultSet rs2 = DataBase.executeQuery(sql, par2, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                            if (rs2.next()) {
                                Util.lookandfeel(rs.getString("NOME_LOOKANDFEELS"), rs2.getString("nome_modulos"), c);
                            } else {
                                Variaveis.addNewLog("Modulo que contém o lookandfeel não foi encontrado ou esta desativo!", false, true, null);
                            }
                        } else {
                            Util.lookandfeel(rs.getString("NOME_LOOKANDFEELS"), c);
                        }
                    }
                    if (rs.getString("NOME_MENU_CONFIG") != null) {
                        Variaveis.setNomeMenu(rs.getString("NOME_MENU_CONFIG"));
                    }
                    if (rs.getString("GRAVA_LOGS_CONFIG") != null) {
                        Variaveis.setGravaLog(rs.getBoolean("GRAVA_LOGS_CONFIG"));
                    }
                    if (rs.getString("DISTANCIA_FRAME_CONFIG") != null) {
                        Variaveis.setDistanciaFrames(rs.getInt("DISTANCIA_FRAME_CONFIG"));
                    }
                    Variaveis.setIDImage(rs.getInt("CD_WALLPAPER"));
                } catch (Exception a) {
                    Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(a), false, true, null);
                    OperacoesDB.gravaLogBanco("FALHA", "CONFIGURACAO", "ERRO AO APLICAR CONFIGURACAO");
                }
            }

        } catch (SQLException ex) {
            Variaveis.addNewLog(UsuarioDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
        return false;
    }
}
