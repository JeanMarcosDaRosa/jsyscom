/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Conexao;
import beans.Empresa;
import beans.Imagem;
import beans.Variaveis;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;
import utilitarios.DataBase;
import utilitarios.Utils;

/**
 *
 * @author root
 */
public class ImagemDao {

    public static Imagem getImagemBanco(String sql) throws IOException {
        try {
            Connection conexao = Conexao.getInstance().getCon(Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), Variaveis.getDbType());
            Boolean autoCommit = false;
            try {
                autoCommit = conexao.getAutoCommit();
                conexao.setAutoCommit(false);
            } catch (SQLException ex) {
                Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
            }
            try {
                BufferedImage i = null;
                Imagem img = null;
                //Get the Large Object Manager to perform operations with
                LargeObjectManager lobj = ((org.postgresql.PGConnection) conexao).getLargeObjectAPI();
                PreparedStatement ps = (PreparedStatement) conexao.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    //Open the large object for reading
                    int oid = rs.getInt("blob");
                    if (oid == 0) {
                        break;
                    }
                    LargeObject obj = lobj.open(oid, LargeObjectManager.READ);
                    //Read the data
                    byte buf[] = new byte[obj.size()];
                    obj.read(buf, 0, obj.size());
                    //Do something with the data read here
                    ByteArrayInputStream ms = new ByteArrayInputStream(buf);
                    i = ImageIO.read(ms);
                    ImageIcon image = new ImageIcon(i);
                    img = new Imagem(image);
                    img.setCodImagem(rs.getInt("cod"));
                    img.setMd5Imagem(rs.getString("md5"));
                    img.setNomeImagem(rs.getString("nome"));
                    //Close the object
                    obj.close();
                }
                rs.close();
                ps.close();
                //Finally, commit the transaction.
                conexao.commit();
                return img;
            } catch (SQLException ex) {
                Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
            } finally {
                try {
                    conexao.setAutoCommit(autoCommit);
                } catch (SQLException ex) {
                    Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, false, null);
                }
            }

        } catch (SQLException ex) {
            Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
        return null;
    }


    public static ArrayList<Imagem> getListaImagemBanco(String sql, Integer posicao, Integer qtde) throws IOException {
        try {
            Connection conexao = Conexao.getInstance().getCon(Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), Variaveis.getDbType());
            Boolean autoCommit = false;
            try {
                autoCommit = conexao.getAutoCommit();
                conexao.setAutoCommit(false);
            } catch (SQLException ex) {
                Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
            }
            try {
                ArrayList<Imagem> lista = new ArrayList<Imagem>();
                //Get the Large Object Manager to perform operations with
                LargeObjectManager lobj = ((org.postgresql.PGConnection) conexao).getLargeObjectAPI();
                PreparedStatement ps = (PreparedStatement) conexao.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int count = 0;
                int seq = 0;
                while ((rs.next()) && (count < qtde)) {
                    if (seq >= posicao) {
                        BufferedImage i = null;
                        //Open the large object for reading
                        int oid = rs.getInt("blob");
                        if (oid == 0) {
                            break;
                        }
                        LargeObject obj = lobj.open(oid, LargeObjectManager.READ);
                        //Read the data
                        byte buf[] = new byte[obj.size()];
                        obj.read(buf, 0, obj.size());
                        //Do something with the data read here
                        ByteArrayInputStream ms = new ByteArrayInputStream(buf);
                        i = ImageIO.read(ms);
                        ImageIcon image = new ImageIcon(i);
                        Imagem img = new Imagem(image);
                        img.setCodImagem(rs.getInt("cod"));
                        img.setMd5Imagem(rs.getString("md5"));
                        img.setNomeImagem(rs.getString("nome"));
                        obj.close();
                        count++;
                        lista.add(img);
                    }
                    seq++;
                }
                rs.close();
                ps.close();
                //Finally, commit the transaction.
                conexao.commit();
                return lista;
            } catch (SQLException ex) {
                Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
            } finally {
                try {
                    conexao.setAutoCommit(autoCommit);
                } catch (SQLException ex) {
                    Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, false, null);
                }
            }

        } catch (SQLException ex) {
            Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, false, null);
        }
        return null;
    }

    @SuppressWarnings("empty-statement")
    public static Integer setImagemBanco(String sql, File file) throws IOException {
        try {
            Connection conexao = Conexao.getInstance().getCon(Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), Variaveis.getDbType());
            Boolean autoCommit = false;
            try {
                autoCommit = conexao.getAutoCommit();
                conexao.setAutoCommit(false);
            } catch (SQLException ex) {
                Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, false, null);
            }
            try {
                LargeObjectManager lobj = ((org.postgresql.PGConnection) conexao).getLargeObjectAPI();
                long oid = lobj.createLO(LargeObjectManager.READ | LargeObjectManager.WRITE);
                LargeObject obj = lobj.open(oid, LargeObjectManager.WRITE);
                FileInputStream fis = new FileInputStream(file);
                byte buf[] = new byte[2048];
                int s, tl = 0;
                while ((s = fis.read(buf, 0, 2048)) > 0) {
                    obj.write(buf, 0, s);
                    tl += s;
                }
                obj.close();
                PreparedStatement ps = conexao.prepareStatement(sql);
                ps.setLong(1, oid);
                ResultSet rs = ps.executeQuery();
                Integer ret = null;
                if (rs.next()) {
                    ret = rs.getInt(1);
                }
                ps.close();
                fis.close();
                ps.close();
                conexao.commit();
                return ret;
            } catch (SQLException ex) {
                Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);;
            } finally {
                try {
                    conexao.setAutoCommit(autoCommit);
                } catch (SQLException ex) {
                    Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, false, null);
                }
            }

        } catch (SQLException ex) {
            Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, false, null);
        }
        return null;
    }

    public static Imagem getWallpaper(Integer cd_imagem) throws IOException {
        return getImagemBanco("SELECT cd_wallpaper as cod, blob_wallpaper as blob, md5_wallpaper as md5, nome_wallpaper as nome FROM wallpaper WHERE cd_empresa = " + Variaveis.getEmpresa().getCodigo() + " and cd_wallpaper = " + cd_imagem);
    }

    public static Integer wallpaperInDB(Empresa empresa, Imagem imagem) {
        try {
            String sql = "select * from wallpaper where cd_empresa = ? and md5_wallpaper like(?)";
            Object[] par = {empresa.getCodigo(), imagem.getMd5Imagem()};
            ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (res.next()) {
                return res.getInt("cd_wallpaper");
            }
        } catch (Exception e) {
            Variaveis.addNewLog(ImagemDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return null;
    }
}
