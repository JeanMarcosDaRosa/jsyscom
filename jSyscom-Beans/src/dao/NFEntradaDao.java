/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Empresa;
import beans.NFEntrada;
import beans.NFFatura;
import beans.NFItemProd;
import beans.Produto;
import beans.Variaveis;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utilitarios.DataBase;
import utilitarios.Utils;

/**
 *
 * @author jean-marcos
 */
public class NFEntradaDao {

    public static NFEntrada buscaNFEntrada(Integer codigo, Empresa empresa, Boolean getImage) {
        NFEntrada nf = new NFEntrada(codigo);
        buscaNFEntrada(nf, empresa, getImage);
        return nf;
    }

    public static void buscaNFEntrada(NFEntrada nfEntrada, Empresa empresa, Boolean getImage) {
        try {
            String SQL = "select nf.* from mov_nf as nf where nf.cd_empresa = ? and tipo_mov_nf = 0 and nf.nro_mov_nf = ?";
            Object[] par = {empresa.getCodigo(), nfEntrada.getNumeroNf()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                populaNFEntradaResultSet(rs, nfEntrada, empresa, getImage);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(NFEntradaDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static ArrayList<NFEntrada> buscaNFEntradaProduto(Produto p, Empresa empresa, Boolean getImage) {
        ArrayList<NFEntrada> lista = new ArrayList<>();
        try {
            String SQL = "select nf.* from mov_nf as nf, mov_nf_items as i, produto as p "
                    + "where nf.cd_empresa = i.cd_empresa and "
                    + "nf.tipo_mov_nf = i.tipo_mov_nf and "
                    + "nf.nro_mov_nf = i.nro_mov_nf and "
                    + "nf.cd_empresa = p.cd_empresa and "
                    + "i.cd_produto = p.cd_produto and "
                    + "nf.cd_empresa = ? and nf.tipo_mov_nf = 0 and i.cd_produto = ?";
            Object[] par = {empresa.getCodigo(), p.getCodProduto()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                NFEntrada nf = new NFEntrada();
                populaNFEntradaResultSet(rs, nf, empresa, getImage);
                lista.add(nf);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(NFEntradaDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return lista;
    }

    public static void populaNFEntradaResultSet(ResultSet rs, NFEntrada nf, Empresa empresa, Boolean getImage) {
        try {
            nf.setNumeroNf(rs.getInt("nro_mov_nf"));
            nf.setSerieNf(rs.getInt("serie_mov_nf"));
            nf.setNaturezaOperacao(rs.getString("nat_op_mov_nf"));
            nf.setChaveAcesso(rs.getString("chave_acesso_mov_nf"));
            nf.setProtocoloAutorizacao(rs.getString("prot_auth_mov_nf"));
            nf.setDataEmissao(rs.getDate("dt_emi_mov_nf"));
            nf.setDataEntrada(rs.getDate("dt_entrada_mov_nf"));
            nf.setTipoFrete(rs.getString("tipo_frete_mov_nf"));
            nf.setBaseIcms(rs.getDouble("base_icms_mov_nf"));
            nf.setValorIcms(rs.getDouble("valor_icms_mov_nf"));
            nf.setBaseIcmsSubs(rs.getDouble("base_icms_subs_mov_nf"));
            nf.setValorIcmsSubs(rs.getDouble("valor_icms_subs_mov_nf"));
            nf.setValorTotProd(rs.getDouble("valor_tot_prod_mov_nf"));
            nf.setValorFrete(rs.getDouble("valor_frete_mov_nf"));
            nf.setValorSeguroFrete(rs.getDouble("valor_seguro_frete_mov_nf"));
            nf.setDesconto(rs.getDouble("desconto_mov_nf"));
            nf.setDemaisDespesas(rs.getDouble("demais_despesas_mov_nf"));
            nf.setValorIpi(rs.getDouble("valor_ipi_mov_nf"));
            nf.setValorTotNota(rs.getDouble("valor_tot_mov_nf"));
            nf.setQuantidadeCarga(rs.getDouble("qtd_carga_mov_nf"));
            nf.setPesoBrutoCarga(rs.getDouble("peso_bruto_carga_mov_nf"));
            nf.setPesoLiqCarga(rs.getDouble("peso_liq_carga_mov_nf"));
            nf.setIssqnvlrTotServicos(rs.getDouble("issqn_vlr_tot_mov_nf"));
            nf.setIssqnBaseCalc(rs.getDouble("issqn_base_mov_nf"));
            nf.setIssqnValor(rs.getDouble("issqn_vlr_mov_nf"));
            nf.setContaAntt(rs.getString("conta_antt_mov_nf"));
            nf.setPlacaVeiculo(rs.getString("placa_mov_nf"));
            nf.setUfPlacaVeiculo(rs.getString("uf_placa_mov_nf"));
            nf.setEspecieCarga(rs.getString("especie_carga_mov_nf"));
            nf.setMarcaCarga(rs.getString("marca_carga_mov_nf"));
            nf.setNumeracaoCarga(rs.getString("nro_carga_mov_nf"));
            nf.setDadosAdicionaisNf(rs.getString("dados_adicionais_mov_nf"));
            nf.setFornecedor(PessoaDao.buscaFornecedor(rs.getInt("cd_fornecedor"), empresa));
            nf.setEnderecoFornecedor(PessoaDao.getEnderecoPessoa(nf.getFornecedor(), rs.getString("cep_logradouro_fornecedor"), rs.getInt("numero_endereco_fornecedor"), empresa));
            nf.setTransportadora(PessoaDao.buscaTransportadora(rs.getInt("cd_transportadora"), empresa));
            nf.setEnderecoTransp(PessoaDao.getEnderecoPessoa(nf.getTransportadora(), rs.getString("cep_logradouro_transp"), rs.getInt("numero_endereco_transp"), empresa));
            nf.setIssqnInscMun(rs.getString("issqn_insc_mun_mov_nf"));
            buscaItensNF(nf, empresa, getImage);
            buscaFaturasNF(nf, empresa);
            nf.setFlagPronto(true);
        } catch (SQLException ex) {
            Variaveis.addNewLog(NFEntradaDao.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
    }

    public static void buscaFaturasNF(NFEntrada nf, Empresa empresa) {
        nf.getFaturas().clear();
        try {
            String SQL = "select * from mov_nf_fatura where cd_empresa = ? and tipo_mov_nf = 0 and nro_mov_nf = ? order by numero_mov_nf_fatura";
            Object[] par = {empresa.getCodigo(), nf.getNumeroNf()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                NFFatura o = new NFFatura();
                o.setNfEntrada(nf);
                o.setNumero(rs.getString("numero_mov_nf_fatura"));
                o.setValor(rs.getDouble("valor_mov_nf_fatura"));
                o.setVencimento(rs.getDate("dt_venc_mov_nf_fatura"));
                nf.getFaturas().add(o);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(NFEntradaDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static void buscaItensNF(NFEntrada nf, Empresa empresa, Boolean getImage) {
        nf.getItemsProd().clear();
        try {
            String SQL = "select * from mov_nf_items where cd_empresa = ? and tipo_mov_nf = 0 and nro_mov_nf = ? order by cd_mov_nf_items";
            Object[] par = {empresa.getCodigo(), nf.getNumeroNf()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                NFItemProd o = new NFItemProd();
                o.setNfEntrada(nf);
                o.setAliquota(rs.getDouble("aliq_mov_nf_items"));
                o.setQuantidade(rs.getDouble("qtd_mov_nf_items"));
                o.setValorICMS(rs.getDouble("vlr_icms_mov_nf_items"));
                o.setValorTotal(rs.getDouble("vlr_tot_mov_nf_items"));
                o.setValorUnitario(rs.getDouble("vlr_un_mov_nf_items"));
                o.setProduto(ProdutoDao.buscaProduto(rs.getInt("cd_produto"), empresa, getImage));
                nf.getItemsProd().add(o);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(NFEntradaDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static void excluiAllFaturas(NFEntrada nf, Empresa empresa) {
        try {
            String SQL = "delete from mov_nf_fatura where cd_empresa = ? and tipo_mov_nf = 0 and nro_mov_nf = ?";
            Object[] par = {empresa.getCodigo(), nf.getNumeroNf()};
            DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
        } catch (Exception e) {
            Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static void salvaAllFaturas(NFEntrada nf, Empresa empresa) {
        try {
            excluiAllFaturas(nf, empresa);
            String SQL = "INSERT INTO mov_nf_fatura (cd_empresa, tipo_mov_nf, nro_mov_nf, numero_mov_nf_fatura, "
                    + "dt_venc_mov_nf_fatura, valor_mov_nf_fatura) VALUES(?,0,?,?,?,?)";
            for (NFFatura o : nf.getFaturas()) {
                Object[] par = {empresa.getCodigo(), nf.getNumeroNf(), o.getNumero(), o.getVencimento(), o.getValor()};
                DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                        Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
            }
        } catch (Exception e) {
            Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static void excluiAllItemsNF(NFEntrada nf, Empresa empresa) {
        try {
            String SQL = "delete from mov_nf_items where cd_empresa = ? and tipo_mov_nf = 0 and nro_mov_nf = ?";
            Object[] par = {empresa.getCodigo(), nf.getNumeroNf()};
            DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
        } catch (Exception e) {
            Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static void salvaAllItemsNF(NFEntrada nf, Empresa empresa) {
        try {
            excluiAllItemsNF(nf, empresa);
            String SQL = "INSERT INTO mov_nf_items (cd_empresa, tipo_mov_nf, nro_mov_nf, cd_produto, "
                    + "qtd_mov_nf_items, vlr_un_mov_nf_items, vlr_tot_mov_nf_items, vlr_icms_mov_nf_items, aliq_mov_nf_items) "
                    + "VALUES(?,0,?,?,?,?,?,?,?)";
            for (NFItemProd o : nf.getItemsProd()) {
                Object[] par = {empresa.getCodigo(), nf.getNumeroNf(), o.getProduto().getCodProduto(), o.getQuantidade(), o.getValorUnitario(),
                    o.getValorTotal(), o.getValorICMS(), o.getAliquota()};
                DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                        Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
            }
        } catch (Exception e) {
            Variaveis.addNewLog(OrdemServicoDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }

    public static Boolean salvarNFEntrada(NFEntrada nf, Empresa empresa) {
        try {
            if (existeNFEntrada(nf, empresa)) {
                String SQL = "UPDATE mov_nf SET "
                        + "cd_fornecedor = ?, "
                        + "cep_logradouro_fornecedor = ?, "
                        + "numero_endereco_fornecedor = ?, "
                        + "cd_transportadora = ?, "
                        + "cep_logradouro_transp = ?, "
                        + "numero_endereco_transp = ?, "
                        + "serie_mov_nf = ?, "
                        + "nat_op_mov_nf = ?, "
                        + "chave_acesso_mov_nf = ?, "
                        + "prot_auth_mov_nf = ?, "
                        + "dt_emi_mov_nf = ?, "
                        + "dt_entrada_mov_nf = ?, "
                        + "tipo_frete_mov_nf = ?, "
                        + "base_icms_mov_nf = ?, "
                        + "valor_icms_mov_nf = ?, "
                        + "base_icms_subs_mov_nf = ?, "
                        + "valor_icms_subs_mov_nf = ?, "
                        + "valor_tot_prod_mov_nf = ?, "
                        + "valor_frete_mov_nf = ?, "
                        + "valor_seguro_frete_mov_nf = ?, "
                        + "desconto_mov_nf = ?, "
                        + "demais_despesas_mov_nf = ?, "
                        + "valor_ipi_mov_nf = ?, "
                        + "valor_tot_mov_nf = ?, "
                        + "issqn_vlr_tot_mov_nf = ?, "
                        + "issqn_base_mov_nf = ?, "
                        + "issqn_vlr_mov_nf = ?, "
                        + "issqn_insc_mun_mov_nf = ?, "
                        + "conta_antt_mov_nf = ?, "
                        + "peso_bruto_carga_mov_nf = ?, "
                        + "peso_liq_carga_mov_nf = ?, "
                        + "qtd_carga_mov_nf = ?, "
                        + "placa_mov_nf = ?, "
                        + "uf_placa_mov_nf = ?, "
                        + "especie_carga_mov_nf = ?, "
                        + "marca_carga_mov_nf = ?, "
                        + "nro_carga_mov_nf = ?, "
                        + "dados_adicionais_mov_nf = ? "
                        + "where cd_empresa = ? and tipo_mov_nf = 0 and nro_mov_nf = ?";
                Object[] par = {
                    nf.getFornecedor().getCdPessoa(), nf.getEnderecoFornecedor().getCepLogradouro(),
                    nf.getEnderecoFornecedor().getNumero(), nf.getTransportadora().getCdPessoa(),
                    nf.getEnderecoTransp().getCepLogradouro(), nf.getEnderecoTransp().getNumero(),
                    nf.getSerieNf(), nf.getNaturezaOperacao(), nf.getChaveAcesso(), nf.getProtocoloAutorizacao(),
                    nf.getDataEmissao(), nf.getDataEntrada(), nf.getTipoFrete(), nf.getBaseIcms(), nf.getValorIcms(),
                    nf.getBaseIcmsSubs(), nf.getValorIcmsSubs(), nf.getValorTotProd(), nf.getValorFrete(),
                    nf.getValorSeguroFrete(), nf.getDesconto(), nf.getDemaisDespesas(), nf.getValorIpi(),
                    nf.getValorTotNota(), nf.getIssqnvlrTotServicos(), nf.getIssqnBaseCalc(), nf.getIssqnValor(),
                    nf.getIssqnInscMun(), nf.getContaAntt(), nf.getPesoBrutoCarga(), nf.getPesoLiqCarga(),
                    nf.getQuantidadeCarga(), nf.getPlacaVeiculo(), nf.getUfPlacaVeiculo(), nf.getEspecieCarga(),
                    nf.getMarcaCarga(), nf.getNumeracaoCarga(), nf.getDadosAdicionaisNf(),
                    empresa.getCodigo(), nf.getNumeroNf()};
                DataBase.executeQuery(SQL, par,
                        Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(),
                        Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE,
                        Variaveis.getDbType());
            } else {
                String SQL = "INSERT INTO mov_nf ("
                        + "cd_empresa, "
                        + "tipo_mov_nf, "
                        + "nro_mov_nf, "
                        + "cd_fornecedor, "
                        + "cep_logradouro_fornecedor, "
                        + "numero_endereco_fornecedor, "
                        + "cd_transportadora, "
                        + "cep_logradouro_transp, "
                        + "numero_endereco_transp, "
                        + "serie_mov_nf, "
                        + "nat_op_mov_nf, "
                        + "chave_acesso_mov_nf, "
                        + "prot_auth_mov_nf, "
                        + "dt_emi_mov_nf, "
                        + "dt_entrada_mov_nf, "
                        + "tipo_frete_mov_nf, "
                        + "base_icms_mov_nf, "
                        + "valor_icms_mov_nf, "
                        + "base_icms_subs_mov_nf, "
                        + "valor_icms_subs_mov_nf, "
                        + "valor_tot_prod_mov_nf, "
                        + "valor_frete_mov_nf, "
                        + "valor_seguro_frete_mov_nf, "
                        + "desconto_mov_nf, "
                        + "demais_despesas_mov_nf, "
                        + "valor_ipi_mov_nf, "
                        + "valor_tot_mov_nf, "
                        + "issqn_vlr_tot_mov_nf, "
                        + "issqn_base_mov_nf, "
                        + "issqn_vlr_mov_nf, "
                        + "issqn_insc_mun_mov_nf, "
                        + "conta_antt_mov_nf, "
                        + "peso_bruto_carga_mov_nf, "
                        + "peso_liq_carga_mov_nf, "
                        + "qtd_carga_mov_nf, "
                        + "placa_mov_nf, "
                        + "uf_placa_mov_nf, "
                        + "especie_carga_mov_nf, "
                        + "marca_carga_mov_nf, "
                        + "nro_carga_mov_nf, "
                        + "dados_adicionais_mov_nf) "
                        + "VALUES (?,0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                Object[] par = {empresa.getCodigo(), nf.getNumeroNf(),
                    nf.getFornecedor().getCdPessoa(), nf.getEnderecoFornecedor().getCepLogradouro(),
                    nf.getEnderecoFornecedor().getNumero(), nf.getTransportadora().getCdPessoa(),
                    nf.getEnderecoTransp().getCepLogradouro(), nf.getEnderecoTransp().getNumero(),
                    nf.getSerieNf(), nf.getNaturezaOperacao(), nf.getChaveAcesso(), nf.getProtocoloAutorizacao(),
                    nf.getDataEmissao(), nf.getDataEntrada(), nf.getTipoFrete(), nf.getBaseIcms(), nf.getValorIcms(),
                    nf.getBaseIcmsSubs(), nf.getValorIcmsSubs(), nf.getValorTotProd(), nf.getValorFrete(),
                    nf.getValorSeguroFrete(), nf.getDesconto(), nf.getDemaisDespesas(), nf.getValorIpi(),
                    nf.getValorTotNota(), nf.getIssqnvlrTotServicos(), nf.getIssqnBaseCalc(), nf.getIssqnValor(),
                    nf.getIssqnInscMun(), nf.getContaAntt(), nf.getPesoBrutoCarga(), nf.getPesoLiqCarga(),
                    nf.getQuantidadeCarga(), nf.getPlacaVeiculo(), nf.getUfPlacaVeiculo(), nf.getEspecieCarga(),
                    nf.getMarcaCarga(), nf.getNumeracaoCarga(), nf.getDadosAdicionaisNf()
                };
                DataBase.executeQuery(SQL, par,
                        Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(),
                        Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE,
                        Variaveis.getDbType());
            }
            salvaAllFaturas(nf, empresa);
            buscaFaturasNF(nf, empresa);
            salvaAllItemsNF(nf, empresa);
            buscaItensNF(nf, empresa, true);
            return true;
        } catch (Exception e) {
            Variaveis.addNewLog(NFEntradaDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static Boolean existeNFEntrada(NFEntrada nf, Empresa empresa) {
        try {
            String SQL = "select * from mov_nf where cd_empresa = ? and tipo_mov_nf = 0 and nro_mov_nf = ?";
            Object[] par = {empresa.getCodigo(), nf.getNumeroNf()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            Variaveis.addNewLog(NFEntradaDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static Boolean excluirNFEntrada(NFEntrada nf, Empresa empresa) {
        try {
            String SQL = "delete from mov_nf as nf where nf.cd_empresa = ? and tipo_mov_nf = 0 and nf.nro_mov_nf = ?";
            Object[] par = {empresa.getCodigo(), nf.getNumeroNf()};
            DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(),
                    Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE,
                    Variaveis.getDbType());
            return true;
        } catch (Exception e) {
            Variaveis.addNewLog(NFEntradaDao.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }
}
