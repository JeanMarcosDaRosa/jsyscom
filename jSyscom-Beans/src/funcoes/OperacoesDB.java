/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package funcoes;

import beans.*;
import java.io.*;
import java.sql.*;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import utilitarios.*;

/**
 *
 * @author Jean
 */
public class OperacoesDB {

    private static DefaultTableModel modeloVaziu = new DefaultTableModel(0, 0);

    public static Boolean gravaLogBanco(String titulo, String Operacao, String log) {
        try {
            String sql = "INSERT INTO logs (data_logs, hora_logs, titulo_logs, operacao_logs, descricao_logs, cd_usuario, cd_empresa, versao_js_logs) "
                    + "VALUES (current_date, current_time, ?, ?, ?, ?, ?, ?)";
            if (Variaveis.getUserSys() == null) {
                sql = "INSERT INTO logs (data_logs, hora_logs, titulo_logs, operacao_logs, descricao_logs, versao_js_logs) "
                        + "VALUES (current_date, current_time, ?, ?, ?, ?)";
                Object[] par = {titulo, Operacao, log, Variaveis.getVersao()};
                DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                return true;
            } else {
                Object[] par = {titulo, Operacao, log, Variaveis.getUserSys().getCodigoUsuario(), Variaveis.getEmpresa().getCodigo(), Variaveis.getVersao()};
                DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                return true;
            }

        } catch (Exception e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
            return false;
        }
    }

    public static boolean atualizaModulos(Empresa empresa) {
        Variaveis.addNewLog("Atualizando...", false, false, null);
        String sql = "select * from config_empresa where cd_empresa = ?";
        Object[] par1 = {empresa.getCodigo()};
        try {
            ResultSet res1 = DataBase.executeQuery(sql, par1, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (res1.next()) {
                if (res1.getBoolean("atu_mod") == true) {
                    sql = "select * from modulos where ativo_modulos = true";
                    ResultSet res2 = DataBase.executeQuery(sql, null, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
                    while (res2.next()) {
                        String md5db = res2.getString("MD5_modulos").toUpperCase().trim();
                        String md5lc = Arquivo.getMD5Checksum(Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim());
                        Variaveis.addNewLog("md5lc:" + md5lc, false, false, null);
                        Variaveis.addNewLog("md5db:" + md5db, false, false, null);
                        if (!md5lc.equals(md5db) || ("".equals(md5lc))) {
                            Variaveis.addNewLog("Hash de arquivos diferentes!", false, false, null);
                            Blob blob = res2.getBlob("bind_binarys");
                            InputStream input = blob.getBinaryStream();
                            OutputStream output = new FileOutputStream(Arquivo.getPathBase() + "tmpAtu.jar");
                            try {
                                if (input != null) {
                                    Variaveis.addNewLog("Aplicando atualizações...", false, false, null);
                                    byte[] rb = new byte[1024];
                                    int ch;
                                    while ((ch = input.read(rb)) != -1) {
                                        output.write(rb, 0, ch);
                                    }
                                    Variaveis.addNewLog("Apagando arquivo [" + res2.getString("nome_modulos").trim() + "]", false, false, null);
                                    if (Arquivo.DeleteFile(Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim())) {
                                        Variaveis.addNewLog("Arquivo apagado com sucesso!", false, false, null);
                                    } else {
                                        Extra.Informacao(Variaveis.getFormularioPrincipal(), "Falha ao apagar arquivo de módulos antigo, favor apaga-lo\n"
                                                + "manualmente, para DEPOIS clicar em OK!\n"
                                                + "Arquivo para ser apagado:\n"
                                                + Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim());
                                        Variaveis.addNewLog("Falha ao apagar arquivo!", false, false, null);
                                    }
                                    Arquivo.MoveFile(Arquivo.getPathBase() + "tmpAtu.jar", Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim());
                                    Variaveis.addNewLog("Movendo atualização para [" + Arquivo.getPathBase() + "lib" + Arquivo.getBarra() + res2.getString("nome_modulos").trim() + "]", false, false, null);
                                }
                            } catch (IOException | SQLException e) {
                                Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
                            } finally {
                                if (input != null) {
                                    input.close();
                                }
                                if (output != null) {
                                    output.close();
                                }
                            }
                        }
                    }
                }
            }
            Variaveis.addNewLog("Atualização de modulos do sistema completa!", false, false, null);
            return true;
        } catch (Exception e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
            Variaveis.addNewLog("Falha ao atualizar modulos do sistema!", true, true, null);
            return false;
        }
    }

    public static Integer countQueryResult(String sql) {
        int c = 0;
        try {
            ResultSet res = DataBase.executeQuery(sql, null, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (res.next()) {
                c++;
            }
        } catch (Exception e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return c;
    }

    public static void analisaAutorun(Usuario usuario, Boolean start) {
        String sql = "SELECT * FROM autorun FULL OUTER JOIN acao USING(cd_acao) WHERE "
                + "autorun.cd_empresa = ? and autorun.cd_usuario = ? and autorun.somente_inicio = ? "
                + "and autorun.ativo_autorun = true order by cd_autorun";
        try {
            Object[] par = {usuario.getEmpresa().getCodigo(), usuario.getCodigoUsuario(), start};
            ResultSet res = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            sql = "UPDATE autorun SET ativo_autorun = ? "
                    + "where cd_empresa = ? and cd_usuario = ? and cd_autorun = ?";
            while (res.next()) {
                Object[] par1 = {start, usuario.getEmpresa().getCodigo(), usuario.getCodigoUsuario(), res.getInt("cd_autorun")};
                DataBase.executeQuery(sql, par1, Variaveis.getIpServ(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                if (res.getInt("cd_acao") != 0) {
                    Util.processaAcao(res.getInt("cd_acao"), res.getString("descricao_autorun"));
                }
                if (res.getInt("nro_operacao") != 0) {
                    Operacoes.executa(res.getInt("nro_operacao"), res.getString("descricao_autorun"));
                }
            }
        } catch (Exception e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        verificaAvisos(Variaveis.getUserSys(), true);
    }

    public static void verificaAvisos(Usuario usuario, Boolean forcar) {
        String SQL = "select * from aviso where cd_empresa = ? and cd_usuario = ?";
        try {
            Object[] par = {usuario.getEmpresa().getCodigo(), usuario.getCodigoUsuario()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            while (rs.next()) {
                if (forcar) {
                    if (rs.getBoolean("insistente_aviso")) {
                        Variaveis.setAviso(rs.getString("descricao_aviso"));
                        Variaveis.setCdAviso(rs.getInt("cd_aviso"));
                        Util.execucaoDiretaClass("formularios.FI_SysAvisos", "Modulos-jSyscom.jar", "Aviso", new OpcoesTela(true, false, false, true, true, false), usuario);
                        Object[] p = {rs.getInt("cd_aviso")};
                        SQL = "UPDATE aviso SET visualizado_aviso = true WHERE cd_aviso = ?";
                        DataBase.executeQuery(SQL, p, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                    }
                } else {
                    if (rs.getBoolean("insistente_aviso") && !rs.getBoolean("visualizado_aviso")) {
                        Variaveis.setAviso(rs.getString("descricao_aviso"));
                        Variaveis.setCdAviso(rs.getInt("cd_aviso"));
                        Util.execucaoDiretaClass("formularios.FI_SysAvisos", "Modulos-jSyscom.jar", "Aviso", new OpcoesTela(true, false, false, true, true, false), usuario);
                        Object[] p = {rs.getInt("cd_aviso")};
                        SQL = "UPDATE aviso SET visualizado_aviso = true WHERE cd_aviso = ?";
                        DataBase.executeQuery(SQL, p, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
                    }
                }
            }
        } catch (SQLException ex) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
    }

    public static Boolean validaLicenca(Empresa empresa) {
        try {
            String sql = "SELECT valida_licenca(?)";
            Object[] par = {empresa.getCodigo()};
            ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static Boolean aplicaLicenca(String chave) {
        try {
            String c = Extra.CriptoDecritoSenhaInteiro(Extra.DECRIPTOGRAFA, chave);
            String sql = "SELECT aplica_licenca(?, ?)";
            Object[] par = {Integer.parseInt(c.substring(0, 4)), new Date(Long.parseLong(c.substring(4, c.length())))};
            ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                return (rs.getBoolean(1));
            }
        } catch (NumberFormatException | SQLException e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return false;
    }

    public static Date getDataExpLic(Empresa empresa) {
        try {
            String sql = "SELECT * FROM licenca WHERE cd_empresa = ? order by data_expira desc";
            Object[] par = {empresa.getCodigo()};
            ResultSet rs = DataBase.executeQuery(sql, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                if (rs.getDate("data_expira") != null) {
                    return rs.getDate("data_expira");
                }
            }
        } catch (Exception e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return null;
    }

    public static Integer getDiasRestantesLic(Empresa empresa) {
        try {
            Date ex = getDataExpLic(empresa);
            if (ex != null) {
                Date hoje = new Date(System.currentTimeMillis());
                if (Format.DateToLong(hoje) <= (Format.DateToLong(ex))) {
                    return Extra.dataDiff(hoje, ex);
                } else {
                    return Extra.dataDiff(ex, hoje);
                }
            }

        } catch (Exception e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
        return null;
    }

    public static void notInsistenteAvisos(Integer cd_aviso) {
        String SQL = "update aviso set insistente_aviso = false where cd_aviso = ?";
        try {
            Object[] par = {cd_aviso};
            DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
        } catch (Exception ex) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
    }

    public static void visualizadoAvisos(Integer cd_aviso) {
        String SQL = "update aviso set visualizado_aviso = true where cd_aviso = ?";
        try {
            Object[] par = {cd_aviso};
            DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.EXECUTE, Variaveis.getDbType());
        } catch (Exception ex) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
    }

    public static String getTitleCustom(Empresa empresa) {
        String SQL = "select titulo_software from config_empresa where cd_empresa = ?";
        String title = "";
        try {
            Object[] par = {empresa.getCodigo()};
            ResultSet rs = DataBase.executeQuery(SQL, par, Variaveis.getIpServ(), Variaveis.getDbPort(), Variaveis.getDbLocal(), Variaveis.getDbUser(), Variaveis.getDbPass(), DataBase.RESULT, Variaveis.getDbType());
            if (rs.next()) {
                title = rs.getString("titulo_software");
            }
            title = title.replaceFirst("@title", "Sistema de Automação Comercial jSyscom");
            title = title.replaceFirst("@company", Variaveis.getEmpresa().getFantasia());
            title = title.replaceFirst("@version", Variaveis.getVersao());
        } catch (SQLException ex) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(ex), false, true, null);
        }
        return title;
    }

    public static void executeSQL(Integer db, String SQL, String ip, Integer port, String local, String user, String pass, JTable table) {
        try {
            if (SQL.trim().toUpperCase().startsWith("SELECT")) {
                ResultSet rs = DataBase.executeQuery(SQL, null, ip, port, local, user, pass, DataBase.RESULT, db);
                table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                if (rs != null) {
                    table.setModel(Format.getResultSetTableModal(rs));
                } else {
                    table.setModel(modeloVaziu);
                }
            } else {
                DataBase.executeQuery(SQL, null, ip, port, local, user, pass, DataBase.EXECUTE, db);
                table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                table.setModel(modeloVaziu);
            }
        } catch (Exception e) {
            Variaveis.addNewLog(OperacoesDB.class.getName() + ":\n" + Utils.getStackTrace(e), false, true, null);
        }
    }
}
