/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package funcoes;

/**
 *
 * @author Jean
 */
import java.net.URL;
import java.net.URLClassLoader;

public class ServiceFactory {

    private static URL[] classLoaderUrls;

    public static void setURLs(URL[] urls) {
        classLoaderUrls = urls;
    }

    public static Class getService(String classe) throws Exception {
        URLClassLoader loader = new URLClassLoader(classLoaderUrls);
        return Class.forName(classe, true, loader);
    }
}
