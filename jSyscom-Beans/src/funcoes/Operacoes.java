/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package funcoes;

import beans.Variaveis;
import utilitarios.Extra;

/**
 *
 * @author jean
 */
public abstract class Operacoes {
    
    public static final Integer INFO    = 1000;
    public static final Integer BLOK    = 1001;
    public static final Integer DBLOK   = 1002;
    public static final Integer EXIT    = 1003;
    
    
    public static void executa(Integer codigoOperacao, String descricao){
        switch(codigoOperacao){
            //Operacoes Personalizadas
            
            //Operações do Sistema
            case 1000:
                Extra.Informacao(Variaveis.getFormularioPrincipal(), descricao);
                break;
            case 1001:
                Variaveis.setBloqueado(true);
                break;
            case 1002:
                Variaveis.setBloqueado(false);
                break;
            case 1003:
                System.exit(0);
                break;
            default:
                Variaveis.addNewLog("Operação solicitada não foi encontrada!", false, true, null);
                break;
        }
    }
}
